package com.cbc.portal.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.auth0.NonceGenerator;
import com.auth0.NonceStorage;
import com.auth0.RequestNonceStorage;
import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.EnrollBean;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.service.CardDetailsService;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.service.MemberDetailsService;
import com.cbc.portal.service.MemberEnrollService;
import com.cbc.portal.utils.ApplicationPropertiesUtil;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.DecryptJpNumberCards;
import com.cbc.portal.utils.OTPGeneration;
import com.cbc.portal.utils.VerifyRecaptcha;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ApplyOtherBankController {

    private Logger logger = Logger.getLogger(ApplyOtherBankController.class.getName());
    private final NonceGenerator nonceGenerator = new NonceGenerator();

    @Autowired
    ApplicationPropertiesUtil applicationPropertiesUtil;

    @Autowired
    CardDetailsService cardDetailsService;

    @Autowired
    MemberEnrollService memberEnrollService;

    @Autowired
    MemberDetailsService memberDetailsService;

    @Autowired
    CommonService commonService;
    
      
    @Value("${application.pii.encryption.strSoapURL1}")
	private String strSoapURL1;
    
    @Value("${application.pii.decrypt.strSOAPAction1}")
    private String strSOAPActiondecrypt;
    
    
    @Value("${application.pii.encrypt.strSOAPAction1}")
    private String strSOAPActionencrypt;
    
    
    @Value("${application.reset.password.url}")
    private String resetPasswordUrl;

    @RequestMapping(value = "/applyOther")
    public String applyOtherPage(HttpServletRequest request, ModelMap model) {
        String tier = "";
        int cpNo = 0;
        CardDetailsBean cardDetailsBean = new CardDetailsBean();
        String user = getLoggedInUser(request);

        String reqParam = request.getParameter("cpNo");
        String utm_source = request.getParameter("utm_source");
        String utm_medium = request.getParameter("utm_medium");
        String utm_campaign = request.getParameter("utm_campaign");
        logger.info("request param cpNo=== " + reqParam + " | jpNum=== " + request.getParameter("jpNum"));
        logger.info("ApplyOtherBankController ==> utm_source: " + utm_source + " utm_medium: " + utm_medium + " utm_campaign: " + utm_campaign);
//		try{
        if (null != reqParam && !reqParam.isEmpty()) {
            cpNo = Integer.parseInt(reqParam);
        }
        String jpNum = CommonUtils.nullSafe(request.getParameter("jpNum"));

        DecryptJpNumberCards decryptJpnumber = new DecryptJpNumberCards();
        jpNum = decryptJpnumber.DecryptJpnumber(jpNum,strSoapURL1,strSOAPActiondecrypt);


        cardDetailsBean.setCpNo(cpNo);
        cardDetailsBean.setJpNumber(jpNum);
//		cardDetailsBean.setJpNumber(CommonUtils.nullSafe(request.getParameter("jpNum")));
        AmexBean amexBean = new AmexBean();
        String status = "";
        try {
            memberDetailsService.getMemberDetails(cardDetailsBean.getJpNumber(), amexBean);
            tier = amexBean.getJetpriviligemembershipTier();
            if (tier.trim().length() != 0) {
                tier = tier.trim();
                cardDetailsBean.setJpTier(tier);
            }
            cardDetailsBean.setTitle(CommonUtils.nullSafe(amexBean.getTitle()));
            cardDetailsBean.setGender(CommonUtils.nullSafe(amexBean.getGender()));
            cardDetailsBean.setFname(CommonUtils.nullSafe(amexBean.getFname()));
            cardDetailsBean.setMname(CommonUtils.nullSafe(amexBean.getMname()));
            cardDetailsBean.setLname(CommonUtils.nullSafe(amexBean.getLname()));
            cardDetailsBean.setMobile(CommonUtils.nullSafe(amexBean.getMobile()));
            cardDetailsBean.setEmail(CommonUtils.nullSafe(amexBean.getEmail()));
            cardDetailsBean.setDob(CommonUtils.nullSafe(amexBean.getDateOfBirth()));
            cardDetailsBean.setUtmSource(utm_source);
            cardDetailsBean.setUtmMedium(utm_medium);
            cardDetailsBean.setUtmCampaign(utm_campaign);
            String pid=(String) request.getSession().getAttribute("PID");//new
            logger.info(pid+"------PID FOR JP NUMBER----"+jpNum);
                          cardDetailsBean.setPID(pid);

            status = commonService.addUserInformation(cardDetailsBean);
            model.addAttribute("bpupgradeurl", commonService.getUpgrdURL());
            model.addAttribute("bpupgrademessage", commonService.getUpgrdMessage());
            if ("success".equalsIgnoreCase(status)) {
                cardDetailsBean = cardDetailsService.getCardDetailsById(cpNo);
                String redirection = cardDetailsBean.getCardUrl().trim();
                redirection = redirection.contains("http") ? redirection : "http://" + redirection;
                model.addAttribute("redirection", redirection);
                request.setAttribute("redirectUrl", redirection);

            }
            if (null == user) {
                NonceStorage nonceStorage = new RequestNonceStorage(request);
                String nonce = nonceGenerator.generateNonce();
                nonceStorage.setState(nonce);
                model.addAttribute("state", nonce);
                request.setAttribute("state", nonce);
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ApplyOtherBankController applyOtherPage() :", e);
        }
        return "portal/cards/redirectToOtherBank";
    }

    @RequestMapping(value = "/applyOtherupgrade")
    public String applyOtherPageUpgrade(HttpServletRequest request, ModelMap model) {
        String tier = "";
        int cpNo = 0;
        CardDetailsBean cardDetailsBean = new CardDetailsBean();
        String user = getLoggedInUser(request);

        String reqParam = request.getParameter("cpNo");
        String utm_source = request.getParameter("utm_source");
        String utm_medium = request.getParameter("utm_medium");
        String utm_campaign = request.getParameter("utm_campaign");
        logger.info("request param cpNo=== " + reqParam + " | jpNum=== " + request.getParameter("jpNum"));

        if (null != reqParam && !reqParam.isEmpty()) {
            cpNo = Integer.parseInt(reqParam);
        }
        String jpNum = CommonUtils.nullSafe(request.getParameter("jpNum"));
        DecryptJpNumberCards decryptJpnumber = new DecryptJpNumberCards();
        jpNum = decryptJpnumber.DecryptJpnumber(jpNum,strSoapURL1,strSOAPActiondecrypt);

        cardDetailsBean.setCpNo(cpNo);
        cardDetailsBean.setJpNumber(jpNum);
//		cardDetailsBean.setJpNumber(CommonUtils.nullSafe(request.getParameter("jpNum")));
        AmexBean amexBean = new AmexBean();
        String status = "";
        try {
            memberDetailsService.getMemberDetails(cardDetailsBean.getJpNumber(), amexBean);
            tier = amexBean.getJetpriviligemembershipTier();
            if (tier.trim().length() != 0) {
                tier = tier.trim();
                cardDetailsBean.setJpTier(tier);
            }
            cardDetailsBean.setTitle(CommonUtils.nullSafe(amexBean.getTitle()));
            cardDetailsBean.setGender(CommonUtils.nullSafe(amexBean.getGender()));
            cardDetailsBean.setFname(CommonUtils.nullSafe(amexBean.getFname()));
            cardDetailsBean.setMname(CommonUtils.nullSafe(amexBean.getMname()));
            cardDetailsBean.setLname(CommonUtils.nullSafe(amexBean.getLname()));
            cardDetailsBean.setMobile(CommonUtils.nullSafe(amexBean.getMobile()));
            cardDetailsBean.setEmail(CommonUtils.nullSafe(amexBean.getEmail()));
            cardDetailsBean.setDob(CommonUtils.nullSafe(amexBean.getDateOfBirth()));
            cardDetailsBean.setUtmSource(utm_source);
            cardDetailsBean.setUtmMedium(utm_medium);
            cardDetailsBean.setUtmCampaign(utm_campaign);

            status = commonService.addUserInformation(cardDetailsBean);
            model.addAttribute("bpupgradeurl", commonService.getUpgrdURL());
            model.addAttribute("bpupgrademessage", commonService.getUpgrdMessage());
            if ("success".equalsIgnoreCase(status)) {
                cardDetailsBean = cardDetailsService.getCardDetailsById(cpNo);
                String redirection = commonService.getUpgrdURL();

                redirection = redirection.contains("http") ? redirection : "http://" + redirection;
                model.addAttribute("redirection", redirection);
                request.setAttribute("redirectUrl", redirection);

            }
            if (null == user) {
                NonceStorage nonceStorage = new RequestNonceStorage(request);
                String nonce = nonceGenerator.generateNonce();
                nonceStorage.setState(nonce);
                model.addAttribute("state", nonce);
                request.setAttribute("state", nonce);
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ApplyOtherBankController applyOtherPage() :", e);
        }
        return "portal/cards/redirectToOtherBank";
    }

    @RequestMapping(value = "/enrollme", method = RequestMethod.POST)
    @ResponseBody
    public String enrollmeFunction(@ModelAttribute("enrollBean") EnrollBean enrollBean, HttpServletRequest request) {
       String jpNumber = "";
        String ResponseDescription = "";
        OTPGeneration oTPGeneration=new OTPGeneration();
        JSONObject json = null ;
        Map<String,String> hashMap=new HashMap<String,String>();
        json  = new JSONObject();
        try {
            hashMap = memberEnrollService.memberEnrollment(enrollBean, request);
            
            for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                
                if(entry.getKey().equalsIgnoreCase("JPNumber")){
                    jpNumber=entry.getValue();
                   
                    
                }else if(entry.getKey().equalsIgnoreCase("ResponseDescription")){
                  ResponseDescription=entry.getValue();
                }
                

            }
 
            if (!jpNumber.equalsIgnoreCase(null)  && jpNumber.length()>= 9) {
              
                 json.put("jpNumber", jpNumber);
                 
                 json.put("msg", "");
                 if(jpNumber.length()==9){
               logger.info("jpNumber length is 09s : "+jpNumber+"  resetPasswordUrl :"+resetPasswordUrl);
                     oTPGeneration.callMac3service(jpNumber,resetPasswordUrl);
                
                 }else if(jpNumber.length()==11){
                     
               logger.info("jpNumber length is 11 : "+jpNumber+"  resetPasswordUrl :"+resetPasswordUrl);
                 oTPGeneration.callMac3service(jpNumber.substring(2, 11),resetPasswordUrl);
                 }
                 
                 
                 
            } else {
                logger.info("ResponseDescription  :"+ResponseDescription);
//                ResponseDescription="Email Address belongs to some other member";
                
               String errormsg="";
                 
//                 if(ResponseDescription.equalsIgnoreCase("Same email id already exists for another member. Please try with another email id")){
//                 ResponseDescription="Sorry! This Email ID is already present against another registered and verified JetPrivilege Number. Please provide a different Email ID.";
//                 }else if(ResponseDescription.equalsIgnoreCase("Same Mobile no already exists for another member. Please try with another Mobile Number")){
//                 ResponseDescription="Sorry! This Mobile Number is already present against another registered and verified JetPrivilege Number. Please provide a different Mobile Number.";
//                 }else if(ResponseDescription.equalsIgnoreCase("Duplicate record found")){
//                  ResponseDescription=ResponseDescription;
                 
                 if(ResponseDescription != null && !ResponseDescription.isEmpty() && !"".equals(ResponseDescription)){
                     errormsg=commonService.getEnrollCodeError(ResponseDescription);
                 logger.info("-----inside if condition---->"+errormsg);
                     if(("null").equals(errormsg) || "".equals(errormsg)){
                     errormsg="Your application may not have gone through successfully. Please click Submit again.";
                     }
                 if(errormsg == null){
                          errormsg=commonService.getEnrollCodeError("DEFAULT");

                     logger.info("-----inside if  condition of null---->"+errormsg);
//                 
                     }
                 
                 
                 }
                 else{
                        errormsg=commonService.getEnrollCodeError("DEFAULT");
                     logger.info("-----inside else condition---->"+errormsg);
                 errormsg="";
                 }
                 logger.info("-----errormsg---->"+errormsg);
                 json.put("jpNumber", jpNumber);
                 json.put("msg", errormsg);
            }
            
                 

        } catch (Exception e) {
           

            logger.error("@@@@ Exception in ApplyOtherBankController enrollmeFunction() :", e);
        }
        logger.info("-------------"+json.toString());
        return json.toString();
    }

    public String getLoggedInUser(HttpServletRequest request) {
        return (String) request.getSession().getAttribute("loggedInUser");

    }

    @RequestMapping(value = "/verifycaptcha", method = RequestMethod.POST)
    @ResponseBody
    public boolean verifycaptcha(@RequestParam("responsecode") String responsecode, HttpServletRequest request) throws IOException {
//         String gRecaptchaResponse = request.getParameter("g-recaptcha-response");

        boolean verify = false;
        try {

            String str = "";
            str = "" + request.getSession().getAttribute("responscode");

            if (str.equalsIgnoreCase(responsecode)) {
                verify = true;
            } else {
                request.getSession().setAttribute("responscode", responsecode);
                verify = VerifyRecaptcha.verify(responsecode);
            }

        } catch (Exception E) {

        }

        return verify;
    }

}
