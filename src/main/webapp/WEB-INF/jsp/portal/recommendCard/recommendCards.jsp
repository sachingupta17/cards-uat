<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<!--<div id="social-profiles" data-toggle="modal"  class="hidden" onclick="assistMeeClick()">  
    <img src="${applicationURL}static/images/footer/phone-call.png" width="30" height="30">
    <h5>Assist Me</h5>
</div> -->

<form:form autocomplete="off" id="callMe" commandName="callMeBean">
    <div class="modal fade" id="callMe_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div id="callmeclose" class="proceedPopup_close pull-right">
                        <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="formAbandonOfAssistMe()">
                    </div>
                    <div class="callme_popup">  
                        <div class="text-center">
                            Fill in your details for us to call you & help you complete the credit card application
                        </div>
                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Tell us about yourself</h2>
                        </div>
                        <div class="error_box1 hidden">
                            <ul class="error_list1"></ul>
                        </div>
                        <div class="row">
                            <div class="enroll_div margT2">
                                <div class="middleName_input user_details">  
                                    <form:input id="socialName" path="socialName"  placeholder="Full Name" type="text" value="" />
                                </div>
                                <div class="phone_input user_details">
                                    <form:input id="socialPhone" path="socialPhone"  maxlength="10" placeholder="Phone" type="number"  onKeyPress="if(this.value.length==10) return false;" value="" />
                                </div>
                                <div class="email_input user_details">
                                    <form:input id="socialEmail" path="socialEmail"  placeholder="Email" type="text" value="" />
                                </div>

                                <!--                                <div class="jpNumber_input user_details">
                                <%--<form:input id="socialjpNumber" path="socialjpNumber"  placeholder="JetPrivilege Number" type="number" value="" maxlength="9" onKeyPress="if(this.value.length==9) return false;"/>--%>
                            </div>-->
                            </div>
                            <form:hidden path="formNumber" id="formNumber"/>
                            <form:hidden path="pageName" id="pageName"/>
                            <form:hidden path="cardName" id="cardName"/>
                        </div>
                        <div id="loader" class="text_center hidden margT5">
                            <img src="${applicationURL}static/images/header/ajax-loader.gif" alt="Page is loading, please wait." />
                        </div>
                        <div class="text-center margT10">
                            <input class="button_thickBlue" type="button" id="callMeContine" value="Submit">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form:form>
<div class="modal fade" id="successCallmeModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="sizer modal-content">
            <div class="modal-header">
                <!--              <div class="proceedPopup_close pull-right">
                                        <img src="$ {applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.CloseOnPopUpClick('Enroll Now')">
                                    </div>-->

            </div>
            <div class="modal-body">
                <p class="modaltext" style="color:#01a200; text-align:center; font-size: 18px; ">Thank you for sharing your details! Our representative will reach out to you for further assistance.</p>


            </div>
            <div class="modal-footer">
                <button type="button" class="successcallbut" data-dismiss="modal">OK</button>
            </div>

        </div>

    </div>
</div>


<div class="modal fade" id="filterPopup" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="filterPopup">
                    <div class="filter_title">
                        <ul class="list-inline">
                            <li>FILTERS</li>
                            <li class="close_filters"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></li>
                        </ul>
                    </div>
                    <div>
                        <ul class="filter_tab list-inline">
                            <li class="filter">
                                <span>FILTERS</span>
                            </li>
                            <li class="sort">
                                <span>SORT</span>
                            </li>
                        </ul>
                    </div>
                    <div class="filter_area">
                        <div class="banks filter_wrap margT10">
                            <span class="filter_names">Bank</span>
                            <span class="dropdown">
                                <img src="${applicationURL}static/images/co-brand/filterdrop.png"  alt="">
                            </span>
                        </div>
                        <div class="banks_listing parent_div hidden">
                            <div class="arrow-up"></div>
                            <ul class="banks_list">
                                <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                                    <c:forEach items="${bankList}" var="list">
                                    <li>
                                        <input type="checkbox" name ="bankName" id="mob_bankName${list.bpNo}" class="mob_bankNum" value="${list.bpNo}">
                                        <label for="mob_bankName${list.bpNo}">
                                            <img src="${applicationURL}banks/${list.bankImagePath}" class="bank_logos">
                                            <span>${list.bankName}</span>
                                        </label>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="joiningFees filter_wrap margT10">
                            <span class="filter_names">Joining Fees</span>
                            <span class="dropdown">
                                <img src="${applicationURL}static/images/co-brand/filterdrop.png"  alt="">
                            </span>
                        </div>
                        <div class="slider_div parent_div filter_content hidden">
                            <div class="arrow-up"></div>
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                            <div class="slider_valu margB5 margT5">
                                <div style="display:inline">&#8377; 500 </div>
                                <div style="display:inline; float:right;"> &#8377; 10,000</div>
                            </div>
                            <input min="500" max="10000" step="500" value="700" data-orientation="horizontal" type="range" id="fee_range">
                            <div class="text-center margT10">
                                <input class="filter_button button_red" type="button" value="Done">
                            </div>
                        </div>
                        <div class="life_benefits filter_wrap margT10">
                            <span class="filter_names">Lifestyle Benefits</span>
                            <span class="dropdown">
                                <img src="${applicationURL}static/images/co-brand/filterdrop.png"  alt="">
                            </span>
                        </div>
                        <div class="life_benefits_list parent_div filter_content hidden">
                            <div class="arrow-up"></div>
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                            <div class="row">
                                <div class="life_benefits_holder" >
                                    <ul>
                                        <c:forEach items="${prefList}" var="list">
                                            <li>
                                                <input class="mob_lsbpNum" name ="lsbpName" id="mob_lsbpName${list.lsbpNo}" type="checkbox" value="${list.lsbpNo}" onclick="selectBenefits(this);">
                                                <label for="mob_lsbpName${list.lsbpNo}" >
                                                    <img src="${applicationURL}lifestyles/${list.lsbpImageIconPath}"  alt=""  >
                                                    <span>${list.imageAsIconText}</span>
                                                    <img src="${applicationURL}lifestyles/${list.lsbpImageIconBluePath}"  alt="" style="display:none">
                                                    <span style="display:none">${list.imageAsIconText}</span></label>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="filter-btn-holder filter_holder margB5">
                            <ul>
                                <li>
                                    <div class="clearFilters" id="mob_clearFilters">Clear</div>
                                </li>
                                <li>
                                    <div class="apply-btn" id="mob_apply-btn">Apply</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="sort_tab margT10 hidden">
                        <div class="sort_div">
                            <span>Sort By</span>
                        </div>
                    </div>
                    <div class="sort_listing parent_div hidden">
                        <div class="arrow-up"></div>
                        <ul class="sort_list">
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                            <li class="mob_sorting_ascending">
                                Bank (A-Z)
                            </li>
                            <li class="mob_sorting_descending">
                                Bank (Z-A)
                            </li>
                            <li class="mob_sort_by_fee_asc">
                                Fees (Low to High)
                            </li>
                            <li class="mob_sort_by_fee_dsc">
                                Fees (High to Low)
                            </li>
                            <li class="mob_sort_by_lyf_asc">
                                Lifestyle Benefit (Min to Max)
                            </li>
                            <li class="mob_sort_by_lyf_dsc">
                                Lifestyle Benefit (Max to Min)
                            </li>
                            <li class="air_ticket_asc">
                                Air Tickets (Min to Max)
                            </li>
                            <li class="air_ticket_dsc">
                                Air Tickets (Max to Min)
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="spends_slider" tabindex="-1" role="dialog">

    <div class="modal-dialog vertical-align-center">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body calculatemiles">   
                <div class="proceedPopup_close pull-right">
                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.formAbandonmentOnSubmitJPnumberPopup('Pop-up close', 'Submit JPNumber')">
                </div>             
                <div class="spendsSlider_popup margT5">
                    <div class="margT2 colorDarkBlue text-center">
                        <h2 class="heading_txt">Calculate your Free Flights based on your monthly spends</h2>
                    </div>
                    <div style="padding-top: 10px; text-align: center;" class="spendsVal_error">
                        <span id="error_msg" style="font-size: 14px; color: red; display:none;"></span>
                    </div>
                    <div class="col-md-12">
                        <p class="amtvalue1">Annual Spends : INR <span class="slider-output" id="yearly_bill1">0</span></p>

                    </div>
                    <div class="col-md-9">
                        <div class="range-example-modal"></div>
                        <div class="imagebar1"><img src="${applicationURL}static/images/co-brand/numbar.png"/></div>

                    </div>

                    <div class="col-md-3"><div class="aheadiv">INR</div><input id="unranged-value2" type="text" inputmode="numeric" name="amountInput"  value="0" maxlength="9" /><div class="donebut">Done</div></div>
                    <div class="slider_modal_div_error"></div>
                    <!--                    <div class="margT2 colorDarkBlue text-center">
                                            <h2 class="heading_txt">Calculate your Free Flights based on your monthly spends</h2>
                                        </div>
                                        <div style="padding-top: 10px; text-align: center;" class="spendsVal_error">
                                            <span id="error_msg" style="font-size: 14px; color: red; display:none;"></span>
                                        </div>
                                        <div class="slider_valu margB5">
                                            <div style="display:inline"><span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5,000</span></div>
                                            <div style="display:inline; float:right;"><span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span></div>
                                        </div>
                                        <div class="row">
                    
                                            <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal">										   									   
                                        </div>-->
                </div>
                <!--                <div class="annualSpends margT5 text-center">
                                    <span>Annual Spends: </span><span class="annualSpends_value">0</span>
                                </div>	
                                <div class="text-center margT2">
                                    <input class="button_red annual_spends" type="button" value="Done">
                                </div>-->
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="abtYourself" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="proceedPopup_close pull-right">
                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="formAbandonmentOnPopUp('Pop-up close')">
                </div>
                <form:form autocomplete="off" id="tellMeAbout" action="applyOther" commandName="cardDetailsBean" method="post">     
                    <div class="jpNum_popup">
                        <div class="text-center jpPopup_header">
                            Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-brand Card. Apply here
                        </div>

                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Provide InterMiles No.</h2>
                        </div>

                        <div class="row">
                            <div class="jpNumber_div margT2">
                                <div class="jpNumber_input">
                                    <%--<form:input path="jpNumber" id="jpNumber" placeholder="JetPrivilege Number" maxlength="9" pattern="[0-9]*" />--%>

                                    <c:choose>
                                        <c:when test="${empty sessionScope.loggedInUser}">
                                            <form:input path="jpNumber" id="jpNumber" type="number" placeholder="InterMiles No." maxlength="9" pattern="[0-9]*" onkeypress="if(this.value.length==9) return false;"/>
                                        </c:when>
                                        <c:otherwise>

                                            <form:input path="jpNumber" id="jpNumber" type="number" placeholder="InterMiles No."  maxlength="9" pattern="[0-9]*" onkeypress="if(this.value.length==9) return false;" />
                                        </c:otherwise>
                                    </c:choose>



                                </div>
                                <div class="jpNumber_input margT2 hidden">
                                    <form:hidden path="jpTier" id="jpTier" placeholder="InterMiles Membership Tier" disabled="true" pattern="[0-9]*" />
                                    <form:hidden path="cpNo" id="otcpNo"/>
                                    <input type="hidden" id="otbpNo"/>
                                </div>
                                <%--<c:if test="${empty sessionScope.loggedInUser}">--%>
                                <div class="text-center margT2">
                                    If you are not a InterMiles member, <a href="javascript:void(0)" class="enroll_here"  id="enroll_here" style="text-decoration: underline;" onclick="afterEnrollHereClick('Enroll here', 'co-brand quick enrollment')">please enroll here</a>
                                </div>
                                <%--</c:if>--%>
                            </div>
                        </div>
                        <div class="text-center margT10">
                            <input class="button_thickBlue" type="button" id="tellme" onClick="$(this).closest('form').submit();" value="Submit">
                        </div>
                    </div>
                </form:form>

                <form:form autocomplete="off" id="quickEnrollUser" action="enrollme" commandName="enrollBean">
                    <form:hidden path="enrollbpNo"/>
                    <div class="enroll_popup hidden">
                        <div class="text-center">
                            Enroll into the InterMiles Programme now to start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-Brand Card.
                        </div>
                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Enroll Now</h2>
                        </div>
                        <div class="error_box2 hidden">
                            <ul class="error_list2">
                            </ul>
                        </div>
                        <div class="row">
                            <div class="pull-left margT2 nav_web">
                                <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_slide goTo_abtYourself_div" onclick="$.fn.formAbandonmentOnSubmitJPnumberPopup('pop-up back', 'co-brand quick enrollment')">
                            </div>
                            <div class="enroll_div margT2">
                                <div class="title user_details">
                                    <form:select path="enrollTitle" id="enrollTitle" placeholder="Title" class="title_select">
                                        <form:option value="">Select Title</form:option>
                                        <form:option value="Mr">Mr</form:option>
                                        <form:option value="Ms">Ms</form:option>
                                        <form:option value="Mrs">Mrs</form:option>
                                        <form:option value="Dr">Doctor</form:option>
                                        <form:option value="Prof">Professor</form:option>
                                        <form:option value="Captain">Captain</form:option>
                                    </form:select>
                                </div>
                                <div class="gender hidden">
                                    <c:forEach items="${genderStatus}" var="status">
                                        <div style="display:inline-block">
                                            <label class="radio-inline">
                                                <form:radiobutton
                                                    path="enrollGender" name="enrollgender"
                                                    id="enrollGender"
                                                    value="${status.key}" /> </label>
                                            <label class="gender_desc" for="${status.key}" >${status.value}</label>
                                        </div>
                                    </c:forEach>
                                </div>
                                <div class="firstName_input user_details">
                                    <form:input type="text" path="enrollFname" id="enrollFname" placeholder="First Name"/>
                                </div>
                                <div class="middleName_input user_details">
                                    <form:input type="text" path="enrollMname" id="enrollMname" placeholder="Middle Name"/>
                                </div>
                                <div class="lastName_input user_details">
                                    <form:input type="text" path="enrollLname" id="enrollLname" placeholder="Last Name"/>
                                </div>
                                <div class="city_input user_details">
                                    <form:input type="text" path="enrollCity" id="enrollCity" placeholder="City"/>
                                </div>
                                <div class="phone_input user_details">
                                    <form:input path="enrollPhone" id="enrollPhone" maxlength="10" type="number" placeholder="Phone" onkeypress="if(this.value.length==10) return false;"/>
                                </div>
                                <div class="email_input user_details">
                                    <form:input path="enrollemail" id="enrollemail" placeholder="Email"/>
                                </div>
                                <!--                                <div class="password_input user_details">
                                                                    < form:password path="enrollPassword" id="enrollPassword" placeholder="Password"/>
                                                                    < form:hidden path="hashPassword" id="hashPassword"/>
                                                                </div>
                                                                <div class="password_input user_details">
                                                                    < form:password path="enrollRenterPassword" id="enrollRenterPassword" placeholder="Re Enter Password"/>
                                                                </div>-->
                                <div class="dob_input user_details">
                                    <form:input path="enrollDob" id="date_of_birth2" class="datepicker" readonly="true" placeholder="Date Of Birth"/>
                                </div>
                                <div class="g-recaptcha recaptchaarea"
                                     data-sitekey=<spring:message code="google.recaptcha.site"/>  id="captch"></div>
                                <div> 
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="termsAndConditionsEnroll" class="margT1 statement" path="termsAndConditionsEnroll" /> 
                                        <span>I agree to the InterMiles membership 
                                            <a href="https://www.jetprivilege.com/terms-and-conditions/jetprivilege"  target="_blank">
                                                Terms and Conditions</a><span style="margin: 0px 4px;display: inline;">and</span> <a href="https://www.jetprivilege.com/disclaimer-policy"  target="_blank">
                                                Privacy Policy</a></span>
                                    </div>
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="recieveMarketingTeam" class="margT1 statement" path="recieveMarketingTeam" />  
                                        <span> Yes, I would like to receive Marketing Communication


                                        </span>
                                    </div>
                                </div>
                                <div class="error_box_enrolStatus hidden" id="enroll_error">
                                </div>
                            </div>
                        </div>
                        <div id="loader" class="text_center hidden margT5">
                            <img src="${applicationURL}static/images/header/ajax-loader.gif" alt="Page is loading, please wait." />
                        </div>
                        <div class="text-center margT10 nav_web">
                            <input class="button_thickBlue" type="button"  id="enrollForm" value="Continue">
                        </div>
                        <div class="nav_arrow_mobile margT10">
                            <div class="left_nav">
                                <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_arrow goTo_abtYourself_div" onclick="enrollHereBackClick('Back to enroll here', 'Submit JPNumber')">
                            </div>
                            <div class="continue_button">
                                <input class="button_thickBlue" type="button" id="enrollForm_mob_view" value="Continue">
                            </div>
                        </div>
                    </div>  
                </form:form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="proceed_modal" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">                
                <div class="proceed_popup">
                    <div class="proceed_popup_title">
                        <h2>
                            <span>Earn with Partners: Swipe any of our Co-brand Cards</span>
                        </h2>
                        <div class="proceedPopup_close pull-right">
                            <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.CloseOnPopUpClick(popupName)">
                        </div>
                    </div>
                    <div class="row card_toProceed margT2">
                        <div class="card_display text-center pad5">
                            <div class="">
                                <h4 id="popCardName"></h4>
                            </div>
                            <div class="card_img_holder card_img">
                                <img  id="replaceImg">
                            </div>
                            <div class="fee_value">
                                <h4>Fees: <span id="popFees"></span></h4>
                            </div>
                        </div>
                        <div class="card_details pad5 text-center margT5">
                            <div class="membershipNo_div padB10">
                                <p>Your InterMiles No. is</p>
                                <h4><span class="membershipNo" id="jpNo"></span></h4>
                            </div>
                            <div class="margT5">
                                <p>Please ensure to enter your same InterMiles No. in your application form</p>
                            </div>
                            <div class="margT5 proceed">
                                <a href="#" class="button_blue margT5 proceedToBank" id="proceedTo" target="_blank">
                                    <span id="proceedBank" onclick="$.fn.IndusIndProceedClick()"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="benefits" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="pull-right close_div"><img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg"  alt=""></div>
                <h5 class="margT2 colorDarkBlue pad2">Benefits</h5>
                <span class="list_cont" id="benefitContent"></span>
            </div>
        </div>
    </div>
</div>

<form:form autocomplete="off" commandName="cardDetailsBean" id="cardDetailsBean" enctype="" action="best-co-branded-cards" method="post">


    <section class="innerContainer clearfix bottomPadding">
        <section class="main-section">      
            <div class="banner" style="background:url('${applicationURL}banners/${banner.bannerAttachementPath}');" title="InterMiles Co-brand Credit Cards, Debit Cards & Corporate Cards">
                <div class="container">
                    <div class="banner_textarea">
                        <h1>Turn all your spends <br>
                            on the ground into miles in the sky.</h1>
                        <h3>Get any of our Co-Brand Cards</h3>

                        <div class="banner_button margT5">

                            <a href="${applicationURL}cardswidget?recommendPage=1"><button type="button" class="button_blue recommend_card pull-left" id="CrdRC_rec-me-a-card" onclick="$.fn.HelpMeChooseACardClick()">Help me choose a card</button></a>
                            <a href="${applicationURL}"><button type="button" class="button_blue2" id="CrdRC_choose-own-card">I will choose my own card</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="container">
            <div class="cont_area">  
                <h1>
                    Best Co brand Cards for you 
                </h1>
            </div>
            <div class="recommended_cards">
                <form:hidden path="gmNo" id="gmNo" value="${recommendList[0].gmNo}"/>
                <div class="swiptopband">
                    <div class="dropdown swiptopbanddrop">
                        <a data-toggle="dropdown" href="#"><span>Swipe<img src="${applicationURL}static/images/co-brand/swipdrop.png" class="img-responsive"></span></a>
                        <ul class="swiptopbanddrop-menu" role="menu" aria-labelledby="dLabel">
                            <li><a href="#">Fly</a></li>
                            <li><a href="#">Swipe</a></li>
                            <li><a href="#">Convert</a></li>
                            <li><a href="#">Stay</a></li>
                            <li><a href="#">Shop</a></li>
                            <li><a href="#">Dine</a></li>
                            <li><a href="#">Talk</a></li>
                            <li><a href="#">Live it up</a></li>
                            <li><a href="#">Read</a></li>
                            <li><a href="#">Rent-a-car</a></li>
                        </ul>
                    </div>
                </div>	
                <!-- filter area 2 start -->
                <div class="filterarea2">
                    <ul>
                        <li><span>Filter By:</span></li>
                        <li>
                            <div class="filterareadrop2">
                                <select class="" id="filterareadropdown">
                                    <option selected="selected">Personal Credit Card (<c:out value="${recommendList.size()}"></c:out>)</option>
                                        <option>Other Cards</option>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="filterareadrop2">
                                    <select class="" id="filterareadropdownhide">
                                        <option selected="selected">India</option>
                                    </select>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- filter area 1 start -->

                    <div class="row filter_div margT2">
                        <div class="col-sm-2">
                            <label>Filter By: </label>
                        </div>
                        <div class="col-sm-2">
                            <span class="banks" id="CrdRC_fltr_Bank">
                                <label class="dropDown"> Bank </label>
                                <img src="${applicationURL}static/images/co-brand/filter_dropdown.png" alt="" class="margL10">
                        </span>
                        <span class="margL15"> | </span>
                    </div>
                    <div class="banks_listing parent_div hidden">
                        <div class="arrow-up"></div>
                        <ul class="banks_list">
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                                <c:forEach items="${bankList}" var="list">	
                                <li>
                                    <input type="checkbox" name ="bankName" id="bankName${list.bpNo}" class="bankNum" value="${list.bpNo}">
                                    <label for="bankName${list.bpNo}">
                                        <img src="${applicationURL}banks/${list.bankImagePath}" class="bank_logos">
                                        <c:choose>
                                            <c:when test="${list.bpNo eq 1}">
                                                <span>${list.bankName} <sup>&reg;</sup> </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span>${list.bankName}</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </label>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                    <div class="col-sm-3 ">
                        <span class="joiningFees" id="CrdRC_fltr_Joiningfees">
                            <label>Joining Fees </label>
                            <img src="${applicationURL}static/images/co-brand/filter_dropdown.png" class="margL10">	
                        </span>
                        <span class="margL15"> | </span>	

                        <div class="slider_div parent_div hidden">
                            <div class="arrow-up"></div>
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                            <div class="slider_valu margB5 margT5">  
                                <div style="display:inline"><span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span>
                                    <span>500</span>
                                </div>
                                <div style="display:inline; float:right;"><span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span>
                                    <span> 10,000</span>
                                </div>
                            </div>
                            <input min="500" max="10000" step="500" data-orientation="horizontal" type="range" id="fee_range">
                            <div class="text-center margT7">
                                <input class="filter_button button_red" id="filter_jf" type="button" value="Done" onclick="submitFilterAdobe()">
                            </div>
                        </div>						
                    </div>
                    <div class="col-sm-2 life_benefits_parent">
                        <span class="life_benefits" id="CrdRC_fltr_LifeStyle-Benefits">
                            <label>Lifestyle Benefits</label>
                            <img src="${applicationURL}static/images/co-brand/filter_dropdown.png" class="margL10">	
                        </span>

                        <div class="life_benefits_list parent_div hidden">
                            <div class="arrow-up"></div>
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                            <div class="row"> 
                                <div class="life_benefits_holder" >
                                    <ul>
                                        <c:forEach items="${prefList}" var="list">
                                            <li>
                                                <input class="lsbpNum" name ="lsbpName" id="lsbpName${list.lsbpNo}" type="checkbox" value="${list.lsbpNo}" onclick="selectBenefits(this);">
                                                <label for="lsbpName${list.lsbpNo}" >
                                                    <img src="${applicationURL}lifestyles/${list.lsbpImageIconPath}"  alt=""  >
                                                    <span>${list.imageAsIconText}</span>
                                                    <img src="${applicationURL}lifestyles/${list.lsbpImageIconBluePath}"  alt="" style="display:none">
                                                    <span style="display:none">${list.imageAsIconText}</span></label>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div> 
                            </div>
                        </div>					
                    </div>
                    <div class="col-sm-3 clearFilters">
                        <span id="clear_fliter">Clear Filters</a></span>
                        <span class="sort_by" id="CrdRC_fltr_sort-by">Sort By</span>
                        <div class="sort_listing parent_div hidden">
                            <div class="arrow-up"></div>
                            <ul class="sort_list">
                                <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                                <li class="sorting_ascending">
                                    Bank (A-Z)
                                </li>
                                <li class="sorting_descending">
                                    Bank (Z-A)
                                </li>
                                <li class="sort_by_fee_asc">
                                    Fees (Low to High)
                                </li>
                                <li class="sort_by_fee_dsc">
                                    Fees (High to Low)
                                </li>
                                <li class="sort_by_lyf_asc">
                                    Lifestyle Benefit (Min to Max)
                                </li>
                                <li class="sort_by_lyf_dsc">
                                    Lifestyle Benefit (Max to Min)
                                </li>
                                <li class="air_ticket_asc">
                                    Air Tickets (Min to Max)
                                </li>
                                <li class="air_ticket_dsc">
                                    Air Tickets (Max to Min)
                                </li>
                            </ul>
                        </div>										
                    </div>	
                </div>
                <div style="padding-top: 10px; text-align: center;">
                    <span id="error_msg" style="font-size: 14px; color: red; display:none;"></span>
                </div>
                <div class="freeFlights_calc margT2" id="CrdRC_btn_cal-ur-ff-asper-spend">
                    <div class="flight-calc-holder">
                        <span>
                            <p> Calculate your free flights based on your monthly spends </p>
                        </span>
                        <!--<li><img src="images/co-brand/filter_dropdown.png" class="margL15 filter_icon" alt=""></li>-->                                                 
                    </div>
                    <div class="spends_calc">
                        <div class="col-md-12">
                            <p class="amtvalue">Annual Spends : INR <span class="slider-output" id="yearly_bill">0</span></p>

                        </div>
                        <div class="col-md-9">
                            <div class="range-example-5"></div>
                            <div class="imagebar"><img src="${applicationURL}static/images/co-brand/numbar.png"/></div>

                        </div>

                        <div class="col-md-3">
                            <div class="rightdata">
                                <div class="aheadiv">INR</div>
                                <input id="unranged-value1" type="text" inputmode="numeric" name="amountInput"  value="0" maxlength="9" />
                                <div class="donebut donebutdone">Done</div>
                                <div class="slider_error_div"></div>
                            </div>   
                        </div>
                        <!--                    <div class="spends_calc margT2">
                                                <div class="slider_wrapper">								
                        
                                                    <div class="row">
                                                        <div class="slider_division col-sm-8">
                                                            <div class="slider_valu rangeMobile">
                                                                <div style="display:inline; float:left">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5,000</span> 
                                                                </div>
                                                                <div style="display:inline; float:right;">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span>
                                                                </div>
                                                            </div>
                                                            <ul class="slider_view">
                                                                <li class="rangeWeb">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5,000</span>
                                                                </li>
                                                                <li>
                                                                    <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal" class="">
                                                                </li>
                                                                <li class="rangeWeb">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="annualSpends_wrapper col-sm-4">
                                                            <div class="annualSpends text-center">
                                                                <span>Annual Spends: </span><span class="annualSpends_value">0</span>
                                                            </div>
                                                            <div class="text-center">
                                                                <input type="button" class="filter_button button_red annual_spends" id="CrdRC_btn_cal-ur-ff-asper-spend" value="Done">
                                                            </div>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>--><!--                    <div class="spends_calc margT2">
                                                <div class="slider_wrapper">								
                        
                                                    <div class="row">
                                                        <div class="slider_division col-sm-8">
                                                            <div class="slider_valu rangeMobile">
                                                                <div style="display:inline; float:left">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5,000</span> 
                                                                </div>
                                                                <div style="display:inline; float:right;">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span>
                                                                </div>
                                                            </div>
                                                            <ul class="slider_view">
                                                                <li class="rangeWeb">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5,000</span>
                                                                </li>
                                                                <li>
                                                                    <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal" class="">
                                                                </li>
                                                                <li class="rangeWeb">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="annualSpends_wrapper col-sm-4">
                                                            <div class="annualSpends text-center">
                                                                <span>Annual Spends: </span><span class="annualSpends_value">0</span>
                                                            </div>
                                                            <div class="text-center">
                                                                <input type="button" class="filter_button button_red annual_spends" id="CrdRC_btn_cal-ur-ff-asper-spend" value="Done">
                                                            </div>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>-->
                    </div>
                </div>
                <div class="row margT2 recCards_title">
                    <p>Top recommended cards</p>
                </div>
                </hr>
                <div id="recommendedCards_div" class="margT2">
                    <c:forEach items="${recommendList}" var="list">
                        <form:hidden path="gmNo" value="${list.gmNo}"/>
                        <div class="checkname" id="${pageinfo}" hidden></div>
                        <div class="recommendedCards margB2">
                            <div class="rec_features">
                                <div class="rcommend_div" id="card_row${list.cpNo}">
                                    <div class="platinumCard_div">
                                        <strong>${list.cardName}</strong>
                                    </div>
                                    <div class="margT2">
                                        <div class="margT2">
                                            <img
                                                src="${applicationURL}static/images/co-brand/jet_icon_orange.png"
                                                class="orangeIcon">
                                            <p>${list.ticketMessage}</p>
                                        </div>
                                        <div class="margT2">
                                            <img
                                                src="${applicationURL}static/images/co-brand/jet_icon_orange.png"
                                                class="orangeIcon">
                                            <p>${list.lyfPrefMessage}</p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="features margT2">
                                        <div class="header_freeze">
                                            <div class="features_title title_header benefits_web">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <p>Cards</p>
                                                    </div>
                                                    <c:forEach items="${groupHeadings}" var="heading">
                                                        <div class="col-sm-2">
                                                            <p>${heading}</p>
                                                        </div>
                                                    </c:forEach>
                                                    <div class="col-sm-2 free_flight_basis">
                                                        <div class="">
                                                            <p class="freeFlight_spends">Free flight basis</p>
                                                            <div class="freeFlight_spends_calc parent_div hidden">
                                                                <div class="arrow-up"></div>
                                                                <div class="pull-right close_filter">
                                                                    <img
                                                                        src="${applicationURL}static/images/co-brand/close_btn.png"
                                                                        alt="">
                                                                </div>
                                                                <div class="slider_valu  margB5 margT5">
                                                                    <div
                                                                        style="display: inline; text-align: left !important;">INR
                                                                        5,000</div>
                                                                    <div style="display: inline; float: right;">INR 15
                                                                        Lakhs</div>
                                                                </div>
                                                                <input type="range" min="5000" max="1500000" step="5000"
                                                                       data-orientation="horizontal">
                                                                <div class="annualSpends margT10 text-center">
                                                                    <span>Annual Spends: </span><span
                                                                        class="annualSpends_value">INR 60,000</span>
                                                                </div>
                                                                <div class="text-center">
                                                                    <input class="filter_button button_red annual_spends"
                                                                           type="button" value="Done">
                                                                </div>
                                                            </div>
                                                            <p>
                                                                spends <a class="tooltips" href="javascript:void(0);"><img
                                                                        src="${applicationURL}static/images/co-brand/inrimg.png">
                                                                    <span class="tooltip_content" id="CrdRC_icon_info-i">
                                                                        <span class="pull-right close_toolTip"><img
                                                                                src="${applicationURL}static/images/co-brand/close_btn.png"
                                                                                alt=""></span> The free flights calculation basis
                                                                        spends is calculated on the InterMiles earned basis the
                                                                        spends & on the minimum award flight redemption of 5000
                                                                        InterMiles. <br> <span class="terms_conditions"
                                                                                            style="color: black; text-decoration: underline;"
                                                                                            id="tcLink">Terms & Conditions Apply</span>
                                                                    </span> </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 noBorder"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="features_desc">
                                            <div class="row">
                                                <div class="col-sm-2 pad0">
                                                    <div class="card_name">
                                                        <h2 id="cardName${list.cpNo}">${list.cardName}</h2>
                                                    </div>

                                                    <!--New adobe changes 16th 07 18 start-->
                                                    <div class="cardinfo hidden" id="cardinfo">${list.cardInfo}</div>
                                                    <input type="hidden" id="cardinfo" value="${cdList.cardInfo}">
                                                    <!--New adobe changes 16th 07 18 end-->

                                                    <input type="hidden" id="cpNo" class="cpNo_sort" value="${list.cpNo}">
                                                    <input type="hidden" id="bpNo" value="${list.bpNo}">
                                                    <input type="hidden" class="bankName_sort"
                                                           id="list_bankName${list.cpNo}" value="${list.bankName}">
                                                    <input type="hidden" id="lspbList" value="${list.lsbpList}">
                                                    <input type="hidden" class="lspbListSize" id="lspbListSize"
                                                           value="${fn:length(list.lsbpList)}"> <input
                                                           type="hidden" class="disp_lsbp_list"
                                                           value="${list.lyfList}">
                                                    <div class="card_img_holder">
                                                        <span class="card_img"> <c:choose>
                                                                <c:when test="${not empty list.cardImagePath}">
                                                                    <img src="${applicationURL}cards/${list.cardImagePath}"
                                                                         height="50" width="50" id="card_img${list.cpNo}" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <img
                                                                        src="${applicationURL}static/images/co-brand/noimage.png" />
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </span> <span class="card_value"> <label>Fees: </label> <span
                                                                id="feesId${list.cpNo}" class="fees_class"
                                                                data-fees="${list.fees}"> <%-- INR ${cdList.fees} --%>
                                                            </span>
                                                        </span>
                                                        <div class="details_link">
                                                            <a href="javascript:void(0);"
                                                               style="color: #28262c; text-decoration: underline;"
                                                               data-id="${list.cpNo}" data-name="${list.imageText}"
                                                               class="forMoreDetails"
                                                               id="CrdHP_${list.bankName}&${list.cardName}&${list.networkName}_details" data-utm_source="${utm_source}"
                                                               data-utm_medium="${utm_medium}" data-utm_campaign="${utm_campaign}" onclick="moreDetailsClick('${list.imageText}', '${list.imageText}')">For
                                                                more details ></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <c:forEach items="${list.featureList}" var="feature">
                                                    <div class="col-sm-2 webView">
                                                        <div class="list_cont">
                                                            <c:if test="${not empty feature}">
                                                                <div class="home_feature">${feature}</div>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </c:forEach>

                                                <div class="col-sm-2 ">
                                                    <div class="free_flights_div" data-target="spends_slider">
                                                        <div class="free_flights">
                                                            <p>You get</p>
                                                            <p class="ff" id="freeFlights${list.cpNo}">${list.freeTickets}</p>
                                                            <p>Free Flights</p>
                                                            <p class="gg" id="jpMiles${list.cpNo}">
                                                                on earning <br> ${list.jpMiles} InterMiles
                                                            </p>
                                                        </div>

                                                    </div>
                                                    <div class="lifestyle_benefits" data-toggle="modal"
                                                         data-target="#benefits" data-cpNo="${list.cpNo}">
                                                        <div class="benifits-img-holder">
                                                            <img
                                                                src="${applicationURL}static/images/co-brand/benifits_icon.png">
                                                        </div>
                                                        <label>Benefits</label>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-sm-2 ">
                                                    <div class="instant_approval">
                                                        <c:if test="${list.bpNo eq 1}">
                                                            <p class="textColor_orange">${list.approvalMessage}</p>
                                                            <!-- <p class="colorDarkBlue">
    </p> -->
                                                        </c:if>
                                                        &nbsp;
                                                        <div>

                                                            <c:choose>
                                                                <c:when test="${list.bpNo eq 1}">
                                                                    <c:choose>
                                                                        <c:when test="${(list.applyflag eq 0)}">
                                                                            <div class="instAppr_button_holder"
                                                                                 id="CrdRC_${list.bankName}&${list.cardName}_${list.networkName}_apply">
                                                                                <ul class="applyAmex" data-name="${list.imageText}" data-utm_source="${utm_source}"
                                                                                    data-utm_medium="${utm_medium}" data-utm_campaign="${utm_campaign}">
                                                                                    <li>
                                                                                        <div class="apply-inst apply_instantAppr" onclick="$.fn.ApplyButtonAmexClick('${list.cardName}', '${list.cardName}', '${list.bankName}')">
                                                                                            <span><img
                                                                                                    src="${applicationURL}static/images/co-brand/apply_icon_white.png"></span>
                                                                                            <span>Apply</span>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="apply_instantAppr2">
                                                                                            <span><img
                                                                                                    src="${applicationURL}static/images/co-brand/instant_apprvl-icon.png"></span>
                                                                                            <span>Instant Decision</span>
                                                                                        </div>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div class="apply_position margB5">

                                                                            </div> 

                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:when>

                                                                <c:when test="${(list.bpNo eq 2)}">
                                                                    <c:choose>
                                                                        <c:when test="${(list.applyflag eq 0)}">
                                                                            <div class="instAppr_button_holder " id="CrdRC_${list.bankName}&${list.cardName}_${list.networkName}_apply">

                                                                                <ul class="applyIcici" data-name="${list.imageText}" data-spMailingID="${spMailingID}" 
                                                                                    data-spUserID="${spUserID}" data-spJobID="${spJobID}" 
                                                                                    data-spReportId="${spReportId}">
                                                                                    <li>
                                                                                        <div class="apply-inst apply_instantAppr" onclick="$.fn.ApplyButtonAmexClick('${list.cardName}', '${list.cardName}', '${list.bankName}')">
                                                                                            <span><img src="${applicationURL}static/images/co-brand/apply_icon_white.png"></span>
                                                                                            <span>Apply</span>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="apply_instantAppr2">
                                                                                            <span><img src="${applicationURL}static/images/co-brand/instant_apprvl-icon.png"></span> 
                                                                                            <span>Instant Decision</span>
                                                                                        </div>
                                                                                    </li>
                                                                                </ul>

                                                                            </div>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div class="apply_position margB5">

                                                                            </div> 

                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:when>

                                                                <c:when test="${(list.bpNo eq 4)}">
                                                                    <c:choose>
                                                                        <c:when test="${(list.applyflag eq 0)}">
                                                                            <div class="apply_position margB5">
                                                                                <div class="apply_btn hdfc_asking_apply" data-cpName="${list.cardName}" data-bpNo="${list.bpNo}" data-NoNO="${list.networkName}" data-cpNo="${cdList.cpNo}"  data-toggle="modal" data-target="#askingUser" onclick="applyButtonAmexClickAdobe('${list.cardName}', '${list.cardName}', '${list.bankName}', 'Submit JPNumber', 'Page Refresh')">
                                                                                    <span>
                                                                                        <img src="${applicationURL}static/images/co-brand/apply_icon_white.png" class=""> 
                                                                                    </span>
                                                                                    <span>
                                                                                        Apply
                                                                                    </span>
                                                                                </div>
                                                                            </div> 

                                                                            <div class="modal fade hdfc_popup" id="askingUser" tabindex=-1 role="dialog">
                                                                                <div class="modal-dialog askingPopup-modal-dialog">
                                                                                    <!-- Modal content-->
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-body">
                                                                                            <div class="proceedPopup_close pull-right">
                                                                                                <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="">
                                                                                            </div>
                                                                                            <div class="askingPopup">
                                                                                                <div class="margT2 colorDarkBlue text-center">
                                                                                                    <h2 class="heading_txt">Are you an existing HDFC Bank Credit Card holder?</h2>
                                                                                                </div>
                                                                                                <div class="text-center margT11">
                                                                                                    <a class="hdfc_credit_card_yes" id="CrdRC_${cdList.cardName}_${cdList.networkName}_YES"  data-toggle="modal" data-target="#userWithCreditCard" onclick="$.fn.existingCreditCardHolderClick('Yes-Existing Card Holder')">
                                                                                                        <h1>YES</h1>
                                                                                                    </a>
                                                                                                    <a class="non_amex_apply" id="CrdRC_${cdList.cardName}_${cdList.networkName}_NO" data-cpNo="${list.cpNo}" data-bpNo="${list.bpNo}" data-toggle="modal" data-target="#abtYourself" onclick="noExistingCardHolderClick('No-Existing Card Holder', 'Submit JPNumber')">
                                                                                                        <h1>No</h1>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="modal fade hdfc_popup" id="userWithCreditCard" tabindex=-1 role="dialog">
                                                                                <div class="modal-dialog askingPopup-modal-dialog">
                                                                                    <!-- Modal content-->
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-body">
                                                                                            <div class="proceedPopup_close pull-right">
                                                                                                <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="">
                                                                                            </div>
                                                                                            <div class="userWithCreditCardPopup">

                                                                                                <p>${bpupgrademessage}
                                                                                                </p>

                                                                                                <div class="text-center midareabtn">
                                                                                                    <a class="viewCobranCards" id="CrdRC_${cdList.cardName}_${cdList.networkName}_View-Others" href="${applicationURL}cardswidget?bank=1,2,3" onclick="upgradeOtherCardsClickforYes('View Other-Existing Card Holder')"> View other Jet Airways Cobranded Cards</a>
                                                                                                    <a class="upgradeNow" id="CrdRC_${cdList.cardName}_${cdList.networkName}_Upgrade" data-cpNo="${list.cpNo}" data-bpNo="${list.bpNo}" data-toggle="modal" onclick="upgradeOtherCardsClick('Upgrade-Existing Card Holder', 'Submit JPNumber')"> 
                                                                                                        <!--data-target="#abtYourself"-->
                                                                                                        Upgrade Now
                                                                                                    </a>
                                       <!--                                                            <a class="upgradeNow" href="${bpupgradeurl}" target="#_">
                                                                                                        Upgrade Now</a>-->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div class="apply_position margB5">

                                                                            </div> 

                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:when>

                                                                <c:otherwise>
                                                                    <c:choose>
                                                                        <c:when test="${(list.applyflag eq 0)}">
                                                                            <div class="apply_position">
                                                                                <div class="apply_btn non_amex_apply" id="CrdRC_${list.bankName}&${list.cardName}_${list.networkName}_apply" data-cpNo="${list.cpNo}" data-bpNo="${list.bpNo}" data-toggle="modal" data-target="#abtYourself" onclick="applyButtonAmexClickAdobe('${list.cardName}', '${list.cardName}', '${list.bankName}', 'Submit JPNumber', 'Page Refresh')">
                                                                                    <span> <img
                                                                                            src="${applicationURL}static/images/co-brand/apply_icon_white.png"
                                                                                            class="">
                                                                                    </span> <span> Apply </span>
                                                                                </div>
                                                                            </div>
                                                                        </c:when>
                                                                    </c:choose>
                                                                </c:otherwise>
                                                            </c:choose>
                                                            <div class="row"></div>
                                                        </div>
                                                        <c:if test="${fn:length(recommendList) gt 1}">
                                                            <div class="select_compare">
                                                                <input type="checkbox" class="compareCards" name='check'
                                                                       value="${list.cpNo}" id="compare_cards"> <span>Select
                                                                    to compare</span>
                                                            </div>
                                                        </c:if>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <div class="btn_holder">
                    <input class="apply_btn showAll hidden" value="Show All">
                </div>
                <div class="no_cards hidden">
                    Sorry, there is no card matching your selected criteria.
                </div>
                <div class="terms_conditions">
                    <a href="https://www.jetprivilege.com/terms-and-conditions/cards" target="_blank" style="color: black; text-decoration: underline;">Terms & Conditions Apply</a>
                </div>
        </section>
    </section>

    <div class="filter_img hidden-lg"><img src="${applicationURL}static/images/co-brand/filter_img.png" alt=""></div>

    <section class="compare_card_holder hidden">
        <section class="container">
            <div class="compare_card">
                <ul>
                    <li><div class="card_holder"></div></li>
                    <li><div class="card_holder"></div></li>
                    <li><div class="card_holder"></div></li>
                    <li>
                        <div class="compare_btn_holder">
                            <button type="button" class="CrdRC_btn_compare disable_comp_btn" data-utm_source="${utm_source}"
                                    data-utm_medium="${utm_medium}" data-utm_campaign="${utm_campaign}">Compare Cards</button>
                        </div>
                    </li>
                </ul>

            </div>
        </section>
    </section>
</form:form>
<script>
    
    //Adobe code start(phase 6)
    var assistMeFlag = false;
    var name = "";
    var mobile = "";
    var email = "";
    var assistError="";
    var lastAccessedField="";
    
    name = $("#socialName").val();
    mobile = $("#socialPhone").val();
    email = $("#socialEmail").val();
    //Adobe code end(phase 6)
    
    window.onpageshow = function (event) {
        if (event.persisted) {
            window.location.reload()
        }
    };
    var spend = '${spendValue}';
    spend = spend.replace(/\,/g, '');
    spend = parseInt(spend, 10);
    var spend_per_month = spend;

    $(document).ready(function () {
        console.log("----", '${callMeFlag}')
        var callmeflag = '${callMeFlag}';
        console.log("callmeflag===>", callmeflag);
        if (callmeflag != "") {
            if (callmeflag == 1) {
                $("#social-profiles").removeClass('hidden');

            } else {
                $("#social-profiles").addClass('hidden');

            }
        } else {
            $("#social-profiles").addClass('hidden');
        }
        $("#enrollPhone,#jpNumber,#socialPhone").on("keydown, keypress", function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;
            }

        });
        $("#jpNumber").on('keyup', function (evt) {
            console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 9) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $('#socialName').keydown(function (event) {
            var charCode = event.keyCode;
            if ((charCode > 64 && charCode < 91) || charCode == 8 || charCode == 9 || charCode == 32) {
                return true;
            } else {
                return false;
            }
        });
        $("#enrollPhone,#socialPhone").on('keyup', function (evt) {
            console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 10) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        var result_Value, result_Value_year;
//new implementation
        function handleRange(valueoftext)
        {
//        console.log("legth",valueoftext)
            if (valueoftext < 5000 || valueoftext > 1500000) {
//             console.log("at keyup if");
//                if ($(".slider_error_div").is(':visible') == false) {
                $(".slider_error_div").show().html("Please enter spends value within the range.");
//                }
                return false;
            } else {
                $(".slider_error_div").hide().html("");
                return true;
            }

        }
        function handleRangeModal(legth)
        {
//        console.log("legth",legth)
            if (legth < 5000 || legth > 1500000) {
//             console.log("at keyup if");
//             if($(".slider_modal_div_error").is(':visible') == false){
                $('.slider_modal_div_error').css('display', 'block');
                $(".slider_modal_div_error").html("Please enter spends value within the range.");
//        }
                return false;
            } else {
                $('.slider_modal_div_error').css('display', 'none');
                $(".slider_modal_div_error").html("");

                return true;
            }

        }
        $('#unranged-value1').on('keyup', function () {
//            console.log("at keyup");
            var valSpend = $("#unranged-value1").val();
            valSpend = valSpend.replace(/\,/g, '');
            return handleRange(valSpend);
//        if (valSpend < 5000 || valSpend > 1500000) {
////             console.log("at keyup check");
//            $(".slider_error_div").show().html("Please enter spends value within the range.");
//            return false;
//        }else{
//            $(".slider_error_div").hide().html("");
//            return true;
//        }

        });
        $('#unranged-value2').on('keyup', function () {
//            console.log("at keyup");
            var valSpend = $("#unranged-value2").val();
            valSpend = valSpend.replace(/\,/g, '');
            return handleRangeModal(valSpend);
//        if (valSpend < 5000 || valSpend > 1500000) {
////             console.log("at keyup check");
//            $(".slider_modal_div_error").show().html("Please enter spends value within the range.");
//            return false;
//        }else{
//            $(".slider_modal_div_error").hide().html("");
//            return true;
//        }

        });
        slider();
        slidermodal();
        function slider() {
            $(".range-example-5").asRange({
                step: 1,
                min: 5000,
                max: 500000,
                format: function (value) {
                    getValue(value);
                    return result_Value;
                },
                onChange: function (valueslider) {
                    var value = $('.range-example-5').asRange('get');
//                    console.log( "--value", value);
//                             if(temp != '' && temp > 0){
//                                 console.log("temp",temp);
                    var a = document.getElementById("unranged-value1");
                    getValue(value);
                    var valueoftext = $("#unranged-value1").val();
                    console.log("valueoftext", valueoftext);
                    valueoftext = valueoftext.replace(/\,/g, '');
                    handleRange(valueoftext);

                    a.value = result_Value;
//                    console.log("result_Value_year1", result_Value_year)
                    $("#yearly_bill").html(result_Value_year);
//                    temp = 0;
//                             }else{
//                                  var a = document.getElementById("unranged-value1");
//                                  if(temp2!='' && temp2 > 0){
//                                      console.log("at temp2")
//                                        getValue(temp2);
//                                         a.value = result_Value;
//                    console.log("result_Value_year", result_Value_year)
//                    $("#yearly_bill").html(result_Value_year);
//                    temp2=0;
//                                  }else{
//                                       console.log("no temp")
//                                        getValue(value);
//                                         a.value = result_Value;
//                    console.log("result_Value_year", result_Value_year)
//                    $("#yearly_bill").html(result_Value_year);
//                                  }
//                  
//                   
//                             }


                    if (value > 5000) {
//                        console.log("----------1--->", value);
                        $(".range-example-modal").asRange('set', value);
                    }


                }
            });
        }
        $('.range-example-5').asRange('set', spend_per_month);
//        console.log("at on change", $("#unranged-value1").val());
//        console.log("--->", $('.range-example-5').asRange('get'));
        getValue(spend_per_month);
        $("#unranged-value1").val(result_Value);
        handleRange(result_Value);
        $("#yearly_bill").html(result_Value_year);
        getValue($('.range-example-modal').asRange('get'));
        $("#unranged-value2").val(result_Value);
        $("#yearly_bill1").html(result_Value_year);

        function slidermodal() {
            $(".range-example-modal").asRange({

                min: 5000,
                max: 500000,
                format: function (value) {
                    getValue(value);
                    return result_Value;
                },
                onChange: function (valueslider) {
                    var value = $('.range-example-modal').asRange('get');
//                    console.log("--value", value);
//                       if(temp3 != '' && temp3 > 0){
//                            console.log("temp",temp3);
                    var a = document.getElementById("unranged-value2");

                    getValue(value);

                    a.value = result_Value;
                    var valueoftext = $("#unranged-value2").val();
                    valueoftext = valueoftext.replace(/\,/g, '');
                    handleRangeModal(valueoftext);

//                    console.log("at on change", $("#unranged-value2").val());
//                    console.log("result_Value_year", result_Value_year)
                    $("#yearly_bill1").html(result_Value_year);
//                    temp3=0;
//                }else{
//                              var a = document.getElementById("unranged-value2");
//                               if(temp4!='' && temp4 > 0){
//                                   getValue(temp4);
//                    a.value = result_Value;
//                    console.log("at on change", $("#unranged-value2").val());
//                    console.log("result_Value_year", result_Value_year)
//                    $("#yearly_bill1").html(result_Value_year);
//                    temp4=0;
//                               }else{
//                                  getValue(value);
//                    a.value = result_Value;
//                    console.log("at on change", $("#unranged-value2").val());
//                    console.log("result_Value_year", result_Value_year)
//                    $("#yearly_bill1").html(result_Value_year); 
//                               }
//                    
//                    
//                       }
//                 
                    if (value > 5000) {
//                        console.log("----------2--->", value);
                        $(".range-example-5").asRange('set', value);
                    }
                }
            });
        }
        $("#unranged-value1").keyup(function (event) {
            var valSpend = $("#unranged-value1").val();
            var valSpendNum = '';
            valSpendNum = valSpend.replace(/\,/g, '');
            valSpendNum = parseInt(valSpendNum, 10);
            if (valSpendNum >= 0) {  //&& valSpendNum <= 1500000

                // skip for arrow keys
                if (event.which == 37 || event.which == 39)
                    return;
                // format number
                $(this).val(function (index, value) {

                    if (valSpend < 5000 || valSpend > 1500000) {
                        $(".range-example-5").asRange('val', 5000);

                        getValue(5000);
                        $("#yearly_bill").html(result_Value_year);
//                                (valSpendNum > 5000) || (valSpendNum < 1500000)
//                               console.log("***********")
//                           $(".range-example-5").asRange('val', valSpendNum);
//                          
//                                getValue(valSpendNum);
//                                 $("#yearly_bill").html(result_Value_year);
                    } else {
//                               console.log("^^^^^^^^^^^^")
                        $(".range-example-5").asRange('val', valSpendNum);
//                          
                        getValue(valSpendNum);
                        $("#yearly_bill").html(result_Value_year);
//                               $(".range-example-5").asRange('val', 5000);
//                          
//                                getValue(5000);
//                                 $("#yearly_bill").html(result_Value_year);
                    }
//                           $(".range-example-5").asRange('val', valSpendNum);
                    $("#unranged-value1").focus();
//                              if(valSpendNum != ""){
//                                getValue(valSpendNum);
//                                 $("#yearly_bill").html(result_Value_year);
//                           }
                    var x = value.replace(/,/g, "");
                    var lastThree = x.substring(x.length - 3);
                    var otherNumbers = x.substring(0, x.length - 3);
                    if (otherNumbers != '')
                        lastThree = ',' + lastThree;
                    var res = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                    return res;

                });
            }
        });

//$("#unranged-value1").focusout(function(){
//    var value= $("#unranged-value1").val();
//      value = value.replace(/\,/g, '');
//    if(value < 5000){
//        $("#unranged-value1").val("5,000");
//         getValue(5000);
//      
//        $("#yearly_bill").html(result_Value_year);
//          $(".range-example-5").asRange('val', 5000);
//    }
//})
//$("#unranged-value2").focusout(function(){
//    var value= $("#unranged-value2").val();
//     value = value.replace(/\,/g, '');
//    if(value < 5000){
//        $("#unranged-value2").val("5,000");
//          getValue(5000);
//           $("#yearly_bill1").html(result_Value_year);
//          $(".range-example-modal").asRange('val', 5000);
//    }
//})

        /* Capturing entered spends value in slider */

        /* Decimal and Alphabets are not allowing */
        $("#unranged-value1").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            // Allow: home, end, left, right, down, up
                                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || (e.keyCode == 110) || (e.keyCode == 190)) {
                        e.preventDefault();
                    }
                });

        //end
        $("#unranged-value2").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            // Allow: home, end, left, right, down, up
                                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || (e.keyCode == 110) || (e.keyCode == 190)) {
                        e.preventDefault();
                    }
                });
        $("#unranged-value2").keyup(function (event) {
            var valSpend = $("#unranged-value2").val();
            var valSpendNum = '';
            valSpendNum = valSpend.replace(/\,/g, '');
            valSpendNum = parseInt(valSpendNum, 10);
            if (valSpendNum >= 0) {  //&& valSpendNum <= 1500000

                // skip for arrow keys
                if (event.which == 37 || event.which == 39)
                    return;
                // format number
                $(this).val(function (index, value) {
                    if (valSpend < 5000 || valSpend > 1500000) {
                        $(".range-example-modal").asRange('val', 5000);

                        getValue(5000);
                        $("#yearly_bill1").html(result_Value_year);
//                                (valSpendNum > 5000) || (valSpendNum < 1500000)
//                               console.log("***********")
//                           $(".range-example-5").asRange('val', valSpendNum);
//                          
//                                getValue(valSpendNum);
//                                 $("#yearly_bill").html(result_Value_year);
                    } else {
//                               console.log("^^^^^^^^^^^^")
                        $(".range-example-modal").asRange('val', valSpendNum);
//                          
                        getValue(valSpendNum);
                        $("#yearly_bill1").html(result_Value_year);
//                               $(".range-example-5").asRange('val', 5000);
//                          
//                                getValue(5000);
//                                 $("#yearly_bill").html(result_Value_year);
                    }
//                           $(".range-example-modal").asRange('val', valSpendNum);
                    $("#unranged-value2").focus();
//                              if(valSpendNum != ""){
//                                getValue(valSpendNum);
//                                 $("#yearly_bill1").html(result_Value_year);
//                           }
                    var x = value.replace(/,/g, "");
                    var lastThree = x.substring(x.length - 3);
                    var otherNumbers = x.substring(0, x.length - 3);
                    if (otherNumbers != '')
                        lastThree = ',' + lastThree;
                    var res = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                    return res;

                });
            }
        });
//        var temp, temp2=0, temp3, temp4;
//        $("#unranged-value1").on('change', function () {
//            var rangedvalue = $("#unranged-value1").val();
//            if(rangedvalue > 500000){
//                console.log("1")
//                temp=rangedvalue;
//                 $(".range-example-5").asRange('set', rangedvalue);
//            }else{
//                temp=0;
//                temp2=rangedvalue;
//                 console.log("2")
//                $(".range-example-5").asRange('set', rangedvalue);
//            }
////            $(".range-example-5").asRange('set', rangedvalue);
//        });
//        $("#unranged-value2").on('change', function () {
//            var rangedvalue = $("#unranged-value2").val();
//             if(rangedvalue > 500000){
//                console.log("3")
//                temp3=rangedvalue;
//                 $(".range-example-modal").asRange('set', rangedvalue);
//            }else{
//                temp3=0;
//                temp4=rangedvalue;
//                 console.log("4")
//                 $(".range-example-modal").asRange('set', rangedvalue);
//            }
//           
//        });
        $('#unranged-value1, #unranged-value2').on("keyup", function (evt) {

            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;

            } else
                return true;
        });

        function getValue(param) {
            var yearly = (param * 12).toString();
            var monthly = param.toString();
            var lastThree = monthly.substring((monthly.length) - 3);
            var otherNumbers = monthly.substring(0, monthly.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            result_Value = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            lastThree = yearly.substring((yearly.length) - 3);
            otherNumbers = yearly.substring(0, yearly.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            result_Value_year = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        }
//end

        $(document).scrollTop(700);
        $('#filterareadropdown').prop("disabled", true);
        $('#filterareadropdownhide').prop("disabled", true);

        var tierExist = false;

        var jp_num_ppp1 = '${sessionScope.loggedInUser}';
        var jp_num_ppp = '${sessionScope.loggedInUser}';
        if (jp_num_ppp1 != "") {
//            console.log('jp_num_ppp1'+jp_num_ppp1);
//            $('#jpNumber').val(jp_num_ppp1);
//            tierExist = true;
        }

        if (jp_num_ppp != "") {
//            console.log('test1'+jp_num_ppp);
            $('#jpNumber').val(jp_num_ppp);
            tierExist = true;
        }
        var checkupgrade = false;
        defaultSorting();


        $('#filterareadropdown').on('change', function () {
            if ($(this).find(":selected").text() == "Other Cards") {
                window.open(
                        'https://www.jetprivilege.com/earn-jpmiles/programme-partners/cobrand-cards ',
                        '_blank'
                        );

                $("#filterareadropdownhide").hide();
            } else {
                $("#filterareadropdownhide").show();
            }
        });


        /* onPage load sorting byAirticket */

        function defaultSorting() {
            var $divs = $(".recommendedCards");
            var air_ticket_dsc = $divs.sort(function (a, b) {
                var ffA = parseInt($(a).find(".ff").html());
                var ffB = parseInt($(b).find(".ff").html());
                if (ffA > ffB)
                    return -1
                if (ffA < ffB)
                    return 1
                else {
                    var ggA = $(a).find(".gg").html();
                    var ggB = $(b).find(".gg").html();
                    var res = [];
                    res = ggA.split(' ');
                    ggA = parseInt(res[3]);
                    res = ggB.split(' ');
                    ggB = parseInt(res[3]);
                    if (ggA > ggB)
                        return -1
                    if (ggA < ggB)
                        return 1
                    else
                        return parseInt($(b).find(".disp_lsbp_list").val()) - parseInt($(a).find(".disp_lsbp_list").val());
                }

            });
            $("#recommendedCards_div").html(air_ticket_dsc);
        }

        /* onPage load sorting byAirticket */

        /* Recommended page when opens should take to gray filter */

        if (navigator.userAgent.match(/(iPod|iPhone|Android)/)) {
            window.scrollTo(200, 950) // first value for left offset, second value for top offset
        } else if (navigator.userAgent.match(/(iPad)/)) {
            window.scrollTo(200, 650) // first value for left offset, second value for top offset
        } else {
            $('html,body').animate({
                scrollTop: $(".filter_div").offset().top - 320
            }, 10);
        }

        /* Recommended cards - counter*/
        var countCards = 0;
        $('.rec_features').each(function () {
            countCards = countCards + 1;
//							console.log(countCards)
            if (countCards > 3) {
                $(this).addClass("hidden");
            }
            if (countCards > 3) {
                $('.showAll').removeClass('hidden');
            }
        });
        /* Recommended cards - counter*/

    <c:forEach items="${recommendList}" var="listItem">
        $("#feesId" + "<c:out value="${listItem.cpNo}" />").html("INR " + formatNumber("<c:out value="${listItem.fees}" />"));
    </c:forEach>

        function formatNumber(x) {
            x = x.toString();
            var afterPoint = '';
            if (x.indexOf('.') > 0)
                afterPoint = x.substring(x.indexOf('.'),
                        x.length);
            if (afterPoint == 0)
                afterPoint = '';
            x = Math.floor(x);
            x = x.toString();
            var lastThree = x.substring(x.length - 3);
            var otherNumbers = x.substring(0, x.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(
                    /\B(?=(\d{2})+(?!\d))/g, ",")
                    + lastThree + afterPoint;
            return res;

        }

        //var originalState = $("#recommendedCards_div").clone(); 

        var arr = [];
        var checkedBankId = [];
        var bankid = [];
        var lyfId = [];
        var checkedlyfId = [];
        var mob_checkedBankId = [];
        var mob_bankid = [];
        var mob_lyfId = [];
        var mob_checkedlyfId = [];
        var fees_range = 10000;
        var islyfunchecked = false;

        $(".bankNum").each(function () {
            bankid.push($(this).val());
            checkedBankId.push($(this).val());
        });
        $(".lsbpNum").each(function () {
            lyfId.push($(this).val());
            checkedlyfId.push($(this).val());
        });

        var totalBank = bankid.length;
        var totallyfId = lyfId.length;

        $(".mob_bankNum").each(function () {
            mob_bankid.push($(this).val());
            mob_checkedBankId.push($(this).val());
        });
        $(".mob_lsbpNum").each(function () {
            mob_lyfId.push($(this).val());
            mob_checkedlyfId.push($(this).val());
        });
        var mob_totalBank = mob_bankid.length;
        var mob_totallyfId = mob_lyfId.length;

        /*Mobile Filter functionality starts*/
        $(".mob_bankNum").on("change", function () {
            mob_checkedBankId = [];
            $(".mob_bankNum").each(function () {
                if ($(this).is(':checked')) {
                    mob_checkedBankId.push($(this).val());
                } else {
                    mob_checkedBankId.remove($(this).val());
                }
            });
        });

        $(".mob_lsbpNum").on("change", function () {
            mob_checkedlyfId = [];
            $(".mob_lsbpNum").each(function () {
                if ($(this).is(':checked')) {
                    mob_checkedlyfId.push($(this).val());
                } else {
                    mob_checkedlyfId.remove($(this).val());
                }
            });
        });

        $('#mob_apply-btn').on('click', function () {

            $(".showAll").trigger('click');

            if (mob_checkedBankId.length == 0)
                mob_checkedBankId = mob_bankid;

            if (mob_checkedlyfId.length == 0) {
                mob_checkedlyfId = mob_lyfId;
                islyfunchecked = true;
            }

            $("div.features_desc").each(function () {
                var bpId = $(this).find('#bpNo').val();
                var cpNo = $(this).find('#cpNo').val();
                var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                fees_range = $(".slider_div .rangeslider__handle").html();
                if (jQuery.type(fees_range) === "undefined"
                        || fees_range == '') {
                    fees_range = 10000;
                } else {
                    fees_range = fees_range.replace(/<\/?span[^>]*>/g, "");
                    fees_range = fees_range.replace(/\,/g, '');
                }
                var lsbpList = $(this).find('#lspbList').val();
                if ($.inArray(bpId, mob_checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {
                    var flag = false;
                    $.each(mob_checkedlyfId, function (k, v) {
                        if ($.inArray(v, lsbpList) != -1) {
                            flag = true;
                        }
                    });
                    if (flag) {
                        $('#card_row' + cpNo).show();
                    } else {
                        $('#card_row' + cpNo).hide();
                    }

                } else {
                    $('#card_row' + cpNo).hide();
                }
            });
            $('#filterPopup').modal('hide');
            if (!islyfunchecked) {
                if (!($('.features').hasClass('hidden'))) {
                    var $divs = $(".recommendedCards");
                    var sort_cards = $divs.sort(function (a, b) {
                        var ffA = parseInt($(a).find(".ff").html());
                        var ffB = parseInt($(b).find(".ff").html());
                        if (ffA > ffB)
                            return -1
                        if (ffA < ffB)
                            return 1
                        else {
                            var ggA = $(a).find(".gg").html();
                            var ggB = $(b).find(".gg").html();
                            var res = [];
                            res = ggA.split(' ');
                            ggA = parseInt(res[3]);
                            res = ggB.split(' ');
                            ggB = parseInt(res[3]);
                            if (ggA > ggB)
                                return -1
                            if (ggA < ggB)
                                return 1
                            else {
                                var countA = 0;
                                var countB = 0;
                                if (mob_checkedlyfId.length == 1) {
                                    var bankA = $(a).find(".bankName_sort").val();
                                    var bankB = $(b).find(".bankName_sort").val();
                                    if (bankA > bankB)
                                        return 1
                                    if (bankA < bankB)
                                        return -1
                                } else {
                                    var lsbpListA = $(a).find('#lspbList').val();
                                    var lsbpListB = $(b).find('#lspbList').val();
                                    $.each(mob_checkedlyfId, function (k, v) {
                                        if ($.inArray(v, lsbpListA) != -1) {
                                            countA++;
                                        }
                                    });
                                    $.each(mob_checkedlyfId, function (k, v) {
                                        if ($.inArray(v, lsbpListB) != -1) {
                                            countB++;
                                        }
                                    });
                                    if (countA > countB)
                                        return -1
                                    if (countA < countB)
                                        return 1
                                    else {
                                        var bankA = $(a).find(".bankName_sort").val();
                                        var bankB = $(b).find(".bankName_sort").val();
                                        if (bankA > bankB)
                                            return 1
                                        if (bankA < bankB)
                                            return -1
                                    }
                                }
                            }
                        }
                    });
                    $("#recommendedCards_div").html(sort_cards);
                }
            } else {
                defaultSorting();
            }
            if ($('div.features_desc:visible').length > 0) {
                $('.no_cards').addClass(
                        'hidden');
                $('.recCards_title')
                        .removeClass('hidden');
            } else {
                $('.no_cards').removeClass(
                        'hidden');
                $('.recCards_title').addClass(
                        'hidden');
            }
        });

        $('#mob_clearFilters').on('click', function () {
            $('.life_benefits_holder li input:checked').next().children().toggle();
            $('.mob_bankNum').attr('checked', false);
            $('.mob_lsbpNum').attr('checked', false);
            $('.no_cards').addClass('hidden');
            $('.recCards_title').removeClass('hidden');
            checkedBankId = [];
            bankid = [];
            lyfId = [];
            checkedlyfId = [];
            fees_range = 10000;
            joiningFees = 500;

            $(".mob_bankNum").each(function () {
                mob_bankid.push($(this).val());
                mob_checkedBankId.push($(this).val());
            });
            $(".mob_lsbpNum").each(function () {
                mob_lyfId.push($(this).val());
                mob_checkedlyfId.push($(this).val());
            });

    <c:forEach items="${recommendList}" var="listItem">
            arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
            $.each(arr, function (i, val) {
                $('#card_row' + val).show();
            });
            defaultSorting();
            $('#filterPopup').modal('hide');
        });
        /*Mobile Filter functionality ends*/

        /*Web Filter functionality strats*/

        $(".bankNum").on("change", function () {
            checkedBankId = [];
            $(".showAll").trigger('click');
            $(".bankNum").each(function () {
                if ($(this).is(':checked')) {
                    checkedBankId.push($(this).val());
                    //Adobe code starts here
                    if (!isInArray(bankNamesAdobe, $(this).parent().find('span:first').text()))
                    {
                        bankNamesAdobe.push($(this).parent().find('span:first').text())
                    }
                    //Adobe code ends here

                } else {
                    checkedBankId.remove($(this).val());
                    //Adobe code starts here
                    bankNamesAdobe.remove($(this).parent().find('span:first').text())
                    //Adobe code ends here
                }
            });

            //Adobe code starts here
            submitFilterAdobe();
            //Adobe code ends here

            if (checkedBankId.length == 0)
                checkedBankId = bankid;

            $("div.features_desc").each(function () {
                var bpId = $(this).find('#bpNo').val();
                var cpNo = $(this).find('#cpNo').val();
                var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                var lsbpList = $(this).find('#lspbList').val();
                if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {
                    var flag = false;
                    $.each(checkedlyfId, function (k, v) {
                        if ($.inArray(v, lsbpList) != -1) {
                            flag = true;
                        }
                    });
                    if (flag) {
                        $('#card_row' + cpNo).show();
                    } else {
                        $('#card_row' + cpNo).hide();
                    }

                } else {
                    $('#card_row' + cpNo).hide();
                }
            });
            if ($('div.features_desc:visible').length > 0) {
                $('.no_cards').addClass('hidden');
                $('.recCards_title').removeClass('hidden');
            } else {
                $('.no_cards').removeClass('hidden');
                $('.recCards_title').addClass('hidden');
            }
        });

        $("#filter_jf").on('click', function (e) {
            $(".showAll").trigger('click');
            $("div.features_desc").each(function () {
                var cpNo = $(this).find('#cpNo').val();
                var bpId = $(this).find('#bpNo').val();
                var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                fees_range = $(".slider_div .rangeslider__handle").html().replace(/<\/?span[^>]*>/g, "");
                fees_range = fees_range.replace(/\,/g, '');
//                console.log('fees'+ fees);
//                console.log('fees_range'+ fees_range);

                var lsbpList = $(this).find('#lspbList').val();
                if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {

                    var flag = false;
                    $.each(checkedlyfId, function (k, v) {
                        if ($.inArray(v, lsbpList) != -1) {
                            flag = true;
                        }
                    });
                    if (flag) {
                        $('#card_row' + cpNo).show();
                    } else {
                        $('#card_row' + cpNo).hide();
                    }

                } else {
                    $('#card_row' + cpNo).hide();
                }

            });

            $(this).parents('.parent_div')
                    .addClass('hidden');
            if ($('div.features_desc:visible').length > 0) {
                $('.no_cards').addClass('hidden');
                $('.recCards_title').removeClass('hidden');
            } else {
                $('.no_cards').removeClass('hidden');
                $('.recCards_title').addClass('hidden');
            }
            e.stopImmediatePropagation();

        });

        $(".lsbpNum").on("change", function () {
            checkedlyfId = [];
            $(".showAll").trigger('click');
            $(".lsbpNum").each(function () {
                if ($(this).is(':checked')) {
                    checkedlyfId.push($(this).val());
                    //Adobe code starts here
                    if (!isInArray(lifestyleBenefitsAdobe, $(this).parent().find('span:first').text()))
                    {
                        lifestyleBenefitsAdobe.push($(this).parent().find('span:first').text());
                    }
                    //Adobe code ends here       
                } else {
                    checkedlyfId.remove($(this).val());
                    //Adobe code starts here
                    lifestyleBenefitsAdobe.remove($(this).parent().find('span:first').text());
                    //Adobe code ends here   
                }
            });

            //Adobe code starts here
            if (lifestyleBenefitsAdobe.length != 0)
            {
                submitFilterAdobe();
            }
            //Adobe code ends here



            if (checkedlyfId.length == 0) {
                checkedlyfId = lyfId;
                islyfunchecked = true;
            }

            $("div.features_desc").each(function () {
                var bpId = $(this).find('#bpNo').val();
                var cpNo = $(this).find('#cpNo').val();
                var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                var lsbpList = $(this).find('#lspbList').val();
                if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {
                    var flag = false;
                    $.each(checkedlyfId, function (k, v) {
                        if ($.inArray(v, lsbpList) != -1) {
                            flag = true;
                        }
                    });
                    if (flag) {
                        $('#card_row' + cpNo).show();
                    } else {
                        $('#card_row' + cpNo).hide();
                    }

                } else {
                    $('#card_row' + cpNo).hide();
                }
            });
            if (!islyfunchecked) {
                if (!($('.features')
                        .hasClass('hidden'))) {
                    var $divs = $(".recommendedCards");
                    var sort_cards = $divs.sort(function (a, b) {
                        var ffA = parseInt($(a).find(".ff").html());
                        var ffB = parseInt($(b).find(".ff").html());
                        if (ffA > ffB)
                            return -1
                        if (ffA < ffB)
                            return 1
                        else {
                            var ggA = $(a).find(".gg").html();
                            var ggB = $(b).find(".gg").html();
                            var res = [];
                            res = ggA.split(' ');
                            ggA = parseInt(res[3]);
                            res = ggB.split(' ');
                            ggB = parseInt(res[3]);
                            if (ggA > ggB)
                                return -1
                            if (ggA < ggB)
                                return 1
                            else {
                                var countA = 0;
                                var countB = 0;
                                if (checkedlyfId.length == 1) {
                                    var bankA = $(a).find(".bankName_sort").val();
                                    var bankB = $(b).find(".bankName_sort").val();
                                    if (bankA > bankB)
                                        return 1
                                    if (bankA < bankB)
                                        return -1
                                } else {
                                    var lsbpListA = $(a).find('#lspbList').val();
                                    var lsbpListB = $(b).find('#lspbList').val();
                                    $.each(checkedlyfId, function (k, v) {
                                        if ($.inArray(v, lsbpListA) != -1) {
                                            countA++;
                                        }
                                    });
                                    $.each(checkedlyfId, function (k, v) {
                                        if ($.inArray(v, lsbpListB) != -1) {
                                            countB++;
                                        }
                                    });
                                    if (countA > countB)
                                        return -1
                                    if (countA < countB)
                                        return 1
                                    else {
                                        var bankA = $(a).find(".bankName_sort").val();
                                        var bankB = $(b).find(".bankName_sort").val();
                                        if (bankA > bankB)
                                            return 1
                                        if (bankA < bankB)
                                            return -1
                                    }
                                }
                            }
                        }

                    });
                    $("#recommendedCards_div").html(sort_cards);
                }
            } else {
                defaultSorting();
            }
            if ($('div.features_desc:visible').length > 0) {
                $('.no_cards').addClass('hidden');
                $('.recCards_title').removeClass('hidden');
            } else {
                $('.no_cards').removeClass('hidden');
                $('.recCards_title').addClass('hidden');
            }
        });

        $("#clear_fliter").on('click', function () {
            $('.life_benefits_holder li input:checked').next().children().toggle();
            $('.bankNum').attr('checked', false);
            $('.lsbpNum').attr('checked', false);
            $('.no_cards').addClass('hidden');
            $('.recCards_title').removeClass('hidden');

            checkedBankId = [];
            bankid = [];
            lyfId = [];
            checkedlyfId = [];
            fees_range = 10000;
            joiningFees = 500;

            $(".bankNum").each(function () {
                bankid.push($(this).val());
                checkedBankId.push($(this).val());
            });
            $(".lsbpNum").each(function () {
                lyfId.push($(this).val());
                checkedlyfId.push($(this).val());
            });

    <c:forEach items="${finalList}" var="listItem">
            arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
            $.each(arr, function (i, val) {
                $('#card_row' + val).show();
            });
            defaultSorting();
        });

        /*Web Filter functionality ends*/

        /*Sort functionality starts*/
        $(".sort_list").on("click", ".sorting_ascending, .mob_sorting_ascending", function (event) {
            var divs = $(".recommendedCards");
            var ascSort = divs.sort(function (a, b) {
                if (!($(a).find('.recommendedCards').hasClass('hidden'))) {
                    var bankA = $(a).find(".bankName_sort").val();
                    var bankB = $(b).find(".bankName_sort").val();
                    if (bankA > bankB)
                        return 1
                    if (bankA < bankB)
                        return -1
                }
            });
            $("#recommendedCards_div").html(ascSort);
            $('#filterPopup').modal('hide');

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".sorting_descending, .mob_sorting_descending", function (event) {
            var divs = $(".recommendedCards");
            var descSort = divs.sort(function (a, b) {
                if (!($(a).find('.recommendedCards').hasClass('hidden'))) {
                    var bankA = $(a).find(".bankName_sort").val();
                    var bankB = $(b).find(".bankName_sort").val();
                    if (bankA > bankB)
                        return -1
                    if (bankA < bankB)
                        return 1
                }
            });
            $("#recommendedCards_div").html(descSort);
            $('#filterPopup').modal('hide');

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".sort_by_fee_asc, .mob_sort_by_fee_asc", function (event) {
            var divs = $(".recommendedCards");
            var fees_sort_asc = divs.sort(function (a, b) {
                if (!($(a).find('.recommendedCards').hasClass('hidden'))) {
                    return $(a).find(".fees_class").attr('data-fees') - $(b).find(".fees_class").attr('data-fees');
                }
            });
            $("#recommendedCards_div").html(
                    fees_sort_asc);
            $('#filterPopup').modal('hide');

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".sort_by_fee_dsc, .mob_sort_by_fee_dsc", function (event) {
            var $divs = $(".recommendedCards");
            var fees_sort_dsc = $divs.sort(function (a, b) {
                if (!($(a).find('.recommendedCards').hasClass('hidden'))) {
                    return $(b).find(".fees_class").attr('data-fees') - $(a).find(".fees_class").attr('data-fees');
                }
            });
            $("#recommendedCards_div").html(
                    fees_sort_dsc);
            $('#filterPopup').modal('hide');

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".sort_by_lyf_asc, .mob_sort_by_lyf_asc ", function (event) {
            var $divs = $(".recommendedCards");
            var lyf_sort_asc = $divs.sort(function (a, b) {
                if (!($(a).find('.recommendedCards').hasClass('hidden'))) {
                    return $(a).find(".lspbListSize").val() - $(b).find(".lspbListSize").val();
                }
            });
            $("#recommendedCards_div").html(
                    lyf_sort_asc);
            $('#filterPopup').modal('hide');

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".sort_by_lyf_dsc, .mob_sort_by_lyf_dsc", function (event) {
            var $divs = $(".recommendedCards");
            var lyf_sort_dsc = $divs.sort(function (a, b) {
                if (!($(a).find('.recommendedCards').hasClass('hidden'))) {
                    return $(b).find(".lspbListSize").val() - $(a).find(".lspbListSize").val();
                }
            });
            $("#recommendedCards_div").html(
                    lyf_sort_dsc);
            $('#filterPopup').modal('hide');

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".air_ticket_asc, .mob_air_ticket_asc", function (event) {
            if ($('.ff').html() != 'XX') {
                var $divs = $(".recommendedCards");
                var air_ticket_asc = $divs.sort(function (a, b) {
                    if (!($(a).find('.recommendedCards').hasClass('hidden'))) {
                        return parseInt($(a).find(".ff").html()) - parseInt($(b).find(".ff").html());
                    }
                });
                $("#recommendedCards_div")
                        .html(air_ticket_asc);
                $('#filterPopup').modal('hide');
            }
            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", " .air_ticket_dsc, .mob_air_ticket_dsc", function (event) {
            if ($('.ff').html() != 'XX') {
                var $divs = $(".recommendedCards");
                var air_ticket_dsc = $divs.sort(function (a, b) {
                    if (!($(a).find('.recommendedCards').hasClass('hidden'))) {
                        return parseInt($(b).find(".ff").html()) - parseInt($(a).find(".ff").html());
                    }
                });
                $("#recommendedCards_div")
                        .html(air_ticket_dsc);
                $('#filterPopup').modal('hide');
            }
            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });
        /*Sort functionality ends*/

        /*Recommended validation starts*/

        var membercityName = '${memberCity}'

        $(".city_uncheck").each(function () {
            if (membercityName != "" && membercityName.toLowerCase() === $(this).val().toLowerCase()) {
                $("input[value='" + $(this).val() + "']").prop('checked', true);
                $('input[name="modal_city"]').trigger('change');
                $("#cityName").val(membercityName);
            }
        });

        $('#select_modal_city > option').each(function () {
            if (membercityName.toLowerCase() === $(this).val().toLowerCase()) {
                $(this).prop('selected', true);
                $("#cityName").val(membercityName);
            }
        });

        $('input[name="modal_city"]').on('change', function () {
            $("#error_msg_city").hide();
            $(".select_city_uncheck").val('');
            $("[name=" + $(this).prop('name') + "]").prop("checked", false);
            $(this).prop("checked", true);
            $("#cityName").val(this.value);
        });

        $("#select_modal_city").change(function () {
            $(".city_uncheck").prop("checked", false);
            var cityName = $("#select_modal_city").val();
            $("#error_msg_city").hide();
            $("#cityName").val(cityName);

        });

        $("#date_of_birth").on("change", function () {
            $("#error_msg_dob").hide();
        });

        $(':checkbox[name=lsbp_name]').on('click', function () {
            var checkedBoxlength = $(':checkbox[name=lsbp_name]:checked').length;
            if (checkedBoxlength > 2) {
                $("#error_msg_lsbpArray").show().text("Sorry, You can select only 2 benefit preferences at a time");
                return false;
            } else {
                $("#error_msg_lsbpArray").hide();
                $("#lsbpArray").val(this.value);
            }
        });
        $("input[name=annual_Income]").change(function () {
            $("#annualIncome").val(this.value);
            $("#error_msg_annualIncome").hide();
        });

        $('#CrdRC_btn_submit-recommend').on('click', function () {
            var annualIncome = $("#annualIncome").val();
            if (annualIncome == "") {
                $("#error_msg_annualIncome").show().text("Please Select Gross Annual Income");
                return false;
            } else {
                $("cardDetailsBean").submit();
                //Adobe code starts here
                $.fn.AnnualIncomeClick($("#annualIncome").val());
                //Adobe code ends here
            }
        });
        /*Recommended validation ends*/

        /*Spends calculation logic starts*/
        $(".donebutdone").on('click', function () {
            $('.free_flights_div').css('display', 'block');
            $('.free_flights_desc').css('display', 'none');
            $('.freeFlight_spends_calc').addClass('hidden');
            //$('.spends_calc').addClass('hidden');
            if ($(".slider_error_div").text() == "") {
                var spend = $("#yearly_bill").html().replace(/<\/?img[^>]*>/g, "");
                spend = spend.substring(spend.indexOf(' ') + 1);

                //Adobe code starts here
                $.fn.CompareCardsPopupClick(spend);
                //Adobe code ends here

                var arr = [];
    <c:forEach items="${recommendList}" var="listItem">
                arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
                $.get("${applicationURL}calcfreeflights?spend=" + spend + "&cpNoList=" + arr, function (data, status) {
                    if (status == "success") {
                        $.each(data, function (key, value) {
                            $("#freeFlights" + key).html(value.split(",")[1]);
                            $("#jpMiles" + key).html("on earning <br>" + value.split(",")[0] + " InterMiles");
                        });
                        $('.parent_div').addClass('hidden');
                    }
                });
            }
        });
        $(".donebut").on('click', function () {
            $('.free_flights_div').css('display', 'block');
            $('.free_flights_desc').css('display', 'none');
            $('.freeFlight_spends_calc').addClass('hidden');
            //$('.spends_calc').addClass('hidden');
            if ($(".slider_modal_div_error").text() == "") {
                var spend = $("#yearly_bill1").html().replace(/<\/?img[^>]*>/g, "");
                spend = spend.substring(spend.indexOf(' ') + 1);

                //Adobe code starts here
//                            $.fn.CompareCardsPopupClick(spend);
                //Adobe code ends here

                var arr = [];
    <c:forEach items="${recommendList}" var="listItem">
                arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
                $.get("${applicationURL}calcfreeflights?spend=" + spend + "&cpNoList=" + arr, function (data, status) {
                    if (status == "success") {
                        $.each(data, function (key, value) {
                            $("#freeFlights" + key).html(value.split(",")[1]);
                            $("#jpMiles" + key).html("on earning <br>" + value.split(",")[0] + " InterMiles");
                        });
                        $('.parent_div').addClass('hidden');
                    }
                });
            }
        });
        $(".annual_spends").on('click', function () {
            $('.free_flights_div').css('display', 'block');
            $('.free_flights_desc').css('display', 'none');
            $('.freeFlight_spends_calc').addClass('hidden');
            //$('.spends_calc').addClass('hidden');

            var spend = $(".spends_calc .annualSpends_value").html().replace(/<\/?img[^>]*>/g, "");
            spend = spend.substring(spend.indexOf(' ') + 1);

            //Adobe code starts here
            $.fn.CompareCardsPopupClick(spend);
            //Adobe code ends here

            var arr = [];
    <c:forEach items="${recommendList}" var="listItem">
            arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
            $.get("${applicationURL}calcfreeflights?spend=" + spend + "&cpNoList=" + arr, function (data, status) {
                if (status == "success") {
                    $.each(data, function (key, value) {
                        $("#freeFlights" + key).html(value.split(",")[1]);
                        $("#jpMiles" + key).html("on earning <br>" + value.split(",")[0] + " InterMiles");
                    });
                    $('.parent_div').addClass('hidden');
                }
            });
        });
        /*Spends calculation logic ends*/

        $.validator.addMethod('valideJpNumber', function (value,
                element) {
            var jpNumLastDig, jpNoTemp;
            var jp = $("#jpNumber").val();
            if (jp.length != 9)
                return false;
            else {
                jpNumLastDig = jp % 10;
                jpNoTemp = eval(jp.slice(0, -1)) % 7;
                if (jpNumLastDig !== jpNoTemp)
                    return false;
                else
                    return true;
            }
        });

        $.validator.addMethod('checkForValidTier', function (value, element) {
            if (tierExist) {
                return true;
            } else {
                return false;
            }
        });

        $('#jpNumber').on('change', function () {
            var jpNum = $("#jpNumber").val()
            $.ajax({url: "${applicationURL}getTier?jpNum=" + jpNum,
                async: false,
                success:
                        function (data) {
                            tierExist = data.length > 2 ? true : false;
                        }
            });

            $("#tellMeAbout").validate().element('#jpNumber');
        });

        $(document).on("click", ".upgradeNow", function () {
            $("#otcpNo").val(parseInt($(this).attr('data-cpNo')));
            $("#otbpNo").val(parseInt($(this).attr('data-bpNo')));
            $('#askingUser').modal('hide');
            $('#userWithCreditCard').modal('hide');
            $('#abtYourself').modal('show');
            checkupgrade = true;
            //  $("#proceed_modal").modal('hide');


        });
        $(document).on("click", ".non_amex_apply", function () {
            checkupgrade = false;
        });
        $("#tellMeAbout")
                .validate(
                        {
                            ignore: [],
                            wrapper: "li",
                            rules: {
                                jpNumber: {
                                    required: true,
                                    valideJpNumber: true,
                                    checkForValidTier: true
                                }
                            },
                            messages: {
                                jpNumber: {
                                    required: "Please Enter InterMiles No.",
                                    valideJpNumber: "Please enter valid InterMiles No.",
                                    checkForValidTier: "Your application may not have gone through successfully. Please click Submit again."
                                }
                            },
                            errorElement: "div",
                            errorPlacement: function (error,
                                    element) {
                                error.insertAfter($(element)
                                        .closest("div"));

                            },
                            submitHandler: function (form) {

                                var fees, cardname, cardimg, bankName, id, jpNum;
//                 console.log("in submit handler"+checkupgrade);
                                if (checkupgrade == true) {
                                    id = $("#otcpNo").val();
                                    jpNum = $("#jpNumber").val();
                                    fees = $("#feesId" + id).html();
                                    cardname = $("#cardName" + id).html();
                                    cardimg = $("#card_img" + id).attr('src');
                                    bankName = $("#list_bankName" + id).val();
                                    var utm_source = '${utm_source}';
                                    var utm_medium = '${utm_medium}';
                                    var utm_campaign = '${utm_campaign}';
                                    var utm_param = "";
//    			 console.log("normal else submit handler");
                                    $("#abtYourself").modal('hide');
                                    $("#jpNo").text($("#jpNumber").val());
//                console.log("jpNumber in tell me about submit handler"+$("#jpNumber").val());
//                console.log("jpNo in tell me about submit handler "+$("#jpNo").val());


                                    if (utm_source != "" && utm_source != 'undefined') {
                                        utm_param = "&utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
                                    }
                                    $.ajax({url: "${applicationURL}Encryptjpnumber?jpNumber=" + jpNum, success: function (result) {
                                            var encryptnum = result;
                                            var win = window.open('${applicationURL}applyOtherupgrade?cpNo=' + id + '&imNum=' + encryptnum + utm_param, '_blank');
                                            if (win) {
                                                //Browser has allowed it to be opened
                                                win.focus();
                                            } else {

                                            }
                                        }});
                                } else {
                                    id = $("#otcpNo").val();
                                    jpNum = $("#jpNumber").val();
                                    fees = $("#feesId" + id).html();
                                    cardname = $("#cardName" + id).html();
                                    cardimg = $("#card_img" + id).attr('src');
                                    bankName = $("#list_bankName" + id).val();
                                    var utm_source = '${utm_source}';
                                    var utm_medium = '${utm_medium}';
                                    var utm_campaign = '${utm_campaign}';
                                    var utm_param = "";
//    			 console.log("normal else submit handler");
                                    $("#abtYourself").modal('hide');
                                    $("#jpNo").text($("#jpNumber").val());
                                    $("#proceed_modal").modal('show');
                                    $('#replaceImg').attr('src', cardimg);
                                    $('#popCardName').html(cardname);
                                    $('#popFees').html(fees);
                                    $("#proceedBank").text("Proceed to " + bankName);

                                    if (utm_source != "" && utm_source != 'undefined') {
                                        utm_param = "&utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
                                    }
                                    var encryptnum;
                                    $.ajax({url: "${applicationURL}Encryptjpnumber?jpNumber=" + jpNum, success: function (result) {
                                            encryptnum = result;
                                        }});
                                    $("#proceedTo").click(function (event) {
                                        $('#proceedTo').attr("href", "${applicationURL}applyOther?cpNo=" + id + "&imNum=" + encryptnum + utm_param);
                                        $("#proceed_modal").modal('hide');
                                        event.stopPropagation();
                                    });

                                    //Adobe code starts here
//                                    submitButtonClick(jpNum, 'Earn with Partners: Swipe any of our Co-brand Cards','Proceed to Bank','Page Refresh');
//                                    $.fn.AfterApplyJPNumberPopUpClick(jpNum, 'Earn with Partners: Swipe any of our Co-brand Cards');
                                    //Adobe code ends here
                                }
                            },
                        });

        $("#quickEnrollUser").validate(
                {
                    ignore: [],
                    rules: {
                        enrollTitle: {
                            required: true,
                        },
                        enrollFname: {
                            required: true,
                            AlphabetsOnly: true
                        },
                        enrollLname: {
                            required: true,
                            AlphabetsOnly: true
                        },
                        enrollCity: {
                            required: true,
                        },
                        enrollemail: {
                            required: true,
                            emailValidation: true
                        },
                        enrollPhone: {
                            required: true,
                            mobileValidation: true
                        },
//                                enrollPassword: {
//                                    required: true,
//                                    passwordChecking: true
//                                },
//                                enrollRenterPassword: {
//                                    required: true,
//                                    equalTo: "#enrollPassword",
//                                    passwordChecking: true
//                                },
                        enrollDob: {
                            required: true
                        },
                        termsAndConditionsEnroll: {
                            required: true
                        }
                    },
                    messages: {
                        enrollTitle: {
                            required: "Please Select Title",
                        },
                        enrollFname: {
                            required: "Please Enter FirstName",
                            AlphabetsOnly: "Only Characters from A-Z are allowed",
                        },
                        enrollLname: {
                            required: "Please Enter LastName",
                            AlphabetsOnly: "Only Characters from A-Z are allowed",
                        },
                        enrollCity: {
                            required: "Please Enter City Name",
                        },
                        enrollemail: {
                            required: "Please Enter Email Id",
                            emailValidation: "Please Enter valid Email Id"
                        },
//                                enrollPassword: {
//                                    required: "Please enter new password",
//                                    passwordChecking: '<ul class="pwd_error"><li>Password must have:</li> <div class="pwd_error_content"><li> 8 - 13 characters.</li><li> at least 1 lowercase alphabet.</li><li> at least 1 uppercase alphabet.</li><li> at least 1 number.</li></div></ul>'
//                                },
//                                enrollRenterPassword: {
//                                    required: "Please Re enter password",
//                                    equalTo: "Passwords do not match",
//                                    passwordChecking: '<ul class="pwd_error"><li>Re Enter Password must have:</li> <div class="pwd_error_content"><li> 8 - 13 characters.</li><li> at least 1 lowercase alphabet.</li><li> at least 1 uppercase alphabet.</li><li> at least 1 number.</li></div></ul>'
//                                },
                        enrollPhone: {
                            required: "Please Enter Mobile Number",
                            mobileValidation: "Please Enter Valid Mobile Number"
                        },
                        enrollDob: {
                            required: "Please Enter Date of Birth",
                        },
                        termsAndConditionsEnroll: {
                            required: "Please accept the InterMiles membership Terms & Conditions to proceed."
                        }
                    },
                    errorElement: "div",
                    onfocusout: function (element) {
                        this.element(element);  // <- "eager validation"
                    },
                    highlight: function (element) {
                        $(element)
                                .siblings('div.error')
                                .addClass('alert');
                    },
                    unhighlight: function (element) {
                        $(element)
                                .siblings('div.error')
                                .removeClass('alert');
                    },
                    errorPlacement: function (error,
                            element) {
                        $('.error_box2').removeClass(
                                'hidden');
                        error.insertAfter($(element)
                                .closest("div"));
                        $(error).appendTo('.error_box2 ul.error_list2');
                        $('.modal').stop(true, false)
                                .animate({
                                    scrollTop: 0
                                }, 'slow');

                    },
                    submitHandler: function (form) {
                        form.submit();
                    },
                });
//        $("#enrollTitle,#enrollCity,#enrollFname,#enrollLname,#enrollPhone,#enrollemail,#termsAndConditionsEnroll").on('focus', function () {
//            console.log("in focu of enroll")
//            $('.error_box1').addClass('hidden');
////            
//        })
        $("#enrollMname").on('focusout', function () {
            console.log("in focusout enrollDob")

//            $("#quickEnrollUser").validate().element('#enrollDob');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollDob").on('focusout', function () {
            console.log("in focusout enrollDob")

            $("#quickEnrollUser").validate().element('#enrollDob');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $(".ui-datepicker-trigger").click(function () {
            $('.error_box2').addClass('hidden');

        })
        $("#termsAndConditionsEnroll").on('focusout', function () {
            console.log("in focusout termsAndConditionsEnroll")

            $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollemail").on('focusout', function () {
            console.log("in focusout enrollemail")

            $("#quickEnrollUser").validate().element('#enrollemail');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollPhone").on('focusout', function () {
            console.log("in focusout enrollPhone")

            $("#quickEnrollUser").validate().element('#enrollPhone');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollLname").on('focusout', function () {
            console.log("in focusout enrollLname")

            $("#quickEnrollUser").validate().element('#enrollLname');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollTitle").on('focusout', function () {
            console.log("in focusout enrolltitle")

            $("#quickEnrollUser").validate().element('#enrollTitle');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollCity").on('focusout', function () {
            console.log("in focusout enrollcity")

            $("#quickEnrollUser").validate().element('#enrollCity');
            $('.error_box2').removeClass('hidden');

            var len = $('.error_list2 > li:visible').length;

            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }
        });
        $("#enrollFname").on('focusout', function () {
            console.log("in focusout enrollFname")

            $("#quickEnrollUser").validate().element('#enrollFname');
            $('.error_box2').removeClass('hidden');

            var len = $('.error_list2 > li:visible').length;

            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }
        });

        $('#enrollForm,#enrollForm_mob_view').on('click', function () {
            var bpNo = $("#otbpNo").val();
            if ($("#quickEnrollUser").validate().element('#enrollTitle') && $("#quickEnrollUser").validate().element('#enrollFname')
                    && $("#quickEnrollUser").validate().element('#enrollLname') && $("#quickEnrollUser").validate().element('#enrollCity')
                    && $("#quickEnrollUser").validate().element('#enrollPhone') && $("#quickEnrollUser").validate().element('#enrollemail')
//                                    && $("#quickEnrollUser").validate().element('#enrollPassword')
//                                    && $("#quickEnrollUser").validate().element('#enrollRenterPassword')
                    && $("#quickEnrollUser").validate().element('#date_of_birth2') && $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll')) {

                var data;


                var responsecode = $('#g-recaptcha-response').val()
//                                console.log("result---->" + responsecode);
                $.ajax({method: 'POST',
                    url: '${applicationURL}verifycaptcha?responsecode=' + responsecode, success: function (result) {

//                                        console.log("result---->" + result);
                        data = result;
                        if (data == false) {

//                                            console.log("false data--->")
                            $('.error_box2').removeClass('hidden');
//                            $('#error').html("Please select captcha").appendTo('.error_box1 ul.error_list1');
                            $('.error_box2 ul.error_list2').html("Please select captcha");
                            $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');
                        } else {
//                                            console.log("true data-->")
                            $('.error_box2').addClass('hidden');
                            $('.error_box2 ul.error_list1').html("");
                            //password hashing//
//                                            var enrollPassword = "";
//                                            var hashInBase64 = "";
//                                            var hash = "";
//
//                                            enrollPassword = $("#enrollPassword").val();
//                                            hash = CryptoJS.HmacSHA256(enrollPassword, "bff29f3a-90d4-42d5-94f0-448e699991c1");
//                                            hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
//                                            $("#hashPassword").val(hashInBase64);
                            $("#enrollbpNo").val(bpNo);
                            //password hashing//

//                                            $('#enroll_error').addClass('hidden');
                            $('#loader').removeClass('hidden');
                            $('.modal-backdrop').css('z-index', '1050');
                            var checkbox = $("#termsAndConditionsEnroll");
                            checkbox.val(checkbox[0].checked ? "true" : "false");
                            var checkbox2 = $("#recieveMarketingTeam");
                            checkbox2.val(checkbox2[0].checked ? "true" : "false");
//                                            console.log("1" + $("#termsAndConditionsEnroll").val());
//                                            console.log("2" + $("#recieveMarketingTeam").val());
                            $.ajax({
                                url: '${applicationURL}enrollme',
                                data: $(
                                        '#quickEnrollUser')
                                        .serialize(),
                                type: 'POST',
                                success: function (
                                        data) {
                                    var jsonStr = JSON.parse(data);
                                    if (jsonStr.msg == "" && jsonStr.jpNumber != "" && jsonStr.jpNumber != "null") {
                                        $("#jpNo").text(jsonStr.jpNumber);
                                        $("#jpNumber").val(jsonStr.jpNumber);
                                        tierExist = true;
                                        $('.enroll_popup').addClass('hidden');
                                        $('.jpNum_popup').removeClass('hidden');
                                    } else {
                                        $('#loader').addClass('hidden');
                                        $('#enroll_error').removeClass('hidden');
                                        if (jsonStr.msg == "")
                                            $('#enroll_error').text("Your application may not have gone through successfully. Please click 'Continue' again.");
                                        else
                                            $('#enroll_error').text(jsonStr.msg);
                                        $('.modal-backdrop').css('z-index', '1048');
                                    }
                                },
                                complete: function () {
                                    $('#loader')
                                            .addClass(
                                                    'hidden');
                                    $(
                                            '.modal-backdrop')
                                            .css(
                                                    'z-index',
                                                    '1048');
                                },
                                error: function (
                                        xhr, status) {
                                    console.log(status);
                                    console.log(xhr.responseText);
                                }
                            });
                        }
                    },
                    error: function (e) {
                        console.log("error---->" + e);
                    }
                });
            } else {
                $('.error_box2').removeClass('hidden');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');

            }
        });

        $('#socialName').keydown(function (event) {
            var charCode = event.keyCode;
            if ((charCode > 64 && charCode < 91) || charCode == 8 || charCode == 9 || charCode == 32) {
                return true;
            } else {
                return false;
            }
        });
        $("#social-profiles").on('click', function () {
            $('.error_box1').addClass('hidden');
            var fullName = "";
            var socialName = $("#socialName").val();
            var socialPhone = $("#socialPhone").val();
            var socialEmail = $("#socialEmail").val();
//          
//            var formNumber = ' $ { formNumber}';
//            var cardName='${bean.cardName}';
            if ($("#socialName").val() != "") {
                $("#socialName").val(socialName);
            }
            if ($("#socialPhone").val() != "") {
                $("#socialPhone").val(socialPhone);
            }
            if ($("#socialEmail").val() != "") {
                $("#socialEmail").val(socialEmail);
            }
//            $("#socialjpNumber").val(jetpriviligemembershipNumber);
            $("#formNumber").val("");
            $("#pageName").val("Recommend Card Page");
            $("#cardName").val("");
//                        $('#callMeContine').attr('disabled',true)

            $('#callMe_modal').modal('show');
//             if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
//                console.log("****")
//                $('#callMeContine').attr('disabled',true)
//            }else{
//                console.log("&&&&&&")
//               $('#callMeContine').attr('disabled',false)
//
//            }

        });
        $('#callMeContine').on('click', function () {

            if ($("#callMe").validate().element('#socialName')
                    && $("#callMe").validate().element('#socialPhone')
                    && $("#callMe").validate().element('#socialEmail')
//                    && $("#callMe").validate().element("#socialjpNumber")
                    )
            {
                $('#loader').removeClass('hidden');
                $('.modal-backdrop').css('z-index', '1050');
                $.ajax({
                    url: '${applicationURL}callme',
                    data: $('#callMe').serialize(),
                    type: 'POST',
                    success: function (data) {
                        if (data != '') {
                            console.log("data in call me", data);
                            $('#callMe_modal').modal('hide');
                            $('#successCallmeModal').modal('show');
                        } else {
                            $('#loader').addClass('hidden');
                            alert("sorry please try again");
                            $('.modal-backdrop').css('z-index', '1048');
                        }
                        
                        //Adobe code start (phase 6)
                        assistFormSubmitButton($("#socialName").val(),$("#socialPhone").val(),$("#socialEmail").val());
                        //Adobe code end (phase 6)
                    },
                    complete: function () {
                        $('#loader').addClass('hidden');
                        $('.modal-backdrop').css('z-index', '1048');
                        $("#socialName").val('');
                        $("#socialPhone").val('');
                        $("#socialEmail").val('');
                    },
                    error: function (xhr, status) {
                        console.log(status);
                        console.log(xhr.responseText);
                    }
                });
            } else {
                $('.error_box1').removeClass('hidden');
            }
        });
        $("#callMe").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                socialName: {
                    required: true,
                    AlphabetsOnly: true
                },
                socialPhone: {
                    required: true,
                    digits: true,
                    mobileValidation: true
                },
                socialEmail: {
//                    required: true,
                    emailValidation: true
                }
//                socialjpNumber:{
//                digits:true
//                }
            },
            messages: {
                socialName: {
                    required: "Please Enter Full Name",
                    AlphabetsOnly: "Enter Characters Only"
                },
                socialPhone: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid.",
                    mobileValidation: "Please Enter Valid Mobile Number"
                },
                socialEmail: {
//                    required: "Please Enter Email Id",
                    emailValidation: "Email ID is not valid."
                }
//                ,
//                socialjpNumber:{
//                digits:"Please Enter valid Jetprivilege Number."
//                }
            },
            errorElement: "div",
            onfocusout: function (element) {
                this.element(element);  // <- "eager validation"           
            },

            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },
            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                $('.error_box1').removeClass('hidden');
                error.insertAfter($(element).closest("div"));
                $(error).appendTo('.error_box1 ul');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');
                
                //Adobe code starts here(phase 6)
                assistError = assistError+error.text()+",";
                $.fn.AssistMeError(assistError);
                console.log("Error at validator !!!",assistError);
               //Adobe code ends here(phase 6)
               
            },
            submitHandler: function (form) {
                form.submit();
            },
        });
        $("#socialName").on('focusout', function () {
            console.log("in focusout socialName")

            $("#callMe").validate().element('#socialName');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }


        });
        $("#socialPhone").on('focusout', function () {
            console.log("in focusout socialPhone")

            $("#callMe").validate().element('#socialPhone');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;

            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }
        });
        $("#socialEmail").on('focusout', function () {
            console.log("in focusout socialEmail")

            $("#callMe").validate().element('#socialEmail');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;

            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }
        });
//        $("#socialName,#socialPhone,#socialEmail").on('focus', function () {
//            $('.error_box1').addClass('hidden');
//        })

//$("#socialName,#socialPhone,#socialEmail").on('focusout', function () {
//if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
//                console.log("***!!!!*")
//                $('#callMeContine').attr('disabled',true)
//            }else{
//                console.log("&&&!!!!&&&")
//               $('#callMeContine').attr('disabled',false)
//
//            }            
//        });
//        $("#socialName,#socialPhone,#socialEmail").on('input change', function () {
//            if($("#socialName").is(':focus') || $("#socialPhone").is(':focus') || $("#socialEmail").is(':focus')){
//if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
//                console.log("*")
//                $('#callMeContine').attr('disabled',true)
//            }else{
//                console.log("&")
//               $('#callMeContine').attr('disabled',false)
//
//            } 
//        }
//        });

        $('#callmeclose').click(function () {
            $('.error_box1').addClass('hidden');
        });

        $('.proceedPopup_close').click(function () {
            $('.error_box2').addClass('hidden');
        });



        $('#date_of_birth2').on('change', function () {
            $("#quickEnrollUser").validate().element('#date_of_birth2');
        });

        $(".title_select").change(function () {
            if ($(this).find(":selected").text() == "Mr") {
                $("#tellMeAbout input[type=radio][value=0]").prop('checked', true);

            }
            if ($(this).find(":selected").text() == "Ms" || $(this).find(":selected").text() == "Mrs") {
                $("#tellMeAbout input[type=radio][value=1]").prop('checked', true);
            }
        });

        Array.prototype.remove = function () {
            var what, a = arguments, L = a.length, ax;
            while (L && this.length) {
                what = a[--L];
                while ((ax = this.indexOf(what)) !== -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };

    });


    //Adobe code filter usage functionality starts here

    function isInArray(array, value)
    {
        return array.indexOf(value) >= 0;

    }

    function applyButtonAmexClickAdobe(cardName, variant, bankName, formName, abandonType) {
        var cardName1 = cardName.replace("<sup>�</sup>", "");
        var variant1 = variant.replace("<sup>�</sup>", "");

        IsButtonClicked = true;
        popupAbandonType = formName;
        abandonFormName = formName;
        formAbandonType = abandonType;
        $.fn.ApplyButtonAmexClick(cardName1, variant1, bankName);

    }

    $("#tellme").click(function () {
        var jpNum = $("#jpNumber").val();
        var popupName = "Earn with Partners: Swipe any of our Co-brand Cards";
        var formName = "Proceed to Bank";
        var abandonType = "Page Refresh";

        abandonFormName = formName;
        formAbandonType = abandonType;
        $.fn.AfterApplyJPNumberPopUpClickForHdfc(jpNum, popupName);

    });
    
    //submit button assist me form(phase 6 start)
     function assistMeeClick(){
         assistMeFlag = true;
         $.fn.ClickOfAssistMe();
     }
     
     function assistFormSubmitButton(name,mobile,email){
         if(name !== "" && mobile !== ""){
             name = "name:yes";
             mobile = "mobile:yes";
         }else{
             name = "name:no";
             mobile = "mobile:no";
         }
         if(email !== ""){
             email = "email:yes";
         }else{
             email = "email:no";
         }
         $.fn.ClickOfAssistContinueButton(name,mobile,email);
     }
     //(phase 6 end)

    function moreDetailsClick(cardName, variant) {
        var cardName1 = cardName.replace("�", "");
        var variant1 = variant.replace("�", "");

        $.fn.MoreDetailsForCardsClick(cardName1, variant1);
    }

    function successEnrolClick() {
        var jpNum = $("#jpNumber").val();
        $.fn.successfulQuickEnrollmentOnHdfc(jpNum);

    }

    function noExistingCardHolderClick(existingName, popupType) {
        popupAbandonType = popupType;
        $.fn.existingCreditCardHolderClick(existingName);
    }

    function afterEnrollHereClick(enrollName, popupType) {
        abandonFormName = popupType;
        popupAbandonType = popupType;
        $.fn.AfterApplyEnrolPopUpClick(enrollName);
        $.fn.AfterApplyEnrolPopUpClickCT();
    }

    function formAbandonmentOnPopUp(closeType) {
        $.fn.formAbandonmentOnSubmitJPnumberPopup(closeType, popupAbandonType);
    }

    function upgradeOtherCardsClick(upgradeType, popupType) {
        popupAbandonType = popupType;
        $.fn.upgradeOrViewOtherCardsClick(upgradeType);
    }

    function upgradeOtherCardsClickforYes(upgradeType) {
        IsButtonClicked = false;
        $.fn.upgradeOrViewOtherCardsClick(upgradeType);

    }

    function  getFinalFilters()
    {
        var lBankNames = "";
        var lLifeStyleBenefits = "";
        var lJoiningFees = "";
        var lFinalFilters = "";
        var joiningFeesAdobe = $(".slider_div .rangeslider__handle").html().replace(/<\/?span[^>]*>/g, "").replace(/\,/g, '');


        if (bankNamesAdobe.length > 0)
            for (i = 0; i < bankNamesAdobe.length; i++) {
                lBankNames = lBankNames + "Bank" + ":" + bankNamesAdobe[i] + "|";
            }


        if (joiningFeesAdobe.length > 0)
            lJoiningFees = "Joining Fees" + ":" + joiningFeesAdobe + "|";

        if (lifestyleBenefitsAdobe.length > 0)
            for (i = 0; i < lifestyleBenefitsAdobe.length; i++) {

                if (i == lifestyleBenefitsAdobe.length - 1)
                {
                    lLifeStyleBenefits = lLifeStyleBenefits + "LifeStyle Benefits" + ":" + lifestyleBenefitsAdobe[i];

                } else
                {
                    lLifeStyleBenefits = lLifeStyleBenefits + "LifeStyle Benefits" + ":" + lifestyleBenefitsAdobe[i] + "|";
                }

            }
        lFinalFilters = lBankNames + lJoiningFees + lLifeStyleBenefits;

        return lFinalFilters;

    }

    function submitFilterAdobe() {

        var lFinalFilters = getFinalFilters();
//        console.log("final filter is here" + lFinalFilters)
        $.fn.FilterUsage(lFinalFilters, sortByAdobe);
    }


    //Call to load of all cards
    function loadOfCardsAdobe() {
        var lFinalFilters = getFinalFilters();
        $(".cardinfo").each(function () {
            cardNamesAdobe = cardNamesAdobe + $(this).text() + "|"

        });
        cardsCountAdobe = $(".cardinfo").length;
        $.fn.LoadOfAllCards(cardNamesAdobe, cardsCountAdobe, lFinalFilters, sortByAdobe);
    }


    //Adobe code starts here
    function pageInfo() {
        
        //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----recommendedCards--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----recommendedCards--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----recommendedCards--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----recommendedCards--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----recommendedCards--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----recommendedCards--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----recommendedCards--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----recommendedCards-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----recommendedCards--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End   
        
        
        
        
        
        
        
        
         //Adobe code (phase 6)start
        var callmeflag ='${callMeFlag}';
          if(callmeflag == 1){
              setTimeout(function()
              { 
                $.fn.ViewOfAssistMe() 
              }, 3000);
          }
        //Adobe code (phase 6)ends
        $('form input, form select').click(function () {
           lastAccessedField = $(this).attr('name');
         });

        digitalData.pageInfo = {"pageName": "Recommend", "category": "Cards", "subCategory1": "Card Listing", "subCategory2": "Recommended", "partner": "JP", "currency": "INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
        loadOfCardsAdobe();
        $(document.body).on('hidden.bs.modal', function () {
            digitalData.pageInfo.pageName = "Recommend";
            digitalData.pageInfo.category = "Cards";
            digitalData.pageInfo.subCategory1 = "Card Listing";
            digitalData.pageInfo.subCategory2 = "Recommended";
        });
    }
    
    function formAbandonOfAssistMe(){
         var abandonType = "pop-up close";
         if(lastAccessedField == "socialName"){
            lastAccessedField="Name";
         }else if(lastAccessedField == "socialPhone"){
            lastAccessedField="Mobile";
         }else if(lastAccessedField == "socialEmail"){
            lastAccessedField="Email";
         }
         $.fn.FormAbandonmentOfAssistMe(name,email,mobile,lastAccessedField,abandonType);
     }


    //form abandonment start
    var checkCloseX = 0;
    $(document).mousemove(function (e) {
        if (e.pageY <= 5) {
            checkCloseX = 1;
        } else {
            checkCloseX = 0;
            formAbandonType = "Page Refresh";
        }
    });

    window.onbeforeunload = function (event) {
        if (checkCloseX === 1)
        {
            formAbandonType = "Browser Close";
        }

        if (IsButtonClicked)
        {
            $.fn.formAbandonmentOnSubmitJPnumberPopup(formAbandonType, abandonFormName);
        }
        
        if(assistMeFlag == true)
        {
            $.fn.FormAbandonmentOfAssistMe(name,mobile,email,lastAccessedField,"page refresh");
        }  


    };
    //Form Abandonment close
    //Adobe code ends here     
</script>