<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<section class="innerContainer clearfix bottomPadding">
        <section class="main-section">
            <div class="banner" style="background:url('https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/banners/Cards_1291px_364px_2_1.jpg') no-repeat; background-size: cover;" title="InterMiles Co-brand Credit Cards, Debit Cards &amp; Corporate Cards">
                <div class="container">
                    <div class="banner_textarea">
                        <h1 style="font-weight: bold"> <strong>JetPrivilege is now InterMiles</strong>! </h1><br><p class="bannertxt">Continue using your Co-brand Cards to accrue InterMiles on all your spends & redeem them for exciting travel and lifestyle rewards!  </p>
                        
                        
                      
                    </div>
                </div>
            </div>
        </section>

                
    
    <!--style="color: #28262c;font-weight: 500;display: inline-block;"-->
        <!-- banner end --> 
        <div class="clearfix"></div>
        <!-- content start -->  
        <section class="container">
          
            <div class="card_listing_page">
                <div class="cont_area">
                    <!--<h1>Your existing credit cards now have new features. Check them out <a href="#cardslink">here</a>! *This CTA to take member to the card listing section*</h1>-->
                     <div class="que" >Q. Are my JetPrivilege / Jet Airways Credit Cards still valid?</div>
                    <div class="ans" >A: Yes! Your credit cards are still valid. You will now earn InterMiles on your existing cards, just as today, without any change. Our respective bank partners will get in touch with you with regards to the new plastics, when they are ready. </div>

                        
                    <div class="que">Q. What happens to my accumulated JPMiles?</div>   
                    <div class="ans">A: Your miles are now called InterMiles. All your accumulated miles will continue being available to you, just as they are, without any change in your InterMiles account. </div>
                    
                    <div class="que">Q. What is InterMiles?</div>
                    <div class="ans">A: InterMiles is the most rewarding Travel & Lifestyle program you will ever need. At InterMiles we believe every mile count, so the more InterMiles you accumulate the higher your Tier and the more the benefits. So, move up with every Swipe! 
Accumulate InterMiles faster by booking flights & hotels with us or by earning with over 150+ partners in the InterMiles program. 
Also, you can use your InterMiles everywhere – Redeem your InterMiles for flight tickets across any airline, for your hotel stays, free fuel or for thousands of options on our Reward Store!</div>
                    
                    
       <div class="ans_faq"><a href="${applicationURL}faq">  <b>Click here</a> for more FAQ'S & Benefits  </b> </div>


                  
                </div>
                <input id="gmNo" name="gmNo" value="5" type="hidden">
                <div class="checkname" id="Home" hidden=""></div>
              
              
                    
                <div class="clearfix"></div>
                <div class="header_freeze" style="">
                    <div class="features_title title_header benefits_web" style="">
                        <div class="row" style="">
                            <div class="col-sm-3">
                                <p>Cards</p>
                            </div>

                            
                                <div class="col-sm-3">
                                    <p>Joining Benefit</p>
                                </div>
                            
                                <div class="col-sm-3">
                                    <p>InterMiles Benefit on spends*</p>
                                </div>
                            
                                <div class="col-sm-3">
                                    <p>Lifestyle &amp; Airline Benefits*</p>
                                </div>
                        </div>
                    </div>
                </div>

                <div id="sort_list_div">
                    
                    
                    
                    <!--added new cards content-->
                    
                                                <!--intermiles icici bank sapphiro visa-->
                    
                    
                    <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1"> InterMiles ICICI Bank Sapphiro Visa Credit Card </h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="InterMiles ICICI Bank Sapphiro Visa Credit Card " src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/SapphiroVisa.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 5,000</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
                                            
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 750 and INR 500 flight discount vouchers on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">30% Miles back for International flights*</span></li><li><span style="color: #000000;"></span>InterMiles Silver Tier </li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                                <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            
                                            <div class="list_cont">
                                                   
                                                
                                                    <div class="home_feature">
                                                        <!--<p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>4 InterMiles per INR 100/- spent domestically </li><li>5 InterMiles per INR 100/- spent internationally</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                        <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Fuel surcharge waiver</li><li>Concierge service</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>8 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>10 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>10% discount on Business and 5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                
                                                <!--intermiles icici bank sapphiro amex-->
                                                
                                                <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">InterMiles ICICI Bank Sapphiro AMEX Credit Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="InterMiles ICICI Bank Sapphiro AMEX Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/InterMiles_card_shot_Ame_sapphiro.jpg" height="50" width="50" id="card_img1">
                                                        <!--<img alt="InterMiles ICICI Bank Sapphiro AMEX Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/SapphiroAmex.jpg" height="50" width="50" id="card_img1">-->
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 5,000</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 750 and INR 500 flight discount vouchers on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">30% Miles back for International flights*</span></li><li><span class="cc-f98a7a" style="color: #000000;">InterMiles Silver Tier </span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <!--<p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>5 InterMiles per INR 100/- spent domestically </li><li>7 InterMiles per INR 100/- spent internationally</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Fuel surcharge waiver</li><li>Concierge service</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>10 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>12.5 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>10% discount on Business and 5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                
                                                
                                                <!--intermiles icici bank rubyx visa-->
                                                
                                                <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1"> InterMiles ICICI Bank Rubyx Visa Credit Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="InterMiles ICICI Bank Rubyx Visa Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/RubyxVisa.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 2,500</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 500 and IN 250 flight discount voucher on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">25% Miles back for International flights* </span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <!--<p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>3 InterMiles per INR 100/- spent domestically </li><li>4 InterMiles per INR 100/- spent internationally</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Fuel surcharge waiver</li><li>Concierge service</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>6 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>7.5 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                
                                                
                                                <!--intermiles icici bank rubyx amex-->
                                                
                                                
                                                <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">InterMiles ICICI Bank Rubyx AMEX Credit Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <!--<img alt="InterMiles ICICI Bank Rubyx AMEX Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/RubyxAMex.jpg" height="50" width="50" id="card_img1">-->
                                                        <img alt="InterMiles ICICI Bank Rubyx AMEX Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/RubyxAMex1.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 2,500</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 500 and IN 250 flight discount vouchers on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">25% Miles back for International flights* </span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <!--<p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>4 InterMiles per INR 100/- spent domestically </li>
                                                            <li>5 InterMiles per INR 100/- spent internationally</li>
                                                        </ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Fuel surcharge waiver</li><li>Concierge service</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>8 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>10 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> </li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                
                                                
<!--                                                intermiles icici bank coral visa-->
                                                
                                                 <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">InterMiles ICICI Bank Coral Visa Credit Card </h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="InterMiles ICICI Bank Coral Visa Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/CoralVisa.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 1,250</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;"> INR 250 flight discount voucher on enrollment for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;"> 20% Miles back for International flights*  </span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <!--<p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li> 2 InterMiles per INR 100/- spent</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li></<li>Dining offers</li><li>Fuel surcharge waiver</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>4 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>5 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> </li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                
                                                
                                                <!--intermiles icici bank coral amex-->
                    
                                                
                                                <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">InterMiles ICICI Bank Coral AMEX Credit Card </h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <!--<img alt="InterMiles ICICI Bank Coral AMEX Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/CoralAmex.jpg" height="50" width="50" id="card_img1">-->
                                                        <img alt="InterMiles ICICI Bank Coral AMEX Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/final_InterMiles_card _Amex_Coral.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 1,250</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 250 flight discount voucher on enrollment for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">20% Miles back for International flights* </span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <!--<p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>3 InterMiles per INR 100/- spent</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                        <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Fuel surcharge waiver</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>6 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>7.5 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> </li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                
                                                
                                                <!--intermiles indusind bank odyssey visa-->
                                                
                                                
                                                 <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">InterMiles IndusInd Bank Odyssey Visa Credit Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="InterMiles IndusInd Bank Odyssey Visa Credit Card" src="https://cards.intermiles.com/cards/Odyssey.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="10000">INR 10,000</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 750 and INR 500 flight discount vouchers on enrolment and renewal for flights booked on InterMiles.com  </span><li><span class="cc-f98a7a" style="color: #000000;">30% Miles back for International flights*</span></li><li><span class="cc-f98a7a" style="color: #000000;">InterMiles Silver Tier </span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <!--<p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>3 InterMiles per INR 100/- spent on weekdays </li><li>4 InterMiles per INR 100/- spent on weekends</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                        <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Concierge service</li><li>Wellness benefits</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>6 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> on weekdays</li><li>8 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> on weekend</li><li>7.5 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> on weekdays </li><li>10 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> on weekdays </li><li>10 % discount on Business and 5% discount on Economy class Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                    
                                          <!--intermiles indusind bank odyssey amex-->
                                          
                                          
<!--                                           <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">InterMiles IndusInd Bank Odyssey Amex Credit Card</h2>
                                        </div>
                                        
                                        New adobe changes 16th 07 18 start
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="InterMiles IndusInd Bank Odyssey Amex Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/intermiles-card_Odyssey_amex_front.png" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 10,000</span>
                                            </span>
                                            <div class="details_link">
                                                <a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>
                                            </div>
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 750 and INR 500 flight discount vouchers on enrolment and renewal for flights booked on InterMiles.com  </span><li><span class="cc-f98a7a" style="color: #000000;">30% Miles back for International flights*</span></li><li><span class="cc-f98a7a" style="color: #000000;">InterMiles Silver Tier </span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <ul><li>4 InterMiles per INR 100/- spent on weekdays </li><li>6 InterMiles per INR 100/- spent on weekends</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong>Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Concierge service</li><li>Wellness benefits</li></ul><p><strong>Airline Benefits </strong></p><ul><li>8 InterMiles for every INR 100/- spent on <a href="http://www.flighs.intermiles.com">flights.intermiles.com</a> on weekdays</li><li>12 InterMiles for every INR 100/- spent on <a href="http://www.flighs.intermiles.com">flights.intermiles.com</a> on weekend</li><li>10 InterMiles for every INR 100.- on Etihad flights booked on flights.intermiles.com on weekdays </li><li>15 InterMiles for every INR 100.- on Etihad flights booked on flights.intermiles.com on weekdays </li><li>10 % discount on Business and 5% discount on Economy class Etihad flights being booked on flights.intermiles.com</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>-->
                    
                                                
                                  <!--intermiles indusind bank voyage visa-->       
                                  
                                  
                                  <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">InterMiles IndusInd Bank Voyage Visa Credit Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="InterMiles IndusInd Bank Voyage Visa Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/intermiles-card_Voyage_visa_front.png" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 2,000</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 500 and INR 250 flight discount vouchers on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">25% Miles back for International flights*</span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <!--<p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>2 InterMiles per INR 100/- spent on weekdays </li><li>3 InterMiles per INR 100/- spent on weekends</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Concierge service</li><li>Wellness benefits</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>4 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> on weekdays </li><li>6 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> on weekend</li><li>5 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> on weekdays </li><li>7.5 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> on weekdays </li><li>5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                
                                        <!--intermiles indusind bank voyage amex-->   
                                        
                                        
<!--                                        <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">InterMiles IndusInd Bank Voyage AMEX Credit Card</h2>
                                        </div>
                                        
                                        New adobe changes 16th 07 18 start
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="InterMiles IndusInd Bank Voyage AMEX Credit Card" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/intermiles-card_Voyage_amex_front.png" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 5,000</span>
                                            </span>
                                            <div class="details_link">
                                                <a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>
                                            </div>
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 500 and INR 250 flight discount vouchers on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">25% Miles back for International flights*</span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <ul><li>3 InterMiles per INR 100/- spent on weekdays </li><li>4 InterMiles per INR 100/- spent on weekends</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong>Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Concierge service</li><li>Wellness benefits</li></ul><p><strong>Airline Benefits </strong></p><ul><li>6 InterMiles for every INR 100/- spent on <a href="http://www.flighs.intermiles.com">flights.intermiles.com</a> on weekdays </li><li>8 InterMiles for every INR 100/- spent on <a href="http://www.flighs.intermiles.com">flights.intermiles.com</a> on weekends</li><li>7.5 InterMiles for every INR 100.- on Etihad flights booked on flights.intermiles.com on weekdays </li>
                                                            <li>10 InterMiles for every INR 100.- on Etihad flights booked on flights.intermiles.com on weekdays </li><li>10 % discount on Business and 5% discount on Economy class Etihad flights being booked on flights.intermiles.com</li>
                                                        </ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                -->
                                                
                                                <!--jet privilege hdfc bank diners-->
                                                
                                                <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">Jet Privilege HDFC Diners Credit Card </h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="Jet Privilege HDFC Diners Credit Card " src="https://cards.intermiles.com/cards/JetPrivilege_HDFC_Bank_Diners_Club_Credit_Card1542181691327.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 10,000</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 1000 and INR 750 flight discount voucher on enrolment and renewal for flights booked on InterMiles.com
                                                            </span><li><span class="cc-f98a7a" style="color: #000000;">40% Miles back for International flights*</span></li><li><span class="cc-f98a7a" style="color: #000000;">InterMiles Gold Tier </span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <!--<p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>8 InterMiles per INR 150/- spent</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Golf access</li><li>Dining offers</li><li>Fuel surcharge waiver</li> <li>Concierge service</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>16 InterMiles for every INR 150/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>20 InterMiles for every INR 150.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>10% discount on Business and 5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> </li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                            
                                                
                                                <!--Jet Privilege HDFC Signature Credit Card--> 
                                                
                                                
                                                 <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1"> Jet Privilege HDFC Signature Credit Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="Jet Privilege HDFC Signature Credit Card" src="https://cards.intermiles.com/cards/JetPrivilege_HDFC_Bank_Signature_Credit_Card1542181654941.png" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 2,500</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 750 and INR 500 flight discount voucher on enrolment and renewal for flights booked on InterMiles.com </span>
                                                            <li><span class="cc-f98a7a" style="color: #000000;">35% Miles back for International flights* </span></li>
                                                            <li><span class="cc-f98a7a" style="color: #000000;">InterMiles Silver Tier  </span></li>
                                                            </ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
<!--                                                        <p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>6 InterMiles per INR 150/- spent</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>International and Domestic Lounge Access</li><li>Fuel Convenience fee Waiver</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>12 InterMiles for every INR 150/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>15 InterMiles for every INR 150.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a>
                                                         </li><li>10% discount on Business and 5% discount Economy on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                <!--Jet Privilege HDFC Platinum Credit Card--> 
                                                
                                                <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">Jet Privilege HDFC Platinum Credit Card </h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="Jet Privilege HDFC Platinum Credit Card" src="https://cards.intermiles.com/cards/JetPrivilegeHDFC_BankPlatinum_CreditCard1542181621117.png" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 1,000</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 500 and INR 250 flight discount voucher on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">30% Miles back for International flights*</span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>
                                                        <ul><li>4 InterMiles per INR 150/- spent</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul>
                                                            <li>Domestic Lounge Access</li>
                                                            <li>Fuel Convenience fee Waiver</li></ul>
                                                        <p><strong class="textleft">Airline Benefits </strong></p>
                                                        <ul><li>8 InterMiles for every INR 150/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li>
                                                            <li>10 InterMiles for every INR 150.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li>
                                                            <li>5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> </li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                        <!--Jet Privilege HDFC Select Credit Card-->
                                                
                                                <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">Jet Privilege HDFC Select Credit Card </h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="Jet Privilege HDFC Select Credit Card" src="https://cards.intermiles.com/cards/JetPrivilege_HDFC_Bank_Select_Credit_Card1542181556219.png" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 500</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 250 flight discount voucher on enrollment for flights booked on InterMiles.com </span></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <!--<p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>2 InterMiles per INR 150/- spent</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Fuel Convenience fee Waiver</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>4 InterMiles for every INR 150/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>5 InterMiles for every INR 150.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                
                                              
<!--                                                jet airways american express platinum credit card-->
                                                
<div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">Jet Airways American Express Platinum Credit Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="Jet Airways American Express Platinum Credit Card" src="https://cards.intermiles.com/cards/Jet_AirwaysAmerican_ExpressPlatinum_CreditCard1542182851505.png" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 5,000</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul>
                                                            <li>
                                                                <span style="color: #000000;"><strong>^Special Offer</strong></span>
                                                                <br>
                                                                <span class="cc-f98a7a" style="color: #000000;">INR 1000 and INR 750 flight discount voucher on enrolment and renewal for flights booked on InterMiles.com </span>
                                                            </li>
                                                            <li>
                                                         <span class="cc-f98a7a" style="color: #000000;"> 40% Miles back for International flights*</span>
                                                            </li>
                                                           <li>
                                                         <span class="cc-f98a7a" style="color: #000000;">InterMiles Gold Tier</span>
                                                            </li> 
                                                           
                                                        </ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
<!--                                                        <p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>8 InterMiles per INR 150/- spent</li></ul>
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                       <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Dining offers</li><li>Concierge service</li><li>5 Star hotel offers</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>16 InterMiles for every INR 150/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>20 InterMiles for every INR 150/- spent on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>10% discount on business and 5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li>
<!--                                                            <li>20 InterMiles for every INR 150.- on Etihad flights booked on flights.intermiles.com</li>
                                                            <li>10% discount on business and 5% discount on Etihad flights being booked on flights.intermiles.com -->
                                                         </li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                             
                    <!--jet privilege hdfc signature debit-->
                    
                    <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1">Jet Privilege HDFC Signature Debit Card </h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="JJet Privilege HDFC Signature Debit Card" src="https://cards.intermiles.com/cards/HDFC-Jet-Privilege-World-DI-Card_final-24-10-17-011519069130907.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="500">INR 500</span>
                                            </span>
                                            <div class="details_link">
                                                <!--<a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>-->
                                            </div>
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>-->
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 250 flight discount voucher on enrolment for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">15% Miles back for International flights* </span></li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
<!--                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>-->
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
<!--                                                        <p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>2 InterMiles per INR 150/- spent</li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>International and Domestic Lounge Access</li><li>Fuel Convenience fee Waiver</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>4 InterMiles for every INR 150/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>5 InterMiles for every INR 150.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li><li>5% discount Economy on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a> </li></ul>                                       
                                                    </div>
                                                 	                                  
                                            </div><div class="collps_img1 coldown">
<!--                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                     <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                
                    <!--Jet Airways ICCI Bank Business Advantage Card-->
                                                
<!--                                                <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1"> Jet Airways ICCI Bank Business Advantage Card</h2>
                                        </div>
                                        
                                        New adobe changes 16th 07 18 start
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="Jet Airways ICCI Bank Business Advantage Card" src="https://cards.intermiles.com/cards/ICICI-CARD-JET-CC_1-011519071467735.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 2,500</span>
                                            </span>
                                            <div class="details_link">
                                                <a href="javascript:void(0);" style=" text-decoration: underline;" data-id="1" data-name="Jet Airways American Express� Platinum Credit Card" data-utm_source="" data-utm_medium="" data-utm_campaign="" class="forMoreDetails" id="CrdHP_American Express&amp;Jet Airways American Express<sup>�</sup> Platinum Credit Card&amp;AMEX_details" onclick="moreDetailsClick('Jet Airways American Express� Platinum Credit Card','Jet Airways American Express� Platinum Credit Card')">For more details &gt;</a>
                                            </div>
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">        
                                                    <div class="label_joining"> <label>Joining Benefit</label></div>        
                                                </div>
                                        </div>
                                    </div>

                                    
                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 500 and INR 250 flight discount voucher on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">25% Miles back for International flights* </span></li></ul>                                       
                                                    </div>
                                                                                      
                                            </div>
                                            <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div><div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                                    
                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>
                                                        <ul><li>2.5 InterMiles per INR 100/- spent</li></ul>                                       
                                                    </div>
                                                                                      
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Dining offers</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>5 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>                                       
                                                    </div>
                                                                                      
                                            </div><div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
                                                <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>-->
                    

                                                
                                                <!--Jet Airways American Express Corporate Card-->
                    
                    
                        <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1"> Jet Airways American Express Corporate Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="Jet Airways American Express<sup>�</sup> Platinum Credit Card - JetPrivilege" src="https://cards.intermiles.com/cards/jet_card1519068605724.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 2,500</span>
                                            </span>
                                            <div class="details_link">
                                                 </div>
                                        </div>
                                    </div>

                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           

                                            <div class="list_cont">
                                                
                                                 
                                                    <div class="home_feature">
<!--                                                        <p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>8 InterMiles per INR 150/- spent</li></ul>                                       
                                                    </div>
                                                                                      
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Dining offers</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>5 InterMiles for every INR 150/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul> <p><strong>Bank Privileges</strong></p><ul><li>Concierge service</li><li>Insurance</li><li>Holistic Business Expense Management</li></ul>                                      
                                                    </div>
                                                                                      
                                            </div><div class="collps_img1 coldown">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>
                                                
                            <!--InterMiles ICICI Bank Business Advantage Card -->                     
                         <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1"> InterMiles ICICI Bank Business Advantage Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="Jet Airways American Express<sup>�</sup> Platinum Credit Card - JetPrivilege" src="https://cards.intermiles.com/cards/ICICI_Business_advantage_545_372.png" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 500</span>
                                            </span>
                                            <div class="details_link">
                                                 </div>
                                        </div>
                                    </div>

                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           

                                            <div class="list_cont">
                                                
                                                 <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed1">
                                                        <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 500 and INR 250 flight discount voucher on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">25% Miles back for International flights* </span></li></ul>                                       
                                                    </div>
                                                                                      
                                            </div>
                                                                                      
                                            </div>
                                        </div>
                                    
                                    
                                                        <div class="col-sm-3 webView">
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
<!--                                                        <p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>-->
                                                        <ul><li>2.5 InterMiles per INR 100/- spent</li></ul>                                       
                                                    </div>
                                                                                      
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Dining offers</li></ul><p><strong class="textleft">Airline Benefits </strong></p><ul><li>5 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>                                       
                                                    </div>
                                                                                      
                                            </div><div class="collps_img1 coldown">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">-->
                                                <img src="${applicationURL}static/images/co-brand/collps_down.png">

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <!--<img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">-->
                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">

                                            </div>
                                        </div>
                    
                                    
                                    
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div> 
                                                
                                    <!--IndusInd Odyssey Amex-->              
                                                
                   <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1"> InterMiles IndusInd Bank Odyssey Amex Credit Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="Jet Airways American Express<sup>�</sup> Platinum Credit Card - JetPrivilege" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/intermiles+card_Odyssey_amex_front.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 10,000</span>
                                            </span>
                                            <div class="details_link">
                                                 </div>
                                        </div>
                                    </div>

                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           

                                            <div class="list_cont">
                                                
<!--                                                <div class="home_feature trimmed1">
                                                    </div>
                                                -->
                                                 
                                                    <div class="home_feature" trimmed1>
                                                        
                                                                <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 750 and INR 500 flight discount vouchers on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">30% Miles back for International flights*</span></li><li><span style="color: #000000;">InterMiles Silver Tier </span></li>                                       
                                                
                                                        
                                                                <li><span class="cc-f98a7a" style="color: #000000;">4 InterMiles per INR 100/- spent on weekdays</span></li>                                      
                                                                <li><span class="cc-f98a7a" style="color: #000000;">6 InterMiles per INR 100/- spent on weekends</span></li>                                       
                                                    </ul>
                                                    </div>
                                                                                      
                                            </div>
                                        </div>
                                    
                                           <div class="col-sm-3 webView">
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>
                                                        <ul><li>2.5 InterMiles per INR 100/- spent</li></ul>                                       
                                                    </div>
                                                                                      
                                            </div>-->
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Concierge service</li><li>Wellness benefits</li></ul><p><strong class="textleft">Airline Benefits </strong></p>
                                                        <ul><li>  8 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com on weekdays</a></li></ul>
                                                        <ul><li>  12 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com on weekend</a></li></ul>
                                                        <ul><li>  10 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com on weekdays</a></li></ul>
                                                        <ul><li>  15 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com on weekdays</a></li></ul>
                                                        <ul><li>   10 % discount on Business and 5% discount on Economy class Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>
                                                                                              
                                                    </div>
                                                                                      
                                            </div>
                                             <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
<!--                                                <img src="${applicationURL}static/images/co-brand/collps_down.png">-->

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
<!--                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">-->

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>  
                                                
                           <div class="features margT2">
                            <div class="features_desc" id="card_row1">
                                <div class="row">
                                    <div class="col-sm-3 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName1"> InterMiles IndusInd Bank Voyage Amex Credit Card</h2>
                                        </div>
                                        
                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo"></div>

                                        <div class="card_img_holder">
                                            <span class="card_img">
                                                
                                                    
                                                        <img alt="Jet Airways American Express<sup>�</sup> Platinum Credit Card - JetPrivilege" src="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/cards/intermiles+card_Voyage_amex_front.jpg" height="50" width="50" id="card_img1">
                                                    
                                                    
                                                
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId1" class="fees_class" data-fees="5000">INR 2,000</span>
                                            </span>
                                            <div class="details_link">
                                                 </div>
                                        </div>
                                    </div>

                                        <div class="col-sm-3 webView">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
	                                            </div>
                                           

                                            <div class="list_cont">
                                                
                                                 <div class="home_feature" trimmed1>
                                                        
                                                                <ul><li><span style="color: #000000;"><strong>^Special Offer</strong></span></li><span class="cc-f98a7a" style="color: #000000;">INR 500 and INR 250 flight discount vouchers on enrolment and renewal for flights booked on InterMiles.com </span><li><span class="cc-f98a7a" style="color: #000000;">25% Miles back for International flights*</span></li><li><span style="color: #000000;">3 InterMiles per INR 100/- spent on weekdays </span></li>                                       
                                                
                                                        
                                                                <li><span class="cc-f98a7a" style="color: #000000;">4 InterMiles per INR 100/- spent on weekends</span></li>                                      
                                                                                               
                                                    </ul>
                                                    </div>                     
                                            </div>
                                        </div>
                                    
                                         <div class="col-sm-3 webView">
<!--                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>InterMiles Benefit on spends*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature">
                                                        <p class="benifit_text"><strong>InterMiles Benefit on spends*</strong></p>
                                                        <ul><li>2.5 InterMiles per INR 100/- spent</li></ul>                                       
                                                    </div>
                                                                                      
                                            </div>-->
                                        </div>
                                    
                                         <div class="col-sm-3 webView spacer">
                                             <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpno="1">		
	                                                <div class="label_joining"> <label>Lifestyle &amp; Airline Benefits*</label></div>		
	                                            </div>
                                            <div class="list_cont">
                                                 
                                                    <div class="home_feature trimmed">
                                                        <p><strong class="textleft">Lifestyle Benefits</strong></p><ul><li>Lounge access</li><li>Movie ticket offers</li><li>Dining offers</li><li>Concierge service</li><li>Wellness benefits</li></ul>
                                                        <ul><li>    6 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com on weekdays</a></li></ul>
                                                        <ul><li>   8 InterMiles for every INR 100/- spent on <a href="https://www.intermiles.com/flights">flights.intermiles.com on weekend</a></li></ul>
                                                        <ul><li>  7.5 InterMiles for every INR 100.- on Etihad flights booked on  <a href="https://www.intermiles.com/flights">flights.intermiles.com on weekdays</a></li></ul>
                                                        <ul><li>  10 InterMiles for every INR 100.- on Etihad flights booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com on weekdays</a></li></ul>
                                                        <ul><li>   5% discount on Etihad flights being booked on <a href="https://www.intermiles.com/flights">flights.intermiles.com</a></li></ul>
                                                                                              
                                                    </div>
                                                                                      
                                            </div>
                                             <div class="collps_img1 coldown">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_down.png">
<!--                                                <img src="${applicationURL}static/images/co-brand/collps_down.png">-->

                                            </div>
                                            <div class="collps_img1 colUp" style="display:none">
                                                <img src="https://cards.jetprivilege.com/static/images/co-brand/collps_up.png">
<!--                                                <img src="${applicationURL}/static/images/co-brand/collps_up.png">-->

                                            </div>
                                        </div>
                    

                                    <div class="clearfix"></div>
                                  
                                </div>
                            </div>
                        </div>                       
                                                
                                                
                                                
                                                
                                                
                                                
                    
                      

                    
                </div>   

                <div class="no_cards hidden">
                    Sorry, there is no card matching your selected criteria.
                    
                    
                </div>

                
            </div>
              <div class="terms_conditions">
                  <a class="creditfooter" style="width: 50%; text-align:left">Check Your Credit Score</a>
                  <a class="termsfooter"style="width: 50%; text-align:right" href="https://www.intermiles.com/terms-and-conditions/cards">  Terms & Conditions Apply</a>
                </div>
        </section>
         
    </section>


    <script>
        
    
    $(".collps_img1").click(function(){
        // var trimElement = $(this).parent().find(".list_cont").attr("class");
        var trimElement = $(this).parent().find(".list_cont").children();
        if(trimElement.hasClass("trimmed"))
        {
            trimElement.removeClass("trimmed");
            $(this).parent().find(".colUp").css("display","block");
            $(this).css("display","none");
        }
        else{
            trimElement.addClass("trimmed");
            $(this).parent().find(".coldown").css("display","block");
            $(this).css("display","none");
        }
        console.log("trimElement ---> ",trimElement);
    });
    
    //Adobe code start here
   
    var assistMeFlag = false;
    var name = "";
    var mobile = "";
    var email = "";
    var assistError="";
    var lastAccessedField="";
    
    name = $("#socialName").val();
    mobile = $("#socialPhone").val();
    email = $("#socialEmail").val();
    //Adobe code end
    var loggedInUser = '${sessionScope.loggedInUser}';
    //Adobe code filter usage functionality starts here
    function proceedClick() {
        var redirectBank_card = $("#popCardName").text();
        localStorage.setItem("CardName", redirectBank_card);
//         console.log("=====redirectBank_card======="+redirectBank_card)
		$.fn.partnerRedirectionCT();
        $.fn.IndusIndProceedClick();
    }

    $("#tellme").click(function () {
        var jpNum = $("#jpNumber").val();
        var popupName = "Earn with Partners: Swipe any of our Co-brand Cards";
        var formName = "Proceed to Bank";
        var abandonType = "Page Refresh";

        abandonFormName = formName;
        formAbandonType = abandonType;
        $.fn.AfterApplyJPNumberPopUpClickForHdfc(jpNum, popupName);

    });

    function moreDetailsClick(cardName, variant) {
        var cardName1 = cardName.replace("®", "");
        var variant1 = variant.replace("®", "");

        $.fn.MoreDetailsForCardsClick(cardName1, variant1);
    }

    //submit button assist me form(phase 6)
    function assistMeeClick() {
        assistMeFlag = true;
        $.fn.ClickOfAssistMe();
    }

    function assistFormSubmitButton(name, mobile, email) {
        if (name !== "" && mobile !== "") {
            name = "name:yes";
            mobile = "mobile:yes";
        } else {
            name = "name:no";
            mobile = "mobile:no";
        }
        if (email !== "") {
            email = "email:yes";
        } else {
            email = "email:no";
        }
        $.fn.ClickOfAssistContinueButton(name, mobile, email);
    }



    function adobeAmexClick(cardName, variant, bankName, formName, abandonType) {
        var cardName1 = cardName.replace("<sup>®</sup>", "");
        var variant1 = variant.replace("<sup>®</sup>", "");

        IsButtonClicked = true;
        popupAbandonType = formName;
        abandonFormName = formName;
        formAbandonType = abandonType;
        $.fn.ApplyButtonAmexClick(cardName1, variant1, bankName);
    }

    function adobeAmexAmericanClick(cardName, variant, bankName, formName, abandonType) {
        var cardName1 = cardName.replace("<sup>®</sup>", "");
        var variant1 = variant.replace("<sup>®</sup>", "");

        IsButtonClicked = false;
        abandonFormName = formName;
        formAbandonType = abandonType;
        $.fn.ApplyButtonAmexClick(cardName1, variant1, bankName);
    }

    function afterEnrollHereClick(enrollName, popupType) {
        abandonFormName = popupType;
        popupAbandonType = popupType;
        $.fn.AfterApplyEnrolPopUpClick(enrollName);
        $.fn.AfterApplyEnrolPopUpClickCT();
    }

    function successEnrolClick() {
        var jpNum = $("#jpNumber").val();
        $.fn.successfulQuickEnrollmentOnHdfc(jpNum);

    }

    function noExistingCardHolderClick(existingName, popupType) {
        popupAbandonType = popupType;
        $.fn.existingCreditCardHolderClick(existingName);
    }

    function upgradeOtherCardsClickforNo(upgradeType, popupType) {
        popupAbandonType = popupType;
        $.fn.upgradeOrViewOtherCardsClick(upgradeType);
    }

    function upgradeOtherCardsClickforYes(upgradeType) {
        IsButtonClicked = false;
        $.fn.upgradeOrViewOtherCardsClick(upgradeType);

    }

    function enrollHereBackClick(enrollName, formName) {
        abandonFormName = formName;
        $.fn.EnrolHereBackClick(enrollName);
    }

    function formAbandonmentOnPopUp(closeType) {
        $.fn.formAbandonmentOnSubmitJPnumberPopup(closeType, popupAbandonType);
    }


    function isInArray(array, value)
    {
        return array.indexOf(value) >= 0;

    }

    function  getFinalFilters()
    {
        var lBankNames = "";
        var lLifeStyleBenefits = "";
        var lJoiningFees = "";
        var lFinalFilters = "";
        var joiningFeesAdobe = $(".slider_div .rangeslider__handle").html().replace(/<\/?span[^>]*>/g, "").replace(/\,/g, '');


        if (bankNamesAdobe.length > 0)
            for (i = 0; i < bankNamesAdobe.length; i++) {
                lBankNames = lBankNames + "Bank" + ":" + bankNamesAdobe[i] + "|";
            }


        if (joiningFeesAdobe.length > 0)
            lJoiningFees = "Joining Fees" + ":" + joiningFeesAdobe + "|";

        if (lifestyleBenefitsAdobe.length > 0)
            for (i = 0; i < lifestyleBenefitsAdobe.length; i++) {

                if (i == lifestyleBenefitsAdobe.length - 1)
                {
                    lLifeStyleBenefits = lLifeStyleBenefits + "LifeStyle Benefits" + ":" + lifestyleBenefitsAdobe[i];

                } else
                {
                    lLifeStyleBenefits = lLifeStyleBenefits + "LifeStyle Benefits" + ":" + lifestyleBenefitsAdobe[i] + "|";
                }

            }
        lFinalFilters = lBankNames + lJoiningFees + lLifeStyleBenefits;

        return lFinalFilters;

    }

    function submitFilterAdobe() {

        var lFinalFilters = getFinalFilters();
        //   console.log("final filter is here"+lFinalFilters) 
        $.fn.FilterUsage(lFinalFilters, sortByAdobe);
    }


    //Call to load of all cards
    function loadOfCardsAdobe() {
        var lFinalFilters = getFinalFilters();
        $(".cardinfo").each(function () {
            cardNamesAdobe = cardNamesAdobe + $(this).text() + "|"

        });
        cardsCountAdobe = $(".cardinfo").length;
        $.fn.LoadOfAllCards(cardNamesAdobe, cardsCountAdobe, lFinalFilters, sortByAdobe);
    }



                      




    //Adobe page bottom function call
    function pageInfo()
    {
        //Adobe code phase 6 start
        var callMeFlagAssist = '${callMeFlag}';
        if (callMeFlagAssist == 1) {
            setTimeout(function ()
            {
                $.fn.ViewOfAssistMe()
            }, 3000);
        }
        //Adobe code phase 6 ends

        digitalData.pageInfo = {"pageName": "CardsHome", "category": "Cards", "subCategory1": "Home", "subCategory2": "", "partner": "JP", "currency": "INR"}
        console.log('Home Page PageInfo is Called...........................');
        // Code to assign JPNumber and DOB dynamically
        $('#jpNumber').val(loggedInUser);
        $('#hjpNumber').val(loggedInUser);
        $('#date_of_birth').val(loggedInUserDob);
        $('#hdate_of_birth').val(loggedInUserDob);

        var popupName = "";
        var lastAccessedField = "";

        $('form input, form select').click(function () {
            lastAccessedField = $(this).attr('name');
        });

        $('#userDetails').on('hidden.bs.modal', function () {
            digitalData.pageInfo.pageName = "CardsHome";
            digitalData.pageInfo.category = "Cards";
            digitalData.pageInfo.subCategory1 = "Home";
            digitalData.pageInfo.subCategory2 = "";
        });

        // Code to assign JPNumber and DOB dynamically(Milestone 1 changes)
        $('#jpNumber').val(loggedInUser);
        $('#hjpNumber').val(loggedInUser);
        $('#date_of_birth').val(loggedInUserDob);
        $('#hdate_of_birth').val(loggedInUserDob);

    }

    function formAbandonOfAssistMe() {
        var abandonType = "pop-up close";
        if (lastAccessedField == "socialName") {
            lastAccessedField = "Name";
        } else if (lastAccessedField == "socialPhone") {
            lastAccessedField = "Mobile";
        } else if (lastAccessedField == "socialEmail") {
            lastAccessedField = "Email";
        }
        $.fn.FormAbandonmentOfAssistMe(name, email, mobile, lastAccessedField, abandonType);
    }



    var checkCloseX = 0;
    $(document).mousemove(function (e) {
        if (e.pageY <= 5) {
            checkCloseX = 1;
        } else
        {
            checkCloseX = 0;
            formAbandonType = "Page Refresh";
        }
    });

    window.onbeforeunload = function (event) {

        if (checkCloseX === 1) {
            formAbandonType = "Browser close";
        }

        if (IsButtonClicked)
        {
            console.log("--------formAbandonType is-----------" + formAbandonType);
            $.fn.formAbandonmentOnSubmitJPnumberPopup(formAbandonType, abandonFormName);
        }

        if (assistMeFlag === true)
        {
            $.fn.FormAbandonmentOfAssistMe(name, mobile, email, lastAccessedField, "page refresh");
        }

    };
    //Form Abandonment close
    
    </script>
   