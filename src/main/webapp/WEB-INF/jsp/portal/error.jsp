<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<c:set var="contextPath" value="${pageContext.servletContext.contextPath}" />
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <title>Error</title>
         <link rel="stylesheet" href="${contextPath}/static/css/error.css" />
         <link rel="stylesheet" href="${contextPath}/static/css/style.css?version=1.0.3" />
         <link rel="stylesheet" href="${contextPath}/static/css/co-branded_responsive.css" />
         
<!--Adobe code starts here-->

<link href="${contextPath}/static/images/favicon.ico.jpg" rel="shortcut icon" type="image/ico">
 	<script>
 		var ctx = "${contextPath}/";
//                console.log("==============>"+ctx);
	</script>
 <script src="${contextPath}/static/js/integrated.js"></script>
<link rel='stylesheet' href='${contextPath}/static/css/integrated.css' />
<script type="text/javascript" src="${contextPath}/static/js/Adobe/AdobeFramework.js?version=1.0.4"></script>
<%-- <script type="text/javascript" src="${contextPath}/static/js/CleverTap/CleverTapFramework.js"></script> --%>
<script type="text/javascript" src="${applicationURL}static/js/CleverTap/CleverTapFramework.js?cver=1.0.2"></script>
<script type="text/javascript" src="${contextPath}/static/js/Adobe/Fingureprint5.js"></script>
<!--Adobe code ends here-->
         
<script src="${contextPath}/static/js/jquery-1.10.1.min.js"></script>  
</head>
    <!--Adobe code starts here-->
    <script src=<spring:message code="application.adobe.url"/>></script>
    <!--Adobe code ends here-->
    
   
    <body class="error_bg">
        <section class="clearfix" style="width: 100%;">
            <a class="errorLogo">InterMiles</a>
            <div class="errorMsgContainer">
             <div><span>404</span><span class="page_not_found">Page not found</span></div>
                <p>Ooops...Looks like the page you are looking for doesn't exist</p>
                <div class="center">
                    <a href="<spring:message code="application.cbc.application_URL"/>"><input id="goToHome" value="Go to the Home Page" class="btnblue1" style="margin-top: 40px;" type="button"></a>
                </div>
            </div>
        </section>
                
       <!--Adobe code starts here-->
       <script src="${applicationURL}initDigitalData"></script>
       <script>
        var PID="${sessionScope.PID}";
       </script>
       <script type="text/javascript">
	var user = "${sessionScope.loggedInUser}";
        var loggedUser = loggedInUser;
        
//	console.log("Logged-In User is:" + loggedUser)
       </script>
       <script type="text/javascript" >
       $(document).ready(function(){
//                                         Fingerprint implementation Start
                                       if(user === ''){
                                           SendFPData('');
                                       }   
                                       else{
                                            SendFPData(loggedUser); 
                                       } 
//                                       Fingerprint implementation end
                     digitalData.userInfo.sessionID = JSESSIONID;
                     digitalData.userInfo.sessionDateTime = JSESSIONTIME; 
                     digitalData.userInfo.firstloginDate = FIRSTLOGINDATE;
                     digitalData.userInfo.lastloginDate = LASTLOGINDATE;
        digitalData.eventInfo = {"eventName":"Error","eventType":"click"}
        digitalData.errorInfo = {"errorDescription":"Page not found:Ooops...Looks like the page you are looking for doesnot exist","errorCode":"404"}
        _satellite.track("error");
//        console.log(digitalData);
       });
      </script>  
     <!--Adobe code ends here-->
       
    </body>
</html>