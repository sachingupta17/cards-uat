<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

         <section class="container clearfix bottomPadding">
               <section class="main-section">
                  <div class="msgWrapper text-center">
                  <div class="message_header"><h1>Sorry!</h1></div>
                     <div class="msgDenied_div margT5">
                        <h1>${Statustitle}</h1>
                        <p> ${messagecontent} </p>
                     </div>
                     
                     <div>
                      <a href="${sessionScope.appurl}"><button class="buttonBlue_rec margT5" id="CrdApp_btn_view-other-cards">View Other Cards</button></a>
                     </div>
                  </div>
               </section>
            </section>
    <!--Adobe code starts here-->                 
    <script type="text/javascript"> 
                      
                      
             //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----decline--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----decline--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----decline--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----decline--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----decline--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----decline--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----decline--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----decline-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----decline--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End         
                      
                      
       function pageInfo(){
        var url='${sessionScope.appurl}';
        digitalData.pageInfo={"pageName":"Decline","category":"Cards","subCategory1":"App-Status","subCategory2":"","partner":"JP","currency":"INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
        digitalData.formInfo={"formName":"Amex card application form decline","formNumber":""}
        $.fn.ErrorTracking('Page not found:Ooops...Looks like the page you are looking for doesnot exist','404');
        }
        
     </script> 
    <!--Adobe code ends here--> 