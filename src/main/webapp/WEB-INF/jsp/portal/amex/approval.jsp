<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

 <section class="container clearfix bottomPadding">
               <section class="main-section">
                  <div class="msgWrapper text-center">
                  <div class="message_header"><h1>Congratulations!</h1></div>
                     <div class="msgApproved_div margT5">
                        <h1>${Statustitle}</h1>
                        <p>${messagecontent}</p>
                     </div>                     
                     <div class="margT5 otherCards">
                       
                     </div>
                  </div>
               </section>
            </section>
<script type="text/javascript">
$(document).ready(function($) {

    if (window.history && window.history.pushState) {

      $(window).on('popstate', function() {
        var hashLocation = location.hash;
        var hashSplit = hashLocation.split("#!/");
        var hashName = hashSplit[1];

        if (hashName !== '') {
          var hash = window.location.hash;
          if (hash === '') {
           
              window.location='home';
          
              return false;
          }
        }
      });

      window.history.pushState('forward', null,  window.location.href);
    }

  });
  
        //Adobe code starts here
        
        //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----approval--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----approval--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----approval--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----approval--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----approval--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----approval--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----approval--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----approval-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----approval--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End
        
        
        
       function pageInfo(){
           digitalData.pageInfo={"pageName":"Amex-Platinum_Approved","category":"Cards","subCategory1":"App Status - Req Approved","subCategory2":"Amex-Platinum","partner":"JP","currency":"INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}           
           var pcnNumber = '${pcnNumber}';
           $.fn.LoadOfThankUPage(pcnNumber,'Amex');
        }
       //Adobe code ends here
</script>
<script>
	ewt.cot({action:'Lead',detail: 'Form Name' ,amount:'1.00'});
</script>