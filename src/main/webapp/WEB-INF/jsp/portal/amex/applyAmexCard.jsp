<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<div class="modal fade" id="offer_jp_popup" tabindex="-1" role="dialog" style="padding-left: -1px;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="proceedPopup_close pull-right">
                    <a href="applyAmexCard.jsp"></a>
                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="formAbandonmentOnPopUp('Pop-up close')">
                </div>
                <div class="jpNum_popup">
                    <div class="text-center jpPopup_header">
                        Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-brand Card. Apply here
                    </div>
                    <div class="margT2 colorDarkBlue text-center">
                        <h2 class="heading_txt">Provide InterMiles No.</h2>
                    </div>
                    <div class="row">
                        <div class="jpNumber_div margT2">
                            <div class="jpNumber_input">
                                <input type="number" placeholder="InterMiles No." id="mini_jpNumber" maxlength="9">
                            </div>
                            <div id="mini_error_msg" class="mini_error_msg hidden" style="padding: 10px 0; font-size: 12px; color: red;font-weight:bold;"></div>
                        </div>
                    </div>
                    <div class="text-center margT10">
                        <input class="button_thickBlue" type="button" value="Submit" id="mini_jpNumber_submit">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--<div id="social-profiles" data-toggle="modal"  class="hidden" onclick="assistMeeClick()">  
    <img src="${applicationURL}static/images/footer/phone-call.png" width="30" height="30">
    <h5>Assist Me</h5>
</div> -->
<div class="modal fade" id="CompletedForm" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="sizer modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">
                <p class="modaltext" style="color:#01a200; text-align:center; font-size: 18px; ">This form has been already submitted.</p>


            </div>
            <div class="modal-footer">
                <a href="${applicationURL}">  <input class="button_thickBlue" type="button" id="completedBtn" value="OK"></a>
            </div>

        </div>

    </div>
</div>
<form:form autocomplete="off" id="callMe" commandName="callMeBean">
    <div class="modal fade" id="callMe_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div id="callmeclose" class="proceedPopup_close pull-right">
                        <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="formAbandonOfAssistMe()">
                    </div>
                    <div class="callme_popup">  
                        <div class="text-center">
                            Fill in your details for us to call you & help you complete the credit card application
                        </div>
                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Tell us about yourself</h2>
                        </div>
                        <div class="error_box1 hidden">
                            <ul class="error_list1"></ul>
                        </div>
                        <div class="row">
                            <div class="enroll_div margT2">
                                <div class="middleName_input user_details">  
                                    <form:input id="socialName" path="socialName"  placeholder="Full Name" type="text" value="" />
                                </div>
                                <div class="phone_input user_details">
                                    <form:input id="socialPhone" path="socialPhone"  maxlength="10" placeholder="Phone" type="number"  onKeyPress="if(this.value.length==10) return false;" value="" />
                                </div>
                                <div class="email_input user_details">
                                    <form:input id="socialEmail" path="socialEmail"  placeholder="Email" type="text" value="" />
                                </div>

                                <!--                                <div class="jpNumber_input user_details">
                                <%--<form:input id="socialjpNumber" path="socialjpNumber"  placeholder="JetPrivilege Number" type="number" value="" maxlength="9" onKeyPress="if(this.value.length==9) return false;"/>--%>
                            </div>-->
                            </div>
                            <form:hidden path="pageName" id="pageName"/>
                            <form:hidden path="cardName" id="cardName"/>
                            <form:hidden path="formNumber" id="formNumber"/>
                        </div>
                        <div id="loader" class="text_center hidden margT5">
                            <img src="${applicationURL}static/images/header/ajax-loader.gif" alt="Page is loading, please wait." />
                        </div>
                        <div class="text-center margT10">
                            <input class="button_thickBlue" type="button" id="callMeContine" value="Submit">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form:form>
<!--sprint 54-->
<div class="modal fade" id="successCallmeModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="sizer modal-content">
            <div class="modal-header">
                <!--              <div class="proceedPopup_close pull-right">
                                        <img src="$ {applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.CloseOnPopUpClick('Enroll Now')">
                                    </div>-->

            </div>
            <div class="modal-body">
                <p class="modaltext" style="color:#01a200; text-align:center; font-size: 18px; ">Thank you for sharing your details! Our representative will reach out to you for further assistance.</p>


            </div>
            <div class="modal-footer">
                <button type="button" class="successcallbut" data-dismiss="modal">OK</button>
            </div>

        </div>

    </div>
</div>

<div class="modal fade" id="spends_slider" tabindex="-1" role="dialog">

    <div class="modal-dialog vertical-align-center">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body calculatemiles">   
                <div class="proceedPopup_close pull-right">
                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.CloseOnPopUpClick('Calculate your free flight')">
                </div>             
                <div class="spendsSlider_popup margT5">
                    <div class="margT2 colorDarkBlue text-center">
                        <h2 class="heading_txt">Calculate your Free Flights based on your monthly spends</h2>
                    </div>
                    <div style="padding-top: 10px; text-align: center;" class="spendsVal_error">
                        <span id="error_msg" style="font-size: 14px; color: red; display:none;"></span>
                    </div>
                    <div class="extest">
                        <div class="col-md-12">
                            <p class="amtvalue1">Annual Spends : INR <span class="slider-output" id="yearly_bill1">0</span></p>

                        </div>
                        <div class="col-md-12">
                            <div class="range-example-modal"></div>
                            <div class="imagebar1"><img src="${applicationURL}static/images/co-brand/numbar.png"/></div>

                        </div>

                        <div class="col-md-12">
                            <div class="infodown">
                                <div class="aheadiv">INR</div>
                                <input id="unranged-value2" type="text" inputmode="numeric" name="amountInput2"  value="0" maxlength="9"  />
                                <div class="donebut">Done</div>
                            </div>  
                            <div class="slider_modal_div_error"></div>
                        </div>

                    </div>
                    <!--                    <div class="margT2 colorDarkBlue text-center">
                                            <h2 class="heading_txt">Calculate your Free Flights based on your monthly spends</h2>
                                        </div>
                                        <div class="slider_valu margB5">
                                            <div style="display:inline"><span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5 Thousand</span></div>
                                            <div style="display:inline; float:right;"><span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span></div>
                                        </div>
                                        <div class="row">
                    
                                            <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal">										   									   
                                        </div>-->
                </div>
                <!--                <div class="annualSpends margT5 text-center">
                                    <span>Annual Spends: </span><span class="annualSpends_value">0</span>
                                </div>	
                                <div class="text-center margT2">
                                    <input class="button_red annual_spends" type="button" value="Done">
                                </div>-->

            </div>
        </div>
    </div>
</div>

<form:form autocomplete="off" id="quickEnrollUser" action="enrollme" commandName="enrollBean">
    <form:hidden path="enrollbpNo"/>
    <div class="modal fade" id="provide_jpNumber" tabindex=-1 role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="proceedPopup_close pull-right">
                        <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="formAbandonmentOnPopUp('Pop-up close')">
                    </div>
                    <div class="jpNum_popup">
                        <div class="text-center jpPopup_header">
                            Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-brand Card. Apply here
                        </div>

                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Provide InterMiles No. </h2>
                        </div>

                        <div class="row">
                            <div class="jpNumber_div margT2">
                                <div class="inputwidthinfo">

                                    <div class="jpNumber_input takeallwidth">


                                        <form:input  type="number" path="jpNumber" id="jpNumber" placeholder="InterMiles No." maxlength="9" pattern="[0-9]*" onKeyPress="if(this.value.length==9) return false;"/>
                                    </div>
                                    <div class="promoinfo hidden">
                                        <a class="tooltips" href="javascript:void(0);"><img src="${applicationURL}static/images/co-brand/whiteicon.png">
                                            <span class="tooltip_content amextooltip amexheightwidth"><span class="pull-right close_toolTip"><img src="${applicationURL}static/images/co-brand/close_btn.png" alt=""></span> 
                                                Promocode has been applied for the given InterMiles No.
                                        </a>
                                    </div>
                                </div>
                                <div class="jpNumber_input margT2 hidden">
                                    <form:hidden path="jpTier" id="jpTier" placeholder="InterMiles Membership Tier" disabled="true" pattern="[0-9]*" />
                                    <input type="hidden" id="otbpNo"/>
                                </div>

                                <div id="error_msg" class="error_msg" style="padding: 10px 0; font-size: 13px; color: orangered"></div>
                                <%--<c:if test="${empty sessionScope.loggedInUser}">--%>
                                <c:if test="${empty beJpNumber}">
<!--                                <div class="text-center margT2">
                                    If you are not a InterMiles No. , <a href="javascript:void(0)" class="enroll_here"  id="quickEnroll" style="text-decoration: underline;" onclick="afterEnrollHereClick('Enroll here', 'co-brand quick enrollment')">please enroll here</a>
                                </div>-->
                                </c:if>
                            </div>

                            <div class="genotp">
                                <button  id="otpButton" class="genbut"></button><form:input class="geninput" path="otpVal" id="otpValue" maxlength="6" type="number" onKeyPress="if(this.value.length==6) return false;" />  
                                <!--added error div for otp input-->
                                <div id="error_msg_otp" class="error_msg_otp"  style="padding: 10px 0; font-size: 12px; color: red;"></div>

                                <!--<button  id=" regenerateotp" class="genbut">Re-Generate OTP</button>-->


                            </div>

                            <div class="otpsuccess" id="otpSucces">
                                <p></p>
                            </div>
                            <div class="otpactive" id="otpactive">
                            <p></p>
                            </div>


                        </div>
                        <div class="text-center margT10">
                            <input class="button_thickBlue" type="button" id="jpNumpopup" onClick="return ewt.trackLink({name: 'Amex_Submit_Button', type: 'click', link: this});" value="Submit">
                        </div>
                    </div>
                    <div class="enroll_popup hidden">
                        <div class="text-center">
                            Enroll into the InterMiles Programme now to start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-Brand Card.
                        </div>
                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Enroll Now</h2>
                        </div>
                        <div class="error_box2 hidden">
                            <ul class="error_list2">
                            </ul>
                        </div> 
                        <div class="row">
                            <div class="pull-left margT2 nav_web">
                                <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_slide go_To_Jp" onclick="$.fn.formAbandonmentOnSubmitJPnumberPopup('pop-up back', 'co-brand quick enrollment')">
                            </div>
                            <div class="enroll_div margT2">
                                <div class="title user_details">
                                    <form:select path="enrollTitle" id="enrollTitle" placeholder="Title" class="title_select">
                                        <form:option value="">Select Title</form:option>
                                        <form:option value="Mr">Mr</form:option>
                                        <form:option value="Ms">Ms</form:option>
                                        <form:option value="Mrs">Mrs</form:option>
                                        <form:option value="Dr">Doctor</form:option>
                                        <form:option value="Prof">Professor</form:option>
                                        <form:option value="Captain">Captain</form:option>
                                    </form:select>
                                </div>
                                <div class="gender hidden">
                                    <c:forEach items="${genderStatus}" var="status">
                                        <div style="display:inline-block">
                                            <label class="radio-inline">
                                                <form:radiobutton
                                                    path="enrollGender" name="enrollgender"
                                                    id="enrollGender"
                                                    value="${status.key}" /> </label>
                                            <label class="gender_desc" for="${status.key}" >${status.value}</label>
                                        </div>
                                    </c:forEach>
                                </div>
                                <div class="firstName_input user_details">
                                    <form:input type="text" path="enrollFname" id="enrollFname" placeholder="First Name"/>
                                </div>
                                <div class="middleName_input user_details">
                                    <form:input type="text" path="enrollMname" id="enrollMname" placeholder="Middle Name"/>
                                </div>
                                <div class="lastName_input user_details">
                                    <form:input type="text" path="enrollLname" id="enrollLname" placeholder="Last Name"/>
                                </div>
                                <div class="city_input user_details">
                                    <form:input type="text" path="enrollCity" id="enrollCity" placeholder="City"/>
                                </div>
                                <div class="phone_input user_details">
                                    <form:input path="enrollPhone" id="enrollPhone" maxlength="10" placeholder="Phone" type="number" onKeyPress="if(this.value.length==10) return false;"/>
                                </div>
                                <div class="email_input user_details">
                                    <form:input path="enrollemail" id="enrollemail" placeholder="Email"/>
                                </div>

                                <div class="dob_input user_details">
                                    <form:input path="enrollDob" id="enrollDob" class="datepicker" placeholder="Date Of Birth"/>
                                </div>
                                <div class="g-recaptcha recaptchaarea" id="test"
                                     data-sitekey=<spring:message code="google.recaptcha.site"/>  id="captch"></div>
                                <div> 
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="termsAndConditionsEnroll" class="margT1 statement termscheckbox" path="termsAndConditionsEnroll" /> 
                                        <span>I agree to the InterMiles membership 
                                            <a href="https://www.jetprivilege.com/terms-and-conditions/jetprivilege"  target="_blank">
                                                Terms and Conditions</a><span style="margin: 0px 4px;display: inline;">and</span> <a href="https://www.jetprivilege.com/disclaimer-policy"  target="_blank">
                                                Privacy Policy</a></span>
                                    </div>
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="recieveMarketingTeam" class="margT1 statement termscheckbox" path="recieveMarketingTeam" />  
                                        <span> Yes, I would like to receive Marketing Communication


                                        </span>
                                    </div>
                                </div>
                                <div class="error_box_enrolStatus hidden" id="enroll_error">
                                </div>
                            </div>
                        </div>
                        <div id="loader" class="text_center hidden margT5">
                            <img src="${applicationURL}static/images/header/ajax-loader.gif" alt="Page is loading, please wait." />
                        </div>
                        <div class="text-center margT10 nav_web">
                            <input class="button_thickBlue" type="button"  id="enrollForm1" value="Continue">
                        </div>
                        <div class="nav_arrow_mobile margT10">
                            <div class="left_nav">
                                <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_arrow go_To_Jp">
                            </div>
                            <div class="continue_button">
                                <input class="button_thickBlue" type="button" id="enrollForm_mob_view1" value="Continue">
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <div class="loaddiv"><div class="loaderamex" id="sad" style="/* margin:auto; */"> </div></div>
    </div>
</form:form>
<form:form autocomplete="off" id="amexBean" commandName="amexBean">
    <form:hidden path="cpNo"/>
    <form:hidden path="offerId"/>
    <form:hidden path="offerDesc"/>
    <form:hidden path="hstate"/>
    <form:hidden path="hcity"/>
    <form:hidden path="hstate1"/>
    <form:hidden path="hcity1"/>
    <form:hidden path="hstate2"/>
    <form:hidden path="hcity2"/>

    <input type="hidden" value="${cardBean.gmNo}" id="gmNo"/>
    <section class="container bottom_padding">
        <div class="cont_area">  <h1>
                ${cardBean.cardName} - Apply Form
            </h1></div>
        <div class="features margT2">
            <div class="features_title benefits_web">
                <div class="row">

                    <div class="col-sm-2">
                        <p>Cards</p>
                    </div>
                    <c:forEach items="${groupHeadings}" var="heading">
                        <div class="col-sm-2">
                            <p>${heading}</p>
                        </div>
                    </c:forEach>
                    <div class="col-sm-2 ">
                        <div class="">
                            <p class="freeFlight_spends">Free flight basis
                                <%--                                     <img src="${applicationURL}static/images/co-brand/filter_dropdown.png" class="margL10 filter_icon">  --%>
                            </p>
                            <div class="freeFlight_spends_calc parent_div hidden">
                                <div class="arrow-up"></div>
                                <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                                <div class="slider_valu margB5 margT5">
                                    <div style="display:inline; text-align:left !important;">INR 5,000</div>
                                    <div style="display:inline; float:right;"> INR 15 Lakhs</div>
                                </div>
                                <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal">
                                <div class="annualSpends margT2 text-center">
                                    <span>Annual Spends: </span><span class="annualSpends_value">60,000</span>
                                </div>
                                <div class="text-center">
                                    <input type="button" class="filter_button button_red" id="doneButton" value="Done">
                                </div>
                            </div>
                            <p>spends
                                <a class="tooltips" href="javascript:void(0);"><img src="${applicationURL}static/images/co-brand/inrimg.png">
                                    <span class="tooltip_content"><span class="pull-right close_toolTip"><img src="${applicationURL}static/images/co-brand/close_btn.png" alt=""></span> 
                                        The free flights calculation basis spends is calculated on the InterMiles earned basis the spends & on the minimum award flight redemption of 5000 InterMiles. 
                                        <br><span class="terms_conditions" style="color: black; text-decoration: underline;" id="tcLink">Terms & Conditions Apply</span>
                                    </span> </a>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-2 noBorder offer_header">
                        <!--<div class="col-sm-2">-->

                        <!--<p class="hide" >Offer Details</p>-->
                        <p>Offer Details</p>
                        <!--</div>-->
                    </div>
                </div>
            </div>
            <div class="features_desc">
                <div class="row">
                    <div class="col-sm-2 pad0">
                        <div class="card_name">
                            <h2>${cardBean.cardName}</h2>
                        </div>
                        <div class="card_img_holder">
                            <span class="card_img"> <img src="${applicationURL}cards/${cardBean.cardImagePath}"/></span>
                            <span class="card_value">
                                <label>Fees: </label>
                                <span>INR ${cardBean.fees}</span>
                            </span>
                            <div class="details_link" >
                                <a href="javascript:void(0);" style="color: #28262c; text-decoration: underline;" data-id="${cardBean.cpNo}" data-name="${cardBean.imageText}" data-utm_source="${amexBean.utmSource}"
                                   data-utm_medium="${amexBean.utmMedium}" data-utm_campaign="${amexBean.utmCampaign}" class="forMoreDetails" onclick="moreDetailsClick('${cardBean.imageText}', '${cardBean.imageText}')">For more details ></a>
                            </div>
                        </div>
                    </div>
                    <c:forEach items="${cardBean.featureList}" var="feature">
                        <div class="col-sm-2 webView">
                            <div class="list_cont">
                                <c:if test="${not empty feature}">
                                    <div class="home_feature">
                                        ${feature}
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </c:forEach> 

                    <div class="col-sm-2 ">
                        <div class="free_flights_desc">
                            <p onclick="$.fn.actionClickCT('Calculate your free flights','${cdList.cardName}','${cdList.bankName}');">Calculate your free flights</p>
                        </div>
                        <div class="free_flights_div hidden">
                            <div class="free_flights">
                                <p>You get</p>
                                <p class="ff" id="freeFlights${cardBean.cpNo}">XX</p>
                                <p>Free Flights</p>
                                <p class="gg" id="jpMiles${cardBean.cpNo}">on earning<br>XXXX JPamexBeanMiles</p>
                            </div>
                        </div>
                        <div class="lifestyle_benefits" data-toggle="modal" data-target="#benefits" data-cpNo="${cardBean.cpNo}">
                            <div class="benifits-img-holder"> <img src="${applicationURL}static/images/co-brand/benifits_icon.png"></div>
                            <label>Benefits</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-2 webView mob_view">
                        <c:choose>
                            <c:when test="${not empty cardBean.offerDesc}">
                                <div class="list_cont">
                                    <div class="home_feature login_offer">
                                        ${cardBean.offerDesc}
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="offer_link" data-toggle="modal" data-target="#offer_jp_popup">
                                    <a href="javascript:void(0)" class="view_offer_link cc-1371ba">View Offer</a>
                                </div>
                                <div class="list_cont">
                                    <div class="home_feature offer_display hidden"></div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </div>

        <div class="about">
            <div class="cont_area">
                <h1>Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-Brand Card. Apply here.</h1>
            </div>
            <div class="colorDarkBlue">
                <h2 class="heading_txt">Basic Profile</h2>
                <%-- <div class="heading_txt appNum_div margR2" style="display: none">
        <span class="margT1">Application Number : </span>
        <span class="appNum"><c:out value="${formNumber}"></c:out></span>
</div> --%>
            </div>
            <c:if test="${not empty errorMsg}">
                <div id="errorMsg" style="color: black;" class="error_box">
                    <c:out value="${errorMsg}"></c:out>
                    </div>
            </c:if>
            <div class="margT1">
                <div class="row">
                    <c:choose>
                        <c:when test="${promocodeFlag eq 1}">
                            <div id="namefield" class="col-sm-8">   
                                <div class="margT1 name_div" id="div_hide">	
                                    <label class="margT1">Name<span class="mandatory-field">*</span></label>	
                                    <div class="fullName" id="div_hide1">		  					
                                        <form:input class="row margT1 input name_inputBox" id="fullName" path = "fullName"  placeholder="Full Name"/>
                                    </div>
                                    <ul class="name_ul margT1 hidden"  id="div_hide2">   
                                        <li><div class="fName"  id="div_hide3"><span>First Name<span class="mandatory-field">*</span></span>
                                                <form:input	class="firstName"  path="fname"  name="FNAME" />
                                            </div>
                                        </li>
                                        <li><div class="mName"><span>Middle Name</span>
                                                <form:input	class="middleName"  path="mname" name="MNAME"/>
                                            </div>
                                        </li>
                                        <li><div class="lName"><span>Last Name<span class="mandatory-field">*</span></span>
                                                <form:input	class="lastName"  path="lname" name="LNAME"/>
                                            </div>
                                        </li>
                                    </ul> 
                                    <!-- 						  </div> -->
                                </div>
                                <div class="fullName-error-div hidden" style="color:red">
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div id="namefield">   
                                <div class="margT1 name_div" id="div_hide">	
                                    <label class="margT1">Name<span class="mandatory-field">*</span></label>	
                                    <div class="fullName" id="div_hide1">		  					
                                        <form:input class="row margT1 input name_inputBox" id="fullName" path = "fullName"  placeholder="Full Name"/>
                                    </div>
                                    <ul class="name_ul margT1 hidden"  id="div_hide2">   
                                        <li><div class="fName"  id="div_hide3"><span>First Name<span class="mandatory-field">*</span></span>
                                                <form:input	class="firstName"  path="fname"  name="FNAME" />
                                            </div>
                                        </li>
                                        <li><div class="mName"><span>Middle Name</span>
                                                <form:input	class="middleName"  path="mname" name="MNAME"/>
                                            </div>
                                        </li>
                                        <li><div class="lName"><span>Last Name<span class="mandatory-field">*</span></span>
                                                <form:input	class="lastName"  path="lname" name="LNAME"/>
                                            </div>
                                        </li>
                                    </ul> 
                                    <!-- 						  </div> -->
                                </div>
                                <div class="fullName-error-div hidden" style="color:red">
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${promocodeFlag eq 1}">

                        <div class="col-sm-4 margT1" id="promo">
                            <div  class="margT1">
                                <label class="margT1">Promo Code</label>
                            </div>
                            <div>
                                <form:input	class="row margT1 input" id="promocode" placeholder="Enter Promocode (if any)" path="promocode" maxlength="12"/>
                            </div>    
                            <div class="promocodeError" id="promocodeError" style="color:red">
                            </div> 
                            <div class="promocodeAccept" id="promocodeAccept" style="color:#1de31d"></div>

                        </div>
                    </c:if>

                </div>
                <div class="margT1 jpMember hidden">	
                    <label class="margT1">Are you an existing InterMiles member? </label>
                    <div class="row">
                        <div class="col-sm-6">
                            <form:hidden class="row margT1 input" placeholder="InterMiles No." maxlength="9" path="jetpriviligemembershipNumber" id="jetpriviligemembershipNumber"/>
                            <div id="jetpriviligemembershipNumber_validate"></div>
                        </div>
                        <div class="col-sm-6">
                            <form:hidden path="jetpriviligemembershipTier" placeholder="InterMiles Membership Tier" class="row margT1 input" readonly="true"  id="jetpriviligemembershipTier"/>
                        </div>
                        <div id="error_msg_JP" style="padding: 10px 0; font-size: 12px; color: red;"></div>
                    </div>
                </div> 
                <div class="margT1">	
                    <div class="row">
                        <div class="col-sm-6">
                            <div>
                                <label class="margT1">E-mail ID<span class="mandatory-field">*</span></label>
                            </div>
                            <div>
                                <form:input	class="row margT1 input" id="email" placeholder="E-mail ID" maxlength="255" path="email" value="${email}"/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div>
                                <label class="margT1">Mobile Number<span class="mandatory-field">*</span></label>
                            </div>
                            <div>
                                <form:input	class="row margT1 input"  placeholder="Mobile Number" maxlength="10" path="mobile" id="mobile" onKeyPress="if(this.value.length==10) return false;"  type="number" value="${mobile}"/>
                            </div>
                        </div>
                    </div>
                </div><hr/>
                <div class="margT1">	
                    <div class="row">
                        <div class="col-sm-6 hasDatePicker">
                            <div>
                                <label class="margT1">Date of Birth<span
                                        class="mandatory-field">*</span></label>
                            </div>
                            <div class="dob">
                                <form:input class="datepicker1 row margT1 input"
                                            placeholder="Date of Birth" id="dateOfBirth" path="dateOfBirth"
                                            title="MM-DD-YYYY" readonly="true" />
                            </div>
                            <button class="datepicker_icon" type="button">
                                <img title="Select Date" alt="Select Date"
                                     src="${applicationURL}static/images/header/calender.png">
                            </button>
                        </div>
                        <div class="col-sm-6">
                            <div>
                                <label class="margT1">Gender<span class="mandatory-field">*</span></label>
                            </div>
                            <div>
                                <form:select class="margT1" path="gender" id="gender">
                                    <form:option value="">Select Gender</form:option>
                                    <c:forEach items="${genderStatus}" var="status">
                                        <form:option value="${status.key}">${status.value}</form:option>
                                    </c:forEach> 
                                </form:select>
                            </div>
                        </div>
                    </div>
                </div>	<hr/>
                <div class="margT1">	
                    <div class="row">
                        <div class="col-sm-6">
                            <div>
                                <label class="margT1">Education Qualification<span class="mandatory-field">*</span></label>
                            </div>
                            <div>
                                <form:select path="eduQualification" class="margT1" id="eduQualification" value="${eduQualification}" >
                                    <form:option value="">Select Education Qualification</form:option>
                                    <form:options items="${qualification}" /> 
                                </form:select>	

                            </div>								
                        </div>
                        <div class="col-sm-6">
                            <div>
                                <label class="margT1"> Net Annual Income<span class="mandatory-field">*</span></label>
                            </div>
                            <div>
                                <form:input	class="row margT1 input" placeholder="Net Annual Income" type="number" onKeyPress="if(this.value.length==8) return false;" maxlength="8" path="monthlyIncome" id="monthlyIncome" value="${monthlyIncome}" />
                            </div>
                        </div>
                    </div>
                </div>	<hr/>
                <div class="margT1">	
                    <div class="row">
                        <div class="col-sm-6">
                            <div>
                                <label class="margT1">PAN Number<span class="mandatory-field">*</span></label>
                            </div>
                            <div>
                                <form:input	class="row margT1 input" placeholder="PAN Number" maxlength="10" path="pancard" id="pancard" value="${pancard}"/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div>
                                <label class="margT1">Aadhaar Number</label>
                            </div>
                            <div>
                                <form:input	class="row margT1 input" placeholder="Aadhaar ID(For Faster Processing)" path="aadharNumber" type="number" onKeyPress="if(this.value.length==12) return false;" id="aadharNumber" value="${aadharNumber}" maxlength="12"/>
                            </div>
                        </div>
                    </div>
                </div><hr/>
                <div class="margT1 currentResAddress">	
                    <label class="margT1">Current Residential Address<span class="mandatory-field">*</span></label>				  					
                    <form:input path="resedentialAddress" class="row margT1 input currentResidentialAddress" placeholder="Current Residential Address"/>
                </div>
                <div class="residentialAddr_div hidden">
                    <div class="residentAddress">
                        <div> Residential Address 1 
                            <span>( H. No., Floor No... etc)</span>
                        </div>
                        <div>						
                            <form:input	class="residentAddressField1" path="address" id="address"  value="${address}" name="address"/>
                        </div>	
                    </div>
                    <div class="residentAddress">
                        <div class=""> Residential Address 2 
                            <span>( Street, Near by... etc)</span>
                        </div>
                        <div>
                            <form:input	class="residentAddressField2" path="address2" id="address2"  value="${address2}" name="address"/>
                        </div>
                    </div>
                    <div class="residentAddress">
                        <div class=""> Residential Address 3
                            <span>( Street, Near by... etc)</span>
                        </div>
                        <div>
                            <form:input	class="residentAddressField3" path="address3" id="address3"  value="${address3}" name="address"/>
                        </div>
                    </div>					
                    <div class="residentAddress">
                        <ul class="address">
                            <li>
                                <form:input class="pincode" maxlength="6" path="pinCode" type="number" onKeyPress="if(this.value.length==6) return false;" id="pinCode" value="${pinCode}" placeholder="Pincode"/>
                            </li>
                            <li>
                                <form:select class="state" path="state1" id="state1" value="${state1}" >
                                    <form:option value="">Select State</form:option>
                                    <form:options items="${states}" /> 
                                </form:select>	

                            </li>
                            <li class="rescity">
                                <form:select class="city" path="city" id="city" value="${city}" >
                                    <form:option value="">Select City</form:option>
                                    <form:options items="${cities}" /> 
                                </form:select>	

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="resedentialAddress-error-div hidden" style="color:red; width: 100%; display: -webkit-box;"></div>
                <hr/>
                <div class="margT1">	
                    <label class="margT1">Do you have a valid address proof for your current residential address mentioned?</label>
                    <div class="addressProof">
                        <div class="radio_btn_yes"><form:radiobutton path="peraddresssameascurr" name="addressProof" checked="true" class="" value="Yes"/><span>Yes</span></div>
                        <div><form:radiobutton path="peraddresssameascurr" name="addressProof" class="" value="No"/><span>No</span></div>
                    </div>				  					
                </div>
                <div class="margT1 permanentAddr hidden">	
                    <label class="margT1">Permanent Residential Address<span class="mandatory-field">*</span></label>				  					
                    <form:input path="permanentAddress" class="row margT1 input permanentResidentialAddress" placeholder="Permanent Residential Address"/>
                </div>
                <div class="permanentAddr_div hidden">
                    <div class="permanent_residentAddress">
                        <div> Residential Address 1 
                            <span>( H. No., Floor No... etc)</span>
                        </div>			
                        <div>				
                            <form:input	class="permanent_AddressField1" name="address"  path="permaddress" value="${permaddress}"/>
                        </div>
                    </div>
                    <div class="permanent_residentAddress">
                        <div class=""> Residential Address 2 
                            <span>( Street, Near by... etc)</span>
                        </div>
                        <div>
                            <form:input	class="permanent_AddressField2"  path="permaddress2" value="${permaddress2}" name="address"/>
                        </div>
                    </div>
                    <div class="permanent_residentAddress">
                        <div class=""> Residential Address 3
                            <span>( Street, Near by... etc)</span>
                        </div>
                        <div>
                            <form:input	class="permanent_AddressField3"  path="permaddress3" name="address" value="${permaddress3}"/>
                        </div>
                    </div>					
                    <div class="permanent_residentAddress">
                        <ul class="address">
                            <li>
                                <form:input class="pincode" maxlength="6" path="PpinCode" type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==6) return false;" id="PpinCode" value="${PpinCode}" placeholder="Pincode"/>
                            </li>
                            <li>
                                <form:select class="state" path="PState" id="PState" value="${PState}" >
                                    <form:option value="">Select State</form:option>
                                    <form:options items="${states}" /> 
                                </form:select>	

                            </li>
                            <li class="rescity">
                                <form:select class="city" path="PCity" id="PCity" value="${PCity}" >
                                    <form:option value="">Select City</form:option>
                                    <form:options items="${cities}" /> 
                                </form:select>	

                            </li>
                        </ul>
                    </div>
                </div>

                <div class="permanentAddr-error-div hidden" style="color: red;"></div>

                <div class="modal fade" id="benefits" tabindex=-1 role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="pull-right close_div"><img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg"  alt=""></div>
                                <h5 class="margT2 colorDarkBlue pad2">Benefits</h5>
                                <span class="list_cont" id="benefitContent"></span>
                            </div>
                        </div>
                    </div>
                </div>   

                <!-- Work details -->
                <div class="colorDarkBlue">
                    <h2 class="heading_txt margT10">Professional Details</h2>
                </div><hr/>				   
                <div class="margT1">	
                    <div class="row">
                        <div class="col-sm-6">
                            <div>
                                <label class="margT1">Employment Status<span class="mandatory-field">*</span></label>
                            </div>
                            <div>
                                <form:select class="margT1" path="employmentType" id="employmentType" value="${employmentType}" >
                                    <form:option value="">Select Employment Type</form:option>
                                    <form:options items="${salaryStatus}" /> 
                                </form:select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div>
                                <label class="margT1">Company Name<span class="mandatory-field">*</span></label>
                            </div>
                            <div>
                                <form:input	class="row margT1 input" maxlength="24" path="companyName" id="companyName" value="${companyName}" placeholder="Company Name"/>
                            </div>
                        </div>
                    </div><hr/>							
                    <div class="margT1 companyAddr">	
                        <label class="margT1">Company Address<span class="mandatory-field">*</span></label>										  					
                        <form:input path="companyAddress" id="companyAddress" class="row margT1 input companyAddress_box" placeholder="Company Address"/>
                    </div>

                    <div class="companyAddr_div hidden">
                        <div class="companyAddress">
                            <div> Company Address 1 
                                <span>( H. No., Floor No... etc)</span>
                            </div>		
                            <div>					
                                <form:input	class="companyAddressField1" name="address" path="OAddress" id="OAddress" value="${OAddress}" />
                            </div>
                        </div>
                        <div class="companyAddress">
                            <div class=""> Company Address 2 
                                <span>( Street, Near by... etc)</span>
                            </div>
                            <div>
                                <form:input	class="companyAddressField2" name="address"  path="OAddress2" id="OAddress2" value="${OAddress2}" />
                            </div>
                        </div>
                        <div class="companyAddress">
                            <div class=""> Company Address 3
                                <span>( Street, Near by... etc)</span>
                            </div>
                            <div>
                                <form:input	class="companyAddressField3" name="address" path="OAddress3" id="OAddress3" value="${OAddress3}" />
                            </div>
                        </div>					
                        <div class="companyAddress">
                            <ul class="address">
                                <li>
                                    <form:input class="pincode" maxlength="6" path="OPincode" type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==6) return false;" id="OPincode" value="${OPincode}" placeholder="Pincode"/>
                                </li>
                                <li>
                                    <form:select class="state" path="OState" id="OState" value="${OState}" >
                                        <form:option value="">Select State</form:option>
                                        <form:options items="${states}" /> 
                                    </form:select>	
                                </li>
                                <li class="rescity">
                                    <form:select class="city" path="OCity" id="OCity" value="${OCity}" >
                                        <form:option value="">Select City</form:option>
                                        <form:options items="${cities}" /> 
                                    </form:select>	

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="companyAddr_div-error-div hidden" style="color:red;width: 100%;display: -webkit-box;"></div>
                    <hr/>
                    <div class="margT1">	
                        <label class="margT1">Please provide either your home or office <strong>landline number</strong> for verification purposes<span class="mandatory-field">*</span></label>				  					
                        <div class="row">
                            <div class="col-sm-6 phoneNum">
                                <form:input path="homeNumber" id="homeNumber" class="row margT1 input homeNumber" placeholder="Home Landline Number"/>

                                <div class="phoneNum_div margT1 hidden row">
                                    <ul>
                                        <li class="std">
                                            <form:input	type="number" class="row input std_class" placeholder="STD" onKeyPress="if(this.value.length==4) return false;"  maxlength="4" path="std" id="std" value="${std}" />
                                        </li>
                                        <li class="phNum">
                                            <form:input type="number" class="row input phone_class" placeholder="Phone Number" onKeyPress="if(this.value.length==8) return false;" maxlength="8" path="phone" id="phone" value="${phone}" />
                                        </li>									
                                    </ul>
                                </div>
                                <div class="homeNumber-error-div hidden" style="color:red"></div>
                            </div>
                            <div class="col-sm-6">
                                <form:input class="row margT1 input officeNumber" path= "officeNumber" id="officeNumber" placeholder="Office Landline Number"/>

                                <div class="officePhoneNum_div margT1 hidden row">
                                    <ul>
                                        <li class="std_office">
                                            <form:input	type="number" class="row input ostd_class" placeholder="STD" minlength="3" onKeyPress="if(this.value.length==4) return false;"  title="Office std should be Min 3 digits" maxlength="4" path="OStd" id="OStd"/>
                                        </li>
                                        <li class="phNum_office">
                                            <form:input type="number" class="row input ophone_class" placeholder="Phone Number" minlength="7" onKeyPress="if(this.value.length==8) return false;" maxlength="8" path="OPhone" id="OPhone"/> 
                                            <!--title="Office phone should be Min 7 digits"-->
                                        </li>									
                                    </ul>
                                </div>
                                <div class="officeNumber-error-div hidden" style="color:red"></div>
                            </div>
                        </div>
                    </div>
                    <hr/>

                    <div class="margT1">
                        <label class="margT1">Statement to be sent to<span class="mandatory-field">*</span></label>	
                        <div>
                            <div class="radio_btn_yes">
                                <form:radiobutton   path="stmtTosent" id="stmtTosent" value="Home" name="statement"  class="statement"/> <span>Home</span>
                            </div>
                            <div>
                                <form:radiobutton   path="stmtTosent" id="stmtTosent" value="Office" name="statement" checked="true" class="statement"/><span> Office</span>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <form:hidden path="initiatedBy" id="initiatedBy"/>
                    <form:hidden path="randomNumber" id="randomNumber"/>
                    <form:hidden path="sourceOfCall" id="sourceOfCall"/>
                    <form:hidden path="formNumber" id="formNumber"/>
                    <form:hidden path="initiationDate" id="initiationDate"/>
                    <form:hidden path="agentId" id="agentId"/>
                    <form:hidden path="utmSource" id="utmSource"/>
                    <form:hidden path="utmMedium" id="utmMedium"/>
                    <form:hidden path="utmCampaign" id="utmCampaign"/>
                    <form:hidden path="otpVal" id="otpVal"/>
                    <form:hidden path="emailInactiveFlag" id="emailInactiveFlag"/>
                    <form:hidden path="message" id="message"/>
                    <form:hidden path="form_Status" id="form_Status"/>
                    <form:hidden path="otpTransactionId" id="otpTransactionId"/>
                    <form:hidden path="token" id="token"/>
                    <form:hidden path="pendingAttempt" id="pendingAttempt"/>
                    <div class="margT1">	
                        <label class="margT1">Terms & Conditions</label>		  					
                        <div class="termsAndConditions">
                            <div class="tc_wrapper">
                                <span class="margT1"> 
                                    ${termsAndCondition}
                                </span><hr/>
                            </div>
                            <div class="terms-checkbox-holder">
                                <form:checkbox value="termsAndConditions" class="margT1 statement" path="termsAndConditions" id="termsAndConditions" onclick="termsAndConditionClick('${cardBean.cardName}','${cardBean.cardName}')"/>  I agree with the Terms and Conditions
                                <!--onclick="$.fn.TermsAndConditionsClickOnIcIcI('$ {cardBean.cardName}','$ {cardBean.cardName}')"-->
                            </div>
                        </div> 
                    </div><hr/>

                </div>
            </div>
            <div class="submit_button">
                <input class="submit_btn button_blue submit" id="jp_btn" onClick="return ewt.trackLink({name: 'Amex_Continue_Button', type: 'click', link: this});" type="submit" value="Submit">
            </div>

    </section>
</form:form>


<script>
    var pincodemsg = 'We are sorry, American Express does not service this pincode, please enter an alternate pincode';
    var invalidMsg = "Please select State/City from the dropdown. If your city/state is not found, choose an alternative pincode as Amex doesn't service your pincode.";

    //Adobe code starts here
    var buttonClicked = false;
    var amexCardName = "";
    var amexVariantName = "";
    var errorDetails = "";
    var genTime = "";
    var submitTime = "";
    
    //(phase 6 start)
    var assistMeFlag = false;
    var loggedInUser = '${sessionScope.loggedInUser}';
    var name = "";
    var mobile = "";
    var email = "";
    var assistError="";
    var lastAccessedField="";
    
    name = $("#socialName").val();
    mobile = $("#socialPhone").val();
    email = $("#socialEmail").val();

    //Adobe code ends here

    function residentpincode() {
//        console.log("resident pincode");
        var pinCode = $("#pinCode").val();
        var cpNo = $("#cpNo").val();
        var cCity;
        var cState;
        if (pinCode != "" && pinCode.length == 6) {
            if (validPinCode(pinCode)) {
                $.get("${applicationURL}checkExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {

                    if (status == "success" && data != "excluded") {

                        $.get("${applicationURL}getCity?pinCode=" + pinCode, function (data, status) {

                            if (status == "success" && data.length != 2) {
                                var json = JSON.parse(data);

                                $.get("${applicationURL}getStateByCity?city=" + json.city, function (data, status) {
                                    if (status == "success" && data.length != 2) {
                                        $("#city").val(json.city);
                                        $("#state1").val(data);
                                        $("#hcity").val(json.city);
                                        $("#hstate").val(data);
                                        $('#state1').attr("disabled", true);
                                        $('#city').attr("disabled", true);


//											$('.resedentialAddress-error-div').html('');
//							             	$('.resedentialAddress-error-div').addClass('hidden');
                                        CheckAddress('RES');
                                        if ($("#city").val() == null || $("#city").val() == '') {
                                            $('#city').empty();
                                            $('select#city').append('<option>' + json.city + '</option>');
                                        }
                                    } else {
                                        $('.resedentialAddress-error-div').html(pincodemsg).css("color", "#ff0000");
                                        $('.resedentialAddress-error-div').removeClass('hidden');
                                        $('#state1').attr("disabled", true);
                                        $('#city').attr("disabled", true);
                                    }
                                });
                            } else {

                                $('.resedentialAddress-error-div').html(invalidMsg).css("color", "#ff6e00");
                                $('.resedentialAddress-error-div').removeClass('hidden');
                                $("#city").val("");
                                $("#state1").val("");
                                $('#state1').attr("disabled", false);
                                $('#city').attr("disabled", false);
                            }
                        });

                    } else {


                        $('.resedentialAddress-error-div').html(pincodemsg).css("color", "#ff0000");
                        $('.resedentialAddress-error-div').removeClass('hidden');
                        $('#state1').attr("disabled", true);
                        $('#city').attr("disabled", true);
                    }
                });
            } else {


                $(".resedentialAddress-error-div").html("Please enter valid Pincode").css("color", "#ff0000");
                $('.resedentialAddress-error-div').removeClass('hidden');
                $('#state1').attr("disabled", true);
                $('#city').attr("disabled", true);
            }
        } else {

            $("#city").val("");
            $("#state1").val("");
//                        $("#city").html('<option value="">Select City</option>');
            $.get("${applicationURL}getAllCity", function (data) {
                $('#city').empty();
                $('#city').append("<option value=''>Select City</option>");
                if (data != '') {
//                        console.log("data1", data);
                    $.each(data, function (index, value) {
                        $('#city').append($("<option>", {value: index, html: value}));

                    });
                }
            });
            $('#state1').attr("disabled", false);
            $('#city').attr("disabled", false);
        }
    }
    function companypincode() {
        var pinCode = $("#OPincode").val();
        var cpNo = $("#cpNo").val();
        if (pinCode != "" && pinCode.length == 6) {
            if (validPinCode(pinCode)) {
                $.get("${applicationURL}checkExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
//                                             console.log("data",data)
                    if (status == "success" && data != "excluded") {
//                                                      console.log("data exclude not",data)
                        $.get("${applicationURL}getCity?pinCode=" + pinCode, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                var json = JSON.parse(data);
                                $.get("${applicationURL}getStateByCity?city=" + json.city, function (data, status) {
                                    if (status == "success" && data.length != 2) {
//                                         console.log($("#OCity").val(json.city));
                                        $("#OCity").val(json.city);

                                        $("#OState").val(data);
                                        $("#hcity2").val(json.city);
                                        $("#hstate2").val(data);
                                        $('#OCity').attr("disabled", true);
                                        $('#OState').attr("disabled", true);
//										$('.companyAddr_div-error-div').html('');
//					             		$('.companyAddr_div-error-div').addClass('hidden');
                                        CheckAddress('COMP');
                                        if ($("#OCity").val() == null || $("#OCity").val() == '') {
                                            $('#OCity').empty();
//                                            console.log('logger1  '+json.city);
                                            $('select#OCity').append('<option>' + json.city + '</option>');
                                        }
                                    } else {
//                                        console.log('else consdtion om COM');
                                        $('.companyAddr_div-error-div').html(pincodemsg).css("color", "#ff0000");

                                        $('.companyAddr_div-error-div').removeClass('hidden');
                                        $('#OCity').attr("disabled", true);
                                        $('#OState').attr("disabled", true);
                                    }
                                });
                            } else {

                                $('.companyAddr_div-error-div').html(invalidMsg).css("color", "#ff6e00");
//                                console.log('else 2 consdtion om COM');

                                $('.companyAddr_div-error-div').removeClass('hidden');
                                $("#OCity").val("");
                                $("#OState").val("");
                                $('#OCity').attr("disabled", false);
                                $('#OState').attr("disabled", false);
                            }
                        });
                    } else {

                        $('.companyAddr_div-error-div').html(pincodemsg).css("color", "#ff0000");
                        $('.companyAddr_div-error-div').removeClass('hidden');
                        $('#OCity').attr("disabled", true);
                        $('#OState').attr("disabled", true);
                    }
                });
            } else {

                $(".companyAddr_div-error-div").html("Please enter valid Pincode").css("color", "#ff0000");
                $('.companyAddr_div-error-div').removeClass('hidden');
                $('#OCity').attr("disabled", true);
                $('#OState').attr("disabled", true);
            }
        } else {
            $("#OCity").val("");
            $("#OState").val("");
//                                                 $("#OCity").html('<option value="">Select City</option>');
            $.get("${applicationURL}getAllCity", function (data) {
                $('#OCity').empty();
                $('#OCity').append("<option value=''>Select City</option>");
                if (data != '') {
//                        console.log("dataO", data);
                    $.each(data, function (index, value) {
                        $('#OCity').append($("<option>", {value: index, html: value}));

                    });
                }
            });
            $('#OCity').attr("disabled", false);
            $('#OState').attr("disabled", false);
        }
    }
    function permanentpincode() {
        var radioValue = $(".addressProof input[type='radio']:checked").val();
        if (radioValue === 'No')
        {
//                 console.log("Called in Permant addressgggggggggggggg");
            var pinCode = $("#PpinCode").val();
            var cpNo = $("#cpNo").val();
            if (pinCode != "" && pinCode.length == 6) {
                if (validPinCode(pinCode)) {
                    $.get("${applicationURL}checkExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
                        if (status == "success") {
                            $.get("${applicationURL}getCity?pinCode=" + pinCode, function (data, status) {
                                if (status == "success" && data.length != 2) {


                                    var json = JSON.parse(data);
                                    $.get("${applicationURL}getStateByCity?city=" + json.city, function (data, status) {
                                        if (status == "success" && data.length != 2) {
                                            $("#PCity").val(json.city);
                                            $("#PState").val(data);
                                            $("#hcity1").val(json.city);
                                            $("#hstate1").val(data);
                                            $('#PCity').attr("disabled", true);
                                            $('#PState').attr("disabled", true);
//										    $('.permanentAddr-error-div').addClass('hidden');
//							                             $(".permanentAddr-error-div").html('');
                                            CheckAddress('PRES');
                                            if ($("#PCity").val() == null || $("#PCity").val() == '') {
                                                $('#PCity').empty();
                                                $('select#PCity').append('<option>' + json.city + '</option>');
                                            }
                                        } else {

//                                            $('.permanentAddr-error-div').html(pincodemsg).css("color", "#ff0000");
//                                            
//                                            $('.permanentAddr-error-div').removeClass('hidden');
//                                            $('#PCity').attr("disabled", true);
//                                            $('#PState').attr("disabled", true);
                                        }
                                    });
                                } else {

//                                                                alert("Inside Elseeeeeee");
//                                                                console.log("Inside Elseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
                                    $('.permanentAddr-error-div').html(invalidMsg).css("color", "#ff6e00");
                                    $('.permanentAddr-error-div').removeClass('hidden');
                                    $("#PState").val("");
                                    $('#PCity').attr("disabled", false);
                                    $('#PState').attr("disabled", false);
                                }
                            });
                        } else {
//                            $('.permanentAddr-error-div').removeClass('hidden');
//                            $(".permanentAddr-error-div").html(pincodemsg).css("color", "#ff0000");
//                            $('#PCity').attr("disabled", true);
//                            $('#PState').attr("disabled", true);
                        }
                    });
                } else {
                    $(".permanentAddr-error-div").html("Please enter valid Pincode").css("color", "#ff0000");
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('#PCity').attr("disabled", true);
                    $('#PState').attr("disabled", true);
                }
            } else {
                $("#PCity").val("");
                $("#PState").val("");
//                        $("#PCity").html('<option value="">Select City</option>');
                $.get("${applicationURL}getAllCity", function (data) {
                    $('#PCity').empty();
                    $('#PCity').append("<option value=''>Select City</option>");
                    if (data != '') {
//                        console.log("dataP", data);
                        $.each(data, function (index, value) {
                            $('#PCity').append($("<option>", {value: index, html: value}));

                        });
                    }
                });
                $('#PCity').attr("disabled", false);
                $('#PState').attr("disabled", false);
            }
        }

    }
    function validPinCode(value) {
        return /^[1-9][0-9]{5}.*$/.test(value);
    }
    $(document).ready(function () {
//        $("#promo").addClass('hidden');
//        $("#namefield").removeClass('col-sm-8');
  console.log("--" + '${beJpNumber}');
        var beJpNumber = '${beJpNumber}';
        console.log("beJpNumber",beJpNumber);
        if (beJpNumber != "") {
            console.log("inside not null beJpNumber")
            $.get("${applicationURL}checkFormStatus?randomNumber=" + '${beRandomNumber}' , function (data, status) {
//                        if(status == "success"){
                            console.log("data===>",data);
                            if(data  != "Completed" || data == ""){
            $('#offer_jp_popup').modal('hide');
            $("#provide_jpNumber").modal('show');
            $(".proceedPopup_close").addClass('hidden');
console.log("utmSource===>",$("#utmSource").val(),"==utmMedium====>",$("#utmMedium").val(),"==utmCampaign===>",$("#utmCampaign").val());
            $('.enroll_popup').addClass('hidden');
            $('.jpNum_popup').removeClass('hidden');
            $('#jpNumber').val(beJpNumber);
            $("#jpNumber").attr("disabled", true);
            console.log("jnuber", $('#jpNumber').val());
            $("#jpNumber").trigger('keyup');
            $("#jetpriviligemembershipNumber").val(beJpNumber);
            $.get("${applicationURL}getTier?jpNum=" + beJpNumber, function (data, status) {
                if (status == "success" && data.length > 2) {
                    console.log("in success")
                    $("#jetpriviligemembershipTier").val(data);
                    isCorrectJpNumber = true;
                    $(".error_msg").text("");
                    $(".error_msg").addClass('hidden');
                    $('#otpButton').attr('disabled', false);
                    $("#otpButton").css("color", "#fff");
                    $('#jpNumpopup').attr('disabled', true);
                    $("#jpNumpopup").css("color", "#ddd");
                } else {
                    $(".error_msg").text("Your application may not have gone through successfully. Please click Submit again.");
                    $(".error_msg").removeClass('hidden');
                    $("#jetpriviligemembershipTier").val('');
                    $('#jpNumpopup').attr('disabled', true);
                    $("#jpNumpopup").css("color", "#ddd");
                    $('#otpButton').attr('disabled', true);
                    $("#otpButton").css("color", "#ddd");
                    $('.geninput').attr('disabled', true);
                    isCorrectJpNumber = false;
                    return false;
                }
            });
            $.get("${applicationURL}getmemberdetail?jpNum=" + beJpNumber, function (data, status) {
                if (status == "success" && data.length > 2) {
                    console.log("get member", data);
                    if (data != "") {
                        $("#mobile").val(data);
                    }
                }
            });
            $("#quickEnroll").attr("disabled", true);
            $('#otpButton').attr('disabled', false);
            $('#quickEnroll').css('cursor', 'default');
            $("a.enroll_here").off("click", callenroll);
            getoffer(beJpNumber, 1);
            $("#CompletedForm").modal('hide');

}else{
                $("#CompletedForm").modal('show');

}
//}
});

        }else{
                    console.log("inside  null beJpNumber")

         console.log("message", $("#form_Status").val());
        if ($("#form_Status").val() != "" && $("#form_Status").val() == "Completed") {
            $("#CompletedForm").modal('show');
        } else {
            $("#CompletedForm").modal('hide');
        }
        }

        $("#pageName").val("AMEX Form Page");
        $("#cardName").val("");
        var callmeflag = '${callMeFlag}';
//          console.log("callmeflag===>",callmeflag);
        if (callmeflag != "") {
            if (callmeflag == 1) {
                $("#social-profiles").removeClass('hidden');

            } else {
                $("#social-profiles").addClass('hidden');

            }
        } else {
            $("#social-profiles").addClass('hidden');
        }
//var promocodeFlag ='${promocodeFlag}';
//          console.log("promocodeFlag===>",promocodeFlag);
//          if(promocodeFlag !=""){
//              if(promocodeFlag == 1){
//                                $("#promo").removeClass('hidden');
//                                $("#namefield").addClass('col-sm-8')
//              }else{
//                                $("#promo").addClass('hidden');
//                                 $("#namefield").removeClass('col-sm-8')
//
//              }
//          }else{
//               $("#promo").addClass('hidden');
//                                                $("#namefield").removeClass('col-sm-8')
//
//          }
        $("#enrollPhone, #socialPhone, #jpNumber, #otpValue, #mini_jpNumber, #mobile, #monthlyIncome, #aadharNumber, #std, #phone, #OStd, #OPhone").on("keydown, keypress", function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;


            }


        });
        $("#jpNumber, #mini_jpNumber").on('keyup', function (evt) {
//             console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 9) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $("#otpValue, #pinCode, #PpinCode, #OPincode").on('keyup', function (evt) {
//             console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 6) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $("#monthlyIncome, #phone, #OPhone").on('keyup', function (evt) {
//             console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 8) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $("#OStd, #std").on('keyup', function (evt) {
//             console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 4) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $("#socialPhone, #mobile, #enrollPhone").on('keyup', function (evt) {
//             console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 10) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $('#socialName').keydown(function (event) {
            var charCode = event.keyCode;
            if ((charCode > 64 && charCode < 91) || charCode == 8 || charCode == 9 || charCode == 32) {
                return true;
            } else {
                return false;
            }
        });
        $("#promocode").on("keyup", function () {
            $("#promocode").attr("maxlength", "12");

        });
        $("#promocodeError").hide();
        $("#promocodeAccept").hide();

        $('#sad').hide();
        $('.loaddiv').css("bottom", "0px");
        $('#otpButton').text('Generate OTP');
        $('#otpButton').attr('disabled', true);
        $('.geninput').attr('disabled', true);
        $('#otpSucces').hide();
        $('#otp_error').hide();
        if ($('#pinCode').val() != "" && $('#city').val() != "" && $('#state1').val() != "") {
            $("#hcity").val($('#city').val());
            $("#hstate").val($('#state1').val());
            $('#state1').attr("disabled", true);
            $('#city').attr("disabled", true);

        }
        if ($('#PpinCode').val() != "" && $('#PCity').val() != "" && $('#PState').val() != "") {
            $("#hcity1").val($('#PCity').val());
            $("#hstate1").val($('#PState').val());
            $('#PCity').attr("disabled", true);
            $('#PState').attr("disabled", true);
        }
        if ($('#OPincode').val() != "" && $('#OCity').val() != "" && $('#OState').val() != "") {
            $("#hcity2").val($('#OCity').val());
            $("#hstate2").val($('#OState').val());
            $('#OCity').attr("disabled", true);
            $('#OState').attr("disabled", true);
        }
        $('#jpNumpopup').attr('disabled', true);
        var isValid = false;
        var originalJPNum;
        var user = '${isLoginUser}';
//        console.log("user t" + user)
        var errMsg = '${errorMsg}';
        if (user == "Yes" && errMsg == "") {
if (beJpNumber != "") {
                $('#offer_jp_popup').modal('hide');
            } else {

            $('#offer_jp_popup').modal('show');
            var offerContent = '${cardBean.offerDesc}';
            var offerDesc = offerContent != "" ? offerContent : $("#offerDesc").val();

            if (offerDesc != "") {
                $(".jpPopup_header").addClass("add_bullet");
                $(".jpPopup_header").html(offerDesc);
                isValid = true;
            }
            var loginJPNum = $("#jetpriviligemembershipNumber").val();
            $("#mini_jpNumber").val(loginJPNum);
            originalJPNum = loginJPNum;
        }
        }

        var mobile;
        $('#jpNumpopup').attr('disabled', true);
        if ('${amexBean.peraddresssameascurr}' == "No") {
            $('.permanentAddr').removeClass('hidden');
        }

        var isCorrectJpNumber = false;
        var result_Value, result_Value_year;
        slidermodal();
        $('#unranged-value2').on('keyup', function () {
//            console.log("at keyup");
            var valSpend = $("#unranged-value2").val();
            valSpend = valSpend.replace(/\,/g, '');
            return handlespend(valSpend);

        });
        function handlespend(valSpend) {
            if (valSpend < 5000 || valSpend > 1500000) {
//             console.log("at keyup check");
                if ($(".slider_modal_div_error").is(':visible') == false) {

                    $('.slider_modal_div_error').css('display', 'block');
                    $(".slider_modal_div_error").html("Please enter spends value within the range.");
                }
                return false;
            } else {
                $('.slider_modal_div_error').css('display', 'none');
                $(".slider_modal_div_error").html("");
                return true;
            }
        }
        $("#unranged-value2").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            // Allow: home, end, left, right, down, up
                                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || (e.keyCode == 110) || (e.keyCode == 190)) {
                        e.preventDefault();
                    }
                });
        $("#unranged-value2").keyup(function (event) {
            var valSpend = $("#unranged-value2").val();
            var valSpendNum = '';
            valSpendNum = valSpend.replace(/\,/g, '');
            valSpendNum = parseInt(valSpendNum, 10);
//        if (valSpendNum >= 0) {  //&& valSpendNum <= 1500000

            // skip for arrow keys
            if (event.which == 37 || event.which == 39)
                return;
            // format number
            $(this).val(function (index, value) {
                if (valSpend < 5000 || valSpend > 1500000) {
                    $(".range-example-modal").asRange('val', 5000);

                    getValue(5000);
                    $("#yearly_bill1").html(result_Value_year);
//                                (valSpendNum > 5000) || (valSpendNum < 1500000)
//                               console.log("***********")
//                           $(".range-example-5").asRange('val', valSpendNum);
//                          
//                                getValue(valSpendNum);
//                                 $("#yearly_bill").html(result_Value_year);
                } else {
//                               console.log("^^^^^^^^^^^^")
                    $(".range-example-modal").asRange('val', valSpendNum);
//                          
                    getValue(valSpendNum);
                    $("#yearly_bill1").html(result_Value_year);
//                               $(".range-example-5").asRange('val', 5000);
//                          
//                                getValue(5000);
//                                 $("#yearly_bill").html(result_Value_year);
                }
//                           $(".range-example-modal").asRange('val', valSpendNum);
                $("#unranged-value2").focus();
//                              if(valSpendNum != ""){
//                                getValue(valSpendNum);
//                                 $("#yearly_bill1").html(result_Value_year);
//                           }
                var x = value.replace(/,/g, "");
                var lastThree = x.substring(x.length - 3);
                var otherNumbers = x.substring(0, x.length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                return res;

            });
//        }//if close
        });
        $('#unranged-value2').on("keyup", function (evt) {

            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;

            } else
                return true;
        });
        getValue($('.range-example-modal').asRange('get'));
        $("#unranged-value2").val(result_Value);
        $("#yearly_bill1").html(result_Value_year);

        function getValue(param) {
            var yearly = (param * 12).toString();
            var monthly = param.toString();
            var lastThree = monthly.substring((monthly.length) - 3);
            var otherNumbers = monthly.substring(0, monthly.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            result_Value = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            lastThree = yearly.substring((yearly.length) - 3);
            otherNumbers = yearly.substring(0, yearly.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            result_Value_year = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        }
        function slidermodal() {
            $(".range-example-modal").asRange({

                min: 5000,
                max: 500000,
                format: function (value) {
                    getValue(value);
                    return result_Value;
                },
                onChange: function (valueslider) {
                    var value = $('.range-example-modal').asRange('get');
//                    console.log("--value", value);

                    var a = document.getElementById("unranged-value2");

                    getValue(value);
                    var valueoftext = $("#unranged-value2").val();
                    valueoftext = valueoftext.replace(/\,/g, '');
                    handlespend(valueoftext);

                    a.value = result_Value;
//                    console.log("at on change", $("#unranged-value2").val());
//                    console.log("result_Value_year", result_Value_year)
                    $("#yearly_bill1").html(result_Value_year);

                    if (value > 5000) {
//                        console.log("----------2--->", value);
                        $(".range-example-5").asRange('set', value);
                    }
                }
            });
        }
        $(".donebut").click(function () {
            var arr = [];
            arr.push('${cardBean.cpNo}');
            var spend = $("#yearly_bill1").html().replace(/<\/?img[^>]*>/g, "");
            spend = spend.substring(spend.indexOf(' ') + 1);
            $.get("${applicationURL}calcfreeflights?spend=" + spend + "&cpNoList=" + arr, function (data, status) {
                if (status == "success") {
                    $.each(data, function (key, value) {
                        $("#freeFlights" + key).html(value.split(",")[1]);
                        $("#jpMiles" + key).html("on earning <br>" + value.split(",")[0] + " JPMiles");
                    });
                    $("#error_msg ,#layover_error_msg").hide();
                    $('#spends_slider').modal('hide');

                    $('.free_flights_div').removeClass('hidden');
                    $('.free_flights_desc').addClass('hidden');
//                    $('#spends_slider').modal('hide');
                }
            });
            $(".close_filter").trigger('click');
        });

        $('#dateOfBirth').on('change', function () {

            $("#amexBean").validate().element('#dateOfBirth')

        });
        $('#dateOfBirth').on('click', function () {

            $('#dateOfBirth').addClass("ignore");

        });
        $('.middleName').on("focus", function () {
            $('.fullName-error-div').addClass('hidden');
        });
        $('.lastName').on("focus", function () {
            $('.fullName-error-div').addClass('hidden');
        });
        //resident
        $('.residentAddressField2').on("focus", function () {
            $('.resedentialAddress-error-div').addClass('hidden');
        });
        $('.residentAddressField3').on("focus", function () {
            $('.resedentialAddress-error-div').addClass('hidden');
        });
        $('#pinCode').on("focus", function () {
            $('.resedentialAddress-error-div').addClass('hidden');
        });
        //permanent
        $('.permanent_AddressField2').on("focus", function () {
            $('.permanentAddr-error-div').addClass('hidden');
        });
        $('.permanent_AddressField3').on("focus", function () {
            $('.permanentAddr-error-div').addClass('hidden');
        });
        $('#PpinCode').on("focus", function () {
            $('.permanentAddr-error-div').addClass('hidden');
        });
        //company
        $('.companyAddressField2').on("focus", function () {
            $('.companyAddr_div-error-div').addClass('hidden');
        });
        $('.companyAddressField3').on("focus", function () {
            $('.companyAddr_div-error-div').addClass('hidden');
        });
        $('#OPincode').on("focus", function () {
            $('.companyAddr_div-error-div').addClass('hidden');
        });
        //---------------------------
        $.validator.addMethod('checkFullName', function (value, element) {
            var fn = $("#fname").val();
            var ln = $("#lname").val();
            if (fn != "" && ln != "" && ln.length >= 2) {
//                console.log("name length is min 2 chhar");
                return true;
            } else {
                return false;
            }
        });

        $.validator.addMethod('checkCurrentAddress', function (value, element) {
            if ($('.residentAddressField1').val() == "") {
                if ($(".resedentialAddress-error").is(':visible') == true) {
//                    console.log("##########called Depends in validator");
                    $('.resedentialAddress-error').addClass('hidden');
                }
            }

            return true;
//            var address = $(".residentAddressField1").val();
//            var pincode = $("#pinCode").val();
//            var city = $("#city").val();
//            var state = $("#state1").val();
//            if (address != "" && pincode != "" && city != "" && state != "") {
//                return true;
//            } else
//                return false;
        });

        $.validator.addMethod('checkCompanyAddress', function (value, element) {
            var address = $("#OAddress").val();

            var pincode = $("#OPincode").val();
            var city = $("#OCity").val();
            var state = $("#OState").val();
            if (address != "" && pincode != "" && city != "" && state != "") {

                return true;
            } else {
                return false;
            }
        });

        $.validator.addMethod('monthlyValidation', function (value, element) {
            var month = $("#monthlyIncome").val();
            if (month >= 600000)
                return true;
            else
                return false;
        });
        //sprint 52
        function checkAlphaNumeric(value) {
            var letter = /[a-zA-Z]/;
            var number = /[0-9]/;
            var spcl = /^[a-zA-Z0-9]+$/;
            var chk_spcl = spcl.test(value);
            if (chk_spcl)
            {
//				console.log("in if of chk_spcl")
                var valid = number.test(value) && letter.test(value);
//				console.log("valid",valid)
                return valid;
            }
        }
        function verifyPromoCode(value) {
            var promoErrorMesg, promoRespStatus, verified;
            $.ajax({method: 'POST',
                async: false,
                url: '${applicationURL}promoCode?voucherNo=' + value,
                success: function (result) {
//                            console.log("result",result);
                    var jsonStr = JSON.parse(result);
//                            console.log("====",jsonStr);

//                            console.log("errroe",jsonStr.JsonResponse);
//                            console.log("errorMsg",jsonStr.errorMsg);
//                            var jsn=JSON.parse(jsonStr.JsonResponse);
//                              var jsn=JSON.parse(jsonStr.JsonResponse);

//                           console.log("- jsn---->",jsn.errorMessage[0],"-----voucherStatus-->",jsn.voucherStatus);
                    if (jsonStr.errorMsg != undefined || jsonStr.errorMsg != "") {
                        promoErrorMesg = jsonStr.errorMsg;
                    } else {
                        promoErrorMesg = ""
                    }

                    promoRespStatus = jsonStr.voucherStatus;
                    if (promoErrorMesg == "" || promoRespStatus == "Unused") {
//                               console.log("not show error");
                        promoErrorMesg = "";
                        verified = promoErrorMesg;
                    } else {
//                               console.log("show error");
                        verified = promoErrorMesg;
                    }

                },
                error: function (xhr, status) {
                    console.log(status);
                    console.log(xhr.responseText);

                }
            });
//                        console.log("promoErrorMesg: ",promoErrorMesg," promoRespStatus:",promoRespStatus)
//                    	console.log("verified",verified);
            return verified;

        }
        $("#promocode").on("focusout", function () {
//           console.log("jp number: ", $("#jetpriviligemembershipNumber").val()," user:",user);
            var promocode = $("#promocode").val();
//                    console.log("promocode:", promocode);
            if (promocode != "") {
                if (promocode == '${defaultPromocode}') {
                    $("#promocodeError").html("");
                    $("#promocodeError").hide();
                    $("#promocodeAccept").show();
                    $("#promocodeAccept").html("Your Promo Code has been applied successfully");

                } else {
                    if ($("#jetpriviligemembershipNumber").val() != "") {
                        var validPromoChar = checkAlphaNumeric(promocode);
                        if (validPromoChar) {
                            var verifiedPromo = verifyPromoCode(promocode);
                            if (verifiedPromo != "") {
                                $("#promocodeError").html("" + verifiedPromo + "");
                                $("#promocodeError").show();
                                $("#promocodeAccept").hide();
                                $("#promocodeAccept").html("");


                            } else {
                                $("#promocodeError").html("");
                                $("#promocodeError").hide();
                                $("#promocodeAccept").show();
                                $("#promocodeAccept").html("Your Promo Code has been applied successfully");


                            }
                        } else {
                            $("#promocodeError").html("Please Enter Valid Promo Code.");
                            $("#promocodeError").show();
                            $("#promocodeAccept").hide();
                            $("#promocodeAccept").html("");

                        }
                    } else {
                        $("#promocodeError").html("Error in Promo Code! Please contact Member Services Desk.");
                        $("#promocodeError").show();
                        $("#promocodeAccept").hide();
                        $("#promocodeAccept").html("");


                    }
                }
            } else {
                $("#promocodeError").html("");
                $("#promocodeError").hide();
                $("#promocodeAccept").hide();
                $("#promocodeAccept").html("");

            }
        });



        $("#address, #address2, #address3, #OAddress3, #OAddress2, #OAddress ,.permanent_AddressField3, .permanent_AddressField2, .permanent_AddressField1").on("keypress", function (e) {
            var len = $(this).val().length;
            if (e.which == 8)
            {

            } else if (len >= 24)
            {
                e.preventDefault();
                if (e.keyCode == 09 || e.keyCode == 11)
                {

                    var inputs = $(this).closest('form').find(':input');
                    inputs.eq(inputs.index(this) + 1).focus();
                }
            }

        });
        $(".error_msg_otp").addClass("hidden");
        var otpCounter = 0;

        $("#amexBean").validate({
//            ignore: [],
            ignore: ".ignore",

            rules: {
                fullName: {
                    required: true,
                    checkFullName: true
                },
                dateOfBirth:
                        {
                            required: true
                        },
                gender:
                        {
                            required: true
                        },
                email:
                        {
                            required: true,
                            emailValidation: true
                        },
                mobile:
                        {
                            required: true,
                            digits: true,
                            mobileValidation: true
                        },
                eduQualification:
                        {
                            required: true
                        },
                monthlyIncome:
                        {
                            required: true,
                            digits: true,
                            monthlyValidation: true
                        },
                pancard:
                        {
                            required: true,
                            panValidation: true
                        },
                aadharNumber:
                        {
                            digits: true,
                            rangelength: [12, 12]
                        },
                resedentialAddress: {
                    required: true,
                    checkCurrentAddress: true
                },
                employmentType:
                        {
                            required: true
                        },
                companyName:
                        {
                            required: true
//                            companyNameVaidation: true
                        },
                companyAddress: {
                    required: true,
                    checkCompanyAddress: true
                },
                homeNumber: {
                    required: {
                        depends: function (element) {
                            return ($("#OPhone").val() == "");
                        }
                    },
                },
                termsAndConditions:
                        {
                            required: true
                        }
//                        ,
//                promocode: {
//
//                    checkPromoCode: true,
//                    promocodeVaidation: true,
//                    verifyPromoCode: 
//                            { 
//                                depends:function(element){
//                                    if(user != "" && $("#promocode").val() !=""){
//                                        console.log("***********************")
//                                        return true;
//                                    }else{
//                                        return false;
//                                    }
//                          }
//                    }
//                }
            },

            messages: {
                fullName: {
                    required: "Please Enter Full Name",
                    checkFullName: ""
                },
                dateOfBirth: {
                    required: "Please Select Date of Birth"
                },
                gender: {
                    required: "Please Select Gender"
                },
                email: {
                    required: "Please Enter Email Id",
                    emailValidation: "Email ID is not valid."
                },
                mobile: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid.",
                    mobileValidation: "Mobile Number is not valid."
                },
                eduQualification: {
                    required: "Please Select Qualification"
                },
                monthlyIncome: {
                    required: "Please Enter Your Net Annual Income Not Including Any Bonus Or Incentives",
                    digits: "Net Annual Income is not valid.",
                    monthlyValidation: "Net Annual Income must be greater than or equal to 6 lakhs."
                },
                pancard: {
                    required: "Please Enter Pancard Number",
                    panValidation: "Please Enter a valid PAN Number."
                },
                aadharNumber:
                        {
                            digits: "Aadhaar Number is not valid.",
                            rangelength: "Minimum of 12 characters permitted for Aadhaar Number"
                        },
                resedentialAddress: {
                    required: "Please provide your Current Residential Address",
                    checkCurrentAddress: "Please provide your Current Residential Address"
                },
                employmentType: {
                    required: "Please Select Employment Type"
                },
                companyName: {
                    required: "Please Enter Company Name"
//                    companyNameVaidation: "Please enter valid Company Name"
                },
                companyAddress: {
                    required: "Please provide your Company Address",
                    checkCompanyAddress: "hfhhfghfghgh"
                },
                homeNumber: {
                    required: "Please enter Home Landline number and must have exact 11 digits including STD code.",
                },
                termsAndConditions: {
                    required: "Please agree to be contacted by American Express."
                }
//                ,
//                promocode: {
//
//                    checkPromoCode: "Please Login",
//                    promocodeVaidation: "Please Enter Valid Promocode.",
////                    verifyPromoCode:"Please Check"+promoErrorMesg+""
//                
//
//                }
            },
            onfocusout: function (element) {
                this.element(element);
//                alert("infocusout");
                if ($('#dateOfBirth').hasClass("ignore")) {
                    $('#dateOfBirth').removeClass("ignore");
                }

                // <- "eager validation"
            },
            errorElement: "div",
            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },
            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                if ($(element).hasClass("datepicker1 row margT1 input hasDatepicker")) {
//                    alert('Testing 4');
                    error.insertAfter($(".datepicker_icon"));
//                    error.insertAfter($("#dateOfBirth"));
                } else if ($(element).hasClass("margT1 statement")) {
                    error.insertAfter($(element).closest("div"));
                } else if ($(element).hasClass("row margT1 input name_inputBox")) {
                    error.insertAfter($(".name_div"));
                } else if ($(element).hasClass("row margT1 input currentResidentialAddress")) {
                    error.insertAfter($(".residentialAddr_div"));
                } else if ($(element).hasClass("row margT1 input companyAddress_box")) {

                    error.insertAfter($(".companyAddr_div"));
                } else if ($(element).hasClass("row margT1 input homeNumber")) {
                    error.insertAfter($(".phoneNum_div"));
                } else {
                    error.insertAfter(element);
                }

                //Adobe code starts here (phase 5)
                errorTrack = errorTrack + error.text() + ",";
//                 console.log("errorTrack====",errorTrack)
//                $.fn.ErrorTracking(errorTrack, " ");

                //Adobe code ends here
            },
            submitHandler: function (form) {
                $("#hcity").val($("#city").val());
                $("#hstate").val($("#state1").val());
                $("#hcity1").val($("#PCity").val());
                $("#hstate1").val($("#PState").val());
                $("#hcity2").val($("#OCity").val());
                $("#hstate2").val($("#OState").val());
                $("#promocode").trigger("focusout");
                $('.about .error_box').addClass('hidden');

                var residentAddressField1 = $('.residentAddressField1').val();
                var residentAddressField2 = $('.residentAddressField2').val();
                var residentAddressField3 = $('.residentAddressField3').val();

                checkAddressLength(residentAddressField1, residentAddressField2, residentAddressField3);

                if ($("#jetpriviligemembershipNumber").val() != "") {

                    $('#otpButton').attr('disabled', false);
                    $("#jpNumber").val($("#jetpriviligemembershipNumber").val());
                    $("#jpTier").val($("#jetpriviligemembershipTier").val());
                    if ($("#promocode").val() != "" && $("#promocodeError").text() == "") {
                        if ($("#promocode").val() == '${defaultPromocode}') {
                            $("#jpNumber").attr("disabled", false);
                            $("#quickEnroll").attr("disabled", false);
                            $("a.enroll_here").on("click", callenroll);
                            $('#quickEnroll').css('cursor', 'pointer');

//                            $("#jpNumber").attr("title","");
                            $(".promoinfo").addClass('hidden');


                        } else {


                            $("#jpNumber").attr("disabled", true);
//                        $("#jpNumber").attr("title","Promo code has been applied for the given JetPrivilege number.")
                            $("#quickEnroll").attr("disabled", true);
                            $('#quickEnroll').css('cursor', 'default');
                            $("a.enroll_here").off("click", callenroll);
                            $('#quickEnroll').css('cursor', 'default');
//                      $("#quickEnroll").attr("title","Promo code has been applied for the given JetPrivilege number.");
                            $(".promoinfo").removeClass('hidden');

                            $('.enroll_popup').addClass('hidden');
                            $('.jpNum_popup').removeClass('hidden');
                        }
                    } else {
                        $("#jpNumber").attr("disabled", false);
                        $("#jpNumber").attr("title", "");
//                            $('.tooltips').hide();
                        $('#quickEnroll').css('cursor', 'pointer');

                        $("#quickEnroll").attr("disabled", false);
//                            ('#quickEnroll').css('cursor','pointer');
//                        $('#quickEnroll').attr('href','javascript:void(0)').css('cursor','pointer');
//                                                    $("#quickEnroll").trigger('click');
                        $("a.enroll_here").on("click", callenroll);

//                        $("a.enroll_here").on("click",function(e){
//                            $("#quickEnroll").trigger('click');
//                        });
                        $(".promoinfo").addClass('hidden');

                    }
                } else {
                    $("a.enroll_here").on("click", callenroll);
                    $(".promoinfo").addClass('hidden');


                }

                var radioValue = $(".addressProof input[type='radio']:checked").val();
                if (radioValue == "No") {
                    var residentAddressField1 = $('.permanent_AddressField1').val();
                    var pin = $('.permanent_residentAddress .pincode').val();
                    var city = $('.permanent_residentAddress .city option:selected').text();
                    var state = $('.permanent_residentAddress .state option:selected').text();
                    if (residentAddressField1 == "" || pin == "" || city == "" || state == "") {
                        $('.permanentAddr-error-div').removeClass('hidden');
                        $(".permanentAddr-error-div").html("Please Enter Permanent Residential Address");
                        $('html,body').stop(true, false).animate({scrollTop: $('.permanentAddr-error-div').offset().top - 150}, 'fast');
                        return false;
                    }
                    checkpermanent();
//                              permanentpincode();
                } else if (radioValue == "Yes") {
                    $('.permanentAddr-error-div').html("");
                    $('.permanentAddr-error-div').addClass('hidden');
                    $(".permanent_AddressField1").val($(".residentAddressField1").val());
                    $(".permanent_AddressField2").val($(".residentAddressField2").val());
                    $(".permanent_AddressField3").val($(".residentAddressField3").val());
                    $('.permanent_residentAddress .pincode').val($('.residentAddress .pincode').val());
                    $('.permanent_residentAddress .city').val($('.residentAddress .city option:selected').text());
                    $('.permanent_residentAddress .state').val($('.residentAddress .state option:selected').text());
                }

                var fName = $('.firstName').val();
                var mName = $('.middleName').val();
                var lName = $('.lastName').val();
                var msg = "Please ensure your keyword limited to 15 characters";
                var fNameLength = fName.length;
                var lNameLength = lName.length;
                var mNameLength = mName.length;
                var appendQuery = fNameLength > 15 && lNameLength > 15 && mNameLength > 15 ? "First Name, Middle Name, Last Name" : fNameLength > 15 && lNameLength > 15 ? "First Name, Last Name" : fNameLength > 15 && mNameLength > 15 ? "First Name, Middle Name" : mNameLength > 15 && lNameLength > 15 ? "Middle Name, Last Name" : fNameLength > 15 ? "First Name" : mNameLength > 15 ? "Middle Name" : lNameLength > 15 ? "Last Name" : "";
                msg = msg.replace('keyword', appendQuery);

                if (fNameLength > 15 || mNameLength > 15 || lNameLength > 15) {
                    $('.fullName-error-div').html(msg);
                    $('.fullName-error-div').removeClass('hidden');
                    $('html,body').stop(true, false).animate({scrollTop: $('.fullName').offset().top - 150}, 'fast');
                    return false;
                }
//                console.log("@@@@@@@@@@@@ R @",$(".resedentialAddress-error-div").text(),"<=length=>",$(".resedentialAddress-error-div").text().length)

                if ($(".resedentialAddress-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.currentResAddress').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".permanentAddr-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.permanentAddr').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".companyAddr_div-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.companyAddr').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".homeNumber-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.homeNumber-error-div').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".officeNumber-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.officeNumber-error-div').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".promocodeError").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.promocodeError').offset().top - 150}, 'fast');
                    return false;
                }
                var offerContent = '${cardBean.offerDesc}';
                var offerDesc = offerContent != "" ? offerContent : $("#offerDesc").val();
                if (offerDesc != "") {
//					$(".jpPopup_header").addClass("add_bullet");
                    $('#jpNumpopup').attr('disabled', true);
                    $("#jpNumber").trigger('keyup');
                }
                if (beJpNumber != "") {
                                  console.log("random number",$("#randomNumber").val());
$.get("$ {applicationURL}checkFormStatus?randomNumber=" + $("#randomNumber").val() , function (data, status) {
                        if(status == "success" && data !=""){
                            console.log("data===>",data);
                            if(data  != "Completed"){
                           $("#CompletedForm").modal('hide');

                    $("#provide_jpNumber").modal('hide');
                    $('.loaddiv').css({"bottom": "-57px", "position": "fixed"});
                    $('#sad').show();
                    form.submit();
      }else{
                                                                $("#CompletedForm").modal('show');

                            }
                        }
                    })
//                    
                } else {
                //start
                $.get("${applicationURL}checkFormStatus?randomNumber=" + $("#randomNumber").val() , function (data, status) {
//                        if(status == "success" && data !=""){
                            console.log("data===>",data);
                            if(data  != "Completed"){
                $("#provide_jpNumber").modal('show');
                $('.error_box').addClass('hidden');
                $('.error_box1').addClass('hidden');

                $("#jpNumpopup").on('click', function () {
                    if ($("#jpNumber").val() != "" && $("#jetpriviligemembershipTier").val() != "") {
                        $(".geninput").trigger('focusout');

                        if ($(".geninput").val() != "" && isCorrectOtp == true) {
                            $("#otpVal").val($(".geninput").val());
                            form.submit();
                            $('#jpNumpopup').attr('disabled', true);
                        }
                    } else {
                        $("#jpNumber").trigger('keyup');
                        if ($("#jpNumber").val() != '' && isCorrectJpNumber) {
                            $(".geninput").trigger('focusout');
                            if ($(".geninput").val() != "" && isCorrectOtp == true) {
                                $("#otpVal").val($(".geninput").val());
                                form.submit();
                                $('#jpNumpopup').attr('disabled', true);
                            }
                        }

                    }


                    if ($(".error_msg_otp").is(':visible') == true) {
                        $('.loaddiv').css("bottom", "0px");
                        $('#sad').hide();
//                        $('.loaddiv').addClass('hidden');
                    } else {
                        $('.loaddiv').css({"bottom": "-57px", "position": "fixed"});
                        $('#sad').show();
//                    $('.loaddiv').removeClass('hidden');
                    }
                    //Adobe code starts here(phase 4)
                    afterApplyAmexSubmitClick($("#jpNumber").val());
                    //Adobe code ends here
                });
                $("#CompletedForm").modal('hide');

                            }else{
                                                                $("#CompletedForm").modal('show');

                            }
//                        }
                    })
                }//else close
                //Adobe code starts here
                IsButtonClicked = true;
                submitAmexFormClick(fName + " " + mName + " " + lName, $("#email").val(), mobile, $('.permanent_residentAddress .city option:selected').text(), $('.permanent_residentAddress .pincode').val(), 'Amex', 'Submit JPNumber');
                //Adobe code ends here    

            }
        });
$("#jpNumpopup").on('click', function () {
            console.log("on click");
            if (beJpNumber != "") {
                $("#provide_jpNumber").modal('hide');

                $.ajax({method: "GET",
                    async: false,
                    url: "${applicationURL}getmemberdata?jpNum=" + beJpNumber, success: function (data) {
                        console.log("===============", data);
//               
                        $("#address").val(data.address);
                        $("#address2").val(data.address2);
                        $("#address3").val(data.address3);
                        $("#city").val(data.city);
                        $("#pinCode").val(data.pinCode);
                        $("#resedentialAddress").val(data.resedentialAddress);
                        $("#state1").val(data.state1);
                        $("#dateOfBirth").val(data.dateOfBirth);
                        $("#email").val(data.email);
                        $("#fname").val(data.fname);
                        $("#fullName").val(data.fullName);
                        $("#gender").val(data.gender);
                        $("#jetpriviligemembershipNumber").val(data.jetpriviligemembershipNumber);
                        $("#jetpriviligemembershipTier").val(data.jetpriviligemembershipTier);
                        $("#lname").val(data.lname);
                        $("#mname").val(data.mname);
                        $("#title").val(data.title);
                        $("#randomNumber").val('${beRandomNumber}');
                        $("#initiatedBy").val("Self via Marketing Campaign");

                        console.log("random inside", $("#randomNumber").val());
                    },
                    error: function (e) {

                        console.log("Error " + e);
                    }
                });
                SaveFormData();
            } else {
                console.log("fullName outside", $("#fullName").val());
            }
        });
        $.validator.addMethod('companyNameVaidation', function (value, element) {

            var companyValidation = companyValid($("#companyName").val());

            return companyValidation;
        });
        $(".datepicker_icon").on("click", function () {
//            alert('TESt  1');
            $("#dateOfBirth-error").remove();

        });
        $(".ui-datepicker-year").on("click", function () {
//            alert('TESt  2');
            $("#dateOfBirth-error").remove();
//            $("#dateOfBirth-error").hide();
        });
        $("#otpValue").on("keydown, keypress", function (e) {
            var charCode;
            if (e.keyCode > 0) {
                charCode = e.which || e.keyCode;
            } else if (typeof (e.charCode) != "undefined") {
                charCode = e.which || e.keyCode;
            }
            if (charCode == 46)
                return true
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;


        });
//        $(".geninput").keyup(function () {
//            if ($(".geninput").val().length == 6) {
//
//                $(".geninput").trigger('focusout');
//            } else {
//                $('#jpNumpopup').attr('disabled', true);
//            }
//        });
        var isCorrectOtp = false;
        $(".geninput").focusout(function () {
            var geinput = $(".geninput").val();

            if (geinput == "") {
                $(".error_msg_otp").html("Please Enter OTP");
                $(".error_msg_otp").removeClass("hidden");
                $('#jpNumpopup').attr('disabled', true);
                isCorrectOtp = false;
            } else if (geinput.length > 6 || geinput.length < 6) {

                $(".error_msg_otp").html("Please Enter Valid OTP");
                $(".error_msg_otp").removeClass("hidden");
                $('#jpNumpopup').attr('disabled', true);
                isCorrectOtp = false;
            } else {
                if (isNaN(geinput) == true) {

                    $(".error_msg_otp").html("Please Enter Valid OTP");
                    $(".error_msg_otp").removeClass("hidden");
                    $('#jpNumpopup').attr('disabled', true);
                    isCorrectOtp = false;
                } else {
                    var formNumber = '${formNumber}';
                    var mobileNumber = $('#mobile').val();
                    var otpTransactionId = $("#otpTransactionId").val();
                    $.ajax({method: "GET",
                        async: false,
                        url: "${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&otpTransactionId=" + otpTransactionId + "&otp=" + geinput + "&token=" + $("#token").val(), success: function (data) {
                            console.log("=========validate======", data);
//               
//                    $.get("$ {applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + 1 + "&formNumber=" + formNumber +"&otpTransactionId="+otpTransactionId+"&otp="+geinput,  { async: false },function (data, status) {

                            if (data.length > 2) {
                                  var myObj = JSON.parse(data);

                                pendingAttempt = myObj.pendingAttempts;
                                $("#pendingAttempt").val(myObj.pendingAttempts);
                                console.log("pending attempt"+$("#pendingAttempt").val())
                                if ($("#pendingAttempt").val() == "Number of attempts exceeds limit") {
                                    $(".geninput").attr('disabled', true);
                                    $(".geninput").val("");
                                    $(".error_msg_otp").html("");
                                    $(".error_msg_otp").hide();
                                    successMsg="Left attempts are exceeded";
//                                    $('#otpSucces').html("Number of attempts exceeds limit");
                                    $('#otpSucces').removeClass('hidden');
                                    $('#otpButton').attr('disabled', true);
                                    $(".otpButton").addClass("hidden");
                                    $('#jpNumpopup').attr('disabled', true);
                                    $("#jpNumpopup").css("color", "#ddd");
                                    var imageUrl = ctx + "static/images/icons/error_icon.png";
                                    $(".otpsuccess").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
                                } else if ($("#pendingAttempt").val() != "") {
                                    console.log("=-=-=p-==-=-=>", $("#pendingAttempt").val());
                                    successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left ' + $("#pendingAttempt").val();

                                } else {
                                    successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                }
                                $('#otpSucces').html(successMsg);
                                if (myObj.OTP_Verified == "true") {

                                    $(".error_msg_otp").html("");
                                    $(".error_msg_otp").addClass("hidden");
                                    $('#jpNumpopup').attr('disabled', false);
                                    $("#jpNumpopup").css("color", "#fff");
                                    $('#otpVal').val(geinput);

                                    isCorrectOtp = true;
                                } else {
                                    $(".error_msg_otp").html("Please Enter Valid OTP");
                                    $(".error_msg_otp").removeClass("hidden");
                                    $('#jpNumpopup').attr('disabled', true);
                                    $("#jpNumpopup").css("color", "#ddd");
                                    isCorrectOtp = false;
                                    $('#otpVal').val('');
                                }
                            } else {
                                $(".error_msg_otp").html("Please Enter Valid OTP");
                                $(".error_msg_otp").removeClass("hidden");
                                $('#jpNumpopup').attr('disabled', true);
                                $("#jpNumpopup").css("color", "#ddd");
                                isCorrectOtp = false;
                            }

                        }});

                }
            }

        });




//          $("#jpNumpopup").focusin(function () {

//          alert('tedfgdgfdgfd');
//          });

//                                    $("#companyName").on('keyup', function () {
//                       
//                        var companyValidation=companyValid($("#companyName").val());
//                      
//                        if(!companyValidation){
//                          
//                               $('.companyName-error').html('Company Name Special charecter not aloowd').css("color", "#ff0000");
//                                $('.companyName-error').removeClass('hidden');
//                           
//                        }
//                        return companyValid($("#companyName").val());     
//                    });

        function companyValid(str) {

            return !/[~`!@#$%\^&*()+=\-\[\]\\';,./{}|\\":<>\?0-9]/g.test(str);

        }


        var previousValue = $("#jpNumber").val();

        $(".proceedPopup_close").on("click", function (e) {

            otpCounter = 0
            $('#otpButton').text("Generate OTP");
            $('#otpSucces').hide();
            $(".error_msg_otp").html("");
            $(".error_msg_otp").addClass("hidden");
            $(".geninput").val("");
            $(".geninput").attr("disabled", true);
            $('#jpNumpopup').attr('disabled', true);

        });
        $("#jpNumber").on("input", function (e) {
            var currentValue = $(this).val();
            if (currentValue != previousValue) {
                previousValue = currentValue;
                otpCounter = 0
                $('#otpButton').text("Generate OTP");
                $('#otpSucces').hide();
                $(".error_msg_otp").html("");
                $(".error_msg_otp").addClass("hidden");
                $(".geninput").val("");
            }
        });

        $("#jpNumber").keyup(function () {
            var jpNum = $(this).val();
            console.log("jpNum>>>"+jpNum);
            var cpNo = $("#cpNo").val();
            if (jpNum == "") {
                $(".error_msg").text("Please enter InterMiles No.");
                $(".error_msg").removeClass('hidden');
                $("#jpTier").val('');
                $("#jetpriviligemembershipTier").val('');
                isCorrectJpNumber = false;
                $('#otpButton').attr('disabled', true);
                $('.geninput').attr('disabled', true);
                $('#jpNumpopup').attr('disabled', true);
                return false;
            }

            if (jpNum != "" && jpNum.length == 9) {
                if (isNaN(jpNum)) {
                    $(".error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                    $(".error_msg").removeClass('hidden');
                    $("#jetpriviligemembershipTier").val('');
                    $('#jpNumpopup').attr('disabled', true);
                    $('#otpButton').attr('disabled', true);
                    $('.geninput').attr('disabled', true);
                    isCorrectJpNumber = false;
                    return false;
                } else {
                    jpNumLastDig = jpNum % 10;
                    jpNoTemp = eval(jpNum.slice(0, -1)) % 7;

                    if (jpNumLastDig !== jpNoTemp)
                    {
                        $(".error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                        $(".error_msg").removeClass('hidden');
                        $("#jetpriviligemembershipTier").val('');
                        isCorrectJpNumber = false;
                        $('#otpButton').attr('disabled', true);
                        $('.geninput').attr('disabled', true);
                        $('#jpNumpopup').attr('disabled', true);
                        return false;
                    } else {
                        $("#jetpriviligemembershipNumber").val(jpNum);
                        $.get("${applicationURL}getTier?jpNum=" + jpNum, function (data, status) {
                            if (status == "success" && data.length > 2) {
                                $("#jetpriviligemembershipTier").val(data);
                                isCorrectJpNumber = true;
                                $(".error_msg").text("");
                                $(".error_msg").addClass('hidden');
                                $('#otpButton').attr('disabled', false);
                                $('#jpNumpopup').attr('disabled', true);
                            } else {
                                $(".error_msg").text("Your application may not have gone through successfully. Please click Submit again.");
                                $(".error_msg").removeClass('hidden');
                                $("#jetpriviligemembershipTier").val('');
                                $('#jpNumpopup').attr('disabled', true);
                                $('#otpButton').attr('disabled', true);
                                $('.geninput').attr('disabled', true);
                                isCorrectJpNumber = false;
                                return false;
                            }
                        });
                        getoffer(jpNum, cpNo);
//                        $.get("${applicationURL}getOffer?jpNum=" + jpNum + "&cpNo=" + cpNo, function (data, status) {
//                            var jsonStr = JSON.parse(data);
//                            if (status == "success" && data.length > 2) {
//                                addOfferToJpPopup(jsonStr);
////			 						 $('#jpNumpopup').attr('disabled', false); 
//                            }
//                        });
                    }
                }
            } else {
//			 		$(".error_msg").text("Invalid JPNumber, Please enter valid JPNumber");
                $(".error_msg").removeClass('hidden');
                $("#jetpriviligemembershipTier").val('');
                $('.geninput').attr('disabled', true);
                $('#otpButton').attr('disabled', true);
                isCorrectJpNumber = false;
                return false;

            }
        });
function getoffer(jpNum, cpNo) {
            $.get("${applicationURL}getOffer?jpNum=" + jpNum + "&cpNo=" + cpNo, function (data, status) {
                var jsonStr = JSON.parse(data);
                if (status == "success" && data.length > 2) {
                    addOfferToJpPopup(jsonStr);
//			 						 $('#jpNumpopup').attr('disabled', false); 
                }
            });
        }
        function addOfferToJpPopup(jsonStr) {
            $(".jpPopup_header").addClass("add_bullet");
            $(".jpPopup_header").html(jsonStr.offerDesc);
            $("#offerId").val(jsonStr.offerId);
            $("#offerDesc").val(jsonStr.offerDesc);
        }

        $("#mini_jpNumber").on('keyup', function () {
            var cpNo = $("#cpNo").val();
            var jpNum = $(this).val();
            console.log("mini_jpNumber jpNum>>>>>>>>"+jpNum);
            console.log("mini_jpNumber cpNo>>>>>>>>"+cpNo);
            if (jpNum != "" && jpNum.length == 9) {
                if (isNaN(jpNum)) {
                    $(".mini_error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                    $(".mini_error_msg").removeClass('hidden');
                    isValid = false;
                    return false;
                } else {
                    jpNumLastDig = jpNum % 10;
                    jpNoTemp = eval(jpNum.slice(0, -1)) % 7;
                    if (jpNumLastDig !== jpNoTemp)
                    {
                        $(".mini_error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                        $(".mini_error_msg").removeClass('hidden');
                        isValid = false;
                        return false;
                    } else {
                        $.get("${applicationURL}getOffer?jpNum=" + jpNum + "&cpNo=" + cpNo, function (data, status) {
                            if (status == "success" && data.length > 2) {
                                var jsonStr = JSON.parse(data);
                                console.log("jsonStr>>"+jsonStr);
                                $(".offer_link").addClass("hidden");
                                $(".offer_display").removeClass("hidden");
                                $(".offer_display").html(jsonStr.offerDesc);
                                $(".login_offer").html(jsonStr.offerDesc);
                                addOfferToJpPopup(jsonStr);
                                $('.mob_view .home_feature').each(function () {
                                    var toggle_img = "<div class='collps_img coldown'><img src=" + ctx + "static/images/co-brand/collps_down.png></div>" +
                                            "<div class='collps_img colUp' style='display:none'><img src=" + ctx + "static/images/co-brand/collps_up.png></div>";
                                    if ($(this).height() > 200) {
                                        $(this).addClass("trimmed");
                                        $(this).closest(".list_cont").after(toggle_img);
                                    }
                                });
                            }
                        });
                        $("#jetpriviligemembershipNumber").val(jpNum);
                        $(".mini_error_msg").text("");
                        $(".mini_error_msg").addClass('hidden');
                        isValid = true;
                    }
                }
            }
        });

        $("#mini_jpNumber_submit").on('click', function () {

            var jpNum = $("#mini_jpNumber").val();
            var originalJPNum = loginJPNum;
            if (jpNum == "" || jpNum.length <= 8) {
                $(".mini_error_msg").text("Please enter valid InterMiles No.");
                $(".mini_error_msg").removeClass('hidden');
                return false;
            }
            if (originalJPNum != jpNum) {
                $('select').val('');
                $('input[type=text]').val('');
            }
            if (isValid) {
                $('#jpNumpopup').attr('disabled', false);
                $('#offer_jp_popup').modal('hide');
            }
            $('.coldown').css('display', 'block');
            $('.colUp').css('display', 'none');
            $('.home_feature').each(function () {
                $(this).addClass('trimmed');
            });
        });

        $("#state1").on("change", function () {
            var state = $("#state1 option:selected").val();
            var pin = $('.residentAddress .pincode').val();

            if (state != "") {
                $.get("${applicationURL}getCitiesByState?state=" + state, function (data) {
                    $('#city').empty();
                    $('#city').append("<option value=''>Select City</option>");
                    if (data != '') {
                        $.each(data, function (index, value) {
                            $('#city').append($("<option>", {value: index, html: value}));

                        });
                    }
                });
            } else {
                $.get("${applicationURL}getAllCity", function (data) {
                    $('#city').empty();
                    $('#city').append("<option value=''>Select City</option>");
                    if (data != '') {

                        $.each(data, function (index, value) {
                            $('#city').append($("<option>", {value: index, html: value}));

                        });
                    }
                });





            }
//								$('.resedentialAddress-error-div').html('');
//				             	$('.resedentialAddress-error-div').addClass('hidden');
            CheckAddress('RES');
            $('#city').selectmenu('refresh', true);

        });

        $("#PState").on("change", function () {
            var PState = $("#PState option:selected").val();

            if (PState != "") {
                $.get("${applicationURL}getCitiesByState?state=" + PState, function (data) {
                    $('#PCity').empty();
                    $('#PCity').append("<option value=''>Select City</option>");
                    if (data != '') {
                        $.each(data, function (index, value) {
                            $('#PCity').append($("<option>", {value: index, html: value}));
                        });
                    }
                });

            } else {

                $.get("${applicationURL}getAllCity", function (data) {
                    $('#PCity').empty();
                    $('#PCity').append("<option value=''>Select City</option>");
                    if (data != '') {

                        $.each(data, function (index, value) {
                            $('#PCity').append($("<option>", {value: index, html: value}));

                        });
                    }
                });


            }
//						$('.permanentAddr-error-div').html('');
//		             	$('.permanentAddr-error-div').addClass('hidden');
            CheckAddress('PRES');
            $('#PCity').selectmenu('refresh', true);
        });

        $("#OState").on("change", function () {

            var OState = $("#OState option:selected").val();
            if (OState != "") {

                $.get("${applicationURL}getCitiesByState?state=" + OState, function (data) {
                    $('#OCity').empty();
                    $('#OCity').append("<option value=''>Select City</option>");
                    if (data != '') {
//                        console.log("data",data);
                        $.each(data, function (index, value) {
                            $('#OCity').append($("<option>", {value: index, html: value}));
                        });
                    }

                });
            } else {

                $.get("${applicationURL}getAllCity", function (data) {
                    $('#OCity').empty();
                    $('#OCity').append("<option value=''>Select City</option>");
                    if (data != '') {
//                        console.log("dataO", data);
                        $.each(data, function (index, value) {

                            $('#OCity').append($("<option>", {value: index, html: value}));

                        });
                    }
                });
            }
//						$('.companyAddr_div-error-div').html('');
//		             	$('.companyAddr_div-error-div').addClass('hidden');
            CheckAddress('COMP');
            $('#OCity').selectmenu('refresh', true);
        });

        $("#city").on("change", function () {
            var city = $("#city option:selected").val();
            var pin = $('.residentAddress .pincode').val();

            $.get("${applicationURL}getStateByCity?city=" + city, function (data, status) {
                if (status == "success" && data.length >= 2) {

//                    var json = JSON.parse(data);

                    $("#state1").val(data);
                    var pin = $('.residentAddress .pincode').val();
                } else {

                    $("#state1").val("");
                    $.get("${applicationURL}getAllCity", function (data) {
                        $('#city').empty();
                        $('#city').append("<option value=''>Select City</option>");
                        if (data != '') {
//                        console.log("data1", data);
                            $.each(data, function (index, value) {
                                $('#city').append($("<option>", {value: index, html: value}));

                            });
                        }
                    });
                }
            });
//						$('.resedentialAddress-error-div').html('');
//		             	$('.resedentialAddress-error-div').addClass('hidden');
            CheckAddress('RES');
        });

        $("#PCity").on("change", function () {
            var PCity = $("#PCity option:selected").val();
            $.get("${applicationURL}getStateByCity?city=" + PCity, function (data, status) {
                if (status == "success" && data.length >= 2) {
//                    var json = JSON.parse(data);
                    $("#PState").val(data);
                } else {
                    $("#PState").val("");
                    $.get("${applicationURL}getAllCity", function (data) {
                        $('#PCity').empty();
                        $('#PCity').append("<option value=''>Select City</option>");
                        if (data != '') {
//                        console.log("data1", data);
                            $.each(data, function (index, value) {
                                $('#PCity').append($("<option>", {value: index, html: value}));

                            });
                        }
                    });
                }
            });
//					$('.permanentAddr-error-div').html('');
//	             	$('.permanentAddr-error-div').addClass('hidden');
            CheckAddress('PRES');
        });

        $("#OCity").on("change", function () {
            var OCity = $("#OCity option:selected").val();
            $.get("${applicationURL}getStateByCity?city=" + OCity, function (data, status) {
                if (status == "success" && data.length >= 2) {
//                    var json = JSON.parse(data);
                    $("#OState").val(data);
                } else {
                    $("#OState").val("");
                    $.get("${applicationURL}getAllCity", function (data) {
                        $('#OCity').empty();
                        $('#OCity').append("<option value=''>Select City</option>");
                        if (data != '') {
//                        console.log("data1", data);
                            $.each(data, function (index, value) {
                                $('#OCity').append($("<option>", {value: index, html: value}));

                            });
                        }
                    });
                }
            });
//					$('.companyAddr_div-error-div').html('');
//	             	$('.companyAddr_div-error-div').addClass('hidden');
            CheckAddress('COMP');
        });



        $('#pinCode').on('keyup', function () {
            residentpincode();
        });

        $('#PpinCode').on('keyup', function () {
            permanentpincode();
        });

        $('#OPincode').on('keyup', function () {
            companypincode();
        });

        $("#quickEnrollUser").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                enrollTitle: {
                    required: true,
                },
                enrollFname: {
                    required: true,
                },
                enrollLname: {
                    required: true,
                },
                enrollCity: {
                    required: true,
                },
                enrollemail: {
                    required: true,
                    emailValidation: true
                },
                enrollPhone: {
                    required: true,
                    digits: true,
                    mobileValidation: true
                },
                enrollDob: {
                    required: true
                },
                termsAndConditionsEnroll: {
                    required: true
                }
            },

            messages: {
                enrollTitle: {
                    required: "Please Select Title",
                },
                enrollFname: {
                    required: "Please Enter FirstName"
                },
                enrollLname: {
                    required: "Please Enter LastName",
                },
                enrollCity: {
                    required: "Please Enter City Name",
                },
                enrollemail: {
                    required: "Please Enter Email Id",
                    emailValidation: "Email ID is not valid."
                },
                enrollPhone: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid.",
                    mobileValidation: "Please Enter Valid Mobile Number"
                },
                enrollDob: {
                    required: "Please Enter Date of Birth",
                },
                termsAndConditionsEnroll: {
                    required: "Please accept the InterMiles membership Terms & Conditions to proceed."
                }
            },
            errorElement: "div",
            onfocusout: function (element) {
                this.element(element);  // <- "eager validation"
            },
            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },

            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                $('.error_box2').removeClass('hidden');
                error.insertAfter($(element).closest("div"));
//                console.log("----errr0rrr", error, "-----", error.length)
                $(error).appendTo('.error_box2 ul.error_list2');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');

            },
            submitHandler: function (form) {
                form.submit();
            },

        });

        $(document).click(function (event) {
            var len = $('.error_list > li:visible').length;
            if (!(len >= 1)) {
                $('.error_box').addClass('hidden');
            } else {
                $('.error_box').removeClass('hidden');
            }
        });
        $('.enroll_popup').click(function (event) {
            var len = $('.error_list2 > li:visible').length;
            if (!(len >= 1)) {
                $('.error_box2').addClass('hidden');
            } else {
                $('.error_box2').removeClass('hidden');
            }
        });
//        callenroll();
        function callenroll() {
            $(".error_box2").addClass('hidden');
            $('.enroll_popup').removeClass('hidden');
            $('.jpNum_popup').addClass('hidden');
            var fname = $("#fname").val();
            var mname = $("#mname").val();
            var lname = $("#lname").val();
            var phone = $("#mobile").val();
            var email = $("#email").val();
            var dob = $("#dateOfBirth").val();
            $("#enrollFname").val(fname);
            $("#enrollMname").val(mname);
            $("#enrollLname").val(lname);
            $("#enrollemail").val(email);
            $("#enrollPhone").val(phone);
            $("#enrollDob").val(dob);
        }
//        $("#quickEnroll").click(function () {
//            $(".error_box2").addClass('hidden');
//            $('.enroll_popup').removeClass('hidden');
//            $('.jpNum_popup').addClass('hidden');
//            var fname = $("#fname").val();
//            var mname = $("#mname").val();
//            var lname = $("#lname").val();
//            var phone = $("#mobile").val();
//            var email = $("#email").val();
//            var dob = $("#dateOfBirth").val();
//            $("#enrollFname").val(fname);
//            $("#enrollMname").val(mname);
//            $("#enrollLname").val(lname);
//            $("#enrollemail").val(email);
//            $("#enrollPhone").val(phone);
//            $("#enrollDob").val(dob);
//        });

//        $("#social-profiles").on('click', function () {
//            var fullName = "";
//            var fname = $("#fname").val();
//            var mname = $("#mname").val();
//            var lname = $("#lname").val();
//            var phone = $("#mobile").val();
//            var email = $("#email").val();
//            var formNumber = '${formNumber}';
//            var jetpriviligemembershipNumber = $("#jetpriviligemembershipNumber").val();
//            if (fname != "" || mname != "" || lname != "")
//                fullName = fname + " " + mname + " " + lname;
//            $("#socialName").val(fullName);
//            $("#socialPhone").val(phone);
//            $("#socialEmail").val(email);
//            $("#socialjpNumber").val(jetpriviligemembershipNumber);
//            $("#formNumber").val(formNumber);
//            $('#callMe_modal').modal('show');
//        });

        var nameflag, phoneflag, emailFlag;
        $("#social-profiles").on('click', function () {
            $('.error_box1').addClass('hidden');

            var fullName = "";
            var fname = $("#fname").val();
            var mname = $("#mname").val();
            var lname = $("#lname").val();
            var phone = $("#mobile").val();
            var email = $("#email").val();
            var socialName = $("#socialName").val();
            var socialPhone = $("#socialPhone").val();
            var socialEmail = $("#socialEmail").val();
//            var socialjpNumber=$("#socialjpNumber").val();

            var formNumber = '${formNumber}';
            var jetpriviligemembershipNumber = $("#jetpriviligemembershipNumber").val();
            if (fname != "" || mname != "" || lname != "")
                fullName = fname + " " + mname + " " + lname;
//            if($("#socialName").val() != ""){
//                $("#socialName").val(socialName);
//            }else{
//                $("#socialName").val(fullName);
//
//            }
//            if($("#socialPhone").val() != ""){
//                $("#socialPhone").val(socialPhone);
//            }else{
//                $("#socialPhone").val(phone);
//
//            }
//            if($("#socialEmail").val() != ""){
//                $("#socialEmail").val(socialEmail);
//            }else{
//                $("#socialEmail").val(email);
//
//            }
            if ($("#socialName").val() != "") {

                $('#socialName').click(function () {
                    nameflag = 1;
                });

                if (nameflag != 1) {

                    $("#socialName").val(fullName);
                } else {

                    $("#socialName").val(socialName);
                }


            } else {
                $("#socialName").val(fullName);

            }
            if ($("#socialPhone").val() != "") {
//                $("#socialPhone").val(socialPhone);

                $('#socialPhone').click(function () {
                    phoneflag = 1;
                });
                if (phoneflag != 1) {
                    $("#socialPhone").val(phone);
                } else {

                    $("#socialPhone").val(socialPhone);
                }
            } else {
                $("#socialPhone").val(phone);

            }
            if ($("#socialEmail").val() != "") {

                $('#socialEmail').click(function () {
                    emailFlag = 1;
                });
                if (emailFlag != 1) {

                    $("#socialEmail").val(email);
                } else {
                    $("#socialEmail").val(socialEmail);
                }


            } else {
                $("#socialEmail").val(email);

            }
//                            $('#callMeContine').attr('disabled',true)

            $("#formNumber").val(formNumber);
            $('#callMe_modal').modal('show');
//            if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
////                console.log("****")
//                $('#callMeContine').attr('disabled',true)
//            }else{
////                console.log("&&&&&&")
//               $('#callMeContine').attr('disabled',false)
//
//            }

        });
        $('#callmeclose').click(function () {
            $('.error_box1').addClass('hidden');
        });




        $('#enrollForm1,#enrollForm_mob_view1').on('click', function () {
            $(".error_box2").removeClass('hidden');
            var fees, cardname, cardimg, bankName;
            var id = $("#cpNo").val();
            var bpNo = '${cardBean.bpNo}';
            if ($("#quickEnrollUser").validate().element('#enrollTitle') && $("#quickEnrollUser").validate().element('#enrollFname')
                    && $("#quickEnrollUser").validate().element('#enrollLname') && $("#quickEnrollUser").validate().element('#enrollCity')
                    && $("#quickEnrollUser").validate().element('#enrollPhone') && $("#quickEnrollUser").validate().element('#enrollemail')
                    && $("#quickEnrollUser").validate().element('#enrollDob') && $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll'))
            {
                var data;


                var responsecode = $('#g-recaptcha-response').val()

                $.ajax({method: 'POST',
                    url: '${applicationURL}verifycaptcha?responsecode=' + responsecode, success: function (result) {


                        data = result;
                        if (data == false) {


                            $('.error_box2').removeClass('hidden');
//                            $('#error').html("Please select captcha").appendTo('.error_box1 ul.error_list1');
                            $('.error_box2 ul.error_list2').html("Please select captcha");
                            $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');
                        } else {

                            $('.error_box2').addClass('hidden');
                            $('.error_box2 ul.error_list2').html("");
                            //password hashing//
                            $("#enrollbpNo").val(bpNo);
                            //password hashing//

                            $('#enroll_error').addClass('hidden');
                            $('#loader').removeClass('hidden');
                            $('.modal-backdrop').css('z-index', '1050');
                            var checkbox = $("#termsAndConditionsEnroll");
                            checkbox.val(checkbox[0].checked ? "true" : "false");
                            var checkbox2 = $("#recieveMarketingTeam");
                            checkbox2.val(checkbox2[0].checked ? "true" : "false");
                            $.ajax({
                                url: '${applicationURL}enrollme',
                                data: $('#quickEnrollUser').serialize(),
                                type: 'POST',
                                success: function (data) {
                                    var jsonStr = JSON.parse(data);

                                    if (jsonStr.msg == "" && jsonStr.jpNumber != "" && jsonStr.jpNumber != "null") {
                                        var radioValue = $("#quickEnrollUser input[type='radio']:checked").val();
                                        $("#title").val($("#enrollTitle").val());
                                        $("#gender").val(radioValue);
                                        $("#fname").val($("#enrollFname").val());
                                        $("#mname").val($("#enrollMname").val());
                                        $("#lname").val($("#enrollLname").val());
                                        $("#email").val($("#enrollemail").val());
                                        $("#mobile").val($("#enrollPhone").val());
                                        $("#dateOfBirth").val($("#enrollDob").val());
                                        $("#jpNumber").val(jsonStr.jpNumber);
                                        $("#jetpriviligemembershipNumber").val(jsonStr.jpNumber);
                                        $("#cpNo").val(id);
                                        $('.enroll_popup').addClass('hidden');
                                        $('.jpNum_popup').removeClass('hidden');
                                        $("#jpNumber").trigger('keyup');
                                        $('form#amexBean').submit();

                                        //Adobe code starts here(Quick enrollment continue button)
                                        successEnrolClick();
                                        //Adobe code ends here

                                    } else {
                                        $('#loader').addClass('hidden');
                                        $('#enroll_error').removeClass('hidden');
                                        if (jsonStr.msg == "") {
                                            $('#enroll_error').text("Your application may not have gone through successfully. Please click 'Continue' again.");
                                        } else {
                                            $('#enroll_error').text(jsonStr.msg);
                                        }
                                        $('.modal-backdrop').css('z-index', '1048');
                                    }
                                },
                                complete: function () {
                                    $('#loader').addClass('hidden');
                                    $('.modal-backdrop').css('z-index', '1048');
                                },
                                error: function (xhrcallMe, status) {
                                    console.log(status);
                                    console.log(xhr.responseText);
                                }
                            });
                        }
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            } else {

                $('.error_box2').removeClass('hidden');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');

            }
        });


        $("#callMe").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                socialName: {
                    required: true,
                    AlphabetsOnly: true

                },
                socialPhone: {
                    required: true,
                    digits: true,
                    mobileValidation: true
                }
                ,
                socialEmail: {
//                    required: true,
                    emailValidation: true
                },
            },
            messages: {
                socialName: {
                    required: "Please Enter Full Name",
                    AlphabetsOnly: "Enter Characters Only"
                },
                socialPhone: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid.",
                    mobileValidation: "Please Enter Valid Mobile Number"
                },
                socialEmail: {
//                    required: "Please Enter Email Id",
                    emailValidation: "Email ID is not valid."
                },
            },
            errorElement: "div",
            onfocusout: function (element) {
                this.element(element);  // <- "eager validation"                   
            },

            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },
            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                $('.error_box1').removeClass('hidden');
                error.insertAfter($(element).closest("div"));
                $(error).appendTo('.error_box1 ul');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');
                
                //Adobe code starts here(phase 6)
                assistError = assistError+error.text()+",";
                $.fn.AssistMeError(assistError);
                console.log("Error at validator !!!",assistError);
               //Adobe code ends here(phase 6)
            },
            submitHandler: function (form) {
                form.submit();
            },
        });

        $('#callMeContine').on('click', function () {

            if ($("#callMe").validate().element('#socialName')
                    && $("#callMe").validate().element('#socialPhone')
                    && $("#callMe").validate().element('#socialEmail'))
            {
                $('#loader').removeClass('hidden');
                $('.modal-backdrop').css('z-index', '1050');
                $.ajax({
                    url: '${applicationURL}callme',
                    data: $('#callMe').serialize(),
                    type: 'POST',
                    success: function (data) {
                        if (data != '') {
                            $('#callMe_modal').modal('hide');
                            $('#successCallmeModal').modal('show');

                        } else {
                            $('#loader').addClass('hidden');
                            alert("sorry please try again");
                            $('.modal-backdrop').css('z-index', '1048');
                        }
                        
                        //Adobe code start (phase 6)
                        assistFormSubmitButton($("#socialName").val(),$("#socialPhone").val(),$("#socialEmail").val());
                        //Adobe code end (phase 6)
                    },
                    complete: function () {
                        $('#loader').addClass('hidden');
                        $('.modal-backdrop').css('z-index', '1048');
                        $("#socialName").val('');
                        $("#socialPhone").val('');
                        $("#socialEmail").val('');

                    },
                    error: function (xhr, status) {
                        console.log(status);
                        console.log(xhr.responseText);
                    }
                });
            } else {
                $('.error_box1').removeClass('hidden');

            }
        });

        $(".title_select").change(function () {
            if ($(this).find(":selected").text() == "Mr") {
                $("#quickEnrollUser input[type=radio][value=0]").prop('checked', true);

            }
            if ($(this).find(":selected").text() == "Ms" || $(this).find(":selected").text() == "Mrs") {
                $("#quickEnrollUser input[type=radio][value=1]").prop('checked', true);
            }
        });

        $('#enrollForm1,#enrollForm_mob_view1').on('click', function () {
            var len = $('.error_list2 > li:visible').length;
//            console.log("len " + len)
            if (len >= 1)
                $('.modal').animate({scrollTop: 0}, 'slow');
        });
        $("#enrollMname").on('focusout', function () {
            console.log("in focusout enrollDob")

//            $("#quickEnrollUser").validate().element('#enrollDob');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollDob").on('focusout', function () {
            console.log("in focusout enrollDob")

            $("#quickEnrollUser").validate().element('#enrollDob');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $(".ui-datepicker-trigger").click(function () {
            $('.error_box2').addClass('hidden');

        })
        $("#termsAndConditionsEnroll").on('focusout', function () {
            console.log("in focusout termsAndConditionsEnroll")

            $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollemail").on('focusout', function () {
            console.log("in focusout enrollemail")

            $("#quickEnrollUser").validate().element('#enrollemail');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollPhone").on('focusout', function () {
            console.log("in focusout enrollPhone")

            $("#quickEnrollUser").validate().element('#enrollPhone');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollLname").on('focusout', function () {
            console.log("in focusout enrollLname")

            $("#quickEnrollUser").validate().element('#enrollLname');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollTitle").on('focusout', function () {
            console.log("in focusout enrolltitle")

            $("#quickEnrollUser").validate().element('#enrollTitle');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollCity").on('focusout', function () {
            console.log("in focusout enrollcity")

            $("#quickEnrollUser").validate().element('#enrollCity');
            $('.error_box2').removeClass('hidden');

            var len = $('.error_list2 > li:visible').length;

            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }
        });
        $("#enrollFname").on('focusout', function () {
            console.log("in focusout enrollFname")

            $("#quickEnrollUser").validate().element('#enrollFname');
            $('.error_box2').removeClass('hidden');

            var len = $('.error_list2 > li:visible').length;

            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }
        });
        $("#socialName").on('focusout', function () {
            console.log("in focusout socialName")

            $("#callMe").validate().element('#socialName');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }


        });
        $("#socialPhone").on('focusout', function () {
            console.log("in focusout socialPhone")

            $("#callMe").validate().element('#socialPhone');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;

            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }
        });
        $("#socialEmail").on('focusout', function () {
            console.log("in focusout socialEmail")

            $("#callMe").validate().element('#socialEmail');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;

            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }
        });
//        $("#enrollTitle,#enrollCity,#enrollFname,#enrollLname,#enrollPhone,#enrollemail,#termsAndConditionsEnroll").on('focus', function () {
////            console.log("in focu of enroll")
//            $('.error_box2').addClass('hidden');
////            
//        })
//         $("#socialName,#socialPhone,#socialEmail").on('focusout', function () {
//if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
////                console.log("***!!!!*")
//                $('#callMeContine').attr('disabled',true)
//            }else{
////                console.log("&&&!!!!&&&")
//               $('#callMeContine').attr('disabled',false)
//
//            }            
//        });
//        $("#socialName,#socialPhone,#socialEmail").on('input change', function () {
//            if($("#socialName").is(':focus') || $("#socialPhone").is(':focus') || $("#socialEmail").is(':focus')){
//if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
////                console.log("*")
//                $('#callMeContine').attr('disabled',true)
//            }else{
////                console.log("&")
//               $('#callMeContine').attr('disabled',false)
//
//            } 
//        }
//        });
//$("#socialName,#socialPhone,#socialEmail").on('focus', function () {
//          
//            $('.error_box1').addClass('hidden');
////            
//        });
        function maxLength(value) {
            return /^.{1,24}$/.test(value);
        }

        function checkAddressLength(residentAddressField1, residentAddressField2, residentAddressField3) {
            if (residentAddressField1 != "" && !maxLength(residentAddressField1)) {
                $('.resedentialAddress-error-div').html('Address cannot be greater than 24 characters in each address line');
                $('.resedentialAddress-error-div').removeClass('hidden');
            }
            if (residentAddressField2 != "" && !maxLength(residentAddressField2)) {
                $('.resedentialAddress-error-div').html('Address cannot be greater than 24 characters in each address line');
                $('.resedentialAddress-error-div').removeClass('hidden');
            }
            if (residentAddressField3 != "" && !maxLength(residentAddressField3)) {
                $('.resedentialAddress-error-div').html('Address cannot be greater than 24 characters in each address line');
                $('.resedentialAddress-error-div').removeClass('hidden');
            }

        }

// 		function saveforamex(){
        //
        //$("#mobile").on("onkeyup",function(){
        var myTimer;
        var timer = "${timer}";
        var timer1 = "${timer1}";
        var saveformDataState = true;
        function SaveFormData()
        {
            mobile = $("#mobile").val();
            if (mobile != "" && mobile.length == 10 && saveformDataState == true) {
                saveformDataState = false;
//                console.log("****************STATE IS (false) :" + saveformDataState + " Time:" + Date());
//                console.log("##### Saving form data")
                $.ajax({
                    url: '${applicationURL}saveFormData',
                    data: $('#amexBean').serialize(),
                    type: "Post",
                    success: function (result) {
//                        console.log("-----3rd---->" + $("#emailInactiveFlag").val());
                        saveformDataState = true;
//                        console.log("****************STATE IS (true) :" + saveformDataState + " Time:" + Date());
                    },
                    error: function (request, status, error) {
                        saveformDataState = true;
//                        console.log("****************STATE IS (true) :" + saveformDataState + " Time:" + Date());
                    }
                });



            } else
            {
//                console.log('################### NOT SAVING ############################');
            }
        }
        function TimerSaveData()
        {
            mobile = $("#mobile").val();
            if (mobile != "" && mobile.length == 10) {
//                console.log("Timer is calling SaveFormData function");
                SaveFormData();
            } else
            {
                clearInterval(myTimer);
//                console.log('TIMER IS STOPPED');
            }
        }

        if (loggedInUser.length > 8)
        {
            //SaveForm data
            SaveFormData();
            //Start Timer
            if (mobile != "" && mobile.length == 10) {
//                console.log('TIMER IS STARTED:' + timer);

                myTimer = setInterval(TimerSaveData, timer);
            }
        }
        //});
//                }
        $("#emailInactiveFlag").val($("#emailInactiveFlag").val());
//        console.log("----1st----->" + $("#emailInactiveFlag").val());

        // New Code for 5 Mintues user idle ..
        var IDLE_TIMEOUT = 300; //seconds
        var _idleSecondsTimer = null;
        var _idleSecondsCounter = 0;
        var sendemailbit = 0;
        document.onclick = function () {
            _idleSecondsCounter = 0;
        };

        document.onmousemove = function () {
            _idleSecondsCounter = 0;
        };

        document.onkeypress = function () {
            _idleSecondsCounter = 0;
        };

        _idleSecondsTimer = window.setInterval(CheckIdleTime, 1000);

        function CheckIdleTime() {
            _idleSecondsCounter++;
            if (_idleSecondsCounter % 10 === 0)
            {
//                console.log('time elapsed MOD 10..' + _idleSecondsCounter);
            }
            if (_idleSecondsCounter >= IDLE_TIMEOUT) {

//                                console.log('User is idle for 5 Minutes');
                //window.clearInterval(_idleSecondsTimer);
                if (mobile != "" && mobile.length == 10) {
//                                 console.log('Mobile Triggered');
                    sendemailbit = 1;
                    $("#mobile").trigger('change');
                    _idleSecondsCounter = 0;

                } else
                {
                    _idleSecondsCounter = 0;
                    //sendemailbit=0
                }
            }
        }



        var mobile1 = '';
        var old;
        var arlene1 = [];
        var oldmobileno = "";
        var inactiveCounter = 0;
        $("#mobile").on("change", function () {
            mobile1 = $("#mobile").val();
//         console.log('test'+arlene1);
//         console.log('Mobile No: '+mobile1 +" OldMobileNo: "+oldmobileno );timer
            if (mobile1 != "" && mobile1.length == 10) {
                SaveFormData();//Saving Form Data
//                console.log('Tab out is calling SaveFormData function');
//                console.log('TIMER IS STARTED');
                myTimer = setInterval(TimerSaveData, timer); // Starting Timer           
//            if(mobile1!=old){
                if (oldmobileno != mobile1) // if oldmobile and new mobileno is not same then reset a timer to send an email
                {
//                    console.log("oldmobileno  ------", oldmobileno);
//                    console.log("mobile1 ------", mobile1);

                    inactiveCounter++;
                    timer1 = "${timer1}";
                }
                var index = arlene1.indexOf(mobile1);
//                   console.log("Mobile no exist in Array: "+index);
                if (index === -1) {
//                  console.log("Phone no array :"+arlene1)
                    oldmobileno = mobile1;
//                    console.log("oldmobileno   )))))", oldmobileno);
//                    console.log("mobile1))))))", mobile1);

                    if (sendemailbit == 1)
                    {
                        _idleSecondsCounter = 0;
                        sendemailbit = 0;
                        arlene1.push(oldmobileno);

//                   console.log('Email will be sent in : some time  '+timer1);

                        //var timers= setTimeout(function() {

//                       console.log('Silver Pop is get Called for..'+mobile1);
                        $.ajax({url: '${applicationURL}SilverpopAmex',
                            data: $('#amexBean').serialize(),
                            type: "Post",
                            success: function (result) {

//                                console.log("inactiveCounter", inactiveCounter);
                                if (inactiveCounter > 1) {

                                    $("#emailInactiveFlag").val('Y-' + (inactiveCounter - 1) + '');
                                } else {
                                    $("#emailInactiveFlag").val('Y');
                                }

//                                console.log("-----2nd---->" + $("#emailInactiveFlag").val());
                            },
                            error: {
                            }
                        });
//                               console.log("silver pop is calling");
                        oldmobileno = mobile1;
                        //timer1= 7200000;

                        //},timer1);

                    } else
                    {
//                     console.log('Idle time is not yet meet!!!');
                        _idleSecondsCounter;
                    }
                    //clearTimeout(timers);

                }


            } else {
                clearInterval(myTimer);
//                console.log('TIMER IS STOPPED');
            }

        });

        /*                    var mobile1='';
         var oldNumber; 
         var arlene1 = [];
         
         $("#mobile").on("change",function(){
         //        console.log("in change moobile")
         mobile1 = $("#mobile").val();
         if(mobile1!="" && mobile1.length==10){
         
         var index = arlene1.indexOf(mobile1);
         
         if(index == -1){
         
         oldNumber=mobile1;
         arlene1.push(oldNumber);
         setTimeout(function() {
         $.ajax({url : '${applicationURL}SilverpopAmex',
         data :$('#amexBean').serialize(),
         type : "Post",
         success : function(result) {
         },
         error:{
         }
         });
         
         },timer1);
         
         }
         
         }
         
         }); */

//        setInterval(function() {
//            mobile = $("#mobile").val();
//            if(mobile!="" && mobile.length==10){
//           	$.ajax({
//	  		        url : '${applicationURL}SilverpopAmex',
//	  		        data :$('#amexBean').serialize(),
//	  		        type : "Post",
//	  		        success : function(result) {
//	  		        },console.log("--------->"+$("#emailInactiveFlag").val());
//	  		        error:{
//	  		        }
//	  			});
//                }
//            },timer1);  
        if ($("#randomNumber").val() !== '') {
            $("#amexBean").submit();
        }
        var myVar,mNumber, timerId, time, successMsg;
        function startTimeout() {
            myVar = setTimeout(function () {
                $("#otpValue").val('');
                $("#otpValue").attr('disabled', true);

                //Adobe phase 5 code starts here
                $.fn.errorTrackOnPopup("OTP Expired");
                //Adobe phase 5 code ends here
            }, 900000);
        }

        function stopTimeout() {
            clearTimeout(myVar);
        }

        $(".genbut").click(function (event) {

//            $('.genbut').on('click','.deletelanguage',function(event){

            otpCounter++;
            event.preventDefault();
            var mobileNumber = $('#mobile').val();
            var formNumber = '${formNumber}';
            if (otpCounter > 3) {
                $('#otpValue').attr('disabled', true);
                $('#otpSucces').show();
                if (beJpNumber != "") {
                    $('#otpSucces').html("Kindly log into your InterMiles membership account and check your mobile number.");
                } else {
                   $('#otpSucces').html("Please Check Mobile Number.");
                }
                $('#jpNumpopup').attr('disabled', true);
                $("#jpNumpopup").css("color", "#ddd");
                $('#otpButton').attr('disabled', true);
                $("#otpButton").css("color", "#ddd");
                $(".error_msg_otp").addClass("hidden");
                var imageUrl = ctx + "static/images/icons/error_icon.png";
                $(".otpsuccess").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});

            } else {

                if ($('#otpButton').text() == 'Re-Generate OTP') {
                    console.log("otpTransactionId=generate===>", $("#otpTransactionId").val());
                    console.log("token==generate==>", $("#token").val());

                    $.ajax({url: "${applicationURL}regenerateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&token=" + $("#token").val() + "&otpTransactionId=" + $("#otpTransactionId").val(),
                        async: false,
                        success:
                                function (data) {
                                    stopTimeout();
                                    startTimeout();
                                    var myObj = JSON.parse(data);
                                    mNumber = myObj.mobileNumber;
                                    time = myObj.Time;
                                    var otpTransactionId = myObj.otpTransactionid;
                                    var token = myObj.token;
                                    console.log("token-reger-->", token);
                                    $("#pendingAttempt").val("");
                                    if ($("#pendingAttempt").val() != "") {
                                        if (beJpNumber != "") {
                                            var mNumberSet1 = mNumber.substring(2, 8);
                                            console.log("mNumberSet1", mNumberSet1);
                                            mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
                                            var mNumberSet0 = mNumber.substring(0, 2);
                                            console.log("mNumberSet0", mNumberSet0);
                                            var mNumberSet2 = mNumber.substring(8);
                                            console.log("mNumberSet2", mNumberSet2);
                                            var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
                                            successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                        } else {
                                            successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                        }

                                    } else {
                                        if (beJpNumber != "") {
                                            var mNumberSet1 = mNumber.substring(2, 8);
                                            console.log("mNumberSet1", mNumberSet1);
                                            mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
                                            var mNumberSet0 = mNumber.substring(0, 2);
                                            console.log("mNumberSet0", mNumberSet0);
                                            var mNumberSet2 = mNumber.substring(8);
                                            console.log("mNumberSet2", mNumberSet2);
                                            var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
                                            successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '.';

                                        } else {
                                            successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                        }

                                    }
                                    if (mNumber != "" && time != "") {
                                        var chkregenerateFlag = true;
//                                            var successMsg;
                                        $("#otpTransactionId").val(otpTransactionId);
                                        $("#token").val(token);
                                        var timeLeft = 30;
                                        timerId = setInterval(countdown, 1000);
                                        var activemsg;
                                        function countdown() {
                                            if (timeLeft == -1) {
                                                clearTimeout(timerId);
                                                clearTimeout(timerId);
                                                $('#otpactive').hide();
                                                $('#otpactive').html('');
                                                $('#otpButton').attr('disabled', false);
                                                $("#otpButton").css("color", "#fff");
                                            } else {
                                                activemsg = 'In case you have not received OTP please try regenerating OTP after ' + timeLeft + ' seconds ';
                                                $('#otpactive').show();
                                                $('#otpactive').html(activemsg);
                                                $('#otpButton').attr('disabled', true);
                                                $("#otpButton").css("color", "#ddd");
                                                timeLeft--;
                                            }
                                        }
                                        $('.geninput').attr('disabled', false);
                                        $('#otpButton').text('Re-Generate OTP');
                                        $('#jpNumpopup').attr('disabled', true);
                                        $("#jpNumpopup").css("color", "#ddd");
                                        $('#otpSucces').html(successMsg);
                                        $('#otpSucces').show();
                                        var imageUrl = ctx + "static/images/icons/success_icon.png";
                                        $("#otpSucces").css({"border": "2px solid #7dd62e", "background": "#f2fbe2 url(" + imageUrl + ") no-repeat 5px"});

                                    } else {
                                        otpCounter = 0;
                                    }
                                }
                    });
                } else {
                    $.ajax({url: "${applicationURL}generateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber,
                        async: false,
                        success:
                                function (data) {
                                    stopTimeout();
                                    startTimeout();
                                    var myObj = JSON.parse(data);
                                    mNumber = myObj.mobileNumber;
                                    time = myObj.Time;
                                    var otpTransactionId = myObj.otpTransactionid;
                                    var token = myObj.token;
                                    console.log("token-reger-->", token);
                                    $("#pendingAttempt").val("");
                                    if ($("#pendingAttempt").val() != "") {
                                        if (beJpNumber != "") {
                                            var mNumberSet1 = mNumber.substring(2, 8);
                                            console.log("mNumberSet1", mNumberSet1);
                                            mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
                                            var mNumberSet0 = mNumber.substring(0, 2);
                                            console.log("mNumberSet0", mNumberSet0);
                                            var mNumberSet2 = mNumber.substring(8);
                                            console.log("mNumberSet2", mNumberSet2);
                                            var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
                                            successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                        } else {
                                            successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                        }

                                    } else {
                                        if (beJpNumber != "") {
                                            var mNumberSet1 = mNumber.substring(2, 8);
                                            console.log("mNumberSet1", mNumberSet1);
                                            mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
                                            var mNumberSet0 = mNumber.substring(0, 2);
                                            console.log("mNumberSet0", mNumberSet0);
                                            var mNumberSet2 = mNumber.substring(8);
                                            console.log("mNumberSet2", mNumberSet2);
                                            var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
                                            successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '.';

                                        } else {
                                            successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                        }

                                    }
                                    if (mNumber != "" && time != "") {
                                        var chkregenerateFlag = true;
//                                            var successMsg;
                                        $("#otpTransactionId").val(otpTransactionId);
                                        $("#token").val(token);
                                        console.log("otpTransactionId=generate===>", $("#otpTransactionId").val());
                                        console.log("token==generate==>", $("#token").val());
                                        var timeLeft = 30;
                                        timerId = setInterval(countdown, 1000);
                                        var activemsg;
                                        function countdown() {
                                            if (timeLeft == -1) {
                                                clearTimeout(timerId);
                                                clearTimeout(timerId);
                                                $('#otpactive').hide();
                                                $('#otpactive').html('');
                                                $('#otpButton').attr('disabled', false);
                                                $("#otpButton").css("color", "#fff");
                                            } else {
                                                activemsg = 'In case you have not received OTP please try regenerating OTP after ' + timeLeft + ' seconds ';
                                                $('#otpactive').show();
                                                $('#otpactive').html(activemsg);
                                                $('#otpButton').attr('disabled', true);
                                                $("#otpButton").css("color", "#ddd");
                                                timeLeft--;
                                            }
                                        }
                                        $('.geninput').attr('disabled', false);
                                        $('#otpButton').text('Re-Generate OTP');
                                        $('#jpNumpopup').attr('disabled', true);
                                        $("#jpNumpopup").css("color", "#ddd");
                                        $('#otpSucces').html(successMsg);
                                        $('#otpSucces').show();
                                        var imageUrl = ctx + "static/images/icons/success_icon.png";
                                        $("#otpSucces").css({"border": "2px solid #7dd62e", "background": "#f2fbe2 url(" + imageUrl + ") no-repeat 5px"});

                                    } else {
                                        otpCounter = 0;
                                    }
                                }
                    });
                }

            }

        });


        $('.submit_btn').click(function () {
            //Adobe error tracking on submit button code starts here
            buttonClicked = true
            var sendError;

            sendError = setTimeout(function () {

                $('.error').each(function () {
                    if (!$(this).hasClass('hidden'))
                    {
                        if ($(this).text().length > 0) {
                            if (errorTrack.length > 0)
                            {
                                errorTrack = errorTrack + ", " + $(this).text();
                            } else
                            {
                                errorTrack = $(this).text();
                            }
                        }
                    }
                })

                // Adobe Code validation error function
                if (errorTrack.length > 0) {

                    if (IsButtonClicked !== true) {
                        $.fn.ErrorTracking(errorTrack, " ");
                    }
                } else
                {
//                        console.log("*********** NO ERROR")
                }

            }, 3000);

//            Adobe error tracking on submit button code ends here  



            residentpincode();
            companypincode();
            permanentpincode();
            checkcompany();
            checkresident();
            checkpermanent();


//            $("#promocode").trigger("focusout");
            //$(document).trigger('click'); 
            //CheckAddress('RES');
            //CheckAddress('PRES');
            //CheckAddress('COMP');

        });

//        $('form input, form select').focus(function () {
//            console.log("at form focus")
//            //Adobe code starts
//            buttonClicked=false;
//            //Adobe code ends
//            
//            
//            //$(document).trigger("click");
//            // $(document).trigger('click'); 
//            // for Resendetial Addresss
//            residentAddressField1 = $('.residentAddressField1').val();
//            residentAddressField2 = $('.residentAddressField2').val();
//            residentAddressField3 = $('.residentAddressField3').val();
//            city = $("#city").val();
//            state = $("#state1").val();
//            pinCode1 = $("#pinCode").val();
////                                             $('#resedentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
//            if (residentAddressField1 != "" || residentAddressField2 != "" || residentAddressField3 != "" || city != "" || state != "" || pinCode1 != "") {
//                $('#resedentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
//            }
//
//
//
////            var lResAddrss = $('#resedentialAddress').val()
////            var bRValue = /^[A-Za-z0-9-&,\/\s]*$/.test(lResAddrss)
//////          console.log('Residentail Address Valid (onfocus): '+bRValue);
////            if (!bRValue)
////            {
////
////                $('.resedentialAddress-error-div').removeClass('hidden');
////                $('.resedentialAddress-error-div').text("Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hyphens and ampersands.");
////
////
////            }
//
//
////            if(pinCode1 != ""){
////                console.log("pincode trigger function");
////                residentpincode();
////
////            }
//
//            //Company Address
//
//
//
//            residentAddressField1 = $('.companyAddressField1').val();
//            residentAddressField2 = $('.companyAddressField2').val();
//            residentAddressField3 = $('.companyAddressField3').val();
//            pin = $("#OPincode").val();
//            city = $("#OCity").val();
//            state = $("#OState").val();
////                                            $('#companyAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//            if (residentAddressField1 != "" || residentAddressField2 != "" || residentAddressField3 != "" || city != "" || state != "" || pin != "") {
//                $('#companyAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//            }
//
////            var lComAddrss = $('#companyAddress').val()
////            var bCValue = /^[A-Za-z0-9#',\/\s]*$/.test(lComAddrss)
//////          console.log('Company Address Valid (onfocus): '+bCValue);
////
////            if (!bCValue)
////            {
////                $('.companyAddr_div-error-div').removeClass('hidden');
////                $('.companyAddr_div-error-div').text("Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hyphens and ampersands.");
////
////
////            }
//
////                  if(pin != ""){
////                console.log("pincode company function");
////                companypincode();
////
////            }
//            // PermananetAddress
//            residentAddressField1 = $('.permanent_AddressField1').val();
//            residentAddressField2 = $('.permanent_AddressField2').val();
//            residentAddressField3 = $('.permanent_AddressField3').val();
//            pin = $("#PpinCode").val();
//            city = $("#PCity").val();
//            state = $("#PState").val();
////                                            $('#permanentAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//            if (residentAddressField1 != "" || residentAddressField2 != "" || residentAddressField3 != "" || city != "" || state != "" || pin != "") {
//                $('#permanentAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//            }
//
//
//
////            var lPResAddrss = $('#permanentAddress').val()
////            var bPRValue = /^[A-Za-z0-9-&,\/\s]*$/.test(lPResAddrss)
//////          console.log('Permanant Residentail Address Valid (onfocus): '+bPRValue);
////
////            if (!bPRValue)
////            {
////                var radioValue = $(".addressProof input[type='radio']:checked").val();
////                if (radioValue === 'No')
////                {
////                    $('.permanentAddr-error-div').removeClass('hidden');
////                    $('.permanentAddr-error-div').text("Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hyphens and ampersands.");
//////
////                }
////            }
//
//
////                var radioValue = $(".addressProof input[type='radio']:checked").val();
////                if (radioValue === 'No')
////                {
////                if(pin != ""){
////                console.log("perment pincode trigger gggggg function");
////                permanentpincode();
////               }
////            }
//        });
    });

    function checkpermanent() {
        var residentAddressField1 = $('.permanent_AddressField1').val();
        var residentAddressField2 = $('.permanent_AddressField2').val();
        var residentAddressField3 = $('.permanent_AddressField3').val();
        pin = $("#PpinCode").val();
        city = $("#PCity").val();
        state = $("#PState").val();
        var radioValue = $(".addressProof input[type='radio']:checked").val();
//        console.log("submit btn===" + radioValue);
        var lPResAddrss = $('#permanentAddress').val()
        if (radioValue === 'No')
        {



            if (residentAddressField1 != "") {
//                    var a =/^.{1,24}$/.test(lPResAddrss);
                var a = residentAddressField1.length;
//                console.log("1--->" + a);
                if (a > 24) {
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Address cannot be greater than 24 characters in each address line");

                }

//                    else{
//                        
//                        $('.permanentAddr-error-div').addClass('hidden');
//                    $('.permanentAddr-error-div').text("");
//                    
//                    }
            }
            if (residentAddressField2 != "") {
//                    var a =/^.{1,24}$/.test(lPResAddrss);
                var b = residentAddressField2.length;
//                console.log("1--->" + b);
                if (b > 24) {
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Address cannot be greater than 24 characters in each address line");

                }
//                    else{
//                        
//                        $('.permanentAddr-error-div').addClass('hidden');
//                    $('.permanentAddr-error-div').text("");
//                    }
            }
            if (residentAddressField3 != "") {
//                    var a =/^.{1,24}$/.test(lPResAddrss);
                var c = residentAddressField3.length;
//                console.log("1--->" + c);
                if (c > 24) {
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Address cannot be greater than 24 characters in each address line");

                }
//                    else{
//                        $('.permanentAddr-error-div').addClass('hidden');
//                    $('.permanentAddr-error-div').text("");
//                    }
            }
        }
    }

    function checkresident() {

        var residentAddressField1 = $('.residentAddressField1').val();
        var residentAddressField2 = $('.residentAddressField2').val();
        var residentAddressField3 = $('.residentAddressField3').val();
        pin = $(".residentAddress .pincode").val();
        city = $("#city").val();
        state = $("#state1").val();

        console.log("on submit resident handlewe" + city);

        var lPResAddrss = $('#resedentialAddress').val()

        if (residentAddressField1 != "") {
//                    var a =/^.{1,24}$/.test(lPResAddrss);
            var a = residentAddressField1.length;
//            console.log("1--->" + a);
            if (a > 24) {
                $('.resedentialAddress-error-div').removeClass('hidden');
                $('.resedentialAddress-error-div').html("Address cannot be greater than 24 characters in each address line");

            }

        } else if (residentAddressField2 != "") {
//                    var a =/^.{1,24}$/.test(lPResAddrss);
            var b = residentAddressField2.length;
//            console.log("1--->" + b);
            if (b > 24) {
                $('.resedentialAddress-error-div').removeClass('hidden');
                $('.resedentialAddress-error-div').html("Address cannot be greater than 24 characters in each address line");

            }


        } else if (residentAddressField3 != "") {
//                    var a =/^.{1,24}$/.test(lPResAddrss);
            var c = residentAddressField3.length;
//            console.log("1--->" + c);
            if (c > 24) {
                $('.resedentialAddress-error-div').removeClass('hidden');
                $('.resedentialAddress-error-div').html("Address cannot be greater than 24 characters in each address line");

            }

        }

    }

    function checkcompany() {

        var residentAddressField1 = $('.companyAddressField1').val();
        var residentAddressField2 = $('.companyAddressField2').val();
        var residentAddressField3 = $('.companyAddressField3').val();
        pin = $(".companyAddress .pincode").val();
        city = $("#OCity").val();
        state = $("#OState").val();

        var lPResAddrss = $('#companyAddress').val()

        if (residentAddressField1 != "") {
//                    var a =/^.{1,24}$/.test(lPResAddrss);
            var a = residentAddressField1.length;
//            console.log("1--->" + a);
            if (a > 24) {
                $('.companyAddr_div-error-div').removeClass('hidden');
                $('.companyAddr_div-error-div').html("Address cannot be greater than 24 characters in each address line");

            }

        }
        if (residentAddressField2 != "") {
//                    var a =/^.{1,24}$/.test(lPResAddrss);
            var b = residentAddressField2.length;
//            console.log("1--->" + b);
            if (b > 24) {
                $('.companyAddr_div-error-div').removeClass('hidden');
                $('.companyAddr_div-error-div').html("Address cannot be greater than 24 characters in each address line");

            }

        }
        if (residentAddressField3 != "") {
//                    var a =/^.{1,24}$/.test(lPResAddrss);
            var c = residentAddressField3.length;
//            console.log("1--->" + c);/
            if (c > 24) {
                $('.companyAddr_div-error-div').removeClass('hidden');
                $('.companyAddr_div-error-div').html("Address cannot be greater than 24 characters in each address line");

            }

        }

    }

    function CheckAddress(addrDetails)
    {
        //$(document).trigger("click");

        // $(document).trigger('click'); 

        if (addrDetails == 'RES')
        {
//            residentpincode();
            residentAddressField1 = $('.residentAddressField1').val();
            residentAddressField2 = $('.residentAddressField2').val();
            residentAddressField3 = $('.residentAddressField3').val();
            city = $("#city").val();
            state = $("#state1").val();
            pinCode1 = $("#pinCode").val();
            $('#resedentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);


            var lResAddrss = $('#resedentialAddress').val()
            var bRValue = /^[A-Za-z0-9-&,\/\s]*$/.test(lResAddrss)

            if (bRValue)
            {
                $('.resedentialAddress-error-div').html('');  //  CheckAddress('RES')
                $('.resedentialAddress-error-div').addClass('hidden');
            }

        }
        if (addrDetails == 'PRES')
        {
//            permanentpincode();
            residentAddressField1 = $('.permanent_AddressField1').val();
            residentAddressField2 = $('.permanent_AddressField2').val();
            residentAddressField3 = $('.permanent_AddressField3').val();
            pin = $("#PpinCode").val();
            city = $("#PCity").val();
            state = $("#PState").val();
            $('#permanentAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
            var lPResAddrss = $('#permanentAddress').val()
            var bPRValue = /^[A-Za-z0-9-&,\/\s]*$/.test(lPResAddrss)


            if (bPRValue)
            {

                $('.permanentAddr-error-div').html('');  // CheckAddress('PRES');
                $('.permanentAddr-error-div').addClass('hidden');

            }

        }
        if (addrDetails == 'COMP')
        {
//            companypincode();
            residentAddressField1 = $('.companyAddressField1').val();
            residentAddressField2 = $('.companyAddressField2').val();
            residentAddressField3 = $('.companyAddressField3').val();
            pin = $("#OPincode").val();
            city = $("#OCity").val();
            state = $("#OState").val();
            $('#companyAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);


            var lComAddrss = $('#companyAddress').val()
            var bCValue = /^[A-Za-z0-9-&,\/\s]*$/.test(lComAddrss)

            if (bCValue)
            {

                $('.companyAddr_div-error-div').html('');  // CheckAddress('COMP');
                $('.companyAddr_div-error-div').addClass('hidden');
            }

        }
    }


//             $("#jpNumpopup").click(function(){
//                 //Adobe code starts here
//                 $.fn.AfterApplyJPNumberPopUpClick($("#jpNumber").val(),'After submitting JpNumber click function');
//                 //Adobe code ends here
//             });
// 



    





    //Adobe code starts here
    function pageInfo() {
        
        //Adobe code phase6 start
          var callmeflag ='${callMeFlag}';
          if(callmeflag == 1){
              setTimeout(function()
              { 
                $.fn.ViewOfAssistMe() 
              }, 3000);
          }
        //Adobe code phase6 ends

        selectedCity = $('#city').val();

        $('form input, form select').click(function () {
            lastAccessedField = $(this).attr('name');
        });

        var amexname = document.URL.substr(document.URL.lastIndexOf('/') + 1);
        var amexDetail = "";

        if (amexname == "Jet-Airways-American-Express-Platinum-Credit-Card") {
            amexDetail = "Amex-Platinum";
        }
        
        
        
        
        
        //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----applyamexcard--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----applyamexcard--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----applyamexcard--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----applyamexcard--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----applyamexcard--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----applyamexcard--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----applyamexcard--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----applyamexcard-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----applyamexcard--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End
                    
                     
                     

        digitalData.pageInfo = {"pageName": "" + amexDetail, "category": "Cards", "subCategory1": "App-Form", "subCategory2": "" + amexDetail, "partner": "JP", "currency": "INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
        digitalData.formInfo = {"formName": "Amex card application form", "lastAccessedField": lastAccessedField, "name": "", "email": "", "mobile": "", "currentCity": "", "pincode": ""}

        //Form abandon
        var checkCloseX = 0;
        $(document).mousemove(function (e) {
            if (e.pageY <= 5) {
                checkCloseX = 1;
                formAbandonType = "Browser close";
            } else {
                checkCloseX = 0;
                formAbandonType = "Page Refresh";
            }
        });

        window.onbeforeunload = function (event) {
            
            if (checkCloseX == 1) 
            {
                formAbandonType="Browser close";
            }  
            
            if (!IsButtonClicked && assistMeFlag == false)
            { 
                var bankName = "Amex";
                $.fn.FormAbandonAmexClick(lastAccessedField, digitalData.formInfo.name, digitalData.formInfo.email, digitalData.formInfo.mobile, digitalData.formInfo.currentCity, digitalData.formInfo.pincode, bankName)
            }
                    
            if(IsButtonClicked)
            { 
                console.log("---formAbandonType is----",formAbandonType)
                $.fn.formAbandonmentOnSubmitJPnumberPopupAmex(formAbandonType,abandonFormName,amexCardName,amexVariantName);
            }  
                
            if(assistMeFlag == true)
            {
                $.fn.FormAbandonmentOfAssistMe(name,mobile,email,lastAccessedField,"page refresh");
            } 

        };
        //Form Abandonment close
    }
    // Code For PageAbandon
    $('form input, form select').click(function () {
        //Adobe start
        lastAccessedField = $(this).attr('name');
        //Adobe end

        var fname = $("#fname").val();
        var mname = $("#mname").val();
        var lname = $("#lname").val();
        var fullName = fname + mname + lname;
        var phone = $("#mobile").val();
        var email = $("#email").val();
        var city = $("#city").val();
        var pincode = $("#pinCode").val();

        var jsonFormData = {"fullName": fullName, "phone": phone, "email": email, "city": city, "pincode": pincode}
        var encrFormData = encryptedData(jsonFormData);
        var encrFormData1 = JSON.parse(encrFormData);
        var encrName = encrFormData1.fullName;
        var encrPhone = encrFormData1.phone;
        var encrEmail = encrFormData1.email;
        var encrCity = encrFormData1.city;
        var encrPincode = encrFormData1.pincode;

        digitalData.formInfo = {"formName": "Amex card application form", "lastAccessedField": lastAccessedField, "name": encrName, "email": encrEmail, "mobile": encrPhone, "currentCity": encrCity, "pincode": encrPincode}
    });
    //Adobe code ends here      
</script>

<!--Form Abandonment-->
<script type='text/javascript'>
    //Values to set for form abandonment  
    function encryptedData(jsonToEncrypt) {
        var encryptedJson;
        $.ajax(
                {method: 'GET',
                    async: false,
                    url: ctx + "encryptedAdobe?jsonData=" + JSON.stringify(jsonToEncrypt),
                    success: function (result) {
                        encryptedJson = JSON.stringify(result);

                    }});

        return encryptedJson;

    }

    function afterApplyAmexSubmitClick(jpNumber) {
        IsButtonClicked = false;
        submitTime = new Date();

        var timeElapsed = Math.abs(submitTime - genTime);
        var diffCreatedMS = (submitTime - genTime) / 1000;
        var diffCreatedSec = diffCreatedMS % 60;
        $.fn.AfterApplyJPNumberPopUpClick(jpNumber, 'JP Number', diffCreatedSec);
    }

    function moreDetailsClick(cardName, variant) {
        var cardName1 = cardName.replace("�", "");
        var variant1 = variant.replace("�", "");

        $.fn.MoreDetailsForCardsClick(cardName1, variant1);
    }

    function successEnrolClick() {
        var jpNum = $("#jpNumber").val();
        $.fn.successfulQuickEnrollment(jpNum, amexCardName, amexVariantName);
    }

    function submitAmexFormClick(fullName, email, mobile, city, pincode, bankName, popupType) {
        abandonFormName = popupType;
        popupAbandonType = popupType;
        $.fn.ApplyButtonAmexFormClick(fullName, email, mobile, city, pincode, bankName);
        var iciciURL = document.URL.substr(document.URL.lastIndexOf('/') + 1);
		if(iciciURL.indexOf('-') !== -1) iciciURL = iciciURL.replaceAll('-',' ').trim()
		var cardName = iciciURL.split('American Express')[1];
		$.fn.actionClickCT('Card Application: Submit',cardName,'American Express')
    }

    function afterEnrollHereClick(enrollName, popupType) {
        abandonFormName = popupType;
        popupAbandonType = popupType;
        $.fn.AfterApplyEnrolPopUpClick(enrollName);
    }

    function termsAndConditionClick(cardName, variantName) {
        amexCardName = cardName.replace("<sup>&reg;</sup>", "");
        amexVariantName = variantName.replace("<sup>&reg;</sup>", "");
    }

    function formAbandonmentOnPopUp(closeType) {
        $.fn.formAbandonmentOnSubmitJPnumberPopupAmex(closeType, popupAbandonType, amexCardName, amexVariantName);
    }

    //Adobe phase 5 code starts
    $("#otpButton").click(function () {
        var buttonName = $("#otpButton").text();
        genTime = new Date();
        var jpNum = $("#jpNumber").val();
        if (buttonName == "Generate OTP") {
            $.fn.ClicksOnGenerateOtp(jpNum, amexCardName, amexVariantName);
        } else {
            $.fn.ClicksOnResendOtp(jpNum);
        }

    });
    //Adobe phase 5 ends
    
    
    //submit button assist me form(phase 6 start)
     function assistMeeClick(){
         assistMeFlag = true;
         $.fn.ClickOfAssistMe();
     }
     
     function assistFormSubmitButton(name,mobile,email){
         if(name !== "" && mobile !== ""){
             name = "name:yes";
             mobile = "mobile:yes";
         }else{
             name = "name:no";
             mobile = "mobile:no";
         }
         if(email !== ""){
             email = "email:yes";
         }else{
             email = "email:no";
         }
         $.fn.ClickOfAssistContinueButton(name,mobile,email);
     }
     
     function formAbandonOfAssistMe(){
         var abandonType = "pop-up close";
         if(lastAccessedField == "socialName"){
            lastAccessedField="Name";
         }else if(lastAccessedField == "socialPhone"){
            lastAccessedField="Mobile";
         }else if(lastAccessedField == "socialEmail"){
            lastAccessedField="Email";
         }
         $.fn.FormAbandonmentOfAssistMe(name,email,mobile,lastAccessedField,abandonType);
     }
     //(phase 6 end)


</script> 

