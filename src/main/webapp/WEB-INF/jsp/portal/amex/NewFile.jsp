<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<section class="container clearfix bottomPadding">
               <section class="main-section">
                  <div class="msgWrapper text-center">
                     <div class="msgDenied_div margT15">
                        <div class="pending-icon"><img src="${applicationURL}static/images/co-brand/loading_icon.png" alt=""></div>
                        <h1>APPROVAL PENDING</h1>
                       <p class="margT2"> Thank you for applying <c:out value="${cardName}"></c:out>.</p> 

						<p class="margT2">We have successfully received your application and its currently in processing. We will finalise your application as soon as possible and notify you on the outcome within 10 business days.</p>	

						<p class="margT2">	In meanwhile, if you have any questions please call us on 24 hour customer service number (0124) 2801111 or 1800 419 1120 (Toll Free). Kindly quote the reference number <c:out value="${pcnNumber}"></c:out>. and we will be happy to assist you.</p>
                        
                       
                     </div>
					
                     <div>
                        <a href="${applicationURL}home"><button class="buttonBlue3 buttonBlue_rec margT5">View Other Cards</button></a>
                     </div>
                  </div>
               </section>
            </section>