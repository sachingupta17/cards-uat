<%-- 
    Document   : faq
    Created on : 20 Oct, 2019, 10:37:03 PM
    Author     : arvind
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<section class="innerContainer clearfix bottomPadding">
        <section class="main-section">
            <div class="banner" style="background:url('https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/banners/Cards_1291px_364px_2_1.jpg') no-repeat;background-size: cover;" title="InterMiles Co-brand Credit Cards, Debit Cards &amp; Corporate Cards">
                <div class="container">
                    <div class="banner_textarea">
                        <h1 style="font-weight: bold"> <strong>Power-Packed Benefits with InterMiles Co-Brand Cards</strong> </h1><br><p style="color:white;font-size: 22px;font-weight:bold">Use your Co-brand Cards to accrue InterMiles on all your spends & redeem them for exciting travel and lifestyle rewards!  </p>
                        </h1>
                        
                       
                    </div>
                </div>
            </div>
        </section>

                
    
    <!--style="color: #28262c;font-weight: 500;display: inline-block;"-->
        <!-- banner end --> 
        <div class="clearfix"></div>
        <!-- content start -->  
        <section class="container">
         

            <div class="card_listing_page">
                <div class="cont_area">
                   
                    <div class="que" >Q. What is InterMiles?</div>
                    <div class="ans" >A: InterMiles is one of the most rewarding Travel & Lifestyle programmes you will ever need. The currency of this programme is also called InterMiles. At InterMiles every mile count, so the more InterMiles you accumulate the higher your Tier and the more your Tier benefits. So, move up with every Swipe! Accumulate InterMiles faster by booking flights & hotels with us or by earning miles with over 150+ partners in the InterMiles program. Also, you can use your InterMiles everywhere – Redeem your InterMiles for flight tickets across any airline, for your hotel stays, free fuel or for thousands of merchandise options on our Reward Store!</div>

                        
                    <div class="que">Q. What benefits do I get on InterMiles Co-brand Credit card?</div>   
                    <div class="ans">A: Your credit cards now come packed with benefits like Tier match, Discount on Flights and Hotels, accelerated earnings on Flights, Hotels, Discounts on Etihad economy and business class apart from Lounge access, Movie ticket vouchers etc. Please check our card variants and benefits associated with each variant on https://cards.intermiles.com/ </div>
                    
                    <div class="que">Q. When will I get a new InterMiles Co-brand Credit Card?</div>
                    <div class="ans">A: If you have an existing Jet Privilege or Jet Airways card, it would be replaced with the new InterMiles card. You can contact your bank to know the status of your new InterMiles Co-brand credit card.</div>

                    
                    <div class="que">Q. I want to apply for an InterMiles credit card. How do I apply for one?</div>
                    <div class="ans">A: Our banking partners will initiate issuance of new cards soon. You can drop a mail on  memberservices@intermiles.com with your contact details and preferred card. Also, keep checking the Cards section on InterMiles.com for regular updates. </div>

                    
                     <div class="que"> Q.If I am Jet Privilege / Jet Airways card holder, do I need to enrol again into the InterMiles Program?</div>
                    <div class="ans">A: All the members who are enrolled into the Jet Privilege program are now InterMiles members by default. You do not need to enrol again into the InterMiles Program.</div>

                    <div class="que">Q. What happens to my accumulated JPMiles with new InterMiles Co-brand credit card and program? </div>
                    <div class="ans">A: Your miles are now called InterMiles. All your accumulated miles will continue being available to you, just as they are, without any change in your InterMiles account.</div>

                    
                     <div class="que">Q. I used to get Tier Points on my credit card spends which enabled me to get a higher tier. Do I still earn Tier Points on credit card spends?</div>
                    <div class="ans">A: You no longer need Tier Points to attain / retain a Tier since we have simplified the InterMiles program. You can now attain / retain a tier by accumulating InterMiles. The more you spend, the more InterMiles you will earn, the higher your InterMiles Tier. So, move up with every swipe!</div>

                     <div class="que">Q. Can my Cobrand card spends help me achieve an InterMiles Tier?</div>
                    <div class="ans">A: Yes! You can now attain / retain a tier by accumulating InterMiles. Your Cobrand card helps you earn additional Miles on every transaction. The more you spend, the more InterMiles you will earn, the higher your InterMiles Tier. So, move up with every swipe!</div>

                     <div class="que">Q. Do Bonus InterMiles on co brand card account for Tier upgrade?</div>
                    <div class="ans">A: Every mile you earn, now counts towards a tier upgrade. </div>

                      <div class="que">Q. What about the Jet Airways benefits I have on my card?</div>
                    <div class="ans">A: All the Jet Airways benefits on your Co-brand card now stand suspended and your card now comes with new power packed features. </div>

                    
                    <div class="que">Q. I have received a VISA variant card in replacement of my Mastercard variant of Jet Privilege HDFC Bank World / Platinum / Titanium co-brand credit card. Does it impact the benefits I used to get with the Mastercard variant?</div>
                    <div class="ans">A: All the benefits which were associated with your Jet Privilege HDFC Bank Mastercard variant are now transferred to the VISA variant.   </div>


                  <!--   <div class="que">Q. What are the benefits on the InterMiles card? </div>
                    <div class="ans">A: Your credit cards now come packed with benefits like Tier match, Discount on flights, 2X bonus miles earnings on flights, Discounts on Etihad economy and business class apart from Lounge access, Movie ticket vouchers etc. Please check our card variants and benefits associated with each variant on cards.intermiles.com
                  <br><br>All benefits associated to Jet Airways are no longer available owing to suspension of the Airline’s operations. <br>
                    </div>
                    
                    <div class="que">Q. What about the Jet Airway benefits I have on my card?</div>
                    <div class="ans">A: Benefits associated to Jet Airways are no longer available on our cards owing to suspension of the Airline’s operations. </div>

                    <br>
                    <br>
                    <br>
                     <div class="que">  <h4><u>Benefits Terms & Conditions</u></h4></div>
                    
                    <h4>2X on Flights.InterMiles.com</h4> 
<div class="que">Who is eligible:</div>
<div class="ans">All co-brands members with a co-brand, applying or renewing a co-brand Credit card by paying fees will be eligible to get accelerated/bonus InterMiles on every eligible activity on https://flights.intermiles.com who hold the following co-brand credit cards
<ul style="list-style-type:circle;">
<li>Jet Airways American Express Platinum Credit Card</li>
<li>Jet Privilege HDFC Bank Select Credit Card</li>
<li>Jet Privilege HDFC Bank Platinum Credit Card</li>
<li>Jet Privilege HDFC Bank Signature Credit Card</li>
<li>Jet Privilege HDFC Bank Diners Credit Card</li>
<li>Jet Privilege HDFC Bank Signature Debit Card</li>
<li> InterMilesICICI Bank Coral Visa Credit Card/ Jet Airways ICICI Bank Coral Visa Credit Card</li>
<li>InterMiless ICICI Bank Coral Amex Credit Card/Jet Airway ICICI Bank Coral Amex Credit Card</li>
<li>InterMiles ICICI Bank Rubyx Visa Credit Card/ Jet Airway ICICI Bank Rubyx Visa Credit Card</li>
<li>InterMiles ICICI Bank Rubyx Amex Credit Card/ Jet Airway ICICI Bank Rubyx Amex Credit Card</li>
<li>InterMiles ICICI Bank Sapphiro Visa Credit Card/ Jet Airway ICICI Bank Sapphiro Visa Credit Card</li>
<li>InterMiles ICICI Bank Sapphiro Amex Credit Card/ Jet Airway ICICI Bank Sapphiro Amex Credit Card</li>
<li>InterMiles IndusInd Bank Odyssey Visa Credit Card/ Jet Airway IndusInd Bank Odyssey Visa Credit Card</li>
<li>InterMiles IndusInd Bank Odyssey Amex Credit Card/ Jet Airway IndusInd Bank Odyssey Amex Credit Card</li>
<li>InterMiles IndusInd Bank Voyage Visa Credit Card/Jet Airway IndusInd Bank Voyage Visa Credit Card</li>
<li>InterMiles IndusInd Bank Voyage Amex Credit Card/ Jet Airway IndusInd Bank Voyage Amex Credit Card</li>
<li>InterMiles ICICI Bank Business Advantage Corporate Card</li>
<li>InterMiles American Express Corporate Credit Card </li>
</ul></div>



<div class="que">Q. What is the 2x benefit?</div>
<div class="ans">A: This is an ongoing benefit; all members will be eligible to earn 2X bonus InterMiles</div>
<div class="que">Q. When do I get the 2x benefit?</div> 
<div class="ans">A: Post 60 days of your flight activity</div>  
<div class="que">Q. On which flights can I utilize the benefit?</div>
<div class="ans">A: You can utilize this discount on any flights being booked on https://flights.intermiles.com subject to terms and conditions.</div>
<div class="que">Q. How do I use this Benefit?</div>

<div class="ans">
    <ol type="a">A:
<li>Visit flights.intermiles.com 
<li>Initiate and complete your booking using your co-brand credit card
<li>If you fail to use the co-brand as per the selection, you will not be eligible to get the bonus InterMiles 
<li>60 Days post a successful flight activity, the bonus InterMiles will be credited to your InterMiles account Bonus InterMiles 
<li>Supplementary credit card holder’s Bonus InterMiles Spends will be credited into primary cardholder’s JetPrivilege membership account
<li> In event of a ticket cancellation proportionate Bonus InterMiles will be negated from the cardholder’s account
</ol>
</div>
<br>

<h4>Flight Discount Voucher on Flights.InterMiles.com</h4>

<div class="que">Q. Who is eligible:</div>
<div class="ans">A: All co-brands members applying or renewing a Co-brand Credit Card by paying fees will be eligible to get a One-time discount voucher valid on bookings made on flights.intermiles.com </div>
<div class="que">Q. What is the benefit?</div>
<div class="ans">A: This is a single use voucher as a flat refund with no fare, sector or amount restriction</div>
<div class="que">Q. When do I get the benefit? </div>
<div class="ans">A: Post 60 days of your renewal/ enrolment fee being realized, you will be eligible to use this benefit </div>
<div class="que">Q. On which flights can I utilize the benefit?</div>
<div class="ans">A: You can utilize this discount on any flights being booked on https://intermiles.com subject to terms and conditions.</div>


<div class="que">Q. How do I use this Benefit?</div>
<div class="ans">
<ol type="g">A:
<li>Visit flights.intermiles.com</li> 
<li>Initiate a return booking for any domestic or international flight booking</li>
<li>Use your co-brand credit card to avail the one-time discount </li>
<li>If you fail to use the co-brand as per the selection, the discount will not be credited to your co-brand card account </li>
<li>30 Days post a successful flight activity, the offer amount will be refunded to your co-brand card account  </li>
<li>This amount will reflect as “Refund’’ in your co-brand card statement</li>

</ol>
</div>

<div classs="que">
    <h4>Terms and Conditions</h4></div>
<div class="ans">
<ol type="a"><li>

<li>The Flight Discount voucher will refer to as a ‘refund’ in your credit card statement.</li>

<li>This Flight Discount voucher is a one time utilization per year voucher only and can be used on https://flights.intermiles.com only

<li>The discount is valid for a return ticket only
</li>

<li>The flight voucher is applicable for both Domestic and International Flights being booked on intermiles.com
</li>

<li>The flight voucher once availed will not be reinstated irrespective of whether member uses the flight ticket or not.
</li>

<li>The discount is offered by InterMiles as a program benefit on the co-brand card and is subject to change the discretion and/or along with the change in the program T&C’s. </li>

<li>InterMiles T&C’s apply
</li>

<li>This offer cannot be clubbed with any other offer
 </li>

<li> Bank T&C’s apply.
</li>


</ol>
</div>

<h4>Miles Back</h4>

<div class="que">Q. Who is eligible:</div>

<div class="ans">A: All co-brands members holding / applying or renewing a Co-brand Credit Card will be eligible to get a One-time Miles back for International redemption bookings being made on flights.intermiles.com</div>

<div class="que">Q. When can I use the benefit?</div>

<div class="ans">A: Any time during the offer period </div>

<div class="que">Q. On which flights can I utilize the benefit?</div>

<div class="ans">A: You can utilize this benefit on any International flights being redeemed from the Flexi Award on intermiles.com, on a return ticket</div>

<div class="que">Q. How do I use this Benefit?</div>

<div class="ans">
    <ul>
    <li>Visit flights.intermiles.com -> Select Redeem InterMiles</li>
    <li>Login using InterMiles your account which is linked co-brand credit card.</li>
    <li>Redeem your InterMiles for an International Flight under the 'Flexi Award'</li>
    <li>30 Days post the flight date, the Miles will be credited to your InterMiles account</li>
    <li>The Miles will be credited as “Miles back’’ in your InterMiles statement</li>
 

</ul>

</div>

<div class="que">Terms and Conditions</div>

 
<div class="ans">
    <ul>
        <li> This benefit will be applied to the first booking </li>
<li>The Miles back is applicable only once a year on International flights being booked on InterMiles.com under the 'Flexi Award'</li>
<li>The Miles back is valid for a return ticket only</li>
<li>The Miles back offer is applicable during the period 15th November 2019 to 30th June 2020</li>
<li>The quantum/ percentage of Miles back will vary by co-brand card variant and may change from time to time at the discretion of InterMiles.</li>
<li>Member has to be a valid InterMiles and must have the pre-request Miles required for the flight</li>
<li>No 2 Offers and can be clubbed to avail this benefits</li>
<li>InterMiles T&C’s apply</li>
<li>Bank T&C’s apply.</li>
    </ul>
</div>

<br> 

<div class="que">
    <h4>5 % Discount on Economy Class Ticket on Etihad Flights</h4></div>

<div class="que">Q. Who is eligible: </div>

<div class="ans"> A: All co-brands members holding a co-brand, applying or renewing a co-brand Credit card by paying fees will be eligible to get a 5% discount on Etihad Airways Economy tickets being booked on http://flights.intermiles.com </div>

<div class="que"> Q. What is the benefit? </div>

<div class="ans"> A: This is an ongoing benefit; all members will be eligible for a 5% discount on base fire amount</div>

<div class="que"> Q. When do I get the benefit? </div>

<div class="ans"> A: All existing co-brand member will be eligible for this benefit </div>

 

<div class="que"> Q. On which flights can I utilize the benefit? </div>

<div class="ans"> A: You can utilize this discount on Etihad Flights for Economy class tickets being booked on https://flights.intermiles.com  subject to terms and conditions. </div>

<div class="que"> Q. How do I use this Benefit? </div>

<ol type="a"><li>The discount will be applicable on international flights originating from India only and if the payment is made in INR currency only. In event if the cardholder is trying to book any international flight originating outside of India and the payment is in international currency, the online discount will not be applicable. For example, if a cardholder is booking Dubai-Mumbai on intermiles.com (India country site) and the payment is to be made in AED then the discount will not be applicable.</li>

    <li>The following Co-brand Card holders are entitled to receive a discount of 5% Base Fare discount (“Offer”) for revenue bookings made online bookings on https://flights.intermiles.com and in Q, M, K, H, B and Y RBD’s. </li>
    <div class="ans">
<ul>
    <li>Jet Airways American Express Platinum Credit Card</li>

<li>Jet Privilege HDFC Bank Select Credit Card</li>

<li>Jet Privilege HDFC Bank Platinum Credit Card</li>

<li>Jet Privilege HDFC Bank Signature Credit Card</li>

<li>Jet Privilege HDFC Bank Diners Credit Card</li>

<li>Jet Privilege HDFC Bank Signature Debit Card</li>

<li>InterMiles ICICI Bank Coral Visa Credit Card/ Jet Airway ICICI Bank Coral Visa Credit Card</li>

<li>InterMiles ICICI Bank Coral American Express Credit Card/ Jet Airways ICICI Bank Coral American Express Credit Card</li>

<li>InterMiles ICICI Bank Rubyx Visa Credit Card/ Jet Airways ICICI Bank Rubyx Visa Credit Card</li>

<li>InterMiles ICICI Bank Rubyx American Express Credit Card/ Jet Airways ICICI Bank Rubyx American Express Credit Card</li>

<li>InterMiles ICICI Bank Sapphiro American Express Credit Card/ Jet Airways ICICI Bank Sapphiro Visa Credit Card</li>

<li>InterMiles ICICI Bank Sapphiro American Express Credit Card/ Jet Airways ICICI Bank Sapphiro American Express Credit Card</li>

<li>InterMiles IndusInd Bank Odyssey Visa Credit Card/ Jet Airways IndusInd Bank Odyssey Visa Credit Card</li>

<li>InterMiles IndusInd Bank Odyssey American Express Credit Card/ Jet Airways IndusInd Bank Odyssey American Express Credit Card</li>

<li>InterMiles IndusInd Bank Voyage Visa Credit Card/ InterMiles IndusInd Bank Voyage Visa Credit Card</li>

<li>InterMiles IndusInd Bank Voyage American Express Credit Card/ Jet Airways IndusInd Bank Voyage American Express Credit Card</li>

</ul>
                </div>
    <div class="ans">
    <ul>
        <li>This offer is limited to co-brand card holders booking with their co-brand payment card on Etihad Airways marketed and operated flights only via https://flights.intermiles.com,</li>
        <li>The discount will be available when a cardholder books revenue tickets using their co-brand payment card on https://flights.intermiles.com</li>
        <li>The 5% discount is eligible on the base fare only, all taxes, fees, surcharges and levies are excluded from the 5% discount.  The cardholder will bear the cost of any additional charges such as fees, taxes, surcharges and other levies applicable to the ticket at the time of booking</li>
        
        <li>Payment of fees/service charges/all other amounts due from the card holder to the banking partner from usage of the eligible co-brand Cards by the card holder under this offer and/or otherwise will be governed by respective bank terms and conditions and the card member terms and conditions</li>
        <li>Conditions of carriage are subject to Etihad Airways regulations relating to the conditions of contract respectively and all other terms and conditions regarding Etihad Airways policies and procedures.  Any queries in regards to Etihad conditions of carriage should be addressed directly with Etihad Airways.  </li>
        <li>The decision of Etihad Airways will be final and binding on all the card holders participating in this offer and that the same is non-contestable</li>
        
        <li>Etihad Airways and InterMiles. reserve the rights to change, amend, withdraw and/or alter any of the terms and conditions of this offer at any time without prior notice</li>
        <li>This Offer is an independent offer and cannot be combined with any other promotional offer, coupon or voucher in conjunction from either Etihad Airways or InterMiles.  </li>
        <li>It is the responsibility of the member to ensure the applicable discount is applied at point of sale on the payment page.</li>
    </ul>
    </div>

 
    <div class="que">
        <h4>10 % Discount on Business Class Ticket on Etihad Flights </h4></div>

<div class="que"> Q. Who is eligible: </div>

<div class="ans"> A: All co-brands members holding a co-brand as mentioned in the point below(4.3), applying or renewing a co-brand Credit card by paying fees will be eligible to get a 10% discount on Etihad Airways Business class tickets being booked on http://flights.intermiles.com </div>

<div class="que"> Q. What is the benefit? </div>

<div class="ans"> A: This is an ongoing benefit; all members will be a 10% discount </div>

<div class="que"> Q. When do I get the benefit? </div>

<div class="ans"> A: All existing co-brand member will be eligible for this benefit </div>

<div class="que"> Q. On which flights can I utilize the benefit? </div>

<div class="ans"> A: You can utilize this discount on Etihad Flights for a Business class tickets being booked on https://flights.intermiles.com  subject to terms and conditions. </div>

 
<div class="ans">
<ul>
    <li>The 10% discount will be applicable on international flights originating from India only and if the payment is made in INR currency only. In event if the cardholder is trying to book any international flight originating out of India and the payment is in international currency, the online discount will not be applicable. For example, if a cardholder is booking Dubai-Mumbai on intermiles.com (India country site) and the payment is to be made in AED then the discount will not be applicable</li>
    <li>The following Co-brand Card holders are entitled to receive a discount of 10% Base Fare discount (“Offer”) for revenue bookings made online bookings on https://flights.intermiles.com in  D, C and J RBD’s</li>
        
 <li>Jet Airways American Express Platinum Credit Card</li>

<li>Jet Privilege HDFC Bank Signature Credit Card</li>

<li>Jet Privilege HDFC Bank Diners Credit Card</li>

<li>InterMiles ICICI Bank Sapphiro Visa Credit Card/ Jet Airways ICICI Bank Sapphiro Visa Credit Card</li>

<li>InterMiles ICICI Bank Sapphiro Amex Credit Card/ Jet Airways ICICI Bank Sapphiro Amex Credit Card</li>

<li>InterMiles IndusInd Bank Odyssey Visa Credit Card/ Jet Airways IndusInd Bank Odyssey Visa Credit Card</li>

<li>InterMiles IndusInd Bank Odyssey Amex Credit Card/ Jet Airways IndusInd Bank Odyssey Amex Credit Card</li></ul>

</div>
<div class="ans">

<ul>
    <li> This offer is limited to co-brand card holders booking with their co-brand payment card on Etihad Airways marketed and operated flights only via https://flights.intermiles.com ,</li>
    <li> The discount will be available when a cardholder books revenue tickets using their co-brand payment card on https://flights.intermiles.com </li>
    <li> The 10% discount is eligible on the base fare only, all taxes, fees, surcharges and levies are excluded from the 5% discount. The cardholder will bear the cost of any additional charges such as fees, taxes, surcharges and other levies applicable to the ticket at the time of booking</li>
    <li> Payment of fees/service charges/all other amounts due from the card holder to the banking partner from usage of the eligible co-brand Cards by the card holder under this offer and/or otherwise will be governed by respective bank terms and conditions and the card member terms and conditions</li>
    <li> Conditions of carriage are subject to Etihad Airways regulations relating to the conditions of contract respectively and all other terms and conditions regarding Etihad Airways policies and procedures.  Any queries in regards to Etihad conditions of carriage should be addressed directly with Etihad Airways. </li> 

    <li> The decision of Etihad Airways will be final and binding on all the card holders participating in this offer and that the same is non-contestable</li>
    <li> Etihad Airways and InterMiles. reserve the rights to change, amend, withdraw and/or alter any of the terms and conditions of this offer at any time without prior notice</li>
    <li> This Offer is an independent offer and cannot be combined with any other promotional offer, coupon or voucher in conjunction from either Etihad Airways or InterMiles.  </li>
    <li> It is the responsibility of the member to ensure the applicable discount is applied at point of sale on the payment page.   </li>

</ul>
    </div>

<div class="que">
    <h4>Bonus InterMiles for Etihad Flights being booked on flights.intermiles.com:</h4></div>

 

<div class="que"> Q. Who is eligible: </div>

<div class="ans"> A: All co-brands members holding a co-brand, applying or renewing a co-brand Credit card by paying fees will be eligible to get a 2.5X bonus Intermiles for Etihad Airways tickets being booked on http://flights.intermiles.com </div>

<div class="que"> Q. What is the benefit? </div>

<div class="ans"> A: This is an ongoing benefit; all members with the co-brand listed below will get the discount  </div>

<div class="que"> Q. When do I get the benefit? </div>

<div class="ans"> A: All existing co-brand member will be eligible for this benefit </div>

<div class="que"> Q. On which flights can I utilize the benefit? </div>

 
<div class="ans">
<ul>
    <li>Cardholder earn accelerated Spends InterMiles upon purchase of revenue airline tickets on https://flights.intermiles.com

<li>The cardholders to get a 2.5X bonus of their base card proposition on all eligible spends being made on https://flights.intermiles.com
<li>
    All eligible co-brand members are entailed to get a 2.5X bonus InterMiles on all eligible Etihad Flights being booked on https://flights.intermiles.com</li>
<li> Bonus InterMiles will reflect in the cardholder’s InterMiles account within a period of 60 working days after the member has completed their travel itinerary</li>
<li> Bonus miles are only eligible for commercial bookings made with InterMiles co-brand payment cards on the specified booking pages https://flights.intermiles.com Purchase of redemption tickets or any other merchandise apart from revenue tickets will not qualify to accrue Bonus InterMiles </li>
   <li>Supplementary credit card holder’s Bonus InterMiles Spends will be credited into primary cardholder’s InterMiles membership account 

</li>

</ul>
</div>
<div class="que">
 
    <h4>InterMiles Tier Match</h4></div>

<div class="que"> Q. Who is eligible:</div>

<div class="ans"> A: Co-brand members holding the following co-brand credit card will get an InterMiles Tier Match. </div>

 

<table width="350" border="1" style="border: 1px solid #000">

  <tr>

    <td><b>Bank</b></td>

    <td><b>Variant</b></td>

    <td><b>Tier Match</b></td>

  </tr>

  <tr>

    <td>HDFC </td>

    <td>Diners</td>

    <td>Gold</td>

  </tr>

  <tr>

    <td>HDFC</td>

    <td>Signature </td>

    <td>Silver</td>

  </tr>

  <tr>

    <td>ICICI</td>

    <td>Sapphiro</td>

    <td>Silver</td>

  </tr>

  <tr>

    <td>IndusInd </td>

    <td>Odyssey</td>

    <td>Silver</td>

  </tr>

 <tr>

    <td>AMEX</td>

    <td>Platinum</td>

    <td>Gold</td>

  </tr>

</table>

<div class="que"> Q. What is the benefit? </div>

<div class="ans">A: The benefit will entail a co-brand member to get an InterMiles Tier and the associated Tier Benefit </div>

<div class="que">Q. When do I get the benefit? </div>

<div class="ans">A: The benefit will be accorded to all existing members with the eligible co-brand or when members renew or enroll for an eligible co-brand credit card</div>

<div class="que">
    <h4>Terms and Conditions</h4></div>
<div class="ans"> 
<ul>
    
        <li>Members will the above co-brand cards will get a complimentary InterMiles Tier Match for a period one year. This benefit will be accorded post 90 days of fee realization   </li>
    <li> Members will be eligible to utilize the Tier Benefits associated with the Tier.</li>
    <li> The Tier validity is as per InterMiles T&C’s </li>
    <li> Members need to achieve the InterMiles Tier Miles requirement to attain or retain a Tier during their current Tier year. </li> 

<li>The Tier benefits are over and above the co-brand benefits</li>

<li>Members who hold a higher Tier will continue to hold the higher Tier </li>

<li>InterMiles T&C’s apply https://www.intermiles.com/terms-and-conditions/intermiles-programme-terms-and-conditions</li>

<li>Bank T&C’s apply  </li></ul>
</div> -->

                    
<!--                     <div class="que">Q. JP had a Tiered Program? What is the InterMiles Program </div>
                    <div class="ans">A: Yes, JetPrivilege had a Tiered Program, the Tiered Program had the following Tiers- Blue Plus, Silver, Gold and Platinum. The members achieved/retained their Tiers basis the Tier Points and Tier Miles accumulated over a period of 12,18 and 24 months. <br>
The InterMiles program is now simpler with only InterMiles as a measure of attaining/retaining a Tier. The InterMiles program has 4 Tiers Red, Silver, Gold and Platinum. <a href="#">Click here</a> to know more about the InterMiles program.
</div>



                    
                    
                     <div class="que">Q.Will I be able to use my credit card anywhere/everywhere? </div>
                    <div class="ans">A: Yes! You can continue to use your credit card as you were earlier. You can make the most of your credit card by using it on InterMiles.com, where you can earn get accelerated InterMiles on *expand categories and partners*</div>


                    

                     

                     

                     <div class="que">Q. Will my accrued JPMiles expire? </div>
                    <div class="ans">A: No, your accrued JPMiles are safe and are now InterMiles. The validity of InterMiles is the same as JPMiles.</div>

                    <div class="que">Q. Since the Jet Airways operations were suspended I have not used the JetPrivilege/Jet Airways co-brand. What do I do to activate it? </div>
                    <div class="ans">A: You can transact using your credit card wherever it is accepted and activate it. In-case the transaction fails, please contact the bank to activate the card or you can call us on +91 8422893333 /write to us on memberservices@intermiles.com . We would be happy to assist you.</div>
                    
                     <div class="que">Q. Since the Jet Airways operations were suspended I have not used the JetPrivilege/Jet Airways co-brand. What do I do to activate it? </div>
                    <div class="ans">A: You can transact using your credit card wherever it is accepted and activate it. In-case the transaction fails, please contact the bank to activate the card or you can call us on +91 8422893333 /write to us on memberservices@intermiles.com . We would be happy to assist you.</div>


                    

                     <div class="que">Q. I had a JetPrivilege/Jet Airways Co-brand, but I migrated to another card of the same bank. What do I do?</div>
                    <div class="ans">A: You can get an InterMiles card by reaching out to the bank and requesting for a new InterMiles. Once you raise a request, you will soon get an InterMiles credit card. If you have any issues, please reach us on <InterMiles id> </div>


                   
                                          <div class="que">Q. Where can I use the benefits accorded to me on my InterMiles credit card?</div>
                    <div class="ans">A: You can utilize all your benefits on InterMiles.com across any airline or Hotel available on InterMiles.com. Click here to know about the benefits of your card. </div>

                    
                     <div class="que">Q. I had applied on for a Jet Privilege/Jet Airways credit card in 2019, but I did not get the card. What do I do to get the card?</div>
                    <div class="ans">A: Please write to us on memberservices@intermiles.com with the date of application number, your JetPrivilege Membership number (now InterMiles Number) and the card you had applied for. We will get back to you with the next steps. </div>

                     <div class="que">Q. I had paid fees for my JetPrivilege/Jet Airways credit card but did not get the quoted benefits. What do I do to claim the benefit?</div>
                    <div class="ans">A: Basis your card variant, we credited JPMiles (now InterMiles) to your account. Please check your InterMiles account statement for the same. For any discrepancy regarding the same, please write to memberservices@intermiles.com</div>

                    <div class="que">Q. I have paid fees for a JetPrivilege/Jet Airways card but have not got my benefits. How do I claim these benefits? </div>
                    <div class="ans">A: If you have paid your fees post 14th November 2019, your benefit will be in the InterMiles Wallet. If these are not visible there, please call us on +91 8422893333 or write to us on memberservices@intermiles.com . If you have paid fees prior to 14th November 2019, we have credited JPMiles (now InterMiles) to your account. Please check your InterMiles account statement for the same. For any discrepancy regarding the same, please write to memberservices@intermiles.com </div>

                    
                    Newly added
                    
                   





 

 

 

 




                    


                      <div class="ans_faq"><a href="#">Click here</a> to go back to the home page*   </div> 
-->

                  
                </div>
                <input id="gmNo" name="gmNo" value="5" type="hidden">
                <div class="checkname" id="Home" hidden=""></div>
              
              
                    
                <div class="clearfix"></div>
           

  


                
            </div>
        </section>
    </section>


<script>

$(document).ready(function () {
    
    console.log('before jp header --------> ',navigator.userAgent == "IM-Mobile-App");
        $(".jp-header-fixed").hasClass("jp-header-fixed");
//        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
//           if(navigator.userAgent === "IM-Mobile-App") {
//            var agentVal = "<h6 class='fixed-top'> Navigator>>>> "+navigator.userAgent+"</h6>";
//            var agentValIM = "<h6 class='fixed-top'> navigator.userAgent.indexOf(IM-Mobile-App)>>>> "+navigator.userAgent.indexOf('IM-Mobile-App')+"</h6>";
           if(navigator.userAgent.indexOf('IM-Mobile-App') > -1){
//            if($(".jp-header-fixed").hasClass("jp-header-fixed") == false){
                    console.log('insdie jp header');
                    $('.innerContainer').addClass('mobileagentcssfaq');                   
                
//            }
        }

});
    
    </script>