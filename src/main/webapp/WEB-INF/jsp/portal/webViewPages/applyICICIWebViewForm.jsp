<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<form:form autocomplete="off" id="quickEnrollUser" action="enrollme" commandName="enrollBean">
    <form:hidden path="enrollbpNo"/>
    <div class="modal fade" id="provide_jpNumber" tabindex=-1 role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="proceedPopup_close pull-right">
                        <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.CloseOnPopUpClick(popupName)">
                    </div>
                    <div class="jpNum_popup">
                        <div class="text-center jpPopup_header">
                            Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-brand Card. Apply here
                        </div>

                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Provide InterMiles No.</h2>
                        </div>

                        <div class="row">
                            <div class="jpNumber_div margT2">
                                <div class="jpNumber_input">


                                    <form:input  type="text" path="jpNumber" id="jpNumber" placeholder="InterMiles No." maxlength="9" pattern="[0-9]*" />
                                </div>
                                
                                <div class="jpNumber_input margT2 hidden">
                                    <form:hidden path="jpTier" id="jpTier" placeholder="InterMiles Membership Tier" disabled="true" pattern="[0-9]*" />
                                    <input type="hidden" id="otbpNo"/>
                                </div>
                                <div id="error_msg" class="error_msg" style="padding: 10px 0; font-size: 12px; color: red;"></div>
                                <%--<c:if test="${empty sessionScope.loggedInUser}">--%>
                                <div class="text-center margT2">
                                    If you are not a InterMiles member, <a href="javascript:void(0)" class="enroll_here"  id="quickEnroll" style="text-decoration: underline;" onclick="$.fn.AfterApplyEnrolPopUpClick('Enroll here');$.fn.AfterApplyEnrolPopUpClickCT();">please enroll here</a>
                                </div>
                                <%--</c:if>--%>
                            </div>
                            
                            <div class="genotp">
                                    <button  id="otpButton" class="genbut"></button><input class="geninput" id="otpValue" type="text" />  
                                     <!--<button  id=" regenerateotp" class="genbut">Re-Generate OTP</button>-->
                                   
                                    
                                </div>
                            
                            <div class="otpsuccess" id="otpSucces">
                                <p></p>
                              </div>
                        </div>
                        <div class="text-center margT10">
                            <input class="button_thickBlue" type="button" id="jpNumpopup" onClick="return ewt.trackLink({name:'Amex_Submit_Button',type:'click',link:this});" value="Submit">
                        </div>
                    </div>
                    <div class="enroll_popup hidden">
                        <div class="text-center">
                            Enroll into the InterMiles Programme now to start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-Brand Card.
                        </div>
                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Enroll Now</h2>
                        </div>
                        <div class="error_box2 hidden">
                            <ul class="error_list2">
                            </ul>
                        </div> 
                        <div class="row">
                            <div class="pull-left margT2 nav_web">
                                <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_slide go_To_Jp" onclick="$.fn.EnrolHereBackClick('JP Number')">
                            </div>
                            <div class="enroll_div margT2">
                                <div class="title user_details">
                                    <form:select path="enrollTitle" id="enrollTitle" placeholder="Title" class="title_select">
                                        <form:option value="">Select Title</form:option>
                                        <form:option value="Mr">Mr</form:option>
                                        <form:option value="Ms">Ms</form:option>
                                        <form:option value="Mrs">Mrs</form:option>
                                        <form:option value="Dr">Doctor</form:option>
                                        <form:option value="Prof">Professor</form:option>
                                        <form:option value="Captain">Captain</form:option>
                                    </form:select>
                                </div>
                                <div class="gender hidden">
                                    <c:forEach items="${genderStatus}" var="status">
                                        <div style="display:inline-block">
                                            <label class="radio-inline">
                                                <form:radiobutton
                                                    path="enrollGender" name="enrollgender"
                                                    id="enrollGender"
                                                    value="${status.key}" /> </label>
                                            <label class="gender_desc" for="${status.key}" >${status.value}</label>
                                        </div>
                                    </c:forEach>
                                </div>
                                <div class="firstName_input user_details">
                                    <form:input type="text" path="enrollFname" id="enrollFname" placeholder="First Name"/>
                                </div>
                                <div class="middleName_input user_details">
                                    <form:input type="text" path="enrollMname" id="enrollMname" placeholder="Middle Name"/>
                                </div>
                                <div class="lastName_input user_details">
                                    <form:input type="text" path="enrollLname" id="enrollLname" placeholder="Last Name"/>
                                </div>
                                <div class="city_input user_details">
                                    <form:input type="text" path="enrollCity" id="enrollCity" placeholder="City"/>
                                </div>
                                <div class="phone_input user_details">
                                    <form:input path="enrollPhone" id="enrollPhone" maxlength="10" placeholder="Phone"/>
                                </div>
                                <div class="email_input user_details">
                                    <form:input path="enrollemail" id="enrollemail" placeholder="Email"/>
                                </div>
                                
                                <div class="dob_input user_details">
                                    <form:input path="enrollDob" id="enrollDob" class="datepicker" placeholder="Date Of Birth"/>
                                </div>
                                 <div class="g-recaptcha recaptchaarea"
                                     data-sitekey=<spring:message code="google.recaptcha.site"/>  id="captch">
                                 </div>
                            
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="termsAndConditionsEnroll" class="margT1 statement" path="termsAndConditionsEnroll" /> 
                                        <span>I agree to the InterMiles membership 
                                            <a href="https://www.jetprivilege.com/terms-and-conditions/jetprivilege"  target="_blank">
                                                Terms and Conditions</a><span style="margin: 0px 4px;display: inline;">and</span> <a href="https://www.jetprivilege.com/disclaimer-policy"  target="_blank">
                                                Privacy Policy</a></span>
                                    </div>
                                    <div class="boxinput newboxinput">	
                                        <form:checkbox value="" id="recieveMarketingTeam" class="margT1 statement" path="recieveMarketingTeam" />  
                                        <span> Yes, I would like to receive Marketing Communication


                                        </span>
                                    </div>
                                </div>
                                <div class="error_box_enrolStatus hidden" id="enroll_error">
                                </div>
                     <div class="text-center margT10 nav_web mobcontinue">
                            <input class="button_thickBlue" type="button"  id="enrollForm1" value="Continue">
                        </div>       
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <div class="loaddiv"><div class="loaderamex" id="sad" style="display: none" > </div></div>
    </div>
</form:form>
<form:form autocomplete="off" id="iciciBean" commandName="iciciBean">

    <form:hidden path="cpNo"/>
    <form:hidden path="offerId"/>
    <form:hidden path="offerDesc"/>

    <form:hidden path="hstate"/>
    <form:hidden path="hcity"/>
    <form:hidden path="hstate1"/>
    <form:hidden path="hcity1"/>
    <form:hidden path="hstate2"/>
    <form:hidden path="hcity2"/>

    <input type="hidden" value="${cardBean.gmNo}" id="gmNo"/>
    <section class="container bottom_padding">
        <div class="about">
            <div class="cont_area">
                <h1>Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-Brand Card. Apply here.</h1>
            </div>
            
           <div class="colorDarkBlue">
                <h2 class="heading_txt">Basic Profile</h2>
                 <!--new cr start-->
                <div class="age_error_div hidden" style="color:red"></div>
             <!--new cr end-->
            </div>
            <c:if test="${not empty errorMsg}">
                <div id="errorMsg" style="color: black;" class="error_box">
                    <c:out value="${errorMsg}"></c:out>
                    </div>
            </c:if>
            <div class="margT1 name_div" id="div_hide">	
                <label class="margT1">Name<span class="mandatory-field">*</span></label>	
                <div class="fullName" id="div_hide1">		  					
                    <form:input class="row margT1 input name_inputBox" id="fullName" path = "fullName"  placeholder="Full Name"/>
                </div>
                <ul class="name_ul margT1 hidden"  id="div_hide2">   
                    <li><div class="fName"  id="div_hide3"><span>First Name<span class="mandatory-field">*</span></span>
                            <form:input	class="firstName" maxlength="27" id="fname" path="fname"  name="FNAME" />
                        </div>
                    </li>
                    <li><div class="mName"><span>Middle Name</span>
                            <form:input	class="middleName" maxlength="27" id="mname" path="mname" name="MNAME" />
                        </div>
                    </li>
                    <li><div class="lName"><span>Last Name<span class="mandatory-field">*</span></span>
                            <form:input	class="lastName"  maxlength="27" id="lname" path="lname" name="LNAME"  />
                        </div>
                    </li>
                </ul> 
                <!-- 						  </div> -->
            </div>
            <div class="fullName-error-div hidden" style="color:red">
            </div><hr>
            <div class="margT1 jpMember hidden">	
                <label class="margT1">Are you an existing InterMiles member? </label>
                <div class="row">
                    <div class="col-sm-6">
                        <form:hidden class="row margT1 input" placeholder="InterMiles No." maxlength="9" path="jetpriviligemembershipNumber" id="jetpriviligemembershipNumber"/>
                        <div id="jetpriviligemembershipNumber_validate"></div>
                    </div>
                    <div class="col-sm-6">
                        <form:hidden path="jetpriviligemembershipTier" placeholder="InterMiles Membership Tier" class="row margT1 input" readonly="true"  id="jetpriviligemembershipTier"/>
                    </div>
                    <div id="error_msg_JP" style="padding: 10px 0; font-size: 12px; color: red;"></div>
                </div>
            </div> 

            <div class="margT1">	
                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">E-mail ID<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" placeholder="E-mail ID" id="email" maxlength="255" path="email" value="${email}"/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">Mobile Number<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" placeholder="Mobile Number" maxlength="10" path="mobile" id="mobile" value="${mobile}"/>
                        </div>
                    </div>
                </div>
            </div><hr/>
            <div class="margT1">	
                <div class="row">
                    <div class="col-sm-6 hasDatePicker">
                        <div>
                            <label class="margT1">Date of Birth<span
                                    class="mandatory-field">*</span></label>
                        </div>
                        <div class="dob">
                            <!--new cr start-->
                            <form:input class="datepicker1 row margT1 input"
                                        placeholder="Select Date of Birth" id="dateOfBirth" path="dateOfBirth"
                                          title="MM-DD-YYYY" readonly="true" />
                             <!--new cr end-->
                        </div>
                        <button class="datepicker_icon" type="button">
                            <img title="Select Date" alt="Select Date"
                                 src="${applicationURL}static/images/header/calender.png">
                        </button>
                        <c:if test="${not empty sessionScope.loggedInUser}">
                                     <div id="LoginValidation" style="padding: 10px 0; font-size: 12px; color: red;"></div>
                                 </c:if>
                    </div>
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">Gender<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select class="margT1" path="gender" id="gender">
                                <form:option value="">Select Gender</form:option>
                                <c:forEach items="${genderStatus}" var="status">
                                    <form:option value="${status.key}">${status.value}</form:option>
                                </c:forEach> 
                            </form:select>
                        </div>
                    </div>
                </div>
            </div>	<hr/>
            <div class="margT1">	
                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">Education Qualification
                                <!--                                <span class="mandatory-field">*</span>-->
                            </label>
                        </div>
                        <div>
                            <form:select id="eduQualification" path="eduQualification" class="margT1" value="${eduQualification}" >
                                <form:option value="">Select Education Qualification</form:option>
                                <form:options items="${qualification}" /> 
                            </form:select>		
                        </div>								
                    </div>
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1"> Net Annual Income (Excluding Bonus Or Incentives)<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" id="monthlyIncome" placeholder="Net Annual Income (Excluding Bonus Or Incentives)" maxlength="7" path="monthlyIncome" value="${monthlyIncome}" />
                        </div>
                    </div>
                </div>
            </div>	<hr/>
            <div class="margT1">	
                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">PAN Number<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" id="pancard" placeholder="PAN Number" maxlength="10" path="pancard" value="${pancard}"/>
                        </div>
                    </div>
<!--                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">Aadhaar Number</label>
                        </div>
                        <div>
                            <%--<form:input	class="row margT1 input" id="aadharNumber" maxlength="12" placeholder="Aadhaar ID(For Faster Processing)" path="aadharNumber" value="${aadharNumber}"/>--%>
                        </div>
                    </div>-->
                </div>
            </div><hr/>
            <div class="margT1 currentResAddress">	
                <label class="margT1">Current Residential Address<span class="mandatory-field">*</span></label>				  					
                <form:input path="resedentialAddress" id="resedentialAddress" class="row margT1 input currentResidentialAddress"  placeholder="Current Residential Address"/>
            </div>
            <div class="residentialAddr_div hidden">
                <div class="residentAddress">
                    <div> Residential Address 1 
                        <span>( H. No., Floor No... etc)</span>
                    </div>
                    <div>						
                        <form:input id="address"	class="residentAddressField1" path="address"  value="${address}" name="address"/>
                    </div>	
                </div>
                <div class="residentAddress">
                    <div class=""> Residential Address 2 
                        <span>( Street, Near by... etc)</span>
                    </div>
                    <div>
                        <form:input	class="residentAddressField2" id="address2" path="address2" value="${address2}" name="address"/>
                    </div>
                </div>
                <div class="residentAddress">
                    <div class=""> Residential Address 3
                        <span>( Street, Near by... etc)</span>
                    </div>
                    <div>
                        <form:input	class="residentAddressField3" id="address3" path="address3"  value="${address3}" name="address"/>
                    </div>
                </div>					
                <div class="residentAddress">
                    <ul class="address">
                        <li>
                            <%--<form:input class="pincode" id="pinCode" maxlength="6" path="pinCode" value="${pinCode}" placeholder="Pincode"/>--%>
                            <form:input class="pincodeICICI" id="pinCodeICICI" maxlength="6" path="pinCode" value="${pinCode}" placeholder="Pincode"/>
                        </li>
                        <li>
                            <form:select class="state" path="state1" id="state1" value="${state1}" >
                                <form:option value="">Select State</form:option>
                                <form:options items="${states}" /> 
                            </form:select>	

                        </li>
                        <li class="rescity">
                            <form:select class="city" path="city" id="city" value="${city}"  >
                                <form:option value="">Select City</form:option>
                                <form:options items="${cities}" /> 
                            </form:select>	

                        </li>

                    </ul>
                </div>
            </div>
            <div class="resedentialAddress-error-div hidden" style="color:red"></div>
            <hr/>
            <div class="margT1">	
                <label class="margT1">Do you have a valid address proof for your current residential address mentioned?</label>
                <div class="addressProof">
                    <div class="radio_btn_yes"><form:radiobutton path="peraddresssameascurr" name="addressProof" checked="true" class="" value="Yes"/><span>Yes</span></div>
                    <div><form:radiobutton path="peraddresssameascurr" name="addressProof" class="" value="No"/><span>No</span></div>
                </div>				  					
            </div>
            <div class="margT1 permanentAddr hidden">	
                <label class="margT1">Permanent Residential Address<span class=""></span></label>				  					
                    <form:input id="permanentAddress" path="permanentAddress" class="row margT1 input permanentResidentialAddress" placeholder="Permanent Residential Address"/>
            </div>
            <div class="permanentAddr_div hidden">
                <div class="permanent_residentAddress">
                    <div> Residential Address 1 
                        <span>( H. No., Floor No... etc)</span>
                    </div>			
                    <div>				
                        <form:input	id="permaddress"  class="permanent_AddressField1" name="address"  path="permaddress" value="${permaddress}"/>
                    </div>
                </div>
                <div class="permanent_residentAddress">
                    <div class=""> Residential Address 2 
                        <span>( Street, Near by... etc)</span>
                    </div>
                    <div>
                        <form:input id="permaddress2" class="permanent_AddressField2"  path="permaddress2" value="${permaddress2}" name="address"/>
                    </div>
                </div>
                <div class="permanent_residentAddress">
                    <div class=""> Residential Address 3
                        <span>( Street, Near by... etc)</span>
                    </div>
                    <div>
                        <form:input id="permaddress3"	class="permanent_AddressField3"  path="permaddress3" name="address" value="${permaddress3}"/>
                    </div>
                </div>					
                <div class="permanent_residentAddress">
                    <ul class="address">
                        <li>
                            <form:input class="pincodeICICI" id="PpinCodeICICI" maxlength="6" path="PpinCode" value="${PpinCode}" placeholder="Pincode"/>
                        </li>
                        <li>
                            <form:select class="state" path="PState" id="PState" value="${PState}" >
                                <form:option value="">Select State</form:option>
                                <form:options items="${states}" /> 
                            </form:select>	

                        </li>
                        <li class="rescity">
                            <form:select class="city" path="PCity" id="PCity" value="${PCity}" >
                                <form:option value="">Select City</form:option>
                                <form:options items="${cities}" /> 
                            </form:select>	

                        </li>

                    </ul>
                </div>
            </div>

            <div class="permanentAddr-error-div hidden" style="color: red;"></div>

            <div class="modal fade" id="benefits" tabindex=-1 role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="pull-right close_div"><img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg"  alt=""></div>
                            <h5 class="margT2 colorDarkBlue pad2">Benefits</h5>
                            <span class="list_cont" id="benefitContent"></span>
                        </div>
                    </div>
                </div>
            </div>   

            <!-- Work details -->
            <div class="colorDarkBlue">
                <h2 class="heading_txt margT10">Professional Details</h2>
            </div><hr/>				   
            <div class="margT1">	
                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">Employment Status<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select id="employmentType" class="margT1" path="employmentType" value="${employmentType}" >
                                <form:option value="">Select Employment Type</form:option>
                                <form:options items="${salaryStatus}" /> 
                            </form:select>
                        </div>
                    </div>
                    <div class="col-sm-6" id="ITRStausd">
                        <div>
                            <label class="margT1">Latest ITR acknowledged?
                                <span id="ITRStausspan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select id="ITRStaus" class="margT1" path="ITRStaus" value="${ITRStaus}" >
                                <form:option value="">Select ITR Status</form:option>
                                <form:options items="${iciciITR}" /> 
                            </form:select>
                        </div>
                    </div>
                </div><hr>

                <div class="row">
                    <div class="col-sm-6" id="iciciBankRelationshipd">
                        <div>
                            <label class="margT1">ICICI Bank Relationship
                                <span id="iciciBankRelationshipspan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select class="margT1" id="iciciBankRelationship" path="iciciBankRelationship" value="${iciciBankRelationship}" >
                                <form:option value="">Select Bank Relationship</form:option>
                                <form:options items="${ICICIBankRelationshipstatus}" /> 
                            </form:select>
                        </div>
                    </div>

                    <div class="col-sm-6" id="ICICIBankRelationShipNod">
                        <div>
                            <label class="margT1">ICICI Bank Account Number
                                <span id="ICICIBankRelationShipNospan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" id="ICICIBankRelationShipNo"  path="ICICIBankRelationShipNo"  value="${ICICIBankRelationShipNo}" placeholder="ICICI Bank Account Number"/>
                        </div>
                    </div>
                         
                </div><hr>
                <div class="row">
                    <div class="col-sm-6" id="salaryBankWithOtherBankd">
                        <div>
                            <label class="margT1">Salary Account With Other Bank
                                <span id="salaryBankWithOtherBankspan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select class="margT1" id="salaryBankWithOtherBank" path="salaryBankWithOtherBank" value="${salaryBankWithOtherBank}" >
                                <form:option value="">Select Salary Account With Other Bank</form:option>
                                <form:options items="${salaryBankWithOtherBank}" /> 
                            </form:select>
                        </div>
                    </div>
                     <!--new cr start-->
                    <div class="col-sm-6" id="salaryAccountOpenedd">
                        <div>
                            <label class="margT1">Duration of Salary Account Opened
                                <span id="salaryAccountOpenedspan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select class="margT1" id="salaryAccountOpened" path="salaryAccountOpened" value="${salaryAccountOpened}" >
                                <form:option value="">Select Duration of Salary Account Opened</form:option>
                                <form:options items="${salaryAccountOpened}" /> 
                            </form:select>
                        </div>
                    </div>

                </div><hr>
                <div class="row">
                    <div class="col-sm-6" id="companyNamed">
                        <div>
                            <label class="margT1">Company Name
                                <span id="companyNamespan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" id="companyName"  path="companyName" value="${companyName}" placeholder="Company Name"/>
                        </div>
                    </div>
           
                         <div class="col-sm-6" id="totalExperienced">
                             <div>
                                 <label class="margT1">Total Work Experience
                                     <span id="totalExperiencespan" class="mandatory-field">*</span></label>
                             </div>
                             <div>
                                 <form:input	class="row margT1 input" id="totalExperience"  path="totalExperience" value="${totalExperience}" placeholder="Total Work Experience"/>
                                 <div class="total_div margT1 hidden row">
                                     <ul class="datetimth">
                                         <li class="yearly">
                                             <form:input	class="row input" placeholder="Years"  maxlength="2" path="Years" id="year" value="${year}" />
                                         </li>
                                         <li class="month">
                                             <form:input	class="row input" placeholder="Months" maxlength="2" path="Months" id="month" value="${month}" />
                                         </li>									
                                     </ul>
                                 </div>
                                 <div class="total_div-error-div hidden" style="color:red"></div>
                             </div>
                         </div>
                                      <!--new cr end-->    
                </div>
                <!--code end--> 
                <hr/>							
                <div class="margT1 companyAddr">	
                    <label class="margT1">Company Address<span class="mandatory-field">*</span></label>										  					
                    <form:input id="companyAddress" path="companyAddress" class="row margT1 input companyAddress_box" placeholder="Company Address"/>
                </div>

                <div class="companyAddr_div hidden">
                    <div class="companyAddress">
                        <div> Company Address 1 
                            <span>( H. No., Floor No... etc)</span>
                        </div>		
                        <div>					
                            <form:input	class="companyAddressField1" id="OAddress" name="address"  path="OAddress" value="${OAddress}" />
                        </div>
                    </div>
                    <div class="companyAddress">
                        <div class=""> Company Address 2 
                            <span>( Street, Near by... etc)</span>
                        </div>
                        <div>
                            <form:input	class="companyAddressField2" id="OAddress2" name="address"  path="OAddress2" value="${OAddress2}" />
                        </div>
                    </div>
                    <div class="companyAddress">
                        <div class=""> Company Address 3
                            <span>( Street, Near by... etc)</span>
                        </div>
                        <div>
                            <form:input	class="companyAddressField3" id="OAddress3" name="address"  path="OAddress3" value="${OAddress3}" />
                        </div>
                    </div>					
                    <div class="companyAddress">
                        <ul class="address">
                            <li>
                                <form:input class="pincodeICICI" id="OPincodeICICI" maxlength="6" path="OPincode" value="${OPincode}" placeholder="Pincode"/>
                            </li>
                            <li>
                                <form:select class="state" path="OState" id="OState" value="${OState}" >
                                    <form:option value="">Select State</form:option>
                                    <form:options items="${states}" /> 
                                </form:select>	
                            </li>
                            <li class="rescity">
                                <form:select class="city" path="OCity" id="OCity" value="${OCity}" >
                                    <form:option value="">Select City</form:option>
                                    <form:options items="${cities}" /> 
                                </form:select>	

                            </li>

                        </ul>
                    </div>
                </div>
                <div class="companyAddr_div-error-div hidden" style="color:red"></div>
                <hr/>


                <div class="margT1">	
                    <label class="margT1">Please provide either your home or office <strong>landline number</strong> for verification purposes<span class="mandatory-field">*</span></label>				  					
                    <div class="row">
                        <div class="col-sm-6 phoneNum">
                            <form:input path="homeNumber" id="homeNumber" class="row margT1 input homeNumber" placeholder="Home Landline Number"/>

                            <div class="phoneNum_div margT1 hidden row">
                                <ul>
                                    <li class="std">
                                        <form:input	class="row input std_class" placeholder="STD"  maxlength="4" path="std" id="std" value="${std}" />
                                    </li>
                                    <li class="phNum">
                                        <form:input	class="row input phone_class" placeholder="Phone Number" maxlength="8" path="phone" id="phone" value="${phone}" />
                                    </li>									
                                </ul>
                            </div>
                            <div class="homeNumber-error-div hidden" style="color:red"></div>
                        </div>
                        <div class="col-sm-6">
                            <form:input class="row margT1 input officeNumber" path= "officeNumber" id="officeNumber" placeholder="Office Landline Number"/>

                            <div class="officePhoneNum_div margT1 hidden row">
                                <ul>
                                    <li class="std_office">
                                        <form:input	class="row input ostd_class" placeholder="STD"  maxlength="4" path="OStd" id="OStd"/>
                                    </li>
                                    <li class="phNum_office">
                                        <form:input	class="row input ophone_class" placeholder="Phone Number"  maxlength="8" path="OPhone" id="OPhone"/>
                                    </li>									
                                </ul>
                            </div>
                            <div class="officeNumber-error-div hidden" style="color:red"></div>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="margT1">
                    <label class="margT1">Statement to be sent to<span class="mandatory-field">*</span></label>	
                    <div>
                        <div class="radio_btn_yes">

                            <form:radiobutton   path="stmtTosent" id="statement"value="Home" name="statement"  class="statement"/> <span>Home</span>

                        </div>
                        <div>

                            <form:radiobutton   path="stmtTosent" id="statement" value="Office" name="statement" checked="true" class="statement"/><span> Office</span>

                        </div>
                    </div>
                </div>
                <hr/>
                <div class="margT1">	
                    <label class="margT1">Terms & Conditions</label>		  					
                    <div class="termsAndConditions">


                        <div class="terms-checkbox-holder">
                            <form:checkbox value="termsAndConditions" id="termsAndConditions" class="margT1 statement" path="termsAndConditions" />  I agree with the <a href="https://www.jetprivilege.com/terms-and-conditions/jetairways-icici-bank-cobrand-credit-card"  target="_blank">Terms and Conditions</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="submit_button">
            <input class="submit_btn button_blue submit" onClick="$(this).closest('form').submit()" id="jp_btn" type="button" value="Submit" onclick="$.fn.AfterApplyJPNumberPopUpClick(jpNum, 'Earn with Partners: Swipe any of our Co-brand Cards')">
        </div>

    </section>
</form:form>


<script>
    
    
    $(document).ready(function () {
         
        
        var loginUser = '${sessionScope.loggedInUser}';
        $('#totalExperience').prop('disabled', false);
        $('#iciciBankRelationship').prop('disabled', false);
        $('#ICICIBankRelationShipNo').prop('disabled', true);
        $('#salaryBankWithOtherBank').prop('disabled', false);
        $('#ITRStaus').prop('disabled', true);
        $('#ITRStausspan').hide();
        $("#companyNamespan").show();
        $('#totalExperiencespan').hide();
        $('#ICICIBankRelationShipNospan').hide();
        $('#salaryBankWithOtherBankspan').hide();
        $('#salaryAccountOpenedspan').hide();
        $('#icicidisable').hide();
        $('.loaderamex').hide();
        $('#otpButton').text('Generate OTP');
        $('#otpButton').attr('disabled', true);
        $('#otpSucces').hide();
         $('.geninput').attr('disabled',true);
           
        $('.termsAndConditions').css({'background-color': "transparent", 'padding': '0%', 'border': '0px'});

        if ('${iciciBean.peraddresssameascurr}' == "No") {
            $('.permanentAddr').removeClass('hidden');
        }

        $('.middleName').on("focus", function () {
            $('.fullName-error-div').addClass('hidden');
        });
        $('.lastName').on("focus", function () {
            $('.fullName-error-div').addClass('hidden');
        })
        function hasalphabet(str) {
      return /^[a-zA-Z()]+$/.test(str);
    }

        var Rtype = $("#iciciBankRelationship").val();


        var isCorrectJpNumber = false;

        if ($('#pinCodeICICI').val() != "" && $('#city').val() != "" && $('#state1').val() != "") {
            $("#hcity").val($('#city').val());
            $("#hstate").val($('#state1').val());
            $('#state1').attr("disabled", true);
            $('#city').attr("disabled", true);

        }
        if ($('#PpinCodeICICI').val() != "" && $('#PCity').val() != "" && $('#PState').val() != "") {
            $("#hcity1").val($('#PCity').val());
            $("#hstate1").val($('#PState').val());
            $('#PCity').attr("disabled", true);
            $('#PState').attr("disabled", true);
        }
        if ($('#OPincodeICICI').val() != "" && $('#OCity').val() != "" && $('#OState').val() != "") {
            $("#hcity2").val($('#OCity').val());
            $("#hstate2").val($('#OState').val());
            $('#OCity').attr("disabled", true);
            $('#OState').attr("disabled", true);
        }
        var isValid = false;
        
        $('#jpNumpopup').attr('disabled', true);
        if ('${iciciBean.peraddresssameascurr}' == "No") {
            $('.permanentAddr').removeClass('hidden');
        }
        $(".annual_spends").click(function () {
            var arr = [];
            arr.push('${cardBean.cpNo}');
            var spend = $(".annualSpends_value").html().replace(/<\/?img[^>]*>/g, "");
            spend = spend.substring(spend.indexOf(' ') + 1);
            $.get("${applicationURL}calcfreeflights?spend=" + spend + "&cpNoList=" + arr, function (data, status) {
                if (status == "success") {
                    $.each(data, function (key, value) {
                        $("#freeFlights" + key).html(value.split(",")[1]);
                        $("#jpMiles" + key).html("on earning <br>" + value.split(",")[0] + " JPMiles");
                    });
                    $('.free_flights_div').removeClass('hidden');
                    $('.free_flights_desc').addClass('hidden');
                    $('#spends_slider').modal('hide');
                }
            });
            $(".close_filter").trigger('click');
        });
        $('#dateOfBirth').on('change', function () {
            $("#iciciBean").validate().element('#dateOfBirth')

        });
        
        
        //         -new cr start-
        var years = [];
        for (var i = 0; i < 100; i++) {
            years.push("" + i);
        }
        var months = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"];
        $(document).on('focus', '#totalExperience', function () {
            $('.total_div').removeClass('hidden');
            $('#totalExperience').addClass('hidden');
            $('#totalExperience-error').addClass('hidden');
            $('.yearly input').focus();
        });
        function checkValue(value, arr) {
            var status = true;

            for (var i = 0; i < arr.length; i++) {
                var name = arr[i];
                if (name == value) {
                    status = false;
                    return status;
                    break;
                }
            }

            return status;
        }
        $('.total_div').focusout(function () {
            var year = $('.yearly input').val();
            var month = $('.month input').val();
            var emptype = $("#employmentType").val();
            $('#totalExperience-error').addClass('hidden');
            if (emptype == "Salaried" && year == "" && month == "") {
                  
                  $('#totalExperience-error').addClass('hidden');
                $('.total_div-error-div').html('Please Enter Years and Months.');
                $('.total_div-error-div').removeClass('hidden');
            }
            else if ((emptype == "Self Employed-Professional" || emptype == "Self Employed-Business") && year == "" && month == "") {
                $('.total_div-error-div').html('');
                $('.total_div-error-div').addClass('hidden');
            }
           
            if (year != "") {
                var message;
                if (checkValue(year, years) == true) {
                    message = "Please select valid year.";
                    $('.total_div-error-div').html(message);
                    $('.total_div-error-div').removeClass('hidden');
                } else if (year != "" && month == "") {
                    if (emptype == "Salaried") {
                        $("#month").val(0);
                    }
                    else if ((emptype == "Self Employed-Professional" || emptype == "Self Employed-Business")) {
                        $("#month").val(0);
                    }
                }

            }
            if (month != "") {
                var message;
                if (checkValue(month, months) == true) {
                    message = "Please select valid month.";
                    $('.total_div-error-div').html(message);
                    $('.total_div-error-div').removeClass('hidden');
                } else if (year == "" && month != "") {
                    if (emptype == "Salaried") {
                        $("#year").val(0);
                    }
                    else if ((emptype == "Self Employed-Professional" || emptype == "Self Employed-Business")) {
                        $("#year").val(0);
                    }
                }

            }

        });

        $('.yearly input, .month input').on('keyup', function () {
            year = $('.yearly input').val();
            month = $('.month input').val();
            if (year != "" || month != "")
                $('.total_div-error-div').html('');
        });
        $(document).click(function (event) {
            if (!$(event.target).closest('.total_div').length) {

                var year = $('.yearly input').val();
                var month = $('.month input').val();
                var emptype = $("#employmentType").val();
                if (emptype == "Salaried" && year == "" || month == "")
                    $('#totalExperience-error').css('display', 'none');
                else if ((emptype == "Self Employed-Professional" || emptype == "Self Employed-Business") && year == "" || month == "")
                    $('#totalExperience-error').css('display', 'block');
                else {

                    $('#totalExperience-error').css('display', 'block');
                }
                $('.total_div').addClass('hidden');
                $('#totalExperience').removeClass('hidden');
                var a;
                if (year <= 1 &&  month > 1) {
                    a = "" + year + " year " + month + " months"
                } else if (year <= 1 && month <= 1) {
                    a = "" + year + " year " + month + " month"
                } else if (year > 1 && month <= 1) {
                    a = "" + year + " years " + month + " month"
                } else {
                    a = "" + year + " years " + month + " months";
                }
                $('#totalExperience').val(a);
                if (year == "" && month == "")
                    $('#totalExperience').val('');
            }
        });

        $('.yearly input, .month input').on("keypress", function (evt) {


            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;

            }
        });

        $(".month input").autocomplete({
            source: months,
            select: function (event, ui) {

                $(".month input").val(ui.item.value);
                var month = ui.item.value;
            }

        });

        $(".yearly input").autocomplete({
            source: years
        });
        
//         new cr end

           
        $.validator.addMethod('checkdateOfBirth', function (value, element) {

            var birthDate1 = document.getElementById('dateOfBirth').value;
            var d = new Date(birthDate1);
            var mdate = birthDate1.toString();

            var monthThen = parseInt(mdate.substring(0, 2), 10);

            var dayThen = parseInt(mdate.substring(3, 5), 10);
            var yearThen = parseInt(mdate.substring(6, 10), 10);
            var today = new Date();
            var loginUser = '${sessionScope.loggedInUser}';

            if (loginUser === "") {
                var birthday = new Date(yearThen, monthThen - 1, dayThen);
            } else {
                var birthday = new Date(monthThen - 1, dayThen, yearThen);
            }

            var differenceInMilisecond = today.valueOf() - birthday.valueOf();
            var year_age = Math.floor(differenceInMilisecond / 31536000000);
            var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);
            var month_age = Math.floor(day_age / 30);
            day_age = day_age % 30;

        });
        
        //         new cr start
        function getage() {
            var birthDate1 = document.getElementById('dateOfBirth').value;
            var d = new Date(birthDate1);
            var mdate = birthDate1.toString();
            var monthThen = parseInt(mdate.substring(0, 2), 10);
            var dayThen = parseInt(mdate.substring(3, 5), 10);
            var yearThen = parseInt(mdate.substring(6, 10), 10);
            var today = new Date();
            var loginUser = '${sessionScope.loggedInUser}';

            if (loginUser === "") {
                var birthday = new Date(yearThen, monthThen - 1, dayThen);
            } else {
                var birthday = new Date(yearThen, monthThen - 1, dayThen);
            }

            var differenceInMilisecond = today.valueOf() - birthday.valueOf();
            var year_age = Math.floor(differenceInMilisecond / 31536000000);
            var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);
            var month_age = Math.floor(day_age / 30);
            day_age = day_age % 30;
            return year_age;
        }
//                new cr end

        $.validator.addMethod('checkFullName', function (value, element) {
            var fn = $("#fname").val();
            var ln = $("#lname").val();
            
            if (fn != "" && ln != "")
                return true;
            else
                return false;
        });
        

        
        $.validator.addMethod('checkCurrentAddress', function (value, element) {
    
            var address = $(".residentAddressField1").val();

            pincode = $('#pinCodeICICI').val();

            var city = $("#city").val();
            var state = $("#state1").val();

            if (address != "" && pincode != "" && city != "" && state != "") {
                return true;
            } else
                return false;
        });

        $.validator.addMethod('checkCompanyAddress', function (value, element) {
            var address = $("#OAddress").val();
            var pincode = $("#OPincode").val();
            var city = $("#OCity").val();
            var state = $("#OState").val();
            if (address != "" && pincode != "" && city != "" && state != "")
                return true;
            else
                return false;
        });

        $.validator.addMethod('minLength', function (value, element) {
            var relationno = $("#ICICIBankRelationShipNo").val().length;
            if (relationno > 12 || relationno < 12) {
                return false
            } else
            {
                return true;
            }

        });

        $.validator.addMethod("minsal", function(value,element) {
            var len=$("#ICICIBankRelationShipNo").val().length;
            if(($("#iciciBankRelationship").val() == "Salary Account")){
            if(len<12 ){
                return false;
            }else{
                return true;
            }
        }
        });
         $.validator.addMethod("mincur", function (value, element) {
            var len = $("#ICICIBankRelationShipNo").val().length;
            if (($("#iciciBankRelationship").val() == "Current Account")) {
                if (len < 12) {
                    return false;
                } else {
                    return true;
                }
            }
        }); 
        
        $.validator.addMethod("minsav", function(value,element) {
            var len=$("#ICICIBankRelationShipNo").val().length;
            if(($("#iciciBankRelationship").val() == "Savings Account")){
            if(len<12 ){
                return false;
            }else{
                return true;
            }
        }
        });
        
        $.validator.addMethod('checkalphanumeric', function (value, element) {
            var relationno = $("#ICICIBankRelationShipNo").val();
            var letter = /[a-zA-Z]/;
            var number = /[0-9]/;
            var spcl = /^[a-zA-Z0-9]+$/;
            var chk_spcl = spcl.test(relationno);
            if (chk_spcl)
            {
                var valid = number.test(relationno) && letter.test(relationno);

                return valid;
            }

        });
        $('#totalExperience').on("keypress", function (e) {
            var charCode;
            if (e.keyCode > 0) {
                charCode = e.which || e.keyCode;
            } else if (typeof (e.charCode) != "undefined") {
                charCode = e.which || e.keyCode;
            }
            if (charCode == 46)
                return true
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

        $.validator.addMethod('checkdecimal', function (value, element) {
            var tex = $("#totalExperience").val();

            if (tex.indexOf(".")) {
            var count1 = tex.split(".").length - 1;
           var before_decimal = tex.toString().split(".")[0];
           var after_decimal = tex.toString().split(".")[1];
           if ((before_decimal.length >= 3 && after_decimal.length == 1) || (before_decimal.length == 0 && after_decimal.length > 0) || after_decimal > 11 || (before_decimal.length > 2 && after_decimal.length == 0) || count1 > 1 && tex != null) {

                    return false;
                } else {
                    return true;
                }
            } else if (tex.length > 2) {
                return false;

            } else
            {
                return true;
            }
        });
        
        $('#pinCodeICICI').on("keydown", function (evt) {


            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
              
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
              
            } else
                return true;
        });
        
        $('#enrollDob').datepicker({dateFormat: 'mm-dd-yy', changeYear: true,
            changeMonth: true, yearRange: '1945:-18', maxDate: 0, defaultDate: '01-01-1945'});
         //cr2 milestone 1
           var below='${agebelowrange}';
           var above='${ageaboverange}';
           if((above != "" && below !="") && (above > 0 && below > 0 )){
                var start1 = new Date();
         start1.setFullYear(start1.getFullYear() - parseInt(below) );
    var end1 = new Date();
    end1.setFullYear(end1.getFullYear() - parseInt(above));
    var loggedInJPnumber = $("#jetpriviligemembershipNumber").val();
        $('#dateOfBirth').datepicker({ 
           dateFormat: 'mm-dd-yy' ,
           changeYear: true,
           changeMonth: true,
           minDate: start1,
           maxDate: end1,
          yearRange:start1.getFullYear()+':'+end1.getFullYear()
        });
    
           }else{
               
            var start = new Date();
        start.setFullYear(start.getFullYear() - 65);
        var end = new Date();
        end.setFullYear(end.getFullYear() - 18);
                   $('#dateOfBirth').datepicker({ 
           dateFormat: 'mm-dd-yy' ,
           changeYear: true,
           changeMonth: true,
           minDate: start,
           maxDate: end,
          yearRange:start.getFullYear()+':'+end.getFullYear()
        });
    }
    //cr2 milestone 1 end
        $('#PpinCodeICICI').on("keydown", function (evt) {

           evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;
 
            }
        });

        $('#OPincodeICICI').on("keydown", function (evt) {

            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;


            }
        });

       

               // new cr start


        $('#employmentType').change(function () {
            var textarea_value = $("#employmentType").val();

            if (textarea_value == "Salaried")
            {
                var flag = validate_month_sal();
             if (!flag && $("#monthlyIncome").val() == null)
                {

                    $("#monthlyIncome").focus();
                    $("#monthlyIncome").focusout();
                    $("#monthlyIncome-error").show();

                } else
                {
                    $("#monthlyIncome-error").hide();
                }

                $("#ICICIBankRelationShipNospan").hide();
                $('#companyName').prop('disabled', false);
                $("#companyNamespan").show();
                $("#companyName-error").show();
                $('#iciciBankRelationship').prop('disabled', false);
                $("#iciciBankRelationshipspan").show();
                $("#iciciBankRelationship-error").show();
                $('#totalExperience').prop('disabled', false);
                $("#totalExperiencespan").show();
                $("#totalExperience-error").show();
                $('#salaryBankWithOtherBankspan').show();
                $('#ITRStaus').prop('disabled', true).css("background-color", "#ebebe4");
                $("#ITRStausspan").hide();
                $("#ITRStaus-error").hide();
                $("#salaryAccountOpenedspan").show();
                $('#salaryAccountOpened').prop('disabled', false).css("background-color", "");
                $('#salaryAccountOpened').prop('disabled', false);
                $("#salaryAccountOpened-error").show();
                $("#ITRStaus").val('');
                $('select#iciciBankRelationship').html("")
                $('select#iciciBankRelationship').append('<option value>Select Bank Relationship</option><option value="Salary Account">Salary Account</option> <option value="Savings Account">Savings Account</option><option value="Loan Account">Loan Account</option><option value="No Relationship">No Relationship</option>')
                $('#salaryBankWithOtherBank').val('');
                $('#salaryBankWithOtherBank').prop('disabled', false).css("background-color", "");
                $("#salaryBankWithOtherBankspan").show();
                $("#salaryBankWithOtherBank-error").show();

                $("#iciciBankRelationship").change(function () {

                    var textarea_value1 = $("#iciciBankRelationship").val();

                    if ((textarea_value1 == "Salary Account")) {


                        $("#ICICIBankRelationShipNospan").show();
                        $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(2).val()).attr("selected", "selected");
                        $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryAccountOpenedspan").show();
                        $("#salaryAccountOpened-error").show();
                        $('#salaryAccountOpened').prop('disabled', false).css("background-color", "");
                        $('#salaryAccountOpened').val('');

                        $('#ICICIBankRelationShipNo').prop('disabled', false);
                        $("#salaryBankWithOtherBankspan").show();
                        $("#salaryBankWithOtherBank-error").hide();
                        $("#ICICIBankRelationShipNospan").show();
                        $("#ICICIBankRelationShipNo-error").show();
                        $('#ICICIBankRelationShipNo').val('');

                    } else if ((textarea_value1 == "Loan Account") || (textarea_value1 == "Savings Account")) {


                        $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(1).val()).attr("selected", "selected");

                        var textarea_value1 = $('#salaryBankWithOtherBank option').eq(1).val();
                        $('#salaryAccountOpened').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryAccountOpenedspan").hide();
                        $("#salaryAccountOpened-error").hide();
                        $('#salaryAccountOpened').val('');
                        //end-----------------
                        $("#ICICIBankRelationShipNospan").hide();
                        $("#ICICIBankRelationShipNo-error").hide();
                        $('#ICICIBankRelationShipNo').prop('disabled', false);
                        $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryBankWithOtherBankdspan").show();
                        $("#salaryBankWithOtherBank-error").hide();
                        $('#ICICIBankRelationShipNo').val('');


                    } else {

                        $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(1).val()).attr("selected", "selected");
                        $("#ICICIBankRelationShipNod").show();
                        $("#ICICIBankRelationShipNospan").hide();

                        var textarea_value1 = $('#salaryBankWithOtherBank option').eq(1).val();
                        $('#salaryAccountOpened').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryAccountOpenedspan").hide();
                        $("#salaryAccountOpened-error").hide();
                        $('#salaryAccountOpened').val('');
                        //---end---
                        $("#ICICIBankRelationShipNospan").hide();
                        $("#ICICIBankRelationShipNo-error").hide();
                        $('#ICICIBankRelationShipNo').prop('disabled', true);
                        $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4")
                        $("#salaryBankWithOtherBankdspan").show();
                        $("#salaryBankWithOtherBank-error").hide();
                        $('#ICICIBankRelationShipNo').val('');
                    }

                });
                //code end here
            } else {
               $('.total_div-error-div').addClass('hidden');
                var flag = validate_month_self();
                if (!flag && $("#monthlyIncome").val() == null)
                {
                    $("#monthlyIncome").focus();
                    $("#monthlyIncome").focusout();
                    $("#monthlyIncome-error").show();
                } else
                {
                    $("#monthlyIncome-error").hide();
                }
                $('#ITRStaus').prop('disabled', false).css("background-color", "");
                $("#ITRStausspan").show();
                $("#ITRStaus-error").show();
                $('#companyName').prop('disabled', false);
                $("#companyNamespan").show();
                $('#companyName-error').show();
                $('#iciciBankRelationship').prop('disabled', false);
                $('#totalExperience').prop('disabled', false);
                $("#totalExperiencespan").hide();
                $("#totalExperience-error").hide();
                $("#salaryAccountOpenedspan").hide();
                $('#salaryAccountOpened').prop('disabled', false).css("background-color", "");
                $('#salaryAccountOpened').prop('disabled', false);
                $("#salaryAccountOpened-error").show();
                $('select#iciciBankRelationship').html("")
                $('select#iciciBankRelationship').append('<option value>Select Bank Relationship</option> <option value="Current Account">Current Account</option><option value="Savings Account">Savings Account</option><option value="Loan Account">Loan Account</option><option value="No Relationship">No Relationship</option>')
                // code start here 
                $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(2).val()).attr("selected", "selected");
                $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
                $("#salaryBankWithOtherBankspan").show();
                $("#salaryBankWithOtherBank-error").show();

                $("#iciciBankRelationship").change(function () {

                    var textarea_value1 = $("#iciciBankRelationship").val();

                    if ((textarea_value1 == "Loan Account") || (textarea_value1 == "Savings Account") ||(textarea_value1 == "Current Account")) {

                        $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(2).val()).attr("selected", "selected");
                        $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryBankWithOtherBankspan").show();
                        $("#salaryBankWithOtherBank-error").show();
                        $("#ICICIBankRelationShipNospan").hide();
                        $("#ICICIBankRelationShipNo-error").hide();
                        $('#ICICIBankRelationShipNo').prop('disabled', false);
                        $('#ICICIBankRelationShipNo').val('');
                        $('#salaryAccountOpened').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryAccountOpenedspan").hide();
                        $("#salaryAccountOpened-error").hide();
                        $('#salaryAccountOpened').val('');

                    } else {

                        $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(2).val()).attr("selected", "selected");
                        $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryBankWithOtherBankspan").show();
                        $("#salaryBankWithOtherBank-error").show();
                        $("#ICICIBankRelationShipNod").show();
                        $("#ICICIBankRelationShipNospan").hide();
                        $("#ICICIBankRelationShipNospan").hide();
                        $("#ICICIBankRelationShipNo-error").hide();
                        $('#ICICIBankRelationShipNo').prop('disabled', true);
                        $('#ICICIBankRelationShipNo').val('');
                        $('#salaryAccountOpened').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryAccountOpenedspan").hide();
                        $("#salaryAccountOpened-error").hide();
                        $('#salaryAccountOpened').val('');
                    }

                });
                         }
        });
  

        $("#companyName").on("input keypress", function (e) {

            var company = $("#companyName").val();
            var emptype = $("#employmentType").val();
            var relationship = $("#iciciBankRelationship").val();

                
                var inp = $("#companyName").val();
                 $.get("${applicationURL}companyNameByLetter?companyName=" + inp, function (data, status) {
                if (status = "success" && data != "") {
                    var test = data;
                    $("#companyName").autocomplete({
                        disabled: false,
                        source: function (request, response) {
                            var matches = $.map(test, function (companyNameItem) {
                                if (companyNameItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                                    return companyNameItem;
                                }
                            });
                            response(matches);
                        }
                        ,

                    });
                }
            });

        });
        $("#salaryAccountOpened").on("focusout", function(e){
            if(($("#employmentType").val() == "Salaried") && ($("#iciciBankRelationship").val() == "Salary Account") )
            {
                $("#salaryAccountOpened-error").hide();
                
            }else{
                $("#salaryAccountOpened-error").show();
              
            }
        });
                      if(!$("#salaryBankWithOtherBankspan").is(':visible'))
        {
            $("#salaryAccountOpened-error").hide();
         
        }
        $.validator.addMethod("checklength", function (value, element) {
            var addr1 = $('#OAddress').val();
            var addr2 = $('#OAddress2').val();
            var addr3 = $('#OAddress3').val();

            if (addr1.length > 24) {
                return false;
            } else {
                return true;
            }

        });
        $.validator.addMethod("minloan", function (value, element) {
            var len = $("#ICICIBankRelationShipNo").val().length;
            if (($("#iciciBankRelationship").val() == "Loan Account")) {
                if (len < 16) {
                    return false;
                } else {
                    return true;
                }
            }
        });
        
        $("#ICICIBankRelationShipNo").on("keyup", function (e) {
            var len = $("#ICICIBankRelationShipNo").val().length;
              if (($("#iciciBankRelationship").val() == "Salary Account") || ($("#iciciBankRelationship").val() == "Savings Account") || ($("#iciciBankRelationship").val() == "Current Account") ) {
                  $("#ICICIBankRelationShipNo").attr("maxlength","12");
            }
            //------for loan
            if (($("#iciciBankRelationship").val() == "Loan Account")) {
                 $("#ICICIBankRelationShipNo").attr("maxlength","16");
            }

        });
        var a = '${SalariedIncome}';
        var b = '${SelfEmpIncome}';


        a = a.toString().replace("[", "").replace("]", "");
        b = b.toString().replace("[", "").replace("]", "");
        var totalsal = a * 12;
        var totalself = b * 12;
        function validate_month_self()
        {
            if (totalself != "") {
                var anualincome = $("#monthlyIncome").val();
                if (anualincome < totalself) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        function validate_month_sal()
        {
            if (totalsal != "") {
                var anualincome = $("#monthlyIncome").val();
                if (anualincome < totalsal) {
                    return false;
                } else {
                    return true;
                }
            }
        }

        $.validator.addMethod("checkannualincomeSelf", function (value, element) {
            var anualincome = $("#monthlyIncome").val();
            var emptype = $("#employmentType").val();
            if (emptype != null && anualincome != null) {
                if (emptype == "Self Employed-Professional" || emptype == "Self Employed-Business") {
                    if (anualincome < totalself) {
                        $("#monthlyIncome-error").show();
                        return false;
                    } else {
                        return true;
                    }

                }
            }

        }, "Net Annual Income must be greater than or equal to INR " + totalself + "");
        $.validator.addMethod("checkannualincomeSal", function (value, element) {
            var anualincome = $("#monthlyIncome").val();
            var emptype = $("#employmentType").val();
            if (emptype != null && anualincome != null) {
                if (emptype == "Salaried") {
                    if (anualincome < totalsal) {
                        return false;
                    } else {
                        return true;
                    }

                }
            }

        }, "Net Annual Income must be greater than or equal to INR " + totalsal + "");
        $("#dateOfBirth").on("change", function () {
            var emp = $("#employmentType").val();
            var ci = $(".residentAddress .city").val();
            if (emp != "" || ci != "") {
                var c = $('.residentAddress .city').val();
                var dataageabove, dataagebelow;
                $.get("${applicationURL}AgeCriteriaageabove?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + emp + "&City=" + c, function (data, status) {
                    if (status == "success") {
                        dataageabove = data;
                    }
                    $.get("${applicationURL}AgeCriteriaagebelow?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + emp + "&City=" + c, function (data, status) {
                        if (status == "success") {
                            dataagebelow = data;
                        }
                        var a = getage();
                        if (dataageabove != "" && dataagebelow != "" && a != "") {
                            if (a < dataageabove || a > dataagebelow) {
                                $('#dateOfBirth-error').html("You are not applicable for application");
                                $('#LoginValidation').html("You are not applicable for application");
                                $('#dateOfBirth-error').show();
                                  $("#dateOfBirth-error").focus(); //new cr2
                                  $("html, body").animate({ scrollTop: 400 }, 600);//new cr2
                                      return false;//new cr2
                                      
                                

                            } else {
                                $('#dateOfBirth-error').html("");
                                 $('#LoginValidation').html("");
                                $('#dateOfBirth-error').hide();
                            }

                        }
                    });
                });


            } else {
            }
        });
        $("#employmentType").on("change", function () {
            var emp = $("#employmentType").val();
            var ci = $('.residentAddress .city').val();
            if (emp != null || $('.residentAddress .city').val() != "") {
                var c = $('.residentAddress .city').val();
                var dataageabove, dataagebelow;
                $.get("${applicationURL}AgeCriteriaageabove?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + emp + "&City=" + c, function (data, status) {
                    if (status == "success") {
                        dataageabove = data;
                    }
                    $.get("${applicationURL}AgeCriteriaagebelow?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + emp + "&City=" + c, function (data, status) {
                        if (status == "success") {
                            dataagebelow = data;
                        }
                        var a = getage();
                        if (dataageabove != "" && dataagebelow != "" && a != "") {
                            $("#employmentType").focusout(function () {

                                if (a < dataageabove || a > dataagebelow) {
                                    $('#dateOfBirth-error').html("You are not applicable for application");
                                    $('#LoginValidation').html("You are not applicable for application");
                                    $('#dateOfBirth-error').show();
                                    $("#dateOfBirth-error").focus(); //new cr2
                                    
                                  $("html, body").animate({ scrollTop: 400 }, 600);//new cr2
                                      return false;//new cr2

                                } else {
                                    $('#dateOfBirth-error').html("");
                                     $('#LoginValidation').html("");
                                    $('#dateOfBirth-error').hide();
                                }

                            });

                        }
                    });
                });


            } else {
            }
        });
        $('.residentAddress').on("change", function () {
            var ci = $('.residentAddress .city').val();
            var emp = $("#employmentType").val();
            if ($("#employmentType").val() != "" && $('.residentAddress .city').val() != "") {
                var dataageabove, dataagebelow;
                var em = $("#employmentType").val();
                $.get("${applicationURL}AgeCriteriaageabove?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + em + "&City=" + ci, function (data, status) {
                    if (status == "success") {
                        dataageabove = data;
                    }

                    $.get("${applicationURL}AgeCriteriaagebelow?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + em + "&City=" + ci, function (data, status) {
                        if (status == "success") {
                            dataagebelow = data;
                        }
                        var a = getage();
                        if (dataageabove != "" && dataagebelow != "" && a != "") {

                            if (a < dataageabove || a > dataagebelow) {
                                $('#dateOfBirth-error').html("You are not applicable for application");
                                $('#LoginValidation').html("You are not applicable for application");
                                $('#dateOfBirth-error').show();
                                $("#dateOfBirth-error").focus(); //new cr2
                                  $("html, body").animate({ scrollTop: 400 }, 600);//new cr2
                                      return false;//new cr2

                            } else {
                                $('#dateOfBirth-error').html("");
                                $('#LoginValidation').html("");
                                $('#dateOfBirth-error').hide();
                            }


                        }
                    });
                });

            } else {
            }
        });

        
        
        
        $("#address, #address2, #address3, #OAddress3, #OAddress2, #OAddress ,.permanent_AddressField3, .permanent_AddressField2, .permanent_AddressField1").on("keypress", function (e) {
            var len = $(this).val().length;
            if (e.which == 8)
			{

            }
            else if (len >= 40) 
			{
            e.preventDefault();
                if(e.keyCode==09 || e.keyCode==11)
					{
                               var inputs = $(this).closest('form').find(':input');
                             inputs.eq( inputs.index(this)+ 1 ).focus();
                    }
                }
            
});
        $("#iciciBean").validate({
            ignore: ":disabled,:hidden",
            rules: {

                fullName: {
                    required: true,
                    checkFullName: true
//                    nameValidation:true
                 

                },
                dateOfBirth:
                        {
                            required: true

                        },
                gender:
                        {
                            required: true
                        },
                email:
                        {
                            required: true,
                            emailValidation: true
                        },
                mobile:
                        {
                            required: true,
                            digits: true,
                            mobileValidation: true,
                            maxlength: 10

                        },
             
                totalExperience:
                        {
                            required: {
                                depends: function (element) {
                                    return ($("#employmentType").val() == "Salaried");
                                },
                            }
                        },
                monthlyIncome:
                        {
                            required: true,
                            digits: true,
                           checkannualincomeSal:
                                    {
                                        depends: function (element) {
                                            if ($("#employmentType").val() == "Salaried" && $("#monthlyIncome").val() != "") {
//                                                console.log("return 111 true "); 
                                                 
                                                return true;
                                            } else {
//                                                console.log("return 111 false ");
                                                return false;
                                            }
                                        }
                                    },
                            checkannualincomeSelf:
                                    {
                                        depends: function (element) {
                                            if (($("#employmentType").val() == "Self Employed-Professional" || $("#employmentType").val() == "Self Employed-Business") && $("#monthlyIncome").val() != "") {
//                                                console.log("return 111 true ");
                                                return true;
                                            } else {
//                                                console.log("return 111 false ");
                                                return false;
                                            }
                                        }
                                    }
                        },
                pancard:
                        {
                            required: true,
                            IciciPanValidation: true
                        },
                aadharNumber:
                        {
                            digits: true,
                            rangelength: [12, 12]
                        },
                resedentialAddress: {
                    required: true,
                    checkCurrentAddress: true,
//                    pincodelist:true


                },
                employmentType:
                        {

                            required: true

                        },
                ITRStaus:
                        {
                            required: true
                        },
                companyName:
                        {
                            required:true
                        },
                iciciBankRelationship:
                        {
                            required: true

                        },

                        ICICIBankRelationShipNo:
                        {
                           
//                                    new cr start
                            required: {
                                depends: function (element) {
                                    if (($("#iciciBankRelationship").val() == "Salary Account")) {
                                        return true;
                                    } else {
                                        $("#ICICIBankRelationShipNo-error").hide();
                                        return false;
                                    }
                                }
                            },
                            minsal: {
                                depends: function (element) {
                                    if (($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Salary Account")) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            minsav: {
                                depends: function (element) {
                                    if (($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Savings Account")) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            mincur:{
                                depends: function (element) {
                                    if (($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Current Account")) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            minloan: {
                                depends: function (element) {
                                    if (($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Loan Account")) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            checkalphanumeric: {
                                depends: function (element) {
                                    if (($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Loan Account")) {
                                        return true;
                                    } else {
                                        return false;
                                    }

                                }
                            },
                            digits: {
                                depends: function (element) {

                                    if ((($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Salary Account"))) {
                                        return true;
                                    } else if ((($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Savings Account"))) {

                                        return true;
                                    } 
                                     else if ((($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Current Account"))) {

                                        return true;
                                    }else {
                                        return false;
                                    }

                                }
                            },
                        },
                salaryBankWithOtherBank :
                        {
                            required: true
                        },
                salaryAccountOpened :
                        {
                            required:
                                    {
                                        depends: function (element) {
                                            return ($("#employmentType").val() == "Salaried");
                                        }
                                    }
                        },
  
                companyAddress: {
                    required: true,
                    checkCompanyAddress: true,
                    minlength: 3
                },
                homeNumber: {
                    required: {
                        depends: function (element) {
                            return ($("#OPhone").val() == "");
                        }
                    },
                },
                termsAndConditions:
                        {
                            required: true
                        }
            },

            messages: {

                fullName: {
                    required: "Please Enter Full Name",
                    checkFullName: ""
//                    nameValidation: "Name can have only letters"
                   


                },
                dateOfBirth: {
                    required: "Please Select Date of Birth"
//                     checkdateOfBirth:"Age should between 18 to 65"
                },
                gender: {
                    required: "Please Select Gender"
                },
                email: {
                    required: "Please Enter E-mail ID",
                    emailValidation: "E-mail ID is not valid"
                },
                mobile: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid",
                    mobileValidation: "Mobile Number is not valid",
                    maxlength: "Please ensure mobile number is limited to 10 digits"
                },
                eduQualification: {
                    //required: "Please Select Qualification"
                },
                totalExperience: {
                   required: "Please Enter Total Work Experience"
                },
                monthlyIncome: {
                    required: "Please Enter Your Net Annual Income (Excluding Bonus Or Incentives)",
                    digits: "Net Annual Income is not Valid",
                    // monthlyValidation: "Net Annual Income must be greater than or equal to 6 lakhs."
                },
                pancard: {
                    required: "Please Enter Pancard Number",
                    IciciPanValidation: "Please Enter a valid PAN Number"
                },
                aadharNumber:
                        {
                            digits: "Aadhaar Number is not valid",
                            rangelength: "Maximum of 12 characters permitted for Aadhaar Number"
                        },
                resedentialAddress: {
                    required: "Please provide your Current Residential Address",
                    checkCurrentAddress: "Please provide your Current Residential Address"

                },
                employmentType: {
                    required: "Please Select Employment Type"
                },
                companyName: {
                    required: "Please Enter Company Name"

                },
                ITRStaus:
                        {
                            required: "Please Select ITR Status"
                        },
                iciciBankRelationship:
                        {
                            required: "Please select ICICI Bank Relationship"
                        },
                ICICIBankRelationShipNo:
                        {
                            required:"Please enter ICICI Account Number",
                            digits:"Please provide Numeric values",
                            minsal:"Please Enter only 12-digit Numeric values",
                            minsav:"Please Enter only 12-digit Numeric values",
                            mincur : "Please Enter only 12-digit Numeric values",
                            minloan:"Please Enter only 16-character Alphanumeric values",
//                          minLength:"Please Enter only 12-digit Numeric values",
//                          maxlength:"Please Enter only 16-character Alphanumeric values",
                            checkalphanumeric:"Please provide Alphanumeric values"
                        },
                salaryBankWithOtherBank:
                        {
                            required: "Please select Salary Account With Other Bank"
                        },
                salaryAccountOpened:
                        {
                            required: "Please select Salary Account Opened"
                        },
  
                companyAddress: {
                    required: "Please provide your Company Address",
                    checkCompanyAddress: "",
                    minlength: "minimum length is 3"
                },
                homeNumber : {
                    required : "Please enter Home Landline number and must have exact 11 digits including STD code",
                },
                termsAndConditions: {
                    required: "Please agree to be contacted by ICICI Bank"
                }
            },
            onfocusout: function (element) {
                this.element(element);  // <- "eager validation"
            },
            errorElement: "div",
            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },
            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                if ($(element).hasClass("datepicker1 row margT1 input hasDatepicker")) {
                    error.insertAfter($(".datepicker_icon"));
                } else if ($(element).hasClass("margT1 statement")) {
                    error.insertAfter($(element).closest("div"));
                } else if ($(element).hasClass("row margT1 input name_inputBox")) {
                    error.insertAfter($(".name_div"));
                } else if ($(element).hasClass("row margT1 input currentResidentialAddress")) {
                    error.insertAfter($(".residentialAddr_div"));
                } else if ($(element).hasClass("row margT1 input companyAddress_box")) {
                    error.insertAfter($(".companyAddr_div"));
                } else if ($(element).hasClass("row margT1 input homeNumber")) {
                    error.insertAfter($(".phoneNum_div"));
                }//                 new cr start
                else if ($(element).hasClass("row margT1 input totalExperience")) {
                    error.insertAfter($(".total_div"));
                }
//                 new cr end
                else {
                    error.insertAfter(element);
                }
            },
            submitHandler : function (form) {

                $('#city').prop('disabled', false);
                $('#state1').prop('disabled', false);
                $('#PCity').prop('disabled', false);
                $('#PState').prop('disabled', false);
                $('#OCity').prop('disabled', false);
                $('#OState').prop('disabled', false);
                $('.about .error_box').addClass('hidden');
                $("#salaryBankWithOtherBank").prop('disabled', false);

                if ($("#jetpriviligemembershipNumber").val() != "") {
                    $("#jpNumber").val($("#jetpriviligemembershipNumber").val());
                    $("#jpTier").val($("#jetpriviligemembershipTier").val());
                }

                var radioValue = $(".addressProof input[type='radio']:checked").val();
                if (radioValue == "No") {

                    var residentAddressField1 = $('.permanent_AddressField1').val();
                    var pin = $('.permanent_residentAddress .pincodeICICI').val();
                    var city = $('.permanent_residentAddress .city option:selected').text();
                    var state = $('.permanent_residentAddress .state option:selected').text();
                     checkpermanent();
                } else if(radioValue == "Yes") {
                    
                   $('.permanentAddr-error-div').html("");
                    $('.permanentAddr-error-div').addClass('hidden');
                    $(".permanent_AddressField1").val($(".residentAddressField1").val());
                    $(".permanent_AddressField2").val($(".residentAddressField2").val());
                    $(".permanent_AddressField3").val($(".residentAddressField3").val());
                    $('.permanent_residentAddress .pincodeICICI').val($('.residentAddress .pincodeICICI').val());
                    $('.permanent_residentAddress .city').val($('.residentAddress .city option:selected').text());
                    $('.permanent_residentAddress .state').val($('.residentAddress .state option:selected').text());
                    
                }

                var fName = $('.firstName').val();
                var mName = $('.middleName').val();
                var lName = $('.lastName').val();
                var msg = "Please ensure your keyword limited to 26 characters";
                var fNameLength = fName.length;
                var lNameLength = lName.length;
                var mNameLength = mName.length;
                var appendQuery = fNameLength > 26 && lNameLength > 26 && mNameLength > 26 ? "First Name, Middle Name, Last Name" : fNameLength > 26 && lNameLength > 26 ? "First Name, Last Name" : fNameLength > 26 && mNameLength > 26 ? "First Name, Middle Name" : mNameLength > 26 && lNameLength > 26 ? "Middle Name, Last Name" : fNameLength > 26 ? "First Name" : mNameLength > 26 ? "Middle Name" : lNameLength > 26 ? "Last Name" : "";
                msg = msg.replace('keyword', appendQuery);
                
                


             if(!hasalphabet(fName)|| !hasalphabet(lName)) {
               $('.fullName-error-div').html("Name can have only letters");
                    $('.fullName-error-div').removeClass('hidden');
                     $('html,body').stop(true, false).animate({scrollTop: $('.fullName').offset().top - 150}, 'fast');
                    return false;
                } 
                else if (mName != "" && !hasalphabet(mName)) {
                    $('.fullName-error-div').html("Name can have only letters");
                    $('.fullName-error-div').removeClass('hidden');
                     $('html,body').stop(true, false).animate({scrollTop: $('.fullName').offset().top - 150}, 'fast');
                     return false;
                }
                
               
            if (fNameLength > 26 || mNameLength > 26 || lNameLength > 26) {
                
                    $('.fullName-error-div').html(msg);
                    $('.fullName-error-div').removeClass('hidden');
                    $('html,body').stop(true, false).animate({scrollTop: $('.fullName').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".resedentialAddress-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.currentResAddress').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".permanentAddr-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.permanentAddr').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".companyAddr_div-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.companyAddr').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".homeNumber-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.homeNumber-error-div').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".officeNumber-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.officeNumber-error-div').offset().top - 150}, 'fast');
                    return false;
                }
                //                 new cr start
                if ($(".total_div-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.total_div-error-div').offset().top - 150}, 'fast');
                    return false;
                }
                if ($('.age_error_div').text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.age_error_div').offset().top - 150}, 'fast');
                    return false;
                }
//                 new cr end
                var offerContent = '${cardBean.offerDesc}';

                if($("#jpNumber").val() !== ""){
                    $('#jpNumpopup').attr('disabled', false);
                    
                    }
                    
               

                $("#provide_jpNumber").modal('show');
                $('.error_box').addClass('hidden');
                $('.error_box1').addClass('hidden');
                $("#jpNumpopup").on('click', function () {
                    if($("#jpNumber").val()==""){
                        $('#jpNumpopup').attr('disabled', true);
                    }
                   else if ($("#jpNumber").val() != "" && $("#jetpriviligemembershipTier").val() != "") {
                            form.submit();
                        $('#jpNumpopup').attr('disabled', true);
                        
                    } 
                    else {
                        $("#jpNumber").trigger('keyup');
                        if ($("#jpNumber").val() != '' && isCorrectJpNumber) {
                            form.submit();
                           
                         $('#jpNumpopup').attr('disabled', true);
                         $('#otpButton').attr('disabled', true);
                                $('.geninput').attr('disabled',true);
                        }
                    }
                    $('#icicidisable').show();
                     //Adobe code starts here
                     $.fn.AfterApplyJPNumberPopUpClick($("#jpNumber").val(),'JP Number' );
                     //Adobe code ends here
                });
                
                //Adobe code starts here
                 IsButtonClicked=true;
                $.fn.ApplyButtonAmexFormClick(fName+" "+mName+" "+lName,$("#email").val(),$("#mobile").val(),$('#city').val(),$("#pinCodeICICI").val(),'ICICI');
                var iciciURL = document.URL.substr(document.URL.lastIndexOf('/')+1);
                if(iciciURL.indexOf(-) !== -1) iciciURL = iciciURL.replaceAll('-',' ')
                var cardName = iciciURL.split("ICICI Bank")[1];
                $.fn.actionClickCT('Card Application: Submit',iciciURL,'ICICI Bank');
                //Adobe code ends here
            }
        });
        //generate otp 
          $(".genbut").click(function(){
                 
                 var mobileNumber=$('#mobile').val();
                 console.log("OTP Response "+mobileNumber);
                 $.get("${applicationURL}generateOTP?mobileNumber="+ mobileNumber, function(data){
		    if(data != ''){
                         var myObj = JSON.parse(data);
                         var mNumber = myObj.mobileNumber;
                         var time = myObj.Time;
                       
                       var successMsg;
                       if($('#otpButton').text() == 'Re-Generate OTP'){
                      
                      successMsg='A new OTP has been sent to your mobile number '+mNumber+' at '+time+'.';
                        }else{
                            
                          successMsg='OTP has been sent to your mobile number '+mNumber+' at '+time+'.';  
                            
                        }
                       
	         	console.log("OTP Response "+data);
//                         $('#regenerateotp').show();
                          $('.geninput').attr('disabled',false);
                          $('#otpButton').text('Re-Generate OTP');
                          $('#otpSucces').html(successMsg);
                          $('#otpSucces').show();
                         
                              
		   }
	       });
                 
                 
             });
        
        $("#jpNumber").keyup(function () {
            var jpNum = $(this).val();
            var cpNo = $("#cpNo").val();
            $('#jpNumpopup').attr('disabled', true);
            
            if (jpNum == "") {
                $(".error_msg").text("Please enter InterMiles No.");
                $(".error_msg").removeClass('hidden');
                $("#jpTier").val('');
                $("#jetpriviligemembershipTier").val('');
                isCorrectJpNumber = false;

                return false;
            } else
            if (jpNum != "" && jpNum.length == 9) {
                if (isNaN(jpNum)) {
                    $(".error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                    $(".error_msg").removeClass('hidden');
                    $("#jetpriviligemembershipTier").val('');
                    $('#otpButton').attr('disabled', true);
                    $('.geninput').attr('disabled',true);
                    isCorrectJpNumber = false;
                    return false;
                } else {
                    jpNumLastDig = jpNum % 10;
                    jpNoTemp = eval(jpNum.slice(0, -1)) % 7;

                    if (jpNumLastDig !== jpNoTemp)
                    {
                        $(".error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                        $(".error_msg").removeClass('hidden');
                        $("#jetpriviligemembershipTier").val('');
                        isCorrectJpNumber = false;
                        $('#otpButton').attr('disabled', true);
                        $('.geninput').attr('disabled',true);
                        return false;
                    } else {
                        $("#jetpriviligemembershipNumber").val(jpNum);
                        $.get("${applicationURL}getTier?jpNum=" + jpNum, function (data, status) {
                            if (status == "success" && data.length > 2) {
                                $("#jetpriviligemembershipTier").val(data);
                                isCorrectJpNumber = true;
                                $(".error_msg").text("");
                                $(".error_msg").addClass('hidden');
                                $('#jpNumpopup').attr('disabled', false);
                                 $('#otpButton').attr('disabled', false);
                            } else {
                                $(".error_msg").text("Your application may not have gone through successfully. Please click Submit again.");
                                $(".error_msg").removeClass('hidden');
                                $("#jetpriviligemembershipTier").val('');
                                $('#jpNumpopup').attr('disabled', true);
                                $('#otpButton').attr('disabled', true);
                                $('.geninput').attr('disabled',true);
                                isCorrectJpNumber = false;
                                return false;
                            }
                        });

                    }
                }
            } else {

                $(".error_msg").removeClass('hidden');
                $("#jetpriviligemembershipTier").val('');
                isCorrectJpNumber = false;
                return false;
            }
        });
$(".proceedPopup_close").on('click',function(){
                console.log("in close button")
                 $(".error_msg").text('');
                 $(".error_msg").addClass('hidden');
                 $("#jpNumber").text('');
                 $("#jetpriviligemembershipNumber").val('');
                  $(".jpNumber_input").val('');
                  $("#jpTier").val(''); 
		  $("#jetpriviligemembershipTier").val('');
                  $(".otpsuccess").addClass('hidden');
                 $("#otpButton").attr("disabled",true)
                 });
        function addOfferToJpPopup(jsonStr) {
            $(".jpPopup_header").addClass("add_bullet");
            $(".jpPopup_header").html("<p>Avail Offer</p>" + jsonStr.offerDesc);
            $("#offerId").val(jsonStr.offerId);
            $("#offerDesc").val(jsonStr.offerDesc);
        }

        $("#mini_jpNumber").on('keyup', function () {
            var cpNo = $("#cpNo").val();
            var jpNum = $(this).val();
            if (jpNum != "" && jpNum.length == 9) {
                if (isNaN(jpNum)) {
                    $(".mini_error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                    $(".mini_error_msg").removeClass('hidden');
                    isValid = false;
                    return false;
                } else {
                    jpNumLastDig = jpNum % 10;
                    jpNoTemp = eval(jpNum.slice(0, -1)) % 7;
                    if (jpNumLastDig !== jpNoTemp)
                    {
                        $(".mini_error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                        $(".mini_error_msg").removeClass('hidden');
                        isValid = false;
                        return false;
                    } else {
                        $.get("${applicationURL}getOffer1?jpNum=" + jpNum + "&cpNo=" + cpNo, function (data, status) {
                            if (status == "success" && data.length > 2) {
                                var jsonStr = JSON.parse(data);
                                $(".offer_link").addClass("hidden");
                                $(".offer_display").removeClass("hidden");
                                $(".offer_display").html(jsonStr.offerDesc);
                                $(".login_offer").html(jsonStr.offerDesc);
                                addOfferToJpPopup(jsonStr);
                                $('.home_feature').each(function () {
                                    var toggle_img = "<div class='collps_img coldown'><img src=" + ctx + "static/images/co-brand/collps_down.png></div>" +
                                            "<div class='collps_img colUp' style='display:none'><img src=" + ctx + "static/images/co-brand/collps_up.png></div>";
                                    if ($(this).height() > 200) {
                                        $(this).addClass("trimmed");
                                        $(this).closest(".list_cont").after(toggle_img);
                                    }
                                });
                            }
                        });
                        $("#jetpriviligemembershipNumber").val(jpNum);
                        $(".mini_error_msg").text("");
                        $(".mini_error_msg").addClass('hidden');
                        isValid = true;
                    }
                }
            }
        });

        $("#mini_jpNumber_submit").on('click', function () {
            var jpNum = $("#mini_jpNumber").val();
            var originalJPNum = loginJPNum;
            if (jpNum == "" || jpNum.length <= 8) {
                $(".mini_error_msg").text("Please enter valid InterMiles No.");
                $(".mini_error_msg").removeClass('hidden');
                return false;
            }
            if (originalJPNum != jpNum) {
                $('select').val('');
                $('input[type=text]').val('');
            }
            if (isValid) {
                $('#jpNumpopup').attr('disabled', false);

            }
        });

        $("#state1").on("change", function () {
            var state = $("#state1 option:selected").val();
            var pin = $('.residentAddress .pincodeICICI').val();
            $.get("${applicationURL}getCitiesByState1?state=" + state, function (data) {
                $('#city').empty();
                $('#city').append("<option value=''>Select City</option>");
                if (data != '') {
                    $.each(data, function (index, value) {
                        $('#city').append($("<option>", {value: index, html: value}));

                    });
                }
            });
           CheckAddress('RES');
            $('#city').selectmenu('refresh', true);

        });

        $("#PState").on("change", function () {
            var PState = $("#PState option:selected").val();
            $.get("${applicationURL}getCitiesByState1?state=" + PState, function (data) {
                $('#PCity').empty();
                $('#PCity').append("<option value=''>Select City</option>");
                if (data != '') {
                    $.each(data, function (index, value) {
                        $('#PCity').append($("<option>", {value: index, html: value}));
                    });
                }
            });
            CheckAddress('PRES');
            $('#PCity').selectmenu('refresh', true);
        });

        $("#OState").on("change", function () {
            var OState = $("#OState option:selected").val();
            $.get("${applicationURL}getCitiesByState1?state=" + OState, function (data) {
                $('#OCity').empty();
                $('#OCity').append("<option value=''>Select City</option>");
                if (data != '') {
                    $.each(data, function (index, value) {
                        $('#OCity').append($("<option>", {value: index, html: value}));
                    });
                }

            });
            CheckAddress('COMP');
            
            $('#OCity').selectmenu('refresh', true);
        });
        
        
        

         $("#city").on("change", function () {
            var city = $("#city option:selected").val();
            var pin = $('.residentAddress .pincodeICICI').val();
            $.get("${applicationURL}getICICIStateByCity?city=" + city, function (data, status) {
                if (status == "success" && data.length >= 2) {
                    $("#state1").val(data);
                    var pin = $('.residentAddress .pincodeICICI').val();
                }
            });
            CheckAddress('RES');
        });

        $("#PCity").on("change", function () {
            var PCity = $("#PCity option:selected").val();
            $.get("${applicationURL}getICICIStateByCity?city=" + PCity, function (data, status) {
                if (status == "success" && data.length >= 2) {
                    $("#PState").val(data);
                }
            });
            CheckAddress('PRES');
        });


       $("#OCity").on("change", function () {
            var OCity = $("#OCity option:selected").val();
            $.get("${applicationURL}getICICIStateByCity?city=" + OCity, function (data, status) {
                if (status == "success" && data.length >= 2) {
                    $("#OState").val(data);
                }
            });
            CheckAddress('COMP');
        });

         var pincodemsg = 'We are sorry, American Express does not service this pincode, please enter an alternate pincode';
        var invalidMsg = "We are sorry, ICICI bank does not service this pincode, please enter an alternate pin code";

        $('#pinCodeICICI').on('keyup', function () {

            var pinCode = $(this).val();
            var cpNo = $("#cpNo").val();
            var cCity;
            var cState;
            if (pinCode != "" && pinCode.length == 6) {
                //$(".ui-menu-item").hide();
                $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
                    if (status == "success" && data != "excluded") {
                        $.get("${applicationURL}getCity1?pinCode=" + pinCode, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                var json = JSON.parse(data);
                                $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                    if (status == "success" && data.length != 2) {
                                        $("#city").val(json.city);
                                        $("#state1").val(json.state);
                                        $("#hcity").val(json.city);
                                        $("#hstate").val(json.state);
                                        $('#state1').attr("disabled", true);
                                        $('#city').attr("disabled", true);
                                             CheckAddress('RES');
                                         
                                        if ($("#city").val() == null || $("#city").val() == '') {
                                            $('#city').empty();
                                            $('select#city').append('<option>' + json.city + '</option>');
                                        }
                                    } else {
                                        $('.resedentialAddress-error-div').html(pincodemsg);
                                        $('.resedentialAddress-error-div').removeClass('hidden');
                                        $("#city").val("");
                                        $("#state1").val("");
                                        $('#state1').attr("disabled", false);
                                        $('#city').attr("disabled", false);
                                    }
                                });
                            } else {
                                $('.resedentialAddress-error-div').html(invalidMsg);
                                $('.resedentialAddress-error-div').removeClass('hidden');
                                $("#city").val("");
                                $("#state1").val("");
                                //------change here---------
                                $('#state1').attr("disabled", true);
                                $('#city').attr("disabled", true);
                                //---------change end here------
                            }
                        });
                    } else {
                        $('.resedentialAddress-error-div').html(pincodemsg);
                        $('.resedentialAddress-error-div').removeClass('hidden');
                        $("#city").val("");
                        $("#state1").val("");
                    }
                });


            } else {
                $("#city").val("");
                $("#state1").val("");
                $('#state1').attr("disabled", false);
                $('#city').attr("disabled", false);
            }
        });

        $('#PpinCodeICICI').on('keyup', function () {

            var pinCode = $(this).val();
            var cpNo = $("#cpNo").val();
            if (pinCode != "" && pinCode.length == 6) {
                $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
                    if (status == "success" && data != "excluded") {
                       
                        $.get("${applicationURL}getCity1?pinCode=" + pinCode, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                var json = JSON.parse(data);
                                $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                    if (status == "success" && data.length != 2) {
                                        $("#PCity").val(json.city);
                                        $("#PState").val(json.state);
                                        $("#hcity1").val(json.city);
                                        $("#hstate1").val(json.state);
                                        $('#PCity').attr("disabled", true);
                                        $('#PState').attr("disabled", true);
                                       
                                           CheckAddress('PRES');
                                        if ($("#PCity").val() == null || $("#PCity").val() == '') {
                                            $('#PCity').empty();
                                            $('select#PCity').append('<option>' + json.city + '</option>');
                                        }
                                    } else {
                                        $('.permanentAddr-error-div').html(pincodemsg);
                                        $('.permanentAddr-error-div').removeClass('hidden');
                                        $("#PCity").val("");
                                        $("#PState").val("");
                                        $('#PCity').attr("disabled", false);
                                        $('#PState').attr("disabled", false);
                                    }
                                });
                            } else {
                                $('.permanentAddr-error-div').html(invalidMsg);
                                $('.permanentAddr-error-div').removeClass('hidden');
                                $("#PState").val("");
                                //---------change here------------
                                $('#PCity').attr("disabled", true);
                                $('#PState').attr("disabled", true);
                                //--------change here end----------
                            }
                        });
                    } else {
                        $('.permanentAddr-error-div').removeClass('hidden');
                        $(".permanentAddr-error-div").html(pincodemsg);
                        $("#PCity").val("");
                        $("#PState").val("");
                    }
                });
            } else {
                $("#PCity").val("");
                $("#PState").val("");
                $('#PCity').attr("disabled", false);
                $('#PState').attr("disabled", false);
            }
        });

        $('#OPincodeICICI').on('keyup', function () {
            var pinCode = $(this).val();
            var cpNo = $("#cpNo").val();
            if (pinCode != "" && pinCode.length == 6) {
                $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
                    if (status == "success" && data != "excluded") {
                        $.get("${applicationURL}getCity1?pinCode=" + pinCode, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                var json = JSON.parse(data);
                                $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                    if (status == "success" && data.length != 2) {
                                        $("#OCity").val(json.city);
                                        $("#OState").val(json.state);
                                        $("#hcity2").val(json.city);
                                        $("#hstate2").val(json.state);
                                        $('#OCity').attr("disabled", true);
                                        $('#OState').attr("disabled", true);
                                        CheckAddress('COMP');
                                    
                                        if ($("#OCity").val() == null || $("#OCity").val() == '') {
                                            $('#OCity').empty();
                                            $('select#OCity').append('<option>' + json.city + '</option>');
                                        }
                                    } else {
                                        $('.companyAddr_div-error-div').html(pincodemsg);
                                        $('.companyAddr_div-error-div').removeClass('hidden');
                                        $("#OCity").val("");
                                        $("#OState").val("");
                                        $('#OCity').attr("disabled", false);
                                        $('#OState').attr("disabled", false);
                                    }
                                });
                            } else {
                                $('.companyAddr_div-error-div').html(invalidMsg);
                                $('.companyAddr_div-error-div').removeClass('hidden');
                                $("#OCity").val("");
                                $("#OState").val("");
                                //---------change here--------
                                $('#OCity').attr("disabled", true);
                                $('#OState').attr("disabled", true);
                                //----------change here end--------
                            }
                        });
                    } else {
                        $('.companyAddr_div-error-div').html(pincodemsg);
                        $('.companyAddr_div-error-div').removeClass('hidden');
                        $("#OCity").val("");
                        $("#OState").val("");
                    }
                });
            } else {
                $("#OCity").val("");
                $("#OState").val("");
                $('#OCity').attr("disabled", false);
                $('#OState').attr("disabled", false);
            }
        });

//                  icici pincode

        var searchTerms =${pincode};

        var array = searchTerms.toString().split(",");
        var availableTags = array;

        $("#pinCodeICICI").autocomplete({
//                        disabled: false,
// new cr start
            source: function (request, response) {
                var matches = $.map(availableTags, function (acItem) {
                    if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return acItem;
                    }
                });
                response(matches);
            },
            minLength: 2,
            focus: function (event, ui) {
                event.preventDefault();
            },
            select: function (event, ui) {

                $("#pinCodeICICI").val(ui.item.value);
                var pinCode = ui.item.value;
              
                var cpNo = $("#cpNo").val();
                var cCity;
                var cState;
                if (pinCode != "" && pinCode.length == 6) {

                    $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {

                        if (status == "success" && data != "excluded") {

                            $.get("${applicationURL}getCity1?pinCode=" + ui.item.value, function (data, status) {

                                if (status == "success" && data.length != 2) {

                                    var json = JSON.parse(data);
                                    $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                        if (status == "success" && data.length != 2) {

                                            $("#city").val(json.city);
                                            $("#state1").val(json.state);

                                            $("#hcity").val(json.city);
                                            $("#hstate").val(json.state);
                                            $('#state1').attr("disabled", true);
                                            $('#city').attr("disabled", true);

                                            // Code for Assigning values
                                            residentAddressField1 = $('.residentAddressField1').val();
                                            residentAddressField2 = $('.residentAddressField2').val();
                                            residentAddressField3 = $('.residentAddressField3').val();
                                            city = $("#city").val();
                                            state = $("#state1").val();
                                            pinCode1 = $("#pinCodeICICI").val();
                                            $('.currentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);

                                            // End Code for Assigning values

                                            if ($("#city").val() == null || $("#city").val() == '') {
                                                $('#city').empty();
                                                $('select#city').append('<option>' + json.city + '</option>');
                                            }
                                        } else {
                                            $('.resedentialAddress-error-div').html(pincodemsg).css("color", "#ff0000");
                                            $('.resedentialAddress-error-div').removeClass('hidden');
                                            $('#state1').attr("disabled", false);
                                            $('#city').attr("disabled", false);
                                        }
                                    });
                                } else {
                                    $('.resedentialAddress-error-div').html(invalidMsg).css("color", "#ff6e00");
                                    $('.resedentialAddress-error-div').removeClass('hidden');
                                    $("#city").val("");
                                    $("#state1").val("");
                                    $('#state1').attr("disabled", false);
                                    $('#city').attr("disabled", false);
                                }
                            });
                        } else {
                            $('.resedentialAddress-error-div').html(pincodemsg).css("color", "#ff0000");
                            $('.resedentialAddress-error-div').removeClass('hidden');

                        }
                    });
                }
                residentAddressField1 = $('.residentAddressField1').val();
                residentAddressField2 = $('.residentAddressField2').val();
                residentAddressField3 = $('.residentAddressField3').val();
                pin = $("#pinCodeICICI").val();
                city = $("#city").val();
                state = $("#state1").val();
                if (($('.residentAddress .city option:selected').val()) != 0) {
                    city = $('.residentAddress .city option:selected').text();
                }
                if (($('.residentAddress .state option:selected').val()) != 0) {
                    state = $('.residentAddress .state option:selected').text();
                }

                if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
                    $('#resedentialAddress-error').css('display', 'block');
                else
                    $('#resedentialAddress-error').css('display', 'none');

                $('.residentialAddr_div').addClass('hidden');
                $('.currentResidentialAddress').removeClass('hidden');
                $('.currentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

                if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
                {
                    $('.currentResidentialAddress').val('');
                }
                // Code for Assigning values
                residentAddressField1 = $('.residentAddressField1').val();
                residentAddressField2 = $('.residentAddressField2').val();
                residentAddressField3 = $('.residentAddressField3').val();
                city = $("#city").val();
                state = $("#state1").val();
                pinCode1 = $("#pinCodeICICI").val();
                $('.currentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
                // End Code for Assigning values
                //$('.resedentialAddress-error-div').html('');
                //$('.resedentialAddress-error-div').addClass('hidden');
                CheckAddress('RES');
                return false;

            },

            response: function (event, ui) {

            }

        });

        //office new code start here 

        $("#OPincodeICICI").autocomplete({
//                        disabled: false,
           source: function (request, response) {
                var matches = $.map(availableTags, function (acItem) {
                    if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return acItem;
                    }
                });
                response(matches);
            },
          
            minLength: 2,
            focus: function (event, ui) {
                event.preventDefault();
            },
            select: function (event, ui) {

                $("#OPincodeICICI").val(ui.item.value);
                var pinCode = ui.item.value;
                var cpNo = $("#cpNo").val();
                var cCity;
                var cState;
                if (pinCode != "" && pinCode.length == 6) {

                    $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {

                        if (status == "success" && data != "excluded") {

                            $.get("${applicationURL}getCity1?pinCode=" + ui.item.value, function (data, status) {

                                if (status == "success" && data.length != 2) {

                                    var json = JSON.parse(data);
                                    $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                        if (status == "success" && data.length != 2) {

                                            $("#OCity").val(json.city);
                                            $("#OState").val(json.state);
                                            $("#hcity2").val(json.city);
                                            $("#hstate2").val(json.state);
                                            $('#OState').attr("disabled", true);
                                            $('#OCity').attr("disabled", true);


                                            // Code for Assigning values
                                            residentAddressField1 = $('.companyAddressField1').val();
                                            residentAddressField2 = $('.companyAddressField2').val();
                                            residentAddressField3 = $('.companyAddressField3').val();
                                            city = $("#OCity").val();
                                            state = $("#OState").val();
                                            pinCode1 = $("#OPincodeICICI").val();
                                            $('.companyAddress_box').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);

                                            // End Code for Assigning values

                                            if ($("#OCity").val() == null || $("#OCity").val() == '') {
                                                $('#OCity').empty();
                                                $('select#OCity').append('<option>' + json.city + '</option>');
                                            }
                                        } else {
                                            $('.companyAddr_div-error-div').html(pincodemsg).css("color", "#ff0000");
                                            $('.companyAddr_div-error-div').removeClass('hidden');
                                            
                                            $('#OState').attr("disabled", false);
                                            $('#OCity').attr("disabled", false);
                                        }
                                    });
                                } else {
                                    $('.companyAddr_div-error-div').html(invalidMsg).css("color", "#ff6e00");
                                    $('.companyAddr_div-error-div').removeClass('hidden');
                                    $("#OCity").val("");
                                    $("#OState").val("");
                                    $('#OState').attr("disabled", false);
                                    $('#OCity').attr("disabled", false);
                                }
                            });
                        } else {
//                            console.log("In ELSE : 1380");
                            $('.companyAddr_div-error-div').html(pincodemsg).css("color", "#ff0000");
                            $('.companyAddr_div-error-div').removeClass('hidden');

                        }
                    });
                }
                residentAddressField1 = $('.companyAddressField1').val();
                residentAddressField2 = $('.companyAddressField2').val();
                residentAddressField3 = $('.companyAddressField2').val();
                pin = $("#OPincodeICICI").val();
                city = $("#OCity").val();
                state = $("#OState").val();
                if (($('.companyAddress .city option:selected').val()) != 0) {
                    city = $('.companyAddress .city option:selected').text();
                }
                if (($('.companyAddress .state option:selected').val()) != 0) {
                    state = $('.companyAddress .state option:selected').text();
                }

                if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
                    $('#companyAddress-error').css('display', 'block');
                else
                    $('#companyAddress-error').css('display', 'none');

                $('.companyAddr_div').addClass('hidden');
                $('.companyAddress_box').removeClass('hidden');
                $('.companyAddress_box').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

                if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
                {
                    $('.companyAddress_box').val('');
                }
                // Code for Assigning values
                residentAddressField1 = $('.residentAddressField1').val();
                residentAddressField2 = $('.residentAddressField2').val();
                residentAddressField3 = $('.residentAddressField3').val();
                city = $("#OCity").val();
                state = $("#OState").val();
                pinCode1 = $("#OPincodeICICI").val();
                $('.companyAddress_box').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
                // End Code for Assigning values
                //$('.companyAddr_div-error-div').html('');
                //$('.companyAddr_div-error-div').addClass('hidden');
                   CheckAddress('COMP');


                return false;

            },

            response: function (event, ui) {

            }

        });

        //office new code end here

        $("#PpinCodeICICI").autocomplete({
//                        disabled: false,
             source: function (request, response) {
                var matches = $.map(availableTags, function (acItem) {
                    if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return acItem;
                    }
                });
                response(matches);
            },
            minLength: 2,
            focus: function (event, ui) {
                event.preventDefault();
            },
            select: function (event, ui) {

                $("#PpinCodeICICI").val(ui.item.value);
                var pinCode = ui.item.value;
                var cpNo = $("#cpNo").val();
                var cCity;
                var cState;
                if (pinCode != "" && pinCode.length == 6) {

                    $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {

                        if (status == "success" && data != "excluded") {

                            $.get("${applicationURL}getCity1?pinCode=" + ui.item.value, function (data, status) {

                                if (status == "success" && data.length != 2) {

                                    var json = JSON.parse(data);
                                    $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                        if (status == "success" && data.length != 2) {

                                            $("#PCity").val(json.city);
                                            $("#PState").val(json.state);

                                            $("#hcity1").val(json.city);
                                            $("#hstate1").val(json.state);
                                            $('#PState').attr("disabled", true);
                                            $('#PCity').attr("disabled", true);

                                            // Code for Assigning values
                                            residentAddressField1 = $('.permanent_AddressField1').val();
                                            residentAddressField2 = $('.permanent_AddressField2').val();
                                            residentAddressField3 = $('.permanent_AddressField3').val();
                                            city = $("#PCity").val();
                                            state = $("#PState").val();
                                            pinCode1 = $("#PpinCodeICICI").val();
                                            $('.permanentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);

                                            // End Code for Assigning values

                                            if ($("#PCity").val() == null || $("#PCity").val() == '') {
                                                $('#PCity').empty();
                                                $('select#PCity').append('<option>' + json.city + '</option>');
                                            }
                                        } else {
                                            $('.permanentAddr-error-div').html(pincodemsg).css("color", "#ff0000");
                                            $('.permanentAddr-error-div').removeClass('hidden');
                                            $('#PState').attr("disabled", false);
                                            $('#PCity').attr("disabled", false);
                                        }
                                    });
                                } else {
                                    $('.permanentAddr-error-div').html(invalidMsg).css("color", "#ff6e00");
                                    $('.permanentAddr-error-div').removeClass('hidden');
                                    $("#PCity").val("");
                                    $("#PState").val("");
                                    $('#PState').attr("disabled", false);
                                    $('#PCity').attr("disabled", false);
                                }
                            });
                        } else {
//                            console.log("In ELSE : 1380");
                            $('.permanentAddr-error-div').html(pincodemsg).css("color", "#ff0000");
                            $('.permanentAddr-error-div').removeClass('hidden');

                        }
                    });
                }
                residentAddressField1 = $('.permanent_AddressField1').val();
                residentAddressField2 = $('.permanent_AddressField2').val();
                residentAddressField3 = $('.permanent_AddressField3').val();
                pin = $("#PpinCodeICICI").val();
                city = $("#PCity").val();
                state = $("#PState").val();
                if (($('.permanent_residentAddress .city option:selected').val()) != 0) {
                    city = $('.permanent_residentAddress .city option:selected').text();
                }
                if (($('.permanent_residentAddress .state option:selected').val()) != 0) {
                    state = $('.permanent_residentAddress .state option:selected').text();
                }

                if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
                    $('#permanentAddr-error').css('display', 'block');
                else
                    $('#permanentAddr-error').css('display', 'none');

                $('.permanentAddr_div').addClass('hidden');
                $('.permanentResidentialAddress').removeClass('hidden');
                $('.permanentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

                if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
                {
                    $('.permanentResidentialAddress').val('');
                }
                // Code for Assigning values
                residentAddressField1 = $('.permanent_AddressField1').val();
                residentAddressField2 = $('.permanent_AddressField2').val();
                residentAddressField3 = $('.permanent_AddressField3').val();
                city = $("#PCity").val();
                state = $("#PState").val();
                pinCode1 = $("#pinCodeICICI").val();
                $('.permanentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
                CheckAddress('PRES');
                return false;

            },

            response: function (event, ui) {

            }

        });


        //permanent code end here
       

        $("#quickEnrollUser").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                enrollTitle: {
                    required: true,
                },
                enrollFname: {
                    required: true,
                    maxlength: 26,
                    AlphabetsOnly: true
                },
                enrollLname: {
                    required: true,
                    maxlength: 26,
                    AlphabetsOnly: true
                },
                enrollCity: {
                    required: true,
                    AlphabetsOnly: true
                },
                enrollemail: {
                    required: true,
                    emailValidation: true
                },
                enrollPhone: {
                    required: true,
                    mobileValidation: true
                },
                enrollPassword: {
                    required: true,
                    passwordChecking: true
                },
                enrollRenterPassword: {
                    required: true,
                    equalTo: "#enrollPassword",
                    passwordChecking: true
                },
                enrollDob: {
                    required: true
                },
                termsAndConditionsEnroll:{
                    required: true
                }
            },

            messages: {
                enrollTitle: {
                    required: "Please Select Title",

                },
                enrollFname: {
                    required: "Please Enter FirstName",
                    maxlength: "Maximum Length should 26",
                    AlphabetsOnly: "Alphabate only"
                },
                enrollLname: {
                    required: "Please Enter LastName",
                    maxlength: "Maximum Length should 26",
                    AlphabetsOnly: "Alphabate only"
                },
                enrollCity: {
                    required: "Please Enter City Name",
                    AlphabetsOnly: "Alphabate only"
                },
                enrollemail: {
                    required: "Please Enter Email Id",
                    emailValidation: "Email ID is not valid."
                },
                enrollPassword: {
                    required: "Please enter new password",
                    passwordChecking: '<ul class="pwd_error"><li>Password must have:</li> <div class="pwd_error_content"><li> 8 - 13 characters.</li><li> at least 1 lowercase alphabet.</li><li> at least 1 uppercase alphabet.</li><li> at least 1 number.</li></div></ul>'
                },
                enrollRenterPassword: {
                    required: "Please Re enter password",
                    equalTo: "Passwords do not match",
                    passwordChecking: '<ul class="pwd_error"><li>Re Enter Password must have:</li> <div class="pwd_error_content"><li> 8 - 13 characters.</li><li> at least 1 lowercase alphabet.</li><li> at least 1 uppercase alphabet.</li><li> at least 1 number.</li></div></ul>'
                },
                enrollPhone: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid.",
                    mobileValidation: "Please Enter Valid Mobile Number"
                },
                enrollDob: {
                    required: "Please Enter Date of Birth",
                },
                termsAndConditionsEnroll:{
                    required:"Please accept the InterMiles membership Terms & Conditions to proceed."
                }
            },
            errorElement: "div",
            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },

            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                $('.error_box2').removeClass('hidden');
                error.insertAfter($(element).closest("div"));
                $(error).appendTo('.error_box2 ul.error_list2');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');

            },
            submitHandler: function (form) {
                form.submit();
            },

        });

        $(document).click(function (event) {
            var len = $('.error_list > li:visible').length;
            if (!(len >= 1)) {
                $('.error_box').addClass('hidden');
            } else {
                $('.error_box').removeClass('hidden');
            }
        });
        $('.enroll_popup').click(function (event) {
            var len = $('.error_list2 > li:visible').length;
            if (!(len >= 1)) {
                $('.error_box2').addClass('hidden');
            } else {
                $('.error_box2').removeClass('hidden');
            }
        });

        $("#quickEnroll").click(function () {
            $('.enroll_popup').removeClass('hidden');
            $('.jpNum_popup').addClass('hidden');
            var fname = $("#fname").val();
            var mname = $("#mname").val();
            var lname = $("#lname").val();
            var phone = $("#mobile").val();
            var email = $("#email").val();
            var dob = $("#dateOfBirth").val();
            $("#enrollFname").val(fname);
            $("#enrollMname").val(mname);
            $("#enrollLname").val(lname);
            $("#enrollemail").val(email);
            $("#enrollPhone").val(phone);
            $("#enrollDob").val(dob);
        });

        $("#social-profiles").on('click', function () {
            var fullName = "";
            var fname = $("#fname").val();
            var mname = $("#mname").val();
            var lname = $("#lname").val();
            var phone = $("#mobile").val();
            var email = $("#email").val();
            var formNumber = '${formNumber}';
            var jetpriviligemembershipNumber = $("#jetpriviligemembershipNumber").val();
            if (fname != "" || mname != "" || lname != "")
                fullName = fname + " " + mname + " " + lname;
            $("#socialName").val(fullName);
            $("#socialPhone").val(phone);
            $("#socialEmail").val(email);
            $("#socialjpNumber").val(jetpriviligemembershipNumber);
            $("#formNumber").val(formNumber);
            //$('#callMe_modal').modal('show');
        });

        $('#enrollForm1,#enrollForm_mob_view1').on('click', function () {
            var fees, cardname, cardimg, bankName;
            var id = $("#cpNo").val();
            var bpNo = '${cardBean.bpNo}';
            if ($("#quickEnrollUser").validate().element('#enrollTitle') && $("#quickEnrollUser").validate().element('#enrollFname')
                    && $("#quickEnrollUser").validate().element('#enrollLname') && $("#quickEnrollUser").validate().element('#enrollCity')
                    && $("#quickEnrollUser").validate().element('#enrollPhone') && $("#quickEnrollUser").validate().element('#enrollemail')
                    && $("#quickEnrollUser").validate().element('#enrollDob') && $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll'))
            {
                 var data;
                                    
                                    
                              var responsecode=$('#g-recaptcha-response').val()
	 		$.ajax({ method : 'POST',
                                 url:'${applicationURL}verifycaptcha?responsecode='+responsecode, success: function (result) {
                                data=result;
                                 if(data == false){
                             $('.error_box1').removeClass('hidden');
                              $('.error_box1 ul.error_list1').html("Please select captcha");
                               $('.modal').stop(true, false).animate({ scrollTop: 0 }, 'slow');
                        }else{
                             $('.error_box1').addClass('hidden');
                             $('.error_box1 ul.error_list1').html("");
                //password hashing//
                var enrollPassword = "";
                var hashInBase64 = "";
                var hash = "";
                debugger;
                enrollPassword = $("#enrollPassword").val();
                hash = CryptoJS.HmacSHA256(enrollPassword, "bff29f3a-90d4-42d5-94f0-448e699991c1");
                hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
                $("#hashPassword").val(hashInBase64);
                $("#enrollbpNo").val(bpNo);
                //password hashing//

                $('#enroll_error').addClass('hidden');
                $('#loader').removeClass('hidden');
                $('.modal-backdrop').css('z-index', '1050');
                 var checkbox = $( "#termsAndConditionsEnroll" );
                         checkbox.val(checkbox[0].checked ? "true" : "false" );
                          var checkbox2 = $( "#recieveMarketingTeam" );
                         checkbox2.val(checkbox2[0].checked ? "true" : "false" );
                $.ajax({
                    url: '${applicationURL}enrollme',
                    data: $('#quickEnrollUser').serialize(),
                    type: 'POST',
                    success: function (data) {
                        var jsonStr = JSON.parse(data);
                        if (jsonStr.msg == "" && jsonStr.jpNumber != "" && jsonStr.jpNumber!="null") {
                            var radioValue = $("#quickEnrollUser input[type='radio']:checked").val();
                            $("#title").val($("#enrollTitle").val());
                            $("#gender").val(radioValue);
                            $("#fname").val($("#enrollFname").val());
                            $("#mname").val($("#enrollMname").val());
                            $("#lname").val($("#enrollLname").val());
                            $("#email").val($("#enrollemail").val());
                            $("#mobile").val($("#enrollPhone").val());
                            $("#dateOfBirth").val($("#enrollDob").val());
                            $("#jpNumber").val(jsonStr.jpNumber);
                            $("#jetpriviligemembershipNumber").val(jsonStr.jpNumber);
                            $("#cpNo").val(id);
                            $('.enroll_popup').addClass('hidden');
                            $('.jpNum_popup').removeClass('hidden');
                            $("#jpNumber").trigger('keyup');
                            $('form#iciciBean').submit();
                        } else {
                            $('#loader').addClass('hidden');
                            $('#enroll_error').removeClass('hidden');
                            if (jsonStr.msg == "")
                                $('#enroll_error').text("Your application may not have gone through successfully. Please click 'Continue' again.");
                            else
                                $('#enroll_error').text(jsonStr.msg);
                            $('.modal-backdrop').css('z-index', '1048');
                        }
                    },
                    complete: function () {
                        $('#loader').addClass('hidden');
                        $('.modal-backdrop').css('z-index', '1048');
                    },
                    error: function (xhrcallMe, status) {

                    }
                });
                 }
                        },
                            error: function (e) {
                         console.log("error---->"+e);
                    }
                        });
            }
        });

        $(".title_select").change(function () {
            if ($(this).find(":selected").text() == "Mr") {
                $("#quickEnrollUser input[type=radio][value=0]").prop('checked', true);

            }
            if ($(this).find(":selected").text() == "Ms" || $(this).find(":selected").text() == "Mrs") {
                $("#quickEnrollUser input[type=radio][value=1]").prop('checked', true);
            }
        });

        $('#enrollForm1,#enrollForm_mob_view1').on('click', function () {
            var len = $('.error_list2 > li:visible').length;
            if (len >= 1)
                $('.modal').animate({scrollTop: 0}, 'slow');
        });

        function maxLength(value) {
            return /^.{1,24}$/.test(value);
        }

        function checkAddressLength(residentAddressField1, residentAddressField2, residentAddressField3) {
            if (residentAddressField1 != "" && !maxLength(residentAddressField1)) {
                $('.resedentialAddress-error-div').html('Address cannot be greater than 24 characters in each address line');
                $('.resedentialAddress-error-div').removeClass('hidden');
            }
            if (residentAddressField2 != "" && !maxLength(residentAddressField2)) {
                $('.resedentialAddress-error-div').html('Address cannot be greater than 24 characters in each address line');
                $('.resedentialAddress-error-div').removeClass('hidden');
            }
            if (residentAddressField3 != "" && !maxLength(residentAddressField3)) {
                $('.resedentialAddress-error-div').html('Address cannot be greater than 24 characters in each address line');
                $('.resedentialAddress-error-div').removeClass('hidden');
            }

        }


       // Code for checking Address
       $('form input, form select').focus(function () {
            residentAddressField1 = $('.residentAddressField1').val();
            residentAddressField2 = $('.residentAddressField2').val();
            residentAddressField3 = $('.residentAddressField3').val();
            city = $("#city").val();
            state = $("#state1").val();
            pinCode1 = $("#pinCodeICICI").val();
            if (residentAddressField1 != "" || residentAddressField2 != "" || residentAddressField3 != "" || city != "" || state != "" || pinCode1 != "") {
                $('#resedentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
            }

            if ($('#resedentialAddress').val() != "" && residentAddressField2.trim().length == 0)
            {
                $('.resedentialAddress-error-div').removeClass('hidden');
                $('.resedentialAddress-error-div').text("Please provide your Current Residential Address");

            }

            var lResAddrss = $('#resedentialAddress').val()
            var bRValue = /^[A-Za-z0-9#',\/\s]*$/.test(lResAddrss)
            if (!bRValue)
            {

                $('.resedentialAddress-error-div').removeClass('hidden');
                $('.resedentialAddress-error-div').text("Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hash,and apostrophe.");


            }
            residentAddressField1 = $('.companyAddressField1').val();
            residentAddressField2 = $('.companyAddressField2').val();
            residentAddressField3 = $('.companyAddressField3').val();
            pin = $("#OPincodeICICI").val();
            city = $("#OCity").val();
            state = $("#OState").val();
            if (residentAddressField1 != "" || residentAddressField2 != "" || residentAddressField3 != "" || city != "" || state != "" || pin != "") {
                $('#companyAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
            }
            if ($('#companyAddress').val() != "" && residentAddressField2.trim().length == 0)
            {
                $('.companyAddr_div-error-div').removeClass('hidden');
                $('.companyAddr_div-error-div').text("Please provide your company address");

            }
            var lComAddrss = $('#companyAddress').val()
            var bCValue = /^[A-Za-z0-9#',\/\s]*$/.test(lComAddrss)
            if (!bCValue)
            {
                $('.companyAddr_div-error-div').removeClass('hidden');
                $('.companyAddr_div-error-div').text("Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hash,and apostrophe.");


            }
           

            // PermananetAddress
            residentAddressField1 = $('.permanent_AddressField1').val();
            residentAddressField2 = $('.permanent_AddressField2').val();
            residentAddressField3 = $('.permanent_AddressField3').val();
            pin = $("#PpinCodeICICI").val();
            city = $("#PCity").val();
            state = $("#PState").val();
            if (residentAddressField1 != "" || residentAddressField2 != "" || residentAddressField3 != "" || city != "" || state != "" || pin != "") {
                $('#permanentAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
            }
            
                
            
            var lPResAddrss = $('#permanentAddress').val()
            var bPRValue = /^[A-Za-z0-9#',\/\s]*$/.test(lPResAddrss)
            if (!bPRValue)
            {
                var radioValue = $(".addressProof input[type='radio']:checked").val();
                if (radioValue === 'No')
                {
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hash,and apostrophe.");

                }
            }          


        });
                                    
                                     $('.submit_btn').click(function () {

              checkpermanent();
        });
                                    
    });
    
    
    $('#jp_btn').click(function(){
// new cr satrt
    $('#totalExperience-error').hide();
   if($('.age_error_div').is(':visible')){
      
       $("html, body").animate({ scrollTop: 400 }, 600);
    return false;
   }else
   if($("#dateOfBirth-error").is(':visible') || $('.fullName-error-div').is(':visible') || $("#mobile-error").is(':visible')||$("#email-error").is(':visible')||$("#gender-error").is(':visible') || $("#fullName-error").is(':visible')){
       
       $("html, body").animate({ scrollTop: 500 }, 600);
    return false;
   }else
    if($("#monthlyIncome-error").is(':visible') || $("#pancard-error").is(':visible') || $(".resedentialAddress-error-div").is(':visible')){
       
       $("html, body").animate({ scrollTop: 700 }, 600);
    return false;
   }else
   if($(".permanentAddr-error-div").is(':visible') || $("#employmentType-error").is(':visible') || $("#ITRStaus-error").is(':visible')){
      
       $("html, body").animate({ scrollTop: 1200 }, 600);
    return false;
   }else
   if($("#iciciBankRelationship-error").is(':visible') || $("#ICICIBankRelationShipNo-error").is(':visible') || $("#salaryAccountOpened-error").is(':visible')
           || $("#salaryBankWithOtherBank-error").is(':visible')){
       
       $("html, body").animate({ scrollTop: 1500 }, 600);
    return false;
   }else
   if($("#companyName-error").is(':visible')|| $('.total_div-error-div').is(':visible')){
    
       $("html, body").animate({ scrollTop: 1800 }, 600);
    return false;  
   }else 
   if($("#companyAddress-error").is(':visible')||$("#homeNumber-error").is(':visible')||$('.homeNumber-error-div').is(':visible')||
       $('.officeNumber-error-div').is(':visible')||$('.companyAddr_div-error-div').is(':visible')||$("#termsAndConditions-error").is(':visible')){
   
    $("html, body").animate({ scrollTop:2000 }, 600);
    return false;  
   }else{
      
       $("html, body").animate({ scrollTop:2500 }, 600);
    return false;  
   }
//   // new cr end
 });
 
 function CheckAddress(addrDetails)
    {
        if (addrDetails == 'RES')
        {
            residentAddressField1 = $('.residentAddressField1').val();
            residentAddressField2 = $('.residentAddressField2').val();
            residentAddressField3 = $('.residentAddressField3').val();
            city = $("#city").val();
            state = $("#state1").val();
            pinCode1 = $("#pinCodeICICI").val();
            $('#resedentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);


            var lResAddrss = $('#resedentialAddress').val()
            var bRValue = /^[A-Za-z0-9#',\/\s]*$/.test(lResAddrss)
           
            if (bRValue)
            {
                $('.resedentialAddress-error-div').html('');  //  CheckAddress('RES')
                $('.resedentialAddress-error-div').addClass('hidden');
            }

        }
        if (addrDetails == 'PRES')
        {


            residentAddressField1 = $('.permanent_AddressField1').val();
            residentAddressField2 = $('.permanent_AddressField2').val();
            residentAddressField3 = $('.permanent_AddressField3').val();
            pin = $("#PpinCodeICICI").val();
            city = $("#PCity").val();
            state = $("#PState").val();
            $('#permanentAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

             if ($('#permanentAddress').val() == "" && residentAddressField2.trim().length == 0)
                {    
                $('.permanentAddr-error-div').html('');  // CheckAddress('PRES');
                $('.permanentAddr-error-div').addClass('hidden');
                   
                }
            var lPResAddrss = $('#permanentAddress').val()
            var bPRValue = /^[A-Za-z0-9#',\/\s]*$/.test(lPResAddrss)
            
                    
            if (bPRValue)
            {
               
                $('.permanentAddr-error-div').html('');  // CheckAddress('PRES');
                $('.permanentAddr-error-div').addClass('hidden');
                
            }

        }
        if (addrDetails == 'COMP')
        {

            residentAddressField1 = $('.companyAddressField1').val();
            residentAddressField2 = $('.companyAddressField2').val();
            residentAddressField3 = $('.companyAddressField3').val();
            pin = $("#OPincodeICICI").val();
            city = $("#OCity").val();
            state = $("#OState").val();
            $('#companyAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);


            var lComAddrss = $('#companyAddress').val()
            var bCValue = /^[A-Za-z0-9#',\/\s]*$/.test(lComAddrss)
           
            if (bCValue)
            {
                $('.companyAddr_div-error-div').html('');  // CheckAddress('COMP');
                $('.companyAddr_div-error-div').addClass('hidden');
            }
            
        }

    }
function checkpermanent(){
         var residentAddressField1 = $('.permanent_AddressField1').val();
         var  residentAddressField2 = $('.permanent_AddressField2').val();
          var  residentAddressField3 = $('.permanent_AddressField3').val();
            pin = $("#PpinCodeICICI").val();
            city = $("#PCity").val();
            state = $("#PState").val();
             var radioValue = $(".addressProof input[type='radio']:checked").val();
             var lPResAddrss = $('#permanentAddress').val()
        if (radioValue === 'No')
                {
                
              
                if(residentAddressField1!=""){
                      var a= residentAddressField1.length;
                    if(a<4 || a>40){
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Address cannot be greater than 40 and less than 3 characters in each address line");

                    }
                }
                if(residentAddressField2!=""){
                       var b =residentAddressField2.length;
                    if(b<4 || b>40){
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Address cannot be greater than 40 and less than 3 characters in each address line");

                    }
                }
                if(residentAddressField3!=""){
                      var c =residentAddressField3.length;  
                    if(c<4 || c>40){
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Address cannot be greater than 40 and less than 3 characters in each address line");

                    }
                }
                }    
    }
           //Adobe code starts here
           $( document ).ready(function() {
                

         $("input").focusout(function() {
         errorDetails="";
         $('.error').each(function(){
         if(!$(this).hasClass('hidden'))
         {
            if($(this).text().length>0)
         {
         if(errorDetails.length>0)
         {
            errorDetails=errorDetails+", "+ $(this).text();
         }
         else
         {
            errorDetails= $(this).text();
         }
        }
       }
      })            
      
        // Adobe Code validation error function
           if(errorDetails.length>0) 
           $.fn.ErrorTracking(errorDetails," ");
//           console.log('For Input'+errorDetails)
           });

          $("select").focusout(function() {
          errorDetails="";
          $('.error').each(function(){
          if(!$(this).hasClass('hidden'))
          {
              if($(this).text().length>0)
          {
          if(errorDetails.length>0)
          {
             errorDetails=errorDetails+", "+ $(this).text();
          }
          else
          {
              errorDetails= $(this).text();
          }
        }
       }
      })

             // Adobe Code validation error function
             if(errorDetails.length>0)
             $.fn.ErrorTracking(errorDetails," " );
//             console.log('For Select'+errorDetails)
         });
     
           
          
           
           
           
           
           var iciciURL = document.URL.substr(document.URL.lastIndexOf('/')+1);
           var iciciDetail = "";
          
           if(iciciURL === "Jet-Airways-ICICI-Bank-Coral-VISA-Credit-Card"){
               iciciDetail = "ICICI-Coral-VISA";
           } else if(iciciURL === "Jet-Airways-ICICI-Bank-Coral-AMEX-Credit-Card"){
               iciciDetail = "ICICI-Coral-AMEX";
           }else if(iciciURL === "Jet-Airways-ICICI-Bank-Rubyx-VISA-Credit-Card"){
               iciciDetail = "ICICI-Rubyx-VISA";
           }else if(iciciURL === "Jet-Airways-ICICI-Bank-Rubyx-AMEX-Credit-Card"){
               iciciDetail = "ICICI-Rubyx-AMEX";
           }else if(iciciURL === "Jet-Airways-ICICI-Bank-Sapphiro-VISA-Credit-Card"){
               iciciDetail = "ICICI-Sapphiro-VISA";
           }else if(iciciURL === "Jet-Airways-ICICI-Bank-Sapphiro-AMEX-Credit-Card"){
               iciciDetail = "ICICI-Sapphiro-AMEX";
           }
           
           
           //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----applyICICIWebVIEWForm--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----applyICICIWebVIEWForm--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----applyICICIWebVIEWForm--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----applyICICIWebVIEWForm--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----applyICICIWebVIEWForm--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----applyICICIWebVIEWForm--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----applyICICIWebVIEWForm--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----applyICICIWebVIEWForm-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----applyICICIWebVIEWForm--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End   
           
           
           
           
         digitalData.pageInfo={"pageName":""+iciciDetail,"category":"Cards","subCategory1":"App-Form","subCategory2":""+iciciDetail,"partner":"JP","currency":"INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
         digitalData.formInfo={"formName":"ICICI card application form","lastAccessedField":lastAccessedField,"name":"","email":"","mobile":"","currentCity":"","pincode":""}             
               
        //code for form abandonment
           var checkCloseX = 0;
                                      //Form abandon
                                     // var IsButtonClicked=false;
                                      $(document).mousemove(function (e) {
                                      if (e.pageY <= 5) {
                                      checkCloseX = 1; 
                                      }
                                      else { checkCloseX = 0; }
                                      });
                                      
                                      window.onbeforeunload = function (event) {
                                     //if (event) {
                                     if (checkCloseX == 1) {
                                     //if(getCookieValue('JetPrivilege_CCounter_C', 'userAcceptedCookie')=="")
                                     //{
                                      //digitalData.userInfo.cookieAccept='N'
                                     //}
                                                                        }
                                   //}
                                    if(!IsButtonClicked)
                                     {
                                       
                                       var bankName="ICICI";
                                       $.fn.FormAbandonICICIClick(lastAccessedField,digitalData.formInfo.name,digitalData.formInfo.email,digitalData.formInfo.mobile,digitalData.formInfo.currentCity,digitalData.formInfo.pincode,bankName)
                                       
//                                        console.log('PAGE ABANDON CALLED');
                                      }   

                                  };   
                                     //Form Abandonment close
          }); 
         // Code For PageAbandon
                    $('form input, form select').click(function(){
                         var fname = $("#fname").val();
			 var mname = $("#mname").val();
			 var lname = $("#lname").val();
                         var fullName= fname+" "+mname+" "+lname;
			 var mobile = $("#mobile").val();
			 var email = $("#email").val();
                         var currentCity =$("#city").val();
                         var pincode= $("#pinCodeICICI").val();
                         
                         var jsonNewData = {"name":fullName,"email":email,"mobile":mobile,"currentCity":currentCity,"pincode":pincode}
                         var encryptDetail = encryptedData(jsonNewData);
                         var encryptDetailJson = JSON.parse(encryptDetail);
                         var encryptName = encryptDetailJson.name;
                         var encryptEmail = encryptDetailJson.email;
                         var encryptMobile = encryptDetailJson.mobile;
                         var encryptCity = encryptDetailJson.currentCity;
                         var encryptPincode = encryptDetailJson.pincode;
                digitalData.formInfo={"formName":"ICICI card application form","lastAccessedField":lastAccessedField,"name":encryptName,"email":encryptEmail,"mobile":encryptMobile,"currentCity":encryptCity,"pincode":encryptPincode}             
              });
  
             //Adobe code ends here         

</script>
                                 <!--Form Abandonment-->
                                  <script type='text/javascript'>
                                   //Values to set for form abandonment  
                                   selectedCity=$('#city').val(); 
                                   
                                   $('form input, form select').click(function(){
                                      lastAccessedField=$(this).attr('name');
                                    });
                                    
                                    
                                    function encryptedData(jsonToEncrypt){
                                     var encryptedJson;
                                     $.ajax(
                                        { method : 'GET',
                                          async: false,
                                          url:ctx+"encryptedAdobe?jsonData="+JSON.stringify(jsonToEncrypt),
                                          success: function (result) {
                                          encryptedJson = JSON.stringify(result);

                                           }});

                                           return encryptedJson;

                                      } 
      
                                   </script> 