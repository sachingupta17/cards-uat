<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>   
<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<!--New header footer implementation start-->
<div id="footer" class="jp-react-cmp">


</div>
<!--New header footer implementation end-->

<!--<footer class="jp-footer-clearfix clearfix" id="jpplfooter" aria-label="footer content information" role="content information">
        <div class="footerarrow isDown" id="downarrow">
            <p class="hideElement pTagRemoval"></p>
        </div>
    <div class="grid-Container grid-Container960">
        <div class="row-Container jp-mainFooter">
            <div class="column-sm-12 column-md-3 column-wd-3 jp-footerColumn1 jp-padL10">
                <ul class="jp-footerNavigation iphoneFooter1">
                    <li>
                        <a href="http://www.jetprivilege.com/about-us" onclick="footerClick('About Us')">About Us</a>
                    </li>
                    <li>
                        <a href="https://www.jetprivilege.com/corporate-social-responsibility" onclick="footerClick('Social Impact')">Social Impact</a>
                    </li>
                    <li>
                        <a href="http://www.jetprivilege.com/contact-us" onclick="footerClick('Contact Us')">Contact Us</a>
                    </li>
                    <li>
                        <a href="http://www.jetprivilege.com/faq" onclick="footerClick('FAQs & Glossary')">FAQs &amp; Glossary</a>
                    </li>
                    <li>
                        <a href="http://www.jetprivilege.com/pressroom" onclick="footerClick('Pressroom')">Pressroom</a>
                    </li>
                </ul>
                <ul class="jp-footerNavigation iphoneFooter2">
                    <li>
                        <a href="http://www.jetprivilege.com/careers" onclick="footerClick('Careers')">Careers</a>
                    </li>
                    <li>
                        <a href="http://www.jetprivilege.com/terms-and-conditions" onclick="footerClick('Terms & Conditions')">Terms &amp; Conditions</a>
                    </li>
                    <li>
                        <a  href="http://www.jetprivilege.com/privacy-security-policy" onclick="footerClick('Privacy Policy')">Privacy Policy</a>
                    </li>
                    <li>
                        <a href="http://www.jetprivilege.com/disclaimer-policy" onclick="footerClick('Disclaimers & Policies')">Disclaimers &amp; Policies</a>
                    </li>
                    <li>
                        <a href="http://www.jetprivilege.com/sitemap" onclick="footerClick('Site Map')">Site Map</a>
                    </li>
                </ul>
            </div>
            <div class="column-sm-12 column-md-3 column-wd-3 jp-footerColumn2">
                <ul class="jp-footerNavigation ">
                    <li>
                        <h2 aria-hidden="true" class="hideElement">Hidden Heading</h2>
                        <h3 class="jp-mediumFont">
                            <span>India</span>Metro Cities</h3>
                        <div style="margin-bottom: 7px; margin-top: 3px; line-height:20px">for Bengaluru, Chennai, Delhi, Hyderabad, Kolkata &amp; Mumbai dial 3989 3333</div>
                    </li>
                    <li>
                        <h2 aria-hidden="true" class="hideElement">Hidden Heading</h2>
                        <h3 class="jp-mediumFont txtmarginT20">Other Cities</h3>
                        <div style="margin-bottom: 7px; margin-top: 3px; line-height:20px">Prefix the city code of nearest metro city and dial 3989 3333</div>
                    </li>
                </ul>
            </div>
            <div class="column-sm-12 column-md-3 column-wd-3 jp-footerColumn3">
                <ul class="jp-footerNavigation">
                    <li>
                        <h3 class="jp-mediumFont">United Kingdom</h3>0808-101-1199*</li>
                    <li>
                        <h3 class="jp-mediumFont txtmarginT29">USA and Canada</h3>1-877-U-FLY-JET* (1 877 835 9538)<div style="margin-bottom: 10px; margin-top: 3px"></div>
                    </li>
                    <li class="txtmarginT25">*Toll Free</li>
                </ul>
            </div>
            <div class="column-sm-12 column-md-3 column-wd-3 jp-footerColumn4 jp-padL10">
                <div class="jp-footerNavigation">
                    <div class="jp-footerLeftBlackTxt">Featured Offer<span class="jppl-sprites new-window-icon win-blue-icon extImageIcon exticonMargin"><span class="ui-hidden-accessible">External link icon - Opens in new window. External site which may or may not meet accessibility guidelines</span></span>
                    </div>
                    <a href="https://www.jetprivilege.com/chrome-extension" target="_blank" title="" aria-label="" id="shop.jetprivilege.com">
                        <div class="brickImgCont">
                            <img src="${applicationURL}static/images/footer/banner-footer-Scout.jpg" alt="" data-image-type="Program Initiatives" onclick="footerClick('JetPrivilege Scout')"></div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row-Container">
            <div class="column-sm-0 column-md-8 column-wd-9 jp-padL10 ">
                <div class="jp-footerLeftBlackTxt">
                    <span>Connect with us</span><span class="jppl-sprites new-window-icon win-blue-icon extImageIcon exticonMargin"><span class="ui-hidden-accessible">External link icon - Opens in new window. External site which may or may not meet accessibility guidelines
                        </span></span>
                </div>
                <div class="jp-socialLinksContainer">
                    <ul class="jp-footerNavigation jp-socialLinks">
                        <li>
                            <a class="jppl-sprites sm-icon social1" target="_blank" rel="nofollow"  href="https://www.facebook.com/Dont-Stop-Collecting-160596401062799/?fref=ts" aria-label="Like us on Facebook" title="" onclick="footerClick('Like us on Facebook')"><span class="ui-hidden-accessible" aria-hidden="true">https://www.facebook.com/Dont-Stop-Collecting-160596401062799/?fref=ts</span>
                                <div class="hideElement pTagRemoval"></div>
                            </a>
                        </li>
                        <li>
                            <a class="jppl-sprites sm-icon social2" target="_blank" rel="nofollow" href="https://twitter.com/DSC_JPMiles" aria-label="Follow us on Twitter" title="" onclick="footerClick('Follow us on Twitter')"><span class="ui-hidden-accessible" aria-hidden="true">https://twitter.com/DSC_JPMiles</span>
                                <div class="hideElement pTagRemoval"></div>
                            </a>
                        </li>
                        <li>
                            <a class="jppl-sprites sm-icon social3" target="_blank" rel="nofollow" href="https://www.youtube.com/channel/UCLdU0JHte67B8HSyw2DWvPg" aria-label="Subscribe to us on YouTube" title="" onclick="footerClick('Subscribe to us on YouTube')"><span class="ui-hidden-accessible" aria-hidden="true">https://www.youtube.com/channel/UCLdU0JHte67B8HSyw2DWvPg</span>
                                <div class="hideElement pTagRemoval"></div>
                            </a>
                        </li>
                        <li>
                            <a class="jppl-sprites sm-icon social4" target="_blank" rel="nofollow" href="https://www.linkedin.com/company/jet-privilege-pvt-ltd" aria-label="Join Us on LinkedIn" title="" onclick="footerClick('Join us on LinkedIn')"><span class="ui-hidden-accessible" aria-hidden="true">https://www.linkedin.com/company/jet-privilege-pvt-ltd</span>
                                <div class="hideElement pTagRemoval"></div>
                            </a>
                        </li>
                        <li>
                            <a class="jppl-sprites sm-icon social5" target="_blank" rel="nofollow" href="https://pinterest.com/jetairways" aria-label="Follow Us on Pinterest" title="" onclick="footerClick('Follow us on Pinterest')"><span class="ui-hidden-accessible" aria-hidden="true">https://pinterest.com/jetairways</span>
                                <div class="hideElement pTagRemoval"></div>
                            </a>
                        </li>
                        <li>
                            <a class="jppl-sprites sm-icon social6" target="_blank" rel="nofollow" href="https://www.instagram.com/dontstopcollecting/" aria-label="Follow us on Instagram" title="" onclick="footerClick('Follow us on Instagram')"><span class="ui-hidden-accessible" aria-hidden="true">https://www.instagram.com/dontstopcollecting/</span>
                                <div class="hideElement pTagRemoval"></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="column-sm-12 column-md-4 column-wd-3 jp-iphoneCenter">
                <div class="jp-copy">
                    <ul class="jp-footerlogos">
                        <li>
                            <a target="_blank" href="http://www.jetairways.com" rel="nofollow" class="bottomfooterlogo" aria-label="Jet Airways(external site, opens in a new window)" title="" onclick="footerClick('JET AIRWAYS')">

                            </a>
                        </li>
                                                <li>
                                                    <a target="_blank" href="http://www.etihad.com" rel="nofollow" class="etihadFooterLogo" aria-label="Etihad(external site, opens in a new window)" title=""><span class="ui-hidden-accessible" aria-hidden="true">Etihad(external site, opens in a new window)</span>
                                                        <div class="hideElement pTagRemoval"></div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)" class="jp-jetfooterLogoDivider">
                                                        <p class="hideElement"></p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="http://www.jetairways.com/EN/IN/Home.aspx" target="_blank" rel="nofollow" aria-label="Jet Airways(external site, opens in a new window)" title="" class="jetfooterLogo"><span class="ui-hidden-accessible" aria-hidden="true">Jet Airways(external site, opens in a new window)</span>
                                                        <div class="hideElement pTagRemoval"></div>
                                                    </a>
                                                </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row-Container">
            <div class="column-sm-12 column-md-12 column-wd-12 jp-padL10 jp-marL10">
                <span class="jp-jppl-sprites jp-new-window-icon jp-win-blue-icon jp-win-black-icon-pos"></span><span class="jp-infoFooterTxt">Indicates that you will be redirected to an external site which may or may not meet accessibility guidelines.</span>
                <div class="jp-copyright-block">
                    <p>� Jet Privilege Pvt. Ltd. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>-->
<!--<script type="text/javascript">
    var user = "$ {sessionScope.loggedInUser}";
</script>-->

<script type="text/javascript">

    $(document).ready(function () {
       console.log("@@@@@@@@@@@@@@@@@ initDigitalData is executed$$$$$$$$$$$$$$$$$$$")
        $.getScript('${applicationURL}initDigitalData').done(function (script, status) {
                    console.log("@@@@@@@@@@@@@@@@@ initDigitalData is executed...")
                    console.log("DigitalData UserInfo Prior Calling Header Footer");

                    $.getScript('${applicationURL}reactHeaderFooter')
                            .done(function (script, status) {
                                console.log("@@@@@@@@@@@@@@@@@ reactHeaderFooter is executed...");
                                                      
                                $.getScript('${applicationURL}reactFingerprintImpl')
                                        .done(function (script, status) {
                                            console.log("@@@@@@@@@@@@@@@@@ reactFingerprintImpl is executed else condition...");
                                     digitalData.userInfo.pid = getCookieValue('JetPrivilege_CCounter_C', 'PID')
                                     console.log("******************* Is Logged In ***********" + getCookieValue('JetPrivilege_CCounter_C', 'perm'));

                                  })
                            });
                                    
                                    
                       //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----cbcPortalFooter--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----cbcPortalFooter--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----cbcPortalFooter--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----cbcPortalFooter--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----cbcPortalFooter--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----cbcPortalFooter--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----cbcPortalFooter--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----cbcPortalFooter-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----dcbcPortalFooter--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End   
                     digitalData.pageInfo.pageSource = pageSource;
                     digitalData.pageInfo.platform = plateform;
                     digitalData.pageInfo.isWebView = isWebView;
                     // End             
                                    
                                    
                                    
                                    
                                    
                                    
                    if (getCookieValue('JetPrivilege_CCounter_C', 'userAcceptedCookie') == "true") {

                        digitalData.userInfo.cookieAccept = 'Y'
                        digitalData.userInfo.cookieDateTime = getCookieValue('JetPrivilege_CCounter_C', 'ad')
                        digitalData.userInfo.cookieAcceptedProperty = getCookieValue('JetPrivilege_CCounter_C', 'as');
                        console.log('############## Cookie Accepted ############### cookieDateTime: ' + getCookieValue('JetPrivilege_CCounter_C', 'ad') + '###############cookieAcceptedProperty; ' + getCookieValue('JetPrivilege_CCounter_C', 'as'))
                    } else
                    {
                        digitalData.userInfo.cookieAccept = ''
                    }
                   

                    // Assiging PageInfo
                     pageInfo();
                    _satellite.pageBottom();


                });



        //Adobe Implementation end   

        // Click on Cookie Band
        if ($('.base_notification_banner').is(':visible') == 1) {
            $('.text-style-1').each(function () {

                if ($(this).attr('href').indexOf('privacy-security-policy') >= 0) {

                    $(this).attr("onclick", "AdobeGDPRCookieClick('PRIVACY POLICY')")
                } else {

                    $(this).attr("onclick", " AdobeGDPRCookieClick('COOKIE POLICY')")
                }

            });

            $('#accept').unbind('click');

            $('#accept').bind('click', function (e) {

                AdobeGDPRCookieClick('I AGREE');

            });

            $('#closeCookie').unbind('click');

            $('#closeCookie').bind('click', function () {

                AdobeGDPRCookieClick('CLOSE');

            });
        }
    });

    function AdobeGDPRCookieClick(linkName)

    {

        $.fn.AdobeGDPRCookieClick(linkName);

    }


</script>            
<!--Adobe code ends here-->