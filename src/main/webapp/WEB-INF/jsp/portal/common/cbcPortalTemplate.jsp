<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<%@ page session="true"%>--%>

<!DOCTYPE HTML>
<html>
    <head>
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-type" content="text/html" charset="UTF-8" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
 <meta name="description" content="InterMiles Co-Brand Cards – Earn InterMiles every time you spend using your co-brand cards. Redeem these InterMiles for Free Flights, Free Hotel stays, shopping, dining & more.">
 <meta name="keywords" content="co-brand cards, intermiles co-brand cards, jetprivilege co-brand cards, jet airways co-brand cards, earn intermiles, earn jpmiles">
 <meta name="com.silverpop.brandeddomains" content="www.pages06.net,marketing.jetprivilege.com" />
 <meta name="com.silverpop.cothost" content="pod6.ibmmarketingcloud.com" />
 <meta name="msvalidate.01" content="61C81E4F37A500BAF6EF151BAD21A30B" />
 
 <script src="<spring:message code="application.adobe.url"/>" ></script>

 
 <title>InterMiles Co-Brand Cards: Earn InterMiles On Every Spend | InterMiles</title>
 <!--<title>this is cards</title>-->
<link href="${applicationURL}static/images/favicon.ico.jpg" alt="InterMiles Favicon" rel="shortcut icon" type="image/ico">
 	<script>
 		var ctx = "${applicationURL}";
                var GDPRCheck = '${sessionScope.GDPRCheck}'; 
                console.log("GDPRCheck : "+GDPRCheck);
                 
	</script>
<link rel='stylesheet' href='${applicationURL}static/css/integrated.css' />
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

<!--Header-footer impl file-->
<link type="text/css" href=<spring:message code="application.react.css.url"/> rel="stylesheet">
<script src=<spring:message code="application.react.auth.url"/>></script>
<!--end-->

<script src="${applicationURL}static/js/integrated.js"></script> 

<!--Adobe code starts here-->
 <script type="text/javascript" src="${applicationURL}static/js/Adobe/AdobeFramework.js?version=1.0.4"></script>
<%--  <script type="text/javascript" src="${applicationURL}static/js/CleverTap/CleverTapFramework.js"></script> --%>
 <script type="text/javascript" src="${applicationURL}static/js/CleverTap/CleverTapFramework.js?cver=1.0.2"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/hmac-sha256.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/components/enc-base64-min.js"></script>

<!--Adobe code starts here-->
<!--<script src="${applicationURL}initDigitalData"></script>-->
<script src="${applicationURL}jsFileData"></script>

<script type='text/javascript' >
      var IsButtonClicked=false;
      sessionStorage.setItem("PreviousURL", location.href)
      
  </script>

<!--(commented for new gdpr impl (26th Feb))-->
<%--<c:if test = "${sessionScope.GDPRCheck == true }">--%>

  <script>
dataLayer = [{ 'V_Type': '${sessionScope.gtmloggedInUser}' }];
</script>

<script>
dataLayer = [{ 'V_Type': '${sessionScope.gtmloggedInUser}' }];
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MN7RT6D');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TTLP892');</script>
<!-- End Google Tag Manager -->


             
           
 <script>
	    jppl.init();
            jppl.mobileMenu();
            jppl.masterPageProcess();
</script>


<style>
.panelone>div {
	padding-right: 118px;
}

.panelone>div {
	width: auto;
}

footer h3 {
	font-weight: 700;
}

footer h3.mediumFont {
	font-weight: 400;
}
</style>
    </head>
<body>

       
         <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MN7RT6D"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TTLP892"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
         
	<div id="wrapper">
	<div class="co-brand">
		<!--<header id="includedHeader">-->
			<tiles:insertAttribute name="header" />
			<meta name=?robots? content=?noindex,nofollow?>
		<!--</header>-->
                
                <!--new header-footer implementation start-->
                <div id="root" class="jp-react-cmp"></div>
                <!--new header footer implementation end-->
		
                <tiles:insertAttribute name="body" />  
                
		<footer class="clearfix">
			<tiles:insertAttribute name="footer" />
		</footer>
		 <div class="back_to_top">
               <img src="${applicationURL}static/images/footer/arrow.png" title="Scroll To Top" alt="Scroll To Top"/>
        </div>
	</div>
	</div>
	<script>
	jpplcbc.init();
	</script>

</body>
</html>

