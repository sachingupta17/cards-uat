<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%--<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>--%>   

<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<!--New header footer implementation start-->
<div id="header" class="jp-react-cmp">
    <script src=<spring:message code="application.react.headerUtil.url"/>></script> 

   
   <div id="jpseomenuh1" style="display: none;">
    <a href="https://www.intermiles.com">Flights</a>
    <a href="https://hotels.intermiles.com">Hotels</a>
    <a href="https://cards.intermiles.com/">Cards</a>
    <a href="https://shop.intermiles.com">Shop</a>
    <a href="https://delights.intermiles.com">Dine</a>

    <a href="https://www.intermiles.com/rewards-program/enrol">Enrol now</a>

    <!-- MORE -->
    <a href="https://www.intermiles.com/earn-intermiles/programme-partners/convert">Convert</a>
    <a href="https://shop.intermiles.com/?section_id=talk_partners">Talk</a>
    <a href="https://delights.intermiles.com/offers/category/Tours%20and%20Activities">Live it up</a>
    <a href="https://shop.intermiles.com/?section_id=read_partners">Read</a>
    <a href="https://www.intermiles.com/earn-intermiles/programme-partners/rent-a-car">Rent-a-car</a>
    <a href="https://www.intermiles.com/earn-intermiles/programme-partners/fuel/iocl">Fuel</a>
    <a href="https://www.intermiles.com/earn-intermiles/claim-missing-miles-about">Claim Past InterMiles</a>
    <a href="https://rewardstore.intermiles.com/home">Reward Store</a>

    <!-- HAMBURGER : NOT LOGGED IN -->
    <a href="https://www.intermiles.com/rewards-program?q=op">About the Programme</a>
    <a href="https://www.intermiles.com/earn-intermiles/programme-partners">Earn with Partners</a>
    <a href="https://www.intermiles.com/rewards-program/my-family-plus-about">MyFamily+</a>
    <a href="https://www.intermiles.com/rewards-program/enrol-your-children">Enrol your Children</a>
    <a href="https://www.intermiles.com/rewards-program/business-about">Business Rewards+</a>
    <a href="https://www.intermiles.com/rewards-program/merit-rewards">Merit Rewards</a>
    <a href="https://www.intermiles.com/earn-intermiles/buy-about">Buy InterMiles</a>
    <a href="https://insure.intermiles.com/?q=mmjpexclusives30112016">InterMiles Exclusives</a>

    <a href="https://www.intermiles.com/rewards-program/tiers-recognition">Tiers & Recognition</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/benefits-privileges">Benefits & Privileges</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/platinum">Platinum</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/gold">Gold</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/silver">Silver</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/red">Red</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/dynamic-tier-review-about">Dynamic Tier Review</a>

    <a href="https://www.intermiles.com/use-intermiles">Use InterMiles</a>
    <%--<a href="https://www.intermiles.com/use-intermiles/redeem-about">Standard Flights</a>--%>
    <a href="https://flights.intermiles.com">Select Flights</a>
    <a href="https://www.intermiles.com/use-intermiles/partner-award-flights-form">Partner Award Flights</a>
    <a href="https://hotels.intermiles.com">Hotels</a>
    <a href="https://rewardstore.intermiles.com/home">Reward Store</a>
    <a href="https://www.intermiles.com/use-intermiles/gift-transfer-jpmiles-about">Gift or Transfer InterMiles</a>
    <a href="https://www.intermiles.com/use-intermiles/cabin-upgrade">Cabin Upgrade</a>

    <a href="https://www.intermiles.com/about-us">About the company</a>
    <a href="https://www.intermiles.com/about-us">About Us</a>
    <a href="https://www.intermiles.com/pressroom">Pressroom</a>
    <a href="https://www.intermiles.com/corporate-social-responsibility">Social Impact</a>
    <a href="https://www.intermiles.com/faq">FAQs and Glossary</a>
    <a href="https://www.intermiles.com/offers-deals">Offers & Deals</a>
    <a href="https://www.intermiles.com/rewards-program/awards-accolades">Awards and Accolades</a>
    <a href="https://www.intermiles.com/contact-us">Contact Us</a>

    <!--SOCIALS-->
    <a href="https://www.intermiles.com"> </a>
    <a href="https://www.facebook.com/JetPrivilege/">Facebook</a>
    <a href="https://twitter.com/Intermiles">Twitter</a>
    <a href="https://www.youtube.com/channel/UChX3No6MUpGjVDQXfDce8MQ">Youtube</a>
    <a href="https://www.linkedin.com/company/jet-privilege-pvt-ltd">Linkedin</a>
    <a href="https://pinterest.com/jetairways">Pinterest</a>
    <a href="https://www.instagram.com/inter_miles/">Instagram</a>

    <!-- FOOTER : NOT LOGGED IN -->
    <a href="https://www.intermiles.com/rewards-program/?q=op">Our Program</a>
    <a href="https://www.intermiles.com/rewards-program/my-family-plus-about">MyFamily+</a>
    <a href="https://www.intermiles.com/rewards-program/enrol-your-children">Enrol your Children</a>
    <a href="https://www.intermiles.com/rewards-program/business-about">Business Rewards+</a>
    <a href="https://www.intermiles.com/rewards-program/merit-rewards">Merit Rewards</a>
    <a href="https://www.intermiles.com/earn-intermiles/buy-about">Buy InterMiles</a>
    <a href="https://insure.intermiles.com/?q=mmjpexclusives30112016">InterMiles Exclusives</a>

    <a href="https://www.intermiles.com/use-intermiles">Use InterMiles</a>
    <a href="https://www.intermiles.com/use-intermiles/redeem-about">Standard Flights</a>
    <a href="https://flights.intermiles.com">Select Flights</a>
    <a href="https://www.intermiles.com/use-intermiles/partner-award-flights-form">Partner Award Flights</a>
    <a href="https://hotels.intermiles.com">Hotels</a>
    <a href="https://rewardstore.intermiles.com/home">Reward Store</a>
    <a href="https://www.intermiles.com/use-intermiles/gift-transfer-jpmiles-about">Gift or Transfer InterMiles</a>
    <a href="https://www.intermiles.com/use-intermiles/cabin-upgrade">Cabin Upgrade</a>

    <a href="https://www.intermiles.com/rewards-program/tiers-recognition">Tier�  & Recognition</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/benefits-privileges">Benefits & Privileges</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/platinum">Platinum</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/gold">Gold</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/silver">Silver</a>
    <a href="https://www.intermiles.com/rewards-program/tiers-recognition/dynamic-tier-review-about">Dynamic Tier Review</a>

    <a href="https://www.intermiles.com/earn-intermiles/programme-partners">Earn with Partners</a>
    <a href="https://www.intermiles.com/earn-intermiles/programme-partners/airlines">Fly</a>
    <a href="https://cards.intermiles.com/">Cards</a>
    <a href="https://www.intermiles.com/earn-intermiles/programme-partners/convert">Convert</a>

    <a href="https://www.intermiles.com/use-intermiles/gift-transfer-intermiles">Gift/Transfer</a>
    <a href="https://hotels.intermiles.com">Hotels</a>
    <a href="https://shop.intermiles.com">Shop</a>
    <a href="https://delights.intermiles.com">Dine</a>

    <a href="https://shop.intermiles.com/?section_id=talk_partners">Talk</a>
    <a href="https://www.intermiles.com/earn-intermiles/programme-partners/live-it-up">Live it Up</a>
    <a href="https://shop.intermiles.com/?section_id=read_partners">Read</a>
    <a href="https://www.intermiles.com/earn-intermiles/programme-partners/rent-a-car">Rent-a-car</a>
    <a href="https://www.intermiles.com/earn-intermiles/programme-partners/fuel/iocl">Fuel</a>

    <a href="https://www.intermiles.com/faq">FAQs and Glossary</a>
    <a href="https://www.intermiles.com/corporate-social-responsibility">Social Impact</a>
    <a href="https://www.intermiles.com/contact-us">Contact Us</a>
    <a href="https://www.intermiles.com/pressroom">Pressroom</a>
    <a href="https://www.intermiles.com/careers">Careers</a>
    <a href="https://www.intermiles.com/sitemap">Site Map</a>

    <a href="https://www.intermiles.com/terms-and-conditions/intermiles-programme-terms-and-conditions">Terms & Conditions</a>
    <a href="https://www.intermiles.com/privacy-security-policy">Privacy Policy</a>
    <a href="https://www.intermiles.com/terms-and-conditions/disclaimer">Disclaimers & Policies</a>
</div>


   
    <!--SEO URLs for PRE-PROD end--> 
</div>
<!--New header footer implementation end-->


<!--<div class="site-header">
    <div class="container clearfix">        
        <div class="logoSection clearfix">
            <div class="left">
                <a class="MegamenuOpenlogo" data-attr="first" href="javascript:;"></a>
                <a class="logo" data-attr ='first' href="http://www.jetprivilege.com" onclick="adobeHeaderClick('JetPrivilege')"></a>
            </div>  left 
            <div class="right">
                <div class="headerPanelSearch"></div>
<%--<c:choose>--%>
<%--<c:when test="${empty sessionScope.loggedInUser}">--%>
<div class="login" id="nonlogin" style="display: none">
    <div class="clearfix">
        <ul class="inline" id="nonlogin">
            <li id="test">
                <a class="headerEnrollLink" href="http://www.jetprivilege.com/rewards-program/enrol" onclick="adobeHeaderClick('Enrol')">Enrol</a>
            <li>
                <a class="headerEnrollLinkIcon" href="http://www.jetprivilege.com/rewards-program/enrol"></a>
                <a class="before_login" href="javascript:void(0);" onclick="signin();"></a>
            </li>
            <li class="login_btn">
                <a href="javascript:void(0);" onclick="signin();">Login</a>
            </li> 

        </ul>
    </div>  clearfix 
</div>  login 
<%--</c:when>--%>

<%--<c:otherwise>--%>
<div class="logout"  id="loginuser" style="display: none" >
    <div class="clearfix">

        <ul   class="inline" id="test">
            <li class="after_login" onclick="signOut()"></li>
            <li>Hello <span id="name">
<%--<c:out value="${sessionScope.loggedInUserName}"></c:out>--%>
</span></li>
            <li>JP No: <span id="jpnumber1">
<%--<fmt:parseNumber integerOnly="true" type="number" value="${sessionScope.loggedInUser}" />--%>
</span></li>

            <li class="logspace"><div class="arrow-brdr"></div></li>
            <li class="arrow_down"></li>
        </ul>
    </div>  clearfix 
    <div class="logout_btn hide">
        <a href="javascript:void(0);" onclick="signOut()">Logout</a>
    </div>
</div>  logout 
<%--</c:otherwise>--%>
<%--</c:choose>--%>
</div>  right    
</div>   

<nav class="topNavigation clearfix" data-role="navbar">
<span class="mobMenu"></span>
<ul>
<li data-target="navabout">
    <a href="https://www.jetprivilege.com/rewards-program" class="About" onclick="adobeMegaMenuClick('Our Programme')">
        <span></span>
        Our Programme   
    </a>                    
</li>
<li data-target="navearnjpmiles">
    <a href="https://www.jetprivilege.com/earn-jpmiles" class="EMiles" onclick="adobeMegaMenuClick('Earn JPMiles')">
        <span></span>
        Earn JPMiles
    </a>                    
</li>
<li data-target="navusemiles">
    <a href="https://www.jetprivilege.com/use-jpmiles" class="UMiles" onclick="adobeMegaMenuClick('Use JPMiles')">
        <span></span>
        Use JPMiles 
    </a>                   
</li>
<li data-target="navoffer">                    
    <a href="https://www.jetprivilege.com/offers-deals" class="Offer" onclick="adobeMegaMenuClick('Offers & Deals')">
        <span></span>
        Offers &amp; Deals
    </a>                   
</li>
<li data-target="navbooknow">
    <a href="https://www.jetprivilege.com/book-now" class="PNetwork" onclick="adobeMegaMenuClick('Book Now')">
        <span></span>
        Book Now
    </a>           
</li>
</ul>
</nav>
</div>
</div>-->
<script type="text/javascript">

    jppl.init();
    jppl.mobileMenu();
    jppl.masterPageProcess();
    var redirectUrl = '<c:out value="${sessionScope.redirectUrl}"/>';

    function signin() {
        var external_auth = "<spring:message code="application.cbc.auth0.external_auth.url" />?ExternalReturnUrl=${applicationURL}" + "auth";
                auth_window = window
                        .open(external_auth,
                                "ExternalAuthentication",
                                "top=200,left=200,width=600,height=400,location=yes,status=yes,resizable=yes",
                                true);
                auth_window.focus();

                $.ajax({method: "GET",
                    url: '${applicationURL}loginSessionSet?req=' + redirectUrl + ''});
            }
            function signOut() {

                window.location.href = '${applicationURL}logout';

            }


</script>

<script>
            $(document).ready(function () {

                //**********************************************************************************************************
                var redirectUrl1 = '<c:out value="${sessionScope.redirectUrl}"/>';
                var loginUser = '<c:out value="${sessionScope.loggedInUser}"/>';
                
                
              
//    
                        var allcookies = document.cookie;
                        
                       
               
               // Get all the cookies pairs in an array
               cookiearray = allcookies.split(';');
               
               // Now take key value pair out of this array
               for(var i=0; i<cookiearray.length; i++) {
                  name = cookiearray[i].split('=')[0];
                  value = cookiearray[i].split('=')[1];
                  
                  if(name==="forceFlag"){
                      
                      
                      if(value==="forcelogout"){
                  console.log("forceFlag   logout");
                  window.auth0LogoutMS1();
                    }

                      
                  console.log("Key is : " + name + " and Value is : " + value);      
                  }
                
               }
//              
              
              
                
//                alert(loginUser);

        console.log("redirect url 1--->"+redirectUrl1);
                $.ajax({method: "GET",
                    url: '${applicationURL}currentSession?request=' +redirectUrl1+ ''
//                    alert('test'+redirectUrl1);

                });
               

           $.ajax({method: "GET",
            url: '${applicationURL}getmethod', success: function (data) {
                        console.log("===============" + data);
                var myObj = JSON.parse(data);
                var jpnumber = myObj.Jpnumber;
                var name = myObj.Name;
                console.log("===========jpnumber====" + jpnumber);
                console.log("===========loginUser====" + loginUser);
              
                            
                
                if (jpnumber !== "null" && jpnumber !== "" && (loginUser === "null" || loginUser === "")) {
                   
                    location.reload();
                   
//                    jpnumber = jpnumber.substring(2);
                console.log("===========loginUser====" + loginUser);  
                 console.log("#####" + localStorage.getItem('isLoaded'));  
//                 if (localStorage.getItem('isLoaded') === 'yes') {
//                     
//    localStorage.setItem('isLoaded', 'No');
//   console.log("##### inside loop" + localStorage.getItem('isLoaded'));
//      
//    location.reload();
//  }
                
                
                
//                    location.reload();
                }

            },
            error: function (e) {

                console.log("Error " + e);
            }
        });
            });
</script>


<script  src="<spring:message code="auth0.redirection.url" />client_id=<spring:message code="application.cbc.auth0.clientID" />&redirect_uri=<spring:message code="auth0.logincallback.url"/>%3Fscript%3D1&state=nonce=&scope=openid profile email&prompt=none"></script>
<%-- <script  src="<spring:message code="auth0.redirection.url" />client_id=<spring:message code="application.cbc.auth0.clientID" />&redirect_uri=<spring:message code="auth0.logincallback.url"/>%3Fscript%3D1&state=STATE&nonce=NONCE&scope=openid profile email&prompt=none"></script> --%>

<script>
    function adobeMegaMenuClick(linkName)
    {
        var milesOwned = digitalData.userInfo.pointsBalance;
        $.fn.MegaMenuClick(linkName, milesOwned);
    }

    function adobeHeaderClick(linkName)
    {
        var milesOwned = digitalData.userInfo.pointsBalance;
        $.fn.HeaderAndFooterClick(linkName, milesOwned);
    }

    $(".before_login").click(function () {
        var milesOwned = digitalData.userInfo.pointsBalance;
        $.fn.HeaderAndFooterClick("SignIn", milesOwned);
    });

    $(".after_login").click(function () {
        var milesOwned = digitalData.userInfo.pointsBalance;
        $.fn.HeaderAndFooterClick("Signout", milesOwned);
    });


    $(".login_btn").click(function () {
        var milesOwned = digitalData.userInfo.pointsBalance;
        $.fn.HeaderAndFooterClick("Login", milesOwned);
    });

    $(".logout_btn").click(function () {
        var milesOwned = digitalData.userInfo.pointsBalance;
        $.fn.HeaderAndFooterClick("Logout", milesOwned);
    });

</script>