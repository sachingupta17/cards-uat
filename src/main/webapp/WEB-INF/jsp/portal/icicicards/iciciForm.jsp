<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<spring:htmlEscape defaultHtmlEscape="true" />


<form:form autocomplete="off" id="iciciBean" commandName="iciciBean">

    <form:hidden path="cpNo"/>
    <form:hidden path="offerId"/>
    <form:hidden path="offerDesc"/>

    <form:hidden path="hstate"/>
    <form:hidden path="hcity"/>
    <form:hidden path="hstate1"/>
    <form:hidden path="hcity1"/>
    <form:hidden path="hstate2"/>
    <form:hidden path="hcity2"/>
    <%--<form:hidden path="otpVal" id="otpVal"/>--%>
    <input type="hidden" value="${cardBean.gmNo}" id="gmNo"/>
    <section class="container bottom_padding">
        <div class="cont_area">  <h1>
                ${cardBean.cardName}
            </h1></div>
        <div class="features margT2">
            <div class="features_title benefits_web">
                <div class="row">

                    <div class="col-sm-2">
                        <p>Cards</p>
                    </div>
                    <c:forEach items="${groupHeadings}" var="heading">
                        <div class="col-sm-2">
                            <p>${heading}</p>
                        </div>
                    </c:forEach>
                    <div class="col-sm-2 ">
                        <div class="">
                            <%--                                     <img src="${applicationURL}static/images/co-brand/filter_dropdown.png" class="margL10 filter_icon">  --%>
                            <p class="freeFlight_spends">Free flight basis </p>
                            <div class="freeFlight_spends_calc parent_div hidden">
                                <div class="arrow-up"></div>
                                <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                                <div class="slider_valu margB5 margT5">
                                    <div style="display:inline; text-align:left !important;">INR 5,000</div>
                                    <div style="display:inline; float:right;"> INR 15 Lakhs</div>
                                </div>
                                <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal">
                                <div class="annualSpends margT2 text-center">
                                    <span>Annual Spends: </span><span class="annualSpends_value">60,000</span>
                                </div>
                                <div class="text-center">
                                    <input type="button" class="filter_button button_red" id="doneButton" value="Done">
                                </div>
                            </div>
                            <p>spends
                                <a class="tooltips" href="javascript:void(0);"><img src="${applicationURL}static/images/co-brand/inrimg.png">
                                    <span class="tooltip_content"><span class="pull-right close_toolTip"><img src="${applicationURL}static/images/co-brand/close_btn.png" alt=""></span> 
                                        The free flights calculation basis spends is calculated on the Inter
                                        Miles earned basis the spends & on the minimum award flight redemption of 5000 InterMiles. 
                                        <br><span class="terms_conditions" style="color: black; text-decoration: underline;" id="tcLink">Terms & Conditions Apply</span>
                                    </span> </a>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <!--									<p>Offer Detail</p>-->
                    </div>
                </div>
            </div>
            <div class="features_desc">
                <div class="row">
                    <div class="col-sm-2 pad0">
                        <div class="card_name">
                            <h2>${cardBean.cardName}</h2>
                        </div>
                        <div class="card_img_holder">
                            <span class="card_img"> <img src="${applicationURL}cards/${cardBean.cardImagePath}"/></span>
                            <span class="card_value">
                                <label>Fees: </label>
                                <span>INR ${cardBean.fees}</span>
                            </span>
                            <div class="details_link" >
                                <a href="javascript:void(0);" style="color: #28262c; text-decoration: underline;" data-id="${cardBean.cpNo}" data-name="${cardBean.imageText}" data-utm_source="${amexBean.utmSource}"
                                   data-utm_medium="${amexBean.utmMedium}" data-utm_campaign="${amexBean.utmCampaign}" class="forMoreDetails" onclick="moreDetailsClick('${cardBean.imageText}', '${cardBean.imageText}');$.fn.actionClickCT('More Details','${cardBean.cardName}','${cardBean.bankName}')">For more details ></a>
                            </div>
                        </div>
                    </div>
                    <c:forEach items="${cardBean.featureList}" var="feature">
                        <div class="col-sm-2 webView">
                            <div class="list_cont">
                                <c:if test="${not empty feature}">
                                    <div class="home_feature">
                                        ${feature}
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </c:forEach> 

                    <div class="col-sm-2 ">
                        <div class="free_flights_desc">
                            <p onclick="$.fn.actionClickCT('Calculate your free flights','${cardBean.cardName}','${cardBean.bankName}')">Calculate your free flights</p>
                        </div>
                        <div class="free_flights_div hidden">
                            <div class="free_flights">
                                <p>You get</p>
                                <p class="ff" id="freeFlights${cardBean.cpNo}">XX</p>
                                <p>Free Flights</p>
                                <p class="gg" id="jpMiles${cardBean.cpNo}">on earning<br>XXXX JPamexBeanMiles</p>
                            </div>
                        </div>
                        <div class="lifestyle_benefits" data-toggle="modal" data-target="#benefits" data-cpNo="${cardBean.cpNo}">
                            <div class="benifits-img-holder"> <img src="${applicationURL}static/images/co-brand/benifits_icon.png"></div>
                            <label>Benefits</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-2">
                        <c:choose>
                            <c:when test="${not empty cardBean.offerDesc}">
                                <div class="list_cont">
                                    <div class="home_feature login_offer">
                                        ${cardBean.offerDesc}
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="offer_link" data-toggle="modal" data-target="#offer_jp_popup">
                                    <!--	                                    <a href="javascript:void(0)" class="view_offer_link">View Offer</a>-->
                                </div>
                                <div class="list_cont">
                                    <div class="home_feature offer_display hidden"></div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </div>

        <div class="about">
            <div class="cont_area">
                <h1>Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-Brand Card. Apply here.</h1>
            </div>
            <div class="colorDarkBlue">
                <h2 class="heading_txt">Basic Profile</h2>
                <!--new cr start-->
                <div class="age_error_div hidden" style="color:red"></div>
                <!--new cr end-->
            </div>
            <c:if test="${not empty errorMsg}">
                <div id="errorMsg" style="color: black;" class="error_box">
                    <c:out value="${errorMsg}"></c:out>
                    </div>
            </c:if>
            <div class="margT1">
                <div class="row">
                    <c:choose>
                        <c:when test="${promocodeFlag eq 1}">
                            <div id="namefield" class="col-sm-8">
                                <div class="margT1 name_div" id="div_hide">	
                                    <label class="margT1">Name<span class="mandatory-field">*</span></label>	
                                    <div class="fullName" id="div_hide1">		  					
                                        <form:input class="row margT1 input name_inputBox" id="fullName" path = "fullName"  placeholder="Full Name"/>
                                    </div>
                                    <ul class="name_ul margT1 hidden"  id="div_hide2">   
                                        <li><div class="fName"  id="div_hide3"><span>First Name<span class="mandatory-field">*</span></span>
                                                <form:input	class="firstName" maxlength="27" id="fname" path="fname"  name="FNAME" />
                                            </div>
                                        </li>
                                        <li><div class="mName"><span>Middle Name</span>
                                                <form:input	class="middleName" maxlength="27" id="mname" path="mname" name="MNAME" />
                                            </div>
                                        </li>
                                        <li><div class="lName"><span>Last Name<span class="mandatory-field">*</span></span>
                                                <form:input	class="lastName"  maxlength="27" id="lname" path="lname" name="LNAME"  />
                                            </div>
                                        </li>
                                    </ul> 
                                    <!-- 						  </div> -->
                                </div>
                                <div class="fullName-error-div hidden" style="color:red">
                                </div>
                            </div>
                        </c:when><c:otherwise>
                            <div id="namefield">
                                <div class="margT1 name_div" id="div_hide">	
                                    <label class="margT1">Name<span class="mandatory-field">*</span></label>	
                                    <div class="fullName" id="div_hide1">		  					
                                        <form:input class="row margT1 input name_inputBox" id="fullName" path = "fullName"  placeholder="Full Name"/>
                                    </div>
                                    <ul class="name_ul margT1 hidden"  id="div_hide2">   
                                        <li><div class="fName"  id="div_hide3"><span>First Name<span class="mandatory-field">*</span></span>
                                                <form:input	class="firstName" maxlength="27" id="fname" path="fname"  name="FNAME" />
                                            </div>
                                        </li>
                                        <li><div class="mName"><span>Middle Name</span>
                                                <form:input	class="middleName" maxlength="27" id="mname" path="mname" name="MNAME" />
                                            </div>
                                        </li>
                                        <li><div class="lName"><span>Last Name<span class="mandatory-field">*</span></span>
                                                <form:input	class="lastName"  maxlength="27" id="lname" path="lname" name="LNAME"  />
                                            </div>
                                        </li>
                                    </ul> 
                                    <!-- 						  </div> -->
                                </div>
                                <div class="fullName-error-div hidden" style="color:red">
                                </div>
                            </div>
                        </c:otherwise></c:choose>
                    <c:if test="${promocodeFlag eq 1}">

                        <div class="col-sm-4 margT1" id="promo">
                            <div class="margT1">
                                <label class="margT1">Promo Code</label>
                                <!--<span class="mandatory-field">*</span>-->
                            </div>
                            <div>
                                <form:input	class="row margT1 input" id="promocode" placeholder="Enter Promocode (if any)" path="promocode" maxlength="12"/>
                            </div>   
                            <div class="promocodeError" id="promocodeError" style="color:red">
                            </div>
                            <div class="promocodeaccept" id="promocodeaccept" style="color:#1de31d"></div>
                        </div>  
                    </c:if>
                </div>
            </div>
            <hr>
            <div class="margT1 jpMember hidden">	
                <label class="margT1">Are you an existing InterMiles member? </label>
                <div class="row">
                    <div class="col-sm-6">
                        <form:hidden class="row margT1 input" placeholder="InterMiles No." maxlength="9" path="jetpriviligemembershipNumber" id="jetpriviligemembershipNumber"/>
                        <div id="jetpriviligemembershipNumber_validate"></div>
                    </div>
                    <div class="col-sm-6">
                        <form:hidden path="jetpriviligemembershipTier" placeholder="InterMiles Membership Tier" class="row margT1 input" readonly="true"  id="jetpriviligemembershipTier"/>
                    </div>
                    <div id="error_msg_JP" style="padding: 10px 0; font-size: 12px; color: red;"></div>
                </div>
            </div> 

            <div class="margT1">	
                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">E-mail ID<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" placeholder="E-mail ID" id="email" maxlength="255" path="email" value="${email}"/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">Mobile Number<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                        <input type="hidden" id="mobileNoFrmLogin" >
                            <form:input	class="row margT1 input" placeholder="Mobile Number" maxlength="10" path="mobile" onKeyPress="if(this.value.length==10) return false;" type="number" id="mobile" value="${mobile}"/>
                        </div>
                    </div>
                </div>
            </div><hr/>
            <div class="margT1">	
                <div class="row">
                    <div class="col-sm-6 hasDatePicker">
                        <div>
                            <label class="margT1">Date of Birth<span
                                    class="mandatory-field">*</span></label>
                        </div>
                        <div class="dob">
                            <!--new cr start-->
                            <form:input class="datepicker1 row margT1 input"
                                        placeholder="Select Date of Birth" id="dateOfBirth" path="dateOfBirth"
                                        title="MM-DD-YYYY" readonly="true" />
                            <!--new cr end-->
                        </div>
                        <button class="datepicker_icon" type="button">
                            <img title="Select Date" alt="Select Date"
                                 src="${applicationURL}static/images/header/calender.png">
                        </button>
                        <c:if test="${not empty sessionScope.loggedInUser}">
                            <div id="LoginValidation" style="padding: 10px 0; font-size: 12px; color: red;"></div>
                        </c:if>
                    </div>
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">Gender<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select class="margT1" path="gender" id="gender">
                                <form:option value="">Select Gender</form:option>
                                <c:forEach items="${genderStatus}" var="status">
                                    <form:option value="${status.key}">${status.value}</form:option>
                                </c:forEach> 
                            </form:select>
                        </div>
                    </div>
                </div>
            </div>	<hr/>
            <div class="margT1">	
                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">Education Qualification
                                <!--                                <span class="mandatory-field">*</span>-->
                            </label>
                        </div>
                        <div>
                            <form:select id="eduQualification" path="eduQualification" class="margT1" value="${eduQualification}" >
                                <form:option value="">Select Education Qualification</form:option>
                                <form:options items="${qualification}" /> 
                            </form:select>		
                        </div>								
                    </div>
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1"> Net Annual Income (Excluding Bonus Or Incentives)<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" id="monthlyIncome" placeholder="Net Annual Income (Excluding Bonus Or Incentives)" maxlength="7" path="monthlyIncome" type="number" onKeyPress="if(this.value.length==7) return false;" value="${monthlyIncome}" />
                        </div>
                    </div>
                </div>
            </div>	<hr/>
            <div class="margT1">	
                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">PAN Number<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" id="pancard" placeholder="PAN Number" maxlength="10" path="pancard" value="${pancard}"/>
                        </div>
                    </div>
                    <!--                    <div class="col-sm-6">
                                            <div>
                                                <label class="margT1">Aadhaar Number</label>
                                            </div>
                                            <div>
                    <%--<form:input	class="row margT1 input" id="aadharNumber" maxlength="12" placeholder="Aadhaar ID(For Faster Processing)" path="aadharNumber" value="${aadharNumber}"/>--%>
                </div>
            </div>-->
                </div>
            </div><hr/>
            <div class="margT1 currentResAddress">	
                <label class="margT1">Current Residential Address<span class="mandatory-field">*</span></label>				  					
                <form:input path="resedentialAddress" id="resedentialAddress" class="row margT1 input currentResidentialAddress"  placeholder="Current Residential Address"/>
            </div>
            <div class="residentialAddr_div hidden">
                <div class="residentAddress">
                    <div> Residential Address 1 
                        <span>( H. No., Floor No... etc)</span>
                    </div>
                    <div>						
                        <form:input id="address"	class="residentAddressField1" path="address"  value="${address}" name="address"/>
                    </div>	
                </div>
                <div class="residentAddress">
                    <div class=""> Residential Address 2 
                        <span>( Street, Near by... etc)</span>
                    </div>
                    <div>
                        <form:input	class="residentAddressField2" id="address2" path="address2" value="${address2}" name="address"/>
                    </div>
                </div>
                <div class="residentAddress">
                    <div class=""> Residential Address 3
                        <span>( Street, Near by... etc)</span>
                    </div>
                    <div>
                        <form:input	class="residentAddressField3" id="address3" path="address3"  value="${address3}" name="address"/>
                    </div>
                </div>					
                <div class="residentAddress">
                    <ul class="address">
                        <li>
                            <%--<form:input class="pincode" id="pinCode" maxlength="6" path="pinCode" value="${pinCode}" placeholder="Pincode"/>--%>
                            <form:input class="pincodeICICI" id="pinCodeICICI" maxlength="6" path="pinCode" type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==6) return false;" value="${pinCode}" placeholder="Pincode"/>
                        </li>
                        <li>
                            <form:select class="state" path="state1" id="state1" value="${state1}" >
                                <form:option value="">Select State</form:option>
                                <form:options items="${states}" /> 
                            </form:select>	

                        </li>
                        <li class="rescity">
                            <form:select class="city" path="city" id="city" value="${city}"  >
                                <form:option value="">Select City</form:option>
                                <form:options items="${cities}" /> 
                            </form:select>	

                        </li>

                    </ul>
                </div>
            </div>
            <div class="resedentialAddress-error-div hidden" style="color:red"></div>
            <hr/>
            <div class="margT1">	
                <label class="margT1">Do you have a valid address proof for your current residential address mentioned?</label>
                <div class="addressProof">
                    <div class="radio_btn_yes"><form:radiobutton path="peraddresssameascurr" name="addressProof" checked="true" class="" value="Yes"/><span>Yes</span></div>
                    <div><form:radiobutton path="peraddresssameascurr" name="addressProof" class="" value="No"/><span>No</span></div>
                </div>				  					
            </div>
            <div class="margT1 permanentAddr hidden">	
                <label class="margT1">Permanent Residential Address<span class=""></span></label>				  					
                    <form:input id="permanentAddress" path="permanentAddress" class="row margT1 input permanentResidentialAddress" placeholder="Permanent Residential Address"/>
            </div>
            <div class="permanentAddr_div hidden">
                <div class="permanent_residentAddress">
                    <div> Residential Address 1 
                        <span>( H. No., Floor No... etc)</span>
                    </div>			
                    <div>				
                        <form:input	id="permaddress"  class="permanent_AddressField1" name="address"  path="permaddress" value="${permaddress}"/>
                    </div>
                </div>
                <div class="permanent_residentAddress">
                    <div class=""> Residential Address 2 
                        <span>( Street, Near by... etc)</span>
                    </div>
                    <div>
                        <form:input id="permaddress2" class="permanent_AddressField2"  path="permaddress2" value="${permaddress2}" name="address"/>
                    </div>
                </div>
                <div class="permanent_residentAddress">
                    <div class=""> Residential Address 3
                        <span>( Street, Near by... etc)</span>
                    </div>
                    <div>
                        <form:input id="permaddress3"	class="permanent_AddressField3"  path="permaddress3" name="address" value="${permaddress3}"/>
                    </div>
                </div>					
                <div class="permanent_residentAddress">
                    <ul class="address">
                        <li>
                            <form:input class="pincodeICICI" id="PpinCodeICICI" maxlength="6" type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==6) return false;" path="PpinCode" value="${PpinCode}" placeholder="Pincode"/>
                        </li>
                        <li>
                            <form:select class="state" path="PState" id="PState" value="${PState}" >
                                <form:option value="">Select State</form:option>
                                <form:options items="${states}" /> 
                            </form:select>	

                        </li>
                        <li class="rescity">
                            <form:select class="city" path="PCity" id="PCity" value="${PCity}" >
                                <form:option value="">Select City</form:option>
                                <form:options items="${cities}" /> 
                            </form:select>	

                        </li>

                    </ul>
                </div>
            </div>

            <div class="permanentAddr-error-div hidden" style="color: red;"></div>

            <div class="modal fade" id="benefits" tabindex=-1 role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="pull-right close_div"><img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg"  alt=""></div>
                            <h5 class="margT2 colorDarkBlue pad2">Benefits</h5>
                            <span class="list_cont" id="benefitContent"></span>
                        </div>
                    </div>
                </div>
            </div>   

            <!-- Work details -->
            <div class="colorDarkBlue">
                <h2 class="heading_txt margT10">Professional Details</h2>
            </div><hr/>				   
            <div class="margT1">	
                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <label class="margT1">Employment Status<span class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select id="employmentType" class="margT1" path="employmentType" value="${employmentType}" >
                                <form:option value="">Select Employment Type</form:option>
                                <form:options items="${salaryStatus}" /> 
                            </form:select>
                        </div>
                    </div>
                    <div class="col-sm-6" id="ITRStausd">
                        <div>
                            <label class="margT1">Latest ITR acknowledged?
                                <span id="ITRStausspan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select id="ITRStaus" class="margT1" path="ITRStaus" value="${ITRStaus}" >
                                <form:option value="">Select ITR Status</form:option>
                                <form:options items="${iciciITR}" /> 
                            </form:select>
                        </div>
                    </div>
                </div><hr>

                <div class="row">
                    <div class="col-sm-6" id="iciciBankRelationshipd">
                        <div>
                            <label class="margT1">ICICI Bank Relationship
                                <span id="iciciBankRelationshipspan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select class="margT1" id="iciciBankRelationship" path="iciciBankRelationship" value="${iciciBankRelationship}" >
                                <form:option value="">Select Bank Relationship</form:option>
                                <form:options items="${ICICIBankRelationshipstatus}" /> 
                            </form:select>
                        </div>
                    </div>

                    <div class="col-sm-6" id="ICICIBankRelationShipNod">
                        <div>
                            <label class="margT1">ICICI Bank Account Number
                                <span id="ICICIBankRelationShipNospan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" id="ICICIBankRelationShipNo"  path="ICICIBankRelationShipNo"  value="${ICICIBankRelationShipNo}" placeholder="ICICI Bank Account Number"/>
                        </div>
                    </div>

                </div><hr>
                <div class="row">
                    <div class="col-sm-6" id="salaryBankWithOtherBankd">
                        <div>
                            <label class="margT1">Salary Account With Other Bank
                                <span id="salaryBankWithOtherBankspan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select class="margT1" id="salaryBankWithOtherBank" path="salaryBankWithOtherBank" value="${salaryBankWithOtherBank}" >
                                <form:option value="">Select Salary Account With Other Bank</form:option>
                                <form:options items="${salaryBankWithOtherBank}" /> 
                            </form:select>
                        </div>
                    </div>
                    <!--new cr start-->
                    <div class="col-sm-6" id="salaryAccountOpenedd">
                        <div>
                            <label class="margT1">Duration of Salary Account Opened
                                <span id="salaryAccountOpenedspan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:select class="margT1" id="salaryAccountOpened" path="salaryAccountOpened" value="${salaryAccountOpened}" >
                                <form:option value="">Select Duration of Salary Account Opened</form:option>
                                <form:options items="${salaryAccountOpened}" /> 
                            </form:select>
                        </div>
                    </div>

                </div><hr>
                <div class="row">
                    <div class="col-sm-6" id="companyNamed">
                        <div>
                            <label class="margT1">Company Name
                                <span id="companyNamespan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" id="companyName"  path="companyName" value="${companyName}" placeholder="Company Name"/>
                        </div>
                    </div>

                    <div class="col-sm-6" id="totalExperienced">
                        <div>
                            <label class="margT1">Total Work Experience
                                <span id="totalExperiencespan" class="mandatory-field">*</span></label>
                        </div>
                        <div>
                            <form:input	class="row margT1 input" id="totalExperience"  path="totalExperience" value="${totalExperience}" placeholder="Total Work Experience"/>
                            <div class="total_div margT1 hidden row">
                                <ul class="datetimth">
                                    <li class="yearly">
                                        <form:input	class="row input" placeholder="Years"  maxlength="2" path="Years" type="number" onKeyPress="if(this.value.length==2) return false;" id="year" value="${year}" />
                                    </li>
                                    <li class="month">
                                        <form:input	class="row input" placeholder="Months" maxlength="2" path="Months" type="number" onKeyPress="if(this.value.length==2) return false;"  id="month" value="${month}" />
                                    </li>									
                                </ul>
                            </div>
                            <div class="total_div-error-div hidden" style="color:red"></div>
                        </div>
                    </div>
                    <!--new cr end-->    
                </div>
                <!--code end--> 
                <hr/>							
                <div class="margT1 companyAddr">	
                    <label class="margT1">Company Address<span class="mandatory-field">*</span></label>										  					
                    <form:input id="companyAddress" path="companyAddress" class="row margT1 input companyAddress_box" placeholder="Company Address"/>
                </div>

                <div class="companyAddr_div hidden">
                    <div class="companyAddress">
                        <div> Company Address 1 
                            <span>( H. No., Floor No... etc)</span>
                        </div>		
                        <div>					
                            <form:input	class="companyAddressField1" id="OAddress" name="address"  path="OAddress" value="${OAddress}" />
                        </div>
                    </div>
                    <div class="companyAddress">
                        <div class=""> Company Address 2 
                            <span>( Street, Near by... etc)</span>
                        </div>
                        <div>
                            <form:input	class="companyAddressField2" id="OAddress2" name="address"  path="OAddress2" value="${OAddress2}" />
                        </div>
                    </div>
                    <div class="companyAddress">
                        <div class=""> Company Address 3
                            <span>( Street, Near by... etc)</span>
                        </div>
                        <div>
                            <form:input	class="companyAddressField3" id="OAddress3" name="address"  path="OAddress3" value="${OAddress3}" />
                        </div>
                    </div>					
                    <div class="companyAddress">
                        <ul class="address">
                            <li>
                                <form:input class="pincodeICICI" id="OPincodeICICI" type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==6) return false;" maxlength="6" path="OPincode" value="${OPincode}" placeholder="Pincode"/>
                            </li>
                            <li>
                                <form:select class="state" path="OState" id="OState" value="${OState}" >
                                    <form:option value="">Select State</form:option>
                                    <form:options items="${states}" /> 
                                </form:select>	
                            </li>
                            <li class="rescity">
                                <form:select class="city" path="OCity" id="OCity" value="${OCity}" >
                                    <form:option value="">Select City</form:option>
                                    <form:options items="${cities}" /> 
                                </form:select>	

                            </li>

                        </ul>
                    </div>
                </div>
                <div class="companyAddr_div-error-div hidden" style="color:red"></div>
                <hr/>


                <div class="margT1">	
                    <label class="margT1">Please provide either your home or office <strong>landline number</strong> for verification purposes<span class="mandatory-field">*</span></label>				  					
                    <div class="row">
                        <div class="col-sm-6 phoneNum">
                            <form:input path="homeNumber" id="homeNumber" class="row margT1 input homeNumber" placeholder="Home Landline Number"/>

                            <div class="phoneNum_div margT1 hidden row">
                                <ul>
                                    <li class="std">
                                        <form:input	class="row input std_class" placeholder="STD" type="number"  onKeyPress="if(this.value.length==4) return false;" maxlength="4" path="std" id="std" value="${std}" />
                                    </li>
                                    <li class="phNum">
                                        <form:input	class="row input phone_class" placeholder="Phone Number" type="number"   onKeyPress="if(this.value.length==8) return false;" maxlength="8" path="phone" id="phone" value="${phone}" />
                                    </li>									
                                </ul>
                            </div>
                            <div class="homeNumber-error-div hidden" style="color:red"></div>
                        </div>
                        <div class="col-sm-6">
                            <form:input class="row margT1 input officeNumber" path= "officeNumber" id="officeNumber" placeholder="Office Landline Number"/>

                            <div class="officePhoneNum_div margT1 hidden row">
                                <ul>
                                    <li class="std_office">
                                        <form:input	class="row input ostd_class" placeholder="STD" type="number"  onKeyPress="if(this.value.length==4) return false;" maxlength="4" path="OStd" id="OStd"/>
                                    </li>
                                    <li class="phNum_office">
                                        <form:input	class="row input ophone_class" placeholder="Phone Number" type="number"   onKeyPress="if(this.value.length==8) return false;"  maxlength="8" path="OPhone" id="OPhone"/>
                                    </li>									
                                </ul>
                            </div>
                            <div class="officeNumber-error-div hidden" style="color:red"></div>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="margT1">
                    <label class="margT1">Statement to be sent to<span class="mandatory-field">*</span></label>	
                    <div>
                        <div class="radio_btn_yes">

                            <form:radiobutton   path="stmtTosent" id="statement"value="Home" name="statement"  class="statement"/> <span>Home</span>

                        </div>
                        <div>

                            <form:radiobutton   path="stmtTosent" id="statement" value="Office" name="statement" checked="true" class="statement"/><span> Office</span>

                        </div>
                    </div>
                </div>
                <hr/>
                <form:hidden path="agentId" id="agentId"/>
                <form:hidden path="utmSource" id="utmSource"/>
                <form:hidden path="utmMedium" id="utmMedium"/>
                <form:hidden path="utmCampaign" id="utmCampaign"/>

                <form:hidden path="formNumber" id="formNumber"/>
                <form:hidden path="otpVal" id="otpVal"/>
                <form:hidden path="randomNumber" id="randomNumber"/>
                <form:hidden path="sourceOfCall" id="sourceOfCall"/>
                <form:hidden path="initiatedBy" id="initiatedBy"/>
                <form:hidden path="initiationDate" id="initiationDate"/>
                <form:hidden path="emailInactiveFlag" id="emailInactiveFlag"/>
                <form:hidden path="beJpNumber" id="beJpNumber"/>
                <form:hidden path="otpTransactionId" id="otpTransactionId"/>
                <form:hidden path="token" id="token"/>
                <form:hidden path="pendingAttempt" id="pendingAttempt"/>
                <div class="margT1">	
                    <label class="margT1">Terms & Conditions</label>		  					
                    <div class="termsAndConditions">


                        <div class="terms-checkbox-holder">
                            <form:checkbox value="termsAndConditions" id="termsAndConditions" class="margT1 statement" path="termsAndConditions" onclick="termsAndConditionClick('${cardBean.cardName}','${cardBean.cardName}')"/>  I agree with the <a href="https://www.jetprivilege.com/terms-and-conditions/jetairways-icici-bank-cobrand-credit-card"  target="_blank" onclick="$.fn.TermsAndConditionsClickOnIcIcI('${cardBean.cardName}', '${cardBean.cardName}')">Terms and Conditions</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="submit_button">
            <input class="submit_btn button_blue submit" onClick="$(this).closest('form').submit()" id="jp_btn" type="button" value="Submit" onclick="$.fn.AfterApplyJPNumberPopUpClick(jpNum, 'Earn with Partners: Swipe any of our Co-brand Cards')">
        </div>

    </section>
</form:form>


