<%-- 
    Document   : rejected
    Created on : 16 Sep, 2017, 3:26:37 PM
    Author     : Parvti G
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

          <section class="container clearfix bottomPadding">
               <section class="main-section">
                  <div class="msgWrapper text-center">
<!--                  <div class="message_header"><h1>Congratulations!</h1></div>-->
                     <div class="msgtrigger_div margT5">
                        <h1>Review*</h1>
                        <p class="toppad"> Thank you for applying for ${cardName}. Your application has been received by ICICI Bank and is under process. ICICI Bank will notify you on the application status within 10 working days, after reviewing the details provided. In the meantime, please call ICICI Bank on 18002671400 from 09:30 AM to 7:00 PM (working days only) and quote the Reference Number ${ICICIID} for assistance.</p><br>
                        <p>*T&C of ICICI Bank and third parties apply. ICICI Bank shall have the sole discretion to issue the credit card. The decision of ICICI Bank shall be final and binding in all respects.</p>

                     </div>
<!--                     <div class="margT5 otherCards">-->
                       <div>
                      <a href="${sessionScope.appurl}"><button class="buttonBlue_rec margT5" id="CrdApp_btn_view-other-cards">View Other Cards</button></a>
                     </div>
<!--                     </div>-->
                  </div>
               </section>
            </section>
    
       <!--Adobe code starts here-->
        <script type="text/javascript"> 
                      
        function pageInfo(){
            
            //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----referred--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----referred--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----referred--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----referred--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----referred--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----referred--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----referred--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----referred-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----referred--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End   
            
            
        var url='${sessionScope.appurl}';
        digitalData.pageInfo={"pageName":"ICICI-Provissionaly Approved","category":"Cards","subCategory1":"App Status - Prv Approved","subCategory2":'${cardName}',"partner":"JP","currency":"INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
        digitalData.formInfo={"formName":"ICICI-Provissionaly Approved Form","formNumber":""}
        }
          
        </script>
       <!--Adobe code ends here--> 