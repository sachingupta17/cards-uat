<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>


<!--<div id="social-profiles" data-toggle="modal"  class="hidden" onclick="assistMeeClick()">  
    <img src="${applicationURL}static/images/footer/phone-call.png" width="30" height="30">
    <h5>Assist Me</h5>
</div> -->

<style>
@media only screen and (min-width: 768px) {
	.popup-container {
		width: 838px;
		height: 515px; /*550px;*/
		border: none;
		margin: 0px auto;
		text-align: center;
	}
}

.otpsuccess_credit {
	width: 70%;
	border: 2px solid #7dd62e;
	/*     margin: 165px 0px 0 140px; */
	margin: 100px 0px 0 140px;
	padding: 13px 0 10px 10px;
	background: #f2fbe2 url(../images/icons/success_icon.png) no-repeat 5px;
}

.otpactive {
	width: 371px;
	margin-top: 0px;
	padding: 5px 0 5px 10px;
	color: #939598;
	/*     position: absolute; */
}

.otp-form span {
	display: block;
	position: relative;
	color: #000000 !important;
	font-family: Montserrat !important;
	font-size: 14px !important;
	/*     left: 6px; */
	/*     left: 9px; */
}

div#otpError {
	margin-top: 10px;
}

@media only screen and (min-width: 768px) {
	.application-opt-popup {
		padding: 20px 30px 0 0px;
	}
}

@media only screen and (min-width: 768px) {
	.application-opt-popup img {
		margin-top: 0px !important;
	}
}

@media only screen and (min-width: 768px) {
	.application-opt-popup .logo span {
		width: 24px;
		height: 24px;
		float: right;
		margin-right: 20px;
	}
}

@media only screen and (min-width: 768px) {
	.application-opt-popup .otp-form span a {
		font-weight: bold;
	}
}

.resentActBtn {
	color: #03868b;
}

.disabled123 {
	pointer-events: none;
	opacity: 0.7;
}

.disabledDiv {
	pointer-events: none;
	opacity: 0.4;
}

.otp_aunthication {
	height: 45px;
	width: 304px;
	margin-top: 30px;
	border-radius: 3px;
	/*border: 1px solid #33ccff;*/
	color: #ffffff;
	background-color: #03868b;
	font-family: Montserrat;
	font-size: 14px;
	font-weight: bold;
	letter-spacing: 0.3px;
}
</style>
<!-- <body onload="loadSignup()"> -->
<form:form autocomplete="off" id="callMe" commandName="callMeBean">

	<div class="modal fade" id="callMe_modal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<div id="callmeclose" class="proceedPopup_close pull-right">
						<img
							src="${applicationURL}static/images/co-brand/popup_close_icon.jpg"
							alt="" onclick="formAbandonOfAssistMe()">
					</div>
					<div class="callme_popup">
						<div class="text-center">Fill in your details for us to call
							you & help you complete the credit card application</div>
						<div class="margT2 colorDarkBlue text-center">
							<h2 class="heading_txt">Tell us about yourself</h2>
						</div>
						<div class="error_box1 hidden">
							<ul class="error_list1"></ul>
						</div>
						<div class="row">
							<div class="enroll_div margT2">
								<div class="middleName_input user_details">
									<form:input id="socialName" path="socialName"
										placeholder="Full Name" type="text" value="" />
								</div>
								<div class="phone_input user_details">
									<form:input id="socialPhone" path="socialPhone" maxlength="10"
										placeholder="Phone" type="number"
										onKeyPress="if(this.value.length==10) return false;" value="" />
								</div>
								<div class="email_input user_details">
									<form:input id="socialEmail" path="socialEmail"
										placeholder="Email" type="text" value="" />
								</div>

								<!--                                <div class="jpNumber_input user_details">
                                <%--<form:input id="socialjpNumber" path="socialjpNumber"  placeholder="JetPrivilege Number" type="number" value="" maxlength="9" onKeyPress="if(this.value.length==9) return false;"/>--%>
                            </div>-->
							</div>
							<form:hidden path="formNumber" id="formNumber" />
							<form:hidden path="pageName" id="pageName" />
							<form:hidden path="cardName" id="cardName" />
						</div>
						<div id="loader" class="text_center hidden margT5">
							<img src="${applicationURL}static/images/header/ajax-loader.gif"
								alt="Page is loading, please wait." />
						</div>
						<div class="text-center margT10">
							<input class="button_thickBlue" type="button" id="callMeContine"
								value="Submit">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form:form>

<div class="modal fade" id="successCallmeModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="sizer modal-content">
			<div class="modal-header">
				<!--              <div class="proceedPopup_close pull-right">
                                        <img src="$ {applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.CloseOnPopUpClick('Enroll Now')">
                                    </div>-->

			</div>
			<div class="modal-body">
				<p class="modaltext"
					style="color: #01a200; text-align: center; font-size: 18px;">Thank
					you for sharing your details! Our representative will reach out to
					you for further assistance.</p>


			</div>
			<div class="modal-footer">
				<button type="button" class="successcallbut" data-dismiss="modal">OK</button>
			</div>

		</div>

	</div>
</div>
<div class="modal fade" id="CompletedForm" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="sizer modal-content">
			<div class="modal-header"></div>
			<div class="modal-body">
				<p class="modaltext"
					style="color: #01a200; text-align: center; font-size: 18px;">This
					form has been already submitted.</p>


			</div>
			<div class="modal-footer">
				<a href="${applicationURL}"> <input class="button_thickBlue"
					type="button" id="completedBtn" value="OK"></a>
			</div>

		</div>

	</div>
</div>
<div class="modal fade" id="spends_slider" tabindex="-1" role="dialog">

	<div class="modal-dialog vertical-align-center">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body calculatemiles">
				<div class="proceedPopup_close pull-right">
					<img
						src="${applicationURL}static/images/co-brand/popup_close_icon.jpg"
						alt=""
						onclick="$.fn.CloseOnPopUpClick('Calculate your free flight')">
				</div>
				<div class="spendsSlider_popup margT5">
					<div class="margT2 colorDarkBlue text-center">
						<h2 class="heading_txt">Calculate your Free Flights based on
							your monthly spends</h2>
					</div>
					<div style="padding-top: 10px; text-align: center;"
						class="spendsVal_error">
						<span id="error_msg"
							style="font-size: 14px; color: red; display: none;"></span>
					</div>
					<div class="extest">
						<div class="col-md-12">
							<p class="amtvalue1">
								Annual Spends : INR <span class="slider-output"
									id="yearly_bill1">0</span>
							</p>

						</div>
						<div class="col-md-12">
							<div class="range-example-modal"></div>
							<div class="imagebar1">
								<img src="${applicationURL}static/images/co-brand/numbar.png" />
							</div>

						</div>

						<div class="col-md-12">
							<div class="infodown">
								<div class="aheadiv">INR</div>
								<input id="unranged-value2" type="text" inputmode="numeric"
									name="amountInput2" value="0" maxlength="9" />
								<div class="donebut">Done</div>
							</div>
							<div class="slider_modal_div_error"></div>
						</div>

					</div>
					<!--                    <div class="margT2 colorDarkBlue text-center">
                                            <h2 class="heading_txt">Calculate your Free Flights based on your monthly spends</h2>
                                        </div>
                                        <div class="slider_valu margB5">
                                            <div style="display:inline"><span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5 Thousand</span></div>
                                            <div style="display:inline; float:right;"><span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span></div>
                                        </div>
                                        <div class="row">
                    
                                            <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal">										   									   
                                        </div>-->
				</div>
				<!--                <div class="annualSpends margT5 text-center">
                                    <span>Annual Spends: </span><span class="annualSpends_value">0</span>
                                </div>	
                                <div class="text-center margT2">
                                    <input class="button_red annual_spends" type="button" value="Done">
                                </div>-->
			</div>
		</div>
	</div>
</div>

<%-- <form:form autocomplete="off" id="quickEnrollUser" action="enrollme" commandName="enrollBean"> --%>
<%--     <form:hidden path="enrollbpNo"/> --%>

<div class="modal fade" id="OtpSuccPopup" tabindex=-1 role="dialog">
	<div class="modal-dialog mob-modal">
		<div class="modal-content">
			<div class="modal-body">

				<div class="popup-container icici-popup-cont">
					<div class="application-opt-popup">
						<div class="logo">
							<img src="${applicationURL}static/images/img/intermiles.png" />
							<%-- 					<span class="proceedPopup_close1"><img src="${applicationURL}static/images/img/cross.png" onclick="formAbandonmentOnPopUp('Pop-up close')"></span> --%>
							<!-- 					<span><img src="img/cross.png"></span> -->
						</div>
						<h2>OTP Successfully Authenticated</h2>
						<div class="thms">
							<img src="${applicationURL}static/images/img/thums-up.png">
						</div>
						<button type="submit" class="otp_aunthication proceedPopup_close1">Okay</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="provide_jpNumber" tabindex=-1 role="dialog">
	<div class="modal-dialog mob-modal">
		<div class="modal-content">
			<div class="modal-body">



				<div class="popup-container icici-popup-cont">
					<div class="application-opt-popup logospacer">
						<div class="logo">
							<div class="intlogo" style="width: 100%;">
								<img src="${applicationURL}static/images/img/intermiles.png" />
							</div>
							<span class="proceedPopup_close1 crossmark"><img
								src="${applicationURL}static/images/img/cross.png"></span>
						</div>

						<div class="upperline">
							<div class="icici-line"></div>
						</div>
						<div class="mob-cross">
							<span class="mob-popupClose"><img
								src="${applicationURL}static/images/img/cross.png"></span>
						</div>
						<h2>Authenticate using OTP</h2>
						<input type="hidden" name="otpTransactionId" id="otpTransactionId">
						<input type="hidden" name="token" id="token"> <input
							type="hidden" name="pendingAttempt" id="pendingAttempt">
						<p>Enter the OTP sent on your Mobile Number</p>
						<div class="otp-form icici-form">
							<input type="number" id="otpValue" class="geninput" name=""
								placeholder="Enter OTP" maxlength="6" type="number"
								onKeyPress="if (this.value.length == 6) return false;">

							<div class="resend_otp disabledDiv icici-right">
								<a href="javascript:;" class="regenerateLink"><span
									id="resendId">RESEND</span></a>
							</div>

							<div class="otpError" id="otpError"></div>

							<!-- 					<span><a href="#" id="resend30Sec">Resend in 30 seconds</a></span> -->




							<div id="error_msg_otp" class="error_msg_otp"
								style="padding: 5px 0 5px 0px; font-size: 12px; color: red;"></div>
							<!-- 							<div class="otpsuccess" id="otpSucces"> -->
							<!-- 								<p></p> -->
							<!-- 							</div> -->
							<div class="otpactive" id="otpactive">
								<p></p>
							</div>

							<div class="num-form">
								<form:input path="jpNumber" id="jpNumber"
									placeholder="Enter InterMiles Number" maxlength="9"
									pattern="[0-9]*" class="inputJPNumber"
									onKeyPress="if(this.value.length==9) return false;"
									type="number" />
							</div>
							<%--                                     <form:hidden path="jpTier" id="jpTier" placeholder="InterMiles Membership Tier" disabled="true"  /> --%>
							<!--                                     <input type="hidden" id="otbpNo"/> -->
							<div id="error_msg" class="error_msg"
								style="padding: 5px 0; font-size: 13px; color: red;"></div>


							<h4>
								<p class="login-line">
									<span>If you are not an Intermiles member, <a
										class="enroll_here1" id="enroll_here1"
										href="https://uat.intermiles.com/rewards-program/enrol"
										onclick="getClickVal()"> Sign up Now!</a>
									</span>
								</p>
							</h4>
							<!-- <h4> <p class="login-line"><span>If you are not an Intermiles member, <a class="enroll_here1" id="enroll_here1" href="javascript:void(0);" onclick="getClickVal()"> Sign up Now!</a>  </span></p></h4> -->
							<button type="submit" class="genebutton_HDFC icici-btn"
								id="jpNumpopup" disabled="true">Submit OTP</button>



						</div>
					</div>

					<!-- 			   <div class="otpsuccess_credit hidden" id="otpSucces"> -->
					<!--                     <p></p> -->
					<!--                </div> -->
					<!-- 			   <div class="otpactive hidden" id="otpactive"> -->
					<!-- <!--                     <p></p>Enter your InterMiles Number -->

					<!--                </div>	 -->

				</div>



			</div>
		</div>
	</div>
	<div class="loaddiv">
		<div class="loaderamex" id="icicidisable" style=""></div>
	</div>
</div>

<!--     <div class="modal fade" id="provide_jpNumber" tabindex=-1 role="dialog"> -->
<!--         <div class="modal-dialog"> -->
<!--             <div class="modal-content"> -->
<!--                 <div class="modal-body"> -->
<!--                     <div class="proceedPopup_close pull-right"> -->
<%--                         <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="formAbandonmentOnPopUp('Pop-up close')"> --%>
<!--                     </div> -->
<!--                     <div class="jpNum_popup"> -->
<!--                         <div class="text-center jpPopup_header"> -->
<!--                             Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-brand Card. Apply here -->
<!--                         </div> -->

<!--                         <div class="margT2 colorDarkBlue text-center"> -->
<!--                             <h2 class="heading_txt">Provide InterMiles No.</h2> -->
<!--                         </div> -->

<!--                         <div class="row"> -->
<!--                             <div class="jpNumber_div margT2"> -->
<!--                                 <div class="inputwidthinfo"> -->
<!--                                     <div class="jpNumber_input takeallwidth"> -->
<%--                                         <form:input path="jpNumber" id="jpNumber" placeholder="InterMiles No." maxlength="9" pattern="[0-9]*" class="inputJPNumber" onKeyPress="if(this.value.length==9) return false;" type="number"/> --%>
<!--                                     </div> -->
<!--                                     <div class="promoinfo"> -->
<%--                                         <a class="tooltips" href="javascript:void(0);"><img src="${applicationURL}static/images/co-brand/whiteicon.png"> --%>
<%--                                             <span class="tooltip_content"><span class="pull-right close_toolTip"><img src="${applicationURL}static/images/co-brand/close_btn.png" alt=""></span>  --%>
<!--                                                 Promocode has been applied for the given InterMiles No. -->
<!--                                         </a> -->
<!--                                     </div> -->
<!--                                 </div> -->
<!--                                 <div class="jpNumber_input margT2 hidden"> -->
<%--                                     <form:hidden path="jpTier" id="jpTier" placeholder="InterMiles Membership Tier" disabled="true"  /> --%>
<!--                                     <input type="hidden" id="otbpNo"/> -->
<!--                                 </div> -->
<!--                                 <div id="error_msg" class="error_msg" style="padding: 10px 0; font-size: 13px; color: orangered"></div> -->
<%--                                   <c:if test="${empty beJpNumber}">  
<%--                                 <div class="text-center margT2"> --%>
<%--                                     If you are not a InterMiles member, <a href="javascript:void(0)" class="enroll_here"  id="quickEnroll" style="text-decoration: underline;" onclick="afterEnrollHereClick('Enroll here', 'co-brand quick enrollment')">please enroll here</a> --%>
<%--                                 </div> --%>
<%--                                 </c:if> --%>
<!--                             </div> -->

<!--                             <h4> <p class="login-account">or <a href="javascript:;" id="logincredit">Log into your account</a></p></h3> -->
<!-- 							<h4> <br><p class="login-line"><span>Not a member yet? <a class="enroll_here1" id="enroll_here1" href="https://uat.intermiles.com/rewards-program/enrol" onclick="getClickVal()"> Sign up</a> for Intermiles Membership! </span></p></h4> -->


<!--                             new added card pop 16-10-18 -->
<!--                             <div class="genotp"> -->
<!--                                 <button  id="otpButton" class="genbut"></button><input class="geninput" id="otpValue" maxlength="6" type="number" onKeyPress="if (this.value.length == 6) -->
<!--                                             return false;"  />   -->
<!--                                 added error div for otp input -->
<!--                                 <div id="error_msg_otp" class="error_msg_otp"  style="padding: 10px 0; font-size: 12px; color: red;"></div> -->

<!--                                 <button  id=" regenerateotp" class="genbut">Re-Generate OTP</button> -->


<!--                             </div> -->

<!--                             <div class="otpsuccess" id="otpSucces"> -->
<!--                                 <p></p> -->
<!--                             </div> -->
<!-- <div class="otpactive" id="otpactive"> -->
<!--                                 <p></p> -->
<!--                             </div> -->

<!--                         </div> -->
<!--                         <div class="text-center margT10"> -->
<!-- <!--                             <input class="button_thickBlue" type="button" id="jpNumpopup" onClick="return ewt.trackLink({name: 'Amex_Submit_Button', type: 'click', link: this});" value="Submit"> -->
<!--                             <input class="button_thickBlue" type="button" id="jpNumpopup" value="Submit"> -->
<!--                         </div> -->
<!--                     </div> -->
<!--                     <div class="enroll_popup hidden">  -->
<!--                         <div class="text-center"> -->
<!--                             Enroll into the InterMiles Programme now to start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-Brand Card. -->
<!--                         </div> -->
<!--                         <div class="margT2 colorDarkBlue text-center"> -->
<!--                             <h2 class="heading_txt">Enroll Now</h2> -->
<!--                         </div> -->
<!--                         <div class="error_box2 hidden"> -->
<!--                             <ul class="error_list2"> -->
<!--                             </ul> -->
<!--                         </div>  -->
<!--                         <div class="row"> -->
<!--                             <div class="pull-left margT2 nav_web"> -->
<%--                                 <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_slide go_To_Jp" onclick="$.fn.formAbandonmentOnSubmitJPnumberPopup('pop-up back', 'co-brand quick enrollment')"> --%>
<!--                             </div> -->
<!--                             <div class="enroll_div margT2"> -->
<!--                                 <div class="title user_details"> -->
<%--                                     <form:select path="enrollTitle" id="enrollTitle" placeholder="Title" class="title_select"> --%>
<%--                                         <form:option value="">Select Title</form:option> --%>
<%--                                         <form:option value="Mr">Mr</form:option> --%>
<%--                                         <form:option value="Ms">Ms</form:option> --%>
<%--                                         <form:option value="Mrs">Mrs</form:option> --%>
<%--                                         <form:option value="Dr">Doctor</form:option> --%>
<%--                                         <form:option value="Prof">Professor</form:option> --%>
<%--                                         <form:option value="Captain">Captain</form:option> --%>
<%--                                     </form:select> --%>
<!--                                 </div> -->
<!--                                 <div class="gender hidden"> -->
<%--                                     <c:forEach items="${genderStatus}" var="status"> --%>
<!--                                         <div style="display:inline-block"> -->
<!--                                             <label class="radio-inline"> -->
<%--                                                 <form:radiobutton --%>
<%--                                                     path="enrollGender" name="enrollgender" --%>
<%--                                                     id="enrollGender" --%>
<%--                                                     value="${status.key}" /> </label> --%>
<%--                                             <label class="gender_desc" for="${status.key}" >${status.value}</label> --%>
<!--                                         </div> -->
<%--                                     </c:forEach> --%>
<!--                                 </div> -->
<!--                                 <div class="firstName_input user_details"> -->
<%--                                     <form:input type="text" path="enrollFname" id="enrollFname" placeholder="First Name"/> --%>
<!--                                 </div> -->
<!--                                 <div class="middleName_input user_details"> -->
<%--                                     <form:input type="text" path="enrollMname" id="enrollMname" placeholder="Middle Name"/> --%>
<!--                                 </div> -->
<!--                                 <div class="lastName_input user_details"> -->
<%--                                     <form:input type="text" path="enrollLname" id="enrollLname" placeholder="Last Name"/> --%>
<!--                                 </div> -->
<!--                                 <div class="city_input user_details"> -->
<%--                                     <form:input type="text" path="enrollCity" id="enrollCity" placeholder="City"/> --%>
<!--                                 </div> -->
<!--                                 <div class="phone_input user_details"> -->
<%--                                     <form:input path="enrollPhone" id="enrollPhone" maxlength="10" placeholder="Phone" onKeyPress="if(this.value.length==10) return false;" type="number" /> --%>
<!--                                 </div> -->
<!--                                 <div class="email_input user_details"> -->
<%--                                     <form:input path="enrollemail" id="enrollemail" placeholder="Email"/> --%>
<!--                                 </div> -->
<!--                                 <div class="dob_input user_details"> -->
<%--                                     <form:input path="enrollDob" id="enrollDob" class="datepicker" placeholder="Date Of Birth"/> --%>
<!--                                 </div> -->
<!--                                 <div class="g-recaptcha recaptchaarea" -->
<!--                                      data-sitekey=<spring:message code="google.recaptcha.site"/>  id="captch"></div> -->
<!--                                 <div>  -->
<!--                                     <div class="boxinput">	 -->
<%--                                         <form:checkbox value="" id="termsAndConditionsEnroll" class="margT1 statement" path="termsAndConditionsEnroll" />  --%>
<!--                                         <span>I agree to the InterMiles membership  -->
<!--                                             <a href="https://www.jetprivilege.com/terms-and-conditions/jetprivilege"  target="_blank"> -->
<!--                                                 Terms and Conditions</a><span style="margin: 0px 4px;display: inline;">and</span> <a href="https://www.jetprivilege.com/disclaimer-policy"  target="_blank"> -->
<!--                                                 Privacy Policy</a></span> -->
<!--                                     </div> -->
<!--                                     <div class="boxinput">	 -->
<%--                                         <form:checkbox value="" id="recieveMarketingTeam" class="margT1 statement" path="recieveMarketingTeam" />   --%>
<!--                                         <span> Yes, I would like to receive Marketing Communication -->


<!--                                         </span> -->
<!--                                     </div> -->
<!--                                 </div> -->
<!--                                 <div class="error_box_enrolStatus hidden" id="enroll_error"> -->
<!--                                 </div> -->
<!--                             </div> -->
<!--                         </div> -->
<!--                         <div id="loader" class="text_center hidden margT5"> -->
<%--                             <img src="${applicationURL}static/images/header/ajax-loader.gif" alt="Page is loading, please wait." /> --%>
<!--                         </div> -->
<!--                         <div class="text-center margT10 nav_web"> -->
<!--                             <input class="button_thickBlue" type="button"  id="enrollForm1" value="Continue"> -->
<!--                         </div> -->
<!--                         <div class="nav_arrow_mobile margT10"> -->
<!--                             <div class="left_nav"> -->
<%--                                 <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_arrow go_To_Jp"> --%>
<!--                             </div> -->
<!--                             <div class="continue_button"> -->
<!--                                 <input class="button_thickBlue" type="button" id="enrollForm_mob_view1" value="Continue"> -->
<!--                             </div> -->
<!--                         </div> -->
<!--                     </div>   -->
<!--                 </div> -->
<!--             </div> -->
<!--         </div> -->
<!--         <div class="loaddiv"><div class="loaderamex" id="icicidisable" style="/* margin:auto; */"> </div></div>                                    -->
<!--     </div> -->
<%-- </form:form> --%>
<jsp:include page="iciciForm.jsp" />

<!-- </body> -->


<script>

 $('.proceedPopup_close1').on('click', function () {
    $(this).parents('.modal').modal('hide');
});
 
 $('.mob-popupClose').on('click', function () {
	    $(this).parents('.modal').modal('hide');
	});
 
 
 
 var isIMCorrect = false;
 
    //Adobe code starts here
    var iciciCardName = "";
    var iciciVariantName = "";
    var buttonClicked = false;
    var errorDetails = "";
    var genTime = "";
    var submitTime = "";
    var loggedInUser = '${sessionScope.loggedInUser}';
    
    //(phase 6 start)
    var assistMeFlag = false;
    var loggedInUser = '${sessionScope.loggedInUser}';
    var name = "";
    var mobile = "";
    var email = "";
    var assistError="";
    var lastAccessedField="";
    
    name = $("#socialName").val();
    mobile = $("#socialPhone").val();
    email = $("#socialEmail").val();
    //(phase 6 end)
    //Adobe code ends here

    $(document).ready(function () {
    var signupFlg = getCookie("isSignupICICI");
    var fomNo = getCookie("ICICI_FromNo")
        if(signupFlg == "Y"){
      		fetchFilledForm();
      	  }
    
    
    
    
    
    var loginUser = '${sessionScope.loggedInUser}';
	let str = loginUser;
	let n = 2;
	str = str.substring(n);
    
	var jpNum = str;
    var cpNo = $("#cpNo").val();
    $('#jpNumpopup').attr('disabled', true);
    $('#jpNumpopup').removeClass('genebutton-HDFC');
    $('#jpNumpopup').addClass('genebutton_HDFC');
    if (jpNum == "") {
//         $(".error_msg").text("Please enter InterMiles No.");
//         $(".error_msg").removeClass('hidden');
        $("#jpTier").val('');
        $("#jetpriviligemembershipTier").val('');
        isCorrectJpNumber = false;
        isIMCorrect = false;
        $('#jpNumpopup').attr('disabled', true);
        $('#jpNumpopup').removeClass('genebutton-HDFC');
        $('#jpNumpopup').addClass('genebutton_HDFC');
        return false;
    } else
    if (jpNum != "" && jpNum.length == 9) {
        if (isNaN(jpNum)) {
            $(".error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
            $(".error_msg").removeClass('hidden');
            $("#jetpriviligemembershipTier").val('');
            isCorrectJpNumber = false;
            isIMCorrect = false;
            $('#jpNumpopup').attr('disabled', true);
            $('#jpNumpopup').removeClass('genebutton-HDFC');
            $('#jpNumpopup').addClass('genebutton_HDFC');
            return false;
        } else {
            jpNumLastDig = jpNum % 10;
            jpNoTemp = eval(jpNum.slice(0, -1)) % 7;

            if (jpNumLastDig !== jpNoTemp)
            {
                $(".error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                $(".error_msg").removeClass('hidden');
                $("#jetpriviligemembershipTier").val('');
                isCorrectJpNumber = false;
                isIMCorrect = false;
                $('#jpNumpopup').attr('disabled', true);
                $('#jpNumpopup').removeClass('genebutton-HDFC');
                $('#jpNumpopup').addClass('genebutton_HDFC');
                return false;
            } else { 
                $("#jetpriviligemembershipNumber").val(jpNum);
$.get("${applicationURL}getjpTier?jpNum=" + jpNum, function (data, status) {
                    if (status == "success" && data.length > 2) {
                    	$("#jetpriviligemembershipTier").val(data);
                        isCorrectJpNumber = true; 
                        isIMCorrect = true;
                        $(".error_msg").text("");
                        $(".error_msg").addClass('hidden');
                        $('#jpNumpopup').attr('disabled', true);
//                         $('#otpButton').attr('disabled', false);
                        $('#jpNumpopup').attr('disabled', false);
                       $('#jpNumpopup').removeClass('genebutton_HDFC');
                       $('#jpNumpopup').addClass('genebutton-HDFC');
                       
                       //validateOTPOnSubmit();
                       
                    } else {
                        $(".error_msg").text("Your application may not have gone through successfully. Please click Submit again.");
                        $(".error_msg").removeClass('hidden');
                        $("#jetpriviligemembershipTier").val('');
                        $('#jpNumpopup').attr('disabled', true);
                        $('#jpNumpopup').removeClass('genebutton-HDFC');
                        $('#jpNumpopup').addClass('genebutton_HDFC');
                        isCorrectJpNumber = false;
                        isIMCorrect = false;
                        return false;
                    }
                });

            }
        }
    } else {

        $(".error_msg").removeClass('hidden');
        $("#jetpriviligemembershipTier").val('');
        isCorrectJpNumber = false;
        isIMCorrect = false;
        return false;
    }
     
    
    
    
    
    });
        
        
    $(document).ready(function () {
    	
    	console.log("--" + $("#beJpNumber").val());
        var beJpNumber = $("#beJpNumber").val();
        
        if (beJpNumber != "") {
            $.get("${applicationURL}checkFormStatus?randomNumber=" + '${beRandomNumber}' , function (data, status) {
                        if(status == "success"){
                            console.log("data===>",data);
                            if(data  != "Completed" || data == ""){
            $("#provide_jpNumber").modal('show');
            $(".proceedPopup_close").addClass('hidden');
            $('.enroll_popup').addClass('hidden');
            $('.jpNum_popup').removeClass('hidden');
            $('#jpNumber').val(beJpNumber);
            
            $("#jpNumber").attr("disabled", true);
            console.log("jnuber", $('#jpNumber').val());
            $("#jpNumber").trigger('keyup');
            $("#jetpriviligemembershipNumber").val(beJpNumber);
            $(".promoinfo").addClass('hidden');
//             $.get("${applicationURL}getTier?jpNum=" + beJpNumber, function (data, status) {
	$.get("${applicationURL}getjpTier?jpNum=" + beJpNumber, function (data, status) {
                if (status == "success" && data.length > 2) {
                    console.log("in success")
                    $("#jetpriviligemembershipTier").val(data);
                    isCorrectJpNumber = true;
                    isIMCorrect = true;
                    $(".error_msg").text("");
                    $(".error_msg").addClass('hidden');
//                     $('#otpButton').attr('disabled', false);
//                     $("#otpButton").css("color", "#fff");
                    $('#jpNumpopup').attr('disabled', false);
                    $('#jpNumpopup').removeClass('genebutton_HDFC');
                    $('#jpNumpopup').addClass('genebutton-HDFC');
//                     $("#jpNumpopup").css("color", "#ddd");
                } else {
                    $(".error_msg").text("Your application may not have gone through successfully. Please click Submit again.");
                    $(".error_msg").removeClass('hidden');
                    $("#jetpriviligemembershipTier").val('');
                    $('#jpNumpopup').attr('disabled', true);
                    $('#jpNumpopup').removeClass('genebutton-HDFC');
                    $('#jpNumpopup').addClass('genebutton_HDFC');
//                     $("#jpNumpopup").css("color", "#ddd");
//                     $('#otpButton').attr('disabled', true);
//                     $("#otpButton").css("color", "#ddd");
                    //$('.geninput').attr('disabled', true);
                    isCorrectJpNumber = false;
                    isIMCorrect = false;
                    return false;
                }
            });
            $.get("${applicationURL}getIciciMemberDetail?jpNum=" + beJpNumber, function (data, status) {
                if (status == "success" && data.length > 2) {
                    console.log("get member", data);
                    if (data != "") {
                        $("#mobile").val(data);
                    }
                }
            });
            $("#quickEnroll").attr("disabled", true);
            $('#otpButton').attr('disabled', false);
            $('#quickEnroll').css('cursor', 'default');
            $("a.enroll_here").off("click", callenroll);
                   $("#CompletedForm").modal('hide');

}else{
                $("#CompletedForm").modal('show');

        }
        }
        });
        }else{
                    console.log("inside  null beJpNumber")

         console.log("message", $("#form_Status").val());
        if ($("#form_Status").val() != "" && $("#form_Status").val() == "Completed") {
            $("#CompletedForm").modal('show');
        } else {
            $("#CompletedForm").modal('hide');
        }
        }
//          $("#promo").addClass('hidden');
//        $("#namefield").removeClass('col-sm-8');

        var callmeflag = '${callMeFlag}';
//        console.log("callmeflag===>", callmeflag);
        if (callmeflag != "") {
            if (callmeflag == 1) {
                $("#social-profiles").removeClass('hidden');

            } else {
                $("#social-profiles").addClass('hidden');

            }
        } else {
            $("#social-profiles").addClass('hidden');
        }


//          console.log("adderss length"+$('#permanent_AddressField3').val().length +"  "+$('#permanent_AddressField2').val().length + " "+$('#permanent_AddressField1').val().length);
//        if($('#permanent_AddressField1').val().length >=40 || $('#permanent_AddressField2').val().length >=40 || $('#permanent_AddressField3').val().length >=40 ){
//        console.log("inside if condition");
//            $('.permanentAddr-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
//            $('.permanentAddr-error-div').removeClass('hidden');
//            
//          }else{
//              console.log("inside else condition");
//              $('.permanentAddr-error-div').html('');
//            $('.permanentAddr-error-div').addClass('hidden');
//          }
//        
        $("#jpNumber, #otpValue, #enrollPhone, #mobile, #monthlyIncome, #std, #phone, #OStd, #OPhone").on("keydown, keypress", function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;


            }

        });
        $("#jpNumber").on('keyup', function (evt) {
//            console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 9) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $("#otpValue, #pinCodeICICI, #PpinCodeICICI, #OPincodeICICI").on('keyup', function (evt) {
//            console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 6) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $("#monthlyIncome, #phone, #OPhone").on('keyup', function (evt) {
//            console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 8) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $("#OStd, #std").on('keyup', function (evt) {
//            console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 4) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $("#socialPhone,#mobile, #enrollPhone").on('keyup', function (evt) {
//            console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 10) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $('#socialName').keydown(function (event) {
            var charCode = event.keyCode;
            if ((charCode > 64 && charCode < 91) || charCode == 8 || charCode == 9 || charCode == 32) {
                return true;
            } else {
                return false;
            }
        });
        $("#year, #month").on('keyup', function (evt) {
//            console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 2) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $("#promocode").on("keyup", function () {
            $("#promocode").attr("maxlength", "12");

        });

        $("#promocodeError").hide();
        $("#promocodeError").html("");
        $("#promocodeaccept").hide();
        $("#promocodeaccept").html("");

//         $('#otpButton').text('Generate OTP');
//         $('#otpButton').attr('disabled', true);
        $('.geninput').attr('disabled', true);
        $('#otpSucces').hide();
        $('#otp_error').hide();
        var loginUser = '${sessionScope.loggedInUser}';
//        console.log("loginUser t",loginUser);
//        if (loginUser !== '') {
//
//            var date = $("#dateOfBirth").val();
//       
//            var NDate = date.split("-").reverse()
//            var d = new Date(NDate[0]+"/"+NDate[1]+"/"+NDate[2]);
////            var d = new Date(date.split("-").reverse());
//            var dd = d.getDate();
//            var mm = d.getMonth() + 1;
//            var yy = d.getFullYear();
//            var dbDate = yy + "/" + mm + "/" + dd;
//            var date2 = new Date(dbDate);
//           $("#dateOfBirth").datepicker({
//                dateFormat: 'mm-dd-yy'
//            }).datepicker('setDate', date) //date2
//            
//
//        } else {
//
//
//            $('#dateOfBirth').datepicker({dateFormat: "mm-dd-yy", changeMonth: true,
//                changeYear: true, yearRange: '1900:2020', defaultDate: ''
//            });
//        }
        $('#dateOfBirth-error').hide();
        $('#totalExperience').prop('disabled', false);
        $('#iciciBankRelationship').prop('disabled', false);
        $('#ICICIBankRelationShipNo').prop('disabled', true).css("background-color", "#ebebe4");
        $('#salaryBankWithOtherBank').prop('disabled', false);
        $('#ITRStaus').prop('disabled', true);
        $('#ITRStausspan').hide();
        $("#companyNamespan").show();
        $('#totalExperiencespan').hide();
        $('#ICICIBankRelationShipNospan').hide();
        $('#salaryBankWithOtherBankspan').hide();
        $('#salaryAccountOpenedspan').hide();
        $('#icicidisable').hide();
        $('.loaddiv').css("bottom", "0px");


        $('.termsAndConditions').css({'background-color': "transparent", 'padding': '0%', 'border': '0px'});

        if ('${iciciBean.peraddresssameascurr}' == "No") {
            $('.permanentAddr').removeClass('hidden');
        }

        $('.middleName').on("focus", function () {
            $('.fullName-error-div').addClass('hidden');
        });
        $('.lastName').on("focus", function () {
            $('.fullName-error-div').addClass('hidden');
        })
        function hasalphabet(str) {
            return /^[a-zA-Z()]+$/.test(str);
        }

        var Rtype = $("#iciciBankRelationship").val();


        var isCorrectJpNumber = false;

        if ($('#pinCodeICICI').val() != "" && $('#city').val() != "" && $('#state1').val() != "") {
            $("#hcity").val($('#city').val());
            $("#hstate").val($('#state1').val());
            $('#state1').attr("disabled", true);
            $('#city').attr("disabled", true);

        }
        if ($('#PpinCodeICICI').val() != "" && $('#PCity').val() != "" && $('#PState').val() != "") {
            $("#hcity1").val($('#PCity').val());
            $("#hstate1").val($('#PState').val());
            $('#PCity').attr("disabled", true);
            $('#PState').attr("disabled", true);
        }
        if ($('#OPincodeICICI').val() != "" && $('#OCity').val() != "" && $('#OState').val() != "") {
            $("#hcity2").val($('#OCity').val());
            $("#hstate2").val($('#OState').val());
            $('#OCity').attr("disabled", true);
            $('#OState').attr("disabled", true);
        }
//        $('#jpNumpopup').attr('disabled', true);

        var isValid = false;
        var originalJPNum;
        var user = '${isLoginUser}';
        var errMsg = '${errorMsg}';
        var mobile;
        $('#jpNumpopup').attr('disabled', true);
        $('#jpNumpopup').removeClass('genebutton-HDFC');
        $('#jpNumpopup').addClass('genebutton_HDFC');
        if ('${iciciBean.peraddresssameascurr}' == "No") {
            $('.permanentAddr').removeClass('hidden');
        }



        var result_Value, result_Value_year;
        slidermodal();
        $('#unranged-value2').on('keyup', function () {
//            console.log("at keyup");
            var valSpend = $("#unranged-value2").val();
            valSpend = valSpend.replace(/\,/g, '');
            return handlespend(valSpend);

        });
        function handlespend(valSpend) {
            if (valSpend < 5000 || valSpend > 1500000) {
//             console.log("at keyup check");
                if ($(".slider_modal_div_error").is(':visible') == false) {

                    $('.slider_modal_div_error').css('display', 'block');
                    $(".slider_modal_div_error").html("Please enter spends value within the range.");
                }
                return false;
            } else {
                $('.slider_modal_div_error').css('display', 'none');
                $(".slider_modal_div_error").html("");
                return true;
            }
        }
        $("#unranged-value2").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            // Allow: home, end, left, right, down, up
                                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || (e.keyCode == 110) || (e.keyCode == 190)) {
                        e.preventDefault();
                    }
                });
        $("#unranged-value2").keyup(function (event) {
            var valSpend = $("#unranged-value2").val();
            var valSpendNum = '';
            valSpendNum = valSpend.replace(/\,/g, '');
            valSpendNum = parseInt(valSpendNum, 10);
//        if (valSpendNum >= 0) {  //&& valSpendNum <= 1500000

            // skip for arrow keys
            if (event.which == 37 || event.which == 39)
                return;
            // format number
            $(this).val(function (index, value) {
                if (valSpend < 5000 || valSpend > 1500000) {
                    $(".range-example-modal").asRange('val', 5000);

                    getValue(5000);
                    $("#yearly_bill1").html(result_Value_year);
//                                (valSpendNum > 5000) || (valSpendNum < 1500000)
//                               console.log("***********")
//                           $(".range-example-5").asRange('val', valSpendNum);
//                          
//                                getValue(valSpendNum);
//                                 $("#yearly_bill").html(result_Value_year);
                } else {
//                               console.log("^^^^^^^^^^^^")
                    $(".range-example-modal").asRange('val', valSpendNum);
//                          
                    getValue(valSpendNum);
                    $("#yearly_bill1").html(result_Value_year);
//                               $(".range-example-5").asRange('val', 5000);
//                          
//                                getValue(5000);
//                                 $("#yearly_bill").html(result_Value_year);
                }
//                           $(".range-example-modal").asRange('val', valSpendNum);
                $("#unranged-value2").focus();
//                              if(valSpendNum != ""){
//                                getValue(valSpendNum);
//                                 $("#yearly_bill1").html(result_Value_year);
//                           }
                var x = value.replace(/,/g, "");
                var lastThree = x.substring(x.length - 3);
                var otherNumbers = x.substring(0, x.length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                return res;

            });
//        }//if close
        });
        $('#unranged-value2').on("keyup", function (evt) {

            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;

            } else
                return true;
        });
        getValue($('.range-example-modal').asRange('get'));
        $("#unranged-value2").val(result_Value);
        $("#yearly_bill1").html(result_Value_year);

        function getValue(param) {
            var yearly = (param * 12).toString();
            var monthly = param.toString();
            var lastThree = monthly.substring((monthly.length) - 3);
            var otherNumbers = monthly.substring(0, monthly.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            result_Value = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            lastThree = yearly.substring((yearly.length) - 3);
            otherNumbers = yearly.substring(0, yearly.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            result_Value_year = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        }
        function slidermodal() {
            $(".range-example-modal").asRange({

                min: 5000,
                max: 500000,
                format: function (value) {
                    getValue(value);
                    return result_Value;
                },
                onChange: function (valueslider) {
                    var value = $('.range-example-modal').asRange('get');
//                    console.log("--value", value);

                    var a = document.getElementById("unranged-value2");

                    getValue(value);
                    var valueoftext = $("#unranged-value2").val();
                    valueoftext = valueoftext.replace(/\,/g, '');
                    handlespend(valueoftext);

                    a.value = result_Value;
//                    console.log("at on change", $("#unranged-value2").val());
//                    console.log("result_Value_year", result_Value_year)
                    $("#yearly_bill1").html(result_Value_year);

                    if (value > 5000) { 
//                        console.log("----------2--->", value);
                        $(".range-example-5").asRange('set', value);
                    }
                }
            });
        }
        $(".donebut").click(function () {
            var arr = [];
            arr.push('${cardBean.cpNo}');
            var spend = $("#yearly_bill1").html().replace(/<\/?img[^>]*>/g, "");
            spend = spend.substring(spend.indexOf(' ') + 1);
            $.get("${applicationURL}calcfreeflights?spend=" + spend + "&cpNoList=" + arr, function (data, status) {
                if (status == "success") {
                    $.each(data, function (key, value) {
                        $("#freeFlights" + key).html(value.split(",")[1]);
                        $("#jpMiles" + key).html("on earning <br>" + value.split(",")[0] + " JPMiles");
                    });
                    $("#error_msg ,#layover_error_msg").hide();
                    $('#spends_slider').modal('hide');

                    $('.free_flights_div').removeClass('hidden');
                    $('.free_flights_desc').addClass('hidden');
//                    $('#spends_slider').modal('hide');
                }
            });
            $(".close_filter").trigger('click');
        });

        $('#dateOfBirth').on('change', function () {
            $("#iciciBean").validate().element('#dateOfBirth')

        });
        $('#dateOfBirth').on('click', function () {

            $('#dateOfBirth').addClass("ignore");

        });

        //         -new cr start-
        var years = [];
        for (var i = 0; i < 100; i++) {
            years.push("" + i);
        }
        var months = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"];
        $(document).on('focus', '#totalExperience', function () {
            $('.total_div').removeClass('hidden');
            $('#totalExperience').addClass('hidden');
            $('#totalExperience-error').addClass('hidden');
            $('.yearly input').focus();
        });
        function checkValue(value, arr) {
            var status = true;

            for (var i = 0; i < arr.length; i++) {
                var name = arr[i];
                if (name == value) {
                    status = false;
                    return status;
                    break;
                }
            }

            return status;
        }
        $('.total_div').focusout(function () {
//            console.log("in focusout function");
            var year = $('.yearly input').val();
            var month = $('.month input').val();

            var emptype = $("#employmentType").val();
            $('#totalExperience-error').addClass('hidden');
            if (emptype == "Salaried" && year == "" && month == "") {

                $('#totalExperience-error').addClass('hidden');
                $('.total_div-error-div').html('Please Enter Years and Months.');
                $('.total_div-error-div').removeClass('hidden');
            } else if ((emptype == "Self Employed-Professional" || emptype == "Self Employed-Business") && year == "" && month == "") {
//                console.log("yess");
                $('.total_div-error-div').html('');
                $('.total_div-error-div').addClass('hidden');
            }

            if (year != "") {
                var message;
                if (checkValue(year, years) == true) {
                    message = "Please select valid year.";
                    $('.total_div-error-div').html(message);
                    $('.total_div-error-div').removeClass('hidden');
                } else if (year != "" && month == "") {
                    if (emptype == "Salaried") {
                        $("#month").val(0);
                    } else if ((emptype == "Self Employed-Professional" || emptype == "Self Employed-Business")) {
//                        console.log("yes111s");
                        $("#month").val(0);
                    }
                }

            }
            if (month != "") {
                var message;
                if (checkValue(month, months) == true) {
                    message = "Please select valid month.";
                    $('.total_div-error-div').html(message);
                    $('.total_div-error-div').removeClass('hidden');
                } else if (year == "" && month != "") {
                    if (emptype == "Salaried") {
                        $("#year").val(0);
                    } else if ((emptype == "Self Employed-Professional" || emptype == "Self Employed-Business")) {
//                        console.log("yes111iiis");
                        $("#year").val(0);
                    }
                }

            }

        });

        $('.yearly input, .month input').on('keyup', function () {
            year = $('.yearly input').val();
            month = $('.month input').val();
            if (year != "" || month != "")
                $('.total_div-error-div').html('');
        });
        $(document).click(function (event) {
            $(".total_div").click(function () {
                $('.total_div').removeClass('hidden');
                $('#totalExperience').addClass('hidden');
            });

            $(document).mouseup(function (e)
            {
                var container = $('.total_div');
                if (!container.is(e.target) && container.has(e.target).length === 0)
                {
                    var year = $('.yearly input').val();
                    var month = $('.month input').val();
                    var emptype = $("#employmentType").val();
//                console.log("emptype=1=>" + emptype);

                    if (emptype == "Salaried" && year == "" || month == "")
                        $('#totalExperience-error').css('display', 'none');
                    else if ((emptype == "Self Employed-Professional" || emptype == "Self Employed-Business") && year == "" || month == "")
                        $('#totalExperience-error').css('display', 'block');
                    else {

                        $('#totalExperience-error').css('display', 'block');
                    }
                    $('.total_div').addClass('hidden');
                    $('#totalExperience').removeClass('hidden');
                    var a;
                    if (year <= 1 && month > 1) {
                        a = "" + year + " year " + month + " months"
                    } else if (year <= 1 && month <= 1) {
                        a = "" + year + " year " + month + " month"
                    } else if (year > 1 && month <= 1) {
                        a = "" + year + " years " + month + " month"
                    } else {
                        a = "" + year + " years " + month + " months";
                    }

//            $('#totalExperience').val(year +" years " + month+" months");

                    $('#totalExperience').val(a);
//                console.log("aa" + a);
                    if (year == "" && month == "")
                        $('#totalExperience').val('');

                }
            });
//            if (!$(event.target).closest('.total_div').length) {
//
//                var year = $('.yearly input').val();
//                var month = $('.month input').val();
//                var emptype = $("#employmentType").val();
////                console.log("emptype=1=>" + emptype);
//
//                if (emptype == "Salaried" && year == "" || month == "")
//                    $('#totalExperience-error').css('display', 'none');
//                else if ((emptype == "Self Employed-Professional" || emptype == "Self Employed-Business") && year == "" || month == "")
//                    $('#totalExperience-error').css('display', 'block');
//                else {
//
//                    $('#totalExperience-error').css('display', 'block');
//                }
//                $('.total_div').addClass('hidden');
//                $('#totalExperience').removeClass('hidden');
//                var a;
//                if (year <= 1 && month > 1) {
//                    a = "" + year + " year " + month + " months"
//                } else if (year <= 1 && month <= 1) {
//                    a = "" + year + " year " + month + " month"
//                } else if (year > 1 && month <= 1) {
//                    a = "" + year + " years " + month + " month"
//                } else {
//                    a = "" + year + " years " + month + " months";
//                }
//
////            $('#totalExperience').val(year +" years " + month+" months");
//
//                $('#totalExperience').val(a);
////                console.log("aa" + a);
//                if (year == "" && month == "")
//                    $('#totalExperience').val('');
//            }
        });

        $('.yearly input, .month input').on("keypress", function (evt) {


            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;

            }
        });

        $(".month input").autocomplete({
            source: months,
            select: function (event, ui) {

                $(".month input").val(ui.item.value);
                var month = ui.item.value;
//                console.log("month" + month);
            }

        });

        $(".yearly input").autocomplete({
            source: years
        });

//         new cr end
        if ($("#employmentType").val() == "Salaried") {
            $('#ITRStaus').prop('disabled', true).css("background-color", "#ebebe4");
            $('#ITRStausspan').hide();

        } else {
            $('#ITRStaus').prop('disabled', false).css("background-color", "");
            $('#ITRStausspan').show();

        }
        if ($("#iciciBankRelationship").val() != "No Relationship") {
//            console.log("00000000000000000000000000000000")
            $('#ICICIBankRelationShipNo').prop('disabled', false).css("background-color", "");
            $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
            $('#salaryBankWithOtherBankspan').show();

        } else {
//                        console.log("999999999999999999999")

            $('#ICICIBankRelationShipNo').prop('disabled', true).css("background-color", "#ebebe4");
            $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
            $('#salaryBankWithOtherBankspan').show();

        }
//        console.log("year" + '${Years}' + "month" + '${Months}')
//        console.log("year 1" + $("#year").val() + "month 1" + $("#month").val());
        if ($("#year").val() != "" && $("#month").val() != "") {
            var year = $("#year").val();
            var month = $("#month").val();
            var a;
            if (year <= 1 && month > 1) {
                a = "" + year + " year " + month + " months"
            } else if (year <= 1 && month <= 1) {
                a = "" + year + " year " + month + " month"
            } else if (year > 1 && month <= 1) {
                a = "" + year + " years " + month + " month"
            } else {
                a = "" + year + " years " + month + " months";
            }

//            $('#totalExperience').val(year +" years " + month+" months");

            $('#totalExperience').val(a);
//              
        }
        $('#ICICIBankRelationShipNo').on('click', function () {
            if ($('#iciciBankRelationship').val() == "Savings Account" || $('#iciciBankRelationship').val() == "Current Account") {
                $('#ICICIBankRelationShipNo').attr('type', 'number');
            } else {
                $('#ICICIBankRelationShipNo').attr('type', 'text');
            }
        });

        $.validator.addMethod('checkdateOfBirth', function (value, element) {

            var birthDate1 = document.getElementById('dateOfBirth').value;

            var d = new Date(birthDate1);
            var mdate = birthDate1.toString();

            var monthThen = parseInt(mdate.substring(0, 2), 10);

            var dayThen = parseInt(mdate.substring(3, 5), 10);
            var yearThen = parseInt(mdate.substring(6, 10), 10);
            var today = new Date();
            var loginUser = '${sessionScope.loggedInUser}';

            if (loginUser === "") {
                var birthday = new Date(yearThen, monthThen - 1, dayThen);
            } else {
                var birthday = new Date(monthThen - 1, dayThen, yearThen);
            }

            var differenceInMilisecond = today.valueOf() - birthday.valueOf();
            var year_age = Math.floor(differenceInMilisecond / 31536000000);
            var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);
            var month_age = Math.floor(day_age / 30);
            day_age = day_age % 30;

        });

        //         new cr start
        function getage() {
            var birthDate1 = document.getElementById('dateOfBirth').value;
//            console.log("birthDate1==>" + birthDate1);
            var d = new Date(birthDate1);
            var mdate = birthDate1.toString();

            var monthThen = parseInt(mdate.substring(0, 2), 10);

            var dayThen = parseInt(mdate.substring(3, 5), 10);
            var yearThen = parseInt(mdate.substring(6, 10), 10);
            var today = new Date();
            var loginUser = '${sessionScope.loggedInUser}';

            if (loginUser === "") {
                var birthday = new Date(yearThen, monthThen - 1, dayThen);
            } else {
                var birthday = new Date(yearThen, monthThen - 1, dayThen);
//                var birthday = new Date(monthThen - 1, dayThen, yearThen);
            }

            var differenceInMilisecond = today.valueOf() - birthday.valueOf();
            var year_age = Math.floor(differenceInMilisecond / 31536000000);
            var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);
            var month_age = Math.floor(day_age / 30);
            day_age = day_age % 30;
//            console.log("day_age" + day_age + "year_age" + year_age);
            return year_age;
        }
//                new cr end

        $.validator.addMethod('checkFullName', function (value, element) {
            var fn = $("#fname").val();
            var ln = $("#lname").val();

            if (fn != "" && ln != "")
                return true;
            else
                return false;
        });



        $.validator.addMethod('checkCurrentAddress', function (value, element) {
//            console.log("Inside IF checkaddress validator");
            var address = $(".residentAddressField1").val();

            pincode = $('#pinCodeICICI').val();

            var city = $("#city").val();
            var state = $("#state1").val();



            if ($('.residentAddressField1').val() == "") {
                if ($(".resedentialAddress-error").is(':visible') == true) {
//                    console.log("##########called Depends in validator");
                    $('.resedentialAddress-error').addClass('hidden');
                }
            }

            return true;


//            if (address != "" && pincode != "" && city != "" && state != "") {
//                return true;
//            } else{
//                console.log("Inside #############");
//                if($(".resedentialAddress-error-div").is(':visible') == true){
//                    console.log("Inside ELSE **********************");
//                     $('.resedentialAddress-error-div').addClass('hidden');
//               
//                }
//                return false;
//            }
        });

        $.validator.addMethod('checkCompanyAddress', function (value, element) {
            var address = $("#OAddress").val();
            var pincode = $("#OPincode").val();
            var city = $("#OCity").val();
            var state = $("#OState").val();
            if (address != "" && pincode != "" && city != "" && state != "")
                return true;
            else
                return false;
        });

        $.validator.addMethod('minLength', function (value, element) {
            var relationno = $("#ICICIBankRelationShipNo").val().length;
            if (relationno > 12 || relationno < 12) {
                return false
            } else
            {
                return true;
            }

        });

        $.validator.addMethod("minsal", function (value, element) {
            var len = $("#ICICIBankRelationShipNo").val().length;
            if (($("#iciciBankRelationship").val() == "Salary Account")) {
                if (len < 12) {
                    return false;
                } else {
                    return true;
                }
            }
        });
        $.validator.addMethod("mincur", function (value, element) {
            var len = $("#ICICIBankRelationShipNo").val().length;
            if (($("#iciciBankRelationship").val() == "Current Account")) {
                if (len < 12) {
                    return false;
                } else {
                    return true;
                }
            }
        });

        $.validator.addMethod("minsav", function (value, element) {
            var len = $("#ICICIBankRelationShipNo").val().length;
            if (($("#iciciBankRelationship").val() == "Savings Account")) {
                if (len < 12) {
                    return false;
                } else {
                    return true;
                }
            }
        });
//        $.validator.addMethod("minloan", function(value,element) {
//            var len=$("#ICICIBankRelationShipNo").val().length;
//            if(($("#iciciBankRelationship").val() == "Loan")){
//            if(len<16 ){
//                return false;
//            }else{
//                return true;
//            }
//        }
//        });

        $.validator.addMethod('checkalphanumeric', function (value, element) {
            var relationno = $("#ICICIBankRelationShipNo").val();
            var letter = /[a-zA-Z]/;
            var number = /[0-9]/;
            var spcl = /^[a-zA-Z0-9]+$/;
            var chk_spcl = spcl.test(relationno);
            if (chk_spcl)
            {
                var valid = number.test(relationno) && letter.test(relationno);

                return valid;
            }

        });
        //sprint 52
        //sprint 52
        function checkAlphaNumeric(value) {
            var letter = /[a-zA-Z]/;
            var number = /[0-9]/;
            var spcl = /^[a-zA-Z0-9]+$/;
            var chk_spcl = spcl.test(value);
            if (chk_spcl)
            {
//                console.log("in if of chk_spcl")
                var valid = number.test(value) && letter.test(value);
//                console.log("valid", valid)
                return valid;
            }
        }
        function verifyPromoCode(value) {
            var promoErrorMesg, promoRespStatus, verified;
            $.ajax({method: 'POST',
                async: false,
                url: '${applicationURL}promoCode?voucherNo=' + value,
                success: function (result) {
//                            console.log("result",result);
                    var jsonStr = JSON.parse(result);
//                            console.log("====",jsonStr);

//                    console.log("errroe", jsonStr.JsonResponse);
//                    console.log("errorMsg", jsonStr.errorMsg);
//                            var jsn=JSON.parse(jsonStr.JsonResponse);
//                              var jsn=JSON.parse(jsonStr.JsonResponse);

//                           console.log("- jsn---->",jsn.errorMessage[0],"-----voucherStatus-->",jsn.voucherStatus);
                    if (jsonStr.errorMsg != undefined || jsonStr.errorMsg != "") {
                        promoErrorMesg = jsonStr.errorMsg;
                    } else {
                        promoErrorMesg = ""
                    }

                    promoRespStatus = jsonStr.voucherStatus;
                    if (promoErrorMesg == "" || promoRespStatus == "Unused") {
//                        console.log("not show error");
                        promoErrorMesg = "";
                        verified = promoErrorMesg;
                    } else {
//                        console.log("show error");
                        verified = promoErrorMesg;
                    }

                },
                error: function (xhr, status) {
                    console.log(status);
                    console.log(xhr.responseText);

                }
            });
//            console.log("promoErrorMesg: ", promoErrorMesg, " promoRespStatus:", promoRespStatus)
//            console.log("verified", verified);
            return verified;

        }
        $("#promocode").on("focusout", function () {
//            console.log("jp number: ", $("#jetpriviligemembershipNumber").val(), " user:", loginUser);
            var promocode = $("#promocode").val();
//            console.log("promocode:", promocode);
            if (promocode != "") {
                if (promocode == '${defaultPromocode}') {
                    $("#promocodeError").html("");
                    $("#promocodeError").hide();
                    $("#promocodeaccept").show();
                    $("#promocodeaccept").html("Your Promo Code has been applied successfully");

                } else {
                    if ($("#jetpriviligemembershipNumber").val() != "") {
                        var validPromoChar = checkAlphaNumeric(promocode);
                        if (validPromoChar) {
                            var verifiedPromo = verifyPromoCode(promocode);
                            if (verifiedPromo != "") {
                                $("#promocodeError").html("" + verifiedPromo + "");
                                $("#promocodeError").show();
                                $("#promocodeaccept").html("");
                                $("#promocodeaccept").hide();

                            } else {
                                $("#promocodeError").html("");
                                $("#promocodeError").hide();
                                $("#promocodeaccept").html("Your Promo Code has been applied successfully");
                                $("#promocodeaccept").show();
                                $('.tooltips').show();

                            }
                        } else {
                            $("#promocodeError").html("Please Enter Valid Promo Code.");
                            $("#promocodeError").show();
                            $("#promocodeaccept").html("");
                            $("#promocodeaccept").hide();

                        }

                    } else {
                        $("#promocodeError").html("Error in Promo Code! Please contact Member Services Desk. ");
                        $("#promocodeError").show();
                        $("#promocodeaccept").html("");
                        $("#promocodeaccept").hide();
                        $('.tooltips').hide();

                    }
                }
            } else {
                $("#promocodeError").html("");
                $("#promocodeError").hide();
                $("#promocodeaccept").html("");
                $("#promocodeaccept").hide();
                $('.tooltips').hide();

            }
        });
        //        save or update from data
//        var myTimer = "$ {timer}";
        var myTimer;
        var timer = "${timer}";
        var timer1 = "${timer1}";
        var saveformDataState = true;
        function SaveFormData()
        {
            mobile = $("#mobile").val();
            if (mobile != "" && mobile.length == 10 && saveformDataState == true) {
//                console.log("back end form");
                saveformDataState = false;
//                console.log("****************STATE IS (false) :" + saveformDataState + " Time:" + Date());
//                console.log("##### Saving form data")
                $.ajax({
                    url: '${applicationURL}saveIciciFormData',
                    data: $('#iciciBean').serialize(),
                    type: "Post",
                    success: function timer(result) {
//                        console.log("-----3rd---->" + $("#emailInactiveFlag").val());
                        saveformDataState = true;
//                        console.log("****************STATE IS (true) :" + saveformDataState + " Time:" + Date());
                    },
                    error: function (request, status, error) {
                        saveformDataState = true;
//                        console.log("****************STATE IS (true) :" + saveformDataState + " Time:" + Date());
                    }
                });



            } else
            {
//                console.log('################### NOT SAVING ############################');
            }
        }
        function TimerSaveData()
        {
            mobile = $("#mobile").val();
            if (mobile != "" && mobile.length == 10) {
                console.log("Timer is calling SaveFormData function");
                SaveFormData();
            } else
            {
                clearInterval(myTimer);
                console.log('TIMER IS STOPPED');
            }
        }

        if (loggedInUser.length > 8)
        {
            //SaveForm data
            SaveFormData();
            //Start Timer
            if (mobile != "" && mobile.length == 10) {
                console.log('TIMER IS STARTED:' + timer);

                myTimer = setInterval(TimerSaveData, timer);
            }
        }


        $("#emailInactiveFlag").val($("#emailInactiveFlag").val());
//        console.log("----1st----->" + $("#emailInactiveFlag").val());

        // New Code for 5 Mintues user idle ..
        var IDLE_TIMEOUT = 300; //seconds
        var _idleSecondsTimer = null;
        var _idleSecondsCounter = 0;
        var sendemailbit = 0;
        document.onclick = function () {
            _idleSecondsCounter = 0;
        };

        document.onmousemove = function () {
            _idleSecondsCounter = 0;
        };

        document.onkeypress = function () {
            _idleSecondsCounter = 0;
        };

        _idleSecondsTimer = window.setInterval(CheckIdleTime, 1000);

        function CheckIdleTime() {
            _idleSecondsCounter++;
            if (_idleSecondsCounter % 10 === 0)
            {
                console.log('time elapsed MOD 10..' + _idleSecondsCounter);
            }
            if (_idleSecondsCounter >= IDLE_TIMEOUT) {

//                                console.log('User is idle for 5 Minutes');
                //window.clearInterval(_idleSecondsTimer);
                if (mobile != "" && mobile.length == 10) {
//                                 console.log('Mobile Triggered');
                    sendemailbit = 1;
                    $("#mobile").trigger('change');
                    _idleSecondsCounter = 0;

                } else
                {
                    _idleSecondsCounter = 0;
                    //sendemailbit=0
                }
            }
        }

        var mobile1 = '';
        var old;
        var arlene1 = [];
        var oldmobileno = "";
        var inactiveCounter = 0;

        $("#mobile").on("change", function () {
            mobile1 = $("#mobile").val();
//            console.log('on mobile change');
            //         console.log('test'+arlene1);
//         console.log('Mobile No: '+mobile1 +" OldMobileNo: "+oldmobileno );timer
            if (mobile1 != "" && mobile1.length == 10) {
                SaveFormData();//Saving Form Data
//                console.log('Tab out is calling SaveFormData function');
//                console.log('TIMER IS STARTED');
                myTimer = setInterval(TimerSaveData, timer); // Starting Timer           
//            if(mobile1!=old){
                if (oldmobileno != mobile1) // if oldmobile and new mobileno is not same then reset a timer to send an email
                {
//                    console.log("oldmobileno  ------", oldmobileno);
//                    console.log("mobile1 ------", mobile1);

                    inactiveCounter++;
                    timer1 = "${timer1}";
                }
                var index = arlene1.indexOf(mobile1);
//                   console.log("Mobile no exist in Array: "+index);
                if (index === -1) {
//                  console.log("Phone no array :"+arlene1)
                    oldmobileno = mobile1;
//                    console.log("oldmobileno   )))))", oldmobileno);
//                    console.log("mobile1))))))", mobile1);

                    if (sendemailbit == 1)
                    {
                        _idleSecondsCounter = 0;
                        sendemailbit = 0;
                        arlene1.push(oldmobileno);

//                   console.log('Email will be sent in : some time  '+timer1);

                        //var timers= setTimeout(function() {

//                       console.log('Silver Pop is get Called for..'+mobile1);
                        $.ajax({url: '${applicationURL}SilverpopIcici',
                            data: $('#iciciBean').serialize(),
                            type: "Post",
                            success: function (result) {

//                                console.log("inactiveCounter", inactiveCounter);
//                                if (inactiveCounter > 1) {
//
//                                    $("#emailInactiveFlag").val('Y-' + (inactiveCounter - 1) + '');
//                                } else {
                                $("#emailInactiveFlag").val('Y');
//                                }

//                                console.log("-----2nd---->" + $("#emailInactiveFlag").val());
                            },
                            error: {
                            }
                        });
//                               console.log("silver pop is calling");
                        oldmobileno = mobile1;
                        //timer1= 7200000;

                        //},timer1);

                    } else
                    {
//                     console.log('Idle time is not yet meet!!!');
                        _idleSecondsCounter;
                    }
                    //clearTimeout(timers);

                }


            } else {
                clearInterval(myTimer);
//                console.log('TIMER IS STOPPED');
            }

        });




        $('#totalExperience').on("keypress", function (e) {
            var charCode;
            if (e.keyCode > 0) {
                charCode = e.which || e.keyCode;
            } else if (typeof (e.charCode) != "undefined") {
                charCode = e.which || e.keyCode;
            }
            if (charCode == 46)
                return true
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

        $.validator.addMethod('checkdecimal', function (value, element) {
            var tex = $("#totalExperience").val();

            if (tex.indexOf(".")) {
                var count1 = tex.split(".").length - 1;
                var before_decimal = tex.toString().split(".")[0];
                var after_decimal = tex.toString().split(".")[1];
                if ((before_decimal.length >= 3 && after_decimal.length == 1) || (before_decimal.length == 0 && after_decimal.length > 0) || after_decimal > 11 || (before_decimal.length > 2 && after_decimal.length == 0) || count1 > 1 && tex != null) {

                    return false;
                } else {
                    return true;
                }
            } else if (tex.length > 2) {
                return false;

            } else
            {
                return true;
            }
        });

        $('#pinCodeICICI').on("keydown", function (evt) {


            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;

            } else
                return true;
        });

        
        
        var startDate = new Date();
        startDate.setFullYear(startDate.getFullYear() - 65);
        var endDate = new Date();
        endDate.setFullYear(endDate.getFullYear() - 18);
        
//         $('#dateOfBirth').datepicker({dateFormat: 'mm-dd-yy', changeYear: true,
//             changeMonth: true, yearRange: '-83:-18'});
//         //cr2 milestone 1 
// // console.log("below"+'${agebelowrange}'+"----above"+'${ageaboverange}')


// //           alert("--> "+ start.getFullYear() - 45 );
//         var below = '${agebelowrange}';
//         var above = '${ageaboverange}';
//         if ((above != "" && below != "") && (above > 0 && below > 0)) {
//             var start1 = new Date();
//             start1.setFullYear(start1.getFullYear() - parseInt(below));
// //        alert("start -> "+start.getFullYear());
//             var end1 = new Date();
//             end1.setFullYear(end1.getFullYear() - parseInt(above));
            var loggedInJPnumber = $("#jetpriviligemembershipNumber").val();
            if (loggedInJPnumber != null) {

//       console.log("--> "+"start if -> "+start1.getFullYear() + ':' + end1.getFullYear());
//       alert("start if -> "+start.getFullYear() + ':' + end.getFullYear());
//       $( "#dateOfBirth" ).datepicker( "option", "yearRange", start1.getFullYear()+":"+end1.getFullYear() );
                $('#dateOfBirth').datepicker({
                    dateFormat: 'mm-dd-yy',
                    changeYear: true,
                    changeMonth: true,
                    onClose: function () {
//                alert('Datepicker Closed1');
                        if ($('#dateOfBirth').hasClass("ignore")) { 
                            $('#dateOfBirth').removeClass("ignore");
                        }
                        $("#iciciBean").validate().element('#dateOfBirth');
                    },
                    minDate: startDate,
                    maxDate: endDate,
                    yearRange: startDate.getFullYear() + ':' + endDate.getFullYear()
                });
            } else {
//       console.log("--> "+"start else -> "+start1.getFullYear() + ':' + end1.getFullYear());
//       alert("start else -> "+start.getFullYear() + ':' + end.getFullYear());
//        $( "#dateOfBirth" ).datepicker( "option", "yearRange", start1.getFullYear()+":"+end1.getFullYear() );
                $('#dateOfBirth').datepicker({
                    dateFormat: 'mm-dd-yy',
                    changeYear: true,
                    changeMonth: true,
                    onClose: function () {
//                alert('Datepicker Closed2');
                        if ($('#dateOfBirth').hasClass("ignore")) {
                            $('#dateOfBirth').removeClass("ignore");
                        }
                        $("#iciciBean").validate().element('#dateOfBirth');
                    },
                    minDate: startDate,
                    maxDate: endDate,
                    yearRange: startDate.getFullYear() + ':' + endDate.getFullYear()
                });
            }
        //}  
        
        //else {
            if (loginUser !== '') {

                var date = $("#dateOfBirth").val();

                var NDate = date.split("-").reverse()
                var d = new Date(NDate[0] + "/" + NDate[1] + "/" + NDate[2]);
//            var d = new Date(date.split("-").reverse());
                var dd = d.getDate();
                var mm = d.getMonth() + 1;
                var yy = d.getFullYear();
                var dbDate = yy + "/" + mm + "/" + dd;
                var date2 = new Date(dbDate);
                $("#dateOfBirth").datepicker({
                    dateFormat: 'mm-dd-yy',
                    onClose: function () {
//                alert('Datepicker Closed3');
                        if ($('#dateOfBirth').hasClass("ignore")) {
                            $('#dateOfBirth').removeClass("ignore");
                        }
                        $("#iciciBean").validate().element('#dateOfBirth');
                    },
                }).datepicker('setDate', date) //date2


            } else {
//            console.log("at else if not configure")
                var start = new Date();
                start.setFullYear(start.getFullYear() - 65);
                var end = new Date();
                end.setFullYear(end.getFullYear() - 18);
                $('#dateOfBirth').datepicker({
                    dateFormat: 'mm-dd-yy',
                    changeYear: true,
                    changeMonth: true,
                    onClose: function () {
//                alert('Datepicker Closed4');
                        if ($('#dateOfBirth').hasClass("ignore")) {
                            $('#dateOfBirth').removeClass("ignore");
                        }
                        $("#iciciBean").validate().element('#dateOfBirth');
                    },
                    minDate: start,
                    maxDate: end,
                    yearRange: start.getFullYear() + ':' + end.getFullYear()
                });

//            $('#dateOfBirth').datepicker({dateFormat: "mm-dd-yy", changeMonth: true,
//                changeYear: true, yearRange: '1900:2020', defaultDate: ''
//            });
            }
        //}
        //cr2 milestone 1 end
        $('#PpinCodeICICI').on("keydown", function (evt) {


            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;

            }
        });

        $('#OPincodeICICI').on("keydown", function (evt) {


            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;


            }
        });



        // new cr start


        $('#employmentType').change(function () {
//            $('.total_div').trigger('focusout');
            //--get value of employee status-----
            var textarea_value = $("#employmentType").val();

            if (textarea_value == "Salaried")
            {
                var flag = validate_month_sal();
                if (!flag && $("#monthlyIncome").val() == null)
                {

                    $("#monthlyIncome").focus();
//                        $("#monthlyIncome").select();
                    $("#monthlyIncome").focusout();
                    $("#monthlyIncome-error").show();

                } else
                {
//                    $("#monthlyIncome").focusout();
                    $("#monthlyIncome-error").hide();
//                    console.log("not error" );
                }

                $("#ICICIBankRelationShipNospan").hide();
                $('#companyName').prop('disabled', false);
                $("#companyNamespan").show();
                $("#companyName-error").show();
                $('#iciciBankRelationship').prop('disabled', false);
                $("#iciciBankRelationshipspan").show();
                $("#iciciBankRelationship-error").show();
                $('#totalExperience').prop('disabled', false);
                $("#totalExperiencespan").show();
                $("#totalExperience-error").show();
                $('#salaryBankWithOtherBankspan').show();
                $('#ITRStaus').prop('disabled', true).css("background-color", "#ebebe4");
                $("#ITRStausspan").hide();
                $("#ITRStaus-error").hide();
                $("#salaryAccountOpenedspan").show();
                $('#salaryAccountOpened').prop('disabled', false).css("background-color", "");
                $('#salaryAccountOpened').prop('disabled', false);
                $("#salaryAccountOpened-error").show();
                $("#ITRStaus").val('');
//                $("#iciciBankRelationship").val('');
//                $("#iciciBankRelationship").find('[value="Salary"]').show();
                $('select#iciciBankRelationship').html("")
                $('select#iciciBankRelationship').append('<option value>Select Bank Relationship</option><option value="Salary Account">Salary Account</option> <option value="Savings Account">Savings Account</option><option value="Loan Account">Loan Account</option><option value="No Relationship">No Relationship</option>')
                // code start here 

                $('#salaryBankWithOtherBank').val('');
                $('#salaryBankWithOtherBank').prop('disabled', false).css("background-color", "");
                $("#salaryBankWithOtherBankspan").show();
                $("#salaryBankWithOtherBank-error").show();

                $("#iciciBankRelationship").change(function () {

                    var textarea_value1 = $("#iciciBankRelationship").val();

                    if ((textarea_value1 == "Salary Account")) {


                        $("#ICICIBankRelationShipNospan").show();
                        $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(2).val()).attr("selected", "selected");
                        $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryAccountOpenedspan").show();
                        $("#salaryAccountOpened-error").show();
                        $('#salaryAccountOpened').prop('disabled', false).css("background-color", "");
//                        $('#salaryAccountOpened').prop('disabled', false);
                        $('#salaryAccountOpened').val('');

                        $('#ICICIBankRelationShipNo').prop('disabled', false).css("background-color", "");
                        $("#salaryBankWithOtherBankspan").show();
                        $("#salaryBankWithOtherBank-error").hide();
                        $("#ICICIBankRelationShipNospan").show();
                        $("#ICICIBankRelationShipNo-error").show();
                        $('#ICICIBankRelationShipNo').val('');

                    } else if ((textarea_value1 == "Loan Account") || (textarea_value1 == "Savings Account")) {


                        $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(1).val()).attr("selected", "selected");

                        var textarea_value1 = $('#salaryBankWithOtherBank option').eq(1).val();

//                        if (textarea_value1 == "Yes") {
//                          
//                            $('#salaryAccountOpened').prop('disabled', false).css("background-color", "");
//                            $("#salaryAccountOpenedspan").show();
//                            $("#salaryAccountOpened-error").show();
//
//                        } else {
//                             
//                            $('#salaryAccountOpened').prop('disabled', true).css("background-color", "#ebebe4");
//                            $("#salaryAccountOpenedspan").hide();
//                            $("#salaryAccountOpened-error").hide();
//                            $('#salaryAccountOpened').val('');
//                        }
                        //----------new add
                        $('#salaryAccountOpened').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryAccountOpenedspan").hide();
                        $("#salaryAccountOpened-error").hide();
                        $('#salaryAccountOpened').val('');
                        //end-----------------
                        $("#ICICIBankRelationShipNospan").hide();
                        $("#ICICIBankRelationShipNo-error").hide();
                        $('#ICICIBankRelationShipNo').prop('disabled', false).css("background-color", "");
                        $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryBankWithOtherBankdspan").show();
                        $("#salaryBankWithOtherBank-error").hide();
                        $('#ICICIBankRelationShipNo').val('');


                    } else {

                        $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(1).val()).attr("selected", "selected");
                        $("#ICICIBankRelationShipNod").show();
                        $("#ICICIBankRelationShipNospan").hide();

                        var textarea_value1 = $('#salaryBankWithOtherBank option').eq(1).val();

//                        if (textarea_value1 == "Yes") {
//                           
//                            $('#salaryAccountOpened').prop('disabled', false).css("background-color", "");
//                            $("#salaryAccountOpenedspan").show();
//                            $("#salaryAccountOpened-error").show();
//
//                        } else {
//                                 
//                            $('#salaryAccountOpened').prop('disabled', true).css("background-color", "#ebebe4");
//                            $("#salaryAccountOpenedspan").hide();
//                            $("#salaryAccountOpened-error").hide();
//                            $('#salaryAccountOpened').val('');
//                        }
                        //----new cr
                        $('#salaryAccountOpened').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryAccountOpenedspan").hide();
                        $("#salaryAccountOpened-error").hide();
                        $('#salaryAccountOpened').val('');
                        //---end---
                        $("#ICICIBankRelationShipNospan").hide();
                        $("#ICICIBankRelationShipNo-error").hide();
                        $('#ICICIBankRelationShipNo').prop('disabled', true).css("background-color", "#ebebe4");
                        $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4")
                        $("#salaryBankWithOtherBankdspan").show();
                        $("#salaryBankWithOtherBank-error").hide();
                        $('#ICICIBankRelationShipNo').val('');
                    }

                });
                //code end here
            } else {
                $('.total_div-error-div').addClass('hidden');
                var flag = validate_month_self();
//                console.log("flag ==> ",flag);

                if (!flag && $("#monthlyIncome").val() == null)
                {
                    $("#monthlyIncome").focus();
                    $("#monthlyIncome").focusout();
                    $("#monthlyIncome-error").show();
//           $("#monthlyIncome-error").focus();
//                    console.log("Show error ");

                } else
                {
//                    $("#monthlyIncome").focusout();
                    $("#monthlyIncome-error").hide();
//                    console.log("not error" );
                }
                $('#ITRStaus').prop('disabled', false).css("background-color", "");
                $("#ITRStausspan").show();
                $("#ITRStaus-error").show();
                $('#companyName').prop('disabled', false);
                $("#companyNamespan").show();
                $('#companyName-error').show();
                $('#iciciBankRelationship').prop('disabled', false);
                $('#totalExperience').prop('disabled', false);
                $("#totalExperiencespan").hide();
                $("#totalExperience-error").hide();
                $("#salaryAccountOpenedspan").hide();
                $('#salaryAccountOpened').prop('disabled', false).css("background-color", "");
                $('#salaryAccountOpened').prop('disabled', false);
                $("#salaryAccountOpened-error").show();
//                $("#iciciBankRelationship").val('');
//                $("#iciciBankRelationship").find('[value="Salary"]').hide();
                $('select#iciciBankRelationship').html("")
                $('select#iciciBankRelationship').append('<option value>Select Bank Relationship</option> <option value="Current Account">Current Account</option><option value="Savings Account">Savings Account</option><option value="Loan Account">Loan Account</option><option value="No Relationship">No Relationship</option>')
                // code start here 
                $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(2).val()).attr("selected", "selected");
                $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
                $("#salaryBankWithOtherBankspan").show();
                $("#salaryBankWithOtherBank-error").show();

                $("#iciciBankRelationship").change(function () {

                    var textarea_value1 = $("#iciciBankRelationship").val();

                    if ((textarea_value1 == "Loan Account") || (textarea_value1 == "Savings Account") || (textarea_value1 == "Current Account")) {

                        $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(2).val()).attr("selected", "selected");
                        $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryBankWithOtherBankspan").show();
                        $("#salaryBankWithOtherBank-error").show();
                        $("#ICICIBankRelationShipNospan").hide();
                        $("#ICICIBankRelationShipNo-error").hide();
                        $('#ICICIBankRelationShipNo').prop('disabled', false).css("background-color", "");
                        $('#ICICIBankRelationShipNo').val('');
                        $('#salaryAccountOpened').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryAccountOpenedspan").hide();
                        $("#salaryAccountOpened-error").hide();
                        $('#salaryAccountOpened').val('');

                    } else {

                        $('#salaryBankWithOtherBank').val($('#salaryBankWithOtherBank option').eq(2).val()).attr("selected", "selected");
                        $('#salaryBankWithOtherBank').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryBankWithOtherBankspan").show();
                        $("#salaryBankWithOtherBank-error").show();
                        $("#ICICIBankRelationShipNod").show();
                        $("#ICICIBankRelationShipNospan").hide();
                        $("#ICICIBankRelationShipNospan").hide();
                        $("#ICICIBankRelationShipNo-error").hide();
                        $('#ICICIBankRelationShipNo').prop('disabled', true).css("background-color", "#ebebe4");
                        $('#ICICIBankRelationShipNo').val('');
                        $('#salaryAccountOpened').prop('disabled', true).css("background-color", "#ebebe4");
                        $("#salaryAccountOpenedspan").hide();
                        $("#salaryAccountOpened-error").hide();
                        $('#salaryAccountOpened').val('');
                    }

                });
            }
        });
//new cr end




// aravind  code end
        if (loginUser !== '') {
            var pinCode = $('#pinCodeICICI').val();
            var cpNo = $("#cpNo").val();
            var cCity;
            var cState;
            if (pinCode != "" && pinCode.length == 6) {
                //$(".ui-menu-item").hide();
                $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
                    if (status == "success" && data != "excluded") {
                        $.get("${applicationURL}getCity1?pinCode=" + pinCode, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                var json = JSON.parse(data);
                                $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                    if (status == "success" && data.length != 2) {
                                        $("#city").val(json.city);
                                        $("#state1").val(json.state);
                                        $("#hcity").val(json.city);
                                        $("#hstate").val(json.state);
                                        $('#state1').attr("disabled", true);
                                        $('#city').attr("disabled", true);

                                        //$('.resedentialAddress-error-div').html('');
                                        //$('.resedentialAddress-error-div').addClass('hidden');

                                        CheckAddress('RES');

                                        if ($("#city").val() == null || $("#city").val() == '') {
                                            $('#city').empty();
                                            $('select#city').append('<option>' + json.city + '</option>');
                                        }
                                    } else {

                                        $('.resedentialAddress-error-div').html(pincodemsg);
                                        $('.resedentialAddress-error-div').removeClass('hidden');
                                        $("#city").val("");
                                        $("#state1").val("");
                                        $('#state1').attr("disabled", false);
                                        $('#city').attr("disabled", false);
                                    }
                                });
                            } else {
                                $('.resedentialAddress-error-div').html(invalidMsg);
                                $('.resedentialAddress-error-div').removeClass('hidden');
                                $("#city").val("");
                                $("#state1").val("");
                                //------change here---------
                                $('#state1').attr("disabled", true);
                                $('#city').attr("disabled", true);
                                //---------change end here------
                            }
                        });
                    } else {
                        $('.resedentialAddress-error-div').html(pincodemsg);
                        $('.resedentialAddress-error-div').removeClass('hidden');
                        $("#city").val("");
                        $("#state1").val("");
                    }
                });


            }

        }

        $("#salaryBankWithOtherBank").change(function () {

            var textarea_value1 = $("#salaryBankWithOtherBank").val();
        });

        $("#companyName").on("input keypress", function (e) {

            var company = $("#companyName").val();
            var emptype = $("#employmentType").val();
            var relationship = $("#iciciBankRelationship").val();


            var inp = $("#companyName").val();
            $.get("${applicationURL}companyNameByLetter?companyName=" + inp, function (data, status) {
                if (status = "success" && data != "") {

//                    console.log(data);
                    var test = data;
                    $("#companyName").autocomplete({
                        disabled: false,
//                            source:testarray
                        source: function (request, response) {
                            var matches = $.map(test, function (companyNameItem) {
                                if (companyNameItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
//                                    console.log("acItem-->" + companyNameItem);
                                    return companyNameItem;
                                }
                            });
                            response(matches);
                        }
                        ,

                    });
                }
            });
//                $.ajax({url: "https://www.m-icicibank.com/api/credit-cards/CC/GetCompany?prefix=[" + inp + "]", success: function (result) {
//
//                        $("#companyName").autocomplete({
//                            disabled: false,
//                            source: result,
//                            response: function (event, ui) {
//                                // ui.content is the array that's about to be sent to the response callback.
//                                $.validator.addMethod("nok", function (value, element) {
//
//                                    var temp = result.indexOf(value);
//                                    if (ui.content.length === 0 || temp == -1) {
//
//                                        return false;
//
//                                    } else
//                                    {
//                                        return true;
//
//                                    }
//
//                                }, "Company name in list should only be allowed.");
//
//
//                            }
//                        });
//                    }
//                });





//            } else {
//
//  
//                var inp = $("#companyName").val();
//
//                $.ajax({url: "https://www.m-icicibank.com/api/credit-card/CC/GetCompany?prefix=[" + inp + "]", success: function (result) {
//
//
//                        $("#companyName").autocomplete({
//                            disabled: true,
//                            source: result
//                        });
//                    }});
//
//            }

        });
        $("#salaryAccountOpened").on("focusout", function (e) {
            if (($("#employmentType").val() == "Salaried") && ($("#iciciBankRelationship").val() == "Salary Account"))
            {
                $("#salaryAccountOpened-error").hide();

            } else {
                $("#salaryAccountOpened-error").show();

            }
        });
        if (!$("#salaryBankWithOtherBankspan").is(':visible'))
        {
            $("#salaryAccountOpened-error").hide();

        }
        $.validator.addMethod("checklength", function (value, element) {
            var addr1 = $('#OAddress').val();
            var addr2 = $('#OAddress2').val();
            var addr3 = $('#OAddress3').val();

            if (addr1.length > 24) {
                return false;
            } else {
                return true;
            }

        });
//        $.validator.addMethod("minsalsav", function (value, element) {
//            var len = $("#ICICIBankRelationShipNo").val().length;
//            if (($("#iciciBankRelationship").val() == "Salary") || ($("#iciciBankRelationship").val() == "Saving")) {
//                if (len < 12) {
//                    return false;
//                } else {
//                    return true;
//                }
//            }
//        });
        $.validator.addMethod("minloan", function (value, element) {
            var len = $("#ICICIBankRelationShipNo").val().length;
            if (($("#iciciBankRelationship").val() == "Loan Account")) {
                if (len < 16) {
                    return false;
                } else {
                    return true;
                }
            }
        });

        $("#ICICIBankRelationShipNo").on("keyup", function (e) {
            var len = $("#ICICIBankRelationShipNo").val().length;
            if (($("#iciciBankRelationship").val() == "Salary Account") || ($("#iciciBankRelationship").val() == "Savings Account") || ($("#iciciBankRelationship").val() == "Current Account")) {
                $("#ICICIBankRelationShipNo").attr("maxlength", "12");

//                if (e.which == 8) {
//
//                }
//                else if (len >= 12) {
//                   
//                  
//                    
//                    e.preventDefault();
////                    $("#salaryAccountOpened").focus();
//                     if(e.keyCode==09 || e.keyCode==11){
//                      $("#salaryAccountOpened").focus();
//                    }
//                }

            }
            //------for loan
            if (($("#iciciBankRelationship").val() == "Loan Account")) {
                $("#ICICIBankRelationShipNo").attr("maxlength", "16");
//                if (e.which == 8) {
//                  
//                }
//                else if (len >= 16) {
//
//                    e.preventDefault();
////                    $("#salaryAccountOpened").focus();
//                    if(e.keyCode==09 || e.keyCode==11){
//                      $("#salaryAccountOpened").focus();
//                    }
//                }

            }

        });

//        cr code
        var a = '${SalariedIncome}';
        var b = '${SelfEmpIncome}';


        a = a.toString().replace("[", "").replace("]", "");
        b = b.toString().replace("[", "").replace("]", "");
        var totalsal = a * 12;
        var totalself = b * 12;
//var totalsal = a[0] * 12;
//        var totalself = b[0] * 12;
//        console.log("totalsal" + totalsal + "totalself" + totalself);

        function validate_month_self()
        {
            if (totalself != "") {
                var anualincome = $("#monthlyIncome").val();
                if (anualincome < totalself) {
//                        console.log("return true ");

//                        $("#monthlyIncome-error").show();
                    return false;
                } else {
//                        console.log("return false ");

                    return true;
                }
            }
        }
        function validate_month_sal()
        {
            if (totalsal != "") {
                var anualincome = $("#monthlyIncome").val();
                if (anualincome < totalsal) {
//                        console.log("return true ");
//                      $("#monthlyIncome").focus();
//                        $("#monthlyIncome-error").show();
                    return false;
                } else {
//                        console.log("return false ");

                    return true;
                }
            }
        }

        $.validator.addMethod("checkannualincomeSelf", function (value, element) {
            var anualincome = $("#monthlyIncome").val();
            var emptype = $("#employmentType").val();
            if (emptype != null && anualincome != null) {
                if (emptype == "Self Employed-Professional" || emptype == "Self Employed-Business") {
//                    console.log("inside self")

//                    console.log("=> anualincome ", anualincome, "totalself => ", totalself);
                    if (anualincome < totalself) {
//                        console.log("return true ");

                        $("#monthlyIncome-error").show();
                        return false;
                    } else {
//                        console.log("return false ");

                        return true;
                    }

                }
            }

        }, "Net Annual Income must be greater than or equal to INR " + totalself + "");
        $.validator.addMethod("checkannualincomeSal", function (value, element) {
            var anualincome = $("#monthlyIncome").val();
            var emptype = $("#employmentType").val();
            if (emptype != null && anualincome != null) {
                if (emptype == "Salaried") {
//                        console.log("inside sal")

//                        console.log("=> anualincome ", anualincome, "totalsal => ", totalsal);
                    if (anualincome < totalsal) {
//                            console.log("return true gg");
//                            $("#monthlyIncome").focus();
//                             $("#monthlyIncome").focusout();
//                            $("#monthlyIncome-error").show();
                        return false;
                    } else {
//                            console.log("return false ");
                        return true;
                    }

                }
            }

        }, "Net Annual Income must be greater than or equal to INR " + totalsal + "");
        $("#dateOfBirth").on("change", function () {
//            console.log("inside in dob change");
            var emp = $("#employmentType").val();
            var ci = $(".residentAddress .city").val();
//            console.log("emp" + emp + "ci" + ci);
            if (emp != "" || ci != "") {
                var c = $('.residentAddress .city').val();
                var dataageabove, dataagebelow;
//                console.log("notnull  1")
                $.get("${applicationURL}AgeCriteriaageabove?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + emp + "&City=" + c, function (data, status) {
                    if (status == "success") {
//                        console.log("-----status" + status + " data" + data);
                        dataageabove = data;
                    }
                    $.get("${applicationURL}AgeCriteriaagebelow?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + emp + "&City=" + c, function (data, status) {
                        if (status == "success") {
//                            console.log("-----status" + status + " data" + data);
                            dataagebelow = data;
                        }
//                        console.log("dataageabove " + dataageabove + " dataagebelow " + dataagebelow);
                        var a = getage();
//                        console.log("birthage" + a);
                        if (dataageabove != "" && dataagebelow != "" && a != "") {
//                 $("#dateOfBirth").focusout(function(){

                            if (a < dataageabove || a > dataagebelow) {
//                                console.log("show error dob");
//                                console.log("show error dob");
                                $('#dateOfBirth-error').html("You are not applicable for application");
                                $('#LoginValidation').html("You are not applicable for application");
                                $('#dateOfBirth-error').show();
                                $("#dateOfBirth-error").focus(); //new cr2
                                $("html, body").animate({scrollTop: 500}, 600);//new cr2
                                return false;//new cr2



                            } else {
//                               console.log("no error dob");
                                $('#dateOfBirth-error').html("");
                                $('#LoginValidation').html("");
//                          $(".age_error_div").focusin();
                                $('#dateOfBirth-error').hide();
                            }

//                  });

                        }
                    });
                });


            } else {
//                console.log("null  1")
            }
        });
        $("#employmentType").on("change", function () {
//            console.log("in change emp")
            var emp = $("#employmentType").val();
//            console.log("emp in 1" + emp)
            var ci = $('.residentAddress .city').val();
//            console.log("ci in 1" + ci);

            if (emp != null || $('.residentAddress .city').val() != "") {
                var c = $('.residentAddress .city').val();
                var dataageabove, dataagebelow;
//                console.log("notnull  1")
                $.get("${applicationURL}AgeCriteriaageabove?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + emp + "&City=" + c, function (data, status) {
                    if (status == "success") {
//                        console.log("-----status" + status + " data" + data);
                        dataageabove = data;
                    }
                    $.get("${applicationURL}AgeCriteriaagebelow?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + emp + "&City=" + c, function (data, status) {
                        if (status == "success") {
//                            console.log("-----status" + status + " data" + data);
                            dataagebelow = data;
                        }
//                        console.log("dataageabove " + dataageabove + " dataagebelow " + dataagebelow);
                        var a = getage();
//                        console.log("birthage" + a);
                        if (dataageabove != "" && dataagebelow != "" && a != "") {
                            $("#employmentType").focusout(function () {

                                if (a < dataageabove || a > dataagebelow) {
//                                    console.log("show error emp");
                                    $('#dateOfBirth-error').html("You are not applicable for application");
                                    $('#LoginValidation').html("You are not applicable for application");
                                    $('#dateOfBirth-error').show();
                                    $("#dateOfBirth-error").focus(); //new cr2
                                    $("html, body").animate({scrollTop: 500}, 600);//new cr2
                                    return false;//new cr2

                                } else {
//                                    console.log("no error emp");
                                    $('#dateOfBirth-error').html("");
                                    $('#LoginValidation').html("");
//                          $(".age_error_div").focusin();
                                    $('#dateOfBirth-error').hide();
                                }

                            });

                        }
                    });
                });


            } else {
//                console.log("null  1")
            }
        });
        $('.residentAddress').on("change", function () {
//            console.log("in city change")
            var ci = $('.residentAddress .city').val();
//            console.log("ci in 2" + ci);
            var emp = $("#employmentType").val();
//            console.log("emp in 2" + emp + "--" + emp.length);
            if ($("#employmentType").val() != "" && $('.residentAddress .city').val() != "") {
                var dataageabove, dataagebelow;
                var em = $("#employmentType").val();
//                console.log("notnull 2")
                $.get("${applicationURL}AgeCriteriaageabove?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + em + "&City=" + ci, function (data, status) {
                    if (status == "success") {
//                        console.log("-----status" + status + " data" + data);
                        dataageabove = data;
                    }

                    $.get("${applicationURL}AgeCriteriaagebelow?bpNo=" + 2 + "&cpNo=" +${cardBean.cpNo} + "&EmpType=" + em + "&City=" + ci, function (data, status) {
                        if (status == "success") {
//                            console.log("-----status" + status + " data" + data);
                            dataagebelow = data;
                        }
//                        console.log("dataageabove " + dataageabove + " dataagebelow " + dataagebelow);
                        var a = getage();
//                        console.log("birthage" + a);
                        if (dataageabove != "" && dataagebelow != "" && a != "") {
//                            console.log("-------!!!!---")
//                 $('.residentAddress').focusout(function(){

                            if (a < dataageabove || a > dataagebelow) {
//                                console.log("show error city");
                                $('#dateOfBirth-error').html("You are not applicable for application");
                                $('#LoginValidation').html("You are not applicable for application");
                                $('#dateOfBirth-error').show();
                                $("#dateOfBirth-error").focus(); //new cr2
                                $("html, body").animate({scrollTop: 500}, 600);//new cr2
                                return false;//new cr2

                            } else {
//                                console.log("no error city");
                                $('#dateOfBirth-error').html("");
                                $('#LoginValidation').html("");
//                          $(".age_error_div").focusin();
                                $('#dateOfBirth-error').hide();
                            }

//                  });

                        }
                    });
                });

            } else {
//                console.log("null  2")
            }
        });

//        if($(".ui-datepicker-div").is(':visible') == true){
//        $("#dateOfBirth-error").hide();
//        }
//$(".ui-datepicker-div").is(':visible', function(){ console.log("select")});
//$(".ui-datepicker-year").on('click', function(){ console.log("clicked")});
//$(".ui-datepicker-year").on('focus', function(){ console.log("focused")});


//         $(".ui-datepicker-month").on('focus select click', function(){
//         console.log("inside ui")
//            $("#dateOfBirth-error").hide();
//        });
        $(".datepicker_icon").on("click", function () {
            $("#dateOfBirth-error").remove();
        });
        $(".ui-datepicker-year").on("click", function () {
//            alert('TESt  2');
            $("#dateOfBirth-error").remove();
        });
        //resident-----------------   
        $("#pinCodeICICI").focus(function () {
            $('.resedentialAddress-error-div').addClass('hidden');
//         $('.resedentialAddress-error-div').text('');
        });
        $('.residentAddressField1').on("focus", function () {
            $('.resedentialAddress-error-div').addClass('hidden');
        });

        $('.residentAddressField2').on("focus", function () {
            $('.resedentialAddress-error-div').addClass('hidden');
        });
        $('.residentAddressField3').on("focus", function () {
            $('.resedentialAddress-error-div').addClass('hidden');
        });
        
        
        //company-----------------
        $("#OPincodeICICI").focus(function () {
            $('.companyAddr_div-error-div').addClass('hidden');
//         $('.resedentialAddress-error-div').text('');
        });

        $('.companyAddressField1').on("focus", function () {
            $('.companyAddr_div-error-div').addClass('hidden');
        });

        $('.companyAddressField2').on("focus", function () {
            $('.companyAddr_div-error-div').addClass('hidden');
        });
        $('.companyAddressField3').on("focus", function () {
            $('.companyAddr_div-error-div').addClass('hidden');
        });
        //permanent===========
        $("#PpinCodeICICI").focus(function () {
            $('.permanentAddr-error-div').addClass('hidden');
//         $('.resedentialAddress-error-div').text('');
        });

        $('.permanent_AddressField1').on("focus", function () {
            $('.permanentAddr-error-div').addClass('hidden');
        });

        $('.permanent_AddressField2').on("focus", function () {
            $('.permanentAddr-error-div').addClass('hidden');
        });
        $('.permanent_AddressField3').on("focus", function () {
            $('.permanentAddr-error-div').addClass('hidden');
        });
        
        $("#otpValue").on("keydown, keypress", function (e) {
            var charCode;
            if (e.keyCode > 0) {
                charCode = e.which || e.keyCode;
            } else if (typeof (e.charCode) != "undefined") {
                charCode = e.which || e.keyCode;
            }
            if (charCode == 46)
                return true
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;


        });
//    $("#city").on("focusout", function(){
//        console.log("city focusout")
//        $('.residentialAddr_div').trigger('focusout');
//        
//    });


        $("#address, #address2, #address3, #OAddress3, #OAddress2, #OAddress ,.permanent_AddressField3, .permanent_AddressField2, .permanent_AddressField1").on("keypress", function (e) {
//        alert("Called");    
            var len = $(this).val().length;
            if (e.which == 8)
            {

            } else if (len >= 40)
            {
                e.preventDefault();
                if (e.keyCode == 11) //e.keyCode==09 || 
                {
                    var inputs = $(this).closest('form').find(':input');
                    inputs.eq(inputs.index(this) + 1).focus();
                }
            }

        });

        var iciciFormNumber = '${formNumber}';
        $("#formNumber").val(iciciFormNumber);
        $(".error_msg_otp").addClass("hidden");

        
        $("#iciciBean").validate({
            //debug:true,
            ignore: ".ignore,:disabled,:hidden",
            rules: {

                fullName: {
                    required: true,
                    checkFullName: true
//                    nameValidation:true


                },
                dateOfBirth:
                        {
                            required: true

                        },
                gender:
                        {
                            required: true
                        },
                email:
                        {
                            required: true,
                            emailValidation: true
                        },
                mobile:
                        {
                            required: true,
                            digits: true,
                            mobileValidation: true,
                            maxlength: 10

                        },

                totalExperience:
                        {
                            required: {
                            	depends: function (element) {
                            	if (($("#employmentType").val() == "Salaried")) {
                            		return true;
                              }
                            	
                            }//                                 depends: function (element) {
//                                     return ($("#employmentType").val() == "Salaried");
//                                 },
                            }
                        },
                monthlyIncome:
                        {
                            required: true,
                            digits: true,
                            checkannualincomeSal:
                                    {
                                        depends: function (element) {
                                            if ($("#employmentType").val() == "Salaried" && $("#monthlyIncome").val() != "") {
//                                                console.log("return 111 true "); 

                                                return true;
                                            } else {
//                                                console.log("return 111 false ");
                                                return false;
                                            }
                                        }
                                    },
                            checkannualincomeSelf:
                                    {
                                        depends: function (element) {
                                            if (($("#employmentType").val() == "Self Employed-Professional" || $("#employmentType").val() == "Self Employed-Business") && $("#monthlyIncome").val() != "") {
//                                                console.log("return 111 true ");
                                                return true;
                                            } else {
//                                                console.log("return 111 false ");
                                                return false;
                                            }
                                        }
                                    }
                        },
                pancard:
                        {
                            required: true,
                            IciciPanValidation: true
                        },
                aadharNumber:
                        {
                            digits: true,
                            rangelength: [12, 12]
                        },
                resedentialAddress: {
//                     required: {
//                                depends: function (element) {
//                                    console.log("##########called Depends");
//                                  if ($('.residentAddressField1').val() == "") {
//                                     if($(".resedentialAddress-error-div").is(':visible') == true){
//                                      $('.resedentialAddress-error-div').addClass('hidden');
//                                    }
//                                  }
//                                  
//                                    return true;
//                                }
//                            }
                    required: true,
                    checkCurrentAddress: true,
//                    pincodelist:true


                },
                employmentType:
                        {

                            required: true

                        },
                ITRStaus:
                        {
                            required: true
                        },
                companyName:
                        {
                            required: true
//                                    {
//                                        depends: function (element) {
//                                            return ($("#employmentType").val() == "Salaried");
//                                        }
//                                    },

//                            nok: {
//                                depends: function (element) {
//                                    if ($("#employmentType").val() == "Salaried" && $("#iciciBankRelationship").val() == "Salary") {
////                                       
//                                        return true;
//                                    } else {
//                                        return false;
//                                    }
//                                },
//                            }

                        },
                iciciBankRelationship:
                        {
                            required: true

                        },

                ICICIBankRelationShipNo:
                        {

//                                    new cr start
                            required: {
                                depends: function (element) {
                                    if (($("#iciciBankRelationship").val() == "Salary Account")) {
//                                        alert('navneet')
//                                        $('html, body').animate({
//                                            scrollTop: eval($("#div_hide").offset().top-500)
//                                        }, 600);
                                        return true;
                                    } else {
                                        $("#ICICIBankRelationShipNo-error").hide();
                                        return false;
                                    }
                                }
                            },
                            minsal: {
                                depends: function (element) {
                                    if (($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Salary Account")) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                            },
                            minsav: {
                                depends: function (element) {
                                    if (($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Savings Account")) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                            },
                            mincur: {
                                depends: function (element) {
                                    if (($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Current Account")) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                            },
                            minloan: {
                                depends: function (element) {
                                    if (($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Loan Account")) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                            },
                            checkalphanumeric: {
                                depends: function (element) {
                                    if (($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Loan Account")) {
                                        return true;
                                    } else {
                                        return false;
                                    }

                                }
                            },
                            digits: {
                                depends: function (element) {

                                    if ((($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Salary Account"))) {
                                        return true;
                                    } else if ((($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Savings Account"))) {

                                        return true;
                                    } else if ((($("#ICICIBankRelationShipNo").val() != "") && ($("#iciciBankRelationship").val() == "Current Account"))) {

                                        return true;
                                    } else {
                                        return false;
                                    }

                                }
                            },
                        },
                salaryBankWithOtherBank:
                        {
                            required: true
                        },
                salaryAccountOpened:
                        {
                            required:
                                    {
                                        depends: function (element) {
                                            return ($("#employmentType").val() == "Salaried");
                                        }
                                    }
//                            required: {
//                            depends:function(){
//                                if(($("#employmentType").val() == "Salaried") && ($("#iciciBankRelationship").val() == "Salary") ){
//                                    return false;
//                                }else{
//                                    return true;
//                                }
//                            }}
                        },
//  
                companyAddress: {
                    required: true,
                    checkCompanyAddress: true,
                    minlength: 3
                },
                homeNumber: {
                    required: {
                        depends: function (element) {
                            return ($("#OPhone").val() == "");
                        }
                    },
                },
                termsAndConditions:
                        {
                            required: true
                        }
            },

            messages: {

                fullName: {
                    required: "Please Enter Full Name",
                    checkFullName: ""
//                    nameValidation: "Name can have only letters"



                },
                dateOfBirth: {
                    required: "Please Select Date of Birth"
//                     checkdateOfBirth:"Age should between 18 to 65"
                },
                gender: {
                    required: "Please Select Gender"
                },
                email: {
                    required: "Please Enter E-mail ID",
                    emailValidation: "E-mail ID is not valid"
                },
                mobile: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid",
                    mobileValidation: "Mobile Number is not valid",
                    maxlength: "Please ensure mobile number is limited to 10 digits"
                },
                eduQualification: {
                    //required: "Please Select Qualification"
                },
                totalExperience: {
                    required: "Please Enter Total Work Experience"
                },
                monthlyIncome: {
                    required: "Please Enter Your Net Annual Income (Excluding Bonus Or Incentives)",
                    digits: "Net Annual Income is not Valid",
                    // monthlyValidation: "Net Annual Income must be greater than or equal to 6 lakhs."
                },
                pancard: {
                    required: "Please Enter Pancard Number",
                    IciciPanValidation: "Please Enter a valid PAN Number"
                },
                aadharNumber:
                        {
                            digits: "Aadhaar Number is not valid",
                            rangelength: "Maximum of 12 characters permitted for Aadhaar Number"
                        },
                resedentialAddress: {
                    required: "Please provide your Current Residential Address",
                    checkCurrentAddress: "Please provide your Current Residential Address"

                },
                employmentType: {
                    required: "Please Select Employment Type"
                },
                companyName: {
                    required: "Please Enter Company Name"

                },
                ITRStaus:
                        {
                            required: "Please Select ITR Status"
                        },
                iciciBankRelationship:
                        {
                            required: "Please select ICICI Bank Relationship"
                        },
                ICICIBankRelationShipNo:
                        {
                            required: "Please enter ICICI Account Number",
                            digits: "Please provide Numeric values",
                            minsal: "Please Enter only 12-digit Numeric values",
                            minsav: "Please Enter only 12-digit Numeric values",
                            mincur: "Please Enter only 12-digit Numeric values",
                            minloan: "Please Enter only 16-character Alphanumeric values",
//                          minLength:"Please Enter only 12-digit Numeric values",
//                          maxlength:"Please Enter only 16-character Alphanumeric values",
                            checkalphanumeric: "Please provide Alphanumeric values"
                        },
                salaryBankWithOtherBank:
                        {
                            required: "Please select Salary Account With Other Bank"
                        },
                salaryAccountOpened:
                        {
                            required: "Please select Salary Account Opened"
                        },

                companyAddress: {
                    required: "Please provide your Company Address",
                    checkCompanyAddress: "",
                    minlength: "minimum length is 3"
                },
                homeNumber: {
                    required: "Please enter Home Landline number and must have exact 11 digits including STD code",
                },
                termsAndConditions: {
                    required: "Please agree to be contacted by ICICI Bank"
                }

            },
            onfocusout: function (element) {
                this.element(element);  // <- "eager validation"
                if ($('#dateOfBirth').hasClass("ignore")) {
                    $('#dateOfBirth').removeClass("ignore");
                }
            },
            errorElement: "div",
            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },
            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                if ($(element).hasClass("datepicker1 row margT1 input hasDatepicker")) {
                    error.insertAfter($(".datepicker_icon"));
                } else if ($(element).hasClass("margT1 statement")) {
                    error.insertAfter($(element).closest("div"));
                } else if ($(element).hasClass("row margT1 input name_inputBox")) {
                    error.insertAfter($(".name_div"));
                } else if ($(element).hasClass("row margT1 input currentResidentialAddress")) {
                    error.insertAfter($(".residentialAddr_div"));
                }else if ($(element).hasClass("row margT1 input permanentResidentialAddress")) {
                    error.insertAfter($(".permanentAddr_div"));
                }
                else if ($(element).hasClass("row margT1 input companyAddress_box")) {
                    error.insertAfter($(".companyAddr_div"));
                } else if ($(element).hasClass("row margT1 input homeNumber")) {
                    error.insertAfter($(".phoneNum_div"));
                }//                 new cr start
                else if ($(element).hasClass("row margT1 input totalExperience")) {
                    error.insertAfter($(".total_div"));
                }
//                 new cr end
                else {
                    error.insertAfter(element);
                }

                //Adobe code starts here(error tracking changes)
                errorTrack = errorTrack + error.text() + ",";
                console.log("--errorTrack--", errorTrack)
                //Adobe code ends here
            },
            submitHandler: function (form) {
//                $('#resedentialAddress-error').css('display', 'none');
                $('#city').prop('disabled', false);
                $('#state1').prop('disabled', false);
                $('#PCity').prop('disabled', false);
                $('#PState').prop('disabled', false);
                $('#OCity').prop('disabled', false);
                $('#OState').prop('disabled', false);
                $('.about .error_box').addClass('hidden');
                $("#salaryBankWithOtherBank").prop('disabled', false);
                $("#promocode").trigger("focusout");
                var residentAddressField1 = $('.residentAddressField1').val();
                var residentAddressField2 = $('.residentAddressField2').val();
                var residentAddressField3 = $('.residentAddressField3').val();

//                console.log("adderss length"+$('#address').val().length+ "  "+$('#address2').val().length+ " "+$('#address3').val().length);
                if ($('#address').val().length >= 40 || $('#address2').val().length >= 40 || $('#address3').val().length >= 40) {
//        console.log("inside if condition");
                    $('.resedentialAddress-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
                    $('.resedentialAddress-error-div').removeClass('hidden');

                } else {
//              console.log("inside else condition");
                    $('.resedentialAddress-error-div').html('');
                    $('.resedentialAddress-error-div').addClass('hidden');
                }

//          console.log("adderss length"+$('#OAddress3').val().length+ "  "+$('#OAddress3').val().length+ " "+$('#OAddress').val().length);
                if ($('#OAddress').val().length >= 40 || $('#OAddress2').val().length >= 40 || $('#OAddress3').val().length >= 40) {
//        console.log("inside if condition");
                    $('.companyAddr_div-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
                    $('.companyAddr_div-error-div').removeClass('hidden');

                } else {
                    console.log("inside else conditionlllllllllllllllll");
                    $('.companyAddr_div-error-div').html('');
                    $('.companyAddr_div-error-div').addClass('hidden');
                }
                
                //$('.resedentialAddress-error-div').removeClass('hidden');
                //alert($(".resedentialAddress-error-div").is(':hidden'));
//                 if($(".resedentialAddress-error-div").text != '')
//                 { 
//                   //$('.resedentialAddress-error-div').removeClass('hidden');
//                 	$('html,body').stop(true, false).animate({scrollTop: $('.currentResAddress').offset().top - 150}, 'fast');
//                     return false;
//                 }

//                 if ($('.residentAddressField1').val() == "" || $('.residentAddressField2').val()=="") {
//                     console.log("##########in if")
//                       if($(".resedentialAddress-error-div").is(':visible') == true){
//                        console.log("Inside ELSE **********************");
//                         $('.resedentialAddress-error-div').addClass('hidden');
//                       }
//                 }


			var add1 = $('.residentAddressField1').val();
            var add2 = $('.residentAddressField2').val();
            var add3  = $('.residentAddressField3').val();
           
            var lResAddrss = add1;//$('#resedentialAddress').val()
            var bRValue = /^[A-Za-z0-9#',\/\s]*$/.test(lResAddrss)

            if (!bRValue)
            {
            	$('html,body').stop(true, false).animate({scrollTop: $('.currentResAddress').offset().top - 150}, 'fast');
            	 return false;
            }

            var bRValue2 = /^[A-Za-z0-9#',\/\s]*$/.test(add2)

            if (!bRValue2)
            {
            	$('html,body').stop(true, false).animate({scrollTop: $('.currentResAddress').offset().top - 150}, 'fast');
            	 return false;
            }
            
            var bRValue3 = /^[A-Za-z0-9#',\/\s]*$/.test(add3)

            if (!bRValue3)
            {
            	$('html,body').stop(true, false).animate({scrollTop: $('.currentResAddress').offset().top - 150}, 'fast');
            	 return false;
            }



                if ($("#jetpriviligemembershipNumber").val() != "") {
                    $("#jpNumber").val($("#jetpriviligemembershipNumber").val());
                    $("#jpTier").val($("#jetpriviligemembershipTier").val());
                }

                var radioValue = $(".addressProof input[type='radio']:checked").val();
                if (radioValue == "No") {
//                    console.log('inside No'+radioValue);

                    var residentAddressField1 = $('.permanent_AddressField1').val();
                    var pin = $('.permanent_residentAddress .pincodeICICI').val();
                    var city = $('.permanent_residentAddress .city option:selected').text();
                    var state = $('.permanent_residentAddress .state option:selected').text();
//                    if (residentAddressField1 == "" || pin == "" || city == "" || state == "") {
//                        $('.permanentAddr-error-div').removeClass('hidden');
//                        $(".permanentAddr-error-div").html("Please Enter Permanent Residential Address");
//                        $('html,body').stop(true, false).animate({scrollTop: $('.permanentAddr-error-div').offset().top - 150}, 'fast');
//                        return false;
//                    }
                    checkpermanent();
                } else if (radioValue == "Yes") {

//                    console.log('inside YES'+radioValue);
//  					$('.permanentAddr_div').css('display','none');
                    $('.permanentAddr-error-div').html("");
                    $('.permanentAddr-error-div').addClass('hidden');
                    $(".permanent_AddressField1").val($(".residentAddressField1").val());
                    $(".permanent_AddressField2").val($(".residentAddressField2").val());
                    $(".permanent_AddressField3").val($(".residentAddressField3").val());
                    $('.permanent_residentAddress .pincodeICICI').val($('.residentAddress .pincodeICICI').val());
                    $('.permanent_residentAddress .city').val($('.residentAddress .city option:selected').text());
                    $('.permanent_residentAddress .state').val($('.residentAddress .state option:selected').text());

                }

                var fName = $('.firstName').val();
                var mName = $('.middleName').val();
                var lName = $('.lastName').val();
                var msg = "Please ensure your keyword limited to 26 characters";
                var fNameLength = fName.length;
                var lNameLength = lName.length;
                var mNameLength = mName.length;
                var appendQuery = fNameLength > 26 && lNameLength > 26 && mNameLength > 26 ? "First Name, Middle Name, Last Name" : fNameLength > 26 && lNameLength > 26 ? "First Name, Last Name" : fNameLength > 26 && mNameLength > 26 ? "First Name, Middle Name" : mNameLength > 26 && lNameLength > 26 ? "Middle Name, Last Name" : fNameLength > 26 ? "First Name" : mNameLength > 26 ? "Middle Name" : lNameLength > 26 ? "Last Name" : "";
                msg = msg.replace('keyword', appendQuery);




                if (!hasalphabet(fName) || !hasalphabet(lName)) {
                    $('.fullName-error-div').html("Name can have only letters");
                    $('.fullName-error-div').removeClass('hidden');
                    $('html,body').stop(true, false).animate({scrollTop: $('.fullName').offset().top - 150}, 'fast');
                    return false;
                } else if (mName != "" && !hasalphabet(mName)) {
                    $('.fullName-error-div').html("Name can have only letters");
                    $('.fullName-error-div').removeClass('hidden');
                    $('html,body').stop(true, false).animate({scrollTop: $('.fullName').offset().top - 150}, 'fast');
                    return false;
                }
                
//                 if($(".resedentialAddress-error-div").text != '')
//                 {
//                 	return false;	
//                 }


                if (fNameLength > 26 || mNameLength > 26 || lNameLength > 26) {

                    $('.fullName-error-div').html(msg);
                    $('.fullName-error-div').removeClass('hidden');
                    $('html,body').stop(true, false).animate({scrollTop: $('.fullName').offset().top - 150}, 'fast');
                    return false;
                }
//                console.log("@@@@@@@@@@@@ R @", $(".resedentialAddress-error-div").text(), "<=length=>", $(".resedentialAddress-error-div").text().length)

                if ($(".resedentialAddress-error-div").is(':hidden') == false) {
//                    console.log("in not hidden r")

                    $('html,body').stop(true, false).animate({scrollTop: $('.currentResAddress').offset().top - 150}, 'fast');
                    return false;
                }
                
//                console.log("@@@@@@@@@@@@ p @", $(".permanentAddr-error-div").text(), "<=length=>", $(".permanentAddr-error-div").text().length)

                if ($(".permanentAddr-error-div").is(':hidden') == false) {
//                    console.log("in not hidden p")
                    $('html,body').stop(true, false).animate({scrollTop: $('.permanentAddr').offset().top - 150}, 'fast');
                    return false;
                }
//                console.log("@@@@@@@@@@@@", $(".companyAddr_div-error-div").text(), "<=length=>", $(".companyAddr_div-error-div").text().length)

                if ($(".companyAddr_div-error-div").is(':hidden') == false) {
//                    console.log("in not hidden c")
                    $('html,body').stop(true, false).animate({scrollTop: $('.companyAddr').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".homeNumber-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.homeNumber-error-div').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".officeNumber-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.officeNumber-error-div').offset().top - 150}, 'fast');
                    return false;
                }
                if ($(".promocodeError").text() != "") {
//                    console.log("inside error function");
                    $('html,body').stop(true, false).animate({scrollTop: $('.promocodeError').offset().top - 150}, 'fast');
                    return false;
                }
                //                 new cr start
                if ($(".total_div-error-div").text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.total_div-error-div').offset().top - 150}, 'fast');
                    return false;
                }
                if ($('.age_error_div').text() != "") {
                    $('html,body').stop(true, false).animate({scrollTop: $('.age_error_div').offset().top - 150}, 'fast');
                    return false;
                }

//                 new cr end
                var offerContent = '${cardBean.offerDesc}';

                if ($("#jpNumber").val() !== "") {
//                    console.log("jpnumber present")
                    $('#otpButton').attr('disabled', false);
                    $('#jpNumpopup').attr('disabled', true);
                    if ($("#promocode").val() != "" && $("#promocodeError").text() == "") {

                        if ($("#promocode").val() == '${defaultPromocode}') {
                            $("#jpNumber").attr("disabled", false);
                            $("#quickEnroll").attr("disabled", false);
                            $("a.enroll_here").on("click", callenroll);
                            $('#quickEnroll').css('cursor', 'pointer');

//                            $("#jpNumber").attr("title","");
                            $(".promoinfo").addClass('hidden');


                        } else {


                            $("#jpNumber").attr("disabled", true);
                            $('#quickEnroll').css('cursor', 'default');
//                        $("jpNumber").attr("title","Promocode applied successfully")
                            $("#quickEnroll").attr("disabled", true);
//                        $('#quickEnroll').removeAttr('href').css('cursor','default');
                            $("a.enroll_here").off("click", callenroll);
                            $(".promoinfo").removeClass('hidden');
                            $('.enroll_popup').addClass('hidden');
                            $('.jpNum_popup').removeClass('hidden');
                        }

                    } else {
                        $("#jpNumber").attr("disabled", false);
//                            $('.tooltips').hide();
                        $("#quickEnroll").attr("disabled", false);
                        $('#quickEnroll').css('cursor', 'pointer');
                        $("a.enroll_here").on("click", callenroll);

                        $(".promoinfo").addClass('hidden');


                    }
                } else {
                    $("a.enroll_here").on("click", callenroll);
                    $(".promoinfo").addClass('hidden');


                }
if (beJpNumber != "") {
                console.log("random number",$("#randomNumber").val());
$.get("$ {applicationURL}checkFormStatus?randomNumber=" + $("#randomNumber").val() , function (data, status) {
                        if(status == "success" && data !=""){
                            console.log("data===>",data);
                            if(data  != "Completed"){
                                                           $("#CompletedForm").modal('hide');

                    $("#provide_jpNumber").modal('hide');
                    $('.loaddiv').css({"bottom": "-57px", "position": "fixed"});
                    $('#sad').show();
                    form.submit();   
                    }else{
                                                                $("#CompletedForm").modal('show');

                            }
                        }
                    })
                } else {


//start
$.get("${applicationURL}checkFormStatus?randomNumber=" + $("#randomNumber").val() , function (data, status) {
                                    console.log("data1===>",data);
                
//        if(status == "success" && data !=""){
                            console.log("data===>",data);
                            if(data  != "Completed"){
                                console.log("inside if")
                $("#provide_jpNumber").modal('show'); 
                $('.error_box').addClass('hidden');
                $('.error_box2').addClass('hidden');
                
                $("#jpNumpopup").on('click', function () { 
                	//validateOTPOnSubmit();
                	if ($("#jpNumber").val() == "") {
//                        alert("test 1");
                        $('#jpNumpopup').attr('disabled', true);
                        $('#jpNumpopup').removeClass('genebutton-HDFC');
                        $('#jpNumpopup').addClass('genebutton_HDFC');
                    } else if ($("#jpNumber").val() != "" && $("#jetpriviligemembershipTier").val() != "") {

                        $(".geninput").trigger('keyup');
                        
//                         if ($(".geninput").val() != "" && isCorrectOtp == true) {
						   if ($(".geninput").val() != "") {
                            $("#otpVal").val($(".geninput").val());
                            //form.submit();
                            
//                             $('#jpNumpopup').attr('disabled', true);
//                             $('#jpNumpopup').removeClass('genebutton-HDFC');
//                             $('#jpNumpopup').addClass('genebutton_HDFC');
                            //validateOTPOnSubmit();
                            
                            ////////////////////////////
                            
                            if ($("#otpValue").val().length == 6) {
  	              var otpValue = $("#otpValue").val();
  	              if (otpValue == "") {
  	                  console.log("1");
  	                  $("#otpError").html("Please Enter OTP");
  	                  $("#otpError").show();
  	                  isCorrectOtp = false;
  	              } else if (otpValue.length != 6) {
  	                  console.log("2");
  	                  $("#otpError").html("Please Enter OTP");
  	                  $("#otpError").show();
  	                  isCorrectOtp = false;
  	              } else {
  	                  if (isNaN(otpValue) == true) {
  	                      console.log("3");
  	                      $("#otpError").html("Please Enter OTP");
  	                      $("#otpError").show();
  	                      isCorrectOtp = false;
  	                  } else {
         	
         	
         	var formNumber = '${formNumber}';
              var mobileNumber = $('#mobile').val();
                     var otpTransactionId = $("#otpTransactionId").val();
                     $.ajax({method: "GET", 
                         async: false,
                         url: "${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&otpTransactionId=" + otpTransactionId + "&otp=" + otpValue + "&token=" + $("#token").val(), success: function (data) 
                       {
                             console.log("=========validate======", data);

//                     $.get("${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + 2 + "&formNumber=" + formNumber, function (data, status) {
                             if (data.length > 2) {
                                 var myObj = JSON.parse(data);

                                 pendingAttempt = myObj.pendingAttempts;
                                 $("#pendingAttempt").val(myObj.pendingAttempts);
                                 console.log("pending attempt"+$("#pendingAttempt").val())
                                 if ($("#pendingAttempt").val() == "Number of attempts exceeds limit") {
                                     $(".geninput").attr('disabled', true);
                                     $(".geninput").val("");
                                     $(".error_msg_otp").html(""); 
                                     $(".error_msg_otp").hide();
                                     successMsg="Left attempts are exceeded";
//                                     $('#otpSucces').html("Number of attempts exceeds limit");
                                     $('#otpSucces').removeClass('hidden');
//                                      $('#otpButton').attr('disabled', true);
//                                      $(".otpButton").addClass("hidden");
                                     
                                     $('#jpNumpopup').attr('disabled', true);
//                                      $("#jpNumpopup").css("color", "#ddd");
 	                                $('#jpNumpopup').removeClass('genebutton-HDFC');
 	                                $('#jpNumpopup').addClass('genebutton_HDFC');
                                     
 	                                var imageUrl = ctx + "static/images/icons/error_icon.png";
                                     $(".otpsuccess").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
                                 } else if ($("#pendingAttempt").val() != "") {
                                     console.log("=-=-=p-==-=-=>", $("#pendingAttempt").val());
                                     successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left ' + $("#pendingAttempt").val();

                                 } else {
                                     successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                 }
                                 $('#otpSucces').html(successMsg);

                                 if (myObj.OTP_Verified == "true") {
                                	 $(".error_msg_otp").html("");
                                     $(".error_msg_otp").addClass("hidden");
                                     
                                   setTimeout(disableElement, 5*60*1000);
                                   function disableElement(){
                                 	 $('#jpNumpopup').attr('disabled', false);
	  	                            $('#jpNumpopup').removeClass('genebutton-HDFC');
	  	                            $('#jpNumpopup').addClass('genebutton_HDFC');	
                                 	}
                                     isCorrectOtp = true;
                                     $('#otpVal').val(otpValue);
                                     //submitAPI();
                                     ////////
                                     $("#provide_jpNumber").modal('hide');
                        	    	  $("#OtpSuccPopup").modal('show');
                        	    	 setTimeout(disableSuccPopup, 10000);
                        	          function disableSuccPopup(){
                        	        	  $("#OtpSuccPopup").modal('hide');
                        	        	  form.submit();
                        	        	}  
                        	          ///////
                                     
                                     
                                 } else {
                                     $(".error_msg_otp").html("Please Enter Valid OTP");
                                     $(".error_msg_otp").removeClass("hidden");
                                     $('#jpNumpopup').attr('disabled', true);
//                                      $("#jpNumpopup").css("color", "#ddd");
										$('#jpNumpopup').removeClass('genebutton-HDFC');
 	  	              					$('#jpNumpopup').addClass('genebutton_HDFC');
                                     isCorrectOtp = false;
                                     $('#otpVal').val('');
                                 }
                             } else {
                                 $(".error_msg_otp").html("Please Enter Valid OTP");
                                 $(".error_msg_otp").removeClass("hidden");
                                 $('#jpNumpopup').attr('disabled', true);
//                                  $("#jpNumpopup").css("color", "#ddd");
 								$('#jpNumpopup').removeClass('genebutton-HDFC');
 	  	              			$('#jpNumpopup').addClass('genebutton_HDFC');
                                 isCorrectOtp = false;
                             }

                         }});
                        }
                     }
  	         }else{
  	        	 $(".error_msg_otp").html("Please Enter Valid OTP");
                  $(".error_msg_otp").removeClass("hidden");
                  $('#jpNumpopup').attr('disabled', true);
//                   $("#jpNumpopup").css("color", "#ddd");
 					$('#jpNumpopup').removeClass('genebutton-HDFC');
 	  	            $('#jpNumpopup').addClass('genebutton_HDFC');
                  isCorrectOtp = false;
  	         }
                            
                            ///////////////////////////
                            
                        }

                    } else {
//                         alert("test 3");
                        $("#jpNumber").trigger('keyup');
                        if ($("#jpNumber").val() != '' && isCorrectJpNumber) {
//                            alert("test 4")
                            $(".geninput").trigger('keyup');
//                             if ($(".geninput").val() != "" && isCorrectOtp == true) {
								if ($(".geninput").val() != "") {
                                $("#otpVal").val($(".geninput").val());
                                //form.submit();
//                                 $('#jpNumpopup').attr('disabled', true);
//                                 $('#jpNumpopup').removeClass('genebutton-HDFC');
//                                 $('#jpNumpopup').addClass('genebutton_HDFC');
                                //validateOTPOnSubmit();
                             //////////////////////////////////////////////////   
                                
                                if ($("#otpValue").val().length == 6) {
  	              var otpValue = $("#otpValue").val();
  	              if (otpValue == "") {
  	                  console.log("1");
  	                  $("#otpError").html("Please Enter OTP");
  	                  $("#otpError").show();
  	                  isCorrectOtp = false;
  	              } else if (otpValue.length != 6) {
  	                  console.log("2");
  	                  $("#otpError").html("Please Enter OTP");
  	                  $("#otpError").show();
  	                  isCorrectOtp = false;
  	              } else {
  	                  if (isNaN(otpValue) == true) {
  	                      console.log("3");
  	                      $("#otpError").html("Please Enter OTP");
  	                      $("#otpError").show();
  	                      isCorrectOtp = false;
  	                  } else {
         	
         	
         	var formNumber = '${formNumber}';
              var mobileNumber = $('#mobile').val();
                     var otpTransactionId = $("#otpTransactionId").val();
                     $.ajax({method: "GET", 
                         async: false,
                         url: "${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&otpTransactionId=" + otpTransactionId + "&otp=" + otpValue + "&token=" + $("#token").val(), success: function (data) 
                       {
                             console.log("=========validate======", data);

//                     $.get("${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + 2 + "&formNumber=" + formNumber, function (data, status) {
                             if (data.length > 2) {
                                 var myObj = JSON.parse(data);

                                 pendingAttempt = myObj.pendingAttempts;
                                 $("#pendingAttempt").val(myObj.pendingAttempts);
                                 console.log("pending attempt"+$("#pendingAttempt").val())
                                 if ($("#pendingAttempt").val() == "Number of attempts exceeds limit") {
                                     $(".geninput").attr('disabled', true);
                                     $(".geninput").val("");
                                     $(".error_msg_otp").html("");
                                     $(".error_msg_otp").hide();
                                     successMsg="Left attempts are exceeded";
//                                     $('#otpSucces').html("Number of attempts exceeds limit");
                                     $('#otpSucces').removeClass('hidden');
//                                      $('#otpButton').attr('disabled', true);
//                                      $(".otpButton").addClass("hidden");
                                     
                                     $('#jpNumpopup').attr('disabled', true);
//                                      $("#jpNumpopup").css("color", "#ddd");
 	                                $('#jpNumpopup').removeClass('genebutton-HDFC');
 	                                $('#jpNumpopup').addClass('genebutton_HDFC');
                                     
 	                                var imageUrl = ctx + "static/images/icons/error_icon.png";
                                     $(".otpsuccess").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
                                 } else if ($("#pendingAttempt").val() != "") {
                                     console.log("=-=-=p-==-=-=>", $("#pendingAttempt").val());
                                     successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left ' + $("#pendingAttempt").val();

                                 } else {
                                     successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                 }
                                 $('#otpSucces').html(successMsg);

                                 if (myObj.OTP_Verified == "true") {
                                	 $(".error_msg_otp").html("");
                                     $(".error_msg_otp").addClass("hidden");
                                     
                                   setTimeout(disableElement, 5*60*1000);
                                   function disableElement(){
                                 	 $('#jpNumpopup').attr('disabled', false);
	  	                            $('#jpNumpopup').removeClass('genebutton-HDFC');
	  	                            $('#jpNumpopup').addClass('genebutton_HDFC');	
                                 	}
                                     isCorrectOtp = true;
                                     $('#otpVal').val(otpValue);
                                     //submitAPI();
                                     
                                     ///////////
                                     $("#provide_jpNumber").modal('hide');
                        	    	  $("#OtpSuccPopup").modal('show');
                        	    	 setTimeout(disableSuccPopup, 10000);
                        	          function disableSuccPopup(){
                        	        	  $("#OtpSuccPopup").modal('hide');
                        	        	  form.submit();
                        	        	 //$("#iciciBean").submit();
                        	        	}  
                                     ///////////
                                     
                                 } else {
                                     $(".error_msg_otp").html("Please Enter Valid OTP");
                                     $(".error_msg_otp").removeClass("hidden");
                                     $('#jpNumpopup').attr('disabled', true);
//                                      $("#jpNumpopup").css("color", "#ddd");
										$('#jpNumpopup').removeClass('genebutton-HDFC');
 	  	              					$('#jpNumpopup').addClass('genebutton_HDFC');
                                     isCorrectOtp = false;
                                     $('#otpVal').val('');
                                 }
                             } else {
                                 $(".error_msg_otp").html("Please Enter Valid OTP");
                                 $(".error_msg_otp").removeClass("hidden");
                                 $('#jpNumpopup').attr('disabled', true);
//                                  $("#jpNumpopup").css("color", "#ddd");
 								$('#jpNumpopup').removeClass('genebutton-HDFC');
 	  	              			$('#jpNumpopup').addClass('genebutton_HDFC');
                                 isCorrectOtp = false;
                             }

                         }});
                        }
                     }
  	         }else{
  	        	 $(".error_msg_otp").html("Please Enter Valid OTP");
                  $(".error_msg_otp").removeClass("hidden");
                  $('#jpNumpopup').attr('disabled', true);
//                   $("#jpNumpopup").css("color", "#ddd");
 					$('#jpNumpopup').removeClass('genebutton-HDFC');
 	  	            $('#jpNumpopup').addClass('genebutton_HDFC');
                  isCorrectOtp = false;
  	         }
                                
                                
                                
                            }
                        }
                    }
                    if ($(".error_msg_otp").is(':visible') == true) {
                        $('.loaddiv').css("bottom", "0px");
                        $('#icicidisable').hide();
                    } else {
                        $('.loaddiv').css({"bottom": "-57px", "position": "fixed"});
                        ;
                        console.log("data in call loader me");
                    	console.log("this loader is runnung");
                    	alert("sorry please loadr try again");
                        $('#icicidisable').show();
                    }

                    //Adobe code starts here
                    //afterApplyIciciSubmitClick($("#jpNumber").val());
                    //Adobe code ends here
                });
                //Adobe code starts here
                var iciciURL = document.URL.substr(document.URL.lastIndexOf('/') + 1);
       			if(iciciURL.indexOf('-') !== -1) iciciURL = iciciURL.replaceAll('-',' ').trim()
       			var cardName = iciciURL.split('ICICI Bank')[1];
       			$.fn.actionClickCT('Card Application: Submit',iciciURL,'ICICI Bank')
                //submitIciciFormClick(fName + " " + mName + " " + lName, $("#email").val(), $("#mobile").val(), $('#city').val(), $("#pinCodeICICI").val(), 'ICICI', 'Submit JPNumber');
                //Adobe code ends here
                $("#CompletedForm").modal('hide');

                            }else{
                                                                $("#CompletedForm").modal('show');

                            }
//                        }
                    })
                }//else
                
            }
        });
                
                
        /* function submitAPI(){
   			$("#provide_jpNumber").modal('hide');
 	    	  $("#OtpSuccPopup").modal('show');
 	    	 setTimeout(disableSuccPopup, 10000);
 	          function disableSuccPopup(){
 	        	  $("#OtpSuccPopup").modal('hide');
 	        	  form.submit();
 	        	}  
 	          
 	         	 $("#iciciBean").submit();  
   			} */
        
        $("#jpNumpopup").on('click', function () {
        	if (beJpNumber != "") {
                $("#provide_jpNumber").modal('hide');
                $.ajax({method: "GET",
                    async: false,
                    url: "${applicationURL}getIcicicMemberData?jpNum=" + beJpNumber, success: function (data) {
                        $("#address").val(data.address);
                        $("#address2").val(data.address2);
                        $("#address3").val(data.address3);
                        $("#city").val(data.city);
                        $("#pinCode").val(data.pinCode);
                        $("#resedentialAddress").val(data.resedentialAddress);
                        $("#state1").val(data.state1);
                        $("#dateOfBirth").val(data.dateOfBirth);
                        $("#email").val(data.email);
                        $("#fname").val(data.fname);
                        $("#fullName").val(data.fullName);
                        $("#gender").val(data.gender);
                        $("#jetpriviligemembershipNumber").val(data.jetpriviligemembershipNumber);
                        $("#jetpriviligemembershipTier").val(data.jetpriviligemembershipTier);
                        $("#lname").val(data.lname);
                        $("#mname").val(data.mname);
                        $("#title").val(data.title);
                        $("#randomNumber").val('${beRandomNumber}');
                        $("#initiatedBy").val("Self via Marketing Campaign");
                        

                    },
                    error: function (e) {
                        console.log("Error " + e);
                    }
                });
                SaveFormData();
            } else {
                console.log("fullName outside", $("#fullName").val());
            }
        });

        

//         $(".genbut").click(function (event) {
//             $(".error_box2").addClass('hidden');
//             otpCounter++;
//             event.preventDefault();
//             var mobileNumber = $('#mobile').val();
//             var formNumber = '${formNumber}';

//             if (otpCounter > 3) {
//                 $('#otpValue').attr('disabled', true);
//                 $('#otpSucces').show();
//                 if (beJpNumber != "") {
//                     $('#otpSucces').html("Kindly log into your Jet Privilege membership account and check your mobile number.");
//                 } else {
//                     $('#otpSucces').html("Please Check Mobile Number.");
//                 }
//                 $('#jpNumpopup').attr('disabled', true);
//                 $("#jpNumpopup").css("color", "#ddd");
//                 $('#otpButton').attr('disabled', true);
//                 $("#otpButton").css("color", "#ddd");
//                 $(".error_msg_otp").addClass("hidden");
//                 var imageUrl = ctx + "static/images/icons/error_icon.png";
//                 $(".otpsuccess").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
//             } else {
//                 if ($('#otpButton').text() == 'Re-Generate OTP') {
//                     console.log("otpTransactionId=generate===>", $("#otpTransactionId").val());
//                     console.log("token==generate==>", $("#token").val());

//                     $.ajax({url: "${applicationURL}regenerateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&token=" + $("#token").val() + "&otpTransactionId=" + $("#otpTransactionId").val(),
//                         async: false,
//                         success:
//                                 function (data) {
//                                     stopTimeout();
//                                     startTimeout();
//                                     var myObj = JSON.parse(data);
//                                     mNumber = myObj.mobileNumber;
//                                     time = myObj.Time;
//                                     var otpTransactionId = myObj.otpTransactionid;
//                                     var token = myObj.token;
//                                     console.log("token-reger-->", token);
//                                     $("#pendingAttempt").val("");
//                                     if ($("#pendingAttempt").val() != "") {
//                                         if (beJpNumber != "") {
//                                             var mNumberSet1 = mNumber.substring(2, 8);
//                                             console.log("mNumberSet1", mNumberSet1);
//                                             mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
//                                             var mNumberSet0 = mNumber.substring(0, 2);
//                                             console.log("mNumberSet0", mNumberSet0);
//                                             var mNumberSet2 = mNumber.substring(8);
//                                             console.log("mNumberSet2", mNumberSet2);
//                                             var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
//                                             successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

//                                         } else {
//                                             successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

//                                         }

//                                     } else {
//                                         if (beJpNumber != "") {
//                                             var mNumberSet1 = mNumber.substring(2, 8);
//                                             console.log("mNumberSet1", mNumberSet1);
//                                             mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
//                                             var mNumberSet0 = mNumber.substring(0, 2);
//                                             console.log("mNumberSet0", mNumberSet0);
//                                             var mNumberSet2 = mNumber.substring(8);
//                                             console.log("mNumberSet2", mNumberSet2);
//                                             var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
//                                             successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '.';

//                                         } else {
//                                             successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

//                                         }

//                                     }
//                                     if (mNumber != "" && time != "") {
//                                         var chkregenerateFlag = true;
// //                                            var successMsg;
//                                         $("#otpTransactionId").val(otpTransactionId);
//                                         $("#token").val(token);
//                                         var timeLeft = 30;
//                                         timerId = setInterval(countdown, 1000);
//                                         var activemsg;
//                                         function countdown() {
//                                             if (timeLeft == -1) {
//                                                 clearTimeout(timerId);
//                                                 clearTimeout(timerId);
//                                                 $('#otpactive').hide();
//                                                 $('#otpactive').html('');
//                                                 $('#otpButton').attr('disabled', false);
//                                                 $("#otpButton").css("color", "#fff");
//                                             } else {
//                                                 activemsg = 'In case you have not received OTP please try regenerating OTP after ' + timeLeft + ' seconds ';
//                                                 $('#otpactive').show();
//                                                 $('#otpactive').html(activemsg);
//                                                 $('#otpButton').attr('disabled', true);
//                                                 $("#otpButton").css("color", "#ddd");
//                                                 timeLeft--;
//                                             }
//                                         }
//                                         $('.geninput').attr('disabled', false);
//                                         $('#otpButton').text('Re-Generate OTP');
//                                         $('#jpNumpopup').attr('disabled', true);
//                                         $("#jpNumpopup").css("color", "#ddd");
//                                         $('#otpSucces').html(successMsg);
//                                         $('#otpSucces').show();
//                                         var imageUrl = ctx + "static/images/icons/success_icon.png";
//                                         $("#otpSucces").css({"border": "2px solid #7dd62e", "background": "#f2fbe2 url(" + imageUrl + ") no-repeat 5px"});

//                                     } else {
//                                         otpCounter = 0;
//                                     }
//                                 }
//                     });
//                 } else {
//                     $.ajax({url: "${applicationURL}generateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber,
//                         async: false,
//                         success:
//                                 function (data) {
//                                     stopTimeout();
//                                     startTimeout();
//                                     var myObj = JSON.parse(data);
//                                     mNumber = myObj.mobileNumber;
//                                     time = myObj.Time;
//                                     var otpTransactionId = myObj.otpTransactionid;
//                                     var token = myObj.token;
//                                     console.log("token-reger-->", token);
//                                     $("#pendingAttempt").val("");
//                                     if ($("#pendingAttempt").val() != "") {
//                                         if (beJpNumber != "") {
//                                             var mNumberSet1 = mNumber.substring(2, 8);
//                                             console.log("mNumberSet1", mNumberSet1);
//                                             mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
//                                             var mNumberSet0 = mNumber.substring(0, 2);
//                                             console.log("mNumberSet0", mNumberSet0);
//                                             var mNumberSet2 = mNumber.substring(8);
//                                             console.log("mNumberSet2", mNumberSet2);
//                                             var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
//                                             successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

//                                         } else {
//                                             successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

//                                         }

//                                     } else {
//                                         if (beJpNumber != "") {
//                                             var mNumberSet1 = mNumber.substring(2, 8);
//                                             console.log("mNumberSet1", mNumberSet1);
//                                             mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
//                                             var mNumberSet0 = mNumber.substring(0, 2);
//                                             console.log("mNumberSet0", mNumberSet0);
//                                             var mNumberSet2 = mNumber.substring(8);
//                                             console.log("mNumberSet2", mNumberSet2);
//                                             var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
//                                             successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '.';

//                                         } else {
//                                             successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

//                                         }

//                                     }
//                                     if (mNumber != "" && time != "") {
//                                         var chkregenerateFlag = true;
// //                                            var successMsg;
//                                         $("#otpTransactionId").val(otpTransactionId);
//                                         $("#token").val(token);
//                                         console.log("otpTransactionId=generate===>", $("#otpTransactionId").val());
//                                         console.log("token==generate==>", $("#token").val());
//                                         var timeLeft = 30;
//                                         timerId = setInterval(countdown, 1000);
//                                         var activemsg;
//                                         function countdown() {
//                                             if (timeLeft == -1) {
//                                                 clearTimeout(timerId);
//                                                 clearTimeout(timerId);
//                                                 $('#otpactive').hide();
//                                                 $('#otpactive').html('');
//                                                 $('#otpButton').attr('disabled', false);
//                                                 $("#otpButton").css("color", "#fff");
//                                             } else {
//                                                 activemsg = 'In case you have not received OTP please try regenerating OTP after ' + timeLeft + ' seconds ';
//                                                 $('#otpactive').show();
//                                                 $('#otpactive').html(activemsg);
//                                                 $('#otpButton').attr('disabled', true);
//                                                 $("#otpButton").css("color", "#ddd");
//                                                 timeLeft--;
//                                             }
//                                         }
//                                         $('.geninput').attr('disabled', false);
//                                         $('#otpButton').text('Re-Generate OTP');
//                                         $('#jpNumpopup').attr('disabled', true);
//                                         $("#jpNumpopup").css("color", "#ddd");
//                                         $('#otpSucces').html(successMsg);
//                                         $('#otpSucces').show();
//                                         var imageUrl = ctx + "static/images/icons/success_icon.png";
//                                         $("#otpSucces").css({"border": "2px solid #7dd62e", "background": "#f2fbe2 url(" + imageUrl + ") no-repeat 5px"});

//                                     } else {
//                                         otpCounter = 0;
//                                     }
//                                 }
//                     });
//                 }
//             }
//         });
        
        
        
//        $(".geninput").keyup(function () {
//            if ($(".geninput").val().length == 6) {
//
//                $(".geninput").trigger('focusout');
//            } else {
//                $('#jpNumpopup').attr('disabled', true);
//            }
//        });

// OTP commented by roshan. validated otp on submit button as per new CR

//         var isCorrectOtp = false;
//         $(".geninput").focusout(function () {
//             var geinput = $(".geninput").val();

//             if (geinput == "") {
//                 $(".error_msg_otp").html("Please Enter OTP");
//                 $(".error_msg_otp").removeClass("hidden");
//                 $('#jpNumpopup').attr('disabled', true);
//                 isCorrectOtp = false;
//             } else if (geinput.length > 6 || geinput.length < 6) {

//                 $(".error_msg_otp").html("Please Enter Valid OTP");
//                 $(".error_msg_otp").removeClass("hidden");
//                 $('#jpNumpopup').attr('disabled', true);
//                 isCorrectOtp = false;
//             } else {
//                 if (isNaN(geinput) == true) {

//                     $(".error_msg_otp").html("Please Enter Valid OTP");
//                     $(".error_msg_otp").removeClass("hidden");
//                     $('#jpNumpopup').attr('disabled', true);
//                     isCorrectOtp = false;
//                 } else {
//                    var formNumber = '${formNumber}';
//                     var mobileNumber = $('#mobile').val();
//                     var otpTransactionId = $("#otpTransactionId").val();
//                     $.ajax({method: "GET",
//                         async: false,
//                         url: "${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&otpTransactionId=" + otpTransactionId + "&otp=" + geinput + "&token=" + $("#token").val(), success: function (data) {
//                             console.log("=========validate======", data);

// //                    $.get("${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + 2 + "&formNumber=" + formNumber, function (data, status) {
//                             if (data.length > 2) {
//                                 var myObj = JSON.parse(data);

//                                 pendingAttempt = myObj.pendingAttempts;
//                                 $("#pendingAttempt").val(myObj.pendingAttempts);
//                                 console.log("pending attempt"+$("#pendingAttempt").val())
//                                 if ($("#pendingAttempt").val() == "Number of attempts exceeds limit") {
//                                     $(".geninput").attr('disabled', true);
//                                     $(".geninput").val("");
//                                     $(".error_msg_otp").html("");
//                                     $(".error_msg_otp").hide();
//                                     successMsg="Left attempts are exceeded";
// //                                    $('#otpSucces').html("Number of attempts exceeds limit");
//                                     $('#otpSucces').removeClass('hidden');
//                                     $('#otpButton').attr('disabled', true);
//                                     $(".otpButton").addClass("hidden");
//                                     $('#jpNumpopup').attr('disabled', true);
//                                     $("#jpNumpopup").css("color", "#ddd");
//                                     var imageUrl = ctx + "static/images/icons/error_icon.png";
//                                     $(".otpsuccess").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
//                                 } else if ($("#pendingAttempt").val() != "") {
//                                     console.log("=-=-=p-==-=-=>", $("#pendingAttempt").val());
//                                     successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left ' + $("#pendingAttempt").val();

//                                 } else {
//                                     successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

//                                 }
//                                 $('#otpSucces').html(successMsg);

//                                 if (myObj.OTP_Verified == "true") {
//                                     $(".error_msg_otp").html("");
//                                     $(".error_msg_otp").addClass("hidden");
//                                     $('#jpNumpopup').attr('disabled', false);
//                                     $("#jpNumpopup").css("color", "#fff");
//                                     isCorrectOtp = true;
//                                     $('#otpVal').val(geinput);
//                                 } else {
//                                     $(".error_msg_otp").html("Please Enter Valid OTP");
//                                     $(".error_msg_otp").removeClass("hidden");
//                                     $('#jpNumpopup').attr('disabled', true);
//                                     $("#jpNumpopup").css("color", "#ddd");
//                                     isCorrectOtp = false;
//                                     $('#otpVal').val('');
//                                 }
//                             } else {
//                                 $(".error_msg_otp").html("Please Enter Valid OTP");
//                                 $(".error_msg_otp").removeClass("hidden");
//                                 $('#jpNumpopup').attr('disabled', true);
//                                 $("#jpNumpopup").css("color", "#ddd");
//                                 isCorrectOtp = false;
//                             }

//                         }});
//                 }
//             }

//         });
// End comment roshan


		$(".geninput").focusout(function () {
		var geinput = $(".geninput").val();
		
		if (geinput == "") {
		    $(".error_msg_otp").html("Please Enter OTP");
		    $(".error_msg_otp").removeClass("hidden");
		    $('#jpNumpopup').attr('disabled', true);
		    $('#jpNumpopup').removeClass('genebutton-HDFC');
            $('#jpNumpopup').addClass('genebutton_HDFC');
		    isCorrectOtp = false;
		} else if (geinput.length > 6 || geinput.length < 6) {
		
		    $(".error_msg_otp").html("Please Enter Valid OTP");
		    $(".error_msg_otp").removeClass("hidden");
		    $('#jpNumpopup').attr('disabled', true);
		    $('#jpNumpopup').removeClass('genebutton-HDFC');
            $('#jpNumpopup').addClass('genebutton_HDFC');
		    isCorrectOtp = false;
		} else {
		    if (isNaN(geinput) == true) {
		
		        $(".error_msg_otp").html("Please Enter Valid OTP");
		        $(".error_msg_otp").removeClass("hidden");
		        $('#jpNumpopup').attr('disabled', true);
		        $('#jpNumpopup').removeClass('genebutton-HDFC');
                $('#jpNumpopup').addClass('genebutton_HDFC');
		        isCorrectOtp = false;
		    }
		    
		    $(".error_msg_otp").html("");
            $(".error_msg_otp").addClass("hidden");
//             $('#jpNumpopup').attr('disabled', false);
//             $('#jpNumpopup').removeClass('genebutton-HDFC');
//             $('#jpNumpopup').addClass('genebutton_HDFC');
//             $("#jpNumpopup").css("color", "#fff");
            //isCorrectOtp = true;
		}
		});



        var previousValue = $("#jpNumber").val();

        $(".proceedPopup_close").on("click", function (e) {

            otpCounter = 0
//             $('#otpButton').text("Generate OTP");
            $('#otpSucces').hide();
            $("#otpValue").attr('disabled', true);
            $('#jpNumpopup').attr('disabled', true);
            $('#jpNumpopup').removeClass('genebutton-HDFC');
            $('#jpNumpopup').addClass('genebutton_HDFC');
            $(".error_msg_otp").html("");
            $(".error_msg_otp").addClass("hidden");
            $(".geninput").val("");
            $(".geninput").attr("disabled", true);

        });
        $("#jpNumber").on("input", function (e) {

            var currentValue = $(this).val();
            if (currentValue != previousValue) {
                previousValue = currentValue;
                otpCounter = 0
//                 $('#otpButton').text("Generate OTP");
                $('#otpSucces').hide();
                $(".error_msg_otp").html("");
                $(".error_msg_otp").addClass("hidden");
//                 $(".geninput").val("");
            }
        });
        $("#jpNumber").keyup(function () {
            var jpNum = $(this).val();
            var cpNo = $("#cpNo").val();
            $('#jpNumpopup').attr('disabled', true);
            $('#jpNumpopup').removeClass('genebutton-HDFC');
            $('#jpNumpopup').addClass('genebutton_HDFC');
            if (jpNum == "") {
                $(".error_msg").text("Please enter InterMiles No.");
                $(".error_msg").removeClass('hidden');
                $("#jpTier").val('');
                $("#jetpriviligemembershipTier").val('');
                isCorrectJpNumber = false;
                isIMCorrect = false;
//                 $('#otpButton').attr('disabled', true);
                //$('.geninput').attr('disabled', true);
                $('#jpNumpopup').attr('disabled', true);
                $('#jpNumpopup').removeClass('genebutton-HDFC');
                $('#jpNumpopup').addClass('genebutton_HDFC');
                return false;
            } else
            if (jpNum != "" && jpNum.length == 9) {
                if (isNaN(jpNum)) {
                    $(".error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                    $(".error_msg").removeClass('hidden');
                    $("#jetpriviligemembershipTier").val('');
                    isCorrectJpNumber = false;
                    isIMCorrect = false;
//                     $('#otpButton').attr('disabled', true);
                   // $('.geninput').attr('disabled', true);
                    $('#jpNumpopup').attr('disabled', true);
                    $('#jpNumpopup').removeClass('genebutton-HDFC');
                    $('#jpNumpopup').addClass('genebutton_HDFC');
                    return false;
                } else {
                    jpNumLastDig = jpNum % 10;
                    jpNoTemp = eval(jpNum.slice(0, -1)) % 7;

                    if (jpNumLastDig !== jpNoTemp)
                    {
                        $(".error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                        $(".error_msg").removeClass('hidden');
                        $("#jetpriviligemembershipTier").val('');
                        isCorrectJpNumber = false;
                        isIMCorrect = false;
//                         $('#otpButton').attr('disabled', true);
                       // $('.geninput').attr('disabled', true);
                        $('#jpNumpopup').attr('disabled', true);
                        $('#jpNumpopup').removeClass('genebutton-HDFC');
                        $('#jpNumpopup').addClass('genebutton_HDFC');
                        return false;
                    } else {
                        $("#jetpriviligemembershipNumber").val(jpNum);
//                         $.get("${applicationURL}getTier?jpNum=" + jpNum, function (data, status) {
	$.get("${applicationURL}getjpTier?jpNum=" + jpNum, function (data, status) {
                            if (status == "success" && data.length > 2) {
                            	$("#jetpriviligemembershipTier").val(data);
                                isCorrectJpNumber = true; 
                                isIMCorrect = true;
                                $(".error_msg").text("");
                                $(".error_msg").addClass('hidden');
                                $('#jpNumpopup').attr('disabled', true);
//                                 $('#otpButton').attr('disabled', false);
                                $('#jpNumpopup').attr('disabled', false);
                               $('#jpNumpopup').removeClass('genebutton_HDFC');
                               $('#jpNumpopup').addClass('genebutton-HDFC');
                               
                               validateOTPOnSubmit();
                               
                            } else {
                                $(".error_msg").text("Your application may not have gone through successfully. Please click Submit again.");
                                $(".error_msg").removeClass('hidden');
                                $("#jetpriviligemembershipTier").val('');
                                $('#jpNumpopup').attr('disabled', true);
                                $('#jpNumpopup').removeClass('genebutton-HDFC');
                                $('#jpNumpopup').addClass('genebutton_HDFC');
//                                 $('#otpButton').attr('disabled', true);
                               // $('.geninput').attr('disabled', true);
                                isCorrectJpNumber = false;
                                isIMCorrect = false;
                                return false;
                            }
                        });

                    }
                }
            } else {

                $(".error_msg").removeClass('hidden');
                $("#jetpriviligemembershipTier").val('');
               // $('.geninput').attr('disabled', true);
//                 $('#otpButton').attr('disabled', true);
                isCorrectJpNumber = false;
                isIMCorrect = false;
                return false;
            }
        });

        function addOfferToJpPopup(jsonStr) {
            $(".jpPopup_header").addClass("add_bullet");
            $(".jpPopup_header").html("<p>Avail Offer</p>" + jsonStr.offerDesc);
            $("#offerId").val(jsonStr.offerId);
            $("#offerDesc").val(jsonStr.offerDesc);
        }

        $("#mini_jpNumber").on('keyup', function () {
            var cpNo = $("#cpNo").val();
            var jpNum = $(this).val();
            if (jpNum != "" && jpNum.length == 9) {
                if (isNaN(jpNum)) {
                    $(".mini_error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                    $(".mini_error_msg").removeClass('hidden');
                    isValid = false;
                    return false;
                } else {
                    jpNumLastDig = jpNum % 10;
                    jpNoTemp = eval(jpNum.slice(0, -1)) % 7;
                    if (jpNumLastDig !== jpNoTemp)
                    {
                        $(".mini_error_msg").text("Invalid InterMiles No., Please enter valid InterMiles No.");
                        $(".mini_error_msg").removeClass('hidden');
                        isValid = false;
                        return false;
                    } else {
                        $.get("${applicationURL}getOffer1?jpNum=" + jpNum + "&cpNo=" + cpNo, function (data, status) {
                            if (status == "success" && data.length > 2) {
                                var jsonStr = JSON.parse(data);
                                $(".offer_link").addClass("hidden");
                                $(".offer_display").removeClass("hidden");
                                $(".offer_display").html(jsonStr.offerDesc);
                                $(".login_offer").html(jsonStr.offerDesc);
                                addOfferToJpPopup(jsonStr);
                                $('.home_feature').each(function () {
                                    var toggle_img = "<div class='collps_img coldown'><img src=" + ctx + "static/images/co-brand/collps_down.png></div>" +
                                            "<div class='collps_img colUp' style='display:none'><img src=" + ctx + "static/images/co-brand/collps_up.png></div>";
                                    if ($(this).height() > 200) {
                                        $(this).addClass("trimmed");
                                        $(this).closest(".list_cont").after(toggle_img);
                                    }
                                });
                            }
                        });
                        $("#jetpriviligemembershipNumber").val(jpNum);
                        $(".mini_error_msg").text("");
                        $(".mini_error_msg").addClass('hidden');
                        isValid = true;
                    }
                }
            }
        });

        $("#mini_jpNumber_submit").on('click', function () {
            var jpNum = $("#mini_jpNumber").val();
            var originalJPNum = loginJPNum;
            if (jpNum == "" || jpNum.length <= 8) {
                $(".mini_error_msg").text("Please enter valid InterMiles No.");
                $(".mini_error_msg").removeClass('hidden');
                return false;
            }
            if (originalJPNum != jpNum) {
                $('select').val('');
                $('input[type=text]').val('');
            }
            if (isValid) {
                $('#jpNumpopup').attr('disabled', false);

            }
        });

        $("#state1").on("change", function () {
            var state = $("#state1 option:selected").val();
            var pin = $('.residentAddress .pincodeICICI').val();
            $.get("${applicationURL}getCitiesByState1?state=" + state, function (data) {
                $('#city').empty();
                $('#city').append("<option value=''>Select City</option>");
                if (data != '') {
                    $.each(data, function (index, value) {
                        $('#city').append($("<option>", {value: index, html: value}));

                    });
                }
            });
            // $('.resedentialAddress-error-div').html('');
            // $('.resedentialAddress-error-div').addClass('hidden');
            CheckAddress('RES');
            $('#city').selectmenu('refresh', true);

        });

        $("#PState").on("change", function () {
            var PState = $("#PState option:selected").val();
            $.get("${applicationURL}getCitiesByState1?state=" + PState, function (data) {
                $('#PCity').empty();
                $('#PCity').append("<option value=''>Select City</option>");
                if (data != '') {
                    $.each(data, function (index, value) {
                        $('#PCity').append($("<option>", {value: index, html: value}));
                    });
                }
            });
            //$('.permanentAddr-error-div').html('');
            //$('.permanentAddr-error-div').addClass('hidden');
            CheckAddress('PRES');
            $('#PCity').selectmenu('refresh', true);
        });

        $("#OState").on("change", function () {
            var OState = $("#OState option:selected").val();
            $.get("${applicationURL}getCitiesByState1?state=" + OState, function (data) {
                $('#OCity').empty();
                $('#OCity').append("<option value=''>Select City</option>");
                if (data != '') {
                    $.each(data, function (index, value) {
                        $('#OCity').append($("<option>", {value: index, html: value}));
                    });
                }

            });
            //$('.companyAddr_div-error-div').html('');
            //$('.companyAddr_div-error-div').addClass('hidden');
            CheckAddress('COMP');

            $('#OCity').selectmenu('refresh', true);
        });




        $("#city").on("change", function () {
            var city = $("#city option:selected").val();
            var pin = $('.residentAddress .pincodeICICI').val();
            $.get("${applicationURL}getICICIStateByCity?city=" + city, function (data, status) {
                if (status == "success" && data.length >= 2) {
//                     var json = JSON.parse(data);
//                    var IciciState=json.state;
//                    IciciState=IciciState.toUpperCase();
//                      console.log("Data : "+data);
                    $("#state1").val(data);
                    var pin = $('.residentAddress .pincodeICICI').val();
                }
            });
//            $('.resedentialAddress-error-div').html('');
//            $('.resedentialAddress-error-div').addClass('hidden');

            CheckAddress('RES');
        });

        $("#PCity").on("change", function () {
            var PCity = $("#PCity option:selected").val();
            $.get("${applicationURL}getICICIStateByCity?city=" + PCity, function (data, status) {
                if (status == "success" && data.length >= 2) {
//                    var json = JSON.parse(data);
//                    var IciciState=json.state;
//                    IciciState=IciciState.toUpperCase();
                    $("#PState").val(data);
                }
            });
//            $('.permanentAddr-error-div').html('');
//            $('.permanentAddr-error-div').addClass('hidden');
            CheckAddress('PRES');
        });


        $("#OCity").on("change", function () {
            var OCity = $("#OCity option:selected").val();
            $.get("${applicationURL}getICICIStateByCity?city=" + OCity, function (data, status) {
                if (status == "success" && data.length >= 2) {
//                    var json = JSON.parse(data);
//                    var IciciState=json.state;
//                    IciciState=IciciState.toUpperCase();
                    $("#OState").val(data);
                }
            });
//            $('.companyAddr_div-error-div').html('');
//            $('.companyAddr_div-error-div').addClass('hidden');
            CheckAddress('COMP');
        });

        var pincodemsg = 'We are sorry, American Express does not service this pincode, please enter an alternate pincode';
        var invalidMsg = "We are sorry, ICICI bank does not service this pincode, please enter an alternate pin code";
//             $('#pinCodeICICI').on('keyup', function () {
////                 var pinCode = $(this).val();
////                 checkadress();
//             });

//            function checkadress(){
        $('#pinCodeICICI').on('keyup', function () {

            var pinCode = $("#pinCodeICICI").val();
            var cpNo = $("#cpNo").val();
            var cCity;
            var cState;
            if (pinCode != "" && pinCode.length == 6) {
                //$(".ui-menu-item").hide();
//                $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
//                    if (status == "success" && data != "excluded") {
                $.get("${applicationURL}getCity1?pinCode=" + pinCode, function (data, status) {
                    if (status == "success" && data.length != 2) {
                        var json = JSON.parse(data);
                        $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                $("#city").val(json.city);
                                $("#state1").val(json.state);
                                $("#hcity").val(json.city);
                                $("#hstate").val(json.state);
                                $('#state1').attr("disabled", true);
                                $('#city').attr("disabled", true);

                                //$('.resedentialAddress-error-div').html('');
                                //$('.resedentialAddress-error-div').addClass('hidden');
                                CheckAddress('RES');

                                if ($("#city").val() == null || $("#city").val() == '') {
                                    $('#city').empty();
                                    $('select#city').append('<option>' + json.city + '</option>');
                                }
                            } else {
                                $('.resedentialAddress-error-div').html(pincodemsg);
                                $('.resedentialAddress-error-div').removeClass('hidden');
                                $("#city").val("");
                                $("#state1").val("");
                                $('#state1').attr("disabled", false);
                                $('#city').attr("disabled", false);
                            }
                        });
                    } else {

                        $('.resedentialAddress-error-div').html(invalidMsg);
                        $('.resedentialAddress-error-div').removeClass('hidden');
                        $("#city").val("");
                        $("#state1").val("");
                        //------change here---------
                        $('#state1').attr("disabled", true);
                        $('#city').attr("disabled", true);
                        //---------change end here------
                    }
                });
//                    }
//                    else {
//                        $('.resedentialAddress-error-div').html(pincodemsg);
//                        $('.resedentialAddress-error-div').removeClass('hidden');
//                        $("#city").val("");
//                        $("#state1").val("");
//                    }
//                });


            } else {
                $("#city").val("");
                $("#state1").val("");
                $('#state1').attr("disabled", false);
                $('#city').attr("disabled", false);
            }
        });
//        }
        $('#PpinCodeICICI').on('keyup', function () {

            var pinCode = $(this).val();
            var cpNo = $("#cpNo").val();
            if (pinCode != "" && pinCode.length == 6) {
                $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
                    if (status == "success" && data != "excluded") {

                        $.get("${applicationURL}getCity1?pinCode=" + pinCode, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                var json = JSON.parse(data);
                                $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                    if (status == "success" && data.length != 2) {
                                        $("#PCity").val(json.city);
                                        $("#PState").val(json.state);
                                        $("#hcity1").val(json.city);
                                        $("#hstate1").val(json.state);
                                        $('#PCity').attr("disabled", true);
                                        $('#PState').attr("disabled", true);

                                        CheckAddress('PRES');
                                        //$('.permanentAddr-error-div').addClass('hidden');
                                        //$(".permanentAddr-error-div").html('');

                                        if ($("#PCity").val() == null || $("#PCity").val() == '') {
                                            $('#PCity').empty();
                                            $('select#PCity').append('<option>' + json.city + '</option>');
                                        }
                                    } else {
                                        $('.permanentAddr-error-div').html(pincodemsg);
                                        $('.permanentAddr-error-div').removeClass('hidden');
                                        $("#PCity").val("");
                                        $("#PState").val("");
                                        $('#PCity').attr("disabled", false);
                                        $('#PState').attr("disabled", false);
                                    }
                                });
                            } else {
                                $('.permanentAddr-error-div').html(invalidMsg);
                                $('.permanentAddr-error-div').removeClass('hidden');
                                $("#PState").val("");
                                //---------change here------------
                                $('#PCity').attr("disabled", true);
                                $('#PState').attr("disabled", true);
                                //--------change here end----------
                            }
                        });
                    } else {
                        $('.permanentAddr-error-div').removeClass('hidden');
                        $(".permanentAddr-error-div").html(pincodemsg);
                        $("#PCity").val("");
                        $("#PState").val("");
                    }
                });
            } else {
                $("#PCity").val("");
                $("#PState").val("");
                $('#PCity').attr("disabled", false);
                $('#PState').attr("disabled", false);
            }
        });

        $('#OPincodeICICI').on('keyup', function () {
            var pinCode = $(this).val();
            var cpNo = $("#cpNo").val();
            if (pinCode != "" && pinCode.length == 6) {
                $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
                    if (status == "success" && data != "excluded") {
                        $.get("${applicationURL}getCity1?pinCode=" + pinCode, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                var json = JSON.parse(data);
                                $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                    if (status == "success" && data.length != 2) {
                                        $("#OCity").val(json.city);
                                        $("#OState").val(json.state);
                                        $("#hcity2").val(json.city);
                                        $("#hstate2").val(json.state);
                                        $('#OCity').attr("disabled", true);
                                        $('#OState').attr("disabled", true);


                                        //$('.companyAddr_div-error-div').html('');
                                        //$('.companyAddr_div-error-div').addClass('hidden');
                                        CheckAddress('COMP');

                                        if ($("#OCity").val() == null || $("#OCity").val() == '') {
                                            $('#OCity').empty();
                                            $('select#OCity').append('<option>' + json.city + '</option>');
                                        }
                                    } else {
                                        $('.companyAddr_div-error-div').html(pincodemsg);
                                        $('.companyAddr_div-error-div').removeClass('hidden');
                                        $("#OCity").val("");
                                        $("#OState").val("");
                                        $('#OCity').attr("disabled", false);
                                        $('#OState').attr("disabled", false);
                                    }
                                });
                            } else {
                                $('.companyAddr_div-error-div').html(invalidMsg);
                                $('.companyAddr_div-error-div').removeClass('hidden');
                                $("#OCity").val("");
                                $("#OState").val("");
                                //---------change here--------
                                $('#OCity').attr("disabled", true);
                                $('#OState').attr("disabled", true);
                                //----------change here end--------
                            }
                        });
                    } else {
                        $('.companyAddr_div-error-div').html(pincodemsg);
                        $('.companyAddr_div-error-div').removeClass('hidden');
                        $("#OCity").val("");
                        $("#OState").val("");
                    }
                });
            } else {
                $("#OCity").val("");
                $("#OState").val("");
                $('#OCity').attr("disabled", false);
                $('#OState').attr("disabled", false);
            }
        });

//                  icici pincode

        var searchTerms =${pincode};

        var array = searchTerms.toString().split(",");
        var availableTags = array;

        $("#pinCodeICICI").autocomplete({
//                        disabled: false,
// new cr start
            source: function (request, response) {
                var matches = $.map(availableTags, function (acItem) {
                    if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return acItem;
                    }
                });
                response(matches);
            },
            minLength: 2,
            focus: function (event, ui) {
                event.preventDefault();
            },
            select: function (event, ui) {

                $("#pinCodeICICI").val(ui.item.value);
                var pinCode = ui.item.value;

                var cpNo = $("#cpNo").val();
                var cCity;
                var cState;
                if (pinCode != "" && pinCode.length == 6) {

                    $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {

                        if (status == "success" && data != "excluded") {

                            $.get("${applicationURL}getCity1?pinCode=" + ui.item.value, function (data, status) {

                                if (status == "success" && data.length != 2) {

                                    var json = JSON.parse(data);
                                    $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                        if (status == "success" && data.length != 2) {

                                            $("#city").val(json.city);
                                            $("#state1").val(json.state);

                                            $("#hcity").val(json.city);
                                            $("#hstate").val(json.state);
                                            $('#state1').attr("disabled", true);
                                            $('#city').attr("disabled", true);

                                            // Code for Assigning values
                                            residentAddressField1 = $('.residentAddressField1').val();
                                            residentAddressField2 = $('.residentAddressField2').val();
                                            residentAddressField3 = $('.residentAddressField3').val();
                                            city = $("#city").val();
                                            state = $("#state1").val();
                                            pinCode1 = $("#pinCodeICICI").val();
                                            $('.currentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);

                                            // End Code for Assigning values

                                            if ($("#city").val() == null || $("#city").val() == '') {
                                                $('#city').empty();
                                                $('select#city').append('<option>' + json.city + '</option>');
                                            }
                                        } else {
                                            $('.resedentialAddress-error-div').html(pincodemsg).css("color", "#ff0000");
                                            $('.resedentialAddress-error-div').removeClass('hidden');
                                            $('#state1').attr("disabled", false);
                                            $('#city').attr("disabled", false);
                                        }
                                    });
                                } else {
                                    $('.resedentialAddress-error-div').html(invalidMsg).css("color", "#ff6e00");
                                    $('.resedentialAddress-error-div').removeClass('hidden');
                                    $("#city").val("");
                                    $("#state1").val("");
                                    $('#state1').attr("disabled", false);
                                    $('#city').attr("disabled", false);
                                }
                            });
                        } else {
//               
                            $('.resedentialAddress-error-div').html(pincodemsg).css("color", "#ff0000");
                            $('.resedentialAddress-error-div').removeClass('hidden');

                        }
                    });
                }
                residentAddressField1 = $('.residentAddressField1').val();
                residentAddressField2 = $('.residentAddressField2').val();
                residentAddressField3 = $('.residentAddressField3').val();
                pin = $("#pinCodeICICI").val();
                city = $("#city").val();
                state = $("#state1").val();
                if (($('.residentAddress .city option:selected').val()) != 0) {
                    city = $('.residentAddress .city option:selected').text();
                }
                if (($('.residentAddress .state option:selected').val()) != 0) {
                    state = $('.residentAddress .state option:selected').text();
                }

//                if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
//                    $('#resedentialAddress-error').css('display', 'block');
//                else
//                    $('#resedentialAddress-error').css('display', 'none');

                $('.residentialAddr_div').addClass('hidden');
                $('.currentResidentialAddress').removeClass('hidden');
                $('.currentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

                if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
                {
                    $('.currentResidentialAddress').val('');
                }
                // Code for Assigning values
                residentAddressField1 = $('.residentAddressField1').val();
                residentAddressField2 = $('.residentAddressField2').val();
                residentAddressField3 = $('.residentAddressField3').val();
                city = $("#city").val();
                state = $("#state1").val();
                pinCode1 = $("#pinCodeICICI").val();
                $('.currentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
                // End Code for Assigning values
                //$('.resedentialAddress-error-div').html('');
                //$('.resedentialAddress-error-div').addClass('hidden');
                CheckAddress('RES');
                return false;

            },

            response: function (event, ui) {

            }

        });

        //office new code start here 

        $("#OPincodeICICI").autocomplete({
//                        disabled: false,
            source: function (request, response) {
                var matches = $.map(availableTags, function (acItem) {
                    if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return acItem;
                    }
                });
                response(matches);
            },

            minLength: 2,
            focus: function (event, ui) {
                event.preventDefault();
            },
            select: function (event, ui) {

                $("#OPincodeICICI").val(ui.item.value);
                var pinCode = ui.item.value;
                var cpNo = $("#cpNo").val();
                var cCity;
                var cState;
                if (pinCode != "" && pinCode.length == 6) {

                    $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {

                        if (status == "success" && data != "excluded") {

                            $.get("${applicationURL}getCity1?pinCode=" + ui.item.value, function (data, status) {

                                if (status == "success" && data.length != 2) {

                                    var json = JSON.parse(data);
                                    $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                        if (status == "success" && data.length != 2) {

                                            $("#OCity").val(json.city);
                                            $("#OState").val(json.state);

                                            $("#hcity2").val(json.city);
                                            $("#hstate2").val(json.state);
                                            $('#OState').attr("disabled", true);
                                            $('#OCity').attr("disabled", true);


                                            // Code for Assigning values
                                            residentAddressField1 = $('.companyAddressField1').val();
                                            residentAddressField2 = $('.companyAddressField2').val();
                                            residentAddressField3 = $('.companyAddressField3').val();
                                            city = $("#OCity").val();
                                            state = $("#OState").val();
                                            pinCode1 = $("#OPincodeICICI").val();
                                            $('.companyAddress_box').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);

                                            // End Code for Assigning values

                                            if ($("#OCity").val() == null || $("#OCity").val() == '') {
                                                $('#OCity').empty();
                                                $('select#OCity').append('<option>' + json.city + '</option>');
                                            }
                                        } else {
                                            $('.companyAddr_div-error-div').html(pincodemsg).css("color", "#ff0000");
                                            $('.companyAddr_div-error-div').removeClass('hidden');

                                            $('#OState').attr("disabled", false);
                                            $('#OCity').attr("disabled", false);
                                        }
                                    });
                                } else {
                                    $('.companyAddr_div-error-div').html(invalidMsg).css("color", "#ff6e00");
                                    $('.companyAddr_div-error-div').removeClass('hidden');
                                    $("#OCity").val("");
                                    $("#OState").val("");
                                    $('#OState').attr("disabled", false);
                                    $('#OCity').attr("disabled", false);
                                }
                            });
                        } else {
//                            console.log("In ELSE : 1380");
                            $('.companyAddr_div-error-div').html(pincodemsg).css("color", "#ff0000");
                            $('.companyAddr_div-error-div').removeClass('hidden');

                        }
                    });
                }
                residentAddressField1 = $('.companyAddressField1').val();
                residentAddressField2 = $('.companyAddressField2').val();
                residentAddressField3 = $('.companyAddressField2').val();
                pin = $("#OPincodeICICI").val();
                city = $("#OCity").val();
                state = $("#OState").val();
                if (($('.companyAddress .city option:selected').val()) != 0) {
                    city = $('.companyAddress .city option:selected').text();
                }
                if (($('.companyAddress .state option:selected').val()) != 0) {
                    state = $('.companyAddress .state option:selected').text();
                }

                if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
                    $('#companyAddress-error').css('display', 'block');
                else
                    $('#companyAddress-error').css('display', 'none');

                $('.companyAddr_div').addClass('hidden');
                $('.companyAddress_box').removeClass('hidden');
                $('.companyAddress_box').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

                if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
                {
                    $('.companyAddress_box').val('');
                }
                // Code for Assigning values
                residentAddressField1 = $('.residentAddressField1').val();
                residentAddressField2 = $('.residentAddressField2').val();
                residentAddressField3 = $('.residentAddressField3').val();
                city = $("#OCity").val();
                state = $("#OState").val();
                pinCode1 = $("#OPincodeICICI").val();
                $('.companyAddress_box').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
                // End Code for Assigning values
                //$('.companyAddr_div-error-div').html('');
                //$('.companyAddr_div-error-div').addClass('hidden');
                CheckAddress('COMP');


                return false;

            },

            response: function (event, ui) {

            }

        });

        //office new code end here


        //permanent code start here 


        $("#PpinCodeICICI").autocomplete({
//                        disabled: false,
            source: function (request, response) {
                var matches = $.map(availableTags, function (acItem) {
                    if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return acItem;
                    }
                });
                response(matches);
            },
            minLength: 2,
            focus: function (event, ui) {
                event.preventDefault();
            },
            select: function (event, ui) {

                $("#PpinCodeICICI").val(ui.item.value);
                var pinCode = ui.item.value;
                var cpNo = $("#cpNo").val();
                var cCity;
                var cState;
                if (pinCode != "" && pinCode.length == 6) {

                    $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {

                        if (status == "success" && data != "excluded") {

                            $.get("${applicationURL}getCity1?pinCode=" + ui.item.value, function (data, status) {

                                if (status == "success" && data.length != 2) {

                                    var json = JSON.parse(data);
                                    $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
                                        if (status == "success" && data.length != 2) {

                                            $("#PCity").val(json.city);
                                            $("#PState").val(json.state);

                                            $("#hcity1").val(json.city);
                                            $("#hstate1").val(json.state);
                                            $('#PState').attr("disabled", true);
                                            $('#PCity').attr("disabled", true);

                                            // Code for Assigning values
                                            residentAddressField1 = $('.permanent_AddressField1').val();
                                            residentAddressField2 = $('.permanent_AddressField2').val();
                                            residentAddressField3 = $('.permanent_AddressField3').val();
                                            city = $("#PCity").val();
                                            state = $("#PState").val();
                                            pinCode1 = $("#PpinCodeICICI").val();
                                            $('.permanentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);

                                            // End Code for Assigning values

                                            if ($("#PCity").val() == null || $("#PCity").val() == '') {
                                                $('#PCity').empty();
                                                $('select#PCity').append('<option>' + json.city + '</option>');
                                            }
                                        } else {
                                            $('.permanentAddr-error-div').html(pincodemsg).css("color", "#ff0000");
                                            $('.permanentAddr-error-div').removeClass('hidden');
                                            $('#PState').attr("disabled", false);
                                            $('#PCity').attr("disabled", false);
                                        }
                                    });
                                } else {
                                    $('.permanentAddr-error-div').html(invalidMsg).css("color", "#ff6e00");
                                    $('.permanentAddr-error-div').removeClass('hidden');
                                    $("#PCity").val("");
                                    $("#PState").val("");
                                    $('#PState').attr("disabled", false);
                                    $('#PCity').attr("disabled", false);
                                }
                            });
                        } else {
//                            console.log("In ELSE : 1380");
                            $('.permanentAddr-error-div').html(pincodemsg).css("color", "#ff0000");
                            $('.permanentAddr-error-div').removeClass('hidden');

                        }
                    });
                }
                residentAddressField1 = $('.permanent_AddressField1').val();
                residentAddressField2 = $('.permanent_AddressField2').val();
                residentAddressField3 = $('.permanent_AddressField3').val();
                pin = $("#PpinCodeICICI").val();
                city = $("#PCity").val();
                state = $("#PState").val();
                if (($('.permanent_residentAddress .city option:selected').val()) != 0) {
                    city = $('.permanent_residentAddress .city option:selected').text();
                }
                if (($('.permanent_residentAddress .state option:selected').val()) != 0) {
                    state = $('.permanent_residentAddress .state option:selected').text();
                }

                if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
                    $('#permanentAddr-error').css('display', 'block');
                else
                    $('#permanentAddr-error').css('display', 'none');

                $('.permanentAddr_div').addClass('hidden');
                $('.permanentResidentialAddress').removeClass('hidden');
                $('.permanentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

                if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
                {
                    $('.permanentResidentialAddress').val('');
                }
                // Code for Assigning values
                residentAddressField1 = $('.permanent_AddressField1').val();
                residentAddressField2 = $('.permanent_AddressField2').val();
                residentAddressField3 = $('.permanent_AddressField3').val();
                city = $("#PCity").val();
                state = $("#PState").val();
                pinCode1 = $("#pinCodeICICI").val();
                $('.permanentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
                // End Code for Assigning values
                //$('.permanentAddr-error-div').html('');
                //$('.rpermanentAddr-error-div').addClass('hidden');
                CheckAddress('PRES');



                return false;

            },

            response: function (event, ui) {

            }

        });


        //permanent code end here


        $("#quickEnrollUser").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                enrollTitle: {
                    required: true,
                },
                enrollFname: {
                    required: true,
                    maxlength: 26,
                    AlphabetsOnly: true
                },
                enrollLname: {
                    required: true,
                    maxlength: 26,
                    AlphabetsOnly: true
                },
                enrollCity: {
                    required: true,
                    AlphabetsOnly: true
                },
                enrollemail: {
                    required: true,
                    emailValidation: true
                },
                enrollPhone: {
                    required: true,
                    mobileValidation: true
                },
                enrollDob: {
                    required: true
                },
                termsAndConditionsEnroll: {
                    required: true
                }
            },

            messages: {
                enrollTitle: {
                    required: "Please Select Title",

                },
                enrollFname: {
                    required: "Please Enter FirstName",
                    maxlength: "Maximum Length should 26",
                    AlphabetsOnly: "Alphabate only"
                },
                enrollLname: {
                    required: "Please Enter LastName",
                    maxlength: "Maximum Length should 26",
                    AlphabetsOnly: "Alphabate only"
                },
                enrollCity: {
                    required: "Please Enter City Name",
                    AlphabetsOnly: "Alphabate only"
                },
                enrollemail: {
                    required: "Please Enter Email Id",
                    emailValidation: "Email ID is not valid."
                },
                enrollPhone: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid.",
                    mobileValidation: "Please Enter Valid Mobile Number"
                },
                enrollDob: {
                    required: "Please Enter Date of Birth",
                },
                termsAndConditionsEnroll: {
                    required: "Please accept the InterMiles membership Terms & Conditions to proceed."
                }
            },
            errorElement: "div",
            onfocusout: function (element) {
                this.element(element);  // <- "eager validation"
            },
            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },

            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                $('.error_box2').removeClass('hidden');
                error.insertAfter($(element).closest("div"));
                $(error).appendTo('.error_box2 ul.error_list2');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');

            },
            submitHandler: function (form) {
                form.submit();
            },

        });

        $(document).click(function (event) {
            var len = $('.error_list > li:visible').length;
            if (!(len >= 1)) {
                $('.error_box').addClass('hidden');
            } else {
                $('.error_box').removeClass('hidden');
            }
        });
        $('.enroll_popup').click(function (event) {
            var len = $('.error_list2 > li:visible').length;
            if (!(len >= 1)) {
                $('.error_box2').addClass('hidden');
            } else {
                $('.error_box2').removeClass('hidden');
            }
        });
        function callenroll() {
            $(".error_box2").addClass('hidden');
            $('.enroll_popup').removeClass('hidden');
            $('.jpNum_popup').addClass('hidden');
            var fname = $("#fname").val();
            var mname = $("#mname").val();
            var lname = $("#lname").val();
            var phone = $("#mobile").val();
            var email = $("#email").val();
            var dob = $("#dateOfBirth").val();
            $("#enrollFname").val(fname);
            $("#enrollMname").val(mname);
            $("#enrollLname").val(lname);
            $("#enrollemail").val(email);
            $("#enrollPhone").val(phone);
            $("#enrollDob").val(dob);
        }
//        $("#quickEnroll").click(function () {
//            $(".error_box2").addClass('hidden');
//            $('.enroll_popup').removeClass('hidden');
//            $('.jpNum_popup').addClass('hidden');
//            var fname = $("#fname").val();
//            var mname = $("#mname").val();
//            var lname = $("#lname").val();
//            var phone = $("#mobile").val();
//            var email = $("#email").val();
//            var dob = $("#dateOfBirth").val();
//            $("#enrollFname").val(fname);
//            $("#enrollMname").val(mname);
//            $("#enrollLname").val(lname);
//            $("#enrollemail").val(email);
//            $("#enrollPhone").val(phone);
//            $("#enrollDob").val(dob);
//        });

//        $("#social-profiles").on('click', function () {
//            var fullName = "";
//            var fname = $("#fname").val();
//            var mname = $("#mname").val();
//            var lname = $("#lname").val();
//            var phone = $("#mobile").val();
//            var email = $("#email").val();
//            var formNumber = '$ {formNumber}';
//            var jetpriviligemembershipNumber = $("#jetpriviligemembershipNumber").val();
//            if (fname != "" || mname != "" || lname != "")
//                fullName = fname + " " + mname + " " + lname;
//            $("#socialName").val(fullName);
//            $("#socialPhone").val(phone);
//            $("#socialEmail").val(email);
//            $("#socialjpNumber").val(jetpriviligemembershipNumber);
//            $("#formNumber").val(formNumber);
//            //$('#callMe_modal').modal('show');
//        });
        $('#callMeContine').on('click', function () {

            if ($("#callMe").validate().element('#socialName')
                    && $("#callMe").validate().element('#socialPhone')
                    && $("#callMe").validate().element('#socialEmail')
//                    && $("#callMe").validate().element("#socialjpNumber")
                    )
            {
             
            	console.log("data in call loader me");
            	console.log("this loader is runnung");
            	alert("sorry please loadr try again");
            	$('#loader').removeClass('hidden');
                $('.modal-backdrop').css('z-index', '1050');
                $.ajax({
                    url: '${applicationURL}callme',
                    data: $('#callMe').serialize(),
                    type: 'POST',
                    success: function (data) {
                        if (data != '') {
//                            console.log("data in call me", data);
                            $('#callMe_modal').modal('hide');
                            $('#successCallmeModal').modal('show');

                        } else {
                            $('#loader').addClass('hidden');
//                            alert("sorry please try again");
                            $('.modal-backdrop').css('z-index', '1048');
                        }
                        
                        //Adobe code start (phase 6)
                        assistFormSubmitButton($("#socialName").val(),$("#socialPhone").val(),$("#socialEmail").val());
                        //Adobe code end (phase 6)
                    },
                    complete: function () {
                        $('#loader').addClass('hidden');
                        $('.modal-backdrop').css('z-index', '1048');
                        $("#socialName").val('');
                        $("#socialPhone").val('');
                        $("#socialEmail").val('');
                    },
                    error: function (xhr, status) {
                        console.log(status);
                        console.log(xhr.responseText);
                    }
                });
            } else {
                $('.error_box1').removeClass('hidden');

            }
        });

        var nameflag, phoneflag, emailFlag;
        $("#social-profiles").on('click', function () {
            var fullName = "";
            var fname = $("#fname").val();
            var mname = $("#mname").val();
            var lname = $("#lname").val();
            var phone = $("#mobile").val();
            var email = $("#email").val();
            var socialName = $("#socialName").val();
            var socialPhone = $("#socialPhone").val();
            var socialEmail = $("#socialEmail").val();
//          
            var formNumber = '${formNumber}';
            var cardName = '${cardBean.cardName}';

            if (fname != "" || mname != "" || lname != "")
                fullName = fname + " " + mname + " " + lname;
            if ($("#socialName").val() != "") {

                $('#socialName').click(function () {
                    nameflag = 1;
                });

                if (nameflag != 1) {

                    $("#socialName").val(fullName);
                } else {

                    $("#socialName").val(socialName);
                }


            } else {
                $("#socialName").val(fullName);

            }
            if ($("#socialPhone").val() != "") {
//                $("#socialPhone").val(socialPhone);

                $('#socialPhone').click(function () {
                    phoneflag = 1;
                });
                if (phoneflag != 1) {
                    $("#socialPhone").val(phone);
                } else {

                    $("#socialPhone").val(socialPhone);
                }
            } else {
                $("#socialPhone").val(phone);

            }
            if ($("#socialEmail").val() != "") {

                $('#socialEmail').click(function () {
                    emailFlag = 1;
                });
                if (emailFlag != 1) {

                    $("#socialEmail").val(email);
                } else {
                    $("#socialEmail").val(socialEmail);
                }


            } else {
                $("#socialEmail").val(email);

            }
//            $("#socialjpNumber").val(jetpriviligemembershipNumber);
            $("#formNumber").val(formNumber);
            $("#pageName").val("ICICI Form Page");
            $("#cardName").val(cardName);
//            $('#callMeContine').attr('disabled', true)

            $('#callMe_modal').modal('show');
//            if ($("#socialPhone").val() == "" || $("#socialName").val() == "") {
////                console.log("****")
//                $('#callMeContine').attr('disabled', true)
//            } else {
////                console.log("&&&&&&")
//                $('#callMeContine').attr('disabled', false)
//
//            }

        });
        $('#callmeclose').click(function () {
            $('.error_box1').addClass('hidden');
        })
        $("#enrollMname").on('focusout', function () {
            console.log("in focusout enrollDob")

//            $("#quickEnrollUser").validate().element('#enrollDob');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollDob").on('focusout', function () {
            console.log("in focusout enrollDob")

            $("#quickEnrollUser").validate().element('#enrollDob');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $(".ui-datepicker-trigger").click(function () {
            $('.error_box2').addClass('hidden');

        })
        $("#termsAndConditionsEnroll").on('focusout', function () {
            console.log("in focusout termsAndConditionsEnroll")

            $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollemail").on('focusout', function () {
            console.log("in focusout enrollemail")

            $("#quickEnrollUser").validate().element('#enrollemail');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollPhone").on('focusout', function () {
            console.log("in focusout enrollPhone")

            $("#quickEnrollUser").validate().element('#enrollPhone');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollLname").on('focusout', function () {
            console.log("in focusout enrollLname")

            $("#quickEnrollUser").validate().element('#enrollLname');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollTitle").on('focusout', function () {
            console.log("in focusout enrolltitle")

            $("#quickEnrollUser").validate().element('#enrollTitle');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollCity").on('focusout', function () {
            console.log("in focusout enrollcity")

            $("#quickEnrollUser").validate().element('#enrollCity');
            $('.error_box2').removeClass('hidden');

            var len = $('.error_list2 > li:visible').length;

            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }
        });
        $("#enrollFname").on('focusout', function () {
            console.log("in focusout enrollFname")

            $("#quickEnrollUser").validate().element('#enrollFname');
            $('.error_box2').removeClass('hidden');

            var len = $('.error_list2 > li:visible').length;

            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }
        });
        $("#socialName").on('focusout', function () {
            console.log("in focusout socialName")

            $("#callMe").validate().element('#socialName');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }


        });
        $("#socialPhone").on('focusout', function () {
            console.log("in focusout socialPhone")

            $("#callMe").validate().element('#socialPhone');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;

            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }
        });
        $("#socialEmail").on('focusout', function () {
            console.log("in focusout socialEmail")

            $("#callMe").validate().element('#socialEmail');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;

            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }
        });

//        $("#enrollTitle,#enrollCity,#enrollFname,#enrollLname,#enrollPhone,#enrollemail,#termsAndConditionsEnroll").on('focus', function () {
//
//            $('.error_box2').addClass('hidden');
////            
//        })
//        $("#socialName,#socialPhone,#socialEmail").on('focus', function () {
//
//            $('.error_box1').addClass('hidden');
////            
//        })
//        $("#socialName,#socialPhone,#socialEmail").on('focusout', function () {
//            if ($("#socialPhone").val() == "" || $("#socialName").val() == "") {
////                console.log("***!!!!*")
//                $('#callMeContine').attr('disabled', true)
//            } else {
////                console.log("&&&!!!!&&&")
//                $('#callMeContine').attr('disabled', false)
//
//            }
//        });
//        $("#socialName,#socialPhone,#socialEmail").on('input change', function () {
//            if ($("#socialName").is(':focus') || $("#socialPhone").is(':focus') || $("#socialEmail").is(':focus')) {
//                if ($("#socialPhone").val() == "" || $("#socialName").val() == "") {
////                    console.log("*")
//                    $('#callMeContine').attr('disabled', true)
//                } else {
////                    console.log("&")
//                    $('#callMeContine').attr('disabled', false)
//
//                }
//            }
//        });

//        $("#enrollTitle,#enrollCity,#enrollFname,#enrollLname,#enrollPhone,#enrollemail,#termsAndConditionsEnroll").on('focus', function() {
//            console.log("in focu of enroll")
//            $('.error_box2').addClass('hidden');
////            
//        })
        $("#callMe").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                socialName: {
                    required: true,
//                    AlphabetsOnly:true
                },
                socialPhone: {
                    required: true,
//                    digits: true,
//                    mobileValidation: tCheckAddressrue
                },
                socialEmail: {
//                    required: true,
                    emailValidation: true
                }
//                socialjpNumber:{
//                digits:true
//                }
            },
            messages: {
                socialName: {
                    required: "Please Enter Full Name",
//                    AlphabetsOnly:"Enter Characters Only"
                },
                socialPhone: {
                    required: "Please Enter Mobile Number",
//                    digits: "Mobile Number is not valid.",
//                    mobileValidation: "Please Enter Valid Mobile Number"
                },
                socialEmail: {
//                    required: "Please Enter Email Id",
                    emailValidation: "Email ID is not valid."
                }
//                ,
//                socialjpNumber:{
//                digits:"Please Enter valid Jetprivilege Number."
//                }
            },
            errorElement: "div",
            onfocusout: function (elementCheckAddress) {
                this.element(element);  // <- "eager validation"                   
            },

            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },
            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                $('.error_box1').removeClass('hidden');
                error.insertAfter($(element).closest("div"));
                $(error).appendTo('.error_box1 ul');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');
                
                //Adobe code starts here(phase 6)
                assistError = assistError+error.text()+",";
                $.fn.AssistMeError(assistError);
                console.log("Error at validator !!!",assistError);
               //Adobe code ends here(phase 6)
            },
            submitHandler: function (form) {
                form.submit();
            },
        });


        $('#enrollForm1,#enrollForm_mob_view1').on('click', function () {
            $(".error_box2").removeClass('hidden');

            var fees, cardname, cardimg, bankName;
            var id = $("#cpNo").val();
            var bpNo = '${cardBean.bpNo}';
            if ($("#quickEnrollUser").validate().element('#enrollTitle') && $("#quickEnrollUser").validate().element('#enrollFname')
                    && $("#quickEnrollUser").validate().element('#enrollLname') && $("#quickEnrollUser").validate().element('#enrollCity')
                    && $("#quickEnrollUser").validate().element('#enrollPhone') && $("#quickEnrollUser").validate().element('#enrollemail')
                    && $("#quickEnrollUser").validate().element('#enrollDob') && $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll'))
            {
                var data;


                var responsecode = $('#g-recaptcha-response').val()

                $.ajax({method: 'POST',
                    url: '${applicationURL}verifycaptcha?responsecode=' + responsecode, success: function (result) {


                        data = result;
                        if (data == false) {


                            $('.error_box2').removeClass('hidden');
//                            $('#error').html("Please select captcha").appendTo('.error_box1 ul.error_list1');
                            $('.error_box2 ul.error_list2').html("Please select captcha");
                            $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');
                        } else {

                            $('.error_box2').addClass('hidden');
                            $('.error_box2 ul.error_list2').html("");
                            //password hashing//

                            $("#enrollbpNo").val(bpNo);
                            //password hashing//

                            $('#enroll_error').addClass('hidden');
                            
                            console.log("data in call loader2 me");
                        	console.log("this loader is 2runnung");
                        	alert("sorry please loadr2 try again");
                            
                            $('#loader').removeClass('hidden');
                            $('.modal-backdrop').css('z-index', '1050');
                            var checkbox = $("#termsAndConditionsEnroll");
                            checkbox.val(checkbox[0].checked ? "true" : "false");
                            var checkbox2 = $("#recieveMarketingTeam");
                            checkbox2.val(checkbox2[0].checked ? "true" : "false");
//                       console.log("1"+$( "#termsAndConditionsEnroll" ).val());
//                       console.log("2"+$( "#recieveMarketingTeam" ).val());
                            $.ajax({
                                url: '${applicationURL}enrollme',
                                data: $('#quickEnrollUser').serialize(),
                                type: 'POST',
                                success: function (data) {
                                    var jsonStr = JSON.parse(data);
                                    if (jsonStr.msg == "" && jsonStr.jpNumber != "" && jsonStr.jpNumber != "null") {
                                        var radioValue = $("#quickEnrollUser input[type='radio']:checked").val();
                                        $("#title").val($("#enrollTitle").val());
                                        $("#gender").val(radioValue);
                                        $("#fname").val($("#enrollFname").val());
                                        $("#mname").val($("#enrollMname").val());
                                        $("#lname").val($("#enrollLname").val());
                                        $("#email").val($("#enrollemail").val());
                                        $("#mobile").val($("#enrollPhone").val());
                                        $("#dateOfBirth").val($("#enrollDob").val());
                                        $("#jpNumber").val(jsonStr.jpNumber);
                                        $("#jetpriviligemembershipNumber").val(jsonStr.jpNumber);
                                        otpCounter = 0
                                        $('#otpButton').text("Generate OTP");
                                        $('#otpSucces').hide();
                                        $("#otpValue").attr('disabled', true);
                                        $('#jpNumpopup').attr('disabled', true);
                                        $("#jpNumber").attr('disabled', false);

                                        $(".error_msg_otp").html("");
                                        $(".error_msg_otp").addClass("hidden");
                                        $(".geninput").val("");
                                        $(".geninput").attr("disabled", true);
                                        $("#cpNo").val(id);
                                        $('.enroll_popup').addClass('hidden');
                                        $('.jpNum_popup').removeClass('hidden');
                                        $("#jpNumber").trigger('keyup');
                                        $('form#iciciBean').submit();

                                        //Adobe code starts here(Quick enrollment continue button)
                                        successEnrolClick();
                                        //Adobe code ends here

                                    } else {
                                        $('#loader').addClass('hidden');
                                        $('#enroll_error').removeClass('hidden');
                                        if (jsonStr.msg == "")
                                            $('#enroll_error').text("Your application may not have gone through successfully. Please click 'Continue' again.");
                                        else
                                            $('#enroll_error').text(jsonStr.msg);
                                        $('.modal-backdrop').css('z-index', '1048');
                                    }
                                },
                                complete: function () {
                                    $('#loader').addClass('hidden');
                                    $('.modal-backdrop').css('z-index', '1048');
                                },
                                error: function (xhrcallMe, status) {

                                }
                            });
                        }
                    },
                    error: function (e) {
                        console.log("error---->" + e);
                    }
                });
            } else {

                $('.error_box2').removeClass('hidden');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');

            }
        });






        $(".title_select").change(function () {
            if ($(this).find(":selected").text() == "Mr") {
                $("#quickEnrollUser input[type=radio][value=0]").prop('checked', true);

            }
            if ($(this).find(":selected").text() == "Ms" || $(this).find(":selected").text() == "Mrs") {
                $("#quickEnrollUser input[type=radio][value=1]").prop('checked', true);
            }
        });

        $('#enrollForm1,#enrollForm_mob_view1').on('click', function () {
            var len = $('.error_list2 > li:visible').length;
            if (len >= 1)
                $('.modal').animate({scrollTop: 0}, 'slow');
        });

        function maxLength(value) {
            return /^.{1,24}$/.test(value);
        }

        function checkAddressLength(residentAddressField1, residentAddressField2, residentAddressField3) {
            if (residentAddressField1 != "" && !maxLength(residentAddressField1)) {
                $('.resedentialAddress-error-div').html('Address cannot be greater than 24 characters in each address line');
                $('.resedentialAddress-error-div').removeClass('hidden');
            }
            if (residentAddressField2 != "" && !maxLength(residentAddressField2)) {
                $('.resedentialAddress-error-div').html('Address cannot be greater than 24 characters in each address line');
                $('.resedentialAddress-error-div').removeClass('hidden');
            }
            if (residentAddressField3 != "" && !maxLength(residentAddressField3)) {
                $('.resedentialAddress-error-div').html('Address cannot be greater than 24 characters in each address line');
                $('.resedentialAddress-error-div').removeClass('hidden');
            }

        }


        // Code for checking Address
        $('form input, form select').focus(function () {

            //Adobe code start
            buttonClicked = false;
            //Adobe code ends 


            // for Resendetial Addresss
            residentAddressField1 = $('.residentAddressField1').val();
            residentAddressField2 = $('.residentAddressField2').val();
            residentAddressField3 = $('.residentAddressField3').val();
            city = $("#city").val();
            state = $("#state1").val();
            pinCode1 = $("#pinCodeICICI").val();
//                                             $('#resedentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
//            if (residentAddressField1 != "" || residentAddressField2 != "" || residentAddressField3 != "" || city != "" || state != "" || pinCode1 != "") {
//                $('#resedentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);
//            }
//
//            if ($('#resedentialAddress').val() != "" && residentAddressField2.trim().length == 0)
//            {
//                console.log("if form input")
//                $('.resedentialAddress-error-div').removeClass('hidden');
//                $('.resedentialAddress-error-div').text("Please provide your Current Residential Address");
//
//            }else{
//                 console.log("else form input");
//              $('.resedentialAddress-error-div').addClass('hidden');
//                $('.resedentialAddress-error-div').text("");
//  
//            }
//
           var lResAddrss = $('#resedentialAddress').val()
           var bRValue = /^[A-Za-z0-9#',\/\s]*$/.test(lResAddrss)
//          console.log('Residentail Address Valid (onfocus): '+bRValue);
           if (!bRValue)
           { 

               $('.resedentialAddress-error-div').removeClass('hidden');
               $('.resedentialAddress-error-div').text("Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hash,and apostrophe.");

			
           }
//            

            //Company Address

//            residentAddressField1 = $('.companyAddressField1').val();
//            residentAddressField2 = $('.companyAddressField2').val();
//            residentAddressField3 = $('.companyAddressField3').val();
//            pin = $("#OPincodeICICI").val();
//            city = $("#OCity").val();
//            state = $("#OState").val();
////                                            $('#companyAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//            if (residentAddressField1 != "" || residentAddressField2 != "" || residentAddressField3 != "" || city != "" || state != "" || pin != "") {
//                $('#companyAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//            }
//            if ($('#companyAddress').val() != "" && residentAddressField2.trim().length == 0)
//            {
//                $('.companyAddr_div-error-div').removeClass('hidden');
//                $('.companyAddr_div-error-div').text("Please provide your company address");
//
//            }
//            var lComAddrss = $('#companyAddress').val()
//            var bCValue = /^[A-Za-z0-9#',\/\s]*$/.test(lComAddrss)
////          console.log('Company Address Valid (onfocus): '+bCValue);
//          
//            if (!bCValue)
//            {
//                $('.companyAddr_div-error-div').removeClass('hidden');
//                $('.companyAddr_div-error-div').text("Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hash,and apostrophe.");
//
//
//            }


            // PermananetAddress
            residentAddressField1 = $('.permanent_AddressField1').val();
            residentAddressField2 = $('.permanent_AddressField2').val();
            residentAddressField3 = $('.permanent_AddressField3').val();
            pin = $("#PpinCodeICICI").val();
            city = $("#PCity").val();
            state = $("#PState").val();
//                                            $('#permanentAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
            if (residentAddressField1 != "" || residentAddressField2 != "" || residentAddressField3 != "" || city != "" || state != "" || pin != "") {
                $('#permanentAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
            }

            if (radioValue === 'No')
            {
//                if ($('#permanentAddress').val() != "" && residentAddressField2.trim().length == 0)
//                    {   
//                    $('.permanentAddr-error-div').removeClass('hidden');
//                    $('.permanentAddr-error-div').text("Please provide your Permanent Residential Address");
//                    }
            }

            var lPResAddrss = $('#permanentAddress').val()
            var bPRValue = /^[A-Za-z0-9#',\/\s]*$/.test(lPResAddrss)
//          console.log('Permanant Residentail Address Valid (onfocus): '+bPRValue);

            if (!bPRValue)
            {
                var radioValue = $(".addressProof input[type='radio']:checked").val();
                if (radioValue === 'No')
                {
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hash,and apostrophe.");

                }
            }


        });

        var i = 0;
        $('.submit_btn').click(function () {
        	
			var loginUser = '${sessionScope.loggedInUser}';
			let str = loginUser;
			let n = 2;
			str = str.substring(n);
	
			var jpNo = str;
			if(jpNo >= 9)
				{
				$(".login-account").hide();
				$(".login-line").hide();
				}else{
					$(".login-account").show();
					$(".login-line").show();
				} 
	 
            
            //Adobe code starts here for error tracking on submit button(changes in phase 5)
            buttonClicked = true
            var sendError;
            iciciSignUpFlow();
             
            
            sendError = setTimeout(function () {
                $('.error').each(function () {
                    if (!$(this).hasClass('hidden'))
                    {
                     //alert("is hide")
                    	if ($(this).text().length > 0) {
                            if (errorTrack.length > 0)
                            {
                                errorTrack = errorTrack + ", " + $(this).text();
                            } else
                            {
                                errorTrack = $(this).text();
                            }
                        }
                    }
                })
 
                if(i==1)
               	{
                	errorTrack='';
               	}
                // Adobe Code validation error function
                if (errorTrack.length > 0) {
                    if (IsButtonClicked !== true)
                    {
                        i = i+1;
                    	$.fn.ErrorTracking(errorTrack, " ");
                    }
                } else
                {
                 //alert("succ");
                 //iciciSignUpFlow();
                 generateOTP();
                	console.log("** NO ERROR")
                }
                //clearTimeout(sendError);

            }, 3000);

            //Adobe code ends here

           //checkpermanent();
           //checkresident();
           //$("#promocode").trigger("focusout");
//            checkadress();
           // $(document).trigger('click'); 
            //CheckAddress('RES');
            //CheckAddress('PRES');
            //CheckAddress('COMP');

        });

    });

        
    function iciciSignUpFlow()
    {
    	var mobFrmLogin = $("#mobileNoFrmLogin").val();
    	var mobFrmForm = $("#mobile").val();
    	
    	var imno = $("#jpNumber").val();
    	
    	ckeckIfLogin(imno); 
    	
        var signupFlg = getCookie("isSignupICICI");
        var signupLoginFlw = false;
        
        if(imno >= 9 && signupFlg != "Y"){
      		signupLoginFlw = true;
      	  } 
        
        
        if((mobFrmLogin != mobFrmForm) || signupLoginFlw) 
  	  	{
//         	 $('#otpButton').attr('disabled', false);
//              $("#otpButton").css("color", "#fff");
//              var genOTP = document.getElementsByClassName('genotp');
//              for (var i = 0; i < genOTP.length; i ++) {
//             	 genOTP[i].style.display = 'block';
//              }
             
             $('#jpNumber').attr('readonly', false);
  	  	}
        else{
//         	$('#otpButton').attr('disabled', true);
//             $("#otpButton").css("color", "#ddd");
// 			$('.geninput').attr('disabled', true);
// 			var genOTP = document.getElementsByClassName('genotp');
//             for (var i = 0; i < genOTP.length; i ++) {
//            	 genOTP[i].style.display = 'none';
//             } 
             
            $(".login-line").hide();  
            $(".login-account").hide(); 
            $('#jpNumber').attr('readonly', true);
            
            $('#jpNumpopup').attr('disabled', false);
            $('#jpNumpopup').removeClass('genebutton_HDFC');
            $('#jpNumpopup').addClass('genebutton-HDFC');
//             $("#jpNumpopup").css("color", "#fff");
        }
    }
    
    var otpCounter = 0;
    var isCorrectOtp = false;
	/*
	 * timeout functions for otp fiels disable.
	 **/
	var myVar, mNumber, timerId, time, successMsg;
	function startTimeout() {
	    myVar = setTimeout(function () {
	        $("#otpValue").val('');
	
	        $("#otpValue").attr('disabled', true);
	
	        //Adobe phase 5 code starts here
	        $.fn.errorTrackOnPopup("OTP Expired");
	        //Adobe phase 5 code ends here
	
	    }, 900000);
	}
	
	function stopTimeout() {
	    clearTimeout(myVar); 
	}
	
    function generateOTP(){

//     	$(".genbut").click(function (event) { 
            $(".error_box2").addClass('hidden');
            otpCounter++;
            //event.preventDefault();
            var mobileNumber = $('#mobile').val();
            var formNumber = '${formNumber}';

            if (otpCounter > 3) {
                $('#otpValue').attr('disabled', true);
                $('#otpSucces').show();
                if (beJpNumber != "") {
                    $('#otpSucces').html("Kindly log into your Jet Privilege membership account and check your mobile number.");
                } else {
                    $('#otpSucces').html("Please Check Mobile Number.");
                }
                $('#jpNumpopup').attr('disabled', true);
                $('#jpNumpopup').removeClass('genebutton-HDFC');
                $('#jpNumpopup').addClass('genebutton_HDFC');
//                 $("#jpNumpopup").css("color", "#ddd");
//                 $('#otpButton').attr('disabled', true);
//                 $("#otpButton").css("color", "#ddd");
                $(".error_msg_otp").addClass("hidden");
                var imageUrl = ctx + "static/images/icons/error_icon.png";
                $(".otpsuccess").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
            } else {
                 //else {
                    $.ajax({url: "${applicationURL}generateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber,
                        async: false,
                        success:
                                function (data) {
                                    stopTimeout();
                                    startTimeout();
                                    var myObj = JSON.parse(data);
                                    mNumber = myObj.mobileNumber;
                                    time = myObj.Time;
                                    var otpTransactionId = myObj.otpTransactionid;
                                    var token = myObj.token;
                                    console.log("token-reger-->", token);
                                    $("#pendingAttempt").val("");
                                    if ($("#pendingAttempt").val() != "") {
                                        if (beJpNumber != "") {
                                            var mNumberSet1 = mNumber.substring(2, 8);
                                            console.log("mNumberSet1", mNumberSet1);
                                            mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
                                            var mNumberSet0 = mNumber.substring(0, 2);
                                            console.log("mNumberSet0", mNumberSet0);
                                            var mNumberSet2 = mNumber.substring(8);
                                            console.log("mNumberSet2", mNumberSet2);
                                            var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
                                            successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                        } else {
                                            successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                        }

                                    } else {
                                        if (beJpNumber != "") {
                                            var mNumberSet1 = mNumber.substring(2, 8);
                                            console.log("mNumberSet1", mNumberSet1);
                                            mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
                                            var mNumberSet0 = mNumber.substring(0, 2);
                                            console.log("mNumberSet0", mNumberSet0);
                                            var mNumberSet2 = mNumber.substring(8);
                                            console.log("mNumberSet2", mNumberSet2);
                                            var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
                                            successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '.';

                                        } else {
                                            successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                        }

                                    }
                                    if (mNumber != "" && time != "") {
                                        var chkregenerateFlag = true;
//                                            var successMsg;
                                        $("#otpTransactionId").val(otpTransactionId);
                                        $("#token").val(token);
                                        console.log("otpTransactionId=generate===>", $("#otpTransactionId").val());
                                        console.log("token==generate==>", $("#token").val());
                                        var timeLeft = 30;
                                        timerId = setInterval(countdown, 1000);
                                        var activemsg;
                                        function countdown() {
                                            if (timeLeft == -1) {
                                                clearTimeout(timerId);
                                                clearTimeout(timerId);
                                                
                                                $('#otpactive').hide();
                                                $('#otpactive').html('');
                                                $('.otpactive').hide();                           
//                                                 $('#otpButton').attr('disabled', false);
//                                                 $("#otpButton").css("color", "#fff");
                                                $(".resend_otp").removeClass("disabledDiv");
  	  	                                      $("#resendId").addClass("active");
                                            } else {
                                                activemsg = 'In case you have not received OTP please try regenerating OTP after ' + timeLeft + ' seconds ';
                                                $('.otpactive').show();
                                                $('#otpactive').show();
                                                $('#otpactive').html(activemsg);
//                                                 $('#otpButton').attr('disabled', true);
//                                                 $("#otpButton").css("color", "#ddd");
                                                timeLeft--;
                                                $(".resend_otp").addClass("disabledDiv");
  	  	                                      $("#resendId").removeClass("active");
                                            }
                                        }
                                        $('.geninput').attr('disabled', false);
//                                         $('#otpButton').text('Re-Generate OTP');
                                        $('#jpNumpopup').attr('disabled', true);
                                        $('#jpNumpopup').removeClass('genebutton-HDFC');
     	                                $('#jpNumpopup').addClass('genebutton_HDFC');
//                                         $("#jpNumpopup").css("color", "#ddd");
                                        $('#otpSucces').html(successMsg);
                                        $('#otpSucces').show();
                                        var imageUrl = ctx + "static/images/icons/success_icon.png";
                                        $("#otpSucces").css({"border": "2px solid #7dd62e", "background": "#f2fbe2 url(" + imageUrl + ") no-repeat 5px"});

                                    } else {
                                        otpCounter = 0;
                                    }
                                }
                    });
                //}
            }
//         });
    }
    
 // Regenerate OTP
	   $(".regenerateLink").click(function () {
		  // if ($('#otpButton').text() == 'Re-Generate OTP') {
              event.preventDefault();
            var mobileNumber = $('#mobile').val();
            var formNumber = '${formNumber}';
			  console.log("otpTransactionId=generate===>", $("#otpTransactionId").val());
               console.log("token==generate==>", $("#token").val());

               $.ajax({url: "${applicationURL}regenerateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&token=" + $("#token").val() + "&otpTransactionId=" + $("#otpTransactionId").val(),
                   async: false, 
                   success:
                           function (data) {
                               stopTimeout();
                               startTimeout();
                               var myObj = JSON.parse(data);
                               mNumber = myObj.mobileNumber;
                               time = myObj.Time;
                               var otpTransactionId = myObj.otpTransactionid;
                               var token = myObj.token;
                               console.log("token-reger-->", token);
                               $("#pendingAttempt").val("");
                               if ($("#pendingAttempt").val() != "") {
                                   if (beJpNumber != "") {
                                       var mNumberSet1 = mNumber.substring(2, 8);
                                       console.log("mNumberSet1", mNumberSet1);
                                       mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
                                       var mNumberSet0 = mNumber.substring(0, 2);
                                       console.log("mNumberSet0", mNumberSet0);
                                       var mNumberSet2 = mNumber.substring(8);
                                       console.log("mNumberSet2", mNumberSet2);
                                       var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
                                       successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                   } else {
                                       successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                   }

                               } else {
                                   if (beJpNumber != "") {
                                       var mNumberSet1 = mNumber.substring(2, 8);
                                       console.log("mNumberSet1", mNumberSet1);
                                       mNumberSet1 = mNumberSet1.replace(mNumberSet1, "xxxxxx");
                                       var mNumberSet0 = mNumber.substring(0, 2);
                                       console.log("mNumberSet0", mNumberSet0);
                                       var mNumberSet2 = mNumber.substring(8);
                                       console.log("mNumberSet2", mNumberSet2);
                                       var finalmobile = mNumberSet0 + mNumberSet1 + mNumberSet2;
                                       successMsg = 'OTP has been sent to your mobile number ' + finalmobile + ' at ' + time + '.';

                                   } else {
                                       successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                   }

                               }
                               if (mNumber != "" && time != "") {
                                   var chkregenerateFlag = true;
//                                       var successMsg;
                                   $("#otpTransactionId").val(otpTransactionId);
                                   $("#token").val(token);
                                   var timeLeft = 30;
                                   timerId = setInterval(countdown, 1000);
                                   var activemsg;
                                   function countdown() {
                                       if (timeLeft == -1) {
                                           clearTimeout(timerId);
                                           clearTimeout(timerId);
                                           $('#otpactive').hide();
                                           $('#otpactive').html('');
//                                            $('#otpButton').attr('disabled', false);
//                                            $("#otpButton").css("color", "#fff");
                                           $(".resend_otp").removeClass("disabledDiv");
                                             $("#resendId").addClass("active");
                                       } else {
                                           activemsg = 'In case you have not received OTP please try regenerating OTP after ' + timeLeft + ' seconds ';
                                           $('#otpactive').show();
                                           $('#otpactive').html(activemsg);
//                                            $('#otpButton').attr('disabled', true);
//                                            $("#otpButton").css("color", "#ddd");
                                           timeLeft--;
                                           $(".resend_otp").addClass("disabledDiv");
                                             $("#resendId").removeClass("active");
                                       }
                                   }
                                   $('.geninput').attr('disabled', false);
//                                    $('#otpButton').text('Re-Generate OTP');
                                   $('#jpNumpopup').attr('disabled', true);
                                   $('#jpNumpopup').removeClass('genebutton-HDFC');
	                                $('#jpNumpopup').addClass('genebutton_HDFC');
//                                    $("#jpNumpopup").css("color", "#ddd");
                                   $('#otpSucces').html(successMsg);
                                   $('#otpSucces').show();
                                   var imageUrl = ctx + "static/images/icons/success_icon.png";
                                   $("#otpSucces").css({"border": "2px solid #7dd62e", "background": "#f2fbe2 url(" + imageUrl + ") no-repeat 5px"});

                               } else {
                                   otpCounter = 0;
                               }
                           }
               });
           
	   });
    

    $('#jp_btn').click(function () {
// new cr satrt
        //  $('#totalExperience-error').addClass('hidden');
        $('#totalExperience-error').hide();
        if ($('.age_error_div').is(':visible')) {

            $("html, body").animate({scrollTop: 400}, 600);
            return false;
        } else
        if ($("#dateOfBirth-error").is(':visible') || $('.fullName-error-div').is(':visible') || $("#mobile-error").is(':visible') || $("#email-error").is(':visible') || $("#gender-error").is(':visible') || $("#fullName-error").is(':visible') || $(".promocodeError").is(':visible')) {

            $("html, body").animate({scrollTop: 500}, 600);
            return false;
        } else
        if ($("#monthlyIncome-error").is(':visible') || $("#pancard-error").is(':visible') || $(".resedentialAddress-error-div").is(':visible')) {

            $("html, body").animate({scrollTop: 700}, 600);
            return false;
        } else
        if ($(".permanentAddr-error-div").is(':visible') || $("#employmentType-error").is(':visible') || $("#ITRStaus-error").is(':visible')) {

            $("html, body").animate({scrollTop: 1200}, 600);
            return false;
        } else
        if ($("#iciciBankRelationship-error").is(':visible') || $("#ICICIBankRelationShipNo-error").is(':visible') || $("#salaryAccountOpened-error").is(':visible')
                || $("#salaryBankWithOtherBank-error").is(':visible')) {

            $("html, body").animate({scrollTop: 1500}, 600);
            return false;
        } else
        if ($("#companyName-error").is(':visible') || $('.total_div-error-div').is(':visible')) {

            $("html, body").animate({scrollTop: 1800}, 600);
            return false;
        } else
        if ($("#companyAddress-error").is(':visible') || $("#homeNumber-error").is(':visible') || $('.homeNumber-error-div').is(':visible') ||
                $('.officeNumber-error-div').is(':visible') || $('.companyAddr_div-error-div').is(':visible') || $("#termsAndConditions-error").is(':visible')) {

            $("html, body").animate({scrollTop: 2000}, 600);
            return false;
        } else {

            $("html, body").animate({scrollTop: 2500}, 600);
            return false;
        }
        
        if ($(".resedentialAddress-error-div").is(':visible')) {
            $("html, body").animate({scrollTop: 700}, 600);
            return false;
        }
        
//   // new cr end
    });

    function CheckAddress(addrDetails)
    {
        //$(document).trigger("click");

        // $(document).trigger('click'); 

        if (addrDetails == 'RES')
        {
            residentAddressField1 = $('.residentAddressField1').val();
            residentAddressField2 = $('.residentAddressField2').val();
            residentAddressField3 = $('.residentAddressField3').val();
            city = $("#city").val();
            state = $("#state1").val();
            pinCode1 = $("#pinCodeICICI").val();
            $('#resedentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pinCode1);


            var lResAddrss = residentAddressField1;//$('#resedentialAddress').val()
            var bRValue = /^[A-Za-z0-9#',\/\s]*$/.test(lResAddrss)

            if (bRValue)
            {
                $('.resedentialAddress-error-div').html('');  //  CheckAddress('RES')
                $('.resedentialAddress-error-div').addClass('hidden');
            }
            
            // roshan 19jan
            var bRValue2 = /^[A-Za-z0-9#',\/\s]*$/.test(residentAddressField2)

            if (bRValue2)
            {
                $('.resedentialAddress-error-div').html('');  //  CheckAddress('RES')
                $('.resedentialAddress-error-div').addClass('hidden');
            }
            
            var bRValue3 = /^[A-Za-z0-9#',\/\s]*$/.test(residentAddressField3)

            if (bRValue3) 
            {
                $('.resedentialAddress-error-div').html('');  //  CheckAddress('RES')
                $('.resedentialAddress-error-div').addClass('hidden');
            }
            
            
            if (residentAddressField1 == "" || residentAddressField2 == "")
            {
//                console.log("*******************chck adres 1****************");
                $('.resedentialAddress-error-div').html('Please provide your Current Residential Address').css("color", "#ff0000");
                $('.resedentialAddress-error-div').removeClass('hidden');
            } else {
                $('.resedentialAddress-error-div').html('').css("color", "#ff0000");
                $('.resedentialAddress-error-div').addClass('hidden');
            }

        }
        if (addrDetails == 'PRES')
        {


            residentAddressField1 = $('.permanent_AddressField1').val();
            residentAddressField2 = $('.permanent_AddressField2').val();
            residentAddressField3 = $('.permanent_AddressField3').val();
            pin = $("#PpinCodeICICI").val();
            city = $("#PCity").val();
            state = $("#PState").val();
            $('#permanentAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

            if ($('#permanentAddress').val() == "" && residentAddressField2.trim().length == 0)
            {
                $('.permanentAddr-error-div').html('');  // CheckAddress('PRES');
                $('.permanentAddr-error-div').addClass('hidden');

            }
            var lPResAddrss = $('#permanentAddress').val()
            var bPRValue = /^[A-Za-z0-9#',\/\s]*$/.test(lPResAddrss)


            if (bPRValue)
            {

                $('.permanentAddr-error-div').html('');  // CheckAddress('PRES');
                $('.permanentAddr-error-div').addClass('hidden');

            }
            if (residentAddressField1 == "" || residentAddressField2 == "")
            {
//                console.log("*********check adres 2*******************");
                $('.permanentAddr-error-div').html('Please provide your Permanent Residential Address').css("color", "#ff0000");
                $('.permanentAddr-error-div').removeClass('hidden');
            } else {
                $('.permanentAddr-error-div').html('').css("color", "#ff0000");
                $('.permanentAddr-error-div').addClass('hidden');
            }
        }
        if (addrDetails == 'COMP')
        {

            residentAddressField1 = $('.companyAddressField1').val();
            residentAddressField2 = $('.companyAddressField2').val();
            residentAddressField3 = $('.companyAddressField3').val();
            pin = $("#OPincodeICICI").val();
            city = $("#OCity").val();
            state = $("#OState").val();
            $('#companyAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);


            var lComAddrss = $('#companyAddress').val()
            var bCValue = /^[A-Za-z0-9#',\/\s]*$/.test(lComAddrss)

            if (bCValue)
            {
                $('.companyAddr_div-error-div').html('');  // CheckAddress('COMP');
                $('.companyAddr_div-error-div').addClass('hidden');
            }
            if (residentAddressField1 == "" || residentAddressField2 == "")
            {
//                console.log("***************chk adrs 3*****************");
                $('.companyAddr_div-error-div').html('Please provide your company address').css("color", "#ff0000");
                $('.companyAddr_div-error-div').removeClass('hidden');
            } else {
                $('.companyAddr_div-error-div').html('').css("color", "#ff0000");
                $('.companyAddr_div-error-div').addClass('hidden');


            }
        }

    }
    function checkresident() {
        var residentAddressField1 = $('.residentAddressField1').val();
        var residentAddressField2 = $('.residentAddressField2').val();
        var residentAddressField3 = $('.residentAddressField3').val();
        if (residentAddressField1 != "") {
//                    var a =/^.{3,40}$/.test(lPResAddrss);
            var a = residentAddressField1.length;
//                    console.log("1--->"+a);
            if (a < 4 || a > 40) {
                $('.resedentialAddress-error-div').removeClass('hidden');
                $('.resedentialAddress-error-div').text("Address cannot be greater than 40 and less than 3 characters in each address line");

            }
        }
        if (residentAddressField2 != "") {
//                    var a =/^.{3,40}$/.test(lPResAddrss);
            var a = residentAddressField1.length;
//                    console.log("1--->"+a);
            if (a < 4 || a > 40) {
                $('.resedentialAddress-error-div').removeClass('hidden');
                $('.resedentialAddress-error-div').text("Address cannot be greater than 40 and less than 3 characters in each address line");

            }
        }
        if (residentAddressField3 != "") {
//                    var a =/^.{3,40}$/.test(lPResAddrss);
            var a = residentAddressField1.length;
//                    console.log("1--->"+a);
            if (a < 4 || a > 40) {
                $('.resedentialAddress-error-div').removeClass('hidden');
                $('.resedentialAddress-error-div').text("Address cannot be greater than 40 and less than 3 characters in each address line");

            }
        }
    }
    function checkpermanent() {
        var residentAddressField1 = $('.permanent_AddressField1').val();
        var residentAddressField2 = $('.permanent_AddressField2').val();
        var residentAddressField3 = $('.permanent_AddressField3').val();
        pin = $("#PpinCodeICICI").val();
        city = $("#PCity").val();
        state = $("#PState").val();
        var radioValue = $(".addressProof input[type='radio']:checked").val();
//             console.log("submit btn==="+radioValue);
        var lPResAddrss = $('#permanentAddress').val()
        if (radioValue === 'No')
        {
//                if ($('#permanentAddress').val() != "" && residentAddressField2.trim().length == 0)
//                    {   
//                    $('.permanentAddr-error-div').removeClass('hidden');
//                    $('.permanentAddr-error-div').text("Please provide your Permanent Residential Address");
//                    }


            if (residentAddressField1 != "") {
//                    var a =/^.{3,40}$/.test(lPResAddrss);
                var a = residentAddressField1.length;
//                    console.log("1--->"+a);
                if (a < 4 || a > 40) {
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Address cannot be greater than 40 and less than 3 characters in each address line");

                }
//                    else{
//                        
//                        $('.permanentAddr-error-div').addClass('hidden');
//                    $('.permanentAddr-error-div').text("");
//                    
//                    }
            }
            if (residentAddressField2 != "") {
//                    var b =/^.{3,40}$/.test(lPResAddrss);
                var b = residentAddressField2.length;
//                    console.log("1--->"+b);
                if (b < 4 || b > 40) {
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Address cannot be greater than 40 and less than 3 characters in each address line");

                }
//                    else{
//                        
//                        $('.permanentAddr-error-div').addClass('hidden');
//                    $('.permanentAddr-error-div').text("");
//                    }
            }
            if (residentAddressField3 != "") {
//                    var c =/^.{3,40}$/.test(lPResAddrss);
                var c = residentAddressField3.length;
//                    console.log("1--->"+c);
                if (c < 4 || c > 40) {
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $('.permanentAddr-error-div').text("Address cannot be greater than 40 and less than 3 characters in each address line");

                }
//                    else{
//                        $('.permanentAddr-error-div').addClass('hidden');
//                    $('.permanentAddr-error-div').text("");
//                    }
            }
        }
    }
    //Adobe code starts here
    function pageInfo() {
        
        //Adobe code (phase 6) start
        var callmeflag ='${callMeFlag}';
          if(callmeflag == 1){
              setTimeout(function()
              { 
                $.fn.ViewOfAssistMe() 
              }, 3000);
          }
        //Adobe code (phase 6) ends
        
        var iciciURL = document.URL.substr(document.URL.lastIndexOf('/') + 1);
        var iciciDetail = "";

        if (iciciURL === "Jet-Airways-ICICI-Bank-Coral-VISA-Credit-Card") {
            iciciDetail = "ICICI-Coral-VISA";
        } else if (iciciURL === "Jet-Airways-ICICI-Bank-Coral-AMEX-Credit-Card") {
            iciciDetail = "ICICI-Coral-AMEX";
        } else if (iciciURL === "Jet-Airways-ICICI-Bank-Rubyx-VISA-Credit-Card") {
            iciciDetail = "ICICI-Rubyx-VISA";
        } else if (iciciURL === "Jet-Airways-ICICI-Bank-Rubyx-AMEX-Credit-Card") {
            iciciDetail = "ICICI-Rubyx-AMEX";
        } else if (iciciURL === "Jet-Airways-ICICI-Bank-Sapphiro-VISA-Credit-Card") {
            iciciDetail = "ICICI-Sapphiro-VISA";
        } else if (iciciURL === "Jet-Airways-ICICI-Bank-Sapphiro-AMEX-Credit-Card") {
            iciciDetail = "ICICI-Sapphiro-AMEX";
        }

        //code for abandonment start
        selectedCity = $('#city').val();
        $('form input, form select').click(function () {
            lastAccessedField = $(this).attr('name');
        });
              //code for abandonment end
                    //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----applyicicicards--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----applyicicicards--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----applyicicicards--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----applyicicicards--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----applyicicicards--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----applyicicicards--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----applyicicicards--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----applyicicicards-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----applyicicicards--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End   



        digitalData.pageInfo = {"pageName": "" + iciciDetail, "category": "Cards", "subCategory1": "App-Form", "subCategory2": "" + iciciDetail, "partner": "JP", "currency": "INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
        digitalData.formInfo = {"formName": "ICICI card application form", "lastAccessedField": lastAccessedField, "name": "", "email": "", "mobile": "", "currentCity": "", "pincode": ""}

        //code for form abandonment
        var checkCloseX = 0;
        $(document).mousemove(function (e) {
            if (e.pageY <= 5) {
                checkCloseX = 1;
                formAbandonType = "Browser close";
            } else {
                checkCloseX = 0;
                formAbandonType = "Page Refresh";
            }
        });

        window.onbeforeunload = function (event) {
            
            if (checkCloseX == 1) {
            }
           
            if (!IsButtonClicked && assistMeFlag == false)
            {
                var bankName = "ICICI";
                $.fn.FormAbandonICICIClick(lastAccessedField, digitalData.formInfo.name, digitalData.formInfo.email, digitalData.formInfo.mobile, digitalData.formInfo.currentCity, digitalData.formInfo.pincode, bankName)

            }
            
            if (checkCloseX == 1) 
            {
                formAbandonType="Browser close";
            }   
                    
            if(IsButtonClicked)
              {
                console.log("--------formAbandonType is-----------"+formAbandonType);
                $.fn.formAbandonmentOnSubmitJPnumberPopupAmex(formAbandonType,abandonFormName,iciciCardName,iciciVariantName);
              }
              
              if(assistMeFlag == true)
             {
                 $.fn.FormAbandonmentOfAssistMe(name,mobile,email,lastAccessedField,"page refresh");
             }  

        };

        //Form Abandonment close
    }
    // Code For PageAbandon
    $('form input, form select').click(function () {
//                         console.log( "Apply Icici Card readyyyyyyyyyyyyyyyyyy!");
        var fname = $("#fname").val();
        var mname = $("#mname").val();
        var lname = $("#lname").val();
        var fullName = fname + " " + mname + " " + lname;
        var mobile = $("#mobile").val();
        var email = $("#email").val();
        var currentCity = $("#city").val();
        var pincode = $("#pinCodeICICI").val();

        var jsonNewData = {"name": fullName, "email": email, "mobile": mobile, "currentCity": currentCity, "pincode": pincode}
//         var encryptDetail = encryptedData(jsonNewData);
//         var encryptDetailJson = JSON.parse(encryptDetail);
        var encryptName = name; //encryptDetailJson.name;
        var encryptEmail = email; //encryptDetailJson.email;
        var encryptMobile = mobile; //encryptDetailJson.mobile;
        var encryptCity = currentCity; //encryptDetailJson.currentCity;
        var encryptPincode = pincode; //encryptDetailJson.pincode;

        digitalData.formInfo = {"formName": "ICICI card application form", "lastAccessedField": lastAccessedField, "name": encryptName, "email": encryptEmail, "mobile": encryptMobile, "currentCity": encryptCity, "pincode": encryptPincode}
    });

    //Adobe code ends here         

</script>
<!--Form Abandonment-->
<script type='text/javascript'>

    function submitIciciFormClick(fullName, email, mobile, city, pincode, bankName, popupType) {
        IsButtonClicked = true;
        abandonFormName = popupType;
        popupAbandonType = popupType;
       // $.fn.ApplyButtonAmexFormClick(fullName, email, mobile, city, pincode, bankName);
       
    }

    function afterApplyIciciSubmitClick(jpNumber) {
        IsButtonClicked = false;
        submitTime = new Date();
        var timeElapsed = Math.abs(submitTime - genTime);
        var diffCreatedMS = (submitTime - genTime) / 1000;
        var diffCreatedSec = diffCreatedMS % 60;
//        console.log("---diffCreatedSec----", diffCreatedSec)

        //$.fn.AfterApplyJPNumberPopUpClick(jpNumber, 'JP Number', diffCreatedSec);
    }

    function moreDetailsClick(cardName, variant) {
        var cardName1 = cardName.replace("�", "");
        var variant1 = variant.replace("�", "");
        $.fn.MoreDetailsForCardsClick(cardName1, variant1);
    }

    function afterEnrollHereClick(enrollName, popupType) {
        abandonFormName = popupType;
        popupAbandonType = popupType;
        $.fn.AfterApplyEnrolPopUpClick(enrollName);
        $.fn.AfterApplyEnrolPopUpClickCT();
    }

    function termsAndConditionClick(cardName, variant) {
        iciciCardName = cardName;
        iciciVariantName = variant;
    }

    function formAbandonmentOnPopUp(closeType) {
        $.fn.formAbandonmentOnSubmitJPnumberPopupAmex(closeType, popupAbandonType, iciciCardName, iciciVariantName);
    }

    function successEnrolClick() {
        var jpNum = $("#jpNumber").val();
        $.fn.successfulQuickEnrollment(jpNum, iciciCardName, iciciVariantName);
    }
 
    //Adobe phase 5 start
//     $("#otpButton").click(function () {
        
//     	validateOTPOnSubmit();
    	
//     	var buttonName = $("#otpButton").text();
//         genTime = new Date();
//         var jpNum = $("#jpNumber").val();
//         if (buttonName == "Generate OTP") {

//             //$.fn.ClicksOnGenerateOtp(jpNum, iciciCardName, iciciVariantName);
//         } else {
//             //$.fn.ClicksOnResendOtp(jpNum);
//         }

//     });
    //Adobe phase 5 end
    
    //phase 6 start
    //submit button assist me form
        function assistMeeClick(){
            assistMeFlag = true;
            $.fn.ClickOfAssistMe();
        }
     
        function assistFormSubmitButton(name,mobile,email){
            if(name !== "" && mobile !== ""){
                    name = "name:yes";
                    mobile = "mobile:yes";
                }else{
                    name = "name:no";
                    mobile = "mobile:no";
                }
                if(email !== ""){
                    email = "email:yes";
                }else{
                    email = "email:no";
                }
                $.fn.ClickOfAssistContinueButton(name,mobile,email);
        }
     
        function formAbandonOfAssistMe(){
            var abandonType = "pop-up close";
            if(lastAccessedField == "socialName"){
            lastAccessedField="Name";
            }else if(lastAccessedField == "socialPhone"){
               lastAccessedField="Mobile";
            }else if(lastAccessedField == "socialEmail"){
               lastAccessedField="Email";
            }
            $.fn.FormAbandonmentOfAssistMe(name,email,mobile,lastAccessedField,abandonType);
        }
        //phase 6 end


    /* function encryptedData(jsonToEncrypt) {
        var encryptedJson;
        $.ajax(
                {method: 'GET',
                    async: false,
                    url: ctx + "encryptedAdobe?jsonData=" + JSON.stringify(jsonToEncrypt),
                    success: function (result) {
                        encryptedJson = JSON.stringify(result);

                    }});

        return encryptedJson;

    } */
        
        
        
    // Icici new signup flow JS
    
    $(document).ready(function () {
        $("#logincredit").on('click', function () {
        window.MvpHeader.loginToggleModal();
        $("#provide_jpNumber").modal('hide');
        $("#OtpSuccPopup").modal('hide');
        })

        });

        $(document).ready(function () {
        var imno = $("#jpNumber").val();
        //alert("2nd"+imo);
        var mobLogin = $("#mobile").val();
      	$("#mobileNoFrmLogin").val(mobLogin);
      	//$("#mobileNoFrmLogin").val("9890240733");
     
        });
        
        
         function getClickVal(){
        	 var iniFormNo = '${formNumber}';
        	createCookie("isSignupICICI", "Y", 20);
        	createCookieFormNo("ICICI_FromNo", iniFormNo, 20);
         }
         
//          function loadSignup(){
//         	 var iniFormNo;
//              var signupFlg = getCookie("isSignupICICI");
//              alert("flg---"+signupFlg);
//              iniFormNo = '${formNumber}';
//              alert("form no--"+iniFormNo);
//                  if(signupFlg == "Y"){ 
//                		fetchFilledForm();
//                	  } 
//          }
         
         function createCookie(name,value,minutes) {
        	    if (minutes) {
        	        var date = new Date();
        	        date.setTime(date.getTime()+(minutes*60*1000));
        	        var expires = "; expires="+date.toGMTString();
        	    } else {
        	        var expires = "";
        	    }
        	    document.cookie = name+"="+value+expires+"; path=/"; 
        	} 
        
         function createCookieFormNo(name,value,minutes) {
     	    if (minutes) {
     	        var date = new Date();
     	        date.setTime(date.getTime()+(minutes*60*1000));
     	        var expires = "; expires="+date.toGMTString();
     	    } else {
     	        var expires = "";
     	    }
     	    document.cookie = name+"="+value+expires+"; path=/"; 
     	} 
         
         function ckeckIfLogin(jpNo)
         {
        	 if(jpNo >= 9)
        	    {
        	    //$(".login-line").hide();
        	    $(".login-account").hide();
        	    $('#jpNumber').attr('readonly', true);
        	    console.log("insixed if imno",$("#jpNumber").val())

        	    }else{
        	    $(".login-line").show();
        	    $(".login-account").show();
        	    $('#jpNumber').attr('readonly', false);
        	    console.log("inside else imno",$("#jpNumber").val())
        	    }
         }
         
         
         function getCookie(cname) {
       	  var name = cname + "=";
       	  var decodedCookie = decodeURIComponent(document.cookie);
       	  var ca = decodedCookie.split(';');
       	  for(var i = 0; i < ca.length; i++) {
       	    var c = ca[i];
       	    while (c.charAt(0) == ' ') {
       	      c = c.substring(1);
       	    }
       	    if (c.indexOf(name) == 0) {
       	      return c.substring(name.length, c.length);
       	    }
       	  }
       	  return "";
       	}
        
          
         //$("#permanentAddress").blur(function () {
//          	 $('.permanentAddr_div').removeClass('hidden');
//         	 $('.permanentAddr').removeClass('hidden');
//         	 $('.permanent_residentAddress').removeClass('hidden');
//         	 $('.permanentAddr-error-div').removeClass('hidden');
//         	 $('.permanentAddr-error-div').text('');
        	 
         //});
         
         $("#lname").blur(function () {
        	 var fName = $('.firstName').val();
             var mName = $('.middleName').val();
             var lName = $('.lastName').val(); 
        	 $('.name_inputBox').val(fName + " " + mName + " " + lName);
              $('.name_ul').addClass('hidden');
              $('.name_inputBox').removeClass('hidden');
              if (fName == "" && lName == "")
                  $('.name_inputBox').val('');
         }); 
         
         $("#PpinCodeICICI").blur(function () {
         	//$('.permanentAddr').addClass('hidden');
         	//$('.permanent_residentAddress').addClass('hidden'); 
         	$('.permanentAddr_div').addClass('hidden'); 
         	$('.permanentAddr-error-div').addClass('hidden');
         });
         
         
         
     	 
         $("#otpValue").keyup(function () {
	  	    	
	  	    	if ($("#otpValue").val().length == 6) {
	  	    		$("#otpError").html("");
	                $("#otpError").hide();
	                if ($("#jpNumber").val() != '' && isIMCorrect) {
	                	validateOTPOnSubmit(); 
	                }
// 	  	    		  $('#jpNumpopup').attr('disabled', false);
// 	  	        	  $('#jpNumpopup').removeClass('genebutton_HDFC'); 
// 	  	              $('#jpNumpopup').addClass('genebutton-HDFC');
	  	    	}
	  	    	else if ($("#otpValue").val().length != 6) {
	  	    		$('#jpNumpopup').attr('disabled', true);
	  	        	  $('#jpNumpopup').removeClass('genebutton-HDFC');
	  	              $('#jpNumpopup').addClass('genebutton_HDFC');
	  	    	}
	  	    });
         
         
         
         function validateOTPOnSubmit() 
         {
        	 if ($("#otpValue").val().length == 6) {
 	              var otpValue = $("#otpValue").val();
 	              if (otpValue == "") {
 	                  console.log("1");
 	                  $("#otpError").html("Please Enter OTP");
 	                  $("#otpError").show();
 	                  isCorrectOtp = false;
 	              } else if (otpValue.length != 6) {
 	                  console.log("2");
 	                  $("#otpError").html("Please Enter OTP");
 	                  $("#otpError").show();
 	                  isCorrectOtp = false;
 	              } else {
 	                  if (isNaN(otpValue) == true) {
 	                      console.log("3");
 	                      $("#otpError").html("Please Enter OTP");
 	                      $("#otpError").show();
 	                      isCorrectOtp = false;
 	                  } else {
        	
        	
        	var formNumber = '${formNumber}';
             var mobileNumber = $('#mobile').val();
                    var otpTransactionId = $("#otpTransactionId").val();
                    $.ajax({method: "GET", 
                        async: false,
                        url: "${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&otpTransactionId=" + otpTransactionId + "&otp=" + otpValue + "&token=" + $("#token").val(), success: function (data) 
                      {
                            console.log("=========validate======", data);

//                    $.get("${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + 2 + "&formNumber=" + formNumber, function (data, status) {
                            if (data.length > 2) {
                                var myObj = JSON.parse(data);

                                pendingAttempt = myObj.pendingAttempts;
                                $("#pendingAttempt").val(myObj.pendingAttempts);
                                console.log("pending attempt"+$("#pendingAttempt").val())
                                if ($("#pendingAttempt").val() == "Number of attempts exceeds limit") {
                                    $(".geninput").attr('disabled', true);
                                    $(".geninput").val("");
                                    $(".error_msg_otp").html(""); 
                                    $(".error_msg_otp").hide();
                                    successMsg="Left attempts are exceeded";
//                                    $('#otpSucces').html("Number of attempts exceeds limit");
                                    $('#otpSucces').removeClass('hidden');
//                                     $('#otpButton').attr('disabled', true);
//                                     $(".otpButton").addClass("hidden");
                                    
                                    $('#jpNumpopup').attr('disabled', true);
//                                     $("#jpNumpopup").css("color", "#ddd");
	                                $('#jpNumpopup').removeClass('genebutton-HDFC');
	                                $('#jpNumpopup').addClass('genebutton_HDFC');
                                    
	                                var imageUrl = ctx + "static/images/icons/error_icon.png";
                                    $(".otpsuccess").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
                                } else if ($("#pendingAttempt").val() != "") {
                                    console.log("=-=-=p-==-=-=>", $("#pendingAttempt").val());
                                    successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left ' + $("#pendingAttempt").val();

                                } else {
                                    successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                }
                                $('#otpSucces').html(successMsg);

                                if (myObj.OTP_Verified == "true") {
                               	 $(".error_msg_otp").html("");
                                    $(".error_msg_otp").addClass("hidden");
                                    $('#jpNumpopup').attr('disabled', false);
	  	                            $('#jpNumpopup').removeClass('genebutton_HDFC');
	  	                            $('#jpNumpopup').addClass('genebutton-HDFC');	
//                                   setTimeout(disableElement, 5*60*1000);
//                                   function disableElement(){
                                	 
//                                 	}
                                    isCorrectOtp = true;
                                    $('#otpVal').val(otpValue);
                                    //submitAPI();
                                    ////////
//                                     $("#provide_jpNumber").modal('hide');
//                        	    	  $("#OtpSuccPopup").modal('show');
//                        	    	 setTimeout(disableSuccPopup, 10000);
//                        	          function disableSuccPopup(){
//                        	        	  $("#OtpSuccPopup").modal('hide');
//                        	        	  form.submit();
//                        	        	}  
                       	          ///////
                                    
                                    
                                } else {
                                    $(".error_msg_otp").html("Please Enter Valid OTP");
                                    $(".error_msg_otp").removeClass("hidden");
                                    $('#jpNumpopup').attr('disabled', true);
//                                     $("#jpNumpopup").css("color", "#ddd");
										$('#jpNumpopup').removeClass('genebutton-HDFC');
	  	              					$('#jpNumpopup').addClass('genebutton_HDFC');
                                    isCorrectOtp = false;
                                    $('#otpVal').val('');
                                }
                            } else {
                                $(".error_msg_otp").html("Please Enter Valid OTP");
                                $(".error_msg_otp").removeClass("hidden");
                                $('#jpNumpopup').attr('disabled', true);
//                                 $("#jpNumpopup").css("color", "#ddd");
								$('#jpNumpopup').removeClass('genebutton-HDFC');
	  	              			$('#jpNumpopup').addClass('genebutton_HDFC');
                                isCorrectOtp = false;
                            }

                        }});
                       }
                    }
 	         }else{
 	        	 $(".error_msg_otp").html("Please Enter Valid OTP");
                 $(".error_msg_otp").removeClass("hidden");
                 $('#jpNumpopup').attr('disabled', true);
//                  $("#jpNumpopup").css("color", "#ddd");
					$('#jpNumpopup').removeClass('genebutton-HDFC');
	  	            $('#jpNumpopup').addClass('genebutton_HDFC');
                 isCorrectOtp = false;
 	         }
         }

         
   function fetchFilledForm(){
//////////////Fetch data after signup ///////////////
 		 var fomNo = getCookie("ICICI_FromNo");
 		//var iciciFormNumber = '${formNumber}';
 		 $.ajax({method: "GET",
               async: false,
               url: "${applicationURL}getDataPostSignup?formNo=" + fomNo, success: function (data) {
                   console.log("-------------------"+data);
               	$("#address").val(data.address);
                   $("#address2").val(data.address2);
                   $("#address3").val(data.address3);
                   $("#city").val(data.city);
                   $("#pinCode").val(data.pinCode);
                   $("#resedentialAddress").val(data.resedentialAddress);
                   $("#state1").val(data.state1);
                   
                   var date = new Date(data.dateOfBirth);
                   var newdate = date.toLocaleDateString().split("/").reverse().join("-");
                   var frmtDt = newdate.split(/\D/g);
                   var finalFrmtDt = [frmtDt[1],frmtDt[2],frmtDt[0] ].join("-");
                   
                   $("#dateOfBirth").val(finalFrmtDt);
                   $("#email").val(data.email); 
                   $("#fname").val(data.fname); 
                   $("#fullName").val(data.fullName);
                   $("#gender").val(data.gender);
                   $("#jetpriviligemembershipNumber").val(data.jetpriviligemembershipNumber);
                   $("#jetpriviligemembershipTier").val(data.jetpriviligemembershipTier);
                   $("#lname").val(data.lname);
                   $("#mname").val(data.mname);
                   $("#title").val(data.title);
                   $("#randomNumber").val('${beRandomNumber}');
                   $("#initiatedBy").val("Self via Marketing Campaign");
                   
                   
                   var genVal;
                   if(data.gender == "Male")
                   {
                	   genVal = "0";
                   }
                   if(data.gender == "Female")
                   {
                	   genVal = "1";
                   }
                   
                   $('#gender').val(genVal).attr("selected", "selected");

                   //$("#gender").val(data.gender);
                   $("#mobile").val(data.mobile);
                   
                   var eduVal;
                   if(data.eduQualification == "Under Graduate")
                   {
                	   eduVal = "U";
                   }
                   
                   if(data.eduQualification == "Graduate / Diploma")
                   {
                	   eduVal = "B";
                   }
                   if(data.eduQualification == "Post Graduate")
                   {
                	   eduVal = "M";
                   }
                   if(data.eduQualification == "Professional")
                   {
                	   eduVal = "D";
                   }
                   if(data.eduQualification == "Others")
                   {
                	   eduVal = "O";
                   }
                   
                   $('#eduQualification').val(eduVal).attr("selected", "selected");
                   
                  // $("#eduQualification").val(data.eduQualification);
                   $("#monthlyIncome").val(data.monthlyIncome);
                   $("#pancard").val(data.pancard);
                   $("#employmentType").val(data.employmentType);
                   $("#ITRStaus").val(data.ITRstatus);
                   $("#iciciBankRelationship").val(data.iciciBankRelationship);
                   $("#salaryAccountOpened").val(data.salAccountopend);
                   $("#companyName").val(data.companyName);
                   $("#totalExperience").val(data.totalExp);
                   $("#OAddress").val(data.oaddress);
                   $("#OAddress2").val(data.oaddress2);
                   $("#OAddress3").val(data.oaddress3);
                   $("#OCity").val(data.ocity);
                   $("#OState").val(data.ostate); 
                   $("#OPincodeICICI").val(data.opincode);
                   $("#phone").val(data.phone);
                   $("#std").val(data.std); 
                   $("#OStd").val(data.ostd);
                   $("#OPhone").val(data.ophone);
                   
                   
                   $("#permaddress").val(data.permaddress);
                   $("#pcity").val(data.pcity);
                   $("#peraddresssameascurr").val(data.peraddresssameascurr);
                   $("#permaddress2").val(data.permaddress2);
                   $("#permaddress3").val(data.permaddress3);
                   $("#ppinCode").val(data.ppinCode);
                   
                   var totExp = data.totalExp;
                   var year = totExp.split(".")[0];
                   var month = totExp.split(".")[1];
                   $("#year").val(year);
                   $("#month").val(month);
                   $("#salaryBankWithOtherBank").val(data.salAccwithotherbank);                    
               },
               error: function (e) {
                   console.log("Error " + e);
               } 
           });
 		
 		////////////// Fetch data END ///////////////////////
   
   } // end
         
</script>

