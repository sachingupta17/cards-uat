<%-- 
    Document   : approval
    Created on : 4 Sep, 2017, 11:31:18 AM
    Author     : Aravind E
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<section class="container clearfix bottomPadding">
               <section class="main-section">
                  <div class="msgWrapper text-center">
<!--                  <div class="message_header"><h1>Congratulations!</h1></div>-->
<div class="msgtrigger_div margT5" >
                        <h1>Provisionally Approved*</h1>
                        <p class="toppad2"> Congratulations! Your application for ${cardName} has been provisionally approved. An ICICI Bank representative will get in touch with you within 2 working days.</p>
                        
<!--                        <p>To quickly complete your documentation process, email your ID, address proof, income proof (self-attested)  & photograph  documents to American Express at
      <a href="edocument@aexp.com" target="_blank">edocument@aexp.com</a> (please provide your application reference number)</p><br>-->
<p>For assistance, please call ICICI Bank on 18002671400 from 09:30 AM to 7:00 PM (working days only) and quote the Reference Number ${ICICIID}.</p><br>
   <p>*T&C of ICICI Bank and third parties apply. ICICI Bank shall have the sole discretion to issue the credit card. The decision of ICICI Bank shall be final and binding in all respects.</p>
                     </div>
<!--                     <div class="margT5 otherCards">-->
                       <div>
                      <!--<a href="${applicationURL}home"><button class="buttonBlue_rec margT5" id="CrdApp_btn_view-other-cards">View Other Cards</button></a>-->
                     </div>
<!--                     </div>-->
                  </div>
               </section>
            </section>
<script type="text/javascript">
$(document).ready(function($) {

    if (window.history && window.history.pushState) {

      $(window).on('popstate', function() {
        var hashLocation = location.hash;
        var hashSplit = hashLocation.split("#!/");
        var hashName = hashSplit[1];

        if (hashName !== '') {
          var hash = window.location.hash;
          if (hash === '') {
           
              window.location='home';
          
              return false;
          }
        }
      });

      window.history.pushState('forward', null, './icici-request-approved');
    }

  });
  
            //Adobe code starts here
            function pageInfo(){
             //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----approval--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----approval--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----approval--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----approval--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----approval--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----approval--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----approval--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----approval-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----approval--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End      
                 
             digitalData.pageInfo={"pageName":"ICICI-Approved","category":"Cards","subCategory1":"App Status - Req Approved","subCategory2":'${cardName}',"partner":"JP","currency":"INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
             var formNumber = ${ICICIID};
             $.fn.LoadOfThankUPage(formNumber,'Icici');
    
             }                             
             //Adobe code ends here    
</script>