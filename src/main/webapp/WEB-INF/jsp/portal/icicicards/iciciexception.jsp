<%-- 
    Document   : decline
    Created on : 4 Sep, 2017, 11:32:30 AM
    Author     : Aravind E
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<head>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
    	<link rel = "stylesheet" type = "text/css" href = "style.css" />
    	
     <style>
		.divpara .credittext{
			margin-top:36px;
        font-size: 19px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.26;
  letter-spacing: 0.3px;
  color: #435b73;
		}
		.divpara .creditpara{
			margin-top:20px;
			font-size: 19px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: 0.3px;
  color:#4d4d4f;
		}
		
p.credittext {
    text-align: left;
    font-size: 19px;
}

.divpara p {
    margin-top: 36px;
    font-size: 19px !important;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.26;
    letter-spacing: 0.3px;
    color: #435b73;
    text-align: left;
}
		
        </style>
</head>

<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<!--          <section class="container clearfix bottomPadding"> -->
<!--                <section class="main-section"> -->
<!--                   <div class="msgWrapper text-center"> -->
<!-- <!--                  <div class="message_header"><h1>Exception !</h1></div>--> 
<!--                      <div class="msgtrigger_div margT5"> -->
<%--                         <h1>${Statustitle}</h1> --%>
<%--                         <p class="toppad">${messagecontent}</p> --%>
<!-- <!--                        <span><a href="javascript:void(0);">Terms & conditions apply</a></span>--> 
<!--                      </div> -->
<!-- <!--                     <div class="margT5 otherCards"> -->
<!--                         <p>However, you can check out other cards you are eligible for.</p> -->
<!--                      </div>--> 
<!--                      <div> -->
<%--                          <a href="${applicationURL}${sessionScope.cuurenturl}"><button class="buttonBlue_rec margT5" id="CrdApp_btn_view-other-cards" style="width: 200px;">Try Again</button></a> --%>
<!-- <!--                      <a href="${applicationURL}cardswidget?recommendPage=1"><button class="buttonBlue_rec margT5" id="CrdApp_btn_view-other-cards" style="margin-left: 20px; width: 200px;">Help Me Choose a Card</button></a>-->
<%--                           <a href="${sessionScope.appurl}"><button class="buttonBlue_rec margT5" id="CrdApp_btn_view-other-cards">View Other Cards</button></a> --%>
                     
<!--                      </div> -->
<!--                   </div> -->
<!--                </section> -->
<!--             </section> -->
            
            
            
            <body>
	<div class="desktop">

		<div class="desktop-container">
			<div class="main-content">

				<div class="concent-contain">

					<div class="form-heading">
						<h3>Exception !</h3>
						<h3>${Statustitle}</h3>
						<div class="divpara">
							<p class="credittext">
							${messagecontent}
							</p>
						</div>
					</div>

					<div class="eligibility-check-error-btn concent-btn-box-1">
						<a href="<spring:message code="application.cbc.application_URL"/>" class="othercard-btn">Return Home</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>
            
            
       
       <!--Adobe code starts here-->
        <script type="text/javascript"> 
        window.onload = function () {
            $.fn.failedCT("${messagecontent}".replaceAll('<p>','').replace('</p>',''),"${cardName}".split("ICICI Bank")[1],"ICICI Bank")
        }
        
        function pageInfo(){
        //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----iciciexception--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----iciciexception--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----iciciexception--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----iciciexception--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----iciciexception--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----iciciexception--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----iciciexception--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----iciciexception-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----iciciexception--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End    
        
        
        var url='${sessionScope.appurl}';
        digitalData.pageInfo={"pageName":"ICICI-Exception","category":"Cards","subCategory1":"App Status - Exception","subCategory2":'${cardName}',"partner":"JP","currency":"INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
        digitalData.formInfo={"formName":"ICICI-Exception Form","formNumber":""}
        }
        
        </script> 
        <!--Adobe code ends here-->