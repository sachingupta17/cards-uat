<%-- 
    Document   : iciciFail
    Created on : 11 Jul, 2018, 6:45:19 PM
    Author     : Parvti G
--%>

<%-- 
    Document   : approval
    Created on : 4 Sep, 2017, 11:31:18 AM
    Author     : Aravind E
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<head>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
    
    	<link rel = "stylesheet" type = "text/css" href = "style.css" />
        <style>
		.icicitext p{
			margin-top:30px;
        font-size: 19px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.26;
  letter-spacing: 0.3px;
  color: #435b73;
   text-align: left;
		}
        </style>
</head>


<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<!-- <section class="container clearfix bottomPadding"> -->
<!--                <section class="main-section"> -->
<!--                   <div class="msgWrapper text-center"> -->
<!-- <!--                  <div class="message_header"><h1>Congratulations!</h1></div>--> 
<!-- <div class="msgtrigger_div margT5" > -->
<%--                         <h1>${Statustitle}</h1> --%>
<%--                         <p class="toppad2"> ${messagecontent}</p> --%>
<!--                      </div> -->
<!-- <!--                     <div class="margT5 otherCards">--> 
<!--                        <div> -->
<!--                       <a href="${applicationURL}home"><button class="buttonBlue_rec margT5" id="CrdApp_btn_view-other-cards">View Other Cards</button></a> -->
<!--                      </div> -->
<!-- <!--                     </div>--> 
<!--                   </div> -->
<!--                </section> -->
<!--             </section> -->


<body>
	<div class="desktop">

		<div class="desktop-container">
			<div class="main-content">

				<div class="concent-contain">
					<div class="form-heading">

						<h3>
							${Statustitle}
						</h3>
					</div>


					<img src="${applicationURL}static/images/img/card-crash.png" id="failed">

					<div class="icicitext">
						<p>${messagecontent}</p>
					</div>

					<div class="eligibility-check-error-btn concent-btn-box-1">
						<a href="<spring:message code="application.cbc.application_URL"/>" class="othercard-btn">Return Home</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>




<script type="text/javascript">
window.onload = function () {
    $.fn.failedCT('${messagecontent}'.replaceAll('<p>','').replaceAll('</p>',''),'${cardName}'.split('ICICI Bank')[1],'ICICI Bank');
}

$(document).ready(function($) {

    if (window.history && window.history.pushState) {

      $(window).on('popstate', function() {
        var hashLocation = location.hash;
        var hashSplit = hashLocation.split("#!/");
        var hashName = hashSplit[1];

        if (hashName !== '') {
          var hash = window.location.hash;
          if (hash === '') {
           
              window.location='home';
          
              return false;
          }
        }
      });

      window.history.pushState('forward', null, './icici-request-approved');
    }

  });
  
</script>
