<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<%-- <div id="social-profiles" data-toggle="modal" class="hidden" onclick="assistMeeClick()">  
    <img src="${applicationURL}static/images/footer/phone-call.png" width="30" height="30">
    <h5>Assist Me</h5>
</div> --%> 
<!--<div id="social-profiles" data-toggle="modal" onclick="assistMeeClick()">  
    <img src="${applicationURL}static/images/footer/phone-call.png" width="30" height="30">
    <h5>Assist Me</h5>
</div>-->

<style>


#content-desktop {display: block;}
#content-mobile {display: none;}

@media screen and (max-width: 768px) {

#content-desktop {display: none;}
#content-mobile {display: block;}


  

}

/* for indusind */

 .induspop12{
margin: 0px 0px 10px 0px !important;
}

.indusbut{
	color: #ffffff !important;
    background-color: #b3dbdc;
    margin: 0px 0px 0px 0px;
       width: 374px !important;
       border-color: white !important;
}

 .indus_but{ 
 	color: #ffffff !important; 
     background-color: #03868b; 
     margin: 0px 0px 0px 0px; 
         width: 374px !important;
         border-color: white !important;
 } 

.application-opt-popup .otp-form .error{
	color: #000000; 
}
 

@media only screen and (min-width: 768px){
.application-opt-popup {
    padding: 20px 30px 0 0px;
}}

  @media only screen and (min-width: 768px){ 
 .popup-container { 
     width: 838px; 
     height: 515px; 
     border: none; 
     margin: 0px auto; 
     text-align: center; 
 } 
 }
 
 @media only screen and (min-width: 768px) {
	.application-opt-popup .otp-form span a {
		font-weight: bold;
	}
}


.otp-form span {
	display: block;
	position: relative;
	color: #000000 !important;
	font-family: Montserrat !important;
	font-size: 14px !important;
	/*     left: 6px; */
	/*     left: 9px; */
}
.login-line span {
    width: 371px;
    display: flex !important;
    text-align: left;
    font-family: Montserrat;
    margin: 0 auto;
    padding-top: 6px;
    color: #000000 !important;
    margin-bottom: 10px;
}

.login-line span a {
    margin: -1px 0px 0px 3px !important;
    color: #03868b !important;
    font-size: 14px !important;
    font-weight: bold;
}

</style>

<form:form autocomplete="off" id="callMe" commandName="callMeBean">
    <div class="modal fade" id="callMe_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div id="callmeclose" class="proceedPopup_close pull-right">
                        <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="formAbandonOfAssistMe()">
                    </div>
                    <div class="callme_popup">  
                        <div class="text-center">
                            Fill in your details for us to call you & help you complete the credit card application                        </div>
                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt" id="Tellus">Tell us about yourself</h2>
                        </div>
                        <div class="error_box1 hidden">
                            <ul class="error_list1"></ul>
                        </div>
                        <div class="row">
                            <div class="enroll_div margT2">
                                <div class="middleName_input user_details">  
                                    <form:input id="socialName" path="socialName"  placeholder="Full Name" type="text" value="" />
                                </div>
                                <div class="phone_input user_details">
                                    <form:input id="socialPhone" path="socialPhone"  maxlength="10" placeholder="Phone" type="number"  onKeyPress="if(this.value.length==10) return false;" value="" />
                                </div>
                                <div class="email_input user_details">
                                    <form:input id="socialEmail" path="socialEmail"  placeholder="Email" type="text" value="" />
                                </div>

                                <!--                                <div class="jpNumber_input user_details">
                                <%--<form:input id="socialjpNumber" path="socialjpNumber"  placeholder="JetPrivilege Number" type="number" value="" maxlength="9" onKeyPress="if(this.value.length==9) return false;"/>--%>
                            </div>-->
                            </div>
                            <form:hidden path="formNumber" id="formNumber"/>
                            <form:hidden path="pageName" id="pageName"/>
                            <form:hidden path="cardName" id="cardName"/>
                        </div>
                        <div id="loader" class="text_center hidden margT5">
                            <img src="${applicationURL}static/images/header/ajax-loader.gif" alt="Page is loading, please wait." />
                        </div>
                        <div class="text-center margT10">
                            <input class="button_thickBlue" type="button" id="callMeContine" value="Submit">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form:form>
<div class="modal fade" id="successCallmeModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="sizer modal-content">
            <div class="modal-header">
                <!--              <div class="proceedPopup_close pull-right">
                                        <img src="$ {applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.CloseOnPopUpClick('Enroll Now')">
                                    </div>-->

            </div>
            <div class="modal-body">
                <p class="modaltext" style="color:#01a200; text-align:center; font-size: 18px; ">Thank you for sharing your details! Our representative will reach out to you for further assistance.</p>


            </div>
            <div class="modal-footer">
                <button type="button" class="successcallbut" data-dismiss="modal">OK</button>
            </div>

        </div>

    </div>
</div>


<div class="modal fade" id="filterPopup" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="filterPopup">
                    <div class="filter_title">
                        <ul class="list-inline">
                            <li>FILTERS</li>
                            <li class="close_filters"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></li>
                        </ul>
                    </div>
                    <div>
                        <ul class="filter_tab list-inline">
                            <li class="filter">
                                <span>FILTERS</span>
                            </li>
                            <li class="sort">
                                <span>SORT</span>
                            </li>
                        </ul>
                    </div>
                    <div class="filter_area">
                        <div class="banks filter_wrap margT10">
                            <span class="filter_names">Bank</span>
                            <span class="dropdown">
                                <img src="${applicationURL}static/images/co-brand/filterdrop.png"  alt="">
                            </span>
                        </div>
                        <div class="banks_listing parent_div hidden">
                            <div class="arrow-up"></div>
                            <ul class="banks_list">
                                <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                                    <c:forEach items="${bankList}" var="list">
                                    <li>
                                        <input type="checkbox" name ="bankName" id="mob_bankName${list.bpNo}" class="mob_bankNum" value="${list.bpNo}">
                                        <label for="mob_bankName${list.bpNo}">

                                            <img src="${applicationURL}/banks/${list.bankImagePath}" class="bank_logos">
                                            <span>${list.bankName}</span>
                                        </label>
                                    </li>

                                </c:forEach>
                            </ul>
                        </div>
                        <div class="joiningFees filter_wrap margT10">
                            <span class="filter_names">Joining Fees</span>
                            <span class="dropdown">
                                <img src="${applicationURL}static/images/co-brand/filterdrop.png"  alt="">
                            </span>
                        </div>
                        <div class="slider_div parent_div filter_content hidden">
                            <div class="arrow-up"></div>
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                            <div class="slider_valu  margB5 margT5">
                                <div style="display:inline">&#8377; 500 </div>
                                <div style="display:inline; float:right;"> &#8377; 10,000</div>
                            </div>
                            <input min="500" max="10000" step="500" value="700" data-orientation="horizontal" type="range" id="fee_range">
                            <div class="text-center margT10">
                                <input class="filter_button button_red" type="button" value="Done">
                            </div>
                        </div>
                        <div class="life_benefits filter_wrap margT10">
                            <span class="filter_names">Lifestyle Benefits</span>
                            <span class="dropdown">
                                <img src="${applicationURL}static/images/co-brand/filterdrop.png"  alt="">
                            </span>
                        </div>
                        <div class="life_benefits_list parent_div filter_content hidden">
                            <div class="arrow-up"></div>
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                            <div class="row">
                                <div class="life_benefits_holder" >
                                    <ul>
                                        <c:forEach items="${prefList}" var="list">
                                            <li>
                                                <input class="mob_lsbpNum" name ="lsbpName" id="mob_lsbpName${list.lsbpNo}" type="checkbox" value="${list.lsbpNo}" onclick="selectBenefits(this);">
                                                <label for="mob_lsbpName${list.lsbpNo}" >
                                                    <img src="${applicationURL}lifestyles/${list.lsbpImageIconPath}"  alt=""  >
                                                    <span>${list.imageAsIconText}</span>
                                                    <img src="${applicationURL}lifestyles/${list.lsbpImageIconBluePath}"  alt="" style="display:none">
                                                    <span style="display:none">${list.imageAsIconText}</span></label>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="filter-btn-holder filter_holder margB5">
                            <ul>
                                <li>
                                    <div class="clearFilters" id="mob_clearFilters">Clear</div>
                                </li>
                                <li>
                                    <div class="apply-btn" id="mob_apply-btn">Apply</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="sort_tab margT10 hidden">
                        <div class="sort_div">
                            <span>Sort By</span>
                        </div>
                    </div>
                    <div class="sort_listing parent_div hidden">
                        <div class="arrow-up"></div>

                        <ul class="sort_list">                                 
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                            <li class="mob_sorting_ascending">
                                Bank (A-Z)
                            </li>
                            <li class="mob_sorting_descending">
                                Bank (Z-A)
                            </li>
                            <li class="mob_sort_by_fee_asc">
                                Fees (Low to High)
                            </li>
                            <li class="mob_sort_by_fee_dsc">
                                Fees (High to Low)
                            </li>
                            <li class="mob_sort_by_lyf_asc">
                                Lifestyle Benefit (Min to Max)
                            </li>
                            <li class="mob_sort_by_lyf_dsc">
                                Lifestyle Benefit (Max to Min)
                            </li>
                            <li class="air_ticket_asc">
                                Air Tickets (Min to Max)
                            </li>
                            <li class="air_ticket_dsc">
                                Air Tickets (Max to Min)
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="spends_slider" tabindex="-1" role="dialog">

    <div class="modal-dialog vertical-align-center">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body calculatemiles">   
                <div class="proceedPopup_close pull-right">
                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="">
                </div>             
                <div class="spendsSlider_popup margT5">

                    <div class="margT2 colorDarkBlue text-center">
                        <h2 class="heading_txt">Calculate your Free Flights based on your monthly spends</h2>
                    </div>
                    <div style="padding-top: 10px; text-align: center;" class="spendsVal_error">
                        <span id="error_msg" style="font-size: 14px; color: red; display:none;"></span>
                    </div>
                    <div class="extest">
                        <div class="col-md-12">
                            <p class="amtvalue1">Annual Spends : INR <span class="slider-output" id="yearly_bill1">0</span></p>

                        </div>
                        <div class="col-md-12">
                            <div class="range-example-modal"></div>
                            <div class="imagebar1"><img src="${applicationURL}static/images/co-brand/numbar.jpg"/></div>

                        </div>

                        <div class="col-md-12">
                            <div class="infodown">
                                <div class="aheadiv">INR</div>
                                <input id="unranged-value2" type="text" inputmode="numeric" name="amountInput2"  value="0" maxlength="9"  />
                                <div class="donebut">Done</div>
                            </div>  
                            <div class="slider_modal_div_error"></div>
                        </div>

                    </div>

                    <!--                    <div class="slider_valu margB5">
                                            <div style="display:inline"><span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5,000</span></div>
                                            <div style="display:inline; float:right;"><span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span></div>
                                        </div>-->
                    <!--                    <div class="row padLR15">
                    
                                            <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal">										   									   
                                        </div>-->
                </div>
                <!--                <div class="annualSpends margT5 text-center">
                                    <span>Annual Spends: </span><span class="annualSpends_value">0</span>
                                </div>	-->
                <!--                <div class="text-center margT2">
                                    <input class="button_red annual_spends" type="button" value="Done">
                                </div>-->
            </div>
        </div>
    </div>
</div>

<!--new pop here -->

<div class="modal fade " id="abtYourself" tabindex=-1 role="dialog">
    <div class="modal-dialog mob-modal">
        <!-- Modal content-->
        <div class="modal-content fleft">
            <div class="modal-body">
               <%--  <div class="proceedPopup_close pull-right">
                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="formAbandonmentOnPopUp('Pop-up close')"/>
                </div>--%>
                <form:form autocomplete="off" id="tellMeAbout" action="applyOther" commandName="cardDetailsBean" method="post">     
                    <input type="hidden" id ="CardNo" />
                    <input type="hidden" id ="cardName" />
                    <input type="hidden" id ="cardImage" />
                    <input type="hidden" id ="cardfee" />
                    <input type="hidden" id ="bankname" />
                    <input type="hidden"   id="test"/> 
<!-- here -->
              <%--         <div class="jpNum_popup">
                        <div class="text-center jpPopup_header">
                            Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-brand Card. Apply here
                        </div>

                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Provide InterMiles Number</h2>
                        </div>

                        <div class="row">
                            <div class="jpNumber_div margT2">
                                <div class="jpNumber_input">
                                    <form:input path="jpNumber" id="jpNumber" placeholder="JetPrivilege Number" maxlength="9" pattern="[0-9]*" />
                                    <c:choose>
                                        <c:when test="${empty sessionScope.loggedInUser}">
                                            <form:input path="jpNumber" id="jpNumber" placeholder="JetPrivilege Number" maxlength="9" pattern="[0-9]*" />
                                        </c:when>
                                        <c:otherwise>

                                            <form:input path="jpNumber" id="jpNumber" placeholder="JetPrivilege Number"  maxlength="9" pattern="[0-9]*" />
                                             <form:hidden path="jpNumber" id="hjpNumber"/>
                                        </c:otherwise>
                                    </c:choose>
                                    <form:input path="jpNumber" id="jpNumber" placeholder="Intermiles Number" type="number" maxlength="9" onkeypress="if(this.value.length==9) return false;" pattern="[0-9]*"  /> 
                                    <form:hidden path="jpNumber" id="hjpNumber"/>

                                </div>
                                <div class="jpNumber_input margT2 hidden">
                                    <form:hidden path="jpTier" id="jpTier" placeholder="Intermiles Membership Tier" disabled="true" pattern="[0-9]*" />
                                    <form:hidden path="cpNo" id="otcpNo"/>
                                    <form:hidden path="bpNo" id="otbpNo"/>
                                    <!--<input type="hidden" id="otbpNo"/>-->
                                </div>
                                <c:if test="${empty sessionScope.loggedInUser}">
<!--                                <div class="text-center margT2">
                                    If you are not a Intermiles member, <a href="javascript:void(0)" class="enroll_here"  id="enroll_here" style="text-decoration: underline;" onclick="afterEnrollHereClick('Enroll here', 'co-brand quick enrollment')">please enroll here</a>
                                </div>-->
                                </c:if>
                            </div>
                        </div>
                        <div class="text-center margT10">
                            <input class="button_thickBlue" type="button" id="tellme" onClick="$(this).closest('form').submit();" value="Submit">
                        </div>
                    </div>   --%>
                    
                    <!-- new popup -->
                    
           <div class="popup-container icici-popup-cont">
			<div class="application-opt-popup logospacer">
				<div class="logo induspop12">
					<img src="${applicationURL}static/images/img/intermiles.png"/>
					<span class="proceedPopup_close"><img src="${applicationURL}static/images/img/cross.png" onclick="formAbandonmentOnPopUp('Pop-up close')"></span>
				</div>
				<div class="upperline">
							<div class="icici-line"></div>
						</div> 
						<div class="mob-cross">
							<span class="mob-popupClose"><img
								src="${applicationURL}static/images/img/cross.png"></span>
						</div>
				<h4>Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-brand Card. Apply here</h4>
				
				<p><strong><h3>Provide InterMiles Number</h3></strong></p>
				<div class="num-form">
<!-- 					<input type="number" id="otpValue" name="" placeholder="Enter OTP" maxlength="6" type="number" onKeyPress="if (this.value.length == 6) return false;"> -->
					
					<%-- <form:input path="jpNumber" id="jpNumber" placeholder="JetPrivilege Number" maxlength="9" pattern="[0-9]*" /> --%>
                                    <c:choose>
										<c:when test="${empty sessionScope.loggedInUser}">
                                            <form:input path="jpNumber" id="jpNumber" placeholder="Enter InterMiles Number" maxlength="9" type="number" pattern="[0-9]*" onkeypress="if(this.value.length==9) return false;" />
                                        </c:when>
                                        <c:otherwise>

                                        <form:input path="jpNumber" id="jpNumber" placeholder="Enter InterMiles Number" type="number" maxlength="9" onkeypress="if(this.value.length==9) return false;" pattern="[0-9]*"  /> 
                    					 <form:hidden path="jpNumber" id="hjpNumber"/>	
                                        </c:otherwise>
                                    </c:choose>
					
					
					<%-- <form:input path="jpNumber" id="jpNumber" placeholder="Intermiles Number" type="number" maxlength="9" onkeypress="if(this.value.length==9) return false;" pattern="[0-9]*"  /> 
                     <form:hidden path="jpNumber" id="hjpNumber"/>	 --%>
					
					<div class="otpError" id="otpError"></div> 
							
<!-- 					<span><a href="#" id="resend30Sec">Resend in 30 seconds</a></span> -->
								<div class="jpNumber_input margT2 hidden">
                                    <form:hidden path="jpTier" id="jpTier" placeholder="Intermiles Membership Tier" disabled="disabled" pattern="[0-9]*" />
                                    <form:hidden path="cpNo" id="otcpNo"/>
                                    <form:hidden path="bpNo" id="otbpNo"/>
                                    <!--<input type="hidden" id="otbpNo"/>-->
                                </div>
<!-- 					<input type="button" class="genebutton_HDFC indusbut" disabled="disabled" onClick="$(this).closest('form').submit();" id="tellme" value="Submit">  -->
					
<!-- 					<a href="#" target="_blank"><input type="submit" class="genebutton_HDFC indusbut" id="proceedTo" onclick="window.location.href='' value="Submit"></a> -->
				
							<!-- <div class="genebutton_HDFC indusbut disabled="true">	
                                <a href="#" class="proceedToBank" id="proceedTo" target="_blank">
                                    <span id="proceedBank" onclick="proceedClick()"></span>
                                </a>
                            </div> -->
               

				</div>
				 <h4>
								<p class="login-line">
									<span>If you are not an Intermiles member, <a
										class="enroll_here1" id="enroll_here1"
										href="https://uat.intermiles.com/rewards-program/enrol"
										onclick="getClickVal()"> Sign up Now!</a>
									</span>
								</p>
							</h4>
				<div class="margT5 proceed">	
                                <a href="#" class="margT5 indusbut" id="proceedTo" target="_blank"><input  type="submit" id="indus_pop_but" class="indusbut" value="Submit">
<!--                                     <span id="proceedBank" onclick="proceedClick()"></span> -->
                                </a>
                            </div>
			</div>
	  
		</div> 

                </form:form>

                <form:form autocomplete="off" id="quickEnrollUser" action="enrollme" commandName="enrollBean">
                    <form:hidden path="enrollbpNo" />
                    <div class="enroll_popup hidden">
                        <div class="text-center">
                            Enroll into the Intermiles Programme now to start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-Brand Card.
                        </div>
                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Enroll Now</h2>
                        </div>
                        <div class="error_box2 hidden">
                            <ul class="error_list2">
                            </ul>
                        </div>
                        <div class="row">
                            <div class="pull-left margT2 nav_web">
                                <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png"   class="left_slide goTo_abtYourself_div" onclick="$.fn.formAbandonmentOnSubmitJPnumberPopup('pop-up back', 'co-brand quick enrollment')">
                            </div>

                            <div class="enroll_div margT2">
                                <div class="title user_details">
                                    <form:select path="enrollTitle" id="enrollTitle" placeholder="Title" class="title_select">
                                        <form:option value="">Select Title</form:option>
                                        <form:option value="Mr">Mr</form:option>
                                        <form:option value="Ms">Ms</form:option>
                                        <form:option value="Mrs">Mrs</form:option>
                                        <form:option value="Dr">Doctor</form:option>
                                        <form:option value="Prof">Professor</form:option>
                                        <form:option value="Captain">Captain</form:option>
                                    </form:select>
                                </div>
                                <div class="gender hidden">
                                    <c:forEach items="${genderStatus}" var="status">
                                        <div style="display:inline-block">
                                            <label class="radio-inline">
                                                <form:radiobutton
                                                    path="enrollGender" name="enrollgender"
                                                    id="enrollGender"
                                                    value="${status.key}" /> </label>
                                            <label class="gender_desc" for="${status.key}" >${status.value}</label>
                                        </div>
                                    </c:forEach>
                                </div>
                                <div class="firstName_input user_details">
                                    <form:input type="text" path="enrollFname" id="enrollFname" placeholder="First Name"/>
                                </div>
                                <div class="middleName_input user_details">
                                    <form:input type="text" path="enrollMname" id="enrollMname" placeholder="Middle Name"/>
                                </div>
                                <div class="lastName_input user_details">
                                    <form:input type="text" path="enrollLname" id="enrollLname" placeholder="Last Name"/>
                                </div>
                                <div class="city_input user_details">
                                    <form:input type="text" path="enrollCity" id="enrollCity" placeholder="City"/>
                                </div>
                                <div class="phone_input user_details">
                                    <form:input path="enrollPhone" id="enrollPhone" maxlength="10" placeholder="Phone" type="number" onkeypress="if(this.value.length==10) return false;"/>
                                </div>
                                <div class="email_input user_details">
                                    <form:input path="enrollemail" id="enrollemail" placeholder="Email"/>
                                </div>
                                <!--                                <div class="password_input user_details">
                                                                    < form:password path="enrollPassword" id="enrollPassword" placeholder="Password"/>
                                                                    < form:hidden path="hashPassword" id="hashPassword"/>
                                                                </div>
                                                                <div class="password_input user_details">
                                                                    < form:password path="enrollRenterPassword" id="enrollRenterPassword" placeholder="Re Enter Password"/>
                                                                </div>-->
                                <div class="dob_input user_details">
                                    <form:input path="enrollDob" id="date_of_birth2" class="datepicker" readonly="true" placeholder="Date Of Birth"/>
                                </div>
                                <div class="g-recaptcha recaptchaarea" id="captch" 
                                     data-sitekey=<spring:message code="google.recaptcha.site"/>    ></div>
                                <div> 
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="termsAndConditionsEnroll" class="margT1 statement" path="termsAndConditionsEnroll" /> 
                                        <span>I agree to the Intermile membership 
                                            <a href="https://www.jetprivilege.com/terms-and-conditions/jetprivilege"  target="_blank">
                                                Terms and Conditions</a><span style="margin: 0px 4px;display: inline;">and</span> <a href="https://www.jetprivilege.com/disclaimer-policy"  target="_blank">
                                                Privacy Policy</a></span>
                                    </div>
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="recieveMarketingTeam" class="margT1 statement" path="recieveMarketingTeam" />  
                                        <span> Yes, I would like to receive Marketing Communication


                                        </span>
                                    </div>
                                </div>
                                <div class="error_box_enrolStatus hidden" id="enroll_error">
                                </div>
                            </div>
                        </div>
                        <div id="loader" class="text_center hidden margT5">
                            <img src="${applicationURL}static/images/header/ajax-loader.gif" alt="Page is loading, please wait." />
                        </div>
                        <div class="text-center margT10 nav_web">
                            <input class="button_thickBlue" type="button"  id="enrollForm" value="Continue" data-bpNo="${cdList.bpNo}">
                        </div>
                        <div class="nav_arrow_mobile margT10">
                            <div class="left_nav">
                                <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_arrow goTo_abtYourself_div" onclick="enrollHereBackClick('Back to enroll here', 'Submit JPNumber')">
                            </div>
                            <div class="continue_button">
                                <input class="button_thickBlue" type="button" id="enrollForm_mob_view" value="Continue" >
                            </div>
                        </div>
                    </div>  
                </form:form>
            </div>
        </div>
    </div>
</div>

<!-- end changes 1st part -->

<!-- old procced -->
<%-- <div class="modal fade" id="proceed_modal" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">                
                <div class="proceed_popup">
                    <div class="proceed_popup_title">
                        <h2>
                            <span>Earn with Partners: Swipe any of our Co-brand Cards</span>
                        </h2>
                        <div class="proceedPopup_close pull-right">
                            <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.formAbandonmentOnSubmitJPnumberPopup('Pop-up close', 'Proceed to Bank')">
                        </div>
                    </div>
                    <div class="row card_toProceed margT2">
                        <div class="card_display text-center pad5">
                            <div class="">
                                <h4 id="popCardName"></h4>
                            </div>
                            <div class="card_img_holder card_img">
                                <img  id="replaceImg">
                            </div>
                            <div class="fee_value">
                                <h4>Fees: <span id="popFees"></span></h4>
                            </div>
                        </div>
                        <div class="card_details pad5 text-center margT5">
                            <div class="membershipNo_div padB10">
                                <p>Your InterMiles Membership Number is</p>
                                <h4><span class="membershipNo" id="jpNo"></span></h4>
                            </div>
                            <div class="margT5">
                                <p>Please ensure to enter your same Intermiles Membership Number in your application form</p>
                            </div>
                            <div class="margT5 proceed">	
                                <a href="#" class="button_blue margT5 proceedToBank" id="proceedTo" target="_blank">
                                    <span id="proceedBank" onclick="proceedClick()"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --%>


<!--pending notification pop up-->
<!--<div class="modal fade modal_overflow" id="notification" tabindex=-1 role="dialog">
    <div class="modal-dialog">
         Modal content
        <div class="modal-content">
            <div class="modal-body">

                <div class="notificationpopup">
                    <div class="margT2 colorDarkBlue text-center">
                        <h2 class="heading_txt">Proceed to Application Form</h2>
                    </div>
                    <div class="notifytext">
                        You are one step away from <br/>completing your AMEX Application   
                    </div>

                    <div class="text-center appbutton">
                        <c:forEach items="${cardList}" var="cdList" varStatus="i">
                            <c:choose>
                                <c:when test="${cdList.bpNo eq 1}">
                                    <button class="completenow" data-name="${cdList.imageText}" data-spMailingID="${spMailingID}" data-spUserID="${spUserID}"
                                            data-spJobID="${spJobID}" data-spReportId="${spReportId}" data-utm_source="${utm_source}"
                                            data-utm_medium="${utm_medium}" data-utm_campaign="${utm_campaign}" random="${randomNumber}">
                                        <h1>Complete Now</h1>
                                    </button>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                        <button class="later" data-toggle="modal">
                            <h1>Later</h1>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>-->
<!--pending notification pop up end-->

<div class="modal fade" id="benefits" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="pull-right close_div"><img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg"  alt=""></div>
                <h5 class="margT2 colorDarkBlue pad2">Benefits</h5>
                <span class="list_cont" id="benefitContent"></span>
            </div>
        </div>
    </div>
</div>
<!--cr2-milestone-2-->
<div class="modal fade" id="joiningbenefits" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="pull-right close_div"><img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg"  alt=""></div>
                <h5 class="margT2 colorDarkBlue pad2">Joining Benefit</h5>
                <span class="list_cont" id="joiningbenefitContent"></span>
            </div>
        </div>
    </div>
</div>
<!--end cr2-milestone2-->
<form:form autocomplete="off" commandName="cardDetailsBean" id="cardDetailsBean" enctype="" action="best-co-branded-cards" method="post">
    <form:hidden path="utmSource" value="${utm_source}"/>
    <form:hidden path="utmMedium" value="${utm_medium}"/>
    <form:hidden path="utmCampaign" value="${utm_campaign}"/>
    <input type="hidden"  id="incomevlaue"/>


    <div class="modal fade" id="userDetails" tabindex=-1 role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="proceedPopup_close pull-right" id="helpMeChooseClose">
                        <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="">
                    </div>
                    <div class="userDetails_popup">
                        <div class="city_content city text-center">
                            <div class="colorDarkBlue">	
                                <h2 class="heading_txt">Please answer a few questions to help us select the best card for you!</h2>
                            </div>
                            <div class="margT2 colorDarkBlue text-center">
                                <h2 class="heading_txt">What is your current city of residence?</h2>
                            </div>
                            <div class="margT1 error_msg_recommend" id="error_msg_city"></div>
                            <div class="city_div margT5">
                                <div class="row">
                                    <ul class="cities">
                                        <li>
                                            <div class="city_checkbox">
                                                <label for="city1">
                                                    <span><img src="${applicationURL}static/images/co-brand/mum.jpg" alt="" class=""></span> 
                                                    <span><input type="checkbox" name="modal_city" id="city1" class="city_uncheck" value="Mumbai"></span>
                                                    Mumbai</label>

                                            </div>
                                        </li>
                                        <li>
                                            <div class="city_checkbox">
                                                <label for="city2"> 
                                                    <span><img src="${applicationURL}static/images/co-brand/chennai.jpg" alt="" class=""></span> 
                                                    <span><input type="checkbox" name="modal_city" id="city2" class="city_uncheck" value="Chennai"></span>
                                                    Chennai</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="city_checkbox">
                                                <label for="city3"> 
                                                    <span><img src="${applicationURL}static/images/co-brand/delhi.jpg" alt="" class=""></span> 
                                                    <span><input type="checkbox" name="modal_city" id="city3" class="city_uncheck" value="Delhi"></span>
                                                    Delhi</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="city_checkbox">
                                                <label for="city4"> 
                                                    <span><img src="${applicationURL}static/images/co-brand/bengaluru.jpg" alt="" class=""></span>
                                                    <span><input type="checkbox" name="modal_city" id="city4" class="city_uncheck" value="Bengaluru"></span>
                                                    Bengaluru</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="city_checkbox">
                                                <label for="city5"> 
                                                    <span><img src="${applicationURL}static/images/co-brand/kolkata.jpg" alt="" class=""></span>
                                                    <span><input type="checkbox" name="modal_city" id="city5" class="city_uncheck" value="Kolkata"></span>
                                                    Kolkata</label>
                                            </div>
                                            <form:hidden id="cityName" path="cityName"/>
                                        </li>
                                        <li>

                                            <div class="city_checkbox">
                                                <div class="city_img">
                                                    <img src="${applicationURL}static/images/co-brand/othercity.jpg" alt="" class="">
                                                </div>
                                                <span>
                                                    <select name="select_city_name" id="select_modal_city" class="select_city_uncheck">
                                                        <option value="">Other City</option>
                                                        <c:forEach items="${cityNamesList}" var="city">
                                                            <option value="${city}">${city}</option>
                                                        </c:forEach> 
                                                    </select>
                                                </span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="margT10">									  
                                <input class="nav_button margL2 goTo_dob" id="CrdHP_currentcity_next" type="button" value="Next">
                            </div>
                        </div>

                        <div class="dob_content body_content">
                            <div class="margT2 colorDarkBlue text-center">
                                <h2 class="heading_txt">What is your date of birth?</h2>
                            </div>
                            <div class="margT1 error_msg_recommend" id="error_msg_dob"></div>
                            <div class="row">
                                <div class="dob_div margT2">
                                    <div class="dob_input">
                                        <form:input placeholder="DOB" path="dob" id="date_of_birth"  class="datepicker" inputmode="numeric"/>
                                        <form:hidden path="dob" id="hdate_of_birth"/>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center margT10">
                                <input class="nav_button goTo_City BackButton" id="CrdHP_dob_back" type="button" value="Back">
                                <input class="nav_button margL2 goTo_flightFreq" id="CrdHP_dob_next" type="button" value="Next">
                            </div>
                        </div>

                        <div class="flightFreq_content body_content">
                            <div class="margT2 colorDarkBlue text-center">
                                <h2 class="heading_txt">How often in a year do you fly?</h2>
                            </div>
                            <div class="row">
                                <div class="flightRange_div margT2">
                                    <div class="text-center">
                                        <h1 class="no_of_flights" id="no_of_flights"></h1>
                                        <h5>One way flights</h5>
                                    </div>
                                    <div class="margT10 slider_wrapper margB5">
                                        <div class="flightRange_slider margT10">
                                            <form:input min="1" max="100" step="1" data-orientation="horizontal" type="range" path="flightRange" id="flightRange"/>																	
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center margT10">
                                <input class="nav_button goTo_dob BackButton" id="CrdHP_ff_back" type="button" value="Back">
                                <input class="nav_button margL2 goTo_lifePrivileges" id="CrdHP_ff_next" type="button" value="Next">
                            </div>
                        </div>
                        <div class="lifeStyle_content body_content">
                            <div class="margT2 colorDarkBlue text-center">
                                <h2 class="heading_txt">What lifestyle privileges should your card offer?</h2>
                                <h5>(Select any 2)</h5>
                            </div>
                            <div class="margT1 error_msg_recommend" id="error_msg_lsbpArray"></div>
                            <div class="row">
                                <div class="lifeStyle_div margT2 text-center">
                                    <div class="lifeStyle_div margT5">
                                        <div class="row">
                                            <ul class="privileges">
                                                <c:forEach items="${prefList}" var="list">
                                                    <li>
                                                        <div class="priv_checkbox">
                                                            <label for="lsbp${list.lsbpNo}"><span><img src="${applicationURL}lifestyles/${list.lsbpImagePath}"/></span>
                                                                <span><input type="checkbox" id="lsbp${list.lsbpNo}" name="lsbp_name" value="${list.lsbpNo}"/></span>
                                                                ${list.lsbpImageText}</label>
                                                            <!--For Adobe start-->
                                                            <input type="hidden" name="lsbp_imageText" id="hdnchk_lsbp${list.lsbpNo}" value="${list.lsbpImageText}"/>
                                                            <!--For Adobe end-->
                                                            <form:hidden path="lsbpArray" id="lsbpArray"/>
                                                        </div>
                                                    </li>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center margT10">
                                <input class="nav_button goTo_flightFreq BackButton" id="CrdHP_lifestyle_back" type="button" value="Back"> 
                                <input class="nav_button margL2 goTo_spendCapability" id="CrdHP_lifestyle_next" type="button" value="Next">
                            </div>
                        </div>
                        <div class="spendCapability_content body_content">
                            <div class="margT2 colorDarkBlue text-center">
                                <h2 class="heading_txt" id="incomecompare">How much will you spend on your card?</h2>
                            </div>
                            <div class="margT1 error_msg_recommend" id="error_msg_expend"></div>
                            <div class="row">
                                <div class="spendCapability_div margT2" >
                                    <div class="row">
                                        <div class="col-sm-6 spendValue">
                                            <div class="rupee">
                                                <span><img src="${applicationURL}static/images/co-brand/rupee_white_icon.png" alt="" class="rupee_white_Icon"></span>
                                            </div>
                                            <div class="spendCapability_monthly">
                                                <input type="text" inputmode="numeric" class="spend_choose" id="anualIncome">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 margT1 spends_forFlights">
                                            <span>Annually :</span>
                                            <span class="annualSpends_forFlights"></span>
                                        </div>
                                    </div>
                                    <div class="slider_valu margT5">
                                        <div style="display:inline"><span><img src="${applicationURL}static/images/co-brand/rupee_white_icon.png" alt="" class="rupee_white_icon"></span><span>5,000</span> </div>
                                        <div style="display:inline; float:right;"> <span>> </span><span><img src="${applicationURL}static/images/co-brand/rupee_white_icon.png" alt="" class="rupee_white_icon"></span><span>15 Lakhs</span></div>
                                    </div>
                                    <div class="margT5 slider_wrapper">										 
                                        <div class="spendCapability_slider" >
                                            <form:input min="5000" max="1500000" step="5000" data-orientation="horizontal" type="range" path="monthlySpend" id="monthlySpend"/> 

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center margT10">
                                <input class="nav_button goTo_lifePrivileges BackButton" id="CrdHP_pm-spend_back" type="" value="Back">
                                <input class="nav_button margL2 goTo_annualIncome" id="CrdHP_pm-spend_next" type="button" value="Next">
                            </div>
                        </div>
                        <div class="annualIncome_content body_content">
                            <div class="margT2 colorDarkBlue text-center">
                                <h2 class="heading_txt">What is your gross annual income?</h2>
                            </div>
                            <div class="margT1 error_msg_recommend" id="error_msg_annualIncome"></div>
                            <div class="row">
                                <div class="annualIncome_div margT2">
                                    <ul class="list-inline">
                                        <li>
                                            <div><input type="radio" name="annual_Income" value="100000-200000"/></div>
                                            <div class="spendsValue">1L-2L</div>
                                        </li>
                                        <li>
                                            <div><input type="radio" name="annual_Income" value="200000-300000"/></div>
                                            <div class="spendsValue">2L-3L</div>
                                        </li>
                                        <li>
                                            <div><input type="radio" name="annual_Income" value="300000-500000"/></div>
                                            <div class="spendsValue">3L-5L</div>
                                        </li>
                                        <li>
                                            <div><input type="radio" name="annual_Income" value="600000-700000"/></div>
                                            <div class="spendsValue">6L-7L</div>
                                        </li>
                                        <li>
                                            <div><input type="radio" name="annual_Income" value="800000-1000000"/></div>
                                            <div class="spendsValue">8L-10L</div>
                                        </li>
                                        <li>
                                            <div><input type="radio" name="annual_Income" value="1100000-2000000"/></div>
                                            <div class="spendsValue">11L-20L</div>
                                        </li>
                                        <li>
                                            <div><input type="radio" name="annual_Income" value=">2000000"/></div>
                                            <div class="spendsValue">>20L</div>
                                        </li>
                                    </ul>
                                    <form:hidden path="annualIncome" id="annualIncome"/>
                                </div>
                            </div>
                            <div class="text-center margT10">
                                <input class="nav_button goTo_spendCapability BackButton" id="CrdHP_grs-anlincme_back" type="button" value="Back">
                                <input class="nav_button margL2" type="submit" id="CrdHP_btn_submit-recommend" value="Submit">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="innerContainer clearfix bottomPadding">
         <section class="main-section">
            <%-- <div class="banner" style="background:url('${applicationURL}banners/${banner.bannerAttachementPath}');" title="JetPrivilege Co-brand Credit Cards, Debit Cards & Corporate Cards"> --%>
<!--                 <div class="banner" style="background:url('https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/banners/Cards_1291px_364px_2_1.jpg') no-repeat; background-size: cover;" title="InterMiles Co-brand Credit Cards, Debit Cards &amp; Corporate Cards"> -->
<!--                 <div class="container">
<!--                     <div class="banner_textarea"> -->
<!--                         <h1>Earn JPMiles every time<br> -->
<!--                             you swipe our co-brand card. -->
<!--                         </h1> -->

<!--                         <div class="banner_button margT5"> -->
<!--                             <a href="#recommend-me-a-card"><button type="button" class="button_blue recommend_card pull-left" id="CrdHP_rec-me-a-card" onclick="$.fn.HelpMeChooseACardClick()">Help me choose a card</button></a> -->
<!--                            <a href="${applicationURL}credit-score-application-form "><button type="button" title="Check your credit score here" class="button_blue pull-left" id="checkCreditScoreBtn" onclick="$.fn.HelpMeChooseACardClick()">Check Your Credit Score</button></a> -->
<!--                             <a href="#I-will-choose-my-card"><button type="button" class="button_blue2" id="CrdHP_choose-own-card" onclick="$.fn.IwillChooseMyCardClick()">I will choose my own card</button></a> -->
<!--                         </div> -->
<!--                     </div> -->
<!--                 </div> --> 
<!--                 <div class="container"> -->
<!--                     <div class="banner_textarea"> -->
<!--                         <h1 style="font-weight: bold"> <strong>Power-Packed Benefits with InterMiles Co-Brand Cards</strong> </h1><br><p class="bannertxt">Use your Co-brand Cards to accrue InterMiles on all your spends & redeem them for exciting travel and lifestyle rewards!  </p>                    -->
<!--                     </div> -->
<!--                 </div> -->
<!--             </div> -->

<!-- ************************** -->

<div class="row">
    
        <!-- The carousel -->
        <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel">
			
			<!-- Indicators -->
			<ol class="carousel-indicators">
			<c:forEach items="${banner}" var="list" varStatus="i" begin="0" end="3">
			 <c:if test="${list.cpNo == 12}">
				<li data-target="#transition-timer-carousel" data-slide-to="0" class="active"></li>
			  </c:if>
			  <%-- <c:if test="${list.cpNo == 15}">
				<li data-target="#transition-timer-carousel" data-slide-to="1"></li>
				</c:if>
				<c:if test="${list.cpNo == 14}"> 
				<li data-target="#transition-timer-carousel" data-slide-to="2"></li>
				</c:if>
				 
				<c:if test="${list.cpNo == 13}">
				<li data-target="#transition-timer-carousel" data-slide-to="3"></li>
				</c:if> --%>
				
				</c:forEach>
			</ol>

			<!-- Wrapper for slides -->
		<div id="content-desktop">
		
			<div class="carousel-inner">
			<c:forEach items="${banner}" var="list" varStatus="i">
     			<c:if test="${list.bpNo == 4}">
     			
     			<c:if test="${list.cpNo == 12}">
                   <div class="item active">
                   <div class="mobHt"> 
<%--                     <a href="https://intermiles.poshvine.com/" target="_blank"><img src="${applicationURL}${list.bannerAttachementPath}" style="background-size: cover;" width="100%" /></a> --%>
                    <a href="https://cards.intermiles.com/cardoffers/" target="_blank"><img src="${applicationURL}${list.bannerAttachementPath}" style="background-size: cover;" width="100%" /></a>
                    <div class="container">
                    <!-- <div class="carousel-caption_HDFC banner_textarea carousel-caption_HDFC_Mob">
                    <h1 class="carousel-caption_HDFC-header" style="font-weight: bold"><strong>Power-Packed Benefits with InterMiles Co-Brand Cards</strong></h1><br>
                    <p class="carousel-caption_HDFC-text hidden-sm hidden-xs bannertxt">Use your Co-brand Cards to accrue InterMiles on all your spends &amp; redeem them for exciting travel and lifestyle rewards!  
                        </p> 
                    </div> -->
                    </div>
                    </div>
                </div>  
                </c:if>
                
			<%-- <c:if test="${list.cpNo == 15}">
				<div class="item">
				 <div class="mobHt"> 
                    <img src="${applicationURL}${list.bannerAttachementPath}" style="background-size: cover; " width="100%" onclick="getCardAppln(${list.cpNo})"/>
                    <div class="carousel-caption_HDFC carousel-caption_HDFC_Mob">
                       <p class="carousel-caption_HDFC-text hidden-sm hidden-xs">
                           &nbsp;&nbsp;&nbsp;InterMiles HDFC Bank Diners Club Credit Card
                        </p>
                    </div>
                    </div>
                </div>
                </c:if>
               
                
                <c:if test="${list.cpNo == 14}">
                <div class="item">
                 <div class="mobHt"> 
                    <img src="${applicationURL}${list.bannerAttachementPath}" style="background-size: cover;" width="100%" onclick="getCardAppln(${list.cpNo})"/>
                    <div class="carousel-caption_HDFC carousel-caption_HDFC_Mob">
 					<p class="carousel-caption_HDFC-text hidden-sm hidden-xs">
                            &nbsp;&nbsp;&nbsp;InterMiles HDFC Bank Signature Credit card 
                        </p>
                    </div>
                    </div>
                </div>
                </c:if>
                
                <c:if test="${list.cpNo == 13}">
                <div class="item">
                 <div class="mobHt"> 
                    <img src="${applicationURL}${list.bannerAttachementPath}" style="background-size: cover;" width="100%" onclick="getCardAppln(${list.cpNo})"/>
                    <div class="carousel-caption_HDFC carousel-caption_HDFC_Mob">
						<p class="carousel-caption_HDFC-text hidden-sm hidden-xs">
                            &nbsp;&nbsp;&nbsp;InterMiles HDFC Bank Platinum Credit Card 
                        </p>
                    </div>
                    </div>
                </div>
                </c:if> --%>
                
                 </c:if>
                </c:forEach>
                  
                <!-- <div class="item">
                    <img src="http://placehold.it/1200x400/AAAAAA/888888" />
                    <div class="carousel-caption">
                        <h1 class="carousel-caption-header">Slide 2</h1>
                        <p class="carousel-caption-text hidden-sm hidden-xs">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dignissim aliquet rutrum. Praesent vitae ante in nisi condimentum egestas. Aliquam.
                        </p>
                    </div>
                </div>
                
                <div class="item">
                    <img src="http://placehold.it/1200x400/888888/555555" />
                    <div class="carousel-caption">
                        <h1 class="carousel-caption-header">Slide 3</h1>
                        <p class="carousel-caption-text hidden-sm hidden-xs">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse dignissim aliquet rutrum. Praesent vitae ante in nisi condimentum egestas. Aliquam.
                        </p>
                    </div>
                </div> --> 
               
            </div>
           </div>
            
            
           <div id="content-mobile">
            	<div class="carousel-inner">
			<c:forEach items="${banner}" var="list" varStatus="i">
     			<c:if test="${list.bpNo == 4}">
     			
     			<c:if test="${list.cpNo == 13}">
                   <div class="item active">
                   <div class="mobHt"> 
<%--                     <a href="https://intermiles.poshvine.com/" target="_blank"><img src="${applicationURL}${list.bannerAttachementPath}" style="background-size: cover;" width="100%" /></a> --%>
                    <a href="https://cards.intermiles.com/cardoffers/" target="_blank"><img src="${applicationURL}${list.bannerAttachementPath}" style="background-size: cover;" width="100%" /></a>
                    <div class="container">
                    <!-- <div class="carousel-caption_HDFC banner_textarea carousel-caption_HDFC_Mob">
                    <h1 class="carousel-caption_HDFC-header" style="font-weight: bold"><strong>Power-Packed Benefits with InterMiles Co-Brand Cards</strong></h1><br>
                    <p class="carousel-caption_HDFC-text hidden-sm hidden-xs bannertxt">Use your Co-brand Cards to accrue InterMiles on all your spends &amp; redeem them for exciting travel and lifestyle rewards!  
                        </p> 
                    </div> -->
                    </div>
                    </div>
                </div>  
                </c:if>
 
                 </c:if>
                </c:forEach>
        
            </div>
           </div>
            
            
               
			<!-- Controls -->
			<!-- <a class="left carousel-control" href="#transition-timer-carousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#transition-timer-carousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a> -->
             
            <!-- Timer "progress bar" -->
<!--             <hr class="transition-timer-carousel-progress-bar animate progressBar" /> -->
		
		</div>
		
    </div>

<!-- ************************* -->
        </section> 

<!-- 		<marquee class="scrollmydiv" direction="left"> -->
<!-- 				*For InterMiles HDFC Bank co brand credit/debit cards - New features will be live/applicable from 25th September 2020. -->
<!-- 		</marquee> -->


        <!--style="color: #28262c;font-weight: 500;display: inline-block;"-->
        <!-- banner end --> 
        <div class="clearfix"></div>
        <!-- content start -->  
        <section class="container">
            <!-- <marquee id="warn" scrollamount="4" style="font-size: 10.0pt;font-family: 'Montserrat','sans-serif'!important; color: #5d5b62!important; font-weight: bold">Your existing Jet Airways co-brand credit card continues to offer lifestyle privileges and rewards in the form of JPMiles on all applicable spends. Redeem them for a reward of your choice - be it Select Flights, Hotel stays, Fuel and over 2500+ merchandise options at the JetPrivilege Reward Store. Card benefits associated with Jet Airways are temporarily unavailable.</marquee> -->
            <div class="card_listing_page">
               <%--  <div class="cont_area">
                    <h1>${cardList[0].homePageTitle}</h1>
                    <p>${cardList[0].description}</p>
                </div> --%>
                <form:hidden path="gmNo" id="gmNo" value="${cardList[0].gmNo}"/>
                <div class="checkname" id="${pageinfo}" hidden></div>
                <!-- <div class="swiptopband">
                    <div class="dropdown swiptopbanddrop">
                        <a data-toggle="dropdown" href="#">
                        <span>Swipe</span></a>

                    </div>
                </div> -->
                
                <!-- filter area 2 start -->
                <div class="filterarea2">
                    <ul>
                        <li><span>Filter By:</span></li>
                        <li>
                            <div class="filterareadrop2">
                                <select class="" id="filterareadropdown">
                                    <c:forEach var="cardType" items="${cardType}">

                                        <c:if test="${countryset!='Bangladesh'}">
                                            <option value="${cardType.cardType}" ${cardType.cardType== category ? 'selected':''}>${cardType.cardType}

                                                <c:if test="${cardType.cardType=='Personal Debit Card'}">(<c:out value="${cardList1.size()}"></c:out>)</c:if>
                                                <c:if test="${cardType.cardType=='Corporate Card'}">(<c:out value="${cardList2.size()}"></c:out>)</c:if>
                                                <c:if test="${cardType.cardType=='Personal Credit Card'}">(<c:out value="${cardList3.size()}"></c:out>)</c:if>
                                                <c:if test="${cardType.cardType=='Others'}">(<c:out value="${cardList4.size()}"></c:out>)</c:if>
                                                <c:if test="${cardType.cardType=='All'}">(<c:out value="${cardList5.size()}"></c:out>)</c:if>
                                                            
                                                    </option>
                                        </c:if>

                                        <c:if test="${countryset=='Bangladesh'}">
                                            <%--    <c:if test="${cardType.cardType=='Personal Credit Card'}">--%>
                                            <option value="${cardType.cardType}" ${cardType.cardType== category ? 'selected':''}>${cardType.cardType}

                                                <c:if test="${cardType.cardType=='Personal Debit Card'}">(<c:out value="${cardList1.size()}"></c:out>)</c:if>
                                                <c:if test="${cardType.cardType=='Corporate Card'}">(<c:out value="${cardList2.size()}"></c:out>)</c:if>
                                                <c:if test="${cardType.cardType=='Personal Credit Card'}">(<c:out value="${cardList3.size()}"></c:out>)</c:if>
                                                <c:if test="${cardType.cardType=='Others'}">(<c:out value="${cardList4.size()}"></c:out>)</c:if>
                                                <c:if test="${cardType.cardType=='All'}">(<c:out value="${cardList5.size()}"></c:out>)</c:if>

                                                    </option>
                                            <%-- </c:if>--%>
                                        </c:if>

                                    </c:forEach>


                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="filterareadrop2"> 




                                <c:choose>

                                    <c:when test="${category =='Personal Credit Card'||category =='Personal Debit Card' || category =='Corporate Card' || category =='Others' || category =='All'}">
                                        <select class="" id="filterareadropdownhide" >

                                            <c:forEach var="country" items="${country}">
                                                <option value="${country.cardName}" ${country.cardName == countryset ? 'selected':''}>${country.cardName}</option>  
                                            </c:forEach>

                                        </select>
                                    </c:when>

                                    <c:when test="${empty category}">
                                        <select class="" id="filterareadropdownhide" >

                                            <c:forEach var="country" items="${country}">
                                                <option value="${country.cardName}" ${country.cardName == countryset ? 'selected':''}>${country.cardName}</option>  
                                            </c:forEach>

                                        </select>
                                    </c:when>


                                </c:choose>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- filter area 1 start -->

                <div class="row filter_div margT2">
                    <div class="col-sm-2">
                        <label>Filter By: </label>
                    </div>
                    <div class="col-sm-2">
                        <span class="banks" id="CrdHP_fltr_Bank">
                            <label class="dropDown"> Bank </label>
                            <img src="${applicationURL}static/images/co-brand/filter_dropdown.png" alt="" class="margL10">
                        </span>
                        <span class="margL15"> | </span>					
                    </div>
                    <div class="banks_listing parent_div hidden">
                        <div class="arrow-up"></div>
                        <ul class="banks_list">
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                                <c:forEach items="${bankList}" var="list">	
                                    <%--<c:forEach items="${bankList}" var="list">--%>
                                    <c:set var="refreshSent" value="false"/>

                                <li>
                                    <c:choose>
                                        <c:when test="${bankset!=''}">
                                            <c:set var="numbers" value="${bankset}" />
                                            <c:set var="splitNumbers" value="${fn:split(numbers,',')}" />
                                            <c:set var="joinedNumbers" value="${fn:join(splitNumbers,' ')}" />

                                            <c:forTokens var="token" items="${bankset}" delims=",">
                                                <c:if test="${list.bpNo == token}">
                                                    <c:set var="refreshSent" value="true"/>
                                                </c:if>
                                            </c:forTokens>
                                            <input type="checkbox" name ="bankName" id="bankName${list.bpNo}" ${refreshSent == true ? 'checked':''} class="bankNum" value="${list.bpNo}">

                                            <label for="bankName${list.bpNo}" >
                                                <img src="${applicationURL}banks/${list.bankImagePath}" class="bank_logos">
                                                <c:choose>
                                                    <c:when test="${list.bpNo eq 1}">
                                                        <span>${list.bankName} <sup>&reg;</sup></span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <span>${list.bankName}</span>
                                                    </c:otherwise>
                                                </c:choose>
                                            </label>
                                        </c:when>  
                                        <c:otherwise>
                                            <input type="checkbox" name ="bankName" id="bankName${list.bpNo}" class="bankNum" value="${list.bpNo}">     
                                            <label for="bankName${list.bpNo}" >
                                                <img src="${applicationURL}banks/${list.bankImagePath}" class="bank_logos">
                                                <c:choose>
                                                    <c:when test="${list.bpNo eq 1}">
                                                        <span>${list.bankName} <sup>&reg;</sup></span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <span>${list.bankName}</span>
                                                    </c:otherwise>
                                                </c:choose>
                                            </label>
                                        </c:otherwise>
                                    </c:choose>    

                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                    <div class="col-sm-3 ">
                        <span class="joiningFees" id="CrdHP_fltr_Joiningfees">
                            <label>Joining Fees </label>
                            <img src="${applicationURL}static/images/co-brand/filter_dropdown.png" class="margL10">	
                        </span>
                        <span class="margL15"> | </span>	
                        <div class="slider_div parent_div hidden">
                            <div class="arrow-up"></div>
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                            <div class="slider_valu  margB5 margT5">
                                <div style="display:inline">
                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span>
                                    <span>500</span></div>
                                <div style="display:inline; float:right;">
                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span>
                                    <span> 10,000</span></div>
                            </div>
                            <input min="500" max="10000" step="500" data-orientation="horizontal" type="range" id="fee_range">
                            <div class="text-center margT7">
                                <input class="filter_button button_red" id="filter_jf" type="button" value="Done" onclick="submitFilterAdobe()">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 life_benefits_parent">
                        <span class="life_benefits" id="CrdHP_fltr_LifeStyle-Benefits">
                            <label>Lifestyle Benefits</label>
                            <img src="${applicationURL}static/images/co-brand/filter_dropdown.png" class="margL10">	
                        </span>
                        <div class="life_benefits_list parent_div hidden">
                            <div class="arrow-up"></div>
                            <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                            <div class="row">
                                <div class="life_benefits_holder" >
                                    <ul>
                                        <c:forEach items="${prefList}" var="list">
                                            <c:set var="refreshSent" value="false"/>

                                            <li>
                                                <c:choose>
                                                    <c:when test="${lsBenefitset!=''}">
                                                        <c:set var="numbers" value="${lsBenefitset}" />
                                                        <c:set var="splitNumbers" value="${fn:split(numbers,',')}" />
                                                        <c:set var="joinedNumbers" value="${fn:join(splitNumbers,' ')}" />

                                                        <c:forTokens var="token" items="${lsBenefitset}" delims=",">
                                                            <c:if test="${list.lsbpNo == token}">
                                                                <c:set var="refreshSent" value="true"/>
                                                            </c:if>
                                                        </c:forTokens>
                                                        <input class="lsbpNum" name ="lsbpName" id="lsbpName${list.lsbpNo}" ${refreshSent == true ? 'checked':''} type="checkbox" value="${list.lsbpNo}" onclick="selectBenefits(this);">
                                                        <label for="lsbpName${list.lsbpNo}" >
                                                            <img src="${applicationURL}lifestyles/${list.lsbpImageIconPath}"  alt=""  >
                                                            <span>${list.imageAsIconText}</span>
                                                            <img src="${applicationURL}lifestyles/${list.lsbpImageIconBluePath}"  alt="" style="display:none">
                                                            <span style="display:none">${list.imageAsIconText}</span>
                                                        </label>
                                                        <%--
                                                         <input type="checkbox" name ="bankName" id="bankName${list.lsbpNo}" ${refreshSent == true ? 'checked':''} class="bankNum" value="${list.bpNo}">

                                                      <label for="bankName${list.bpNo}" >
                                                      <img src="${applicationURL}banks/${list.bankImagePath}" class="bank_logos">
                                                      <c:choose>
                                                              <c:when test="${list.bpNo eq 1}">
                                                                      <span>${list.bankName} <sup>&reg;</sup></span>
                                                             </c:when>
                                                             <c:otherwise>
                                                                      <span>${list.bankName}</span>
                                                             </c:otherwise>
                                                     </c:choose>
                                                     </label>--%>
                                                    </c:when>  
                                                    <c:otherwise>
                                                        <input class="lsbpNum" name ="lsbpName" id="lsbpName${list.lsbpNo}" type="checkbox" value="${list.lsbpNo}" onclick="selectBenefits(this);">
                                                        <label for="lsbpName${list.lsbpNo}" >
                                                            <img src="${applicationURL}lifestyles/${list.lsbpImageIconPath}"  alt=""  >
                                                            <span>${list.imageAsIconText}</span>
                                                            <img src="${applicationURL}lifestyles/${list.lsbpImageIconBluePath}"  alt="" style="display:none">
                                                            <span style="display:none">${list.imageAsIconText}</span></label>
                                                        </c:otherwise>
                                                    </c:choose>


                                                <%--
                                                <input class="lsbpNum" name ="lsbpName" id="lsbpName${list.lsbpNo}" type="checkbox" value="${list.lsbpNo}" onclick="selectBenefits(this);">
                                                                                              <label for="lsbpName${list.lsbpNo}" >
                                                                                                            <img src="${applicationURL}lifestyles/${list.lsbpImageIconPath}"  alt=""  >
                                                                                                            <span>${list.imageAsIconText}</span>
                                                                                                    <img src="${applicationURL}lifestyles/${list.lsbpImageIconBluePath}"  alt="" style="display:none">
                                                                                              <span style="display:none">${list.imageAsIconText}</span></label>--%>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 clearFilters">
                        <span id="clear_fliter" class="first-child">Clear Filters</a></span>
                        <span class="sort_by" id="CrdHP_fltr_sort-by">Sort By</span>
                        <div class="sort_listing parent_div hidden">
                            <div class="arrow-up"></div>
                            <ul class="sort_list">
                                <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                                <li class="sorting_ascending">
                                    Bank (A-Z)
                                </li>
                                <li class="sorting_descending">
                                    Bank (Z-A)
                                </li>
                                <li class="sort_by_fee_asc">
                                    Fees (Low to High)
                                </li>
                                <li class="sort_by_fee_dsc">
                                    Fees (High to Low)
                                </li>
                                <li class="sort_by_lyf_asc">
                                    Lifestyle Benefit (Min to Max)
                                </li>
                                <li class="sort_by_lyf_dsc">
                                    Lifestyle Benefit (Max to Min)
                                </li>
                                <li class="air_ticket_asc">
                                    Air Tickets (Min to Max)
                                </li>
                                <li class="air_ticket_dsc">
                                    Air Tickets (Max to Min)
                                </li>
                            </ul>
                        </div>										
                    </div>												
                </div>
                <div style="padding-top: 10px; text-align: center;" class="spendsVal_error">
                    <span id="error_msg" style="font-size: 14px; color: red; display:none;"></span>
                </div>
                <div class="freeFlights_calc margT2" id="CrdHP_btn_cal-ur-ff-asper-spend">
                    <div class="flight-calc-holder">
                        <span>
                            <p> Calculate your free flights based on your monthly spends </p>
                        </span>
                    </div>
                    <div class="spends_calc">
                        <div class="col-md-12">
                            <p class="amtvalue">Annual Spends : INR <span class="slider-output" id="yearly_bill">0</span></p>

                        </div>
                        <div class="col-md-9">
                            <div class="range-example-5"></div>
                            <div class="imagebar"><img src="${applicationURL}static/images/co-brand/numbar.jpg"/></div>

                        </div>

                        <div class="col-md-3">
                            <div class="rightdata">
                                <div class="aheadiv">INR</div>
                                <input id="unranged-value1" type="text" name="amountInput"  value="0" maxlength="9" inputmode="numeric" />
                                <div class="donebut donebutdone">Done</div>
                            </div>   
                            <div class="slider_error_div"></div>
                        </div>
                        <!--                    <div class="spends_calc margT2">
                                                <div class="slider_wrapper">								
                        
                                                    <div class="row">
                                                        <div class="slider_division col-sm-8">
                                                            <div class="slider_valu rangeMobile">
                                                                <div style="display:inline; float:left">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5,000</span> 
                                                                </div>
                                                                <div style="display:inline; float:right;">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span>
                                                                </div>
                                                            </div>
                                                            <ul class="slider_view">
                                                                <li class="rangeWeb">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5,000</span>
                                                                </li>
                                                                <li>
                                                                    <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal" class="">
                                                                </li>
                                                                <li class="rangeWeb">
                                                                    <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="annualSpends_wrapper col-sm-4">
                                                            <div class="annualSpends text-center">
                                                                <span>Annual Spends: </span><span class="annualSpends_value">0</span>
                                                            </div>
                                                            <div class="text-center">
                                                                <input type="button" class="filter_button button_red annual_spends" id="CrdHP_btn_cal-ur-ff-asper-spend" value="Done">
                                                            </div>
                                                        </div>
                                                    </div>  
                                                </div>-->
                    </div>

                </div>
                <div class="clearfix"></div>
                
                <div class="scrollmydiv" align="center">
				*For InterMiles HDFC Bank co brand credit cards - New features will be live/applicable from 25th September 2020.
	 			</div> 
                
                <div class="header_freeze">
                    <div class="features_title title_header benefits_web">
                        <div class="row">
                            <div class="col-sm-2">
                                <p>Cards</p>
                            </div>

                            <c:forEach items="${groupHeadings}" var="heading">
                                <div class="col-sm-2">
                                    <p>${heading}</p>
                                </div>
                            </c:forEach>
                            <div class="col-sm-2 free_flight_basis">
                                <div class="">
                                    <p class="freeFlight_spends">Free flight basis
                                    </p>
                                    <div class="freeFlight_spends_calc parent_div hidden">
                                        <div class="arrow-up"></div>
                                        <div class="pull-right close_filter"><img src="${applicationURL}static/images/co-brand/close_btn.png"  alt=""></div>
                                        <div class="slider_valu  margB5 margT5">
                                            <div style="display:inline; text-align:left !important;">INR 5,000</div>
                                            <div style="display:inline; float:right;"> INR 15 Lakhs</div>
                                        </div>
                                        <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal">
                                        <div class="annualSpends margT10 text-center">
                                            <span>Annual Spends: </span><span class="annualSpends_value">INR 60,000</span>
                                        </div>
                                        <div class="text-center">
                                            <input class="filter_button button_red annual_spends" type="button" value="Done">
                                        </div>
                                    </div>
                                    <p>spends
                                        <a class="tooltips" href="javascript:void(0);"><img src="${applicationURL}static/images/co-brand/inrimg.png">
                                            <span class="tooltip_content" id="CrdHP_icon_info-i"> 
                                                <span class="pull-right close_toolTip"><img src="${applicationURL}static/images/co-brand/close_btn.png" alt=""></span>
                                                The free flights calculation basis spends is calculated on the InterMiles earned basis the spends & on the minimum award flight redemption of 5000 InterMiles. 
                                                <br><span class="terms_conditions" style="color: black; text-decoration: underline;" id="tcLink">Terms & Conditions Apply</span>
                                            </span> 
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-2 noBorder">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sort_list_div">
                    <c:forEach items="${cardList}" var="cdList" varStatus="i">
                        <div class="features margT2">
                            <div class="features_desc" id="card_row${cdList.cpNo}">
                                <div class="row">
                                    <div class="col-sm-2 pad0">
                                        <div class="card_name">
                                            <h2 id="cardName${cdList.cpNo}">${cdList.cardName}</h2>
                                        </div>

                                        <!--New adobe changes 16th 07 18 start-->
                                        <div class="cardinfo hidden" id="cardinfo">${list.cardInfo}</div>
                                        <input type="hidden" id="cardinfo" value="${cdList.cardInfo}">
                                        <!--New adobe changes 16th 07 18 end-->

                                        <input type="hidden" id="cpNo" class="cpNo_sort" value="${cdList.cpNo}">
                                        <!--for internal-->
                                        <input type="hidden" id="cpDpNo" class="cpDpNo_sort" value="${cdList.cardDisplayorder}">
                                        <input type="hidden" id="bpNo" value="${cdList.bpNo}">
                                        <input type="hidden" class="bankName_sort" id="list_bankName${cdList.cpNo}" value="${cdList.bankName}">
                                        <input type="hidden" id="nwName${cdList.cpNo}" value="${cdList.networkName}">
                                        <input type="hidden" id="lspbList" value="${cdList.lsbpList}">
                                        <input type="hidden" class="lspbListSize" id="lspbListSize" value="${fn:length(cdList.lsbpList)}"  >
                                        <div class="card_img_holder">
                                            <span class="card_img" >
                                                <c:choose>
                                                    <c:when test="${not empty cdList.cardImagePath}">
                                                        <img alt="${cdList.cardName} - Intermiles" src="${applicationURL}cards/${cdList.cardImagePath}" height="50" width="50" id="card_img${cdList.cpNo}"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <img alt="${cdList.cardName} - Intermiles" src="${applicationURL}static/images/co-brand/noimage_bg.jpg" height="50" width="50" id="card_img${cdList.cpNo}"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </span>
                                            <span class="card_value">
                                                <label>Fees: </label>
                                                <span id="feesId${cdList.cpNo}" class="fees_class" data-fees="${cdList.fees}"><%-- INR ${cdList.fees} --%></span>
                                            </span>
                                            <div class="details_link" >
                                                <a href="javascript:void(0);" style="color: #28262c; text-decoration: underline;" data-id="${cdList.cpNo}" data-name="${cdList.imageText}" data-utm_source="${utm_source}"
                                                   data-utm_medium="${utm_medium}" data-utm_campaign="${utm_campaign}" class="forMoreDetails" id="CrdHP_${cdList.bankName}&${cdList.cardName}&${cdList.networkName}_details" onclick="moreDetailsClick('${cdList.imageText}', '${cdList.imageText}','${cdList.cardName}','${cdList.bankName}')">For more details ></a>
                                            </div>
                                            <div class="joining_Benefit_info" data-target="#joiningbenefits" id="joiningbenefits" data-cpNo="${cdList.cpNo}" >		
                                                <div class="label_joining"> <label>Joining Benefit</label></div>		
                                            </div>
                                        </div>
                                    </div>

                                    <c:forEach items="${cdList.featureList}" var="feature">
                                        <div class="col-sm-2 webView">
                                            <div class="list_cont">
                                                <c:if test="${not empty feature}"> 
                                                    <div class="home_feature">
                                                        ${feature}                                       
                                                    </div>
                                                </c:if> 	                                  
                                            </div>
                                        </div>
                                    </c:forEach> 

                                    <div class="col-sm-2 ">


                                        <c:choose>
                                            <c:when test="${cdList.freeFlightsMessageFlag eq 1}">
                                                <div class="col-sm-12 webView mob_view">
                                                    <div class="list_cont">
                                                        <div class="home_feature">
                                                            ${cdList.freeFlightsMessage} 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="lifestyle_benefits" data-target="#benefits" id="benefits" data-cpNo="${cdList.cpNo}">
                                                    <div class="benifits-img-holder"> <img src="${applicationURL}static/images/co-brand/benifits_icon.png"></div>
                                                    <label>Benefits</label>
                                                </div>
                                            </c:when>
                                            <c:otherwise>

                                                <div class="free_flights_desc" data-target="spends_slider">
                                                    <p onclick="$.fn.actionClickCT('Calculate your free flights','${cdList.cardName}','${cdList.bankName}');" class="topScrollRange">Calculate your free flights</p>
                                                </div>

                                                <div class="free_flights_div">
                                                    <div class="free_flights">
                                                        <p>You get</p>
                                                        <p class="ff" id="freeFlights${cdList.cpNo}">XX</p>
                                                        <p>Free Flights</p>
                                                        <p class="gg" id="jpMiles${cdList.cpNo}">on earning<br>XXXX InterMiles</p>
                                                    </div>
                                                </div>
                                                <div class="lifestyle_benefits" data-target="#benefits" id="benefits" data-cpNo="${cdList.cpNo}">
                                                    <div class="benifits-img-holder"> <img src="${applicationURL}static/images/co-brand/benifits_icon.png"></div>
                                                    <label>Benefits</label>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>

                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="col-sm-2 ">
                                        <div class="instant_approval">
                                            <c:if test="${cdList.bpNo eq 1}">
                                                <p class="textColor_orange">
                                                    ${cdList.approvalMessage} 
                                                </p>
                                            </c:if>
<!--                                            &nbsp;-->
                                            <div>
                                                <c:choose>
                                                    <c:when test="${cdList.bpNo eq 1}">
                                                        <c:choose>
                                                            <c:when test="${(cdList.applyflag eq 0)}">
                                                                <div class="instAppr_button_holder " id="CrdHP_${cdList.bankName}&${cdList.cardName}_${cdList.networkName}_apply">
                                                                    <ul class="applyAmex" data-name="${cdList.imageText}" data-spMailingID="${spMailingID}" data-spUserID="${spUserID}"
                                                                        data-spJobID="${spJobID}" data-spReportId="${spReportId}" data-utm_source="${utm_source}"
                                                                        data-utm_medium="${utm_medium}" data-utm_campaign="${utm_campaign}">
                                                                        <li>
                                                                            <div class="apply-inst apply_instantAppr" onclick="adobeAmexAmericanClick('${cdList.cardName}', '${cdList.cardName}', '${cdList.bankName}', '${cdList.fees}')">
                                                                                <span><img  name = "evAmex" src="${applicationURL}static/images/co-brand/apply_icon_white.png"></span>
                                                                                <span>Apply</span>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="apply_instantAppr2" onclick="adobeAmexClick('${cdList.cardName}', '${cdList.cardName}', '${cdList.bankName}', 'Submit JPNumber', 'Page Refresh', '${cdList.fees}')">
                                                                                <span><img src="${applicationURL}static/images/co-brand/instant_apprvl-icon.png"></span> 
                                                                                <span>Instant Decision</span>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <div class="apply_position margB5">
                                                                    <!--                                                            <div class="apply_btn" >
                                                                                                                                    <span>
                                                                                                                                        <img src="${applicationURL}static/images/co-brand/apply_icon_white.png" class=""> 
                                                                                                                                    </span>
                                                                                                                                    <span>
                                                                                                                                        Apply
                                                                                                                                    </span>
                                                                                                                                </div>-->
                                                                </div> 

                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>


                                                    <c:when test="${(cdList.bpNo eq 2)}">

                                                        <c:choose>
                                                            <c:when test="${(cdList.applyflag eq 0)}">
                                                                <div class="instAppr_button_holder " id="CrdHP_${cdList.bankName}&${cdList.cardName}_${cdList.networkName}_apply">

                                                                    <ul class="applyIcici" data-name="${cdList.imageText}" data-spMailingID="${spMailingID}" 
                                                                        data-spUserID="${spUserID}" data-spJobID="${spJobID}" 
                                                                        data-spReportId="${spReportId}" data-utm_source="${utm_source}"
                                                                        data-utm_medium="${utm_medium}" data-utm_campaign="${utm_campaign}">
                                                                        <li>
                                                                            <div class="apply-inst apply_instantAppr" onclick="adobeAmexAmericanClick('${cdList.cardName}', '${cdList.cardName}', '${cdList.bankName}','','', '${cdList.fees}')">
                                                                                <span><img name = "evAmex" src="${applicationURL}static/images/co-brand/apply_icon_white.png"></span>
                                                                                <span>Apply</span>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="apply_instantAppr2">
                                                                                <span><img src="${applicationURL}static/images/co-brand/instant_apprvl-icon.png"></span> 
                                                                                <span>Instant Decision</span>
                                                                            </div>
                                                                        </li>
                                                                    </ul>

                                                                </div>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <div class="apply_position margB5">
                                                                    <!--                                                            <div class="apply_btn" >
                                                                                                                                    <span>
                                                                                                                                        <img src="${applicationURL}static/images/co-brand/apply_icon_white.png" class=""> 
                                                                                                                                    </span>
                                                                                                                                    <span>
                                                                                                                                        Apply
                                                                                                                                    </span>
                                                                                                                                </div>-->
                                                                </div> 

                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>



                                                    <c:when test="${(cdList.bpNo eq 4)}">
                                                        <c:choose>
                                                            <c:when test="${(cdList.applyflag eq 0)}">

                                                                <div class="apply_position margB5">
                                                                    <div class="apply_btn hdfc_asking_apply" data-cpName="${cdList.cardName}" data-bpNo="${cdList.bpNo}" data-cpNo="${cdList.cpNo}" data-NoNO="${cdList.networkName}" data-toggle="modal" data-target="#askingUser" onclick="adobeAmexClick('${cdList.cardName}', '${cdList.cardName}', '${cdList.bankName}', 'Submit JPNumber', 'Page Refresh','${cdList.fees}')">
                                                                        <span>
                                                                            <img src="${applicationURL}static/images/co-brand/apply_icon_white.png" class=""> 
                                                                        </span>
                                                                        <span>
                                                                            Apply
                                                                        </span>
                                                                    </div>
                                                                </div> 

                                                                <div class="modal fade hdfc_popup" id="askingUser" tabindex=-1 role="dialog">
                                                                    <div class="modal-dialog askingPopup-modal-dialog">
                                                                        <!-- Modal content-->
                                                                        <div class="modal-content">
                                                                            <div class="modal-body">
                                                                                <div class="proceedPopup_close pull-right">
                                                                                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="">
                                                                                </div>
                                                                                <div class="askingPopup">
                                                                                    <div class="margT2 colorDarkBlue text-center">
                                                                                        <h2 class="heading_txt">Are you an existing HDFC Bank Credit Card holder?</h2>
                                                                                    </div>
                                                                                    <div class="text-center margT11">
                                                                                        <a class="hdfc_credit_card_yes" id="CrdHP_${cdList.cardName}_${cdList.networkName}_YES" data-cpName="${cdList.cardName}" data-bpNo="${cdList.bpNo}" data-NoNO="${cdList.networkName}" data-toggle="modal" data-target="#userWithCreditCard" onclick="$.fn.existingCreditCardHolderClick('Yes-Existing Card Holder')">
                                                                                            <h1>YES</h1>
                                                                                        </a>
                                                                                        <a class="non_amex_apply" id="CrdHP_${cdList.cardName}_${cdList.networkName}_NO" data-toggle="modal" data-target="#abtYourself" onclick="noExistingCardHolderClick('No-Existing Card Holder', 'Submit JPNumber')">
                                                                                            <h1>No</h1>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal fade hdfc_popup" id="userWithCreditCard" tabindex=-1 role="dialog">
                                                                    <div class="modal-dialog askingPopup-modal-dialog">
                                                                        <!-- Modal content-->
                                                                        <div class="modal-content">
                                                                            <div class="modal-body">
                                                                                <div class="proceedPopup_close pull-right">
                                                                                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="">
                                                                                </div>
                                                                                <div class="userWithCreditCardPopup">

                                                                                    <!--                                                        <p>"Please note : Existing HDFC Bank credit card customer need to fill up the Upgrade Form
                                                                                                                                                and submit it to the nearest HDFC Bank branch to apply for another credit card.In this case, the form will be processed 
                                                                                                                                                basis the bank internal policy guidelines "
                                                                                                                                            </p>-->
                                                                                    <p>${bpupgrademessage}</p>
                                                                                    <div class="text-center midareabtn">
                                                                                        <a class="viewCobranCards" id="CrdHP_${cdList.cardName}_${cdList.networkName}_View-Others" href="${applicationURL}cardswidget?bank=<c:out value="1,2,3"></c:out> onclick="upgradeOtherCardsClickforYes('View Other-Existing Card Holder')"> View other Jet Airways Cobranded Cards</a>

                                                                                        <a class="upgradeNow" id="CrdHP_${cdList.cardName}_${cdList.networkName}_Upgrade" data-cpNo="${cdList.cpNo}" data-bpNo="${cdList.bpNo}" data-toggle="modal" onclick="upgradeOtherCardsClickforNo('Upgrade-Existing Card Holder', 'Submit JPNumber')"> 
                                                                                            <!--data-target="#abtYourself"-->
                                                                                            Upgrade Now
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <div class="apply_position margB5">
                                                                    <!--                                                            <div class="apply_btn" >
                                                                                                                                    <span>
                                                                                                                                        <img src="${applicationURL}static/images/co-brand/apply_icon_white.png" class=""> 
                                                                                                                                    </span>
                                                                                                                                    <span>
                                                                                                                                        Apply
                                                                                                                                    </span>
                                                                                                                                </div>-->
                                                                </div> 

                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>

                                                    <c:otherwise>
                                                        <c:choose>
                                                            <c:when test="${(cdList.applyflag eq 0)}">
                                                                <div class="apply_position margB5">
                                                                    <div class="apply_btn non_amex_apply" id="CrdHP_${cdList.bankName}&${cdList.cardName}_${cdList.networkName}_apply" data-cpNo="${cdList.cpNo}" data-bpNo="${cdList.bpNo}" data-toggle="modal" data-target="#abtYourself" onclick="adobeAmexClick('${cdList.cardName}', '${cdList.cardName}', '${cdList.bankName}', 'Submit JPNumber', 'Page Refresh','${cdList.fees}')">
                                                                        <span>
                                                                            <img src="${applicationURL}static/images/co-brand/apply_icon_white.png" class=""> 
                                                                        </span>
                                                                        <span>
                                                                            Apply
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </c:when>
                                                        </c:choose>

                                                    </c:otherwise>
                                                </c:choose>

                                            </div>
                                            <div class="row"></div>
                                        </div>

                                        <c:if test="${cdList.compareFlag eq 1}">    
                                            <div class="select_compare">
                                                <input type="checkbox" name='check' class="compareCards" value="${cdList.cpNo}" id="compare_cards" >
                                                <input type="hidden" id="pageName" value="home">
                                                <span>Select to compare</span>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>   

                <div class="no_cards hidden">
                    Sorry, there is no card matching your selected criteria.
                </div>
                <div class="terms_conditions">
                    <a class="creditfooter">Check Your Credit Score</a>
                </div> 
                <div class="terms_conditions">
                    <a href="https://www.jetprivilege.com/terms-and-conditions/cards" target="_blank" class="termfooter">Terms & Conditions Apply</a>
                </div>
            </div>
                                                
        </section>
    </section>
    
    <div class="container">                                            
     <div class="cont_area new_cont_area">
                    <!--<h1>Your existing credit cards now have new features. Check them out <a href="#cardslink">here</a>! *This CTA to take member to the card listing section*</h1>-->
                     <div class="que">Q. Are my JetPrivilege / Jet Airways Credit Cards still valid?</div>
                    <div class="ans">A: Yes! Your credit cards are still valid. You will now earn InterMiles on your existing cards, just as today, without any change. Our respective bank partners will get in touch with you with regards to the new plastics, when they are ready. </div>

                        
                    <div class="que">Q. What happens to my accumulated JPMiles?</div>   
                    <div class="ans">A: Your miles are now called InterMiles. All your accumulated miles will continue being available to you, just as they are, without any change in your InterMiles account. </div>
                    
                    <div class="que">Q. What is InterMiles?</div>
                    <div class="ans">A: InterMiles is the most rewarding Travel &amp; Lifestyle program you will ever need. At InterMiles we believe every mile count, so the more InterMiles you accumulate the higher your Tier and the more the benefits. So, move up with every Swipe! 
Accumulate InterMiles faster by booking flights &amp; hotels with us or by earning with over 150+ partners in the InterMiles program. 
Also, you can use your InterMiles everywhere - Redeem your InterMiles for flight tickets across any airline, for your hotel stays, free fuel or for thousands of options on our Reward Store!</div>
                    
                    
                     <div class="ans_faq"><a href="https://cards.intermiles.com/faq">  <b>Click here</b></a><b> for more FAQ'S &amp; Benefits  </b>  </div> </div>  </div>                                         
                                                
                                                
    <div class="filter_img"><img src="${applicationURL}static/images/co-brand/filter_img.png" alt=""></div>
    <section class="compare_card_holder hidden">
        <section class="container">
            <div class="compare_card">
                <ul>
                    <li>
                        <div class="card_holder"></div>
                    </li>
                    <li>
                        <div class="card_holder"></div>
                    </li>
                    <li>
                        <div class="card_holder"></div>
                    </li>
                    <li>
                        <div class="compare_btn_holder">
                            <button  type="button"  class="CrdHP_btn_compare disable_comp_btn"  id="CrdRC_Compare" data-utm_source="${utm_source}"
                                     data-utm_medium="${utm_medium}" data-utm_campaign="${utm_campaign}">Compare Cards</button>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </section>
</form:form>

<script>

    //Adobe code start
    var assistMeFlag = false;
    var name = "";
    var mobile = "";
    var email = "";
    var assistError = "";
    var lastAccessedField = "";

    name = $("#socialName").val();
    mobile = $("#socialPhone").val();
    email = $("#socialEmail").val();
    //Adobe code end
    
    var loggedInUser = '${sessionScope.loggedInUser}';

    window.onpageshow = function (event) {
        if (event.persisted) {
            window.location.reload()
        }
    };

    $(document).ready(function () {
        
       
        
       // alert("user agent inside the app is>>>>>>>"+window.navigator.userAgent);
        
        
        

        var callmeflag = '${callMeFlag}';
          console.log("callmeflag===>",callmeflag);
        if (callmeflag != "") {
            if (callmeflag == 0) {
                $("#social-profiles").removeClass('hidden');

            } else {
//                 $("#social-profiles").addClass('hidden'); 
                 $("#social-profiles").removeClass('hidden');             }
        } else {
            $("#social-profiles").addClass('hidden');
        }
        $("#enrollPhone,#jpNumber,#socialPhone").on("keydown, keypress", function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;


            }

        });
        $('#socialName').keydown(function (event) {
            var charCode = event.keyCode;
            if ((charCode > 64 && charCode < 91) || charCode == 8 || charCode == 9 || charCode == 32) {
                return true;
            } else {
                return false;
            }
        });
        $("#jpNumber").on('keyup', function (evt) {
//             console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 9) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $("#enrollPhone,#socialPhone").on('keyup', function (evt) {
//             console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 10) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        function handleRangeModal(legth)
        {
//        console.log("legth",legth)
            if (legth < 5000 || legth > 1500000) {
//             console.log("at keyup if");
                if ($(".slider_modal_div_error").is(':visible') == false) {
                    $('.slider_modal_div_error').css('display', 'block');
                    $(".slider_modal_div_error").html("Please enter spends value within the range.");
                }
                return false;
            } else {
                console.log('inside handle modal');
                $('.slider_modal_div_error').css('display', 'none');
                $(".slider_modal_div_error").html("");

                return true;
            }

        }
        function handleRange(valueoftext)
        {
//        console.log("legth",valueoftext)
            if (valueoftext < 5000 || valueoftext > 1500000) {
//             console.log("at keyup if");
                if ($(".slider_error_div").is(':visible') == false) {
                    $(".slider_error_div").show().html("Please enter spends value within the range.");
                }
                return false;
            } else {
                $(".slider_error_div").hide().html("");
                return true;
            }

        }
        var result_Value, result_Value_year;
        //ne cr2 milestone 2
        $('.joining_Benefit_info').on('click', function () {
//        alert("-------")
            var cpNo = parseInt($(this).attr('data-cpNo'));
            var gmNo = $('#gmNo').val();
            $.get(ctx + "getJoiningBenefitContent?cpNo=" + cpNo, function (data) {
                $('#joiningbenefitContent').html(data);
                $('#joiningbenefits').modal('show');
            });

        });
        $('#unranged-value1').on('keyup', function () {
//            console.log("at keyup");
            var valSpend = $("#unranged-value1").val();
            valSpend = valSpend.replace(/\,/g, '');
            return handleRange(valSpend);
//        if (valSpend < 5000 || valSpend > 1500000) {
////             console.log("at keyup check");
//            $(".slider_error_div").show().html("Please enter spends value within the range.");
//            return false;
//        }else{
//            $(".slider_error_div").hide().html("");
//            return true;
//        }

        });
        $('#unranged-value2').on('keyup', function () {
//            console.log("at keyup");
            var valSpend = $("#unranged-value2").val();
            valSpend = valSpend.replace(/\,/g, '');
            return handleRangeModal(valSpend);
//        if (valSpend < 5000 || valSpend > 1500000) {
////             console.log("at keyup check");
//               $('.slider_modal_div_error').css('display', 'block');
//            $(".slider_modal_div_error").html("Please enter spends value within the range.");
//            return false;
//        }else{
//             $('.slider_modal_div_error').css('display', 'none');
//            $(".slider_modal_div_error").html("");
//            return true;
//        }

        });






        slider();
        slidermodal();
        function slider() {
            $(".range-example-5").asRange({
                step: 1,
                min: 5000,
                max: 500000,
                format: function (value) {
                    getValue(value);
                    return result_Value;
                },
                onChange: function (valueslider) {

                    var value = $('.range-example-5').asRange('get');
//                    console.log( "--value", value);
                    var a = document.getElementById("unranged-value1");
                    getValue(value);
                    var valueoftext = $("#unranged-value1").val();
                    valueoftext = valueoftext.replace(/\,/g, '');
                    handleRange(valueoftext);
                    a.value = result_Value;
//                    console.log("result_Value_year1", result_Value_year)
                    $("#yearly_bill").html(result_Value_year);
                    if (value > 5000) {
//                        console.log("----------1--->", value);
                        $(".range-example-modal").asRange('set', value);
                    }


                }
            });
        }








//        console.log("at on change", $("#unranged-value1").val());
//        console.log("--->", $('.range-example-5').asRange('get'));
        getValue($('.range-example-5').asRange('get'));
        $("#unranged-value1").val(result_Value);
//        if($('.range-example-5').asRange('get') < 5000 || $('.range-example-5').asRange('get') > 1500000 ){
//       $(".slider_error_div").show().html("Please enter spends value within the range.");
//            return false;
//        }else{
//            $(".slider_error_div").hide().html("");
//            return true;
//        }

        $("#yearly_bill").html(result_Value_year);
        getValue($('.range-example-modal').asRange('get'));
        $("#unranged-value2").val(result_Value);
        $("#yearly_bill1").html(result_Value_year);

        function slidermodal() {
            $(".range-example-modal").asRange({

                min: 5000,
                max: 500000,
                format: function (value) {
                    getValue(value);
                    return result_Value;
                },
                onChange: function (valueslider) {
                    var value = $('.range-example-modal').asRange('get');
//                    console.log("--value", value);

                    var a = document.getElementById("unranged-value2");

                    getValue(value);
                    var valueoftext = $("#unranged-value2").val();
                    valueoftext = valueoftext.replace(/\,/g, '');
                    handleRangeModal(valueoftext);

                    a.value = result_Value;
//                    console.log("at on change", $("#unranged-value2").val());
//                    console.log("result_Value_year", result_Value_year)
                    $("#yearly_bill1").html(result_Value_year);

                    if (value > 5000) {
//                        console.log("----------2--->", value);
                        $(".range-example-5").asRange('set', value);
                    }
                }
            });
        }

        //test 
        /* Capturing entered spends value in slider */
        $("#unranged-value1").keyup(function (event) {
            var valSpend = $("#unranged-value1").val();
            var valSpendNum = '';
            valSpendNum = valSpend.replace(/\,/g, '');
            valSpendNum = parseInt(valSpendNum, 10);
//        if (valSpendNum >= 0) {  //&& valSpendNum <= 1500000

            // skip for arrow keys
            if (event.which == 37 || event.which == 39)
                return;
            // format number
            $(this).val(function (index, value) {
//                            console.log("valSpendNum",valSpendNum,"-----",);
                if (valSpend < 5000 || valSpend > 1500000) {
                    $(".range-example-5").asRange('val', 5000);

                    getValue(5000);
                    $("#yearly_bill").html(result_Value_year);
//                                (valSpendNum > 5000) || (valSpendNum < 1500000)
//                               console.log("***********")
//                           $(".range-example-5").asRange('val', valSpendNum);
//                          
//                                getValue(valSpendNum);
//                                 $("#yearly_bill").html(result_Value_year);
                } else {
//                               console.log("^^^^^^^^^^^^")
                    $(".range-example-5").asRange('val', valSpendNum);
//                          
                    getValue(valSpendNum);
                    $("#yearly_bill").html(result_Value_year);
//                               $(".range-example-5").asRange('val', 5000);
//                          
//                                getValue(5000);
//                                 $("#yearly_bill").html(result_Value_year);
                }
                $("#unranged-value1").focus();

                var x = value.replace(/,/g, "");
                var lastThree = x.substring(x.length - 3);
                var otherNumbers = x.substring(0, x.length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                return res;

            });
//        }//if close
        });
//$("#unranged-value1").focusout(function(){
//    var value= $("#unranged-value1").val();
// 
//      value = value.replace(/\,/g, '');
//      
//    if(value < 5000){
//       
//        $("#unranged-value1").val("5,000");
//        getValue(5000);
//        console.log("result_Value_year",result_Value_year);
//        $("#yearly_bill").html(result_Value_year);
//          $(".range-example-5").asRange('val', 5000);
//    }
//});
//$("#unranged-value2").focusout(function(){
//    var value= $("#unranged-value2").val();
//      value = value.replace(/\,/g, '');
//    if(value < 5000){
//        $("#unranged-value2").val("5,000");
//          getValue(5000);
//           $("#yearly_bill1").html(result_Value_year);
//          $(".range-example-modal").asRange('val', 5000);
//    }
//});
//    function updateSlider(val)
//    {
//        $(".range-example-5").asRange('val', val);
//        return;
//    }
//     function updateSlidermodal(val)
//    {
//        $(".range-example-modal").asRange('val', val);
//        return;
//    }
        /* Capturing entered spends value in slider */

        /* Decimal and Alphabets are not allowing */
        $("#unranged-value1").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            // Allow: home, end, left, right, down, up
                                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || (e.keyCode == 110) || (e.keyCode == 190)) {
                        e.preventDefault();
                    }
                });

        //end
        $("#unranged-value2").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            // Allow: home, end, left, right, down, up
                                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || (e.keyCode == 110) || (e.keyCode == 190)) {
                        e.preventDefault();
                    }
                });
        $("#unranged-value2").keyup(function (event) {
            var valSpend = $("#unranged-value2").val();
            var valSpendNum = '';
            valSpendNum = valSpend.replace(/\,/g, '');
            valSpendNum = parseInt(valSpendNum, 10);
//        if (valSpendNum >= 0) {  //&& valSpendNum <= 1500000

            // skip for arrow keys
            if (event.which == 37 || event.which == 39)
                return;
            // format number
            $(this).val(function (index, value) {
                if (valSpend < 5000 || valSpend > 1500000) {
                    $(".range-example-modal").asRange('val', 5000);

                    getValue(5000);
                    $("#yearly_bill1").html(result_Value_year);
//                                (valSpendNum > 5000) || (valSpendNum < 1500000)
//                               console.log("***********")
//                           $(".range-example-5").asRange('val', valSpendNum);
//                          
//                                getValue(valSpendNum);
//                                 $("#yearly_bill").html(result_Value_year);
                } else {
//                               console.log("^^^^^^^^^^^^")
                    $(".range-example-modal").asRange('val', valSpendNum);
//                          
                    getValue(valSpendNum);
                    $("#yearly_bill1").html(result_Value_year);
//                               $(".range-example-5").asRange('val', 5000);
//                          
//                                getValue(5000);
//                                 $("#yearly_bill").html(result_Value_year);
                }
//                           $(".range-example-modal").asRange('val', valSpendNum);
                $("#unranged-value2").focus();
//                              if(valSpendNum != ""){
//                                getValue(valSpendNum);
//                                 $("#yearly_bill1").html(result_Value_year);
//                           }
                var x = value.replace(/,/g, "");
                var lastThree = x.substring(x.length - 3);
                var otherNumbers = x.substring(0, x.length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                return res;

            });
//        }//if close
        });

//        $("#unranged-value1").on('change', function () {
//            var rangedvalue = $("#unranged-value1").val();
////            test(rangedvalue);
//              console.log("rangedvalue-1->",rangedvalue.length);
////            if(rangedvalue.length > 7){
////             rangedvalue=rangedvalue.substring(0,7);
////                console.log("rangedvalue-->",rangedvalue.length);
////            }
////             rangedvalue = rangedvalue.replace(/\,/g, '');
//            
//              
//            if(rangedvalue > 500000){
//                console.log("1")
//                temp=rangedvalue;
//                 $(".range-example-5").asRange('set', rangedvalue);
//            }else{
//                temp=0;
//                temp2=rangedvalue;
//                 console.log("2")
//                $(".range-example-5").asRange('set', rangedvalue);
//            }
////            $(".range-example-5").asRange('set', rangedvalue);
//        });
//        $("#unranged-value2").on('change', function () {
//            
//            var rangedvalue = $("#unranged-value2").val();
//             rangedvalue = rangedvalue.replace(/\,/g, '');
//             if(rangedvalue > 500000){
//                console.log("3")
//                temp3=rangedvalue;
//                 $(".range-example-modal").asRange('set', rangedvalue);
//            }else{
//                temp3=0;
//                temp4=rangedvalue;
//                 console.log("4")
//                 $(".range-example-modal").asRange('set', rangedvalue);
//            }
//           
//        });
        $('#unranged-value1, #unranged-value2').on("keyup", function (evt) {

            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;

            } else
                return true;
        });

        function getValue(param) {
            var yearly = (param * 12).toString();
            var monthly = param.toString();
            var lastThree = monthly.substring((monthly.length) - 3);
            var otherNumbers = monthly.substring(0, monthly.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            result_Value = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            lastThree = yearly.substring((yearly.length) - 3);
            otherNumbers = yearly.substring(0, yearly.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            result_Value_year = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        }
//        //end milestone 2
        $('#date_of_birth').keydown(function (e) {

            e.preventDefault();
            return false;
        });
        var session_validate;
        var random = '${randomNumber}';
        var y = $('#filterareadropdown option:selected').val();
        var c = $("#filterareadropdownhide option:selected").val();
        console.log("y>>>"+y);
        console.log("c>>>"+c);
        if (y == "Personal Credit Card" && c == "India") {
            $("#CrdHP_choose-own-card").show();
            $("#CrdHP_rec-me-a-card").show();

        } else {
            $("#CrdHP_choose-own-card").hide();
            $("#CrdHP_rec-me-a-card").hide();
        }

        var AmexAppStatus = '${AmexAppStatus}';
        var jp_num_ppp = '${sessionScope.loggedInUser}';
        console.log("jp_num_ppp====>", jp_num_ppp);
        var cardName = $('#filterareadropdown :selected').text();
        var countryName = $('#filterareadropdownhide :selected').text();

//        console.log("cardName>>>"+cardName);
//        console.log("countryName>>>"+countryName);
        if (cardName.indexOf("Personal Credit Card") > -1 && countryName.indexOf("India") > -1) {

            if (jp_num_ppp != '' && AmexAppStatus == "Pending") {
                if (check() == true) {
                    $('#notification').modal('hide');
                } else {
                    var value = readCookie('laterStatus');
                    if (value == 1) {
                        $('#notification').modal('hide');
                    } else {
//                        $('#notification').modal('show');
                            $('#notification').modal('hide');
                    }
                }

                function readCookie(name) {
                    var nameEQ = name + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ')
                            c = c.substring(1, c.length);
                        if (c.indexOf(nameEQ) == 0)
                            return c.substring(nameEQ.length, c.length);
                    }
                    return null;
                }

            } else {
                var laterId = "laterStatus";
                var laterIds = '0';
                document.cookie = laterId + "=" + laterIds + ";" + 60000 + ";path=/";
                $('#notification').modal('hide');
            }

            $('.later').on("click", function () {
                console.log("click");
                var laterId = "laterStatus";
                var laterIds = '1';
                session_validate = '${sessionScope.loggedInUser}';
                console.log("session_validate===>", session_validate)
                if (jp_num_ppp === session_validate) {

                    document.cookie = laterId + "=" + laterIds + ";" + 60000 + ";path=/";
                    $('#notification').modal('hide');
                } else {

//                    $('#notification').modal('show');
                        $('#notification').modal('hide');
                }
            });


        } else {

            $('#notification').modal('hide');
        }
        var tierExist = false;
        //code by aravind
//         var jp_num_ppp= '$ {sessionScope.loggedInUser}';
        var jp_num_ppp = loggedInUser;

        if (jp_num_ppp != "") {
//             console.log("jp_num_ppp  :"+jp_num_ppp);
            $('#jpNumber').val(jp_num_ppp);
            tierExist = true;
        }
        var checkupgrade = false;
        if ('${recPage}' != "" && '${recPage}' == 1) {
            $('#userDetails').modal('show');
        }

        function check() {
            if (window.performance && window.performance.navigation.type == window.performance.navigation.TYPE_BACK_FORWARD) {

                return true;
            }
        }
        ;

        history.pushState(null, null, '');
        window.addEventListener('popstate', function () {
            history.pushState(null, null, '');
        });


    <c:forEach items="${cardList}" var="listItem">
        <c:choose>
            <c:when test="${listItem.country eq 3 || country =='Bangladesh' || countryset =='Bangladesh'}"  >

        $("#feesId" + "<c:out value="${listItem.cpNo}" />").html("BDT " + formatNumber("<c:out value="${listItem.fees}" />"));
            </c:when>
            <c:otherwise>

        $("#feesId" + "<c:out value="${listItem.cpNo}" />").html("INR " + formatNumber("<c:out value="${listItem.fees}" />"));
            </c:otherwise>
        </c:choose>

    </c:forEach>

        function formatNumber(x) {
            x = x.toString();
            var afterPoint = '';
            if (x.indexOf('.') > 0)
                afterPoint = x.substring(x.indexOf('.'), x.length);
            if (afterPoint == 0)
                afterPoint = '';
            x = Math.floor(x);
            x = x.toString();
            var lastThree = x.substring(x.length - 3);
            var otherNumbers = x.substring(0, x.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
            return res;

        }

        var arr = [];
        var checkedBankId = [];
        var bankid = [];
        var lyfId = [];
        var checkedlyfId = [];
        var mob_checkedBankId = [];
        var mob_bankid = [];
        var mob_lyfId = [];
        var mob_checkedlyfId = [];
        var fees_range = 10000;
        var islyfunchecked = false;

        //filter code start here


        var countryFilterOnJSP = '${countryFilter}';
        var cardFilterOnJSP = '${cardFilter}';

        if (countryFilterOnJSP != '' && cardFilterOnJSP != '') {


            var cardtype;

            if (cardFilterOnJSP == "1") {
                cardtype = "Personal Credit Card";
            } else if (cardFilterOnJSP == "2") {
                cardtype = "Personal Debit Card";
            } else if (cardFilterOnJSP == "3") {
                cardtype = "Corporate Card"
            } else if (cardFilterOnJSP == "4") {
                cardtype = "Others";
            }else if(cardFilterOnJSP == "5"){
                cardtype = "All";
                
            }

            var bankfilter = '${bankFilter}';
            var lsBenefits = '${lsBenefitsFilter}';
            var joiningTest = '${joiningFees}';


            if ((cardtype.indexOf("Debit") > -1) || (cardtype.indexOf("Corporate") > -1) || (cardtype.indexOf("Others") > -1)) {
                var url;

                if (bankfilter != '' && lsBenefits != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;

                } else if (bankfilter != '' && lsBenefits != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits;


                } else if (bankfilter != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&joiningFees=" + joiningTest;


                } else if (lsBenefits != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;


                } else if (bankfilter != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter;


                } else if (lsBenefits != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&lsBenefits=" + lsBenefits;


                } else if (joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&joiningFees=" + joiningTest;


                } else {
                    url = "${applicationURL}testing?categoryOption=" + cardtype;


                }



                $.get(url, function (data, status) {
                    if ('${utm_source}' != "") {
//                                                         alert("1st utm_source"+'${utm_source}'+"utm_medium"+'${utm_medium}'+"utm_campaign"+'${utm_campaign}');
                        location.href = '${applicationURL}getcategory?countryOption=' + countryFilterOnJSP + '&cardtype=' + cardtype + "&joiningFees=" + joiningTest + "&lsBenefits=" + lsBenefits + "&bank=" + bankfilter + "&utm_source=" + '${utm_source}' + "&utm_medium=" + '${utm_medium}' + "&utm_campaign=" + '${utm_campaign}';
                    } else {
//                                               alert("1st===utm source>"+'${utm_source}'+"---utm_medium"+'${utm_medium}'+"----utm_campaign"+'${utm_campaign}');
//                                             location.href = '${applicationURL}getcategory?countryOption='+countryFilterOnJSP;
                        location.href = '${applicationURL}getcategory?countryOption=' + countryFilterOnJSP + '&cardtype=' + cardtype + "&joiningFees=" + joiningTest + "&lsBenefits=" + lsBenefits + "&bank=" + bankfilter;
                    }

                });

            } else {


                var url;

                if (bankfilter != '' && lsBenefits != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;
                } else if (bankfilter != '' && lsBenefits != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits;
                } else if (bankfilter != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&joiningFees=" + joiningTest;
                } else if (lsBenefits != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;
                } else if (bankfilter != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter;
                } else if (lsBenefits != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&lsBenefits=" + lsBenefits;
                } else if (joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&joiningFees=" + joiningTest;
                } else {
                    url = "${applicationURL}testing?categoryOption=" + cardtype;
                }

                $.get(url, function (data, status) {

                    if ('${utm_source}' != "") {
//                                                         alert("2 utm_source"+'${utm_source}'+"utm_medium"+'${utm_medium}'+"utm_campaign"+'${utm_campaign}');
                        location.href = '${applicationURL}getcategory?countryOption=' + countryFilterOnJSP + '&cardtype=' + cardtype + "&joiningFees=" + joiningTest + "&lsBenefits=" + lsBenefits + "&bank=" + bankfilter + "&utm_source=" + '${utm_source}' + "&utm_medium=" + '${utm_medium}' + "&utm_campaign=" + '${utm_campaign}';
                    } else {
//                                                        alert("2nd===utm source>"+'${utm_source}'+"---utm_medium"+'${utm_medium}'+"----utm_campaign"+'${utm_campaign}');
//                                             location.href = '${applicationURL}getcategory?countryOption='+countryFilterOnJSP;
                        location.href = '${applicationURL}getcategory?countryOption=' + countryFilterOnJSP + '&cardtype=' + cardtype + "&joiningFees=" + joiningTest + "&lsBenefits=" + lsBenefits + "&bank=" + bankfilter;
                    }


                });
                $("#filterareadropdownhide").show();
            }


        } else if (cardFilterOnJSP != '') {


            var cardtype;

            if (cardFilterOnJSP == "1") {
                cardtype = "Personal Credit Card";

            } else if (cardFilterOnJSP == "2") {

                cardtype = "Personal Debit Card";
            } else if (cardFilterOnJSP == "3") {

                cardtype = "Corporate Card"
            } else if (cardFilterOnJSP == "4") {

                cardtype = "Others";
            }

            var bankfilter = '${bankFilter}';
            var lsBenefits = '${lsBenefitsFilter}';
            var joiningTest = '${joiningFees}';
            var countryFilter = $('#filterareadropdownhide option:selected').val();
            var country;

            if (countryFilter == "India") {
                country = "1";
            } else if (countryFilter == "Bangladesh") {
                country = "3";
            }

            if ((cardtype.indexOf("Debit") > -1) || (cardtype.indexOf("Corporate") > -1) || (cardtype.indexOf("Others") > -1) ||(cardtype.indexOf("All") > -1) ) {



                var url;

                if (bankfilter != '' && lsBenefits != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;
                } else if (bankfilter != '' && lsBenefits != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits;
                } else if (bankfilter != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&joiningFees=" + joiningTest;
                } else if (lsBenefits != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;
                } else if ('${bankFilter}' != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter;
                } else if (lsBenefits != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&lsBenefits=" + lsBenefits;
                } else if (joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&joiningFees=" + joiningTest;
                } else {
                    url = "${applicationURL}testing?categoryOption=" + cardtype;
                }

                $.get(url, function (data, status) {

                    if ('${utm_source}' != "") {
//                                                         alert("3 utm_source"+'${utm_source}'+"utm_medium"+'${utm_medium}'+"utm_campaign"+'${utm_campaign}');
                        location.href = '${applicationURL}getcategory?countryOption=' + country + '&cardtype=' + cardtype + "&joiningFees=" + joiningTest + "&lsBenefits=" + lsBenefits + "&bank=" + bankfilter + "&utm_source=" + '${utm_source}' + "&utm_medium=" + '${utm_medium}' + "&utm_campaign=" + '${utm_campaign}';
                    } else {
//                                                        alert("3rd===utm source>"+'${utm_source}'+"---utm_medium"+'${utm_medium}'+"----utm_campaign"+'${utm_campaign}');
//                                             location.href = '${applicationURL}getcategory?countryOption='+country;
                        location.href = '${applicationURL}getcategory?countryOption=' + country + '&cardtype=' + cardtype + "&joiningFees=" + joiningTest + "&lsBenefits=" + lsBenefits + "&bank=" + bankfilter;
                    }


                });

            } else {

                var url;

                if (bankfilter != '' && lsBenefits != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;
                } else if (bankfilter != '' && lsBenefits != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits;
                } else if (bankfilter != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter + "&joiningFees=" + joiningTest;
                } else if (lsBenefits != '' && joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;
                } else if (bankfilter != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&bank=" + bankfilter;
                } else if (lsBenefits != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&lsBenefits=" + lsBenefits;
                } else if (joiningTest != '') {
                    url = "${applicationURL}testing?categoryOption=" + cardtype + "&joiningFees=" + joiningTest;
                } else {
                    url = "${applicationURL}testing?categoryOption=" + cardtype;
                }

                $.get(url, function (data, status) {
                    if ('${utm_source}' != "") {
//                                                        alert("4 utm_source"+'${utm_source}'+"utm_medium"+'${utm_medium}'+"utm_campaign"+'${utm_campaign}');
                        location.href = '${applicationURL}getcategory?countryOption=' + country + '&cardtype=' + cardtype + "&joiningFees=" + joiningTest + "&lsBenefits=" + lsBenefits + "&bank=" + bankfilter + "&utm_source=" + '${utm_source}' + "&utm_medium=" + '${utm_medium}' + "&utm_campaign=" + '${utm_campaign}';
                    } else {
//                                                        alert("4th===utm source>"+'${utm_source}'+"---utm_medium"+'${utm_medium}'+"----utm_campaign"+'${utm_campaign}');
//                                               location.href = '${applicationURL}getcategory?countryOption='+country;
                        location.href = '${applicationURL}getcategory?countryOption=' + country + '&cardtype=' + cardtype + "&joiningFees=" + joiningTest + "&lsBenefits=" + lsBenefits + "&bank=" + bankfilter;
                    }


                });
                $("#filterareadropdownhide").show();
            }

        } else if (countryFilterOnJSP != '') {



            var country;

            if (countryFilterOnJSP == '3') {
                country = "Bangladesh";
            } else if (countryFilterOnJSP == '1') {
                country = "India";
            } else {
                country = "India";
            }

            var cardtype = $('#filterareadropdown option:selected').val();

            var prev = $(this).data(country);
            var bankfilter = '${bankFilter}';
            var lsBenefits = '${lsBenefitsFilter}';
            var joiningTest = '${joiningFees}';
            var url;
            if (bankfilter != '' && lsBenefits != '' && joiningTest != '') {
                url = "${applicationURL}getcountry?countryOption=" + country + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;

            } else if (bankfilter != '' && lsBenefits != '') {
                url = "${applicationURL}getcountry?countryOption=" + country + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits;


            } else if (bankfilter != '' && joiningTest != '') {
                url = "${applicationURL}getcountry?countryOption=" + country + "&bank=" + bankfilter + "&joiningFees=" + joiningTest;


            } else if (lsBenefits != '' && joiningTest != '') {
                url = "${applicationURL}getcountry?countryOption=" + country + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;


            } else if (joiningTest != '') {
                url = "${applicationURL}getcountry?countryOption=" + country + "&joiningFees=" + joiningTest;


            } else if (lsBenefits != '') {
                url = "${applicationURL}getcountry?countryOption=" + country + "&lsBenefits=" + lsBenefits;


            } else if (bankfilter != '') {

                url = "${applicationURL}getcountry?countryOption=" + country + "&bank=" + bankfilter;

            } else {
                url = "${applicationURL}getcountry?countryOption=" + country;


            }



            var urlHome;
            if (bankfilter != '' && lsBenefits != '' && joiningTest != '') {
                urlHome = "${applicationURL}?countryOption=" + countryFilterOnJSP + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;


            } else if (bankfilter != '' && lsBenefits != '') {
                urlHome = "${applicationURL}?countryOption=" + countryFilterOnJSP + "&bank=" + bankfilter + "&lsBenefits=" + lsBenefits;


            } else if (bankfilter != '' && joiningTest != '') {
                urlHome = "${applicationURL}?countryOption=" + countryFilterOnJSP + "&bank=" + bankfilter + "&joiningFees=" + joiningTest;


            } else if (lsBenefits != '' && joiningTest != '') {
                urlHome = "${applicationURL}?countryOption=" + countryFilterOnJSP + "&lsBenefits=" + lsBenefits + "&joiningFees=" + joiningTest;


            } else if (joiningTest != '') {
                urlHome = "${applicationURL}?countryOption=" + countryFilterOnJSP + "&joiningFees=" + joiningTest;


            } else if (lsBenefits != '') {
                urlHome = "${applicationURL}?countryOption=" + countryFilterOnJSP + "&lsBenefits=" + lsBenefits;


            } else if (bankfilter != '') {

                urlHome = "${applicationURL}?countryOption=" + countryFilterOnJSP + "&bank=" + bankfilter;

            } else {
                urlHome = "${applicationURL}?countryOption=" + countryFilterOnJSP;


            }


            $.get(url, function (data, status) {
                if ('${utm_source}' != "") {
//                                                         alert("5 utm_source"+'${utm_source}'+"utm_medium"+'${utm_medium}'+"utm_campaign"+'${utm_campaign}');
                    location.href = urlHome + "&utm_source=" + '${utm_source}' + "&utm_medium=" + '${utm_medium}' + "&utm_campaign=" + '${utm_campaign}';
                } else {
//                                                        alert("5======utm_source"+'${utm_source}'+"utm_medium"+'${utm_medium}'+"utm_campaign"+'${utm_campaign}')
                    location.href = urlHome
                }

            });

        }

        // filter code end here




        if (($('.bankNum').is(':checked') == true || $('.lsbpNum').is(':checked') == true || '${joiningFeeset}' != '')) {

        	console.log(".bankNum clicked");
            if ($('.bankNum').is(':checked') == true) {
                checkedBankId = [];
                $(".bankNum").each(function () {
                    if ($(this).is(':checked')) {
                        checkedBankId.push($(this).val());

                    } else {

                        checkedBankId.remove($(this).val());
                    }
                });
                if (checkedBankId.length == 0) {
                    checkedBankId = bankid;

                }

                $("div.features_desc").each(function () {
                    console.log(".features_desc called");
                    var bpId = $(this).find('#bpNo').val();
                    var cpNo = $(this).find('#cpNo').val();
                    var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                    var lsbpList = $(this).find('#lspbList').val();

                    if ('${lsBenefitsFilter}' == "") {


                        $(".lsbpNum").each(function () {
                            lyfId.push($(this).val());
                            checkedlyfId.push($(this).val());


                        });
                        $(".mob_lsbpNum").each(function () {
                            mob_lyfId.push($(this).val());
                            mob_checkedlyfId.push($(this).val());
                        });
                    }
                    if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {


                        var flag = false;
                        $.each(checkedlyfId, function (k, v) {


                            if ($.inArray(v, lsbpList) != -1) {


                                flag = true;
                            }
                        });
                        if (flag) {


                            $('#card_row' + cpNo).show();
                        } else {


                            $('#card_row' + cpNo).hide();
                        }
                    } else {
//                                                                                             

                        $('#card_row' + cpNo).hide();
                    }
                });

                if ($('div.features_desc:visible').length > 0) {
                    $('.no_cards').addClass('hidden');
                } else {
                    $('.no_cards').removeClass('hidden');
                }

                $("#clear_fliter").on('click', function () {

                    console.log("Inside clear fliter");
                     $('.life_benefits_holder li input:checked').next().children().toggle();


                    $('.bankNum').attr('checked', false);
                    $('.lsbpNum').attr('checked', false);
                    $('.no_cards').addClass('hidden');


                    checkedBankId = [];
                    bankid = [];
                    lyfId = [];
                    checkedlyfId = [];
                    arr = [];
                    fees_range = 10000;
                    joiningFees = 500;

                    $(".bankNum").each(function () {
                        bankid.push($(this).val());
                        checkedBankId.push($(this).val());
                    });
                    $(".lsbpNum").each(function () {
                        lyfId.push($(this).val());
                        checkedlyfId.push($(this).val());
                    });

    <c:forEach items="${cardList}" var="listItem">
                    arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
                    $.each(arr, function (i, val) {
                        $('#card_row' + val).show();
                    });
                    var $divs = $("div.features");
                    var ascSort = $divs.sort(function (a, b) {
//				  return $(a).find(".cpNo_sort").val() - $(b).find(".cpNo_sort").val();
                        return $(a).find(".cpDpNo_sort").val() - $(b).find(".cpDpNo_sort").val();
                    });
                    $("#sort_list_div").html(ascSort);
                    window.location = '${applicationURL}';
                });
            }

            if ($('.lsbpNum').is(':checked') == true) {
                checkedlyfId = [];
                islyfunchecked = false;
                $(".lsbpNum").each(function () {
                    if ($(this).is(':checked')) {
                        checkedlyfId.push($(this).val());
                    } else {
                        checkedlyfId.remove($(this).val());
                    }
                });
                if (checkedlyfId.length == 0) {
                    checkedlyfId = lyfId;
                    islyfunchecked = true;
                }

                $("div.features_desc").each(function () {
                    var bpId = $(this).find('#bpNo').val();

                    var cpNo = $(this).find('#cpNo').val();
                    var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                    var lsbpList = $(this).find('#lspbList').val();
                    if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {
                        var flag = false;
                        $.each(checkedlyfId, function (k, v) {
                            if ($.inArray(v, lsbpList) != -1) {
                                flag = true;
                            }
                        });
                        if (flag) {
                            $('#card_row' + cpNo).show();
                        } else {
                            $('#card_row' + cpNo).hide();
                        }
                    } else {
                        $('#card_row' + cpNo).hide();
                    }
                });
                console.log("islyfunchecked>>>"+islyfunchecked);
                if (!islyfunchecked) {
                    if ($('.ff').html() == 'XX') {
                        var $divs = $("div.features");
                        var ascSort = $divs.sort(function (a, b) {
                            var countA = 0;
                            var countB = 0;
                            if (checkedlyfId.length == 1) {
                                var bankA = $(a).find(".bankName_sort").val();
                                var bankB = $(b).find(".bankName_sort").val();
                                if (bankA > bankB)
                                    return 1
                                if (bankA < bankB)
                                    return -1
                            } else {
                                var lsbpListA = $(a).find('#lspbList').val();
                                var lsbpListB = $(b).find('#lspbList').val();
                                $.each(checkedlyfId, function (k, v) {
                                    if ($.inArray(v, lsbpListA) != -1) {
                                        countA++;
                                    }
                                });
                                $.each(checkedlyfId, function (k, v) {
                                    if ($.inArray(v, lsbpListB) != -1) {
                                        countB++;
                                    }
                                });
                                if (countA > countB)
                                    return -1
                                if (countA < countB)
                                    return 1
                                else {
                                    var bankA = $(a).find(".bankName_sort").val();
                                    var bankB = $(b).find(".bankName_sort").val();
                                    if (bankA > bankB)
                                        return 1
                                    if (bankA < bankB)
                                        return -1
                                }
                            }
                        });
                        $("#sort_list_div").html(ascSort);
                    } else {
                        var $divs = $("div.features");
                        var sort_cards = $divs.sort(function (a, b) {
                            var ffA = parseInt($(a).find(".ff").html());
                            var ffB = parseInt($(b).find(".ff").html());
                            if (ffA > ffB)
                                return -1
                            if (ffA < ffB)
                                return 1
                            else {
                                var ggA = $(a).find(".gg").html();
                                var ggB = $(b).find(".gg").html();
                                var res = [];
                                res = ggA.split(' ');
                                ggA = res[2].replace(/<br ?\/?>/g, "");
                                ggA = parseInt(ggA);
                                res = ggB.split(' ');
                                ggB = res[2].replace(/<br ?\/?>/g, "");
                                ggB = parseInt(ggB);
                                if (ggA > ggB)
                                    return -1
                                if (ggA < ggB)
                                    return 1
                                else {
                                    var countA = 0;
                                    var countB = 0;
                                    if (checkedlyfId.length == 1) {
                                        var bankA = $(a).find(".bankName_sort").val();
                                        var bankB = $(b).find(".bankName_sort").val();
                                        if (bankA > bankB)
                                            return 1
                                        if (bankA < bankB)
                                            return -1
                                    } else {
                                        var lsbpListA = $(a).find('#lspbList').val();
                                        var lsbpListB = $(b).find('#lspbList').val();
                                        $.each(checkedlyfId, function (k, v) {
                                            if ($.inArray(v, lsbpListA) != -1) {
                                                countA++;
                                            }
                                        });
                                        $.each(checkedlyfId, function (k, v) {
                                            if ($.inArray(v, lsbpListB) != -1) {
                                                countB++;
                                            }
                                        });
                                        if (countA > countB)
                                            return -1
                                        if (countA < countB)
                                            return 1
                                        else {
                                            var bankA = $(a).find(".bankName_sort").val();
                                            var bankB = $(b).find(".bankName_sort").val();
                                            if (bankA > bankB)
                                                return 1
                                            if (bankA < bankB)
                                                return -1
                                        }
                                    }
                                }
                            }

                        });
                        $("#sort_list_div").html(sort_cards);
                    }
                } else {
                    var $divs = $("div.features");
                    var ascSort = $divs.sort(function (a, b) {
                        return $(a).find(".cpNo_sort").val() - $(b).find(".cpNo_sort").val();
                    });
                    $("#sort_list_div").html(ascSort);
                }
                if ($('div.features_desc:visible').length > 0)
                    $('.no_cards').addClass('hidden');
                else
                    $('.no_cards').removeClass('hidden');


                $("#clear_fliter").on('click', function () {

					console.log("Inside second clear fliter")
                    $('.life_benefits_holder li input:checked').next().children().toggle();


                    $('.bankNum').attr('checked', false);
                    $('.lsbpNum').attr('checked', false);
                    $('.no_cards').addClass('hidden');


                    checkedBankId = [];
                    bankid = [];
                    lyfId = [];
                    checkedlyfId = [];
                    arr = [];
                    fees_range = 10000;
                    joiningFees = 500;

                    $(".bankNum").each(function () {
                        bankid.push($(this).val());
                        checkedBankId.push($(this).val());
                    });
                    $(".lsbpNum").each(function () {
                        lyfId.push($(this).val());
                        checkedlyfId.push($(this).val());
                    });

    <c:forEach items="${cardList}" var="listItem">
                    arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
                    $.each(arr, function (i, val) {
                        $('#card_row' + val).show();
                    });
                    var $divs = $("div.features");
                    var ascSort = $divs.sort(function (a, b) {
//				  return $(a).find(".cpNo_sort").val() - $(b).find(".cpNo_sort").val();
                        return $(a).find(".cpDpNo_sort").val() - $(b).find(".cpDpNo_sort").val();
                    });
                    $("#sort_list_div").html(ascSort);
                    window.location = '${applicationURL}';
                });
            }

            if ('${joiningFeeset}' != '') {


                joiningFees = '${joiningFeeset}';
                var cpNo = $(this).find('#cpNo').val();
                var fees1 = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                var fees_range = $(".slider_div .rangeslider__handle").html().replace(/<\/?span[^>]*>/g, "");
                fees_range = joiningFees.replace(/\,/g, '');
                checkedlyfId = [];
                checkedBankId = [];
                islyfunchecked = false;
                $(".bankNum").each(function () {
//				bankid.push($(this).val());
                    checkedBankId.push($(this).val());
                });
                $(".lsbpNum").each(function () {
//	   			lyfId.push($(this).val());
                    checkedlyfId.push($(this).val());
                });
                $("div.features_desc").each(function () {
                    var cpNo = $(this).find('#cpNo').val();
                    var bpId = $(this).find('#bpNo').val();
                    var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');

                    fees_range = '${joiningFeeset}';
                    var lsbpList = $(this).find('#lspbList').val();


                    if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {

                        var flag = false;

                        $.each(checkedlyfId, function (k, v) {

                            if ($.inArray(v, lsbpList) != -1) {

                                flag = true;
                            }
                        });
                        if (flag) {


                            $('#card_row' + cpNo).show();
                        } else {

                            $('#card_row' + cpNo).hide();
                        }
                    } else {

                        $('#card_row' + cpNo).hide();
                    }

                });



                $(this).parents('.parent_div').addClass('hidden');



                if ($('div.features_desc:visible').length > 0)
                    $('.no_cards').addClass('hidden');
                else
                    $('.no_cards').removeClass('hidden');
//    			e.stopImmediatePropagation();


                $("#clear_fliter").on('click', function () {

					console.log("Inside third clear_fliter");	
                    $('.life_benefits_holder li input:checked').next().children().toggle();


                    $('.bankNum').attr('checked', false);
                    $('.lsbpNum').attr('checked', false);
                    $('.no_cards').addClass('hidden');


                    checkedBankId = [];
                    bankid = [];
                    lyfId = [];
                    checkedlyfId = [];
                    arr = [];
                    fees_range = 10000;
                    joiningFees = 500;

                    $(".bankNum").each(function () {
                        bankid.push($(this).val());
                        checkedBankId.push($(this).val());
                    });
                    $(".lsbpNum").each(function () {
                        lyfId.push($(this).val());
                        checkedlyfId.push($(this).val());
                    });

    <c:forEach items="${cardList}" var="listItem">
                    arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
                    $.each(arr, function (i, val) {
                        $('#card_row' + val).show();
                    });
                    var $divs = $("div.features");
                    var ascSort = $divs.sort(function (a, b) {
//				  return $(a).find(".cpNo_sort").val() - $(b).find(".cpNo_sort").val();
                        return $(a).find(".cpDpNo_sort").val() - $(b).find(".cpDpNo_sort").val();
                    });
                    $("#sort_list_div").html(ascSort);
                    window.location = '${applicationURL}';
                });
            }


        }

        /*Web Filter functionality strats*/

        $("#filterareadropdownhide").on("change", function () {
            country = $('#filterareadropdownhide option:selected').val();

            var cardtype = $('#filterareadropdown option:selected').val();
            console.log("cardtype>>"+cardtype);
            var prev = $(this).data(country);

            $.get("${applicationURL}getcountry?countryOption=" + country + "&cardtype=" + cardtype, function (data, status) {
                if (status == "success") {
                     console.log("inside success hide");
                    location.href = '${applicationURL}?country=' + country + "&cardtype=" + cardtype;


                }

            });

        });

        $("#filterareadropdown").on("change", function () {
            var cardtype = $('#filterareadropdown option:selected').val();
            var country = $('#filterareadropdownhide option:selected').val();
            console.log("cardtype drop down>>"+cardtype);

            if ((cardtype.indexOf("Debit") > -1) || (cardtype.indexOf("Corporate") > -1) || (cardtype.indexOf("Others") > -1) || (cardtype.indexOf("All") > -1)) {


                $.get("${applicationURL}testing?categoryOption=" + cardtype, function (data, status) {

                    if (status == "success") {
//                                                    alert('test1');
                        console.log("inside fropdown success");
                        location.href = '${applicationURL}getcategory?cardtype=' + cardtype + '&' + 'countryOption=' + country;


                    }

                });

            } else {

                $.get("${applicationURL}testing?categoryOption=" + cardtype, function (data, status) {
                    if (status == "success") {
//                                                  alert('test2');
//                                             location.href = '${applicationURL}getcategory?cardtype='+cardtype+'&'+'countryOption='+country;
                        console.log("inside else success");
                        location.href = '${applicationURL}';

                    }

                });
                $("#filterareadropdownhide").show();
            }


        });
//                end 



        $(".bankNum").on("change", function () {
            checkedBankId = [];
            $(".bankNum").each(function () {
                if ($(this).is(':checked')) {
                    checkedBankId.push($(this).val());
                    //Adobe code starts here
                    if (!isInArray(bankNamesAdobe, $(this).parent().find('span:first').text()))
                    {
                        bankNamesAdobe.push($(this).parent().find('span:first').text())
                    }
                    //Adobe code ends here
                } else {
                    checkedBankId.remove($(this).val());
                    //Adobe code starts here
                    bankNamesAdobe.remove($(this).parent().find('span:first').text())
                    //Adobe code ends here
                }
            });

            //Adobe code starts here   
            submitFilterAdobe();
            //Adobe code ends here

            if (checkedBankId.length == 0) {
                checkedBankId = bankid;
            }

            $("div.features_desc").each(function () {
                var bpId = $(this).find('#bpNo').val();
                var cpNo = $(this).find('#cpNo').val();
                var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                var lsbpList = $(this).find('#lspbList').val();
                if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {
                    var flag = false;
                    $.each(checkedlyfId, function (k, v) {
                        if ($.inArray(v, lsbpList) != -1) {
                            flag = true;
                        }
                    });
                    if (flag) {
                        $('#card_row' + cpNo).show();
                    } else {
                        $('#card_row' + cpNo).hide();
                    }
                } else {
                    $('#card_row' + cpNo).hide();
                }
            });

            if ($('div.features_desc:visible').length > 0) {
                $('.no_cards').addClass('hidden');
            } else {
                $('.no_cards').removeClass('hidden');
            }
        });

        $("#filter_jf").on('click', function (e) {
            $("div.features_desc").each(function () {
                var cpNo = $(this).find('#cpNo').val();
                var bpId = $(this).find('#bpNo').val();
                var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                fees_range = $(".slider_div .rangeslider__handle").html().replace(/<\/?span[^>]*>/g, "");
                fees_range = fees_range.replace(/\,/g, '');
                var lsbpList = $(this).find('#lspbList').val();
                if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {

                    var flag = false;
                    $.each(checkedlyfId, function (k, v) {
                        if ($.inArray(v, lsbpList) != -1) {
                            flag = true;
                        }
                    });
                    if (flag) {
                        $('#card_row' + cpNo).show();
                    } else {
                        $('#card_row' + cpNo).hide();
                    }
                } else {
                    $('#card_row' + cpNo).hide();
                }

            });
            $(this).parents('.parent_div').addClass('hidden');
            if ($('div.features_desc:visible').length > 0)
                $('.no_cards').addClass('hidden');
            else
                $('.no_cards').removeClass('hidden');
            e.stopImmediatePropagation();

        });

        $(".lsbpNum").on("change", function () {
            checkedlyfId = [];
            islyfunchecked = false;
            $(".lsbpNum").each(function () {
                if ($(this).is(':checked')) {
                    checkedlyfId.push($(this).val());
                    //Adobe code starts here
                    if (!isInArray(lifestyleBenefitsAdobe, $(this).parent().find('span:first').text()))
                    {
                        lifestyleBenefitsAdobe.push($(this).parent().find('span:first').text());
                    }
                    //Adobe code ends here
                } else {
                    checkedlyfId.remove($(this).val());
                    //Adobe code starts here
                    lifestyleBenefitsAdobe.remove($(this).parent().find('span:first').text());
                    //Adobe code ends here
                }
            });

            //Adobe code starts here
            if (lifestyleBenefitsAdobe.length != 0)
            {
                submitFilterAdobe();
            }
            //Adobe code ends here

            if (checkedlyfId.length == 0) {
                checkedlyfId = lyfId;
                islyfunchecked = true;
            }

            $("div.features_desc").each(function () {
                var bpId = $(this).find('#bpNo').val();

                var cpNo = $(this).find('#cpNo').val();
                var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                var lsbpList = $(this).find('#lspbList').val();
                if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {
                    var flag = false;
                    $.each(checkedlyfId, function (k, v) {
                        if ($.inArray(v, lsbpList) != -1) {
                            flag = true;
                        }
                    });
                    if (flag) {
                        $('#card_row' + cpNo).show();
                    } else {
                        $('#card_row' + cpNo).hide();
                    }
                } else {
                    $('#card_row' + cpNo).hide();
                }
            });
            if (!islyfunchecked) {
                if ($('.ff').html() == 'XX') {
                    var $divs = $("div.features");
                    var ascSort = $divs.sort(function (a, b) {
                        var countA = 0;
                        var countB = 0;
                        if (checkedlyfId.length == 1) {
                            var bankA = $(a).find(".bankName_sort").val();
                            var bankB = $(b).find(".bankName_sort").val();
                            if (bankA > bankB)
                                return 1
                            if (bankA < bankB)
                                return -1
                        } else {
                            var lsbpListA = $(a).find('#lspbList').val();
                            var lsbpListB = $(b).find('#lspbList').val();
                            $.each(checkedlyfId, function (k, v) {
                                if ($.inArray(v, lsbpListA) != -1) {
                                    countA++;
                                }
                            });
                            $.each(checkedlyfId, function (k, v) {
                                if ($.inArray(v, lsbpListB) != -1) {
                                    countB++;
                                }
                            });
                            if (countA > countB)
                                return -1
                            if (countA < countB)
                                return 1
                            else {
                                var bankA = $(a).find(".bankName_sort").val();
                                var bankB = $(b).find(".bankName_sort").val();
                                if (bankA > bankB)
                                    return 1
                                if (bankA < bankB)
                                    return -1
                            }
                        }
                    });
                    $("#sort_list_div").html(ascSort);
                } else {
                    var $divs = $("div.features");
                    var sort_cards = $divs.sort(function (a, b) {
                        var ffA = parseInt($(a).find(".ff").html());
                        var ffB = parseInt($(b).find(".ff").html());
                        if (ffA > ffB)
                            return -1
                        if (ffA < ffB)
                            return 1
                        else {
                            var ggA = $(a).find(".gg").html();
                            var ggB = $(b).find(".gg").html();
//                                                                         console.log("ggA",ggA);
                            var res = [];
                            res = ggA.split(' ');
                            ggA = res[2].replace(/<br ?\/?>/g, "");
                            ggA = parseInt(ggA);
                            res = ggB.split(' ');
                            ggB = res[2].replace(/<br ?\/?>/g, "");
                            ggB = parseInt(ggB);
                            if (ggA > ggB)
                                return -1
                            if (ggA < ggB)
                                return 1
                            else {
                                var countA = 0;
                                var countB = 0;
                                if (checkedlyfId.length == 1) {
                                    var bankA = $(a).find(".bankName_sort").val();
                                    var bankB = $(b).find(".bankName_sort").val();
                                    if (bankA > bankB)
                                        return 1
                                    if (bankA < bankB)
                                        return -1
                                } else {
                                    var lsbpListA = $(a).find('#lspbList').val();
                                    var lsbpListB = $(b).find('#lspbList').val();
                                    $.each(checkedlyfId, function (k, v) {
                                        if ($.inArray(v, lsbpListA) != -1) {
                                            countA++;
                                        }
                                    });
                                    $.each(checkedlyfId, function (k, v) {
                                        if ($.inArray(v, lsbpListB) != -1) {
                                            countB++;
                                        }
                                    });
                                    if (countA > countB)
                                        return -1
                                    if (countA < countB)
                                        return 1
                                    else {
                                        var bankA = $(a).find(".bankName_sort").val();
                                        var bankB = $(b).find(".bankName_sort").val();
                                        if (bankA > bankB)
                                            return 1
                                        if (bankA < bankB)
                                            return -1
                                    }
                                }
                            }
                        }

                    });
                    $("#sort_list_div").html(sort_cards);
                }
            } else {
                var $divs = $("div.features");
                var ascSort = $divs.sort(function (a, b) {
                    return $(a).find(".cpNo_sort").val() - $(b).find(".cpNo_sort").val();
                });
                $("#sort_list_div").html(ascSort);
            }
            if ($('div.features_desc:visible').length > 0)
                $('.no_cards').addClass('hidden');
            else
                $('.no_cards').removeClass('hidden');
        });

        /*Web Filter functionality ends*/

        /*Mobile Filter functionality starts*/

        $(".mob_bankNum").on("change", function () {
            console.log(".mob_bankNum called on change");
            mob_checkedBankId = [];
            $(".mob_bankNum").each(function () {
                if ($(this).is(':checked')) {
                    mob_checkedBankId.push($(this).val());
                } else {
                    mob_checkedBankId.remove($(this).val());
                }
            });
        });

        $(".mob_lsbpNum").on("change", function () {
            mob_checkedlyfId = [];
            $(".mob_lsbpNum").each(function () {
                if ($(this).is(':checked')) {
                    mob_checkedlyfId.push($(this).val());
                } else {
                    mob_checkedlyfId.remove($(this).val());
                }
            });
        });

        $('#mob_apply-btn').on('click', function () {

            if (mob_checkedBankId.length == 0) {
                mob_checkedBankId = mob_bankid;
            }

            if (mob_checkedlyfId.length == 0) {
                mob_checkedlyfId = mob_lyfId;
                islyfunchecked = true;
            }

            $("div.features_desc").each(function () {
                var bpId = $(this).find('#bpNo').val();
                var cpNo = $(this).find('#cpNo').val();
                var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
                fees_range = $(".slider_div .rangeslider__handle").html();
                if (jQuery.type(fees_range) === "undefined" || fees_range == '') {
                    fees_range = 10000;
                } else {
                    fees_range = fees_range.replace(/<\/?span[^>]*>/g, "");
                    fees_range = fees_range.replace(/\,/g, '');
                }
                var lsbpList = $(this).find('#lspbList').val();
                if ($.inArray(bpId, mob_checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {
                    var flag = false;
                    $.each(mob_checkedlyfId, function (k, v) {
                        if ($.inArray(v, lsbpList) != -1) {
                            flag = true;
                        }
                    });
                    if (flag) {
                        $('#card_row' + cpNo).show();
                    } else {
                        $('#card_row' + cpNo).hide();
                    }

                } else {
                    $('#card_row' + cpNo).hide();
                }
            });
            $('#filterPopup').modal('hide');

            if (!islyfunchecked) {
                if ($('.ff').html() == 'XX') {
                    var $divs = $("div.features");
                    var ascSort = $divs.sort(function (a, b) {
                        var countA = 0;
                        var countB = 0;
                        if (mob_checkedlyfId.length == 1) {
                            var bankA = $(a).find(".bankName_sort").val();
                            var bankB = $(b).find(".bankName_sort").val();
                            if (bankA > bankB)
                                return 1
                            if (bankA < bankB)
                                return -1
                        } else {
                            var lsbpListA = $(a).find('#lspbList').val();
                            var lsbpListB = $(b).find('#lspbList').val();
                            $.each(mob_checkedlyfId, function (k, v) {
                                if ($.inArray(v, lsbpListA) != -1) {
                                    countA++;
                                }
                            });
                            $.each(mob_checkedlyfId, function (k, v) {
                                if ($.inArray(v, lsbpListB) != -1) {
                                    countB++;
                                }
                            });
                            if (countA > countB)
                                return -1
                            if (countA < countB)
                                return 1
                            else {
                                var bankA = $(a).find(".bankName_sort").val();
                                var bankB = $(b).find(".bankName_sort").val();
                                if (bankA > bankB)
                                    return 1
                                if (bankA < bankB)
                                    return -1
                            }
                        }

                    });
                    $("#sort_list_div").html(ascSort);
                } else {
                    var $divs = $("div.features");
                    var sort_cards = $divs.sort(function (a, b) {
                        var ffA = parseInt($(a).find(".ff").html());
                        var ffB = parseInt($(b).find(".ff").html());
                        if (ffA > ffB)
                            return -1
                        if (ffA < ffB)
                            return 1
                        else {
                            var ggA = $(a).find(".gg").html();
                            var ggB = $(b).find(".gg").html();
                            var res = [];
                            res = ggA.split(' ');
                            ggA = res[2].replace(/<br ?\/?>/g, "");
                            ggA = parseInt(ggA);
                            res = ggB.split(' ');
                            ggB = res[2].replace(/<br ?\/?>/g, "");
                            ggB = parseInt(ggB);
                            if (ggA > ggB)
                                return -1
                            if (ggA < ggB)
                                return 1
                            else {
                                var countA = 0;
                                var countB = 0;
                                if (mob_checkedlyfId.length == 1) {
                                    var bankA = $(a).find(".bankName_sort").val();
                                    var bankB = $(b).find(".bankName_sort").val();
                                    if (bankA > bankB)
                                        return 1
                                    if (bankA < bankB)
                                        return -1
                                } else {
                                    var lsbpListA = $(a).find('#lspbList').val();
                                    var lsbpListB = $(b).find('#lspbList').val();
                                    $.each(mob_checkedlyfId, function (k, v) {
                                        if ($.inArray(v, lsbpListA) != -1) {
                                            countA++;
                                        }
                                    });
                                    $.each(mob_checkedlyfId, function (k, v) {
                                        if ($.inArray(v, lsbpListB) != -1) {
                                            countB++;
                                        }
                                    });
                                    if (countA > countB)
                                        return -1
                                    if (countA < countB)
                                        return 1
                                    else {
                                        var bankA = $(a).find(".bankName_sort").val();
                                        var bankB = $(b).find(".bankName_sort").val();
                                        if (bankA > bankB)
                                            return 1
                                        if (bankA < bankB)
                                            return -1
                                    }

                                }
                            }

                        }

                    });
                    $("#sort_list_div").html(sort_cards);
                }
            } else {
                var $divs = $("div.features");
                var ascSort = $divs.sort(function (a, b) {
                    return $(a).find(".cpNo_sort").val() - $(b).find(".cpNo_sort").val();
                });
                $("#sort_list_div").html(ascSort);
            }
            if ($('div.features_desc:visible').length > 0)
                $('.no_cards').addClass('hidden');
            else
                $('.no_cards').removeClass('hidden');
        });





        if ('${bankFilter}' == "") {
            $(".bankNum").each(function () {
                bankid.push($(this).val());
                checkedBankId.push($(this).val());
            });
            $(".mob_bankNum").each(function () {
				console.log("mob_bankNum each function");
                mob_bankid.push($(this).val());
                mob_checkedBankId.push($(this).val());
            });
        }
        if ('${lsBenefitsFilter}' == "") {
            $(".lsbpNum").each(function () {
                lyfId.push($(this).val());
                checkedlyfId.push($(this).val());
            });
            $(".mob_lsbpNum").each(function () {
                mob_lyfId.push($(this).val());
                mob_checkedlyfId.push($(this).val());
            });
        }

        if ('${bankFilter}' != "") {
            var bankFilter = '${bankFilter}'.split(',');
            checkedBankId = bankFilter;
            mob_checkedBankId = bankFilter;
            $(".bankNum").each(function () {
                bankid.push($(this).val());
                if ($.inArray($(this).val(), checkedBankId) > -1) {
                    $(this).attr('checked', true);
                }
            });
            $(".mob_bankNum").each(function () {
                console.log("mob_bankNum each mob_bankid");
                mob_bankid.push($(this).val());
                if ($.inArray($(this).val(), mob_checkedBankId) > -1) {
                    $(this).attr('checked', true);
                }
            });
        }

        if ('${lsBenefitsFilter}' != "") {
            var lsBenefitsFilter = '${lsBenefitsFilter}'.split(',');
            checkedlyfId = lsBenefitsFilter;
            mob_checkedlyfId = lsBenefitsFilter;
            $(".lsbpNum").each(function () {
                lyfId.push($(this).val());
                if ($.inArray($(this).val(), checkedlyfId) > -1) {
                    $(this).attr('checked', true);
                    $(this).next().children().toggle();
                }
            });
            $(".mob_lsbpNum").each(function () {
                mob_lyfId.push($(this).val());
                if ($.inArray($(this).val(), mob_checkedlyfId) > -1) {
                    $(this).attr('checked', true);
                    $(this).next().children().toggle();


                }
            });
        }

        var totalBank = bankid.length;
        var totallyfId = lyfId.length;
        var mob_totalBank = mob_bankid.length;
        var mob_totallyfId = mob_lyfId.length;

        if ('${bankFilter}' != "" || '${lsBenefitsFilter}' != "") {
            $(".lsbpNum").trigger("change");
            $('#mob_apply-btn').trigger("click");
        }
        //parvati bank and joining fees
        if ('${bankFilter}' != "" && '${joiningFeeset}' != '') {
//   alert("bpno new"+'$ {bankFilter}');
            var bankFilter = '${bankFilter}'.split(',');
            checkedBankId = bankFilter;
            mob_checkedBankId = bankFilter;
            $(".bankNum").each(function () {
                bankid.push($(this).val());
                if ($.inArray($(this).val(), checkedBankId) > -1) {
                    $(this).attr('checked', true);
                }
            });
            $(".mob_bankNum").each(function () {
                console.log("mob_bankNum mob_checkedBankId");
                mob_bankid.push($(this).val());
                if ($.inArray($(this).val(), mob_checkedBankId) > -1) {
                    $(this).attr('checked', true);
                }
            });
            $("div.features_desc").each(function () {
                var cpNo = $(this).find('#cpNo').val();
                var bpId = $(this).find('#bpNo').val();
                var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
//                                  console.log("fees->"+fees);
                fees_range = '${joiningFeeset}';
//                                console.log("fees_range-->"+fees_range);
                var lsbpList = $(this).find('#lspbList').val();
                if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {

                    var flag = false;

                    $.each(checkedlyfId, function (k, v) {

                        if ($.inArray(v, lsbpList) != -1) {

                            flag = true;
                        }
                    });
                    if (flag) {


                        $('#card_row' + cpNo).show();
                    } else {

                        $('#card_row' + cpNo).hide();
                    }
                } else {

                    $('#card_row' + cpNo).hide();
                }

            });



            $(this).parents('.parent_div').addClass('hidden');



            if ($('div.features_desc:visible').length > 0)
                $('.no_cards').addClass('hidden');
            else
                $('.no_cards').removeClass('hidden');
        }
        //end parvati bank and joining fees
        //parvati lifestylebenefit and joining fees
        if ('${lsBenefitsFilter}' != "" && '${joiningFeeset}' != '') {
//                     alert("lifestyle ==>"+'${lsBenefitsFilter}'+" and joining fees--->"+'${joiningFeeset}')
            var lsBenefitsFilter = '${lsBenefitsFilter}'.split(',');
            checkedlyfId = lsBenefitsFilter;
            mob_checkedlyfId = lsBenefitsFilter;
            $(".lsbpNum").each(function () {
                lyfId.push($(this).val());
                if ($.inArray($(this).val(), checkedlyfId) > -1) {
                    $(this).attr('checked', true);
                    $(this).next().children().toggle();
                }
            });
            $(".mob_lsbpNum").each(function () {
                mob_lyfId.push($(this).val());
                if ($.inArray($(this).val(), mob_checkedlyfId) > -1) {
                    $(this).attr('checked', true);
                    $(this).next().children().toggle();


                }
            });
            $("div.features_desc").each(function () {
                var cpNo = $(this).find('#cpNo').val();
                var bpId = $(this).find('#bpNo').val();
                var fees = $("#feesId" + cpNo).text().substring(4).replace(/\,/g, '');
//                                  console.log("fees->"+fees);
                fees_range = '${joiningFeeset}';
//                                console.log("fees_range-->"+fees_range);
                var lsbpList = $(this).find('#lspbList').val();
//                                    console.log("cpNo===>"+cpNo);
//                                    console.log("bpId===>"+bpId);
//    				    console.log("checkedBankId===>"+checkedBankId);
//                                    console.log("checkedlyfId====>"+checkedlyfId);
//                                    console.log("lsbpList====>"+lsbpList);
                if ($.inArray(bpId, checkedBankId) != -1 && parseInt(fees) <= parseInt(fees_range)) {

                    var flag = false;

                    $.each(checkedlyfId, function (k, v) {

                        if ($.inArray(v, lsbpList) != -1) {

                            flag = true;
                        }
                    });
                    if (flag) {


                        $('#card_row' + cpNo).show();
                    } else {

                        $('#card_row' + cpNo).hide();
                    }
                } else {

                    $('#card_row' + cpNo).hide();
                }

            });



            $(this).parents('.parent_div').addClass('hidden');



            if ($('div.features_desc:visible').length > 0)
                $('.no_cards').addClass('hidden');
            else
                $('.no_cards').removeClass('hidden');
        }
        //end parvati lifestyle and joining fees
        /*Mobile clear Filter functionality Starts*/
        $('#mob_clearFilters').on('click', function () {
            $('.life_benefits_holder li input:checked').next().children().toggle();
            $('.mob_bankNum').attr('checked', false);
            $('.mob_lsbpNum').attr('checked', false);
            $('.no_cards').addClass('hidden');
            checkedBankId = [];
            bankid = [];
            lyfId = [];
            checkedlyfId = [];
            fees_range = 10000;
            joiningFees = 500;
            arr = [];

            $(".mob_bankNum").each(function () {
                console.log("mob_bankNum each checkedBankId");
                mob_bankid.push($(this).val());
                mob_checkedBankId.push($(this).val());
            });
            $(".mob_lsbpNum").each(function () {
                mob_lyfId.push($(this).val());
                mob_checkedlyfId.push($(this).val());
            });

    <c:forEach items="${cardList}" var="listItem">
            arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
            $.each(arr, function (i, val) {
                $('#card_row' + val).show();
            });
            if (elementClickedSortByFeeAsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_sort_by_fee_asc, .sort_by_fee_asc");

            } else if (elementClickedSortByFeesDsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_sort_by_fee_dsc, .sort_by_fee_dsc");
            } else if (elementClickedBankSortAsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_sorting_ascending, .sorting_ascending");
            } else if (elementClickedBankSortDsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_sorting_descending, .sorting_descending");
            } else if (elementClickedSortByLyfAsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_sort_by_lyf_asc, .sort_by_lyf_asc");
            } else if (elementClickedSortByLyfDsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_sort_by_lyf_dsc, .sort_by_lyf_dsc");
            } else if (elementClickedSortAirTicketDsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_air_ticket_dsc, .air_ticket_dsc");
            } else if (elementClickedSortAirTicketAsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_air_ticket_asc, .air_ticket_asc");
            } else {
                var $divs = $("div.features");
                var ascSort = $divs.sort(function (a, b) {
                    //                                console.log("a-->",$(".cpDpNo_sort").val());
                    return $(a).find(".cpDpNo_sort").val() - $(b).find(".cpDpNo_sort").val();
                    //					  return $(a).find(".cpNo_sort").val() - $(b).find(".cpNo_sort").val();
                });
                $("#sort_list_div").html(ascSort);
                $('#filterPopup').modal('hide');

            }
        });
        /*Mobile Filter functionality ends*/

        //for internal issue
        var elementClickedSortByFeeAsc = false;
        var elementClickedBankSortAsc = false;
        var elementClickedBankSortDsc = false;
        var elementClickedSortByFeesDsc = false;
        var elementClickedSortByLyfAsc = false;
        var elementClickedSortByLyfDsc = false;
        var elementClickedSortAirTicketDsc = false;
        var elementClickedSortAirTicketAsc = false;


        $("#clear_fliter").on('click', function () {


			console.log("Inside fourth clear fliter");
            $('.life_benefits_holder li input:checked').next().children().toggle();
            $('.sort_listing').addClass('hidden');
//            $(".sort_by").text('Sort By');


            $('.bankNum').attr('checked', false);
            $('.lsbpNum').attr('checked', false);
            $('.no_cards').addClass('hidden');


            checkedBankId = [];
            bankid = [];
            lyfId = [];
            checkedlyfId = [];
            arr = [];
            fees_range = 10000;
            joiningFees = 500;
            $(".bankNum").each(function () {
                bankid.push($(this).val());
                checkedBankId.push($(this).val());
            });
            $(".lsbpNum").each(function () {
                lyfId.push($(this).val());
                checkedlyfId.push($(this).val());
            });

    <c:forEach items="${cardList}" var="listItem">
            arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
            $.each(arr, function (i, val) {
                console.log('$(#card_row + val).show()>>>>>>'+$('#card_row' + val).show());
                $('#card_row' + val).show();
            });

    console.log("elementClickedSortByFeeAsc>>"+elementClickedSortByFeeAsc);
    console.log("elementClickedSortByFeesDsc>>"+elementClickedSortByFeesDsc);
    console.log("elementClickedBankSortAsc>>"+elementClickedBankSortAsc);
    console.log("elementClickedBankSortDsc>>"+elementClickedBankSortDsc);
    console.log("elementClickedSortByLyfAsc>>"+elementClickedSortByLyfAsc);
    console.log("elementClickedSortByLyfDsc>>"+elementClickedSortByLyfDsc);
    console.log("elementClickedSortAirTicketDsc>>"+elementClickedSortAirTicketDsc);
    console.log("elementClickedSortAirTicketAsc>>"+elementClickedSortAirTicketAsc);
    
            if (elementClickedSortByFeeAsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_sort_by_fee_asc, .sort_by_fee_asc");  
            } else if (elementClickedSortByFeesDsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_sort_by_fee_dsc, .sort_by_fee_dsc");
            } else if (elementClickedBankSortAsc == true) {			
                $(".sort_list").triggerHandler("click", ".mob_sorting_ascending, .sorting_ascending");                
            } else if (elementClickedBankSortDsc == true) {
                console.log('inside true');
                $(".sort_list").triggerHandler("click", ".mob_sorting_descending, .sorting_descending");
            } else if (elementClickedSortByLyfAsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_sort_by_lyf_asc, .sort_by_lyf_asc");
            } else if (elementClickedSortByLyfDsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_sort_by_lyf_dsc, .sort_by_lyf_dsc");
            } else if (elementClickedSortAirTicketDsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_air_ticket_dsc, .air_ticket_dsc");
            } else if (elementClickedSortAirTicketAsc == true) {
                $(".sort_list").triggerHandler("click", ".mob_air_ticket_asc, .air_ticket_asc");
            } else {

                var $divs = $("div.features");
                console.log('$divs>>>'+$divs);
                var ascSort = $divs.sort(function (a, b) {
//                        console.log("a-->",$(".cpDpNo_sort").val());
                    return $(a).find(".cpDpNo_sort").val() - $(b).find(".cpDpNo_sort").val();
                });
                $("#sort_list_div").html(ascSort);
                
                

            }
        });



        $(".sort_list").on("click", ".mob_sorting_ascending, .sorting_ascending", function (event) {
	console.log(".sort_list called");
        
        $(this).toggleClass('toggle')
            .siblings().removeClass('toggle')     

        
        elementClickedBankSortAsc = true;
            var $divs = $("div.features");
            var ascSort = $divs.sort(function (a, b) {
                var bankA = $(a).find(".bankName_sort").val();
                var bankB = $(b).find(".bankName_sort").val();
				console.log("bankA>>"+bankA);
				console.log("bankB>>"+bankB);
                
                if (bankA > bankB)
                    return 1
                if (bankA < bankB)
                    return -1
            });
            $("#sort_list_div").html(ascSort);
            $('#filterPopup').modal('hide');
//              $('.parent_div').toggle();
              
              if($('.parent_div:visible').length)
              {
                    $('.parent_div').addClass('hidden');
//                    $(".sort_by").html('<span class="ascendingAB" id="CrdHP_fltr_sort-by">Bank (A-Z)</span>');
                        $(".sort_by").text('Bank (A-Z)');
                        $(".sort_div").text('Bank (A-Z)');
                        $('span.sort_by').removeAttr('style');
                }
             else
             {
                $('.parent_div').removeClass('hidden');                
//                $(this).removeAttr('style');
             }
//              

            

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".mob_sorting_descending, .sorting_descending", function (event) {
			console.log("sort_list called sorting_descending");
                        
          $(this).toggleClass('toggle')
            .siblings().removeClass('toggle') 
        elementClickedBankSortDsc = true;
            var $divs = $("div.features");
            var descSort = $divs.sort(function (a, b) {
                var bankA = $(a).find(".bankName_sort").val();
                var bankB = $(b).find(".bankName_sort").val();
                console.log("bankA sorting_descending>>"+bankA);
                console.log("bankB sorting_descending>>"+bankB);
                if (bankA > bankB)
                    return -1
                if (bankA < bankB)
                    return 1
            });
            $("#sort_list_div").html(descSort);
            $('#filterPopup').modal('hide');
            
             if($('.parent_div:visible').length)
              {
                    $('.parent_div').addClass('hidden');
//                    $(".sort_by").html('<span class="ascendingAB" id="CrdHP_fltr_sort-by">Bank (Z-A)</span>');
                    $(".sort_by").text('Bank (Z-A)');
                    $(".sort_div").text('Bank (Z-A)');
                    $('span.sort_by').removeAttr('style');
//                    $('.sorting_descending').css("color","#03868b");
                }
             else
             {
                $('.parent_div').removeClass('hidden');
//                $('.sorting_descending').css("color","#03868b");
             }

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here

        });

        $(".sort_list").on("click", ".mob_sort_by_fee_asc, .sort_by_fee_asc", function (event) {
           
        elementClickedSortByFeeAsc = true;
        
        $(this).toggleClass('toggle')
            .siblings().removeClass('toggle')
    
            var $divs = $("div.features");
            var fees_sort_asc = $divs.sort(function (a, b) {
                return $(a).find(".fees_class").attr('data-fees') - $(b).find(".fees_class").attr('data-fees');
            });
            $("#sort_list_div").html(fees_sort_asc);
            $('#filterPopup').modal('hide');
            
             if($('.parent_div:visible').length)
              {
                    $('.parent_div').addClass('hidden');
//                    $(".sort_by").html('<span class="FeesLH" id="CrdHP_fltr_sort-by">Fees(L to H)</span>');
                    $(".sort_by").text('Fees(L to H)');
                    $(".sort_div").text('Fees(L to H)');
                    $('span.sort_by').removeAttr('style');
                }
             else
             {
                $('.parent_div').removeClass('hidden');                
             }

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".mob_sort_by_fee_dsc, .sort_by_fee_dsc", function (event) {
          
        $(this).toggleClass('toggle')
            .siblings().removeClass('toggle')
    
        elementClickedSortByFeesDsc = true;
            var $divs = $("div.features");
            var fees_sort_dsc = $divs.sort(function (a, b) {
                return $(b).find(".fees_class").attr('data-fees') - $(a).find(".fees_class").attr('data-fees');
            });
            $("#sort_list_div").html(fees_sort_dsc);
            $('#filterPopup').modal('hide');
            
             if($('.parent_div:visible').length)
              {
                    $('.parent_div').addClass('hidden');                    
//                    $(".sort_by").html('<span class="FeesLH" id="CrdHP_fltr_sort-by">Fees(H to L)</span>');
                    $(".sort_by").text('Fees(H to L)');
                    $(".sort_div").text('Fees(H to L)');
                    $('span.sort_by').removeAttr('style');
                    
                }
             else
             {
                $('.parent_div').removeClass('hidden');                
             }

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".mob_sort_by_lyf_asc, .sort_by_lyf_asc", function (event) {
           
        $(this).toggleClass('toggle')
            .siblings().removeClass('toggle')
    
        elementClickedSortByLyfAsc = true;
            var $divs = $("div.features");
            var lyf_sort_asc = $divs.sort(function (a, b) {
                return $(a).find(".lspbListSize").val() - $(b).find(".lspbListSize").val();
            });
            $("#sort_list_div").html(lyf_sort_asc);
            $('#filterPopup').modal('hide');
            
             if($('.parent_div:visible').length)
              {
                    $('.parent_div').addClass('hidden');
//                    $(".sort_by").html('<span class="FeesLH" id="CrdHP_fltr_sort-by">Lifestyle Benefit(Asc)</span>');
                        $(".sort_by").text('Lifestyle(Asc)');
                        $(".sort_div").text('Lifestyle(Asc)');
//                        $(".sort_by").css('font-size','7px');
                }
             else
             {
                $('.parent_div').removeClass('hidden');                
             }

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".mob_sort_by_lyf_dsc, .sort_by_lyf_dsc", function (event) {
            
        $(this).toggleClass('toggle')
            .siblings().removeClass('toggle')
    
        elementClickedSortByLyfDsc = true;
            var $divs = $("div.features");
            var lyf_sort_dsc = $divs.sort(function (a, b) {
                return $(b).find(".lspbListSize").val() - $(a).find(".lspbListSize").val();
            });
            $("#sort_list_div").html(lyf_sort_dsc);
            $('#filterPopup').modal('hide');
            
             if($('.parent_div:visible').length)
              {
                    $('.parent_div').addClass('hidden');
//                    $(".sort_by").html('<span class="FeesLH" id="CrdHP_fltr_sort-by">Lifestyle Benefit(Desc)</span>');
                        $(".sort_by").text('Lifestyle(Dsc)');
                        $(".sort_div").text('Lifestyle(Dsc)');
//                        $(".sort_by").css('font-size','7px');
                }
             else
             {
                $('.parent_div').removeClass('hidden');                
             }

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".mob_air_ticket_asc, .air_ticket_asc", function (event) {
               
        $(this).toggleClass('toggle')
            .siblings().removeClass('toggle')
    
        if ($('.ff').html() != 'XX') {
                elementClickedSortAirTicketAsc = true;
                var $divs = $("div.features");
                var air_ticket_asc = $divs.sort(function (a, b) {
                    return parseInt($(a).find(".ff").html()) - parseInt($(b).find(".ff").html());
                });
                $("#sort_list_div").html(air_ticket_asc);
                $('#filterPopup').modal('hide');
            }
            
             if($('.parent_div:visible').length)
              {
                    $('.parent_div').addClass('hidden');
//                    $(".sort_by").html('<span class="FeesLH" id="CrdHP_fltr_sort-by">Air Tickets(Asc)</span>');
                     $(".sort_by").text('Tickets(Asc)');
                     $(".sort_div").text('Tickets(Asc)');
//                     $(".sort_by").css('font-size','8px');
                    
                }
             else
             {
                $('.parent_div').removeClass('hidden');                
             }

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        $(".sort_list").on("click", ".mob_air_ticket_dsc, .air_ticket_dsc", function (event) {
                    
        $(this).toggleClass('toggle')
            .siblings().removeClass('toggle')
    
        if ($('.ff').html() != 'XX') {
                elementClickedSortAirTicketDsc = true;
                var $divs = $("div.features");
                var air_ticket_dsc = $divs.sort(function (a, b) {
                    return parseInt($(b).find(".ff").html()) - parseInt($(a).find(".ff").html());
                });
                $("#sort_list_div").html(air_ticket_dsc);
                $('#filterPopup').modal('hide');
            }
            
             if($('.parent_div:visible').length)
              {
                    $('.parent_div').addClass('hidden');
//                    $(".sort_by").html('<span class="FeesLH" id="CrdHP_fltr_sort-by">Air Tickets(Desc)</span>');
                    $(".sort_by").text('Tickets(Dsc)');
                    $(".sort_div").text('Tickets(Dsc)');
//                    $(".sort_by").css('font-size','8px');
                    
                }
             else
             {
                $('.parent_div').removeClass('hidden');                
             }

            // Adobe code starts here
            sortByAdobe = ($(this).text().trim());
            submitFilterAdobe();
            // Adobe code ends here
        });

        /*Sort functionality ends*/

        /*Recommended validation starts*/

        var membercityName = '${memberCity}'

        $(".city_uncheck").each(function () {
            if (membercityName != "" && membercityName.toLowerCase() === $(this).val().toLowerCase()) {
                $("input[value='" + $(this).val() + "']").prop('checked', true);
                $('input[name="modal_city"]').trigger('change');
                $("#cityName").val(membercityName);
            }
        });

        $('#select_modal_city > option').each(function () {
            if (membercityName.toLowerCase() === $(this).val().toLowerCase()) {
                $(this).prop('selected', true);
                $("#cityName").val(membercityName);
            }
        });

        $('input[name="modal_city"]').on('change', function () {
            $("#error_msg_city").hide();
            $(".select_city_uncheck").val('');
            $("[name=" + $(this).prop('name') + "]").prop("checked", false);
            $(this).prop("checked", true);
            $("#cityName").val(this.value);
        });

        $("#select_modal_city").change(function () {
            $(".city_uncheck").prop("checked", false);
            var cityName = $("#select_modal_city").val();
            $("#error_msg_city").hide();
            $("#cityName").val(cityName);

        });

        $("#date_of_birth").on("change", function () {
            $("#error_msg_dob").hide();
        });

        $(':checkbox[name=lsbp_name]').on('click', function () {
            var checkedBoxlength = $(':checkbox[name=lsbp_name]:checked').length;
            if (checkedBoxlength > 2) {
                $("#error_msg_lsbpArray").show().text("Sorry, You can select only 2 benefit preferences at a time");
                return false;
            } else {
                $("#error_msg_lsbpArray").hide();
                $("#lsbpArray").val(this.value);
            }
        });
        $("input[name=annual_Income]").change(function () {
//                    console.log("annual income"+this.value);
            if (this.value == ">2000000") {
//                        console.log("in if")
                $("#annualIncome").val("gt2000000");
            } else {
//                         console.log("in else")
                $("#annualIncome").val(this.value);
            }

            $("#error_msg_annualIncome").hide();
        });

        $('#CrdHP_btn_submit-recommend').on('click', function () {
            var annualIncome = $("#annualIncome").val();
            if (annualIncome == "") {
                $("#error_msg_annualIncome").show().text("Please Select Gross Annual Income");
                return false;
            } else {
                $("cardDetailsBean").submit();
                //Adobe code starts here
                $.fn.AnnualIncomeClick($("#annualIncome").val());
                //Adobe code ends here
            }
        });

        /*Recommended validation ends*/

//                 code for set aual income




//              $('#CrdHP_pm-spend_back').onClick(){
//                  
//                  alert('id base ');
//              }
        var newValue;
        $('.goTo_lifePrivileges').on('click', function () {
//                 alert(' else condtion '+$("#monthlySpend").val());    
            if ($(this).hasClass('BackButton'))
            {
//             alert("before setting"+newValue);
                newValue = $("#monthlySpend").val();
//             alert('back adobe '+$("#monthlySpend").val());

            }
        });

        $("#CrdHP_lifestyle_next").click(function () {
//                    alert("out side  condition "+newValue);
            if (newValue != undefined) {
//                         alert("inside if condition "+newValue);
                $('#monthlySpend').val(newValue);
                $('#anualIncome').val(newValue);

            } else {

//                         alert("inside else condition "+newValue);
            }



        });
//


//                         alert($('.spendCapability_content body_content').hasClass("hidden"));
//                       var test;
//                        $("#monthlySpend").change(function(){
//                            
//                             alert($('.spendCapability_content body_content').hasClass("hidden") );
//                             var incomecompare=$('#incomecompare').text();
//                             
//                             if(!$('.spendCapability_content body_content').hasClass("hidden")){
//                                 
//                              
//                     var income=$('#monthlySpend').val();
//                       $('#anualIncome').val(income);
//                      $('#incomevlaue').val(income);
//                      alert("inside on change"+income)
//                      var testing=$('#incomevlaue').val();
//                      
//                     console.log("*******value is set *******"+testing);
//                             
//                       test='';  
//                
//        }
//        test='str';
//                     
//               
//             });







        /*Spends calculation logic starts*/
        $(".donebutdone").on('click', function () {
            console.log('done bu done inside');
            var utm_source = '${utm_source}';
            var utm_medium = '${utm_medium}';
            var utm_campaign = '${utm_campaign}';
            $('.free_flights_div').css('display', 'block');
            $('.free_flights_desc').css('display', 'none');
            $('.freeFlight_spends_calc').addClass('hidden');
            if ($(".slider_error_div").text() == "") {
                var spend = $("#yearly_bill").html().replace(/<\/?img[^>]*>/g, "");
                spend = spend.substring(spend.indexOf(' ') + 1);

                //Adobe code starts here
                $.fn.CompareCardsPopupClick(spend);
                //Adobe code ends here

                if (compareCards_boolean == 1) {
                    arr = [];
                    $('.compareCards:checked').each(function () {
                        arr.push($(this).val());
                    });

                    var cardId = "cardNo";

                    var cpids = arr.join('|');
                    var redirectUrl = ctx + 'compare-cards';

                    document.cookie = cardId + "=" + cpids + ";" + 60000 + ";path=/";

                    function readCookie(name) {
                        var nameEQ = name + "=";
                        var ca = document.cookie.split(';');
                        for (var i = 0; i < ca.length; i++) {
                            var c = ca[i];
                            while (c.charAt(0) == ' ')
                                c = c.substring(1, c.length);
                            if (c.indexOf(nameEQ) == 0)
                                return c.substring(nameEQ.length, c.length);
                        }
                        return null;
                    }

                    var value = readCookie('cardNo');


                    $.redirect(redirectUrl, {'cpids': value, 'spend': spend, 'utm_source': utm_source, 'utm_medium': utm_medium, 'utm_campaign': utm_campaign});
                } else {
                    arr = [];
    <c:forEach items="${cardList}" var="listItem">
                    arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
                    $.get("${applicationURL}calcfreeflights?spend=" + spend + "&cpNoList=" + arr, function (data, status) {
                        if (status == "success") {
                            $.each(data, function (key, value) {
                                $("#freeFlights" + key).html(value.split(",")[1]);
                                $("#jpMiles" + key).html("on earning <br>" + value.split(",")[0] + " InterMiles");
                            });
                            //$('.spends_calc').addClass('hidden');
                            $("#error_msg ,#layover_error_msg").hide();
                            $('#spends_slider').modal('hide');
                        }
                    });

                    $('.rangeslider__handle').html("<span>" + freeFlights_calc.toLocaleString() + '</span>');
                    $('.freeFlights_calc input[type="range"]').val(freeFlights_calc).change();
                }

            }//error message if
        });
        $(".donebut").on('click', function () {
            console.log('inside done but');
            var utm_source = '${utm_source}';
            var utm_medium = '${utm_medium}';
            var utm_campaign = '${utm_campaign}';
            $('.free_flights_div').css('display', 'block');
            $('.free_flights_desc').css('display', 'none');
            $('.freeFlight_spends_calc').addClass('hidden');
            if ($(".slider_modal_div_error").text() == "") {
                var spend = $("#yearly_bill1").html().replace(/<\/?img[^>]*>/g, "");
                spend = spend.substring(spend.indexOf(' ') + 1);

                //Adobe code starts here
//            $.fn.CompareCardsPopupClick(spend);
                //Adobe code ends here

                if (compareCards_boolean == 1) {
                    arr = [];
                    $('.compareCards:checked').each(function () {
                        arr.push($(this).val());
                    });

                    var cardId = "cardNo";

                    var cpids = arr.join('|');
                    var redirectUrl = ctx + 'compare-cards';

                    document.cookie = cardId + "=" + cpids + ";" + 60000 + ";path=/";

                    function readCookie(name) {
                        var nameEQ = name + "=";
                        var ca = document.cookie.split(';');
                        for (var i = 0; i < ca.length; i++) {
                            var c = ca[i];
                            while (c.charAt(0) == ' ')
                                c = c.substring(1, c.length);
                            if (c.indexOf(nameEQ) == 0)
                                return c.substring(nameEQ.length, c.length);
                        }
                        return null;
                    }

                    var value = readCookie('cardNo');


                    $.redirect(redirectUrl, {'cpids': value, 'spend': spend, 'utm_source': utm_source, 'utm_medium': utm_medium, 'utm_campaign': utm_campaign});
                } else {
                    arr = [];
    <c:forEach items="${cardList}" var="listItem">
                    arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
                    $.get("${applicationURL}calcfreeflights?spend=" + spend + "&cpNoList=" + arr, function (data, status) {
                        if (status == "success") {
                            $.each(data, function (key, value) {
                                $("#freeFlights" + key).html(value.split(",")[1]);
                                $("#jpMiles" + key).html("on earning <br>" + value.split(",")[0] + " InterMiles");
                            });
                            //$('.spends_calc').addClass('hidden');
                            $("#error_msg ,#layover_error_msg").hide();
                            $('#spends_slider').modal('hide');
                        }
                    });

                    $('.rangeslider__handle').html("<span>" + freeFlights_calc.toLocaleString() + '</span>');
                    $('.freeFlights_calc input[type="range"]').val(freeFlights_calc).change();
                }
            }//error message if

        });
        $(".annual_spends").on('click', function () {
            var utm_source = '${utm_source}';
            var utm_medium = '${utm_medium}';
            var utm_campaign = '${utm_campaign}';
            $('.free_flights_div').css('display', 'block');
            $('.free_flights_desc').css('display', 'none');
            $('.freeFlight_spends_calc').addClass('hidden');

            var spend = $(".spends_calc .annualSpends_value").html().replace(/<\/?img[^>]*>/g, "");
            spend = spend.substring(spend.indexOf(' ') + 1);

            //Adobe code starts here
            $.fn.CompareCardsPopupClick(spend);
            //Adobe code ends here

            if (compareCards_boolean == 1) {
                arr = [];
                $('.compareCards:checked').each(function () {
                    arr.push($(this).val());
                });

                var cardId = "cardNo";

                var cpids = arr.join('|');
                var redirectUrl = ctx + 'compare-cards';

                document.cookie = cardId + "=" + cpids + ";" + 60000 + ";path=/";

                function readCookie(name) {
                    var nameEQ = name + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ')
                            c = c.substring(1, c.length);
                        if (c.indexOf(nameEQ) == 0)
                            return c.substring(nameEQ.length, c.length);
                    }
                    return null;
                }

                var value = readCookie('cardNo');


                $.redirect(redirectUrl, {'cpids': value, 'spend': spend, 'utm_source': utm_source, 'utm_medium': utm_medium, 'utm_campaign': utm_campaign});
            } else {
                arr = [];
    <c:forEach items="${cardList}" var="listItem">
                arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
                $.get("${applicationURL}calcfreeflights?spend=" + spend + "&cpNoList=" + arr, function (data, status) {
                    if (status == "success") {
                        $.each(data, function (key, value) {
                            $("#freeFlights" + key).html(value.split(",")[1]);
                            $("#jpMiles" + key).html("on earning <br>" + value.split(",")[0] + " InterMiles");
                        });
                        //$('.spends_calc').addClass('hidden');
                        $("#error_msg ,#layover_error_msg").hide();
                        $('#spends_slider').modal('hide');
                    }
                });

                $('.rangeslider__handle').html("<span>" + freeFlights_calc.toLocaleString() + '</span>');
                $('.freeFlights_calc input[type="range"]').val(freeFlights_calc).change();
            }


        });

        /*Spends calculation logic ends*/

        $.validator.addMethod('valideJpNumber', function (value, element) {
            var jpNumLastDig, jpNoTemp;
            var jp = $("#jpNumber").val();
            if (jp == "") {
                return false;
            }
            if (isNaN(jp)) {
                return false;
            }
            if (jp.length != 9) {
//                            console.log("onside validator");
                return false;
            } else
            {
                jpNumLastDig = jp % 10;
                jpNoTemp = eval(jp.slice(0, -1)) % 7;
                if (jpNumLastDig !== jpNoTemp)
                    return false;
                else
                    return true;
            }
        });


        $.validator.addMethod('checkForValidTier', function (value, element) {
            if (tierExist) {
                return true;
            } else {
                return false;
            }
        });

        $("#jpNumber").keyup(function () {
            var jpNum = $("#jpNumber").val();
            var chk;
            if (jpNum != "" && jpNum.length == 9) {  
                $.ajax({url: "${applicationURL}getTier?jpNum=" + jpNum,
                    async: false,
                    success:
                            function (data) {

//                                 tierExist = data.length > 2 ? true : false;    
                                 
                                if (data.length > 2) {
                             console.log("data", data);
                             tierExist = true;
                            // alert(tierExist);
                             
                             $('#indus_pop_but').removeClass('indusbut');
                    		 $('#indus_pop_but').addClass('indus_but');	 
      
                             $("#tier").val(data);
                             
                         } else {
                        	 tierExist = false;
                        	// alert(tierExist);
                        	 $('#indus_pop_but').removeClass('indus_but');
                    		 $('#indus_pop_but').addClass('indusbut');	 
                         }
                                
                            }
                });

                $("#tellMeAbout").validate().element('#jpNumber');
//                 $("#proceedTo").removeAttr('disabled');
            }


        });



//      $ ( ' # jpNumber').on('change',function(){

//        
//    	 var jpNum=$("#jpNumber").val()
//          console.log("=================>"+jpNum);
//    	 if(jpNum!="" && jpNum.length==9){	 
//	   	   	$.ajax({ url: "${applicationURL}getTier?jpNum="+jpNum, 
//	   	            async: false, 
//	   	            success: 
//	   	                function(data) {
//                                    
//	   	            	tierExist = data.length>2 ? true : false; 
//	   	            	}
//	   	          });
//	   	    
//	   	 	$("#tellMeAbout").validate().element('#jpNumber');
//    	  }
//          else{
//              tierExist=false;
//          }
//      });

        $(document).on("click", ".upgradeNow", function () {
            $("#otcpNo").val(parseInt($(this).attr('data-cpNo')));

            $("#otbpNo").val(parseInt($(this).attr('data-bpNo')));

            $('#askingUser').modal('hide');
            $('#userWithCreditCard').modal('hide');
            $('#abtYourself').modal('show');
            checkupgrade = true;
            //  $("#proceed_modal").modal('hide');


        });

        $(document).on("click", ".non_amex_apply", function () {
            checkupgrade = false;
        });
        $("#tellMeAbout").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                jpNumber: {
                    required: true,
                    valideJpNumber: true,
                    checkForValidTier: true
                }
            },

            messages: {
                jpNumber: {
                    required: "Please Enter InterMiles Number",
                    valideJpNumber: "Please enter valid InterMiles Number",
                    checkForValidTier: "Your application may not have gone through successfully. Please click Submit again."
                }
            },
            errorElement: "div",
            errorPlacement: function (error, element) {
                error.insertAfter($(element).closest("div"));
            },
            submitHandler: function (form) {
                var fees, cardname, cardimg, bankName, id, jpNum;

                if (checkupgrade == true) {
                  // alert("1");
                	id = $("#otcpNo").val();
                    jpNum = $("#jpNumber").val(); 
                    fees = $("#feesId" + id).html();
                    cardname = $("#cardName" + id).html();
                    cardimg = $("#card_img" + id).attr('src');
                    bankName = $("#list_bankName" + id).val();
                    var utm_source = '${utm_source}';
                    var utm_medium = '${utm_medium}';
                    var utm_campaign = '${utm_campaign}';
                    var utm_param = "";

                  //  $("#abtYourself").modal('hide');
                    $("#jpNo").text($("#jpNumber").val());



                    if (utm_source != "" && utm_source != 'undefined') {
                        utm_param = "&utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
                    }
                    $.ajax({url: "${applicationURL}Encryptjpnumber?jpNumber=" + jpNum, success: function (result) {
                            var encryptnum = result;
                            var win = window.open('${applicationURL}applyOtherupgrade?cpNo=' + id + '&imNum=' + encryptnum + utm_param, '_blank');
                            if (win) {
                                //Browser has allowed it to be opened
                                win.focus();
                            } else {

                            }
                        }});
                } else {
                	//alert("2");

                    id = $("#otcpNo").val();

                    jpNum = $("#jpNumber").val();
                  //  alert("jpNum"+jpNum);
                    fees = $("#feesId" + id).html();

                    cardname = $("#cardName" + id).html();

                    cardimg = $("#card_img" + id).attr('src');

                    bankName = $("#list_bankName" + id).val();

                    var utm_source = '${utm_source}';
                    var utm_medium = '${utm_medium}';
                    var utm_campaign = '${utm_campaign}';
                    var utm_param = "";
//    			 console.log("normal else submit handler");
                  //  $("#abtYourself").modal('hide');
                    $("#jpNo").text($("#jpNumber").val());
                    $("#abtYourself").modal('show');
                    $('#replaceImg').attr('src', cardimg);
                    $('#popCardName').html(cardname);


                    $('#popFees').html(fees);
                    $("#proceedBank").text("Proceed to " + bankName);

                    if (utm_source != "" && utm_source != 'undefined') {
                        utm_param = "&utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
                    }
                    var encryptnum;
                    $.ajax({url: "${applicationURL}Encryptjpnumber?jpNumber=" + jpNum, success: function (result) {
                            encryptnum = result;
                            var win = window.open('${applicationURL}applyOther?cpNo=' + id + '&imNum=' + encryptnum + utm_param, '_blank');
                            if (win) {
                                //Browser has allowed it to be opened
                                win.focus();
                                $("#abtYourself").modal('hide');
                            } else {

                            }
//                          console.log("encryptnum--->"+encryptnum);
                        }});
                    

//                     let a= document.createElement('a');
//                     a.target= '_blank';
//                     a.href= '${applicationURL}applyOther?cpNo=" + id + "&imNum=" + encryptnum + utm_param';
//                     a.click();
                  /*  $("#proceedTo").click(function (event) {
                        alert("this is on sbmuit");
                    	$('#proceedTo').attr("href", "${applicationURL}applyOther?cpNo=" + id + "&imNum=" + encryptnum + utm_param);
                        $("#abtYourself").modal('hide');
                        event.stopPropagation();
                    }); */

                    //Adobe code starts here
//                         submitButtonClick(jpNum, 'Earn with Partners: Swipe any of our Co-brand Cards','Proceed to Bank','Page Refresh');
//                        $.fn.AfterApplyJPNumberPopUpClick(jpNum, 'Earn with Partners: Swipe any of our Co-brand Cards');
                    //Adobe code ends here 


                }

            },
        });

        $("#quickEnrollUser").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                enrollTitle: {
                    required: true,
                },
                enrollFname: {
                    required: true,
                    AlphabetsOnly: true
                },

                enrollLname: {
                    required: true,
                    AlphabetsOnly: true
                },

                enrollemail: {
                    required: true,
                    emailValidation: true
                },
                enrollCity: {
                    required: true,
                },
                enrollPhone: {
                    required: true,
                    mobileValidation: true
                },
//    				enrollPassword: {
//					 	required :true,
//					 	passwordChecking:true
//					 },
//					 enrollRenterPassword:{
//					 	required: true,
//					 	equalTo: "#enrollPassword",
//					 	passwordChecking:true
//					},
                enrollDob: {
                    required: true
                },
                termsAndConditionsEnroll: {
                    required: true
                }
            },

            messages: {

                enrollTitle: {
                    required: "Please Select Title",
                },
                enrollFname: {
                    required: "Please Enter FirstName",
                    AlphabetsOnly: "Only Characters from A-Z are allowed",
                },
                enrollLname: {
                    required: "Please Enter LastName",
                    AlphabetsOnly: "Only Characters from A-Z are allowed",
                },
                enrollCity: {
                    required: "Please Enter City Name",
                },
                enrollemail: {
                    required: "Please Enter Email Id",
                    emailValidation: "Please Enter valid Email Id"
                },
//				enrollPassword :{
//			       	required: "Please enter new password",
//			       	passwordChecking : '<ul class="pwd_error"><li>Password must have:</li> <div class="pwd_error_content"><li> 8 - 13 characters.</li><li> at least 1 lowercase alphabet.</li><li> at least 1 uppercase alphabet.</li><li> at least 1 number.</li></div></ul>'
//			    },
//			    enrollRenterPassword:{
//			   		required: "Please Re enter password",
//			   		equalTo: "Passwords do not match",
//			   		passwordChecking : '<ul class="pwd_error"><li>Re Enter Password must have:</li> <div class="pwd_error_content"><li> 8 - 13 characters.</li><li> at least 1 lowercase alphabet.</li><li> at least 1 uppercase alphabet.</li><li> at least 1 number.</li></div></ul>'
//			    },
                enrollPhone: {
                    required: "Please Enter Mobile Number",
                    mobileValidation: "Please Enter Valid Mobile Number"
                },
                enrollDob: {
                    required: "Please Enter Date of Birth",
                },
                termsAndConditionsEnroll: {
                    required: "Please accept the InterMilesPrivilege membership Terms & Conditions to proceed."
                }

            },
            errorElement: "div",
            onfocusout: function (element) {
                this.element(element);  // <- "eager validation"                  
            },
            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },

            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                $('.error_box2').removeClass('hidden');
                error.insertAfter($(element).closest("div"));
                $(error).appendTo('.error_box2 ul.error_list2');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');
            },
            submitHandler: function (form) {
                form.submit();
            },

        });
        $('#enrollForm,#enrollForm_mob_view').on('click', function () {
//        		 var bpNo = $("#otbpNo").val();
            var bpNo = bankNumber;


//                         console.log("onSubmit : "+bankNumber);
            if ($("#quickEnrollUser").validate().element('#enrollTitle') && $("#quickEnrollUser").validate().element('#enrollFname')
                    && $("#quickEnrollUser").validate().element('#enrollLname') && $("#quickEnrollUser").validate().element('#enrollCity')
                    && $("#quickEnrollUser").validate().element('#enrollPhone') && $("#quickEnrollUser").validate().element('#enrollemail')
//    						 && $("#quickEnrollUser").validate().element('#enrollPassword') && $("#quickEnrollUser").validate().element('#enrollRenterPassword')
                    && $("#quickEnrollUser").validate().element('#date_of_birth2') && $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll'))
            {

                var data;
                var responsecode = $('#g-recaptcha-response').val()

//                               console.log("result-bjhbjjjjjjjjjjj--->"+responsecode);

                $.ajax({method: 'POST',
                    url: '${applicationURL}verifycaptcha?responsecode=' + responsecode, success: function (result) {

//                                console.log("result---->"+result);
                        data = result;

                        if (data == false) {

//                            console.log("false data--->")
                            $('.error_box2').removeClass('hidden');
//                            $('#error').html("Please select captcha").appendTo('.error_box1 ul.error_list1');
                            $('.error_box2 ul.error_list2').html("Please select captcha");
                            $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');
                        } else {
//                            console.log("true data-->")
                            $('.error_box2').addClass('hidden');
                            $('.error_box2 ul.error_list2').html("");
//                            var enrollPassword = "";
                            var hashInBase64 = "";
                            var hash = "";

//    			          	enrollPassword = $("#enrollPassword").val();
//    			            hash = CryptoJS.HmacSHA256(enrollPassword, "bff29f3a-90d4-42d5-94f0-448e699991c1");
//    			            hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
//							$("#hashPassword").val(hashInBase64);
//                                                        console.log('verify captcha : '+bpNo);
                            $("#enrollbpNo").val(bpNo);
//                                                        alert('test'+$("#enrollbpNo").val());
                            //password hashing//

//    					  	$('#enroll_error').addClass('hidden');
                            $('#loader').removeClass('hidden');
                            $('.modal-backdrop').css('z-index', '1050');
                            var checkbox = $("#termsAndConditionsEnroll");
                            checkbox.val(checkbox[0].checked ? "true" : "false");
                            var checkbox2 = $("#recieveMarketingTeam");

                            checkbox2.val(checkbox2[0].checked ? "true" : "false");
//                       console.log("1"+$( "#termsAndConditionsEnroll" ).val());
//                       console.log("2"+$( "#recieveMarketingTeam" ).val());

                            $.ajax({
                                url: '${applicationURL}enrollme',
                                data: $('#quickEnrollUser').serialize(),
                                type: 'POST',
                                success: function (data) {
                                    var jsonStr = JSON.parse(data);

                                    if (jsonStr.msg == "" && jsonStr.jpNumber != "" && jsonStr.jpNumber != "null") {
                                        $("#jpNo").text(jsonStr.jpNumber);
                                        $("#jpNumber").val(jsonStr.jpNumber);
                                        tierExist = true;
                                        $('.enroll_popup').addClass('hidden');
                                        $('.jpNum_popup').removeClass('hidden');

                                        //Adobe code starts here
                                        successEnrolClick();
                                        //Adobe code ends here

                                    } else {

                                        $('#loader').addClass('hidden');
                                        $('#enroll_error').removeClass('hidden');
                                        if (jsonStr.msg == "")
                                            $('#enroll_error').text("Your application may not have gone through successfully. Please click 'Continue' again.");
                                        else
                                            $('#enroll_error').text(jsonStr.msg);
                                        $('.modal-backdrop').css('z-index', '1048');
                                    }
                                },
                                complete: function () {

                                    $('#loader').addClass('hidden');
                                    $('.modal-backdrop').css('z-index', '1048');
                                },
                                error: function (xhr, status) {

                                }
                            });
                        }
                    },
                    error: function (e) {
                        console.log("error---->" + e);
                    }
                });

            } else {
                $('.error_box2').removeClass('hidden');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');

            }
        });
//    		 $('#enrollForm,#enrollForm_mob_view').on('click',function(){
//        		 var bpNo = $("#otbpNo").val();
//    				  if($("#quickEnrollUser").validate().element('#enrollTitle') && $("#quickEnrollUser").validate().element('#enrollFname')
//    						 && $("#quickEnrollUser").validate().element('#enrollLname') && $("#quickEnrollUser").validate().element('#enrollCity')
//    						 && $("#quickEnrollUser").validate().element('#enrollPhone') && $("#quickEnrollUser").validate().element('#enrollemail')
//    						 && $("#quickEnrollUser").validate().element('#enrollPassword') && $("#quickEnrollUser").validate().element('#enrollRenterPassword')
//    						 && $("#quickEnrollUser").validate().element('#date_of_birth2') && $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll')) 
//    				{
//                                    var data;
//                                    
//                                    
//                              var responsecode=$('#g-recaptcha-response').val()
//                               console.log("result---->"+responsecode);
//	 		$.ajax({ method : 'POST',
//                                 url:'${applicationURL}verifycaptcha?responsecode='+responsecode, success: function (result) {
//                              
//                                console.log("result---->"+result);
//                                data=result;
//                            
//                        if(data == false){
//                            
//                            console.log("false data--->")
//                             $('.error_box1').removeClass('hidden');
////                            $('#error').html("Please select captcha").appendTo('.error_box1 ul.error_list1');
//                              $('.error_box1 ul.error_list1').html("Please select captcha");
//                               $('.modal').stop(true, false).animate({ scrollTop: 0 }, 'slow');
//                        }else{
//							//password hashing//
//    			          	var enrollPassword = "";
//    			          	var hashInBase64 = "";
//    			          	var hash = "";
//    			          	debugger;
//    			          	enrollPassword = $("#enrollPassword").val();
//    			            hash = CryptoJS.HmacSHA256(enrollPassword, "bff29f3a-90d4-42d5-94f0-448e699991c1");
//    			            hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
//							$("#hashPassword").val(hashInBase64);
//							$("#enrollbpNo").val(bpNo);
//							//password hashing//
//    	    				
//    					  	$('#enroll_error').addClass('hidden');
//			    			$('#loader').removeClass('hidden');
//			    			$('.modal-backdrop').css('z-index','1050');
//    			var checkbox = $( "#termsAndConditionsEnroll" );
//                         checkbox.val(checkbox[0].checked ? "true" : "false" );
//                          var checkbox2 = $( "#recieveMarketingTeam" );
//                         
//                         checkbox2.val(checkbox2[0].checked ? "true" : "false" );
//                       console.log("1"+$( "#termsAndConditionsEnroll" ).val());
//                       console.log("2"+$( "#recieveMarketingTeam" ).val());
//    			       $.ajax({
//    			        url: '${applicationURL}enrollme',
//    			        data: $('#quickEnrollUser').serialize(),
//    			        type: 'POST',
//    			        success: function (data) {
//    			        	var jsonStr = JSON.parse(data);
//    			        	if(jsonStr.msg=="" && jsonStr.jpNumber!=""){
//	    			        	$("#jpNo").text(jsonStr.jpNumber);
//	    			        	$("#jpNumber").val(jsonStr.jpNumber);
//	    			        	tierExist = true;
//	    			        	$('.enroll_popup').addClass('hidden');
//	    			        	$('.jpNum_popup').removeClass('hidden');
//    			        	}
//    			        	else{
//    			        		$('#loader').addClass('hidden');
//    			        		$('#enroll_error').removeClass('hidden');
//    			        		if(jsonStr.msg=="")
//    			        			$('#enroll_error').text("Your application may not have gone through successfully. Please click 'Continue' again.");
//    			        		else
//    			        			$('#enroll_error').text(jsonStr.msg);
//    			        		$('.modal-backdrop').css('z-index','1048');
//    			        	}
//    			        },
//    			        complete: function(){
//    			        	$('#loader').addClass('hidden');
//    			        	$('.modal-backdrop').css('z-index','1048');
//    			          },
//    			        error: function (xhr, status) {
// 
//    			        }
//    			     });  
//    			     
//                             }
//                        },
//                            error: function (e) {
//                         console.log("error---->"+e);
//                    }
//                        });
//    			});

        $('#socialName').keydown(function (event) {
            var charCode = event.keyCode;
            if ((charCode > 64 && charCode < 91) || charCode == 8 || charCode == 9 || charCode == 32) {
                return true;
            } else {
                return false;
            }
        });

        $("#social-profiles").on('click', function () {
            console.log("Inside social profiles");
            var fullName = "";
            var socialName = $("#socialName").val();
            var socialPhone = $("#socialPhone").val();
            var socialEmail = $("#socialEmail").val();
//          
//            var formNumber = ' $ { formNumber}';
//            var cardName='$ {bean.cardName}';
            if ($("#socialName").val() != "") {
                $("#socialName").val(socialName);
            }
            if ($("#socialPhone").val() != "") {
                $("#socialPhone").val(socialPhone);
            }
            if ($("#socialEmail").val() != "") {
                $("#socialEmail").val(socialEmail);
            }
//            $("#socialjpNumber").val(jetpriviligemembershipNumber);
            $("#formNumber").val("");
            $("#pageName").val("Home Page");
            $("#cardName").val("");
//            $('#callMeContine').attr('disabled',true)
            $('#callMe_modal').modal('show');
//            if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
////                console.log("****")
//                $('#callMeContine').attr('disabled',true)
//            }else{
////                console.log("&&&&&&")
//               $('#callMeContine').attr('disabled',false)
//
//            }
        });


        $('#callMeContine').on('click', function () {

            if ($("#callMe").validate().element('#socialName')
                    && $("#callMe").validate().element('#socialPhone')
                    && $("#callMe").validate().element('#socialEmail')
//                    && $("#callMe").validate().element("#socialjpNumber")
                    )
            {
                $('#loader').removeClass('hidden');
                $('#loader').css('height', '100px');
                $('.modal-backdrop').css('z-index', '1050');
                $.ajax({
                    url: '${applicationURL}callme',
                    data: $('#callMe').serialize(),
                    type: 'POST',
                    success: function (data) {
                        if (data != '') {
//                            console.log("data in call me",data);
                            $('#callMe_modal').modal('hide');
                            $('#successCallmeModal').modal('show');

                        } else {
                            $('#loader').addClass('hidden');
                            alert("sorry please try again");
                            $('.modal-backdrop').css('z-index', '1048');
                        }
                        //Adobe code start (phase 6)
                        assistFormSubmitButton($("#socialName").val(), $("#socialPhone").val(), $("#socialEmail").val());
                        //Adobe code end (phase 6)
                    },
                    complete: function () {
                        $('#loader').addClass('hidden');
                        $('.modal-backdrop').css('z-index', '1048');
                        $("#socialName").val('');
                        $("#socialPhone").val('');
                        $("#socialEmail").val('');

                    },
                    error: function (xhr, status) {
                        console.log(status);
                        console.log(xhr.responseText);
                    }
                });
            } else {
                $('.error_box1').removeClass('hidden');

            }
        });


        $("#callMe").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                socialName: {
                    required: true,
                    AlphabetsOnly: true
                },
                socialPhone: {
                    required: true,
                    digits: true,
                    mobileValidation: true
                },
                socialEmail: {
//                    required: true,
                    emailValidation: true
                }
//                socialjpNumber:{
//                digits:true
//                }
            },
            messages: {
                socialName: {
                    required: "Please Enter Full Name",
                    AlphabetsOnly: "Enter Characters Only"
                },
                socialPhone: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid.",
                    mobileValidation: "Please Enter Valid Mobile Number"
                },
                socialEmail: {
//                    required: "Please Enter Email Id",
                    emailValidation: "Email ID is not valid."
                }
//                ,
//                socialjpNumber:{
//                digits:"Please Enter valid Jetprivilege Number."
//                }
            },
            errorElement: "div",
            onfocusout: function (element) {
                this.element(element);  // <- "eager validation"
            },

            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },
            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                $('.error_box1').removeClass('hidden');
                error.insertAfter($(element).closest("div"));
                $(error).appendTo('.error_box1 ul');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');

                //Adobe code starts here(phase 6)
                assistError = assistError + error.text() + ",";
                $.fn.AssistMeError(assistError);
                console.log("Error at validator !!!", assistError);
                //Adobe code ends here (phase 6)
            },
            submitHandler: function (form) {
                form.submit();
            },
        });
        $("#socialName").on('focusout', function () {
            console.log("in focusout socialName")

            $("#callMe").validate().element('#socialName');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }


        });
        $("#socialPhone").on('focusout', function () {
            console.log("in focusout socialPhone")

            $("#callMe").validate().element('#socialPhone');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;

            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }
        });
        $("#socialEmail").on('focusout', function () {
            console.log("in focusout socialEmail")

            $("#callMe").validate().element('#socialEmail');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;

            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }
        });
//         $("#socialName,#socialPhone,#socialEmail").on('focus', function () {
//            $('.error_box1').addClass('hidden');
//            
//        });
//         $("#socialName,#socialPhone,#socialEmail").on('focusout', function () {
//if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
////                console.log("***!!!!*")
//                $('#callMeContine').attr('disabled',true)
//            }else{
////                console.log("&&&!!!!&&&")
//               $('#callMeContine').attr('disabled',false)
//
//            }            
//        });
//        $("#socialName,#socialPhone,#socialEmail").on('input change', function () {
//            if($("#socialName").is(':focus') || $("#socialPhone").is(':focus') || $("#socialEmail").is(':focus')){
//if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
////                console.log("*")
//                $('#callMeContine').attr('disabled',true)
//            }else{
////                console.log("&")
//               $('#callMeContine').attr('disabled',false)
//
//            } 
//        }
//        });
        $('#callmeclose').click(function () {
            $('.error_box1').addClass('hidden');
        });

        $('.proceedPopup_close').click(function () {
            $('.error_box2').addClass('hidden');
        });




//    $("#enrollTitle,#enrollCity,#enrollFname,#enrollLname,#enrollPhone,#enrollemail,#termsAndConditionsEnroll").on('focus', function() {
////            console.log("in focu of enroll")
//            $('.error_box2').addClass('hidden');
////            
//        })
        $("#enrollMname").on('focusout', function () {
            console.log("in focusout enrollDob")

//            $("#quickEnrollUser").validate().element('#enrollDob');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollDob").on('focusout', function () {
            console.log("in focusout enrollDob")

            $("#quickEnrollUser").validate().element('#enrollDob');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $(".ui-datepicker-trigger").click(function () {
            $('.error_box2').addClass('hidden');

        })
        $("#termsAndConditionsEnroll").on('focusout', function () {
            console.log("in focusout termsAndConditionsEnroll")

            $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollemail").on('focusout', function () {
            console.log("in focusout enrollemail")

            $("#quickEnrollUser").validate().element('#enrollemail');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollPhone").on('focusout', function () {
            console.log("in focusout enrollPhone")

            $("#quickEnrollUser").validate().element('#enrollPhone');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollLname").on('focusout', function () {
            console.log("in focusout enrollLname")

            $("#quickEnrollUser").validate().element('#enrollLname');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollTitle").on('blur', function () {
            console.log("in focusout enrolltitle")

            $("#quickEnrollUser").validate().element('#enrollTitle');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollCity").on('focusout', function () {
            console.log("in focusout enrollcity")

            $("#quickEnrollUser").validate().element('#enrollCity');
            $('.error_box2').removeClass('hidden');

            var len = $('.error_list2 > li:visible').length;

            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }
        });
        $("#enrollFname").on('focusout', function () {
            console.log("in focusout enrollFname")

            $("#quickEnrollUser").validate().element('#enrollFname');
            $('.error_box2').removeClass('hidden');

            var len = $('.error_list2 > li:visible').length;

            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }
        });

        $('#date_of_birth2').on('change', function () {
            $("#quickEnrollUser").validate().element('#date_of_birth2');
        });

        $(".title_select").change(function () {
            if ($(this).find(":selected").text() == "Mr") {
                $("#tellMeAbout input[type=radio][value=Male]").prop('checked', true);

            }
            if ($(this).find(":selected").text() == "Ms" || $(this).find(":selected").text() == "Mrs") {
                $("#tellMeAbout input[type=radio][value=Female]").prop('checked', true);
            }
        });
        
//         console.log('before clear');
//        console.clear();
//        
         
        
        console.log('before jp header --------> ',navigator.userAgent == "IM-Mobile-App");
        $(".jp-header-fixed").hasClass("jp-header-fixed");
//        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
//           if(navigator.userAgent === "IM-Mobile-App") {
//            var agentVal = "<h6 class='fixed-top'> Navigator>>>> "+navigator.userAgent+"</h6>";
//            var agentValIM = "<h6 class='fixed-top'> navigator.userAgent.indexOf(IM-Mobile-App)>>>> "+navigator.userAgent.indexOf('IM-Mobile-App')+"</h6>";
           if(navigator.userAgent.indexOf('IM-Mobile-App') > -1){
//            if($(".jp-header-fixed").hasClass("jp-header-fixed") == false){
                    console.log('insdie jp header');
                    $('#wrapper').addClass('mobileagentcss');
                
//            }
        }
        
        
        if (window.matchMedia('(max-width: 767px)').matches) {
        console.log('display none header');
        $('.header_freeze').css('display','none');            
    }
    
   
    window.onload = function () { 
             console.clear(); 
         }
        

    });

    Array.prototype.remove = function () {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };

    //Adobe code filter usage functionality starts here
    function proceedClick() {
        var redirectBank_card = $("#popCardName").text();
        localStorage.setItem("CardName", redirectBank_card);
//         console.log("=====redirectBank_card======="+redirectBank_card)
		$.fn.partnerRedirectionCT();
		$.fn.IndusIndProceedClick();
    }

    $("#tellme").click(function () {
        var jpNum = $("#jpNumber").val();
        var popupName = "Earn with Partners: Swipe any of our Co-brand Cards";
        var formName = "Proceed to Bank";
        var abandonType = "Page Refresh";

        abandonFormName = formName;
        formAbandonType = abandonType;
        $.fn.AfterApplyJPNumberPopUpClickForHdfc(jpNum, popupName);

    });

    function moreDetailsClick(cardName, variant,card,bank) {
        var cardName1 = cardName.replace("®", "");
        var variant1 = variant.replace("®", "");
        $.fn.actionClickCT('More Details',card,bank)
        $.fn.MoreDetailsForCardsClick(cardName1, variant1);
    }

    //submit button assist me form(phase 6)
    function assistMeeClick() {
        assistMeFlag = true;
        $.fn.ClickOfAssistMe();
    }

    function assistFormSubmitButton(name, mobile, email) {
        if (name !== "" && mobile !== "") {
            name = "name:yes";
            mobile = "mobile:yes";
        } else {
            name = "name:no";
            mobile = "mobile:no";
        }
        if (email !== "") {
            email = "email:yes";
        } else {
            email = "email:no";
        }
        $.fn.ClickOfAssistContinueButton(name, mobile, email);
    }



    function adobeAmexClick(cardName, variant, bankName, formName, abandonType, fees) {
    	var cardName1 = cardName.replace("<sup>®</sup>", "");
        var variant1 = variant.replace("<sup>®</sup>", "");

        IsButtonClicked = true;
        popupAbandonType = formName;
        abandonFormName = formName;
        formAbandonType = abandonType;
        
        $.fn.ApplyButtonIndusIndNICICI_CT(cardName1, bankName,fees);
        $.fn.ApplyButtonAmexClick(cardName1, variant1, bankName);
    }

    function adobeAmexAmericanClick(cardName, variant, bankName, formName, abandonType, fees) {
    	var cardName1 = cardName.replace("<sup>®</sup>", "");
        var variant1 = variant.replace("<sup>®</sup>", "");

        IsButtonClicked = false;
        abandonFormName = formName;
        formAbandonType = abandonType;
        
        $.fn.ApplyButtonIndusIndNICICI_CT(cardName1, bankName,fees);
        
        $.fn.ApplyButtonAmexClick(cardName1, variant1, bankName);
    }

    function afterEnrollHereClick(enrollName, popupType) {
        abandonFormName = popupType;
        popupAbandonType = popupType;
        $.fn.AfterApplyEnrolPopUpClick(enrollName);
    }

    function successEnrolClick() {
        var jpNum = $("#jpNumber").val();
        $.fn.successfulQuickEnrollmentOnHdfc(jpNum);

    }

    function noExistingCardHolderClick(existingName, popupType) {
        popupAbandonType = popupType;
        $.fn.existingCreditCardHolderClick(existingName);
    }

    function upgradeOtherCardsClickforNo(upgradeType, popupType) {
        popupAbandonType = popupType;
        $.fn.upgradeOrViewOtherCardsClick(upgradeType);
    }

    function upgradeOtherCardsClickforYes(upgradeType) {
        IsButtonClicked = false;
        $.fn.upgradeOrViewOtherCardsClick(upgradeType);

    }

    function enrollHereBackClick(enrollName, formName) {
        abandonFormName = formName;
        $.fn.EnrolHereBackClick(enrollName);
    }

    function formAbandonmentOnPopUp(closeType) {
        $.fn.formAbandonmentOnSubmitJPnumberPopup(closeType, popupAbandonType);
    }


    function isInArray(array, value)
    {
        return array.indexOf(value) >= 0;

    }

    function  getFinalFilters()
    {
        var lBankNames = "";
        var lLifeStyleBenefits = "";
        var lJoiningFees = "";
        var lFinalFilters = "";
        var joiningFeesAdobe = $(".slider_div .rangeslider__handle").html().replace(/<\/?span[^>]*>/g, "").replace(/\,/g, '');


        if (bankNamesAdobe.length > 0)
            for (i = 0; i < bankNamesAdobe.length; i++) {
                lBankNames = lBankNames + "Bank" + ":" + bankNamesAdobe[i] + "|";
            }


        if (joiningFeesAdobe.length > 0)
            lJoiningFees = "Joining Fees" + ":" + joiningFeesAdobe + "|";

        if (lifestyleBenefitsAdobe.length > 0)
            for (i = 0; i < lifestyleBenefitsAdobe.length; i++) {

                if (i == lifestyleBenefitsAdobe.length - 1)
                {
                    lLifeStyleBenefits = lLifeStyleBenefits + "LifeStyle Benefits" + ":" + lifestyleBenefitsAdobe[i];

                } else
                {
                    lLifeStyleBenefits = lLifeStyleBenefits + "LifeStyle Benefits" + ":" + lifestyleBenefitsAdobe[i] + "|";
                }

            }
        lFinalFilters = lBankNames + lJoiningFees + lLifeStyleBenefits;

        return lFinalFilters;

    }

    function submitFilterAdobe() {

        var lFinalFilters = getFinalFilters();
        //   console.log("final filter is here"+lFinalFilters) 
        $.fn.FilterUsage(lFinalFilters, sortByAdobe);
    }


    //Call to load of all cards
    function loadOfCardsAdobe() {
        var lFinalFilters = getFinalFilters();
        $(".cardinfo").each(function () {
            cardNamesAdobe = cardNamesAdobe + $(this).text() + "|"

        });
        cardsCountAdobe = $(".cardinfo").length;
        $.fn.LoadOfAllCards(cardNamesAdobe, cardsCountAdobe, lFinalFilters, sortByAdobe);
    }


    //Adobe page bottom function call
    function pageInfo()
    {
        //Adobe code phase 6 start
        var callMeFlagAssist = '${callMeFlag}';
        if (callMeFlagAssist == 1) {
            setTimeout(function ()
            {
                $.fn.ViewOfAssistMe()
            }, 3000);
        }
        
        //Adobe code phase 6 ends


                //Addede by Arshad
                //For Plateform
           var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----home--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----home--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----home--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
                        
                        //For pagesource
                   var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----home--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----home--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----home--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
                          //for isWebView
                         var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----home--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----home-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----home--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
                //End 
                



        digitalData.pageInfo = {"pageName": "CardsHome", "category": "Cards", "subCategory1": "Home", "subCategory2": "", "partner": "JP", "currency": "INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
        console.log('Home Page PageInfo is Called...........................');
        // Code to assign JPNumber and DOB dynamically
        $('#jpNumber').val(loggedInUser);
        $('#hjpNumber').val(loggedInUser);
        $('#date_of_birth').val(loggedInUserDob);
        $('#hdate_of_birth').val(loggedInUserDob);

        var popupName = "";
        var lastAccessedField = "";

        $('form input, form select').click(function () {
            lastAccessedField = $(this).attr('name');
        });

        $('#userDetails').on('hidden.bs.modal', function () {
            digitalData.pageInfo.pageName = "CardsHome";
            digitalData.pageInfo.category = "Cards";
            digitalData.pageInfo.subCategory1 = "Home";
            digitalData.pageInfo.subCategory2 = "";
        });

        // Code to assign JPNumber and DOB dynamically(Milestone 1 changes)
        $('#jpNumber').val(loggedInUser);
        $('#hjpNumber').val(loggedInUser);
        $('#date_of_birth').val(loggedInUserDob);
        $('#hdate_of_birth').val(loggedInUserDob);

    }

    function formAbandonOfAssistMe() {
        var abandonType = "pop-up close";
        if (lastAccessedField == "socialName") {
            lastAccessedField = "Name";
        } else if (lastAccessedField == "socialPhone") {
            lastAccessedField = "Mobile";
        } else if (lastAccessedField == "socialEmail") {
            lastAccessedField = "Email";
        }
        $.fn.FormAbandonmentOfAssistMe(name, email, mobile, lastAccessedField, abandonType);
    }



    var checkCloseX = 0;
    $(document).mousemove(function (e) {
        if (e.pageY <= 5) {
            checkCloseX = 1;
        } else
        {
            checkCloseX = 0;
            formAbandonType = "Page Refresh";
        }
    });

    window.onbeforeunload = function (event) {

        if (checkCloseX === 1) {
            formAbandonType = "Browser close";
        }

        if (IsButtonClicked)
        {
            console.log("--------formAbandonType is-----------" + formAbandonType);
            $.fn.formAbandonmentOnSubmitJPnumberPopup(formAbandonType, abandonFormName);
        }

        if (assistMeFlag === true)
        {
            $.fn.FormAbandonmentOfAssistMe(name, mobile, email, lastAccessedField, "page refresh");
        }

    };
    //Form Abandonment close


$("body").on("click",function(e) {
    
//    console.log("e----> ",$(e.target).parent().attr("class")); 
//    console.log("e target -----> ",$(e.target).attr("class"));
    var className = $('#CrdHP_fltr_sort-by').attr('class');
    console.log('className>>'+className);
    console.log('$(e.target).attr(class)>>>',$(e.target).attr("class"));
    
    if($(e.target).attr("class") === "sort_div" || $(e.target).attr("class") === 'sort_by')
    {
       console.log('isnide if diffrendt class');
            $('.sort_listing').removeClass('hidden');
        
    }
    else if($(e.target).attr("class") !== "sort_div"){
    $('.sort_listing').addClass('hidden'); 
        }
    
//    console.log('display before none header');
//    if (window.matchMedia('(max-width: 767px)').matches) {
//    
////            console.log('display none header');
//        $('.header_freeze').css('display','none');            
//    }
    
});
$('.carousel-indicators').hide();

$('.mob-popupClose').on('click', function () {
    $(this).parents('.modal').modal('hide');
});

</script>

