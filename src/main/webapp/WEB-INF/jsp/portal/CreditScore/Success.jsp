<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<!--<div style="margin-top: 6%;">-->
<div class="container"><div class="middletop">
        <div class="powerbySuccess"><span class="powerbyspan"> Powered By</span><span><img src="${applicationURL}static/images/co-brand/4Experian_New Logo_180x61_26.12.16.png"/></span></div>
    </div></div>
    <c:choose>
        
         <c:when test="${not empty date}">
          <div id="preview">
            <div class="canvasCredit">
                <canvas id="canvas-preview"></canvas>
                <h2>Your credit score is <span id="preview-textfield"></span></h2>
                <h5>You have already checked your credit score </h5>
                <h5>Credit score displayed above is as on ${date}  </h5>
                <div class="creditHomeBtn"> <a href="${applicationURL}home"><input id="goToHome" value="Go to Home" class="btnblue" style="margin-top: 40px;" type="button"></a>
                </div>
            </div>
            <div class="imageCredit">
                <img class="rightalign" src="${applicationURL}static/images/co-brand/Jp-Credit-Score_01.png"/>
            </div>
        </div>
    </c:when>

        
        <c:when test="${not empty creditScore}">
        <div id="preview">
            <div class="canvasCredit">
                <canvas id="canvas-preview"></canvas>
                <h2>Your credit score is <span id="preview-textfield"></span></h2>
                <c:if test="${emailIdflag eq 'true'}">
                <span id="class-code-name" class="typ">Detailed credit score report is sent your email id: </span>
                <br><label>${emailId}</label>
                </c:if>
                <!--<br><label>parvati.gholap@vernost.in</label>-->
                <div class="creditHomeBtn"> <a href="${applicationURL}home"><input id="goToHome" value="Go to Home" class="btnblue" style="margin-top: 40px;" type="button"></a>
                </div>
            </div>
            <div class="imageCredit">
                <img class="rightalign" src="${applicationURL}static/images/co-brand/Jp-Credit-Score_01.png"/>
            </div>
        </div>
    </c:when>
    <c:when test="${not empty creditScoreError}">
        <section class="container clearfix bottomPadding">
            <section class="main-section">
                <div class="msgWrapper text-center">
                    <div class="msgApproved_div margT5">
                        <p>${creditScoreError}</p>
                    </div></div>
                    <div class="creditHomeBtn"> <a href="${applicationURL}home"><input id="goToHome" value="Go to Home" class="btnblue" style="margin-top: 40px;" type="button"></a>
                </div>
            </section></section>
        </c:when>
        <c:otherwise>
        <section class="container clearfix bottomPadding">
            <section class="main-section">
                <div class="msgWrapper text-center">
                    <div class="msgApproved_div margT5">
                        <p>We are unable to fetch your credit score as per the details provided by you, please try again using different details.</p>
                    </div></div>
                <div class="creditHomeBtn"> <a href="${applicationURL}home"><input id="goToHome" value="Go to Home" class="btnblue" style="margin-top: 40px;" type="button"></a>
                </div>
            </section></section>
        </c:otherwise>
    </c:choose>
<!--</div>-->
<script>
    $(document).ready(function () {
        console.log("creditScore", '${creditScore}');
        console.log("creditScoreError", '${creditScoreError}');
        console.log("emailIdflag", '${emailIdflag}');
        
//       $.fn.gauge = function(opts) {
//  this.each(function() {
//    var $this = $(this),
//        data = $this.data();
//
//    if (data.gauge) {
//      data.gauge.stop();
//      delete data.gauge;
//    }
//    if (opts !== false) {
//      data.gauge = new Gauge(this).setOptions(opts);
//    }
//  });
//  return this;
//};
//var gauge = new <span id="class-code-name" class="typ">Gauge</span>(target).setOptions(opts); // create sexy gauge!
        if ('${creditScore}' != "") {
            var target = document.getElementById('foo'); // your canvas element
//    document.getElementById("class-code-name").innerHTML = "Gauge";
            demoGauge = new Gauge(document.getElementById("canvas-preview"));
            var opts = {
                angle: 0, // The span of the gauge arc
                lineWidth: 0.2, // The line thickness
                radiusScale: 1, // Relative radius
                pointer: {
                    length: 0.5, // // Relative to gauge radius
                    strokeWidth: 0.05, // The thickness
                    color: '#000000' // Fill color
                },
                limitMax: false, // If false, max value increases automatically if value > maxValue
                limitMin: false, // If true, the min value of the gauge will be fixed
                colorStart: '#6FADCF', // Colors
                colorStop: '#8FC0DA', // just experiment with them
                strokeColor: '#E0E0E0', // to see which ones work best for you
                generateGradient: true,
                highDpiSupport: true, // High resolution support
                staticZones: [
//                    {strokeStyle: "#d83832", min: 0, max: 560}, // Red from 100 to 130
//                    {strokeStyle: "#fe993a", min: 561, max: 720}, // Yellow
//                    {strokeStyle: "#faea35", min: 721, max: 880}, // Green
//                    {strokeStyle: "#62d536", min: 881, max: 960}, // Yellow
//                    {strokeStyle: "#40a549", min: 961, max: 999}  // Red
                    {strokeStyle: "#d83832", min: 300, max: 500}, // Red from 100 to 130
                    {strokeStyle: "#fe993a", min: 501, max: 650}, // Yellow
                    {strokeStyle: "#faea35", min: 651, max: 750}, // Green
                    {strokeStyle: "#62d536", min: 751, max: 850}, // Yellow
                    {strokeStyle: "#40a549", min: 851, max: 999}  // Red
                ],
                staticLabels: {
                    font: "13px sans-serif", // Specifies font
                    labels: [300, 500, 650, 750, 850, 999], // Print labels at these values
                    color: "#000000", // Optional: Label text color
                    fractionDigits: 0  // Optional: Numerical precision. 0=round off.
                },
            };


            demoGauge.setOptions(opts);
            demoGauge.setTextField(document.getElementById("preview-textfield"));
            demoGauge.minValue = 300;
            demoGauge.maxValue = 999;
            demoGauge.set('${creditScore}');
        }
//demoGauge.set(500);
    })

</script>
<script type="text/javascript">
    $(document).ready(function ($) {

        if (window.history && window.history.pushState) {

            $(window).on('popstate', function () {
                var hashLocation = location.hash;
                var hashSplit = hashLocation.split("#!/");
                var hashName = hashSplit[1];

                if (hashName !== '') {
                    var hash = window.location.hash;
                    if (hash === '') {

                        window.location = 'home';

                        return false;
                    }
                }
            });

            window.history.pushState('forward', null, window.location.href);
        }

    });
</script>
<script>
    function pageInfo() {

        //Code changes on 24th april
//        var errorDetails = "";
//        $("input").focusout(function () {
//            errorDetails = "";
//            $('.error').each(function () {
//                if (!$(this).hasClass('hidden'))
//                {
//                    if ($(this).text().length > 0)
//                    {
//                        if (errorDetails.length > 0)
//                        {
//                            errorDetails = errorDetails + ", " + $(this).text();
//                        } else
//                        {
//                            errorDetails = $(this).text();
//                        }
//                    }
//                }
//            })
//
//            // Adobe Code validation error function
//            if (errorDetails.length > 0)
//                $.fn.ErrorTracking(errorDetails, " ");
////              console.log('For Input'+errorDetails)
//        });
    }
</script>