<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<div class="topmain">
    <div>
        <img class="fullwidthimg" src="${applicationURL}static/images/co-brand/Get-Free-Credit-Score-website-banner.jpg"/>
        <!--        <h1 class="headerfont">Check your credit score</h1>
                <p class="headerpara">Receive a free credit score</p>-->
    </div>
</div>

<!--<div>
    <h3 style=" margin-left: 66%;"><b>Powered by</b>
  <img src="static/images/co-brand/4Experian_New Logo_180x61_26.12.16.png" alt="logo" style="margin-left: -9%;">
  </h3>
</div>-->
<div class="container">
    <div class="middletop">
        <!--<div class="powerby"><span class="powerbyspan"> Powered By</span><span><img src="${applicationURL}static/images/co-brand/4Experian_New Logo_180x61_26.12.16.png"/></span></div>-->

        <form:form autocomplete="off" id="creditScoreBean" commandName="creditScoreBean">
            <section class="container bottom_padding">

                <div class="margT1">
                    <div class="row">
                        <div id="namefield">
                            <div class="col-md-6">
                                <div>
                                    <label class="margT1 credit_label topspace">InterMiles No. <span class="mandatory-field">*</span></label>
                                </div>
                                <div>
                                    <form:input maxlength="9"  class="row margT1 input inputheight jpNum" placeholder="InterMiles No." id="JPnumber" path="JPnumber" value="${JPnumber}"/>
                                </div>
                                <%--<c:if test="${empty sessionScope.loggedInUser}">--%>
                                <%--<c:if test="${sessionScope.loggedInUser.size() > 0}">--%>
                                    <div class="enroll-link margT1" id="testing" >To fetch your credit score, please <a class="logincredit" >Login</a>. Not a InterMiles member, please <a class="enroll_here"  id="quickEnroll">Sign-Up</a>.</div>
                                <%--</c:if>--%>
                                    
                                    
                            </div> 
                            <div class="col-md-6">
                                <div class="margT1 name_div" id="div_hide">	
                                    <label class="margT1 credit_label">Full Name<span class="mandatory-field">*</span></label>	
                                    <div class="fullName" id="div_hide1">		  					
                                        <form:input class="row margT1 input name_inputBox_credit inputheight" id="fullName_credit" path = "fullName"  placeholder="Full Name"/>
                                    </div>
                                    <ul class="name_ul_credit margT1 hidden"  id="div_hide2">   
                                        <li><div class="fName"  id="div_hide3"><span>First Name<span class="mandatory-field">*</span></span>
                                                <form:input	class="firstName_credit" maxlength="26" id="fname" path="fname"  name="FNAME" />
                                            </div>
                                        </li>
                                        <li><div class="lName"><span>Last Name<span class="mandatory-field">*</span></span>
                                                <form:input	class="lastName_credit"  maxlength="26" id="lname" path="lname" name="LNAME"  />
                                            </div>
                                        </li>
                                    </ul> 
                                    <!-- 						  </div> -->
                                </div>
                                <div class="fullName-error-div-credit hidden" style="color:red">
                                </div>

                            </div>

                        </div>
                    </div></div>

                <div class="col-md-6 margT1">
                    <div>
                        <label class="margT1 credit_label">E-mail ID<span class="mandatory-field">*</span></label>
                    </div>
                    <div>
                        <form:input	class="row margT1 input inputheight" placeholder="E-mail ID" id="email" maxlength="255" path="emailId" value="${email}"/>
                    </div>
                    <div class="emailHelpText margT1">Communication will be sent to this email id</div>
                </div>
                <div class="col-md-6 margT1">
                    <div>
                        <label class="margT1 credit_label">Mobile Number<span class="mandatory-field">*</span></label>
                    </div>
                    <div>
                        <form:input	class="row margT1 input inputheight" placeholder="Mobile Number" maxlength="10" path="mobileNo" onKeyPress="if(this.value.length==10) return false;" type="number" id="mobile" value="${mobile}"/>
                    </div>
                    <div class="mobileHelpText margT1">Credit score will be fetched using this mobile number</div>

                </div>
                <div class="col-md-12 margT1">
                    <div class="bottomdiv">
                        <div class="col-md-6" >
                            <div  class="bottomdiv">
                                <div class="col-md-4" id="otpBtnDiv">
                                    <div class="buttoncenter">
                                        <input type="button"  id="otpButton" class="genebutton" value=""/>
                                        <label id="labelOTP" class="hidden">Enter OTP</label>
                                        <!--</button>-->
                                    </div>  

                                </div>
                                <div class="col-md-8" id="otpBtnDiv">
                                    <div class="margT2">
                                        <input  class="row margT1 input inputheight" id="otpValue" placeholder="Enter OTP" maxlength="6" type="number" onKeyPress="if (this.value.length == 6)
                                                        return false;" />
                                    </div>
                                    <div class="otpError" id="otpError"></div>    
                                </div>
                            </div>


                        </div>
                                    <div class="regenLink hidden"><a class="regenerateLink">Re-Generate OTP</a></div>

                    </div>
                </div>
                <div class="otpsuccess_credit hidden" id="otpSucces">
                    <p></p>
                </div>
                <div class="otpactive hidden" id="otpactive">
                    <p></p>
                </div>
                <div class="row margT1">
                    <div class="margT1 leftspa">	
                        <label class="margT1 credit_label">Terms & Conditions<span class="mandatory-field">*</span></label>		  					
                        <div>
                            <div class="boxwrapper">
                               <input type="checkbox" value="termsAndConditions" id="termsAndConditions" class="" path="termsAndConditions" /> <label>I agree to <a href="https://www.intermiles.com/terms-and-conditions/cards" target="_blank">InterMiles Terms and Conditions</a> of Program.<label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="margT1 leftspa">	
                        <div>
                            <div class="boxwrapper topperspace">
                               <input type="checkbox" value="creditTAndC" id="creditTAndC" class="margT1 statement credit" path="creditTAndC" /> <label>You hereby consent to InterMiles being appointed as your authorised representative to receive your Credit Information from Experian for the purpose of providing products and services to the Individual Consumers pursuant to the business of InterMiles. <a  target="_blank" href='https://cards.intermiles.com/files/InterMilesT&C.pdf'>T&C</a></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="submit_button">
                    <input class="credit_submit_btn button_blue submit" disabled="disabled" onClick="$('#creditScoreBean').submit()" id="Submitbtn" type="button" value="GET MY CREDIT SCORE" onclick="$.fn.AfterApplyJPNumberPopUpClick(jpNum, 'Earn with Partners: Swipe any of our Co-brand Cards')">
                </div>
            </section>
            <form:hidden path="otpTransactionId" id="otpTransactionId"/>
            <form:hidden path="token" id="token"/>
            <form:hidden path="pendingAttempt" id="pendingAttempt"/>
            <form:hidden path="emailJPID" id="emailJPID"/>
            <form:hidden path="mobileNumberJP" id="mobileNumberJP"/>
            <form:hidden path="mergeJP" id="mergeJP"/>
            <form:hidden path="city" id="city"/>
            <form:hidden path="tier" id="tier"/>
            <form:hidden path="dob" id="dob"/>
            <form:hidden path="Gender" id="Gender"/>

        </form:form>
    </div>
</div>
<div class="container">
    <p class="infopara">What are your credit score benefits?</p>
    <div class="info3">
        <div class="col-md-4 enterdetails">
            <!--<img src="${applicationURL}static/images/co-brand/enterdetails.png" />-->
            <img src="${applicationURL}static/images/co-brand/Check your payment history.png" />

            <h1 class="headline">Check your Payment History</h1>
            <!--<p class="insidedetails">LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT. ALIQUAM FEUGIAT SED LEO LEO EU PORTA</p>-->
        </div>
        <div class="col-md-4 verifydetails beforeafter">
            <!--<img src="${applicationURL}static/images/co-brand/verifydetails.png" />-->
            <img src="${applicationURL}static/images/co-brand/Check-your-Financial-Health.jpg" />
            <h1 class="headline">Check your Financial Health</h1>
            <!--<p class="insidedetails">ADIPISCING ELIT. ALIQUAM FEUGIAT SED LEO EU PORTA.</p>-->
        </div>
        <div class="col-md-4 swipemerchant">
            <!--<img src="${applicationURL}static/images/co-brand/merchants.png" />-->
            <img src="${applicationURL}static/images/co-brand/Apply for eligible cards.png" />

            <h1 class="headline">Eligibility for Cobrand Cards</h1>
            <!--<p class="insidedetails">CONSECTETUR ADIPISCING ELIT. ALIQUAM FEUGIAT SED LEO LEO EU PORTA</p>-->
        </div>
    </div>
</div>
<form:form autocomplete="off" id="quickEnrollUser" action="enrollme" commandName="enrollBean">
    <form:hidden path="enrollbpNo"/>
    <div class="modal fade" id="provide_jpNumber" tabindex=-1 role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="proceedPopup_close pull-right">
                        <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.CloseOnPopUpClick('Pop up Close')">
                    </div>

                    <div class="enroll_popup">
                        <div class="text-center">
                            Enroll into the InterMiles No. Programme now to start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-Brand Card.
                        </div>
                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Enroll Now</h2>
                        </div>
                        <div class="error_box2 hidden">
                            <ul class="error_list2">
                            </ul>
                        </div> 
                        <div class="row">
                            <!--                            <div class="pull-left margT2 nav_web">
                                                            <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_slide go_To_Jp" onclick="$.fn.EnrolHereBackClick('JP Number')">
                                                        </div>-->
                            <div class="enroll_div margT2">
                                <div class="title user_details">
                                    <form:select path="enrollTitle" id="enrollTitle" placeholder="Title" class="title_select">
                                        <form:option value="">Select Title</form:option>
                                        <form:option value="Mr">Mr</form:option>
                                        <form:option value="Ms">Ms</form:option>
                                        <form:option value="Mrs">Mrs</form:option>
                                        <form:option value="Dr">Doctor</form:option>
                                        <form:option value="Prof">Professor</form:option>
                                        <form:option value="Captain">Captain</form:option>
                                    </form:select>
                                </div>
                                <div class="gender hidden">
                                    <c:forEach items="${genderStatus}" var="status">
                                        <div style="display:inline-block">
                                            <label class="radio-inline">
                                                <form:radiobutton
                                                    path="enrollGender" name="enrollgender"
                                                    id="enrollGender"
                                                    value="${status.key}" /> </label>
                                            <label class="gender_desc" for="${status.key}" >${status.value}</label>
                                        </div>
                                    </c:forEach>
                                </div>
                                <div class="firstName_input user_details">
                                    <form:input type="text" path="enrollFname" id="enrollFname" placeholder="First Name" maxlength="50"/>
                                </div>
                                <div class="middleName_input user_details">
                                    <form:input type="text" path="enrollMname" id="enrollMname" placeholder="Middle Name" maxlength="=50"/>
                                </div>
                                <div class="lastName_input user_details">
                                    <form:input type="text" path="enrollLname" id="enrollLname" placeholder="Last Name" maxlength="50"/>
                                </div>
                                <div class="city_input user_details">
                                    <form:input type="text" path="enrollCity" id="enrollCity" placeholder="City"/>
                                </div>
                                <div class="phone_input user_details">
                                    <form:input path="enrollPhone" type="number" id="enrollPhone" maxlength="10" onKeyPress="if(this.value.length==10) return false;" placeholder="Phone"/>
                                </div>
                                <div class="email_input user_details">
                                    <form:input path="enrollemail" id="enrollemail" placeholder="Email" maxlength="255"/>
                                </div>
                                <div class="dob_input user_details">
                                    <form:input path="enrollDob" id="enrollDob" class="datepicker" placeholder="Date Of Birth"/>
                                </div>
                                <div class="g-recaptcha recaptchaarea"
                                     data-sitekey=<spring:message code="google.recaptcha.site"/>  id="captch"></div>
                                <div> 
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="termsAndConditionsEnroll" class="margT1 statement" path="termsAndConditionsEnroll" /> 
                                        <span>I agree to the InterMiles membership 
                                                
                                            <a href="https://www.jetprivilege.com/terms-and-conditions/jetprivilege"  target="_blank">
                                                Terms and Conditions</a><span style="margin: 0px 4px;display: inline;">and</span> <a href="https://www.jetprivilege.com/disclaimer-policy"  target="_blank">
                                                    Privacy Policy</a></span>
                                    </div>
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="recieveMarketingTeam" class="margT1 statement" path="recieveMarketingTeam" />  
                                        <span> Yes, I would like to receive Marketing Communication


                                        </span>
                                    </div>
                                </div>
                                <form:hidden path="enrollApplicationInitiatedBy" id="enrollApplicationInitiatedBy"/>
                                <form:hidden path="enrollAgentId" id="enrollAgentId"/>
                                <div class="error_box_enrolStatus hidden" id="enroll_error">
                                </div>
                            </div>
                        </div>
                        <!--                        <div id="loader" class="text_center hidden margT5">
                                                    <img src="${applicationURL}static/images/header/ajax-loader.gif" alt="Page is loading, please wait." />
                                                </div>-->
                        <div class="text-center margT10 nav_web">
                            <input class="button_thickBlue" type="button"  id="enrollForm1" value="Continue">
                        </div>
                        <div class="nav_arrow_mobile margT10">
                            <div class="left_nav">
                                <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_arrow go_To_Jp">
                            </div>
                            <div class="continue_button">
                                <input class="button_thickBlue" type="button" id="enrollForm_mob_view1" value="Continue">
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <div class="loaddiv"><div class="loaderamex" id="icicidisable" style="/* margin:auto; */"> </div></div>                                   
    </div>
</form:form>


<script>

    $(document).ready(function () {
        
        
        
        
        
        
        
        $(".logincredit").on('click', function () {
            window.MvpHeader.loginToggleModal();

        })
         console.log("======================>"+$("#JPnumber").val())
         var jpnum2=$("#JPnumber").val();
         if(jpnum2.length >=9){
             $('#testing').hide();
         }else{
             
             $('#testing').show();
             
         }

        console.log("jpNumber===at start==>" + $("#JPnumber").val());
        var start = new Date();
        start.setFullYear(start.getFullYear() - 65);
        var end = new Date();
        end.setFullYear(end.getFullYear() - 18);
        $('#enrollDob').datepicker({
            dateFormat: 'dd-mm-yy',
            changeYear: true,
            changeMonth: true,
            showButtonPanel: true,
            // yearRange: '1952:-18',
            //maxDate: 0,
            // defaultDate: '01-01-1952',
//            onClose: function () {
//              ///  alert('Datepicker Closed');
//                if ($('#dateOfBirth').hasClass("ignore")) {
//                    $('#dateOfBirth').removeClass("ignore");
//                }
//               $("#amexBean").validate().element('#dateOfBirth');
//            },
            minDate: start,
            maxDate: end,
            yearRange: start.getFullYear() + ':' + end.getFullYear()
        });
//   $('#enrollDob').datepicker({ changeYear: true,dateFormat: 'dd-mm-yy',
//            changeMonth: true, yearRange: '1945:-18', maxDate: 0, defaultDate: '01-01-1945'});

        var jp_num_ppp1 = '${sessionScope.loggedInUser}';
        var jp_num_ppp = "";
        var otpCounter = 0;
        var myVar;
        var isCorrectOtp = false;
        var nameMsgICICI = "Please ensure your keyword limited to 26 characters";
//        var loggedInJPnumber = $("#jetpriviligemembershipNumber").val();
        var timerId;
        var chkregenerateFlag = false;
        var pendingAttempt;
        var mNumber;
        var time;
        var successMsg;
//        console.log("jpNumber=====>" + jp_num_ppp1.substring(2));
//        var jpNumber = jp_num_ppp1.substring(2);
//        $("#JPnumber").val(jpNumber);
        $('.name_inputBox_credit').attr('disabled', true);
        $("#JPnumber").attr('disabled', true);
        $("#otpValue").attr('disabled', true);
        $("#otpButton").attr('disabled', true);
        $("#mobile").attr('disabled', true);
        $("#email").attr('disabled', true);
        $('#otpButton').val('Generate OTP');
        $('#otpButton').attr('disabled', true);
        $(".enroll_here").css({'color': '#337ab7', 'cursor': 'pointer', 'text-decoration-line': 'none'});
        $(".logincredit").css({'color': '#337ab7', 'cursor': 'pointer', 'text-decoration-line': 'none'});
        $('#icicidisable').hide();
//        $('.otpsuccess_credit').hide();
//        $(".otpactive").hide();
        $(".otpError").hide();

        if (jp_num_ppp1 != "") {
//            $(".enroll-link margT1").addClass("hidden");

            $("#mobile").attr('disabled', false);
            $("#email").attr('disabled', false);
            $("#otpButton").attr('disabled', false);
        } else {
//            $(".enroll-link margT1").removeClass("hidden");

            $("#mobile").attr('disabled', true);
            $("#email").attr('disabled', true);
        }
        $('.lastName_credit').on("focus", function () {
            $('.fullName-error-div-credit').addClass('hidden');
        });

        $(document).on('focus', '.name_inputBox_credit', function () {
            $('.name_ul_credit').removeClass('hidden');
            $('.name_inputBox_credit').addClass('hidden');
            $('.firstName_credit').focus();
            $('#fullName_credit-error').addClass('hidden');
        });

        $('.firstName_credit').focusout(function () {
            $('#fullName_credit-error').addClass('hidden');
            var fName = $('.firstName_credit').val();
            var lName = $('.lastName_credit').val();
            var fNameLength = fName.length;
            var appendQuery = fNameLength >= 27 ? "First Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (fName != "" && fNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.firstName_credit').val(fName.substring(0, 26));
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div-credit').removeClass('hidden');
                    //$('#testing').show();
                } else {
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div-credit').removeClass('hidden');
                   // $('#testing').hide();
                }

            } else {
                $('.fullName-error-div-credit').html('');
                $('.fullName-error-div-credit').addClass('hidden');
            }

        });

        $('.firstName_credit').on('click', function () {
            $('#fullName_credit-error').addClass('hidden');
            var fName = $('.firstName_credit').val();
            var lName = $('.lastName_credit').val();
            var fNameLength = fName.length;
            var appendQuery = fNameLength >= 27 ? "First Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if ((fName != "" && fNameLength >= 27)) {
                if (loggedInJPnumber == "") {
                    $('.firstName_credit').val(fName.substring(0, 26));
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div-credit').removeClass('hidden');
                } else {
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div-credit').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div-credit').html('');
                $('.fullName-error-div-credit').addClass('hidden');
            }
        });

        $('.firstName_credit').on('keyup', function () {
            var fName = $('.firstName_credit').val();
            var lName = $('.lastName_credit').val();
            $('.fullName-error-div-credit').addClass('hidden');
            var fNameLength = fName.length;
            var appendQuery = fNameLength >= 27 ? "First Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if ((fName != "" && fNameLength >= 27)) {
                if (loggedInJPnumber == "") {
                    $('.firstName_credit').val(fName.substring(0, 26));
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div-credit').removeClass('hidden');
                } else {
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div-credit').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div-credit').html('');
                $('.fullName-error-div-credit').addClass('hidden');
            }
        });

        $('.lastName_credit').focusout(function () {
            var fName = $('.firstName_credit').val();
            var lName = $('.lastName_credit').val();
            var lNameLength = lName.length;
            $('.fullName-error-div-credit').addClass('hidden');
            var appendQuery = lNameLength >= 27 ? "Last Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (lName != "" && lNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.lastName_credit').val(lName.substring(0, 26));
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div-credit').removeClass('hidden');
                } else {
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div-credit').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div-credit').html('');
                $('.fullName-error-div-credit').addClass('hidden');
            }
        });

        $('.lastName_credit').on('click', function () {
            var fName = $('.firstName_credit').val();
            var lName = $('.lastName_credit').val();
            var lNameLength = lName.length;
            $('.fullName-error-div-credit').addClass('hidden');
            var appendQuery = lNameLength >= 27 ? "Last Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (lName != "" && lNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.lastName_credit').val(lName.substring(0, 26));
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div-credit').removeClass('hidden');
                } else {
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div-credit').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div-credit').html('');
                $('.fullName-error-div-credit').addClass('hidden');
            }
        });

        $('.lastName_credit').on('keyup', function () {
            var fName = $('.firstName_credit').val();
            var lName = $('.lastName_credit').val();
            var lNameLength = lName.length;
            $('.fullName-error-div-credit').addClass('hidden');
            var appendQuery = lNameLength >= 27 ? "Last Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (lName != "" && lNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.lastName_credit').val(lName.substring(0, 26));
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div-credit').removeClass('hidden');
                } else {
                    $('.fullName-error-div-credit').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div-credit').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div-credit').html('');
                $('.fullName-error-div-credit').addClass('hidden');
            }
        });

        $('.name_ul_credit').focusout(function () {
            var fName = $('.firstName_credit').val();
            var lName = $('.lastName_credit').val();
            $('#fullName_credit-error').addClass('hidden');
            $("#fname-error").addClass('hidden');
            var fullname = fName + lName;
            var checkName = hasalphabet(fullname);
            if (fullname != "" && checkName == false) {
                $('.fullName-error-div-credit').html("Please Enter Alphabets only.").css("color", "#ff0000");
                $('.fullName-error-div-credit').removeClass('hidden');
            } else {
                $('.fullName-error-div-credit').html('');
                $('.fullName-error-div-credit').addClass('hidden');
            }
            if (fName == "") {
                $('.fullName-error-div-credit').html('Please Enter Full Name').css("color", "#ff0000");
                $('.fullName-error-div-credit').removeClass('hidden');
            } else if (fName == "") {
                $('.fullName-error-div-credit').html('Please Enter first Name');
                $('.fullName-error-div-credit').removeClass('hidden');
            } else if (lName == "") {
                $('.fullName-error-div-credit').html('Please Enter Full Name').css("color", "#ff0000");
                $('.fullName-error-div-credit').removeClass('hidden');
            }
        });

        $("#fullName_credit").click(function () {
            $('.name_ul_credit').removeClass('hidden');
            $('.name_inputBox_credit').addClass('hidden');
        });

        $(document).mouseup(function (e) {
            var container = $('.name_ul_credit');
            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                var fName = $('.firstName_credit').val();
                var lName = $('.lastName_credit').val();
                $('.name_inputBox_credit').val(fName + " " + lName);
                $('.name_ul_credit').addClass('hidden');
                $('.name_inputBox_credit').removeClass('hidden');
                if (fName == "" && lName == "")
                    $('.name_inputBox_credit').val('');
            }
        });

        console.log("jp_num_ppp1========", jp_num_ppp1, "====jp_num_ppp===", jp_num_ppp);

        $("#JPnumber").on('focusout, change', function () {
            console.log("JPnumber focusout");
            if ($("#JPnumber").val().length == 9 && $("#JPnumber-error").text() == "") {
                $("#mobile").attr('disabled', false);
                $("#email").attr('disabled', false);
            } else {
                $("#mobile").attr('disabled', true);
                $("#email").attr('disabled', true);
            }
        });
        $("#mobile").on('input change', function () {
            $("#mobile").trigger('focusout');
        })
        $("#mobile").on('keyup', function () {
            if ($("#mobile").val().length == 10) {
                $("#mobile").trigger('focusout');
            }
        })
        $("#mobile").on('focusout', function () {
            console.log("mobile focusout");

            if ($("#mobile").val().length == 10 && $("#mobile-error").text() == "") {
                console.log("inside disable otp button")
                $("#otpValue").val("");
                $("#otpValue").attr("disabled", true);
                $("#otpButton").attr('disabled', false);
                $("#Submitbtn").attr('disabled', true);
                $("#otpSucces").addClass('hidden');
                $("#otpactive").addClass('hidden');
                $("#otpButton").val('Generate OTP')
                stopCountDown();
                otpCounter = 0;

            } else {
                console.log("inside else disable otp button")
                $("#otpValue").attr("disabled", true);

                $("#otpButton").attr('disabled', true);
                $("#otpSucces").addClass('hidden');
                $("#otpactive").addClass('hidden');
                $("#otpValue").val("");
                $("#Submitbtn").attr('disabled', true);
                $("#otpButton").val('Generate OTP')

                stopCountDown();
                otpCounter = 0;
            }
        });
        $(".regenerateLink").click(function () {
            var mobileNumber = $('#mobile').val();
            var formNumber = '${formNumber}';
            if (mobileNumber != "" && mobileNumber.length == 10) {
                if (otpCounter > 3) {
                    $('#otpValue').attr('disabled', true);
                    $('#otpValue').val("");
                    $("#otpError").html("");
                    $("#otpError").hide();
                    $('#otpSucces').html("Please Check Mobile Number.");
                    $('#otpSucces').removeClass('hidden');
                    $('#otpButton').attr('disabled', true);
                    $(".otpButton").addClass("hidden");
                    var imageUrl = ctx + "static/images/icons/error_icon.png";
                    $(".otpsuccess_credit").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
                } else {
                    $.ajax({url: "${applicationURL}regenerateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&token=" + $("#token").val() + "&otpTransactionId=" + $("#otpTransactionId").val(),
                        async: false,
                        success:
                                function (data) {
                                    stopTimeout();
                                    startTimeout();
                                    var myObj = JSON.parse(data);
                                    mNumber = myObj.mobileNumber;
                                    time = myObj.Time;
                                    var otpTransactionId = myObj.otpTransactionid;
                                    var token = myObj.token;
                                    console.log("token-reger-->", token);
                                    $("#pendingAttempt").val("");
                                    if ($("#pendingAttempt").val() != "") {
                                        successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                    } else {
                                        successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                    }
                                    if (mNumber != "" && time != "") {
                                        var chkregenerateFlag = true;
//                                            var successMsg;
                                        $("#otpTransactionId").val(otpTransactionId);
                                        $("#token").val(token);
                                        var timeLeft = 30;
                                        timerId = setInterval(countdown, 1000);
                                        var activemsg;
                                        function countdown() {
                                            if (timeLeft == -1) {
                                                clearTimeout(timerId);
                                                $('#otpactive').addClass('hidden');
                                                $('#otpactive').html('');
                                                $('#otpButton').attr('disabled', false);
                                                $(".regenLink").removeClass("hidden");
                                            } else {
                                                activemsg = 'In case you have not received OTP please try regenerating OTP after ' + timeLeft + ' seconds ';
                                                $('#otpactive').removeClass('hidden');
                                                $('#otpactive').html(activemsg);
                                                $('#otpButton').attr('disabled', true);
                                                timeLeft--;
                                                $(".regenLink").addClass("hidden");
                                            }
                                        }
                                        $("#otpValue").attr('disabled', false);
                                        $('#otpButton').attr('disabled', false);
//                                            $('#otpButton').val('Re-Generate OTP');


                                        $('#otpSucces').html(successMsg);
                                        $('#otpSucces').removeClass('hidden');
                                        var imageUrl = ctx + "static/images/icons/success_icon.png";
                                        $(".otpsuccess_credit").css({"border": "2px solid #7dd62e", "background": "#f2fbe2 url(" + imageUrl + ") no-repeat 5px"});

                                    } else {
                                        otpCounter = 0;
                                    }
                                }
                    });
                }
            } else {
                $("#otpButton").attr('disabled', true);
            }

        });
        $("#otpButton").click(function () {
            console.log("pendingAttempt----1----", pendingAttempt);
            var mobileNumber = $('#mobile').val();
            var formNumber = '${formNumber}';
            if (mobileNumber != "" && mobileNumber.length == 10) {
                otpCounter++;
                console.log("otpCounter--", otpCounter)
                if (otpCounter > 3) {
                    $('#otpValue').attr('disabled', true);
                    $('#otpValue').val("");
                    $("#otpError").html("");
                    $("#otpError").hide();
                    $('#otpSucces').html("Please Check Mobile Number.");
                    $('#otpSucces').removeClass('hidden');
                    $('#otpButton').attr('disabled', true);
                    $(".otpButton").addClass("hidden");
                    var imageUrl = ctx + "static/images/icons/error_icon.png";
                    $(".otpsuccess_credit").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
                } else {
                    if ($('#otpButton').val() == 'Re-Generate OTP') {
                        $.ajax({url: "${applicationURL}regenerateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&token=" + $("#token").val() + "&otpTransactionId=" + $("#otpTransactionId").val(),
                            async: false,
                            success:
                                    function (data) {
                                        stopTimeout();
                                        startTimeout();
                                        var myObj = JSON.parse(data);
                                        mNumber = myObj.mobileNumber;
                                        time = myObj.Time;
                                        var otpTransactionId = myObj.otpTransactionid;
                                        var token = myObj.token;
                                        console.log("token-reger-->", token);
                                        $("#pendingAttempt").val("");
                                        if ($("#pendingAttempt").val() != "") {
                                            successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                        } else {
                                            successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                        }
                                        if (mNumber != "" && time != "") {
                                            var chkregenerateFlag = true;
//                                            var successMsg;
                                            $("#otpTransactionId").val(otpTransactionId);
                                            $("#token").val(token);
                                            var timeLeft = 30;
                                            timerId = setInterval(countdown, 1000);
                                            var activemsg;
                                            function countdown() {
                                                if (timeLeft == -1) {
                                                    clearTimeout(timerId);
                                                    $('#otpactive').addClass('hidden');
                                                    $('#otpactive').html('');
                                                    $('#otpButton').attr('disabled', false);
                                                } else {
                                                    activemsg = 'In case you have not received OTP please try regenerating OTP after ' + timeLeft + ' seconds ';
                                                    $('#otpactive').removeClass('hidden');
                                                    $('#otpactive').html(activemsg);
                                                    $('#otpButton').attr('disabled', true);
                                                    timeLeft--;
                                                }
                                            }
                                            $("#otpValue").attr('disabled', false);
                                            $('#otpButton').attr('disabled', false);
                                            $('#otpButton').val('Re-Generate OTP');
                                            $('#otpSucces').html(successMsg);
                                            $('#otpSucces').removeClass('hidden');
                                            var imageUrl = ctx + "static/images/icons/success_icon.png";
                                            $(".otpsuccess_credit").css({"border": "2px solid #7dd62e", "background": "#f2fbe2 url(" + imageUrl + ") no-repeat 5px"});

                                        } else {
                                            otpCounter = 0;
                                        }
                                    }
                        });
                    }//if regenerate
                    else {
                        $.ajax({url: "${applicationURL}generateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber,
                            async: false,
                            success:
                                    function (data) {
                                        stopTimeout();
                                        startTimeout();
                                        var myObj = JSON.parse(data);
                                        mNumber = myObj.mobileNumber;
                                        time = myObj.Time;
                                        var otpTransactionId = myObj.otpTransactionid;
                                        var token = myObj.token;
                                        console.log("token--->", token);
                                        $("#pendingAttempt").val("");
                                        if ($("#pendingAttempt").val() != "") {
                                            successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left' + $("#pendingAttempt").val();

                                        } else {
                                            successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

                                        }
                                        if (mNumber != "" && time != "") {
                                            var chkregenerateFlag = false;
//                                            var successMsg;
                                            $("#otpTransactionId").val(otpTransactionId);
                                            $("#token").val(token);
                                            $('#otpButton').hide();
                                            var timeLeft = 30;
                                            timerId = setInterval(countdown, 1000);
                                            var activemsg;
                                            function countdown() {
                                                if (timeLeft == -1) {
                                                    clearTimeout(timerId);
                                                    $('#otpactive').addClass('hidden');
                                                    $('#otpactive').html('');
                                                    $('#otpButton').attr('disabled', false);
                                                    $(".regenLink").removeClass("hidden");
                                                } else {
                                                    activemsg = 'In case you have not received OTP please try regenerating OTP after ' + timeLeft + ' seconds ';
                                                    $('#otpactive').removeClass('hidden');
                                                    $('#otpactive').html(activemsg);
                                                    $('#otpButton').attr('disabled', true);
                                                    timeLeft--;
                                                    $(".regenLink").addClass("hidden");
                                                }
                                            }
                                            $("#otpValue").attr('disabled', false);
                                            $('#otpButton').attr('disabled', false);
                                            $('#labelOTP').removeClass('hidden');

//                                            $('#otpButton').val('Re-Generate OTP');

                                            $('#otpSucces').html(successMsg);
                                            $('#otpSucces').removeClass('hidden');
                                            var imageUrl = ctx + "static/images/icons/success_icon.png";
                                            $(".otpsuccess_credit").css({"border": "2px solid #7dd62e", "background": "#f2fbe2 url(" + imageUrl + ") no-repeat 5px"});

                                        } else {
                                            otpCounter = 0;
                                        }
                                    }
                        });
                    }
                }
            } else {
                $("#otpButton").attr('disabled', true);
            }
        });

//        $("#otpValue").focusout(function () {
//            var otpValue = $("#otpValue").val();
//            console.log("otpValue===", otpValue)
//            if (otpValue == "") {
//                console.log("1");
//                $("#otpError").html("Please Enter OTP");
//                $("#otpError").show();
//                $("#Submitbtn").attr('disabled', true);
//                isCorrectOtp = false;
//            } else if (otpValue.length != 6) {
//                console.log("2");
//                $("#otpError").html("Please Enter OTP");
//                $("#otpError").show();
//                $("#Submitbtn").attr('disabled', true);
//                isCorrectOtp = false;
//            } else {
//                if (isNaN(otpValue) == true) {
//                    console.log("3");
//                    $("#otpError").html("Please Enter OTP");
//                    $("#otpError").show();
//                    $("#Submitbtn").attr('disabled', true);
//                    isCorrectOtp = false;
//                } else {
//                    var formNumber = '${formNumber}';
//                    var mobileNumber = $('#mobile').val();
//                    var otpTransactionId = $("#otpTransactionId").val();
//                    $.ajax({method: "GET",
//                        async: false,
//                        url: "${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&otpTransactionId=" + otpTransactionId + "&otp=" + otpValue + "&token=" + $("#token").val(), success: function (data) {
//                            console.log("=========validate======", data);
//                            if (data.length > 2) {
//                                var myObj = JSON.parse(data);
//
//                                pendingAttempt = myObj.pendingAttempts;
//                                $("#pendingAttempt").val(myObj.pendingAttempts);
//                                console.log("pendingAttempt----1----", pendingAttempt);
//                                if ($("#pendingAttempt").val() == "Number of attempts exceeds limit") {
//                                    $('#otpValue').attr('disabled', true);
//                                    $('#otpValue').val("");
//                                    $("#otpError").html("");
//                                    $("#otpError").hide();
//                                    successMsg = "Left attempts are exceeded";
////                                    $('#otpSucces').html("Number of attempts exceeds limit");
//                                    $('#otpSucces').removeClass('hidden');
//                                    $('#otpButton').attr('disabled', true);
//                                    $(".otpButton").addClass("hidden");
//                                    var imageUrl = ctx + "static/images/icons/error_icon.png";
//                                    $(".otpsuccess_credit").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
//                                } else if ($("#pendingAttempt").val() != "") {
//                                    console.log("=-=-=p-==-=-=>", $("#pendingAttempt").val());
//                                    successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left ' + $("#pendingAttempt").val();
//
//                                } else {
//                                    successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';
//
//                                }
//                                $('#otpSucces').html(successMsg);
//
//                                if (myObj.OTP_Verified == "true") {
//
//
//
//                                    $("#otpError").html("");
//                                    $("#otpError").hide();
//                                    $("#Submitbtn").attr('disabled', true);
////                                    $("#Submitbtn").removeAttr('disabled');
//                                    isCorrectOtp = true;
////                                    $('#otpVal').val(otpValue);
//                                } else {
//                                    $("#otpError").html("Please Enter Valid OTP");
//                                    $("#otpError").show();
//                                    $("#Submitbtn").attr('disabled', true);
//                                    isCorrectOtp = false;
////                                    $('#otpVal').val('');
//                                }
//                            } else {
//                                $("#otpError").html("Please Enter Valid OTP");
//                                $("#otpError").removeClass("hidden");
//                                $("#Submitbtn").attr('disabled', true);
//                                isCorrectOtp = false;
//                            }
//
//                        }});
//                }
//            }
//        });

//         $("#otpValue").keyup(function () {
//             if ($("#otpValue").val().length == 6) {
//                 var otpValue = $("#otpValue").val();
//                 console.log("otpValue===", otpValue)
//                 if (otpValue == "") {
//                     console.log("1");
//                     $("#otpError").html("Please Enter OTP");
//                     $("#otpError").show();
//                     $("#Submitbtn").attr('disabled', true);
//                     isCorrectOtp = false;
//                 } else if (otpValue.length != 6) {
//                     console.log("2");
//                     $("#otpError").html("Please Enter OTP");
//                     $("#otpError").show();
//                     $("#Submitbtn").attr('disabled', true);
//                     isCorrectOtp = false;
//                 } else {
//                     if (isNaN(otpValue) == true) {
//                         console.log("3");
//                         $("#otpError").html("Please Enter OTP");
//                         $("#otpError").show();
//                         $("#Submitbtn").attr('disabled', true);
//                         isCorrectOtp = false;
//                     } else {
//                         var formNumber = '${formNumber}';
//                         var mobileNumber = $('#mobile').val();
//                         var otpTransactionId = $("#otpTransactionId").val();
//                         $.ajax({method: "GET",
//                             async: false,
//                             url: "${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&otpTransactionId=" + otpTransactionId + "&otp=" + otpValue + "&token=" + $("#token").val(), success: function (data) {
//                                 console.log("=========validate======", data);
//                                 if (data.length > 2) {
//                                     var myObj = JSON.parse(data);

//                                     pendingAttempt = myObj.pendingAttempts;
//                                     $("#pendingAttempt").val(myObj.pendingAttempts);
//                                     console.log("pendingAttempt----1----", pendingAttempt);
//                                     if ($("#pendingAttempt").val() == "Number of attempts exceeds limit") {
//                                         $('#otpValue').attr('disabled', true);
//                                         $('#otpValue').val("");
//                                         $("#otpError").html("");
//                                         $("#otpError").hide();
//                                         successMsg = "Left attempts are exceeded";
// //                                    $('#otpSucces').html("Number of attempts exceeds limit");
//                                         $('#otpSucces').removeClass('hidden');
//                                         $('#otpButton').attr('disabled', true);
//                                         $(".otpButton").addClass("hidden");
//                                         var imageUrl = ctx + "static/images/icons/error_icon.png";
//                                         $(".otpsuccess_credit").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
//                                     } else if ($("#pendingAttempt").val() != "") {
//                                         console.log("=-=-=p-==-=-=>", $("#pendingAttempt").val());
//                                         successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left ' + $("#pendingAttempt").val();

//                                     } else {
//                                         successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';

//                                     }
//                                     $('#otpSucces').html(successMsg);

//                                     if (myObj.OTP_Verified == "true") {



//                                         $("#otpError").html("");
//                                         $("#otpError").hide();
//                                         $("#Submitbtn").attr('disabled', true);
// //                                    $("#Submitbtn").removeAttr('disabled');
//                                         isCorrectOtp = true;
// //                                    $('#otpVal').val(otpValue);
//                                     } else {
//                                         $("#otpError").html("Please Enter Valid OTP");
//                                         $("#otpError").show();
//                                         $("#Submitbtn").attr('disabled', true);
//                                         isCorrectOtp = false;
// //                                    $('#otpVal').val('');
//                                     }
//                                 } else {
//                                     $("#otpError").html("Please Enter Valid OTP");
//                                     $("#otpError").removeClass("hidden");
//                                     $("#Submitbtn").attr('disabled', true);
//                                     isCorrectOtp = false;
//                                 }

//                             }});
//                     }
//                 }
//             } else {
//                 isCorrectOtp = false;
//             }
//         });
        
        $("#fname,#lname,#otpValue,#JPnumber,#mobile,#email,#termsAndConditions,#creditTAndC").on('keyup', function () {
			console.log("-------focusout key up--->", $("#termsAndConditions").is(':checked'));
			//console.log('$(#fname).val()--->'+$("#fname").val());
			//console.log('$(#lname).val()---->'+$("#lname").val());
			console.log('$(#otpValue).val()---->'+$("#otpValue").val());
			console.log('isCorrectOtp---->'+isCorrectOtp);
			console.log('$(#JPnumber).val()----->'+$("#JPnumber").val());
			//console.log('$(#email).val()----->'+$("#email").val());
			//console.log('$(#mobile).val()----->'+$("#mobile").val());
			//console.log('$(#mobile).val()----->'+$("#mobile").val());
			console.log('$(#termsAndConditions).is(:checked)----->'+$("#termsAndConditions").is(':checked'));
			console.log('$(#creditTAndC).is(:checked)----->'+$("#creditTAndC").is(':checked'));
			
			if ($("#fname").val() != "" && $("#lname").val() != "" && $("#otpValue").val() != ""
			&& $("#JPnumber").val() != "" && $("#email").val() != "" && $("#mobile").val() != "" && $("#termsAndConditions").is(':checked') == true && $("#creditTAndC").is(':checked') == true) {
			$("#Submitbtn").removeAttr('disabled');
			console.log('remvove disable attribute key up');
			} else {
			$("#Submitbtn").attr('disabled', true);
			console.log('not remvove disable attribute key up');
			
			}
			
		});
        
        $("#fname,#lname,#otpValue,#JPnumber,#mobile,#email,#termsAndConditions,#creditTAndC").on('click', function () {
            console.log("-------focusout click--->", $("#termsAndConditions").is(':checked'));         
            //console.log('$(#fname).val()--->'+$("#fname").val());
            //console.log('$(#lname).val()---->'+$("#lname").val());
            console.log('$(#otpValue).val()---->'+$("#otpValue").val());
            console.log('isCorrectOtp---->'+isCorrectOtp);
            console.log('$(#JPnumber).val()----->'+$("#JPnumber").val());
            //console.log('$(#email).val()----->'+$("#email").val());
            //console.log('$(#mobile).val()----->'+$("#mobile").val());
            
            console.log('$(#termsAndConditions).is(:checked)----->'+$("#termsAndConditions").is(':checked'));
            console.log('$(#creditTAndC).is(:checked)----->'+$("#creditTAndC").is(':checked'));
            if ($("#fname").val() != "" && $("#lname").val() != "" && $("#otpValue").val() != ""
            	&& $("#JPnumber").val() != "" && $("#email").val() != "" && $("#mobile").val() != "" && $("#termsAndConditions").is(':checked') == true && $("#creditTAndC").is(':checked') == true) {
            	$("#Submitbtn").removeAttr('disabled');
            	console.log('remvove disable attribute click');
           } else {
            	$("#Submitbtn").attr('disabled', true);
            	console.log('not remvove disable attribute click');
            }

        });

//        $('.credit_submit_btn').click(function () {
//            console.log('Clicked');
//            $(this).data('clicked', true);
//        });
        $("#quickEnroll").on('click', function () {
//            $("#provide_jpNumber").modal('show');
//            callenroll();
            var url = 'https://www.intermiles.com/rewards-program/enrol';
            window.open(url, '_blank');
        });
        $("#mobile, #enrollPhone").on('keyup', function (evt) {
            console.log("inside keyup")
            if (evt.which == 8) {
            } else if ($(this).val().length >= 10) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
                }
            }
        });
        $("#JPnumber, #otpValue, #enrollPhone, #mobile").on("keydown, keypress", function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;
            }
        });



        $.validator.addMethod("AlphabetsWithDotOnly", function (value, element) {
            console.log("inside validation");
            return this.optional(element) || /^[a-z][a-z.\s]*$/i.test(value);
        });
        $.validator.addMethod("checkMod7Logic", function (value, element) {
            var jpnumber = $("#JPnumber").val();
            var chk = checkMod7Logic(jpnumber);
            if (chk) {
                return false;
            } else {
                return true;
            }
        });

        $.validator.addMethod("validCrisJPNumber", function (value, element) {
            var jpnumber = $("#JPnumber").val();
            var chkCris = checkValidCrisJPNumber(jpnumber);
            console.log("chk", chkCris)
            if (chkCris) {
                return false;
            } else {
                return true;
            }
        });

        $.validator.addMethod("AlphabetsOnly", function (value, element) {
            return this.optional(element) || /^[a-z][a-z@\s]*$/i.test(value);
        });

        function startTimeout() {
            myVar = setTimeout(function () {
                $("#otpValue").val('');
                $("#otpValue").attr('disabled', true);
            }, 900000);
        }

        function stopTimeout() {
            clearTimeout(myVar);
        }
        function stopCountDown() {
            clearInterval(timerId);
        }
        function checkValidCrisJPNumber(jpnumber) {
            var chk;
            $.ajax({url: "${applicationURL}getTier?jpNum=" + jpnumber,
                async: false,
                success:
                        function (data) {
                            if (data.length > 2) {
                                console.log("data", data)
                                chk = false;
                            } else {
                                chk = true;
                            }
                        }
            });
            return chk;
        }

        function checkMod7Logic(jpnumber) {
            if (jpnumber != "") {
                var jpNumLastDig = jpnumber % 10;
                var jpNoTemp = eval(jpnumber.slice(0, -1)) % 7;
                if (jpNumLastDig !== jpNoTemp)
                {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
        function hasalphabet(str) {
            return /^[a-zA-Z()]+$/.test(str);
        }
        $("#creditScoreBean").validate({
            ignore: ".ignore,:disabled,:hidden",
            rules: {
                fullName: {
                    required: true,
//                    checkFullName: true
//                    nameValidation:true
                },
                emailId: {
                    required: true,
                    emailValidation: true
                },
                mobileNo: {
                    required: true,
                    digits: true,
                    mobileValidation: true,
                    maxlength: 10
                },
                termsAndConditions: {
                    required: true
                },
                JPnumber: {
                    required: true,
                    digits: true,
                    checkMod7Logic: true,
                    validCrisJPNumber: true

                },
                creditTAndC: {
                    required: true
                }
            },
            messages: {
                fullName: {
                    required: "Please Enter Full Name",
//                    checkFullName: ""
//                    nameValidation: "Name can have only letters"
                },
                emailId: {
                    required: "Please Enter E-mail ID",
                    emailValidation: "E-mail ID is not valid"
                },
                mobileNo: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid",
                    mobileValidation: "Mobile Number is not valid",
                    maxlength: "Please ensure mobile number is limited to 10 digits"
                },
                termsAndConditions: {
                    required: "Please select Terms and Conditions"
                },
                JPnumber: {
                    required: "Please enter InterMiles No.",
                    digits: "InterMiles No. is not valid",
                    checkMod7Logic: "Invalid InterMiles No., Please enter valid InterMiles No.",
                    validCrisJPNumber: "Your application may not have gone through successfully."

                },
                creditTAndC: {
                    required: "Please select Experian Terms and Conditions"
                }
            },
            onfocusout: function (element) {
                this.element(element); // <- "eager validation"
            },
            errorElement: "div",
            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },
            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                if ($(element).hasClass("row margT1 input name_inputBox_credit")) {
                    error.insertAfter($(".name_div"));
                } else if ($(element).hasClass("margT1 statement")) {
                    error.insertAfter($(element).closest("div"));
                } else if ($(element).hasClass("margT1 statement credit")) {
                    error.insertAfter($(element).closest("div"));
                } else if ($(element).hasClass("row margT1 input inputheight jpNum")) {
                    error.insertAfter($(".enroll-link"));
                } else {
                    error.insertAfter(element);
                }
            },
           
            // merged code Sachin
            submitHandler: function (form) {
						var fName = $('.firstName_credit').val();
						var lName = $('.lastName_credit').val();
						var fNameLength = fName.length;
						var lNameLength = lName.length;
						if (fNameLength > 26 || lNameLength > 26) {
						$('.fullName-error-div-credit').html(msg);
						$('.fullName-error-div-credit').removeClass('hidden');
						$('html,body').stop(true, false).animate({scrollTop: $('.fullName').offset().top - 150}, 'fast');
						return false;
						}
						
						if ($("#otpValue").val().length < 6) {
						console.log("1");
						$("#otpError").html("Please Enter OTP");
						$("#otpError").show();
						$("#Submitbtn").attr('disabled', true);
						isCorrectOtp = false;
						}
						
						if ($("#otpValue").val().length == 6) {
						var otpValue = $("#otpValue").val();
						console.log("otpValue===", otpValue)
						if (otpValue == "") {
						console.log("1");
						$("#otpError").html("Please Enter OTP");
						$("#otpError").show();
						$("#Submitbtn").attr('disabled', true);
						isCorrectOtp = false;
						} else if (otpValue.length != 6) {
						console.log("2");
						$("#otpError").html("Please Enter OTP");
						$("#otpError").show();
						$("#Submitbtn").attr('disabled', true);
						isCorrectOtp = false;
						} else {
						if (isNaN(otpValue) == true) {
						console.log("3");
						$("#otpError").html("Please Enter OTP");
						$("#otpError").show();
						$("#Submitbtn").attr('disabled', true);
						isCorrectOtp = false;
						} else {
						var formNumber = '${formNumber}';
						var mobileNumber = $('#mobile').val();
						var otpTransactionId = $("#otpTransactionId").val();
						$.ajax({method: "GET",
						async: false,
						url: "${applicationURL}validateOTP?mobileNumber=" + mobileNumber + "&bankNumber=" + "" + "&formNumber=" + formNumber + "&otpTransactionId=" + otpTransactionId + "&otp=" + otpValue + "&token=" + $("#token").val(), success: function (data) {
						console.log("=========validate1======", data);
						if (data.length > 2) {
						var myObj = JSON.parse(data);
						
						pendingAttempt = myObj.pendingAttempts;
						$("#pendingAttempt").val(myObj.pendingAttempts);
						console.log("pendingAttempt----1----", pendingAttempt);
						if ($("#pendingAttempt").val() == "Number of attempts exceeds limit") {
						$('#otpValue').attr('disabled', true);
						$('#otpValue').val("");
						$("#otpError").html("");
						$("#otpError").hide();
						successMsg = "Left attempts are exceeded";
						// $('#otpSucces').html("Number of attempts exceeds limit");
						$('#otpSucces').removeClass('hidden');
						$('#otpButton').attr('disabled', true);
						$(".otpButton").addClass("hidden");
						var imageUrl = ctx + "static/images/icons/error_icon.png";
						$(".otpsuccess_credit").css({"border": "2px solid #ed4136", "background": "#ffeceb url(" + imageUrl + ") no-repeat 5px"});
						} else if ($("#pendingAttempt").val() != "") {
						console.log("=-=-=p- for OTP BYPASS==-=-=>", $("#pendingAttempt").val());
						successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '. Total attempts left ' + $("#pendingAttempt").val();
						
						} else {
						successMsg = 'OTP has been sent to your mobile number ' + mNumber + ' at ' + time + '.';
						
						}
						$('#otpSucces').html(successMsg);
						
						if (myObj.OTP_Verified == "true") {
						
						
						
						$("#otpError").html("");
						$("#otpError").hide();
						$("#Submitbtn").attr('disabled', true);
						// $("#Submitbtn").removeAttr('disabled');
						isCorrectOtp = true;
						// $('#otpVal').val(otpValue);
						form.submit();
						} else {
						$("#otpError").html("Please Enter Valid OTP");
						$("#otpError").show();
						$("#Submitbtn").attr('disabled', true);
						isCorrectOtp = false;
						// $('#otpVal').val('');
						}
						} else {
						$("#otpError").html("Please Enter Valid OTP");
						$("#otpError").removeClass("hidden");
						$("#Submitbtn").attr('disabled', true);
						isCorrectOtp = false;
						}
						
						}});
						
						}
						
						}
						
						
						} else {
						isCorrectOtp = false;
						}
			
			}
            //////////
            
        });
    

    var fewSeconds = 10;
$('#Submitbtn').click(function(){
    // Ajax request
    var btn = $(this);
     $("#Submitbtn").attr('disabled', true);
    setTimeout(function(){
         $("#Submitbtn").attr('disabled', false);
    }, fewSeconds*1000);
});


console.log('before jp header --------> ',navigator.userAgent == "IM-Mobile-App");
        $(".jp-header-fixed").hasClass("jp-header-fixed");
//        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
//           if(navigator.userAgent === "IM-Mobile-App") {
//            var agentVal = "<h6 class='fixed-top'> Navigator>>>> "+navigator.userAgent+"</h6>";
//            var agentValIM = "<h6 class='fixed-top'> navigator.userAgent.indexOf(IM-Mobile-App)>>>> "+navigator.userAgent.indexOf('IM-Mobile-App')+"</h6>";
           if(navigator.userAgent.indexOf('IM-Mobile-App') > -1){
//            if($(".jp-header-fixed").hasClass("jp-header-fixed") == false){
                    console.log('insdie jp header');
                    $('#wrapper').addClass('mobileagentcsscredit');
                    $(".middletop").css("padding-top", "5px");
                
//            }
        }
   
      });
</script>
