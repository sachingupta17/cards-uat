<!DOCTYPE HTML>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />

        <title>InterMiles Exclusive</title>

        <link rel='stylesheet' href='${applicationURL}css/integrated.css' />

        <!--[if lt IE 9]>
        <link rel="stylesheet" href="../../css/style.css" />
        <script src="../../js/html5shiv.min.js"></script>
        <![endif]-->

        <style>
            .panelone > div{padding-right:118px;}
            .panelone > div{width:auto;}
            footer h3{font-weight:700;}
            footer h3.mediumFont{font-weight:400;}
            .wrap_content{
                background-image:url(images/header/header_bg-shadow_old.png);
                background-repeat: repeat-x;
                background-position: center 53%;
            }
             .wrap_content{
                    position:relative !important;
                    left:0 !important;
                    margin:25% auto;
                }
            /* @media screen and (min-device-width: 300px) and (max-device-width : 768px) {
                .wrap_content{
                    position:relative !important;
                    left:0 !important;
                    margin:0 auto;
                }
            } */
        </style>
    </head>
    <body>
    <!-- Google Analytics start -->
	<script>
	   dataLayer = [{
	       'V_Type': '${sessionScope.gtmloggedInUser}'
	   }];
	</script>
	
	   <!--ls:usercontrol:IsLogin end-->
	   <noscript xmlns=""><iframe style="display: none; visibility: hidden" width="0" height="0" src="//www.googletagmanager.com/ns.html?id=GTM-MN7RT6D"></iframe></noscript>
	   <noscript xmlns=""><iframe style="display: none; visibility: hidden" width="0" height="0" src="//www.googletagmanager.com/ns.html?id=GTM-TTLP892"></iframe></noscript>
	 <script> 
             (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MN7RT6D');
	 </script>
         
          <script> 
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TTLP892');
	 </script>
	<!-- Google Analytics end --> 
    
<% 
String redirectUrl=(String)request.getAttribute("redirectUrl");
%> 
    
        <div class="wrap_content" style="text-align:center">
            <img src="${applicationURL}static/images/header/JP_logo.png" alt="logo image"/>
            <p style="font-size:1.3em; font-family:'Ubuntu_Bold', San-serif; color:#606060">Earn with Partners: Swipe any of our Co-brand Cards</p>
                       
            <img  src="${applicationURL}static/images/header/ajax-loader.gif" alt="loader image"/>
        </div>

        <script src="${applicationURL}js/integrated.js"></script> 

        <script>
       		 setTimeout(function(){
       			 window.location.href="<%= redirectUrl %>";
			},
            1000); 
    
             function pageInfo(){
                 
                 //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----redirectToOtherBank--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----redirectToOtherBank--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----redirectToOtherBank--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----redirectToOtherBank--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----redirectToOtherBank--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----redirectToOtherBank--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----redirectToOtherBank--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----redirectToOtherBank-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----redirectToOtherBank--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End   
                 
                 
                 
                 
                var cardName = localStorage.getItem("CardName");
                digitalData.pageInfo={"pageName":"Other-Card-Red2Bank","category":"Cards","subCategory1":"App-Redirect to Bank","subCategory2":cardName,"partner":"","currency":"","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
                localStorage.removeItem("CardName");
            }
        </script>
    </body>
</html>