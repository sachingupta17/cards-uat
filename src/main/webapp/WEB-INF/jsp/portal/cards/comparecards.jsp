<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<spring:htmlEscape defaultHtmlEscape="true" />--%>

<!--<div id="social-profiles" data-toggle="modal"  class="hidden" onclick="assistMeeClick()">  
    <img src="${applicationURL}static/images/footer/phone-call.png" width="30" height="30">
    <h5>Assist Me</h5>
</div> -->

<form:form autocomplete="off" id="callMe" commandName="callMeBean">
    <div class="modal fade" id="callMe_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div id="callmeclose" class="proceedPopup_close pull-right">
                        <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="formAbandonOfAssistMe()">
                    </div>
                    <div class="callme_popup">  
                        <div class="text-center">
                            Fill in your details for us to call you & help you complete the credit card application
                        </div>
                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Tell us about yourself</h2>
                        </div>
                        <div class="error_box1 hidden">
                            <ul class="error_list1"></ul>
                        </div>
                        <div class="row">
                            <div class="enroll_div margT2">
                                <div class="middleName_input user_details">  
                                    <form:input id="socialName" path="socialName"  placeholder="Full Name" type="text" value="" />
                                </div>
                                <div class="phone_input user_details">
                                    <form:input id="socialPhone" path="socialPhone"  maxlength="10" placeholder="Phone" type="number"  onKeyPress="if(this.value.length==10) return false;" value="" />
                                </div>
                                <div class="email_input user_details">
                                    <form:input id="socialEmail" path="socialEmail"  placeholder="Email" type="text" value="" />
                                </div>

                                <!--                                <div class="jpNumber_input user_details">
                                <%--<form:input id="socialjpNumber" path="socialjpNumber"  placeholder="JetPrivilege Number" type="number" value="" maxlength="9" onKeyPress="if(this.value.length==9) return false;"/>--%>
                            </div>-->
                            </div>
                            <form:hidden path="formNumber" id="formNumber"/>
                            <form:hidden path="pageName" id="pageName"/>
                            <form:hidden path="cardName" id="cardName"/>
                        </div>
                        <div id="loader" class="text_center hidden margT5">
                            <img src="${applicationURL}static/images/header/ajax-loader.gif" alt="Page is loading, please wait." />
                        </div>
                        <div class="text-center margT10">
                            <input class="button_thickBlue" type="button" id="callMeContine" value="Submit">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form:form>
<div class="modal fade" id="successCallmeModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="sizer modal-content">
            <div class="modal-header">
                <!--              <div class="proceedPopup_close pull-right">
                                        <img src="$ {applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="" onclick="$.fn.CloseOnPopUpClick('Enroll Now')">
                                    </div>-->

            </div>
            <div class="modal-body">
                <p class="modaltext" style="color:#01a200; text-align:center; font-size: 18px; ">Thank you for sharing your details! Our representative will reach out to you for further assistance.</p>


            </div>
            <div class="modal-footer">
                <button type="button" class="successcallbut" data-dismiss="modal">OK</button>
            </div>

        </div>

    </div>
</div>

<div class="modal fade" id="abtYourself" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="proceedPopup_close pull-right">
                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="">
                </div>
                <form:form autocomplete="off" id="tellMeAboutYourself" action="applyOther" commandName="cardDetailsBean" method="post">     
                    <div class="jpNum_popup">
                        <div class="text-center jpPopup_header">
                            Start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-brand Card. Apply here
                        </div>

                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Provide InterMiles No.</h2>
                        </div>

                        <div class="row">
                            <div class="jpNumber_div margT2">
                                <div class="jpNumber_input">
                                    <%--<form:input path="jpNumber" id="jpNumber" placeholder="JetPrivilege Number" maxlength="9" pattern="[0-9]*" />--%>

                                    <c:choose>
                                        <c:when test="${empty sessionScope.loggedInUser}">
                                            <form:input path="jpNumber" id="jpNumber" placeholder="InterMiles No." maxlength="9" type="number" pattern="[0-9]*" onkeypress="if(this.value.length==9) return false;" />
                                        </c:when>
                                        <c:otherwise>

                                            <form:input path="jpNumber" id="jpNumber" placeholder="InterMiles No."  maxlength="9" type="number" pattern="[0-9]*" onkeypress="if(this.value.length==9) return false;"/>
                                        </c:otherwise>
                                    </c:choose>

                                </div>
                                <div class="jpNumber_input margT2 hidden">
                                    <form:hidden path="jpTier" id="jpTier" placeholder="InterMiles Membership Tier" disabled="true" pattern="[0-9]*" />
                                    <form:hidden path="cpNo" id="otcpNo"/>
                                    <input type="hidden" id="otbpNo"/>
                                </div>
                                <%--<c:if test="${empty sessionScope.loggedInUser}">--%>
                                <div class="text-center margT2">
                                    If you are not a InterMiles member, <a href="javascript:void(0)" class="enroll_here"  id="enroll_here" style="text-decoration: underline;">please enroll here</a>
                                </div>
                                <%--</c:if>--%>
                            </div>
                        </div>
                        <div class="text-center margT10">
                            <input class="button_thickBlue" type="button" id="tellme" onClick="$(this).closest('form').submit();" value="Submit">
                        </div>
                    </div>
                </form:form>

                <form:form autocomplete="off" id="quickEnrollUser" action="enrollme" commandName="enrollBean">
                    <form:hidden path="enrollbpNo"/>
                    <div class="enroll_popup hidden">
                        <div class="text-center">
                            Enroll into the InterMiles Programme now to start your journey towards a world of exclusive airline and lifestyle benefits with your own Co-Brand Card.
                        </div>
                        <div class="margT2 colorDarkBlue text-center">
                            <h2 class="heading_txt">Enroll Now</h2>
                        </div>
                        <div class="error_box2 hidden">
                            <ul class="error_list2">
                            </ul>
                        </div>
                        <div class="row">
                            <div class="pull-left margT2 nav_web">
                                <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_slide goTo_abtYourself_div">
                            </div>
                            <div class="enroll_div margT2">
                                <div class="title user_details">
                                    <form:select path="enrollTitle" id="enrollTitle" placeholder="Title" class="title_select">
                                        <form:option value="">Select Title</form:option>
                                        <form:option value="Mr">Mr</form:option>
                                        <form:option value="Ms">Ms</form:option>
                                        <form:option value="Mrs">Mrs</form:option>
                                        <form:option value="Dr">Doctor</form:option>
                                        <form:option value="Prof">Professor</form:option>
                                        <form:option value="Captain">Captain</form:option>
                                    </form:select>
                                </div>
                                <div class="gender hidden">
                                    <c:forEach items="${genderStatus}" var="status">
                                        <div style="display:inline-block">
                                            <label class="radio-inline">
                                                <form:radiobutton
                                                    path="enrollGender" name="enrollgender"
                                                    id="enrollGender"
                                                    value="${status.key}" /> </label>
                                            <label class="gender_desc" for="${status.key}" >${status.value}</label>
                                        </div>
                                    </c:forEach>
                                </div>
                                <div class="firstName_input user_details">
                                    <form:input type="text" path="enrollFname" id="enrollFname" placeholder="First Name"/>
                                </div>
                                <div class="middleName_input user_details">
                                    <form:input type="text" path="enrollMname" id="enrollMname" placeholder="Middle Name"/>
                                </div>
                                <div class="lastName_input user_details">
                                    <form:input type="text" path="enrollLname" id="enrollLname" placeholder="Last Name"/>
                                </div>
                                <div class="city_input user_details">
                                    <form:input type="text" path="enrollCity" id="enrollCity" placeholder="City"/>
                                </div>
                                <div class="phone_input user_details">
                                    <form:input path="enrollPhone" id="enrollPhone" maxlength="10" type="number" placeholder="Phone" onkeypress="if(this.value.length==10) return false;"/>
                                </div>
                                <div class="email_input user_details">
                                    <form:input path="enrollemail" id="enrollemail" placeholder="Email"/>
                                </div>
                                <!--											<div class="password_input user_details">
                                                                                                                                < form:password path="enrollPassword" id="enrollPassword" placeholder="Password"/>
                                                                                                                                < form:hidden path="hashPassword" id="hashPassword"/>
                                                                                                                        </div>
                                                                                                                        <div class="password_input user_details">
                                                                                                                                < form:password path="enrollRenterPassword" id="enrollRenterPassword" placeholder="Re Enter Password"/>
                                                                                                                        </div>-->
                                <div class="dob_input user_details">
                                    <form:input path="enrollDob" id="date_of_birth2" readonly="true" class="datepicker" placeholder="Date Of Birth"/>
                                </div>
                                <div class="g-recaptcha recaptchaarea"
                                     data-sitekey=<spring:message code="google.recaptcha.site"/>  id="captch"></div>
                                <div> 
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="termsAndConditionsEnroll" class="margT1 statement" path="termsAndConditionsEnroll" /> 
                                        <span>I agree to the InterMiles membership 
                                            <a href="https://www.jetprivilege.com/terms-and-conditions/jetprivilege"  target="_blank">
                                                Terms and Conditions</a><span style="margin: 0px 4px;display: inline;">and</span> <a href="https://www.jetprivilege.com/disclaimer-policy"  target="_blank">
                                                Privacy Policy</a></span>
                                    </div>
                                    <div class="boxinput">	
                                        <form:checkbox value="" id="recieveMarketingTeam" class="margT1 statement" path="recieveMarketingTeam" />  
                                        <span> Yes, I would like to receive Marketing Communication


                                        </span>
                                    </div>
                                </div>
                                <div class="error_box_enrolStatus hidden" id="enroll_error">
                                </div>
                            </div>
                        </div>
                        <div id="loader" class="text_center hidden margT5">
                            <img src="${applicationURL}static/images/header/ajax-loader.gif" alt="Page is loading, please wait." />
                        </div>
                        <div class="text-center margT10 nav_web">
                            <input class="button_thickBlue" type="button"  id="enrollForm" value="Continue">
                        </div>
                        <div class="nav_arrow_mobile margT10">
                            <div class="left_nav">
                                <img src="${applicationURL}static/images/co-brand/left_arrow_icon.png" class="left_arrow goTo_abtYourself_div">
                            </div>
                            <div class="continue_button">
                                <input class="button_thickBlue" type="button" id="enrollForm_mob_view" value="Continue">
                            </div>
                        </div>
                    </div>  
                </form:form>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="proceed_modal" tabindex=-1 role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">                
                <div class="proceed_popup">
                    <div class="proceed_popup_title">
                        <h2>
                            <span>Earn with Partners: Swipe any of our Co-brand Cards</span>
                        </h2>
                        <div class="proceedPopup_close pull-right">
                            <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="">
                        </div>
                    </div>
                    <div class="row card_toProceed margT2">
                        <div class="card_display text-center pad5">
                            <div class="">
                                <h4 id="popCardName"></h4>
                            </div>
                            <div class="card_img_holder card_img">
                                <img  id="replaceImg">
                            </div>
                            <div class="fee_value">
                                <h4>Fees: <span id="popFees"></span></h4>
                            </div>
                        </div>
                        <div class="card_details pad5 text-center margT5">
                            <div class="membershipNo_div padB10">
                                <p>Your InterMiles No. is</p>
                                <h4><span class="membershipNo" id="jpNo"></span></h4>
                            </div>
                            <div class="margT5">
                                <p>Please ensure to enter your same InterMiles No. in your application form</p>
                            </div>
                            <div class="margT5 proceed">
                                <a href="#" class="button_blue proceedToBank" id="proceedTo" target="_blank">
                                    <span id="proceedBank"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<section class="innerContainer clearfix bottomPadding">
    <section class="main-section ">
        <div class="container">
            <div class="compare_cards pad5">
                <div class="freeFlights_calc_compare margT5">
                    <div class="cont_area">  
                        <h1>
                            Co Brand Card Comparison Tool
                        </h1>
                    </div>
                    <div class="flight-calc-holder">
                        <span>
                            <p> Calculate your free flights based on your monthly spends </p>
                        </span>
                    </div>
                    <div class="spends_calc">
                        <div class="col-md-12">
                            <p class="amtvalue">Annual Spends : INR <span class="slider-output" id="yearly_bill">0</span></p>

                        </div>
                        <div class="col-md-8">
                            <div class="range-example-9"></div>
                            <div class="imagebar9"><img src="${applicationURL}static/images/co-brand/numbar.png"/></div>

                        </div>

                        <div class="col-md-4">
                            <div class="rightdata9">
                                <div class="aheadiv">INR</div>
                                <input id="unranged-value1" type="text" inputmode="numeric" name="amountInput"  value="0" maxlength="9" />
                                <div class="donebut_compare donebutdone">Done</div>

                            </div>   
                            <div class="slider_error_div_compare"></div>
                        </div>
                        <!--                        <div class="spends_calc margT2">
                                                                                <div class="slider_wrapper">								
                                                                                         
                                                       <div class="row">
                                                                                                <div class="slider_division col-sm-8">
                                                                                                        <div class="slider_valu">
                                                                                                           <div style="display:inline">
                                                                                                                   <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>5,000</span> 
                                                                                                                </div>
                                                                                                                <div style="display:inline; float:right;">
                                                                                                                         <span><img src="${applicationURL}static/images/co-brand/rupee_black_icon.jpg" alt="" class="rupee_black_Icon"></span><span>15 Lakhs</span>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <div class="padLR15">
                                                                                                                <input type="range" min="5000" max="1500000" step="5000" data-orientation="horizontal" class="slider">
                                                                                                        </div>
                                                                                                </div>
                                                                                                <div class="annualSpends_wrapper col-sm-4">
                                                                                                        <div class="annualSpends text-center">
                                                                                                          <span>Annual Spends: </span><span class="annualSpends_value">60,000</span>
                                                                                                        </div>
                                                                                                        <div class="text-center margT5">
                                                                                                                <input type="button" class="filter_button_compare button_red" id="CrdCPR_btn_cal-ur-ff-asper-spend" value="Done">
                                                                                                        </div>
                                                                                                </div>
                                                                                        </div>  
                                                                                </div>
                                                                                </div>	-->
                    </div> 
                </div>
            </div>
        </div>

        <!-- Compare cards table -->
        <div class="checkname" id="${pageinfo}" hidden></div>
        <div class="compare_cards_table">
            <table class="compareCards" width="100%" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th class="text-center"> 
                        </th>
                        <c:forEach items="${cardList}" var="list">			
                            <th class="text-center"> 
                                <div class="card_image">
                                    <img id="card_img${list.cpNo}" src="${applicationURL}cards/${list.cardImagePath}">
                                </div>
                                <div class="card_name margT2"> 
                                    <p id="cardName${list.cpNo}">${list.cardName}</p>
                                    <p id="cpFees${list.cpNo}" class="hidden">${list.fees},${list.bankName}</p>
                                </div>
                                <c:choose>
                                    <c:when test="${list.bpNo eq 1}">
                                        <c:choose>
                                            <c:when test="${list.applyflag eq 0}">
                                                <div class="instAppr_button_holder instantButton_cc margT10 " id="CrdCPR_${list.bankName}&${list.cardName}_${list.networkName}_apply">
                                                    <ul  class="applyAmex" data-name="${list.imageText}" data-utm_source="${utm_source}"
                                                         data-utm_medium="${utm_medium}" data-utm_campaign="${utm_campaign}">
                                                        <li>
                                                            <div class="apply-inst apply_instantAppr" onclick="$.fn.ApplyButtonAmexClick('${list.cardName}', '${list.cardName}', '${list.bankName}')">
                                                                <span><img src="${applicationURL}static/images/co-brand/apply_icon_white.png"></span>
                                                                <span>Apply</span>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="apply_instantAppr2">
                                                                <span><img src="${applicationURL}static/images/co-brand/instant_apprvl-icon.png"></span> 
                                                                <span>Instant Decision</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="apply_position margB5">

                                                </div> 

                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>

                                    <c:when test="${(list.bpNo eq 2)}">
                                        <c:choose>
                                            <c:when test="${list.applyflag eq 0}">        
                                                <div class="instAppr_button_holder " id="CrdCPR_${list.bankName}&${list.cardName}_${list.networkName}_apply">

                                                    <ul class="applyIcici" data-name="${list.imageText}" data-spMailingID="${spMailingID}" 
                                                        data-spUserID="${spUserID}" data-spJobID="${spJobID}" 
                                                        data-spReportId="${spReportId}"data-utm_source="${utm_source}"
                                                        data-utm_medium="${utm_medium}" data-utm_campaign="${utm_campaign}">
                                                        <li>
                                                            <div class="apply-inst apply_instantAppr" onclick="$.fn.ApplyButtonAmexClick('${list.cardName}', '${list.cardName}', '${list.bankName}')">
                                                                <span><img src="${applicationURL}static/images/co-brand/apply_icon_white.png"></span>
                                                                <span>Apply</span>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="apply_instantAppr2">
                                                                <span><img src="${applicationURL}static/images/co-brand/instant_apprvl-icon.png"></span> 
                                                                <span>Instant Decision</span>
                                                            </div>
                                                        </li>
                                                    </ul>

                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="apply_position margB5">

                                                </div> 

                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>

                                    <c:when test="${(list.bpNo eq 4)}">
                                        <c:choose>
                                            <c:when test="${list.applyflag eq 0}">
                                                <div class="apply_position margB5">
                                                    <div class="apply_btn hdfc_asking_apply" data-cpName="${list.cardName}" data-cpNo="${list.cpNo}" data-bpNo="${list.bpNo}" data-NoNO="${list.networkName}" data-toggle="modal" data-target="#askingUser" onclick="$.fn.ApplyButtonAmexClick('${list.cardName}', '${list.cardName}', '${list.bankName}')">
                                                        <span>
                                                            <img src="${applicationURL}static/images/co-brand/apply_icon_white.png" class=""> 
                                                        </span>
                                                        <span>
                                                            Apply
                                                        </span>
                                                    </div>
                                                </div> 

                                                <div class="modal fade hdfc_popup" id="askingUser" tabindex=-1 role="dialog">
                                                    <div class="modal-dialog askingPopup-modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <div class="proceedPopup_close pull-right">
                                                                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="">
                                                                </div>
                                                                <div class="askingPopup">
                                                                    <div class="margT2 colorDarkBlue text-center">
                                                                        <h2 class="heading_txt">Are you an existing HDFC Bank Credit Card holder?</h2>
                                                                    </div>
                                                                    <div class="text-center margT11">
            <!--                                                            <a class="hdfc_credit_card_yes" id="CrdCPR_$ {cdList.cardName}_${cdList.networkName}_YES" data-toggle="modal" data-target="#userWithCreditCard" onclick="$.fn.existingCreditCardHolderClick('Yes-Existing Card Holder')">
                                                                            <h1>YES</h1>
                                                                        </a>
                                                                        <a class="non_amex_apply" id="CrdCPR_$ {cdList.cardName}_$ { cdList.networkName}_NO" data-cpNo="$ {list.cpNo}" data-bpNo="$ {list.bpNo}"  data-toggle="modal" data-target="#abtYourself" onclick="noExistingCardHolderClick('No-Existing Card Holder','Submit JPNumber')">
                                                                            <h1>No</h1>
                                                                        </a>-->

                                                                        <a class="hdfc_credit_card_yes" id="CrdHP_${list.cardName}_${list.networkName}_YES" data-cpName="${list.cardName}" data-bpNo="${list.bpNo}" data-NoNO="${list.networkName}" data-toggle="modal" data-target="#userWithCreditCard" onclick="$.fn.existingCreditCardHolderClick('Yes-Existing Card Holder')"><h1>YES</h1></a>
                                                                        <a class="non_amex_apply" id="CrdHP_${list.cardName}_${list.networkName}_NO" data-toggle="modal" data-target="#abtYourself" onclick="noExistingCardHolderClick('No-Existing Card Holder', 'Submit JPNumber')">
                                                                            <h1>No</h1>
                                                                        </a>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="modal fade hdfc_popup" id="userWithCreditCard" tabindex=-1 role="dialog">
                                                    <div class="modal-dialog askingPopup-modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <div class="proceedPopup_close pull-right">
                                                                    <img src="${applicationURL}static/images/co-brand/popup_close_icon.jpg" alt="">
                                                                </div>
                                                                <div class="userWithCreditCardPopup">

                                                                    <p>${bpupgrademessage}
                                                                    </p>

                                                                    <div class="text-center midareabtn">
                                                                        <a class="viewCobranCards" id="CrdCPR_${cdList.cardName}_${cdList.networkName}_View-Others" href="${applicationURL}cardswidget?bank=1,2,3" onclick="upgradeOtherCardsClickforYes('View Other-Existing Card Holder')"> View other Jet Airways Cobranded Cards</a>
                                                                        <a class="upgradeNow" id="CrdCPR_${cdList.cardName}_${cdList.networkName}_Upgrade" data-cpNo="${list.cpNo}" data-bpNo="${list.bpNo}" data-toggle="modal" onclick="upgradeOtherCardsClickforNo('Upgrade-Existing Card Holder', 'Submit JPNumber')"> 
                                                                            <!--data-target="#abtYourself"-->
                                                                            Upgrade Now
                                                                        </a>
                        <!--                                                            <a class="upgradeNow" href="${bpupgradeurl}" target="#_">
                                                                            Upgrade Now</a>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="apply_position margB5">

                                                </div> 

                                            </c:otherwise>
                                        </c:choose>                                      
                                    </c:when>

                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${list.applyflag eq 0}">
                                                <div class="apply_card_button margT7 margB5">
                                                    <span class="buttonBlue3 apply_btn non_amex_apply" id="CrdCPR_${list.bankName}&${list.cardName}_${list.networkName}_apply" data-cpNo="${list.cpNo}"  data-bpNo="${list.bpNo}"> Apply Now</span>
                                                </div>
                                            </c:when>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>


                            </th>
                        </c:forEach>
                    </tr>

                </thead>
                <tbody>

                    <c:forEach begin="0" end="${topHeaderSize}" items="${topHeader}" var="top" varStatus="grp"> 
                        <tr>
                            <td>
                                <div class="card_significance_tag text-center">
                                    <h6>${top}</h6>
                                </div>
                            </td>

                            <c:forEach begin="1" end="${cardsize}"  var="ght" varStatus="card">
                                <td>
                                    <div class="card_significance">
                                        <span id="${grp.index}${card.index}"></span>
                                        <a class="tooltips" href="javascript:void(0);"><img id="top${grp.index}${card.index}" class="tooltipIcon" src="${applicationURL}static/images/co-brand/info_grayicon1.png" style="display:none">
                                            <div class="tooltip_content" id="topcont${grp.index}${card.index}">
                                                <div class="pull-right close_toolTip"><img src="${applicationURL}static/images/co-brand/close_btn.png" alt=""></div>
                                            </div>
                                        </a>
                                    </div>

                                </td>
                            </c:forEach>
                        </tr>

                    </c:forEach> 

                </tbody>
            </table>

            <c:forEach items="${featureMap}" var="headers">
                <div class="joiningBenefit accordian" style="">
                    <h6>${headers.key}</h6>
                </div>

                <table class="joiningBenefits_table accordian_table" width="100%" cellspacing="0">						
                    <tbody>
                        <c:forEach items="${headers.value}" var="feat">
                            <tr>
                                <c:forEach items="${feat}" var="featmap">
                                    <td>
                                        <div class="jpMiles text-center">
                                            ${featmap.key}		
                                        </div>
                                    </td> 
                                    <c:forEach items="${featmap.value}" var="cont">
                                        <td>
                                            <div class="jpMiles_content" id="">
                                                ${cont.cfContent} <c:if test="${not empty cont.cfTermsAndConditions}">
                                                    <a class="tooltips" href="javascript:void(0);"><img id="" class="tooltipIcon" 
                                                                                                        src="${applicationURL}static/images/co-brand/info_grayicon1.png">
                                                        <span class="tooltip_content" id=""> 
                                                            <span class="pull-right close_toolTip">
                                                                <img src="${applicationURL}static/images/co-brand/close_btn.png" alt="">
                                                            </span>
                                                            ${cont.cfTermsAndConditions}
                                                        </span> 
                                                    </a>

                                                </c:if>
                                            </div>

                                        </td>

                                    </c:forEach>
                                </c:forEach>
                            </tr>
                        </c:forEach>

                    </tbody>
                </table>	
            </c:forEach>	

            <div class="pad2">
                <p class="disclaimer">
                    *General Disclaimer: See the online credit card application for details about terms and conditions. We make every effort to maintain accurate information. See the online credit card application for details about terms and conditions. We make every effort to maintain accurate information. 
                </p>
            </div>

        </div>

    </section>
</section>

<script>
    
    //Adobe code start here(phase 6)
    var assistError="";
    var lastAccessedField="";
    //Adobe code end here(phase 6)
    
    
    var spend = '${spendValue}';
//      console.log("spend=>",spend);
    spend = spend.replace(/\,/g, '');
    spend = parseInt(spend, 10);

    var spend_per_month = spend / 12;
    if (isNaN(spend_per_month)) {
        spend = 60000;
        spend = parseInt(spend, 10);
        spend_per_month = spend / 12;
    }

    var cpId = "";
    $(document).ready(function () {
        var callmeflag = '${callMeFlag}';
        console.log("callmeflag===>", callmeflag);
        if (callmeflag != "") {
            if (callmeflag == 1) {
                $("#social-profiles").removeClass('hidden');

            } else {
                $("#social-profiles").addClass('hidden');

            }
        } else {
            $("#social-profiles").addClass('hidden');
        }
        //new implementation cr2
        $("#enrollPhone,#jpNumber,#socialPhone").on("keydown, keypress", function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else
            {
                return true;


            }

        });
        $("#jpNumber").on('keyup', function (evt) {
            console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 9) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        $('#socialName').keydown(function (event) {
            var charCode = event.keyCode;
            if ((charCode > 64 && charCode < 91) || charCode == 8 || charCode == 9 || charCode == 32) {
                return true;
            } else {
                return false;
            }
        });
        $("#enrollPhone,#socialPhone").on('keyup', function (evt) {
            console.log("inside keyup")
            if (evt.which == 8) {
//
            } else if ($(this).val().length >= 10) {
                evt.preventDefault();
                if (evt.keyCode == 09 || evt.keyCode == 11) {
//                      $("#socialjpNumber").focus();
                }
            }

        });
        //new implementation cr2
        function handleRange(valueoftext)
        {
            console.log("legth", valueoftext)
            if (valueoftext < 5000 || valueoftext > 1500000) {
                console.log("at keyup if");
//             if($(".slider_error_div_compare").is(':visible') == false){
                $(".slider_error_div_compare").show().html("Please enter spends value within the range.");
//        }
                return false;
            } else {
                $(".slider_error_div_compare").hide().html("");
                return true;
            }

        }
        var result_Value, result_Value_year;
        $('#unranged-value1').on('keyup', function () {
//            console.log("at keyup");
            var valSpend = $("#unranged-value1").val();
            valSpend = valSpend.replace(/\,/g, '');
            return handleRange(valSpend);
//        if (valSpend < 5000 || valSpend > 1500000) {
////             console.log("at keyup check");
//            $(".slider_error_div_compare").show().html("Please enter spends value within the range.");
//            return false;
//        }else{
//            $(".slider_error_div_compare").hide().html("");
//            return true;
//        }

        });
        slider();
        function slider() {
            $(".range-example-9").asRange({
                step: 1,
                min: 5000,
                max: 500000,
                format: function (value) {
                    getValue(value);
                    return result_Value;
                },
                onChange: function (valueslider) {
                    var value = $('.range-example-9').asRange('get');
//                    console.log( "--value", value);
//                             if(temp != '' && temp > 0){
//                                 console.log("temp",temp);
                    var a = document.getElementById("unranged-value1");
                    getValue(value);
                    var valueoftext1 = $("#unranged-value1").val();
                    valueoftext1 = valueoftext1.replace(/\,/g, '');
                    handleRange(valueoftext1);

                    a.value = result_Value;
                    var valueoftext = $("#unranged-value1").val();
                    valueoftext = valueoftext.replace(/\,/g, '');
                    console.log("at sider", valueoftext)

//                    console.log("result_Value_year1", result_Value_year)
                    $("#yearly_bill").html(result_Value_year);
//                    temp = 0;
//                             }else{
//                                  var a = document.getElementById("unranged-value1");
//                                  if(temp2!='' && temp2 > 0){
//                                      console.log("at temp2")
//                                        getValue(temp2);
//                                         a.value = result_Value;
//                    console.log("result_Value_year", result_Value_year)
//                    $("#yearly_bill").html(result_Value_year);
//                    temp2=0;
//                                  }else{
//                                       console.log("no temp")
//                                        getValue(value);
//                                         a.value = result_Value;
//                    console.log("result_Value_year", result_Value_year)
//                    $("#yearly_bill").html(result_Value_year);
//                                  }
//                  
//                   
//                             }


//                    if (value > 5000) {
//                        console.log("----------1--->", value);
//                        $(".range-example-modal").asRange('set', value);
//                    }


                }
            });
        }
        if (!isNaN(spend_per_month))

        {

//            console.log("spend_per_month---at if-=>",spend_per_month);

        } else {
            spend = 60000;
            spend = parseInt(spend, 10);
//      console.log("spend2=>",spend);
            spend_per_month = spend / 12;
//      console.log("spend_per_month=>",spend_per_month);
//             console.log("spend_per_month---at else-=>",spend_per_month);
        }
        $('.range-example-9').asRange('set', spend_per_month);
//        console.log("at on change", $("#unranged-value1").val());
//        console.log("--->", $('.range-example-9').asRange('get'));
        getValue(spend_per_month);
        $("#unranged-value1").val(result_Value);
        $("#yearly_bill").html(result_Value_year);
        handleRange(spend_per_month);
        $("#unranged-value1").keyup(function (event) {
            var valSpend = $("#unranged-value1").val();
            var valSpendNum = '';
            valSpendNum = valSpend.replace(/\,/g, '');
            valSpendNum = parseInt(valSpendNum, 10);
            if (valSpendNum >= 0) {  //&& valSpendNum <= 1500000

                // skip for arrow keys
                if (event.which == 37 || event.which == 39)
                    return;
                // format number
                $(this).val(function (index, value) {

                    if (valSpend < 5000 || valSpend > 1500000) {
                        $(".range-example-9").asRange('val', 5000);

                        getValue(5000);
                        $("#yearly_bill").html(result_Value_year);
//                                (valSpendNum > 5000) || (valSpendNum < 1500000)
//                               console.log("***********")
//                           $(".range-example-5").asRange('val', valSpendNum);
//                          
//                                getValue(valSpendNum);
//                                 $("#yearly_bill").html(result_Value_year);
                    } else {
//                               console.log("^^^^^^^^^^^^")
                        $(".range-example-9").asRange('val', valSpendNum);
//                          
                        getValue(valSpendNum);
                        $("#yearly_bill").html(result_Value_year);
//                               $(".range-example-5").asRange('val', 5000);
//                          
//                                getValue(5000);
//                                 $("#yearly_bill").html(result_Value_year);
                    }
//                           $(".range-example-9").asRange('val', valSpendNum);
                    $("#unranged-value1").focus();
//                              if(valSpendNum != ""){
//                                getValue(valSpendNum);
//                                 $("#yearly_bill").html(result_Value_year);
//                           }
                    var x = value.replace(/,/g, "");
                    var lastThree = x.substring(x.length - 3);
                    var otherNumbers = x.substring(0, x.length - 3);
                    if (otherNumbers != '')
                        lastThree = ',' + lastThree;
                    var res = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                    return res;

                });
            }
        });
//$("#unranged-value1").focusout(function(){
//    var value= $("#unranged-value1").val();
//     value = value.replace(/\,/g, '');
//    if(value < 5000){
//        $("#unranged-value1").val("5,000"); getValue(5000);
//      
//        $("#yearly_bill").html(result_Value_year);
//          $(".range-example-9").asRange('val', 5000);
//    }
//})
//    function updateSlider(val)
//    {
//        $(".range-example-9").asRange('val', val);
//        return;
//    }

        /* Capturing entered spends value in slider */

        /* Decimal and Alphabets are not allowing */
        $("#unranged-value1").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            // Allow: home, end, left, right, down, up
                                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || (e.keyCode == 110) || (e.keyCode == 190)) {
                        e.preventDefault();
                    }
                });
//        var temp, temp2=0;
//        $("#unranged-value1").on('change', function () {
//            var rangedvalue = $("#unranged-value1").val();
//            if(rangedvalue > 500000){
//                console.log("1")
//                temp=rangedvalue;
//                 $(".range-example-9").asRange('set', rangedvalue);
//            }else{
//                temp=0;
//                temp2=rangedvalue;
//                 console.log("2")
//                $(".range-example-9").asRange('set', rangedvalue);
//            }
////            $(".range-example-5").asRange('set', rangedvalue);
//        });
        $('#unranged-value1').on("keyup", function (evt) {

            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;

            } else
                return true;
        });
        function getValue(param) {
            var yearly = (param * 12).toString();
            var monthly = param.toString();
            var lastThree = monthly.substring((monthly.length) - 3);
            var otherNumbers = monthly.substring(0, monthly.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            result_Value = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            lastThree = yearly.substring((yearly.length) - 3);
            otherNumbers = yearly.substring(0, yearly.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            result_Value_year = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        }
        //end
        var tierExist = false;
        var jp_num_ppp = '${sessionScope.loggedInUser}';

        if (jp_num_ppp != "") {
            $('#jpNumber').val(jp_num_ppp.substring(2));
            tierExist = true;
        }
        var checkupgrade = false;
        var testStr = "-->";
        var arr = [];
    <c:forEach items="${cardList}" var="listItem">
        arr.push("<c:out value="${listItem.cpNo}" />");
    </c:forEach>
        var noOfCards = arr.length;
//            console.log("arr",arr);
        if (noOfCards == 2) {
            $('.joiningBenefits_table td:first-child').css('width', '7.8%');
            $('.compare_cards_table th:first-child').css('width', '7.8%');
            $('.compare_cards_table td:first-child').css('width', '7.8%');
        }
        function getTopTableContent(spend) {
            $.get("${applicationURL}getTopTableContent?spend=" + spend + "&cpNoList=" + arr, function (data, status) {
                $.each(JSON.parse(data), function (key, val) {
                    if (document.getElementById(key)) {

                        document.getElementById(key).innerHTML = val;
                        if (val.indexOf(testStr) > -1) {
                            $("#top" + key).show();
                            document.getElementById(key).innerHTML = val.split(testStr)[0];
                            $('#top' + key).click(function () {
                                $('.tooltip_content').css('visibility', 'hidden');
                                $("#topcont" + key).html('');

                                $("#topcont" + key).html('<div class="pull-right close_toolTip"><img alt="" src="${applicationURL}static/images/co-brand/close_btn.png"></div>');
                                $("#topcont" + key).append(val.split(testStr)[1]);
                            });

                        } else {
                            document.getElementById(key).innerHTML = val.split(testStr)[0];
                        }
                    }
                });

            });
        }
        $(getTopTableContent(spend));

        $('.donebutdone').on('click', function (e) {
            if ($(".slider_error_div_compare").text() == "") {
                spend = $("#yearly_bill").html().replace(/<\/?img[^>]*>/g, "");
//                        console.log("spend3=>",spend);
                spend = spend.substring(spend.indexOf(' ') + 1);
//                        console.log("spend4=>",spend);
                getTopTableContent(spend);
                $(this).parents('.parent_div').addClass('hidden');
                e.stopImmediatePropagation();
            }
        });


        $(document).on("click", ".upgradeNow", function () {
            $("#otcpNo").val(parseInt($(this).attr('data-cpNo')));
            $("#otbpNo").val(parseInt($(this).attr('data-bpNo')));
            $('#askingUser').modal('hide');
            $('#userWithCreditCard').modal('hide');
            $('#abtYourself').modal('show');
            checkupgrade = true;
            //  $("#proceed_modal").modal('hide');


        });


        $("#social-profiles").on('click', function () {
            var fullName = "";
            var socialName = $("#socialName").val();
            var socialPhone = $("#socialPhone").val();
            var socialEmail = $("#socialEmail").val();
//          
//            var formNumber = ' $ { formNumber}';
            var cardName = '${bean.cardName}';
            if ($("#socialName").val() != "") {
                $("#socialName").val(socialName);
            }
            if ($("#socialPhone").val() != "") {
                $("#socialPhone").val(socialPhone);
            }
            if ($("#socialEmail").val() != "") {
                $("#socialEmail").val(socialEmail);
            }
//            $("#socialjpNumber").val(jetpriviligemembershipNumber);
            $("#formNumber").val("");
            $("#pageName").val("Compare Card Page");
            $("#cardName").val(cardName);
//                            $('#callMeContine').attr('disabled',true)

            $('#callMe_modal').modal('show');
//             if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
//                console.log("****")
//                $('#callMeContine').attr('disabled',true)
//            }else{
//                console.log("&&&&&&")
//               $('#callMeContine').attr('disabled',false)
//
//            }
        });

        $('#callMeContine').on('click', function () {

            if ($("#callMe").validate().element('#socialName')
                    && $("#callMe").validate().element('#socialPhone')
                    && $("#callMe").validate().element('#socialEmail')
//                    && $("#callMe").validate().element("#socialjpNumber")
                    )
            {
                $('#loader').removeClass('hidden');
                $('.modal-backdrop').css('z-index', '1050');
                $.ajax({
                    url: '${applicationURL}callme',
                    data: $('#callMe').serialize(),
                    type: 'POST',
                    success: function (data) {
                        if (data != '') {
                            console.log("data in call me", data);
                            $('#callMe_modal').modal('hide');
                            $('#successCallmeModal').modal('show');

                        } else {
                            $('#loader').addClass('hidden');
//                            alert("sorry please try again");
                            $('.modal-backdrop').css('z-index', '1048');
                        }
                        
                        //Adobe code start (phase 6)
                        assistFormSubmitButton($("#socialName").val(),$("#socialPhone").val(),$("#socialEmail").val());
                        //Adobe code end (phase 6)
                    },
                    complete: function () {
                        $('#loader').addClass('hidden');
                        $('.modal-backdrop').css('z-index', '1048');
                        $("#socialName").val('');
                        $("#socialPhone").val('');
                        $("#socialEmail").val('');

                    },
                    error: function (xhr, status) {
                        console.log(status);
                        console.log(xhr.responseText);
                    }
                });
            } else {
                $('.error_box1').removeClass('hidden');

            }
        });


        $("#callMe").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                socialName: {
                    required: true,
                    AlphabetsOnly: true
                },
                socialPhone: {
                    required: true,
                    digits: true,
                    mobileValidation: true
                },
                socialEmail: {
//                    required: true,
                    emailValidation: true
                }
//                socialjpNumber:{
//                digits:true
//                }
            },
            messages: {
                socialName: {
                    required: "Please Enter Full Name",
                    AlphabetsOnly: "Enter Characters Only"
                },
                socialPhone: {
                    required: "Please Enter Mobile Number",
                    digits: "Mobile Number is not valid.",
                    mobileValidation: "Please Enter Valid Mobile Number"
                },
                socialEmail: {
//                    required: "Please Enter Email Id",
                    emailValidation: "Email ID is not valid."
                }
//                ,
//                socialjpNumber:{
//                digits:"Please Enter valid Jetprivilege Number."
//                }
            },
            errorElement: "div",
            onfocusout: function (element) {
                this.element(element);  // <- "eager validation"                   
            },
            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },
            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                $('.error_box1').removeClass('hidden');
                error.insertAfter($(element).closest("div"));
                $(error).appendTo('.error_box1 ul');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');
                
                //Adobe code starts here(phase 6)
                assistError = assistError+error.text()+",";
                $.fn.AssistMeError(assistError);
                console.log("Error at validator !!!",assistError);
               //Adobe code ends here(phase 6)
            },
            submitHandler: function (form) {
                form.submit();
            },
        });

        $("#socialName").on('focusout', function () {
            console.log("in focusout socialName")

            $("#callMe").validate().element('#socialName');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }


        });
        $("#socialPhone").on('focusout', function () {
            console.log("in focusout socialPhone")

            $("#callMe").validate().element('#socialPhone');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;

            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }
        });
        $("#socialEmail").on('focusout', function () {
            console.log("in focusout socialEmail")

            $("#callMe").validate().element('#socialEmail');
            $('.error_box1').removeClass('hidden');

            var len = $('.error_list1 > li:visible').length;

            if (len < 1) {
                $('.error_box1').addClass('hidden');

            } else {
                $('.error_box1').removeClass('hidden');

            }
        });
//        $("#socialName,#socialPhone,#socialEmail").on('focus', function () {
//            $('.error_box1').addClass('hidden');
//            
//        });
        $('#callmeclose').click(function () {
            $('.error_box1').addClass('hidden');
        });

//     $("#socialName,#socialPhone,#socialEmail").on('focusout', function () {
//if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
//                console.log("***!!!!*")
//                $('#callMeContine').attr('disabled',true)
//            }else{
//                console.log("&&&!!!!&&&")
//               $('#callMeContine').attr('disabled',false)
//
//            }            
//        });
//        $("#socialName,#socialPhone,#socialEmail").on('input change', function () {
//            if($("#socialName").is(':focus') || $("#socialPhone").is(':focus') || $("#socialEmail").is(':focus')){
//if($("#socialPhone").val() == "" || $("#socialName").val() == "" ){
//                console.log("*")
//                $('#callMeContine').attr('disabled',true)
//            }else{
//                console.log("&")
//               $('#callMeContine').attr('disabled',false)
//
//            } 
//        }
//        });

        $('.proceedPopup_close').click(function () {
            $('.error_box2').addClass('hidden');
        });

        $(document).on("click", ".non_amex_apply", function () {
            checkupgrade = false;
        });
        $("#tellMeAboutYourself").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                jpNumber: {
                    required: true,
                    valideJpNumber: true,
                    checkForValidTier: true
                }
            },

            messages: {
                jpNumber: {
                    required: "Please Enter InterMiles No.",
                    valideJpNumber: "Please enter valid InterMiles No.",
                    checkForValidTier: "Your application may not have gone through successfully. Please click Submit again."
                }
            },
            errorElement: "div",
            errorPlacement: function (error, element) {
                error.insertAfter($(element).closest("div"));
            },
            submitHandler: function (form) {
                if (checkupgrade == true) {
                    var fees, cardname, cardimg, bankName, jpNum, feesfull;
                    cpId = $("#otcpNo").val();
                    jpNum = $("#jpNumber").val();
                    feesfull = $("#cpFees" + cpId).html();
                    fees = feesfull.split(",")[0];
                    cardname = $("#cardName" + cpId).html();
                    cardimg = $("#card_img" + cpId).attr('src');
                    bankName = feesfull.split(",")[1];
                    var utm_source = '${utm_source}';
                    var utm_medium = '${utm_medium}';
                    var utm_campaign = '${utm_campaign}';
                    var utm_param = "";

                    $("#abtYourself").modal('hide');
                    $("#jpNo").text($("#jpNumber").val());

                    if (utm_source != "" && utm_source != 'undefined') {
                        utm_param = "&utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
                    }
                    $.ajax({url: "${applicationURL}Encryptjpnumber?jpNumber=" + jpNum, success: function (result) {
                            var encryptnum = result;
                            var win = window.open('${applicationURL}applyOtherupgrade?cpNo=' + cpId + '&imNum=' + encryptnum + utm_param, '_blank');
                            if (win) {
                                //Browser has allowed it to be opened
                                win.focus();
                            } else {

                            }
                        }});
                } else {
                    var fees, cardname, cardimg, bankName, jpNum, feesfull;
                    cpId = $("#otcpNo").val();
                    jpNum = $("#jpNumber").val();
                    feesfull = $("#cpFees" + cpId).html();
                    fees = feesfull.split(",")[0];
                    cardname = $("#cardName" + cpId).html();
                    cardimg = $("#card_img" + cpId).attr('src');
                    bankName = feesfull.split(",")[1];
                    var utm_source = '${utm_source}';
                    var utm_medium = '${utm_medium}';
                    var utm_campaign = '${utm_campaign}';
                    var utm_param = "";

                    $("#abtYourself").modal('hide');
                    $("#jpNo").text($("#jpNumber").val());
                    $("#proceed_modal").modal('show');
                    $('#replaceImg').attr('src', cardimg);
                    $('#popCardName').html(cardname);
                    $('#popFees').html(fees);
                    $("#proceedBank").text("Proceed to " + bankName);

                    if (utm_source != "" && utm_source != 'undefined') {
                        utm_param = "&utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
                    }
                    var encryptnum;
                    $.ajax({url: "${applicationURL}Encryptjpnumber?jpNumber=" + jpNum, success: function (result) {
                            encryptnum = result;
                        }});
                    $("#proceedTo").click(function (event) {
                        $('#proceedTo').attr("href", "${applicationURL}applyOther?cpNo=" + cpId + "&imNum=" + encryptnum + utm_param);
                        $("#proceed_modal").modal('hide');
                        event.stopPropagation();
                    });
                }

            },
        });

        $('#date_of_birth2').on('change', function () {
            $("#quickEnrollUser").validate().element('#date_of_birth2');
        });

        $.validator.addMethod('valideJpNumber', function (value, element) {
            var jpNumLastDig, jpNoTemp;
            var jp = $("#jpNumber").val();
            if (jp.length != 9)
                return false;
            else
            {
                jpNumLastDig = jp % 10;
                jpNoTemp = eval(jp.slice(0, -1)) % 7;
                if (jpNumLastDig !== jpNoTemp)
                    return false;
                else
                    return true;
            }
        });

        $.validator.addMethod('checkForValidTier', function (value, element) {
            if (tierExist) {
                return true;
            } else {
                return false;
            }
        });


//			$('#jpNumber').on('change',function(){
        $("#jpNumber").keyup(function () {
            var jpNum = $("#jpNumber").val()
            if (jpNum != "" && jpNum.length == 9) {
                $.ajax({url: "${applicationURL}getTier?jpNum=" + jpNum,
                    async: false,
                    success:
                            function (data) {
                                tierExist = data.length > 2 ? true : false;
                            }
                });

                $("#tellMeAboutYourself").validate().element('#jpNumber');
            }
        });

        $("#quickEnrollUser").validate({

            ignore: [],
            wrapper: "li",
            rules: {
                enrollTitle: {
                    required: true,
                },
                enrollFname: {
                    required: true,
                    AlphabetsOnly: true
                },
                enrollCity: {
                    required: true,
                },
                enrollLname: {
                    required: true,
                    AlphabetsOnly: true
                },
                enrollCity: {
                    required: true,
                },
                enrollemail: {
                    required: true,
                    emailValidation: true
                },
                enrollPhone: {
                    required: true,
                    mobileValidation: true
                },
//				enrollPassword: {
//				 	required :true,
//				 	passwordChecking:true
//				 },
//				 enrollRenterPassword:{
//				 	required: true,
//				 	equalTo: "#enrollPassword",
//				 	passwordChecking:true
//				},
                enrollDob: {
                    required: true
                },
                termsAndConditionsEnroll: {
                    required: true
                }
            },

            messages: {

                enrollTitle: {
                    required: "Please Select Title",
                },
                enrollFname: {
                    required: "Please Enter FirstName",
                    AlphabetsOnly: "Only Characters from A-Z are allowed",
                },
                enrollCity: {
                    required: "Please Enter City Name",
                },
                enrollLname: {
                    required: "Please Enter LastName",
                    AlphabetsOnly: "Only Characters from A-Z are allowed",
                },
                enrollemail: {
                    required: "Please Enter Email Id",
                    emailValidation: "Please Enter valid Email Id"
                },
//			enrollPassword :{
//		       	required: "Please enter new password",
//		       	passwordChecking : '<ul class="pwd_error"><li>Password must have:</li> <div class="pwd_error_content"><li> 8 - 13 characters.</li><li> at least 1 lowercase alphabet.</li><li> at least 1 uppercase alphabet.</li><li> at least 1 number.</li></div></ul>'
//		    },
//		    enrollRenterPassword:{
//		   		required: "Please Re enter password",
//		   		equalTo: "Passwords do not match",
//		   		passwordChecking : '<ul class="pwd_error"><li>Re Enter Password must have:</li> <div class="pwd_error_content"><li> 8 - 13 characters.</li><li> at least 1 lowercase alphabet.</li><li> at least 1 uppercase alphabet.</li><li> at least 1 number.</li></div></ul>'
//		    },
                enrollPhone: {
                    required: "Please Enter Mobile Number",
                    mobileValidation: "Please Enter Valid Mobile Number"
                },
                enrollDob: {
                    required: "Please Enter Date of Birth",
                },
                termsAndConditionsEnroll: {

                    required: "Please accept the InterMiles membership Terms & Conditions to proceed."
                }
            },
            errorElement: "div",
            onfocusout: function (element) {
                this.element(element);  // <- "eager validation"
            },
            highlight: function (element) {
                $(element).siblings('div.error').addClass('alert');
            },

            unhighlight: function (element) {
                $(element).siblings('div.error').removeClass('alert');
            },
            errorPlacement: function (error, element) {
                $('.error_box2').removeClass('hidden');
                error.insertAfter($(element).closest("div"));
                $(error).appendTo('.error_box2 ul.error_list2');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');

            },
            submitHandler: function (form) {
                form.submit();
            },

        });
//		$("#enrollTitle,#enrollCity,#enrollFname,#enrollLname,#enrollPhone,#enrollemail,#termsAndConditionsEnroll").on('focus', function() {
//            console.log("in focu of enroll")
//            $('.error_box1').addClass('hidden');
////            
//        })
        $("#enrollMname").on('focusout', function () {
            console.log("in focusout enrollDob")

//            $("#quickEnrollUser").validate().element('#enrollDob');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollDob").on('focusout', function () {
            console.log("in focusout enrollDob")

            $("#quickEnrollUser").validate().element('#enrollDob');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $(".ui-datepicker-trigger").click(function () {
            $('.error_box2').addClass('hidden');

        })
        $("#termsAndConditionsEnroll").on('focusout', function () {
            console.log("in focusout termsAndConditionsEnroll")

            $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollemail").on('focusout', function () {
            console.log("in focusout enrollemail")

            $("#quickEnrollUser").validate().element('#enrollemail');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollPhone").on('focusout', function () {
            console.log("in focusout enrollPhone")

            $("#quickEnrollUser").validate().element('#enrollPhone');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollLname").on('focusout', function () {
            console.log("in focusout enrollLname")

            $("#quickEnrollUser").validate().element('#enrollLname');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollTitle").on('focusout', function () {
            console.log("in focusout enrolltitle")

            $("#quickEnrollUser").validate().element('#enrollTitle');
            $('.error_box2').removeClass('hidden');
            var len = $('.error_list2 > li:visible').length;
            console.log("len", len);
            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }


        });
        $("#enrollCity").on('focusout', function () {
            console.log("in focusout enrollcity")

            $("#quickEnrollUser").validate().element('#enrollCity');
            $('.error_box2').removeClass('hidden');

            var len = $('.error_list2 > li:visible').length;

            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }
        });
        $("#enrollFname").on('focusout', function () {
            console.log("in focusout enrollFname")

            $("#quickEnrollUser").validate().element('#enrollFname');
            $('.error_box2').removeClass('hidden');

            var len = $('.error_list2 > li:visible').length;

            if (len < 1) {
                $('.error_box2').addClass('hidden');

            } else {
                $('.error_box2').removeClass('hidden');

            }
        });


        $('#enrollForm,#enrollForm_mob_view').on('click', function () {
//			var bpNo = $("#otbpNo").val();
            var bpNo = bankNumber;
            if ($("#quickEnrollUser").validate().element('#enrollTitle') && $("#quickEnrollUser").validate().element('#enrollFname')
                    && $("#quickEnrollUser").validate().element('#enrollLname') && $("#quickEnrollUser").validate().element('#enrollCity')
                    && $("#quickEnrollUser").validate().element('#enrollPhone') && $("#quickEnrollUser").validate().element('#enrollemail')
//						 && $("#quickEnrollUser").validate().element('#enrollPassword') && $("#quickEnrollUser").validate().element('#enrollRenterPassword')
                    && $("#quickEnrollUser").validate().element('#date_of_birth2') && $("#quickEnrollUser").validate().element('#termsAndConditionsEnroll'))
            {
                var data;


                var responsecode = $('#g-recaptcha-response').val()
//                               console.log("result---->"+responsecode);
                $.ajax({method: 'POST',
                    url: '${applicationURL}verifycaptcha?responsecode=' + responsecode, success: function (result) {

//                                console.log("result---->"+result);
                        data = result;
                        if (data == false) {

//                            console.log("false data--->")
                            $('.error_box2').removeClass('hidden');
//                            $('#error').html("Please select captcha").appendTo('.error_box1 ul.error_list1');
                            $('.error_box2 ul.error_list2').html("Please select captcha");
                            $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');
                        } else {
//                            console.log("true data-->")
                            $('.error_box2').addClass('hidden');
                            $('.error_box2 ul.error_list2').html("");
                            //password hashing//
//		          	var enrollPassword = "";
//		          	var hashInBase64 = "";
//		          	var hash = "";
//		          	
//		          	enrollPassword = $("#enrollPassword").val();
//		            hash = CryptoJS.HmacSHA256(enrollPassword, "bff29f3a-90d4-42d5-94f0-448e699991c1");
//		            hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
//					$("#hashPassword").val(hashInBase64);
                            $("#enrollbpNo").val(bpNo);
                            //password hashing//
//					$('#enroll_error').addClass('hidden');
                            $('#loader').removeClass('hidden');
                            $('.modal-backdrop').css('z-index', '1050');
                            var checkbox = $("#termsAndConditionsEnroll");
                            checkbox.val(checkbox[0].checked ? "true" : "false");
                            var checkbox2 = $("#recieveMarketingTeam");
                            checkbox2.val(checkbox2[0].checked ? "true" : "false");
//                       console.log("1"+$( "#termsAndConditionsEnroll" ).val());
//                       console.log("2"+$( "#recieveMarketingTeam" ).val());
                            $.ajax({
                                url: '${applicationURL}enrollme',
                                data: $('#quickEnrollUser').serialize(),
                                type: 'POST',
                                success: function (data) {
                                    var jsonStr = JSON.parse(data);
                                    if (jsonStr.msg == "" && jsonStr.jpNumber != "" && jsonStr.jpNumber != "null") {
                                        $("#jpNo").text(jsonStr.jpNumber);
                                        $("#jpNumber").val(jsonStr.jpNumber);
                                        tierExist = true;
                                        $('.enroll_popup').addClass('hidden');
                                        $('.jpNum_popup').removeClass('hidden');
                                    } else {
                                        $('#loader').addClass('hidden');
                                        $('#enroll_error').removeClass('hidden');
                                        if (jsonStr.msg == "")
                                            $('#enroll_error').text("Your application may not have gone through successfully. Please click 'Continue' again.");
                                        else
                                            $('#enroll_error').text(jsonStr.msg);
                                        $('.modal-backdrop').css('z-index', '1048');
                                    }
                                },
                                complete: function () {
                                    $('#loader').addClass('hidden');
                                    $('.modal-backdrop').css('z-index', '1048');
                                },
                                error: function (xhr, status) {
//			            console.log(status);
//			            console.log(xhr.responseText); 
                                }
                            });
                        }
                    },
                    error: function (e) {
                        console.log("error---->" + e);
                    }
                });
            } else {
                $('.error_box2').removeClass('hidden');
                $('.modal').stop(true, false).animate({scrollTop: 0}, 'slow');

            }
        });

        $(".title_select").change(function () {
            if ($(this).find(":selected").text() == "Mr") {
                $("#tellMeAboutYourself input[type=radio][value=0]").prop('checked', true);

            } else if ($(this).find(":selected").text() == "Ms" || $(this).find(":selected").text() == "Mrs") {
                $("#tellMeAboutYourself input[type=radio][value=1]").prop('checked', true);
            }
        });

    });

    //Adobe code starts here

    function noExistingCardHolderClick(existingName, popupType) {
        popupAbandonType = popupType;
        $.fn.existingCreditCardHolderClick(existingName);
    }

    function upgradeOtherCardsClickforNo(upgradeType, popupType) {
        popupAbandonType = popupType;
        $.fn.upgradeOrViewOtherCardsClick(upgradeType);
    }
    
    //submit button assist me form(phase 6)
        function assistMeeClick(){
            assistMeFlag = true;
            $.fn.ClickOfAssistMe();
        }
     
        function assistFormSubmitButton(name,mobile,email){
                if(name !== "" && mobile !== ""){
                    name = "name:yes";
                    mobile = "mobile:yes";
                }else{
                    name = "name:no";
                    mobile = "mobile:no";
                }
                if(email !== ""){
                    email = "email:yes";
                }else{
                    email = "email:no";
                }
                $.fn.ClickOfAssistContinueButton(name,mobile,email);
        }

    function pageInfo() {
        
        //Added By Arshad for global data layer
                        //For Plateform Condition
  var plateform= window.navigator.userAgent;
           var appFlag = false;
                            if(plateform === "IM-Mobile-App"){
                                plateform="imwebview";
                                appFlag = true;
                               console.log("app plateform -----compareCards--inside app true condition----> ",appFlag);   
                            }
                            
                        if(!appFlag){
                        
                        //var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                           plateform="immob";
                           console.log("mobile plateform -----compareCards--inside desk mobile condition----> ",appFlag);  
                        }
                        
                        
                        else{
                             plateform="imweb";
                              console.log("desk plateform -----compareCards--inside desk true condition----> ",appFlag);
                        }
                            
                        }
                        
         //For PageSource Condition   
         
          var appFlag2 = false;      
            var pageSource= window.navigator.userAgent;
                            if(pageSource === "IM-Mobile-App"){
                                pageSource="imapp";
                                appFlag2 = true;
                                 console.log("app pagesource -----compareCards--inside app true condition----> ",appFlag2);
                            }
                            
                          if(!appFlag2){   
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile pagesource -----compareCards--inside mob true condition----> ",appFlag2);
                           pageSource="immob";
                        }
                        else{
                            console.log("deskt pagesource -----compareCards--inside desk true condition----> ",appFlag2);
                             pageSource="imweb";
                        }             
                          }
                          
            //For isWebview Condition   
            
            var appFlag3 = false;
                         var isWebView= window.navigator.userAgent;
                            if(isWebView === "IM-Mobile-App"){
                           console.log("mobile iswebview -----compareCards--inside mobile true condition----> ",appFlag3);      
                                isWebView="Y";
                                 var appFlag3 = true;
                            }
                             if(!appFlag3){ 
                         // var abc  =window.navigator.userAgent.toLowerCase().includes("mobi")
                        if( /Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
                             console.log("mobile iswebview -----compareCards-- mobile condition----> ",appFlag3);
                           isWebView="N";
                        }
                        else{
                             console.log("mobile iswebview -----compareCards--desktop condition----> ",appFlag3);
                             isWebView="N";
                        } 
                    }
  
  
  //End   
        
        
        
        
        
        //(phase 6) start 
          var callmeflag ='${callMeFlag}';
          if(callmeflag == 1){
              setTimeout(function()
              { 
                $.fn.ViewOfAssistMe() 
              }, 3000);
          }
          //(phase 6) end
          
        $('form input, form select').click(function () {
          lastAccessedField = $(this).attr('name');
        });
        
        digitalData.pageInfo = {"pageName": "Compare", "category": "Cards", "subCategory1": "Card Listing", "subCategory2": "Compare Cards", "partner": "JP", "currency": "INR","pageSource": pageSource,"platform": plateform,"isWebView":isWebView}
    }
    
    //(phase 6 start)
    function formAbandonOfAssistMe(){
            var abandonType = "pop-up close";
            if(lastAccessedField == "socialName"){
                lastAccessedField="Name";
             }else if(lastAccessedField == "socialPhone"){
                lastAccessedField="Mobile";
             }else if(lastAccessedField == "socialEmail"){
                lastAccessedField="Email";
             }

            $.fn.FormAbandonmentOfAssistMe(name,email,mobile,lastAccessedField,abandonType);
        }
    //(phase 6 end)    
        
         //abandonment 
        window.onbeforeunload = function () {  
             if(assistMeFlag == true)
             {
                 $.fn.FormAbandonmentOfAssistMe(name,mobile,email,lastAccessedField,"page refresh");
             }  
        };
    //Adobe code ends here

</script>
