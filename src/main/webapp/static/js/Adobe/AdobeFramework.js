/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var password = "";

$.fn.PageLoadCallToAdobe = function () {
    //Load digitalData object from server along with all possible parameters and then call page load event here
    console.log("### CallToAdobe is get called");
};

$.fn.AdobeGDPRCookieClick=function(linkName){
              digitalData.cookieBandClick =linkName;
               digitalData.eventInfo ={ "eventName":"Cookie Band Click","eventType":"click"};
              //Call to Sattelite Method
              $.fn.CallTrackMethod("cookie band");
}

$.fn.ButtonClick = function (a,b) {
    console.log("Button is clicked" + a + b);
    console.log("digitalData.pageInfo.pageName:"+digitalData.pageInfo.pageName);
    console.log("digitalData.pageInfo.category:" + digitalData.pageInfo.category);
    console.log("######Changing Parameters########");
    digitalData.pageInfo.pageName = "Rajesh Page";
    digitalData.pageInfo.category = "Rajesh Category"

    console.log("digitalData.pageInfo.pageName:" + digitalData.pageInfo.pageName);
    console.log("digitalData.pageInfo.category:" + digitalData.pageInfo.category);

    //To Add Section In DataObject
    digitalData.User = {"UserName":""};

    console.log(digitalData);
    console.log("UserName:" + digitalData.User.UserName);
};

//    I will choose my Card
    $.fn.IwillChooseMyCardClick = function(){
    
   //Call to Sattelite Method
    $.fn.CallTrackMethod("i will choose my own card");
     
}

// Help Me Choose a Card
$.fn.HelpMeChooseACardClick = function(){
    console.log('in side of HelpMeChooseACardClick')
    digitalData.eventInfo ={"eventName":"Next","eventType":"click"}; 
    digitalData.popupName="City of Residence";
   
    //Call to Sattelite Method
    $.fn.CallTrackMethod("help me choose a card");
}




// Popup 1: City of Residence
$.fn.CityOfResidencePopUpClick = function(selectedCity){
   
    digitalData.eventInfo = {"eventName":"Next","eventType":"click"};
    digitalData.popupName="Date of Birth";
    digitalData.selectedCity=selectedCity;
    
    $.fn.CallTrackMethod("event");
}


// Popup 2: DOB 
$.fn.DOBUpClick = function(DOB){
    var dobEncrypt = {"DOB":DOB}
    var encryptDetail = encryptedData(dobEncrypt);
    var encryptDetailJson = JSON.parse(encryptDetail);
    var encryptDob = encryptDetailJson.DOB;
    
    digitalData.eventInfo = {"eventName":"Next","eventType":"click"};
    digitalData.popupName="Fly Frequency";
    digitalData.selectedDob=encryptDob;
    //Call to Sattelite Method
    $.fn.CallTrackMethod("event");
}

// POP Up 3: How Often you fly
$.fn.HowOftenYouFlyClick = function(flightFrequency){
    
    digitalData.eventInfo = {"eventName":"Next","eventType":"click"};
    digitalData.popupName="Lifestyle Preferences";
    digitalData.flightFrequency=flightFrequency;
    //Call to Sattelite Method
    $.fn.CallTrackMethod("event");
}

//Popup 4: Lifestyle Preferences
$.fn.LifeStyleClick = function(preferences){
    
    digitalData.eventInfo = {"eventName":"Next","eventType":"click"};
    digitalData.popupName="Spends on Card"
    digitalData.lifestylePreferences=preferences; //Preference1|Preference2
    
    $.fn.CallTrackMethod("event");
}

//Popup 5: Spends on Card 
$.fn.SpendsOnCardClick = function(cardSpends){
    
    digitalData.eventInfo = {"eventName":"Next","eventType":"click"};
    digitalData.cardSpends=cardSpends; 
    digitalData.popupName="Annual Income";
    
    $.fn.CallTrackMethod("event");
}

//Popup 6: Annual Income 
$.fn.AnnualIncomeClick = function(annualIncome){
    
    digitalData.eventInfo = {"eventName":"Next","eventType":"click"};
    digitalData.annualIncome=annualIncome; 

    $.fn.CallTrackMethod("event");
}

//All Screens: Back Button
$.fn.AllScreensBackButtonClick = function(popupName){
   
    digitalData.eventInfo = {"eventName":"Back","eventType":"click"};
    digitalData.popupName=popupName; 

    $.fn.CallTrackMethod("event");
}

//Load Of Cards Result Page
$.fn.LoadOfAllCards = function(cardsDisplayed,noofResults,filterUsed,sortBy){
    
    digitalData.cardsInfo = {"cardsDisplayed":cardsDisplayed, "noofResults":noofResults};
    digitalData.filterUsed=filterUsed;   //[Type:Value|Type:Value]
    digitalData.sortBy =sortBy; //[BANK A-Z/BANK Z-A]" 

    //$.fn.CallTrackMethod("event");
}

//Apply Button: IndusInd/ICICI 
$.fn.ApplyButtonIndusIndNICICI = function(cardName,variant){
    
    digitalData.eventInfo = {"eventName":"Apply","eventType":"click"};
    digitalData.cardsInfo = {"cardName":cardName, "variant":variant};
     // popup name assignment
     popupName="Provide JP Number";
    $.fn.CallTrackMethod("apply");
}

// Submit Button (Present in popup appeared after clicking apply) [PHASE 5 CHANGES ARE HERE]
$.fn.AfterApplyJPNumberPopUpClick = function(jpNumEntered,popupName,timeElapsed){
   
    digitalData.eventInfo = {"eventName":"Submit JpNumber","eventType":"click"};
    digitalData.jpNumEntered=jpNumEntered;  
    digitalData.timeElapsed = timeElapsed+" "+"secs";  //[Time between Last generate/resend otp to Successful submit otp(seconds) ]
    digitalData.popupName=popupName; 
    popupName= popupName;
    $.fn.CallTrackMethod("submit");
}

//
$.fn.AfterApplyJPNumberPopUpClickForHdfc = function(jpNumEntered,popupName){
   
    digitalData.eventInfo = {"eventName":"Submit JpNumber","eventType":"click"};
    digitalData.jpNumEntered=jpNumEntered;  
    digitalData.popupName=popupName; 
    popupName= popupName;
    $.fn.CallTrackMethod("submit");
}

// Enrol Here (Present in popup appeared after clicking apply)
$.fn.AfterApplyEnrolPopUpClick = function(popupName){
    
    digitalData.eventInfo = {"eventName":"Enrol","eventType":"click"};
    digitalData.popupName=popupName; //
      // popup name assignment
     popupName="Enroll Now";
    $.fn.CallTrackMethod("enrol");
}

//Back (Present in popup appeared after clicking enroll here)
$.fn.EnrolHereBackClick = function(popupName){
    
    digitalData.eventInfo = {"eventName":"Back","eventType":"click"};
    digitalData.popupName=popupName; //
     // popup name assignment
     popupName="Provide JP Number";
    $.fn.CallTrackMethod("event");
}

//Close (Present across popups appeared after clicking apply) 
$.fn.CloseOnPopUpClick = function(popupName){
    
    digitalData.eventInfo = {"eventName":"Close","eventType":"click"};
    digitalData.popupName=popupName; //
    
    $.fn.CallTrackMethod("event");
}

//Proceed to IndusInd/ICICI
$.fn.IndusIndProceedClick = function(){
    
    digitalData.eventInfo = {"eventName":"Proceed to Bank","eventType":"click"};
    $.fn.CallTrackMethod("proceed");
 
}

//Popup Abandonment (browserclose, load of new page, etc.):
$.fn.BrowserCloseClick = function(formName,lastAccessedField,selectedCity){
    //for request
    var cityJson = {"selectedCity": selectedCity}
    //after response
    var encryptDetail = encryptedData(cityJson);
    var encryptDetailJson = JSON.parse(encryptDetail);
    var encryptCity = encryptDetailJson.selectedCity;
    
//    digitalData.eventInfo = {"eventName":"Form Abandonment","eventType":"click"}
    digitalData.formInfo={"formName":formName,"lastAccessedField":lastAccessedField,"selectedCity":encryptCity,"formAbandon":"true"};
    
    $.fn.CallTrackMethod("form abandonment");
}

//Error Tracking
$.fn.ErrorTracking = function(errorDescription,errorCode){
    
     digitalData.eventInfo = {"eventName":"Error","eventType":"click"};
     digitalData.errorInfo = {"errorDescription":errorDescription,"errorCode":errorCode};
    $.fn.CallTrackMethod("error");
}


//Filter Usage 
$.fn.FilterUsage = function(filterUsed,sortBy){
    
    digitalData.eventInfo = {"eventName":"Filter Usage","eventType":"click"};
    digitalData.filterUsed=filterUsed; //"[Type1:Value1|Type2:Value2|…..|Type-N:Value-N]", 
    digitalData.sortBy=sortBy; //"[BANK A-Z/BANK Z-A]" 
    
    $.fn.CallTrackMethod("filter usage");
}


//Application Form: Amex
$.fn.ApplyButtonAmexClick = function(cardName,variant,bankName){
    
    digitalData.eventInfo = {"eventName":"Apply","eventType":"click"};
    digitalData.formInfo ={"formName":bankName+" "+"card application form"};
    digitalData.cardsInfo={"cardName":cardName,"variant":variant};
    $.fn.CallTrackMethod("apply");
    // popup name assignment
     popupName="Provide JP Number";
}

//Form submit: Amex 
$.fn.ApplyButtonAmexFormClick = function(name,email,mobile,currentCity,pincode,bankName){
    
    
    var jsonNewData={"name":name,"email":email,"mobile":mobile,"currentCity":currentCity,"pincode":pincode}
    //after getting response
    var encryptDetail = encryptedData(jsonNewData);
    var encryptDetail1 = JSON.stringify(encryptDetail)
    var encryptDetailJson = JSON.parse(encryptDetail);
    var encryptName = encryptDetailJson.name;
    var encryptEmail = encryptDetailJson.email;
    var encryptMobile = encryptDetailJson.mobile;
    var encryptCity = encryptDetailJson.currentCity;
    var encryptPincode = encryptDetailJson.pincode;
    
    digitalData.eventInfo = {"eventName":"Form Submit","eventType":"click"};
    digitalData.formInfo = {"formName":bankName+" "+"card application form","name":encryptName,"email":encryptEmail,"mobile":encryptMobile,"currentCity":encryptCity,"pincode":encryptPincode}
    // popup name assignment
    popupName="Provide JP Number";
    
    $.fn.CallTrackMethod("submit");
      
    
}

//Form Abandon (browserclose, load of new page, etc.): 
$.fn.FormAbandonAmexClick = function(lastAccessedField,name,email,mobile,currentCity,pincode,bankName){
    //for sending request
    var jsonNewData={"name":name,"email":email,"mobile":mobile,"currentCity":currentCity,"pincode":pincode}
    //after getting response
    var encrypt_Detail = encryptedData(jsonNewData);
    var encrypt_Detail1 = JSON.stringify(encrypt_Detail)
    var encryptDetail_Json = JSON.parse(encrypt_Detail);
    var encrypt_Name = encryptDetail_Json.name;
    var encrypt_Email = encryptDetail_Json.email;
    var encrypt_Mobile = encryptDetail_Json.mobile;
    var encrypt_City = encryptDetail_Json.currentCity;
    var encrypt_Pincode = encryptDetail_Json.pincode;
    
    digitalData.eventInfo = {"eventName":"Form Abandonment","eventType":"click"};
    digitalData.formInfo ={"formName":bankName+" "+"card application form","lastAccessedField":lastAccessedField,"name":encrypt_Name,"email":encrypt_Email,"mobile":encrypt_Mobile,"currentCity":encrypt_City,"pincode":encrypt_Pincode};
    
    $.fn.CallTrackMethod("form abandonment");
}


//Form Abandon (browserclose, load of new page, etc.):

$.fn.FormAbandonICICIClick = function(lastAccessedField,name,email,mobile,currentCity,pincode,bankName){
    
    digitalData.eventInfo = {"eventName":"Form Abandonment","eventType":"click"};
    digitalData.formInfo ={"formName":bankName+" "+"card application form","lastAccessedField":lastAccessedField,"name":name,"email":email,"mobile":mobile,"currentCity":currentCity,"pincode":pincode};
    
    $.fn.CallTrackMethod("form abandonment");
}


//Load Of Thank You Page 
$.fn.LoadOfThankUPage = function(formNumber,bankName){
    
     digitalData.formInfo = {"formName":bankName+" "+"card application form","formNumber":formNumber}; 
     
      $.fn.CallTrackMethod("thank you");
}

//Cards Compare Click
$.fn.CompareCardsClick = function(selectedCards) {
    
    digitalData.eventInfo = {"eventName":"Compare cards","eventType":"click"}
    digitalData.compareInfo = {"cardSelected":selectedCards} //[card1 | card2 |..... cardN]
    
     $.fn.CallTrackMethod("card compare");
    
}

//Compare Cards Popup-selection of annual spends
$.fn.CompareCardsPopupClick = function(annualSpends){
    digitalData.eventInfo = {"eventName":"Annual Spends","eventType":"click"}
    digitalData.annualSpends = annualSpends;
    
    $.fn.CallTrackMethod("annual spends");
}

//Mega Menu Click(As per new Techspecs 20th march 18)
$.fn.MegaMenuClick = function(menuItemName,milesOwned){
    digitalData.eventInfo = {"eventName":"Mega Menu:"+menuItemName,"eventType":"click"}
    digitalData.milesInfo = {"Milesowned":milesOwned}
    
    $.fn.CallTrackMethod("event");
}

//Header/Footer Click(As per new Techspecs 20th march 18)
$.fn.HeaderAndFooterClick = function(linkName,milesOwned){
    digitalData.eventInfo = {"eventName":linkName,"eventType":"click"}
    digitalData.milesInfo = {"Milesowned":milesOwned}
    
    $.fn.CallTrackMethod("event");
}

//PHASE 3 START
$.fn.CompareCardsDoneClick = function(annualSpends){
    digitalData.eventInfo = {"eventName":"Annual Spends","eventType":"click"}
    digitalData.annualSpends = annualSpends;
    
    $.fn.CallTrackMethod("annual spends");
    
}

$.fn.MoreDetailsForCardsClick = function(cardName,variant){
     digitalData.eventInfo = {"eventName":"more details","eventType":"click"}
     digitalData.cardsInfo = {"cardName":cardName, "variant":variant}
     
     $.fn.CallTrackMethod("moreDetails");
}

$.fn.TermsAndConditionsClickOnIcIcI = function(cardName,variant){
     
     var cardName1 = cardName.replace("<sup>&reg;</sup>","");
     var variant1 = variant.replace("<sup>&reg;</sup>","");
    
     digitalData.eventInfo = {"eventName":"Terms and Conditions","eventType":"click"}
     digitalData.cardsInfo = {"cardName":cardName1, "variant":variant1}
    
     $.fn.CallTrackMethod("event");
}

$.fn.formAbandonmentOnQuickEnrollment = function(cardName,variant,abandonType,formName){
     digitalData.eventInfo = {"eventName":"form abandonment","eventType":"click"}
     digitalData.cardsInfo = {"cardName":cardName, "variant":variant}
     digitalData.formInfo = {"formName": formName}
     digitalData.type = abandonType;
     
     $.fn.CallTrackMethod("form abandonment");
}


$.fn.formAbandonmentOnSubmitJPnumberPopup = function(abandonType,formName){
     digitalData.eventInfo = {"eventName":"form abandonment","eventType":"click"}
//     digitalData.cardsInfo = {"cardName":cardName, "variant":variant}
     digitalData.formInfo = {"formName": formName}
     digitalData.type = abandonType;
     
     $.fn.CallTrackMethod("form abandonment");
}


$.fn.formAbandonmentOnSubmitJPnumberPopupAmex = function (abandonType,formName,cardName,variant){
    digitalData.eventInfo = {"eventName":"form abandonment","eventType":"click"}
    digitalData.cardsInfo = {"cardName":cardName, "variant":variant}
    digitalData.formInfo = {"formName": formName}
    digitalData.type = abandonType;
    $.fn.CallTrackMethod("form abandonment");
}

$.fn.formAbandonmentOnProceedToBankPopup= function(cardName,variant,abandonType,formName){
    digitalData.eventInfo = {"eventName":"form abandonment","eventType":"click"}
    digitalData.cardsInfo = {"cardName":cardName, "variant":variant}
    digitalData.formInfo = {"formName": formName}
    digitalData.type = abandonType;
    
    $.fn.CallTrackMethod("form abandonment");
}

//PHASE 3 END

//PHASE 4 START

$.fn.existingCreditCardHolderClick = function(eventName){
    digitalData.eventInfo = {"eventName":eventName,"eventType":"click"}
    $.fn.CallTrackMethod("event");
}

$.fn.upgradeOrViewOtherCardsClick = function(eventName){
    digitalData.eventInfo = {"eventName":eventName,"eventType":"click"}
     $.fn.CallTrackMethod("event");
}

$.fn.successfulQuickEnrollment = function(jpNumber,cardName,variant){
     digitalData.eventInfo = {"eventName":"success cobrand quick enrollment","eventType":"click"}
     digitalData.cardsInfo = {"cardName":cardName, "variant":variant}
     digitalData.jpNumEntered = jpNumber;
     digitalData.enrolType = "cobrand enrol";
     
      $.fn.CallTrackMethod("success enrol");
}

$.fn.successfulQuickEnrollmentOnHdfc = function(jpNumber){
      digitalData.eventInfo = {"eventName":"success cobrand quick enrollment","eventType":"click"}
      digitalData.jpNumEntered = jpNumber;
      digitalData.enrolType = "cobrand enrol";
     
      $.fn.CallTrackMethod("success enrol");
}

//PHASE 4 END

//PHASE 5 START

//event to trigger on the click of Generate OTP.
$.fn.ClicksOnGenerateOtp = function(jpNumber,cardName,variant){
    digitalData.eventInfo = {"eventName":"Cards-SJP-Generate OTP","eventType":"click"}
    digitalData.cardsInfo = {"cardName":cardName, "variant":variant}
    digitalData.jpNumEntered = jpNumber;
    
    $.fn.CallTrackMethod("OTP_Click");
}

//event to trigger on the click of Resend OTP
$.fn.ClicksOnResendOtp = function(jpNumber){
     digitalData.eventInfo = {"eventName":"Cards-SJP-Resend OTP","eventType":"click"}
     digitalData.jpNumEntered = jpNumber;
     $.fn.CallTrackMethod("OTP_Click");
}

//error track on popup
$.fn.errorTrackOnPopup = function(errDesc){
    digitalData.eventInfo = {"eventName":"Error","eventType":"click"}
    digitalData.errorInfo = {"errorDescription":errDesc,"errorCode":" "}
//    digitalData.formInfo = {"formName":bankName+"application form"}
    $.fn.CallTrackMethod("error");
}
//Phase 5 end
//Phase 6 start
//event to trigger on the click of Assist Me.
$.fn.ClickOfAssistMe = function(){
    digitalData.eventInfo = {"eventName":"AssistMe","eventType":"click"}
    $.fn.CallTrackMethod("event");
}

//event to be fired on the view/load of Assist Me.
$.fn.ViewOfAssistMe = function(){
    digitalData.eventInfo = {"eventName":"AssistMe View","eventType":"load"}
    $.fn.CallTrackMethod("assistme view");
}

//event to trigger on the click of Continue button.
$.fn.ClickOfAssistContinueButton =  function(name,mobile,email){
    digitalData.eventInfo = {"eventName":"AssistMe Continue","eventType":"click"}
    digitalData.formInfo = {"formName":"AssistMe form","name":name,"email":email,"mobile":mobile} //<name:yes/no>,<email:yes/no>,<mobile:yes/no>
    $.fn.CallTrackMethod("assistme");
}

//event to be fired on the occurrence of error:
$.fn.AssistMeError = function(errorDesc){
    digitalData.eventInfo = {"eventName":"Error","eventType":"click"}
    digitalData.errorInfo = {"errorDescription":errorDesc,"errorCode":""}
    $.fn.CallTrackMethod("error");
}

//event to trigger on the Form Abandonment of Assist Me flow
$.fn.FormAbandonmentOfAssistMe = function(name,email,mobile,lastAccessedField,abandonType){
    var encrypt_Name=""; var encrypt_Email=""; var encrypt_Mobile="";
    var jsonData = {"name":name,"email":email,"mobile":mobile}
    var encryptDetail = encryptedData(jsonData);
    var encryptDetail_Json = JSON.parse(encryptDetail);
    encrypt_Name = encryptDetail_Json.name;
    encrypt_Email = encryptDetail_Json.email;
    encrypt_Mobile = encryptDetail_Json.mobile;
    digitalData.eventInfo = {"eventName":"Form Abandonment","eventType":"click"}
    digitalData.formInfo = {"formName":"AssistMe form","name":encrypt_Name,"email":encrypt_Email,"mobile":encrypt_Mobile,"lastAccessedField":lastAccessedField} //[name/email/mobile in the form hashed]
    digitalData.type = abandonType;     //[pop-up close/pagr refresh]
    $.fn.CallTrackMethod("form abandonment");
}

//PHASE 6 END



// Calling Track Method
$.fn.CallTrackMethod = function (title) {
   console.log("==================TRACK is Called for: "+title+"================================");
   _satellite.track(title);
    console.log(digitalData);
}



//encryption function
function encryptedData(jsonToEncrypt){
    var encryptedJson;
    $.ajax(
            { method : 'GET',
             async: false,
             url:ctx+"encryptedAdobe?jsonData="+JSON.stringify(jsonToEncrypt),
             success: function (result) {
             encryptedJson = JSON.stringify(result);
              
           }});

       return encryptedJson;
           
      } 
      
