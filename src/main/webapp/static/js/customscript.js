var jppl = (function () {
    var o = {
        init: function () {
            this.addfooter();
            this.domload();
            this.shrinkHeader();
            this.homeBanner();
            this.customRadioButton();
        },
        addfooter: function () {
            jppl.footerAnimation();
        },
        
        //changes made in dev, since click is executing twice
        footerAnimation: function () {
            $(document).on('click', '#downarrow', function (e) {
                if ($(this).hasClass("isDown")) {
                    $("footer").animate({bottom: "0px"}, 200);
                    $(this).removeClass("isDown");
                    $("footer").css('height','300px');
                    e.stopImmediatePropagation();
                } else {
                    $("footer").animate({bottom: "-175px"}, 200);
                    $(this).addClass("isDown");
                    $("footer").css('height','180px'); // added, since this is dynamic irrespective of the screen size
                    e.stopImmediatePropagation();
                }
            });
        },
        domload: function () {
            if (/Android|webOS|iPhone|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && ($(window).width() < 786)) {
                $(document).click(function (event) {
                    if (!$(event.target).parents().addBack().is('.helpText')) {
                        $(".helpText .helpmessages").remove();
                    }
                });

                if ($('#optionOverlay .search').length < 1) {
                    $(".searchContainer .search").clone().appendTo("#optionOverlayList");
                    $('.logoSection .searchContainer .search').remove();
                }
                if ($('#optionOverlay .countryList').length < 1) {
                    $(".countryList").clone().appendTo("#optionOverlay");
                    $('.headerPanelSearch .countryList').remove();
                    $('select.countryList').wrap('<span class="selectArrow"></span>');
                }
            }


            if ($(document).width() < 480) {
                $('#bxslideBanner').bxSlider({
                    infiniteLoop: false,
                    minSlides: 2,
                    maxSlides: 2,
                    slideWidth: 360,
                    slideMargin: 2,
                    pager: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    auto: false,
                    autoControls: false,
                    nextSelector: '#sliderNext',
                    prevSelector: '#sliderPrev',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true
                });
                $('#bxsliderSmall').bxSlider({
                    infiniteLoop: false,
                    minSlides: 2,
                    maxSlides: 2,
                    slideWidth: 360,
                    slideMargin: 2,
                    pager: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    auto: false,
                    autoControls: false,
                    nextSelector: '#slide_next',
                    prevSelector: '#slide_prev',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true
                });

                $('#bxsliderBanner ').bxSlider({
                    infiniteLoop: false,
                    minSlides: 2,
                    maxSlides: 2,
                    slideWidth: 360,
                    slideMargin: 2,
                    pager: false,
                    auto: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    autoControls: false,
                    nextSelector: '#next_slide',
                    prevSelector: '#prev_slide',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true
                });
                $('#bxContentSlide ').bxSlider({
                    infiniteLoop: false,
                    minSlides: 2,
                    maxSlides: 2,
                    slideWidth: 360,
                    slideMargin: 2,
                    pager: false,
                    auto: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    autoControls: false,
                    nextSelector: '#contentNext',
                    prevSelector: '#contentPrev',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true
                });
            } else if ($(document).width() <= 768) {
                $('#bxslideBanner').bxSlider({
                    infiniteLoop: false,
                    minSlides: 3,
                    maxSlides: 3,
                    slideWidth: 360,
                    slideMargin: 2,
                    pager: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    auto: false,
                    autoControls: false,
                    nextSelector: '#sliderNext',
                    prevSelector: '#sliderPrev',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true
                });
                $('#bxsliderSmall').bxSlider({
                    infiniteLoop: false,
                    minSlides: 3,
                    maxSlides: 3,
                    slideWidth: 360,
                    slideMargin: 2,
                    pager: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    auto: false,
                    autoControls: false,
                    nextSelector: '#slide_next',
                    prevSelector: '#slide_prev',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true
                });

                $('#bxsliderBanner ').bxSlider({
                    infiniteLoop: false,
                    minSlides: 3,
                    maxSlides: 3,
                    slideWidth: 360,
                    slideMargin: 2,
                    pager: false,
                    auto: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    autoControls: false,
                    nextSelector: '#next_slide',
                    prevSelector: '#prev_slide',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true
                });
                $('#bxContentSlide ').bxSlider({
                    infiniteLoop: false,
                    minSlides: 3,
                    maxSlides: 3,
                    slideWidth: 360,
                    slideMargin: 2,
                    pager: false,
                    auto: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    autoControls: false,
                    nextSelector: '#contentNext',
                    prevSelector: '#contentPrev',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true
                });

            } else if ($(document).width() > 768) {
                $('#bxslideBanner').bxSlider({
                    infiniteLoop: false,
                    minSlides: 5,
                    maxSlides: 5,
                    slideWidth: 360,
                    slideMargin: 10,
                    pager: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    auto: false,
                    autoControls: false,
                    nextSelector: '#sliderNext',
                    prevSelector: '#sliderPrev',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true

                });
                $('#bxsliderSmall').bxSlider({
                    infiniteLoop: false,
                    minSlides: 5,
                    maxSlides: 5,
                    slideWidth: 360,
                    slideMargin: 9,
                    pager: false,
                    auto: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    autoControls: false,
                    nextSelector: '#slide_next',
                    prevSelector: '#slide_prev',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true
                });
                $('#bxsliderBanner ').bxSlider({
                    infiniteLoop: false,
                    minSlides: 5,
                    maxSlides: 5,
                    slideWidth: 360,
                    slideMargin: 9,
                    pager: false,
                    auto: false,
                    easing: 'easeOutElastic',
                    speed: 3000,
                    controls: true,
                    autoControls: false,
                    nextSelector: '#next_slide',
                    prevSelector: '#prev_slide',
                    prevText: '',
                    nextText: '',
                    hideControlOnEnd: true
                });

            }

        },
        homeBanner: function () {
            var slider_length = $('#bigBxslider li').length;
            //.alert(slider_length);
            if (slider_length <= 1) {
                $('.bx-controls').hide();
                $('#bigBxslider li').parent().removeAttr('id');
                $('#box_slider').removeAttr('class');
                $('#box_slider').children().removeAttr('class');
                $('#box_slider').children().children().removeAttr('class');
            }
            var self = o.config;

            slider = $('#bigBxslider').bxSlider({
                mode: 'fade',
                captions: false,
                controls: true,
                easing: 'swing',
                auto: true,
                autoControls: true,
                slideWidth: 1004,
                pause: 4000,
                hideControlOnEnd: true
            });
        },
        shrinkHeader: function () {
            var self = o.config;
            if ($(document).width() > 767) {
                $(window).scroll(function () {

                    //To disable header transition when page content is less
                    var visibleContentHght = $(window).height() - ($(".site-header").height() + 320);	//320: innerContainer bottomPadding 
                    //console.log("visibleContentHght :" + visibleContentHght + " :innerContainer Height: " + $(".innerContainer").height());
                    var shrinkHeader = ($(".innerContainer").height() > visibleContentHght && $(".innerContainer").height() < (visibleContentHght + $(".site-header").height())) ? false : true;
                    //console.log("shrinkHeader::" + shrinkHeader);

                    var nowScrollTop = $(this).scrollTop();
                    if (nowScrollTop >= 2 && shrinkHeader) {
                        $('.site-header').addClass('shrink');
                        $(".shrink").parents("#includedHeader").next('.bottomPadding').css({"margin-top": "150px"});
                        $(".headerPanelSearch").hide();
                        $('.dropdown').css('z-index', '0');
                    } else {
                        $(".shrink").parents("#includedHeader").next('.bottomPadding').css({"margin-top": "0px"});
                        $('.site-header').removeClass('shrink');
                        $(".headerPanelSearch").show();
                        $('.dropdown').css('z-index', '1');
                    }
                });
            }
        },
        mobileMenu: function () {

            if (navigator.userAgent.match(/(iPhone)/i)) {
                $(".megaMenu").remove();
            }

            $(".MegamenuOpenlogo").on('click', function (e) {
                $(".topNavigation").toggleClass("scrollMenu");
                $('.innerContainer').addClass('mobileContainer');
                $(".topNavigation").on('click', function (e) {
                    e.stopPropagation();
                });
                $("html").click(function () {
                    $('.innerContainer').removeClass('mobileContainer');
                    $(".topNavigation").removeClass("scrollMenu");
                });
            });

            $(".MegamenuOpenlogo").on('click', function (e) {
                //to remove login expand
                $('.loginWrapper .headerLoginWrapper').slideUp("slow");
                $('.loginWrapper').removeClass('loginexpand');
                $(".loginWrapper .countryArrow").remove();

                //hide serch input 
                $(".search").removeAttr('style');
                $(".search").find("button").hide();
                $(".searchBox").removeClass("searachpanel");

                //hide flyout
                $("#optionOverlay").slideUp("slow");
                $(this).toggleClass('MegamenuCloselogo');
                e.stopPropagation();

                if ($(".MegamenuOpenlogo").hasClass('MegamenuCloselogo')) {
                    $("#OuterOverlay").show();
                } else {
                    $("#OuterOverlay").hide();
                }
            });

            $("html").click(function () {
                $(".MegamenuOpenlogo").removeClass("MegamenuCloselogo");
                $("#OuterOverlay").hide();
                $('.innerContainer').removeClass("minWidth630");
            });

            $(".OptionOpen").on('click', function (e) {
                //to remove login expand
                $('.loginWrapper').removeClass('loginexpand');
                $(".loginWrapper .countryArrow").remove();
                $('.loginWrapper .headerLoginWrapper').slideUp("slow");
                //hide serch input 
                $(".search").removeAttr('style');
                $(".search").find("button").hide();
                $(".searchBox").removeClass("searachpanel");

                //hide megamenu
                $(".topNavigation").removeClass("scrollMenu");
                $(".MegamenuOpenlogo").removeClass("MegamenuCloselogo");

                $('#OuterOverlay').hide();
                //show flyout
                $("#optionOverlay").slideToggle("slow");
                e.stopPropagation();
            });

            $("#optionOverlay").on('click', function (e) {
                e.stopPropagation();
            });

            $("html").click(function () {
                $("#optionOverlay").slideUp("slow");
                $('.mobMenul3').removeClass("showMobMenul3");
                $('.thirdLevelMenuList').removeClass("thirdLevelMenuListFlip");
            });

            $(".thirdLevelMenuList").on('click', function (e) {
                $(this).find('.mobMenul3').toggleClass("showMobMenul3");
                $(this).toggleClass("thirdLevelMenuListFlip");
                if ($(this).parents('li').hasClass('EMilesList')) {
                    $('.innerContainer').addClass("minWidth630");
                } else {
                    if ($('.innerContainer').hasClass("minWidth630")) {
                        $('.innerContainer').removeClass("minWidth630");
                    }
                }
            });

            $(".PNetwork").on('click', function (e) {
                $('.innerContainer').addClass("minWidth630");
            });

            $(".thirdLevelMenuList .mobMenul3").on('click', function (e) {
                e.stopPropagation();
            });
        },
        customRadioButton: function () { /* Function to show custom radio button */
            $('input[type=radio]:checked').next("label").addClass("checked");
            $("input[type='radio']").change(function () {
                $('input[type=radio]:not(:checked)').next("label").removeClass("checked");
                $('input[type=radio]:checked').next("label").addClass("checked");
            });
        },
        masterPageProcess: function () {
            //seachsection Start
            $(".search").on('click', function (e) {
                //to remove login expand
                $('.loginWrapper .headerLoginWrapper').slideUp("slow");
                $('.loginWrapper').removeClass('loginexpand');
                $(".loginWrapper .countryArrow").remove();

                //hide overlay
                //$("#optionOverlay").slideUp("slow");

                //hide megamenu
                $(".topNavigation").removeClass("scrollMenu");
                $(".MegamenuOpenlogo").removeClass("MegamenuCloselogo");
                $('#OuterOverlay').hide();

                //$(this).css({ "background": "#E8EEF0 url('noimage.png') no-repeat left top" });
                $(this).find('button').show();
                $(this).find("input.searchBox").addClass('searachpanel');
                //$('.search #searchBox').slideToggle("slow");
                $(".searachpanel").attr("role", "alert").focus();

                $(".reset").on('click', function () {
                    $(this).parent(".search").find("input.searchBox").val("");
                    $(".reset").fadeOut("slow");
                });
                e.stopPropagation();
            });

            $("html").click(function () {
                $(".search").removeAttr('style');
                $(".search").find("button").hide();
                $(".searchBox").removeClass("searachpanel");

                // $(".search .countryArrow").remove();
                $(".reset").css('display', "");
                $(".searchBox").val("");

            });

            $("input.searchBox").keydown(function () {
                if ($(".searchBox").val() === "") {
                    $(".reset").fadeOut("slow");
                } else {
                    $(".reset").fadeIn("slow");
                }
            });
        }
    };
    return o;
})();



