/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * version: 1.0.2
 */

$.fn.SetCTGlobalData = function (event_name) {
    try {
        console.log("ct globallll")
        if (window.clevertapData == undefined)
            window.clevertapData = {};
        if (window.clevertapData["Global"] == undefined)
            window.clevertapData["Global"] = {};
        window.clevertapData["Global"]['Channel'] = (window.navigator.userAgent === "IM-Mobile-App") ? 'Webview' : (/Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) ? 'Mweb' : 'Web'; //'<App/Web/Webview/Mweb>', 
        window.clevertapData["Global"]['Property'] = 'Cards';
        window.clevertapData["Global"]['Category'] = "None"; //<Earn/Redeem/None>
        window.clevertapData["Global"]['Page Name'] = (window.location.host + ((window.location.pathname == "/") ? "/home" : window.location.pathname)).replace(/\//g, ":");
        //   'CleverTap ID': window.clevertap.getCleverTapID(),
        window.clevertapData["Global"]['Login Status'] = window.loggedInUser ? "Logged In" : "Anonymous";
    } catch (e) {
        console.log("ct err:", e)
    }
};
$.fn.SetCTGlobalData();

$.fn.bannerClick = function (name,pos) {
//    console.log(name,"   ",pos)
    try {
        window.clevertapData["Banner"] = {
            "Action":"Banner Click",
            "Banner Name":name,
            "Banner Type":"carousel",
            "Banner Site Section":"body",
            "Banner Placement":"C-"+pos,
//            "Banner Category":"",
//            "Banner Offer Code":"",
//            "Banner Golive Date":""
        };
        $.fn.CallCT("Banner");
    }
    catch (e) {
        console.log("ct err: ",e);
    }
};

$.fn.ApplyButtonIndusIndNICICI_CT = function (pno,name,vari,fees) {
    try {
        if(vari == 'American Express') name = name.split(vari)[1].trim()
        else name = name.split(vari + ' Bank')[1]
        window.clevertapData["Card Application"] = {
            "Action":"Card Application - Attempt",
            "Product ID":pno,
            "Product Name":name.replace("<sup>®</sup> ", "").trim(),
            "Partner Name":vari.indexOf('Bank') == -1 && vari != 'American Express'? vari + ' Bank' : vari,
            "Fees": Number(fees)
        }
        $.fn.CallCT('Card Application');
    }
    catch(e) {
        console.log("err: ",e);
    }
};

$.fn.applyOnDetails = function (cardName,prtName,fees, pNo) {
     if (cardName.indexOf('Jet Airways') !== -1) {
        cardName = cardName.replace('Jet Airways','')
        cardName = cardName.split('Bank')[1].trim()
    }
    if (cardName.indexOf('InterMiles') !== -1) {
        cardName = cardName.replace('InterMiles','')
        cardName = cardName.split('Bank')[1].trim()
    }
    try {
        window.clevertapData["Card Application"] = {
            "Action":"Card Application - Attempt",
            "Product ID":pNo,
            "Product Name":cardName,
            "Partner Name":prtName,
            "Fees": Number(fees)
        }
        $.fn.CallCT('Card Application');
    }
    catch(e) {
        console.log("err: ",e);
    }
};

$.fn.AfterApplyLoginPopUpClickCT = function () {
    try {
        window.clevertapData["Action Click"] = {
            "Action" : "Login - Attempt:Card Application"
        }
        $.fn.CallCT("Action Click");
    }
    catch(e) {
        console.log("err: ",e);
    }
};

$.fn.AfterApplyEnrolPopUpClickCT = function () {
    try {
        window.clevertapData["Action Click"] = {
            "Action" : "Enrol - Attempt:Card Application"
        }
        $.fn.CallCT("Action Click");
    }
    catch(e) {
        console.log("err: ",e);
    }
};

$.fn.actionClickCT = function (actionVal,pName,prtName) {
    if (pName&&pName.indexOf('Jet Airways') !== -1) {
                pName = pName.replaceAll('Jet Airways','').trim()  
                console.log('Jet Airways')
            }
    if (pName&&pName.indexOf('InterMiles') !== -1) {
                pName = pName.replaceAll('InterMiles','').trim()  
                console.log('InterMiles')
    }  
    pName = pName&&pName.indexOf('<sup>®</sup>') != -1?pName.replaceAll('<sup>®</sup>',''):pName;
    console.log('   1223',actionVal)
    if (actionVal == 'Card Application: Proceed') {
        var pDesc = decodeURIComponent(document.URL.substr(document.URL.lastIndexOf('/') + 1));
        pName = pDesc.substring(pDesc.indexOf('Bank')+4,pDesc.length);
        prtName = pDesc.substring(0,pDesc.indexOf('Bank')+4);            
    }
    else if(actionVal == 'Card Application: Submit') {
       if (window.clevertapData["Card Application"] !== undefined) {
           pName = window.clevertapData["Card Application"]["Product Name"]
           prtName = window.clevertapData["Card Application"]["Partner Name"]
       }
       else {            
            if(prtName == 'American Express') {
                prtName = prtName
                pName = pName
            }
            else{   
               console.log('inside else')          
               prtName = prtName.indexOf('Bank') == -1? prtName + ' Bank' : prtName;
               pName = pName.split(prtName)[1];         
            }
        }
    }
    else {
        prtName = prtName.indexOf('Bank')== -1&&prtName !== 'American Express'? prtName + ' Bank': prtName
        pName = pName.split(prtName)[1]    
    }
    
    try {
        console.log(pName,prtName)
        if(prtName.indexOf('-HDFC') != -1){ 
            pName = pName.replaceAll('-',' ').trimLeft()
            prtName = prtName.replaceAll('-',' ').trimLeft()           
        }
        window.clevertapData['Action Click'] = {
            'Action' : 'Cards - ' + actionVal,
            'Product Name' : pName? pName.trim():'',
            'Partner Name' : prtName? prtName: '',
        }
        $.fn.CallCT('Action Click');
    } 
    catch (e) {
        console.log('err: ',e)
    } 
};

$.fn.partnerRedirectionCT = function (pName,prtName,fees) {
//    prtName.indexOf('Bank') == -1 ? prtName = prtName + ' Bank' : prtName
    if(window.clevertapData['Card Application']['Partner Name'] == 'IndusInd Bank'){
        pName = window.clevertapData['Card Application']['Product Name']
        prtName = window.clevertapData['Card Application']['Partner Name']
    }
    else return "";
    try {
      window.clevertapData['Partner Redirection'] = {
          'Action' : 'Proceed to partner',
          'Product ID':window.clevertapData['Card Application']?window.clevertapData['Card Application']['Product ID']:'',
          'Product Name': pName,
          'Partner Name': prtName,
          'Card Fees': window.clevertapData['Card Application']? window.clevertapData['Card Application']['Fees'] : fees,
          'IM Number Entered': document.querySelector('.membershipNo').innerHTML
      }  
      $.fn.CallCT('Partner Redirection');
    }
    catch (e) {
        console.log('err: ',e);
    }   
};

$.fn.suceessCT = function (refId,cardName,bankName) {
    console.log('success',refId,cardName,bankName);
    try {
        window.clevertapData['Card Application'] = {
            'Action' : 'Card Application - Success',
//            'Product Id': "",
            'Product Name': cardName?cardName:"",
            'Partner Name': bankName?bankName:"",
//            'Fees': "",
            'Reference ID': refId? refId : "",
//            'Application Status': Status           
        }
        setTimeout(()=>{$.fn.CallCT("Card Application");},1500);
    }
    catch(e){
        console.error(e);
    }      
}

$.fn.ErrorTrackingCT = function (errMsg) {
   try{
       window.clevertapData['Error'] = {
           'Action': 'Error',
           'Error Scenario': '',
           'Error Message': errMsg,
//           'Error apiResponse': '',
//           'Error appVersion': '',
//           'Error requestId': ''
       }
       $.fn.CallCT('Error')
   }
   catch (e) {
       console.error(e)
   }
}

$.fn.failedCT = function (errMsg,pName,bName) {
    try {
        window.clevertapData['Card Application'] = {
           'Action' : 'Card Application - Failed',
//            'Product Id': "",
            'Product Name': pName?pName:"",
            'Partner Name': bName?bName:"",
//            'Fees': "",
//            'Reference ID': refId,
//            'Application Status': Status,
            'Error Message': errMsg
        }
        setTimeout(()=>{$.fn.CallCT("Card Application");},1500);
    }
    catch (e) {
        console.error(e)
    }
}
               /*
                * @param {String} event name
                * 
                */
$.fn.CallCT = function (event_name) {
    try {
        console.log('Call CT event hit here=====>>', event_name)
        var clevertap_data = {...window.clevertapData[event_name], ...window.clevertapData["Global"]};
        clevertap_data['CleverTap ID'] = window.clevertap.getCleverTapID();
        clevertap_data['Login Status'] = window.loggedInUser ? "Logged In" : "Anonymous";
        for (var key in clevertap_data) {
            if (clevertap_data[key] === null || clevertap_data[key] === undefined || clevertap_data[key] === "")
                delete clevertap_data[key];
        }

        console.log("clevertap data:", clevertap_data);
//        if(event_name == "Card Application") window.clevertap.privacy[0].optOut = false;
        window.clevertap.event.push(event_name, clevertap_data);
    } catch (e) {
        console.log("ct err:", e)
    }
}