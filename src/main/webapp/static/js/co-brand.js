var value_common = 5000;
var freeFlights_calc = 5000;
var spendCapability = 5000;
var joiningFees = 500;
var flightFreq = 1;
var checked_cards = 0;
var result_Value = 0;
var result_Value_year = 0;
var _checked = 0;
var compareCards_boolean = 0;
var session_validate = 0;
var residentAddressField1 = "";
var residentAddressField2 = "";
var residentAddressField3 = "";
var pin = "";
var city = "";
var state = "";
var phone = "";
var bankNumber = "";
var std = "";

var companyFullAddress;
var negativeIciciPincodeMsg="We are sorry, ICICI bank does not service this pincode, please enter an alternate pin code";
var negativePincodeMsg = "We are sorry, American Express does not service this pincode, please enter an alternate pincode";
var invalidAddrMsg = "Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hyphens and ampersands."
var addressMsg = "Please provide your";
var nameMsg = "Please ensure your keyword limited to 15 characters";
var invalidAddrMsgICICI = "Please ensure your address contains only letters (A-Z, upper and lower case), digits (0-9), spaces, commas, forward slashes, hash,and apostrophe.";
var nameMsgICICI = "Please ensure your keyword limited to 26 characters";
var tempval;
// Adobe code starts here
var bankNamesAdobe = [];
var lifestyleBenefitsAdobe = [];
var sortByAdobe = "";
var cardNamesAdobe = "";
var cardsCountAdobe = 0;
var cardDetail = "";
var selCards = "";
var lastAccessedField = "";
var selectedCity = "";
var errorTrack ="";
var popupAbandonType="";
//   var redirectBank_card ="";
// var joiningFeesAdobe = "";
//Adobe code ends here


$(function () {

    var loggedInJPnumber = $("#jetpriviligemembershipNumber").val();

    $('.name_ul input, .name_ul_credit input').keydown(function (event) {
        var charCode = event.keyCode;
        if ((charCode > 64 && charCode < 91) || charCode == 8 || charCode == 9) {
            return true;
        } else {
            return false;
        }
    });
    $.fn.modal.Constructor.prototype.enforceFocus = function () {
    };
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.keyboard = false;

    /* validations */
    $.validator.addMethod("AlphabetsOnly", function (value, element) {
        console.log("inside validation");
        $('.error_box1').addClass('hidden');
        $('.error_box2').addClass('hidden');            
        return this.optional(element) || /^[a-z][a-z@\s]*$/i.test(value);
    });

    $.validator.addMethod('emailValidation', function (value, element) {
        $('.error_box1').addClass('hidden');
        console.log("inside email");    
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
    });

    $.validator.addMethod('mobileValidation', function (value, element) {
        console.log("mobile validation function add method");
//        $('.error_box1').addClass('hidden');
//            var phone = $('#socialPhone').val();
//            var mobile = $('#mobile').val();
//            console.log("phone>>>>"+phone);
//            console.log("mobile>>>"+mobile);

//        if(phone.charAt(0) == 7 || phone.charAt(0)== 8 || phone.charAt(0)== 9 || mobile.charAt(0) == 7 || mobile.charAt(0)== 8 || mobile.charAt(0)== 9)
//        {
//            $('.error_box1').addClass('hidden');
//            console.log("inside if codition mobile");
//            return this.optional(element) || /^[7-9][0-9]{9}$/.test(value);
//        }
//        else
//        {
//            $('.error_box1').removeClass('hidden');
//            console.log("inside else codition mobile");
//            return this.optional(element) || /^[7-9][0-9]{9}$/.test(value);
//        }
        
        $('.error_box1').addClass('hidden');
        return this.optional(element) || /^[7-9][0-9]{9}$/.test(value);
    });

    $.validator.addMethod('panValidation', function (value, element) {
        return this.optional(element) || /[a-zA-Z]{3}[Pp]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/.test(value);
//        return this.optional(element) || /^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/.test(value);
    });

    /*-------Vernost----------*/
    $.validator.addMethod('IciciPanValidation', function (value, element) {
        return this.optional(element) || /[a-zA-Z]{3}[Pp]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/.test(value);
    });
    /*-------Vernost End----------*/


    $.validator.addMethod('passwordChecking', function (value, element) {
        return this.optional(element) || /^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z])[\w~@#$%^&*+=`|{}:;!.?\"()\[\]-]{8,13}$/.test(value);
    });

    function addressFieldChecking(value) {
        return /^[A-Za-z0-9-&,\/\s]*$/.test(value);
    }
    function addressFieldCheckingicici(value) {
        return /^[A-Za-z0-9#',\/\s]*$/.test(value);
    }

    function onlyNumeric(value) {
        return /^[0-9]+$/.test(value);
    }

    function startsWithZero(value) {
        return /^0[0-9].*$/.test(value);
    }

    function validPinCode(value) {
        return /^[1-9][0-9]{5}.*$/.test(value);
    }

    function maxLength(value) {
        return /^.{1,24}$/.test(value);
    }

//    function hasalphabet(str) {
//      return /^[a-zA-Z()]+$/.test(str);
//    }
    /*----code by vernost Team---*/
    function icicimaxLength(value) {
        return /^.{3,40}$/.test(value);
    }
    /*====End======= */
    /* enroll pop up arrow*/
    $('.goTo_abtYourself_div,.go_To_Jp').on('click', function () {
        $('.enroll_popup').addClass('hidden');
        $('.jpNum_popup').removeClass('hidden');
        $('#enroll_error').addClass('hidden');
    });
    /* enroll pop up arrow*/

    /* enroll error message box*/
    $('#enrollForm,#enrollForm_mob_view').on('click', function () {
        var len = $('.error_list1 > li:visible').length;
        if (len >= 1)
            $('.modal').animate({scrollTop: 0}, 'slow');
    });
    /* enroll error message box*/

    /* open enroll pop up on click of enroll here*/
    $(document).on('click', '#enroll_here', function () {
        $('.jpNum_popup').addClass('hidden');
        $('.enroll_popup').removeClass('hidden');
        $('#enroll_error').addClass('hidden');
    });
    /* open enroll pop up on click of enroll here*/

    $('.datepicker_icon').on('click', function () {
        $('#dateOfBirth').datepicker("show");
//        $('#dateOfBirth').rules('remove');
        $('#dateOfBirth').addClass("ignore");

//         $("#amexBean").validate().element('#dateOfBirth').

    });
    var formicic = document.getElementById('iciciBean');

    if (formicic != null) {
//        var start = new Date();
//        start.setFullYear(start.getFullYear() - 65);
//        var end = new Date();
//        end.setFullYear(end.getFullYear() - 18);
//        var loggedInJPnumber = $("#jetpriviligemembershipNumber").val();
//        if (loggedInJPnumber != null) {
//            $('#dateOfBirth').datepicker({
//                dateFormat: 'mm-dd-yy',
//                changeYear: true,
//                changeMonth: true,
//                // yearRange: '1952:-18',
//                //maxDate: 0,
//                // defaultDate: '01-01-1952',
//
//                minDate: start,
//                maxDate: end,
//                yearRange: start.getFullYear() + ':' + end.getFullYear()
//            });
//        } else {
//            $('#dateOfBirth').datepicker({
//                dateFormat: 'mm-dd-yy',
//                changeYear: true,
//                changeMonth: true,
//                // yearRange: '1952:-18',
//                //maxDate: 0,
//                // defaultDate: '01-01-1952',
//
//                minDate: start,
//                maxDate: end,
//                yearRange: start.getFullYear() + ':' + end.getFullYear()
//            });
//        }

    } else {
        var start = new Date();
        start.setFullYear(start.getFullYear() - 65);
        var end = new Date();
        end.setFullYear(end.getFullYear() - 18);
        $('#dateOfBirth').datepicker({
            dateFormat: 'mm-dd-yy',
            changeYear: true,
            changeMonth: true,
            // yearRange: '1952:-18',
            //maxDate: 0,
            // defaultDate: '01-01-1952',
            onClose: function () {
              ///  alert('Datepicker Closed');
                if ($('#dateOfBirth').hasClass("ignore")) {
                    $('#dateOfBirth').removeClass("ignore");
                }
               $("#amexBean").validate().element('#dateOfBirth');
            },
            minDate: start,
            maxDate: end,
            yearRange: start.getFullYear() + ':' + end.getFullYear()
        });
    }

    $('.body_content').addClass('hidden');

    /* Pop up navigations */
    $('.goTo_City').on('click', function () {
        $('.dob_content').addClass('hidden');
        $('.city_content').removeClass('hidden');

        //        Adobe code starts here
        if ($(this).hasClass('BackButton'))
        {
            $.fn.AllScreensBackButtonClick('City Of Residence')
//                            console.log("...................Code For Back Button"); 
        } else
        {
            //console.log("...................Code For Next Button"); 
            //  $.fn.CityOfResidencePopUpClick(city);
        }
        //            Adobe code ends here 
    });
    $('.goTo_dob').on('click', function () {
        var city = $("#cityName").val();
        console.log('city>>'+city);
        if (city == "") {
            $("#error_msg_city").show().text("Please Select City");
        } else {
            $('.dob_content').removeClass('hidden');
            $('.city_content').addClass('hidden');
            $('.flightFreq_content').addClass('hidden');

            //Adobe code starts here
            if ($(this).hasClass('BackButton'))
            {
                $.fn.AllScreensBackButtonClick('Date Of Birth');
//                    console.log("...................Code For Back Button"); 
            } else
            {
//                     console.log("...................Code For Next Button"); 
                $.fn.CityOfResidencePopUpClick(city);
            }

            //Adobe code end here
        }
    });
    $('.goTo_flightFreq').on('click', function () {
        var dob = $("#date_of_birth").val();
        console.log("dob>>"+dob)
        if (dob == "") {
            $("#error_msg_dob").show().text("Please Select Date of Birth");
        } else {
            $('.dob_content').addClass('hidden');
            $('.lifeStyle_content').addClass('hidden');
            $('.flightFreq_content').removeClass('hidden');
            $('.flightRange_slider .rangeslider__fill').css('width', '0');
            $('.flightRange_slider .rangeslider__handle').css('left', '0');
            $('.flightRange_slider input[type="range"]').rangeslider('destroy');
            slider("flightFreq");
            $('.flightRange_slider input[type="range"]').val(flightFreq).change();
            $('.no_of_flights').html(flightFreq);
            $('.flightRange_slider .rangeslider__handle').html(" ");

            //Adobe call start here
            if ($(this).hasClass('BackButton'))
            {
                $.fn.AllScreensBackButtonClick('Fly Frequency');
//                  console.log("...................Code For Back Button"); 
            } else
            {
//                    console.log("...................Code For Next Button"); 
                $.fn.DOBUpClick(dob);
            }

            //Adobe call end here
        }
    });
    var testing;
    $('.goTo_lifePrivileges').on('click', function () {
        $('.flightFreq_content').addClass('hidden');
        $('.spendCapability_content').addClass('hidden');
        $('.lifeStyle_content').removeClass('hidden');
//        alert('test2'+$("#monthlySpend").val());
//        alert('back '+$("#monthlySpend").val());
        //Adobe code start here
        var flightRange = $("#flightRange").val();
        console.log('flightRange>>'+flightRange)
        
        if ($(this).hasClass('BackButton'))
        {
//                            alert("before setting"+testing);
            testing = $("#monthlySpend").val();
//                            alert('back adobe '+$("#monthlySpend").val());
            $.fn.AllScreensBackButtonClick('Lifestyle Preferences');
//                            console.log("...................Code For Back Button"); 
        } else
        {
//                            console.log("...................Code For Next Button"); 
            $.fn.HowOftenYouFlyClick(flightRange);
        }

        //Adobe code end here
    });
    $('.goTo_spendCapability').on('click', function () {
        
    	var valSpend = $(".spendCapability_monthly input").val();
    	 console.log('valSpend>>'+valSpend);
        
    	valSpend = valSpend.replace(/\,/g, '')
        console.log('valSpend>>'+valSpend);
    	
    	spendCapability = parseInt(valSpend);
    	console.log('spendCapability>>'+spendCapability);
    	
        $("#error_msg_lsbpArray").hide();
        $('#lsbpArray').val('');
        
        var arr = [];
        
        $('input:checkbox[name=lsbp_name]:checked').each(function () {
            arr.push($(this).val());
        });
        
        $('#lsbpArray').val(arr);
        
        if ($('#lsbpArray').val() == 0) {
            $("#error_msg_lsbpArray").show().text("Please Select Lifestyle Privileges");
        } 
        
        else {
            $('.lifeStyle_content').addClass('hidden');
            $('.annualIncome_content').addClass('hidden');
            $('.spendCapability_content').removeClass('hidden');
            $('.spendCapability_slider .rangeslider__fill').css('width', '0');
            $('.spendCapability_slider .rangeslider__handle').css('left', '0');
            $('.spendCapability_slider input[type="range"]').rangeslider('destroy');
            slider("spendCapability");
            
        
           
        if (testing == undefined) {
                $('.spendCapability_slider input[type="range"]').val(spendCapability).change();
            } 
        else {
                $('.spendCapability_slider input[type="range"]').val(testing).change();
            }

//            alert("result_Value :"+result_Value);
            $('.spendCapability_slider .rangeslider__handle').html("<span>" + result_Value + "</span>");
            $('.spendCapability_monthly input').val(result_Value);
            $('.spendCapability_slider .annualSpends_forFlights').html('<img alt="" class="rupee_white_icon" src=' + ctx + 'static/images/co-brand/rupee_white_icon.png>' + result_Value_year);

            //Adobe code Starts here
            var preference = $('#lsbpArray').val();
            var LifeStylePreferences = preference.split(',');
            var lsp = 0;
            var finalLSP = "";
            for (lsp = 0; lsp < LifeStylePreferences.length; lsp++) {

                if (finalLSP == "")
                {
                    finalLSP = $("#hdnchk_lsbp" + LifeStylePreferences[lsp]).val();
                } else
                {
                    finalLSP = finalLSP + "|" + $("#hdnchk_lsbp" + LifeStylePreferences[lsp]).val();
                }
            }
            //Call to lifecycle click
            if ($(this).hasClass('BackButton'))
            {
                $.fn.AllScreensBackButtonClick('Spends On Card');
//                            console.log("...................Code For Back Button"); 
            } else
            {
//                            console.log("...................Code For Next Button"); 
                $.fn.LifeStyleClick(finalLSP);
            }

        }
    });

    /* Capturing entered spends value in slider */
    $(document).on("keyup", ".spendCapability_monthly input", function (event) {
        var valSpend = $(".spendCapability_monthly input").val();


        var valSpendNum = '';
        valSpendNum = valSpend.replace(/\,/g, '');
        valSpendNum = parseInt(valSpendNum, 10);
        if (valSpendNum >= 5000 && valSpendNum <= 1500000) {

            // skip for arrow keys
            if (event.which == 37 || event.which == 39)
                return;
            // format number
            $(this).val(function (index, value) {

                $('.spendCapability_slider input[type="range"]').val(valSpendNum).change();

                var x = value.replace(/,/g, "");
                var lastThree = x.substring(x.length - 3);
                var otherNumbers = x.substring(0, x.length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
                $('.spendCapability_slider .rangeslider__handle').html("<span>" + res + "</span>");
                return res;

            });
        }
    });
    /* Capturing entered spends value in slider */

    /* Decimal and Alphabets are not allowing */

    $(document).on("keydown", ".spendCapability_monthly input", function (e) {

        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || (e.keyCode == 110) || (e.keyCode == 190)) {
                    e.preventDefault();
                }
            });
    /* Decimal and Alphabets are not allowing */
    $("#helpMeChooseClose").on("click", function () {
        if ($("#error_msg_city").is(':visible')) {
            $("#error_msg_city").hide();
        }
        if ($("#error_msg_dob").is(':visible')) {
            $("#error_msg_dob").hide();
        }
        if ($("#error_msg_lsbpArray").is(':visible')) {
            $("#error_msg_lsbpArray").hide();
        }
        if ($("#error_msg_expend").is(':visible')) {
            $("#error_msg_expend").hide();
        }
        if ($("#error_msg_annualIncome").is(':visible')) {
            $("#error_msg_annualIncome").hide();
        }
    });
    $("#CrdHP_pm-spend_next").on("click", function () {
        $('.spend_choose').trigger('keyup');
    });
    $('.spend_choose').on('keyup', function () {
    	
    	
        var valSpend = $(".spendCapability_monthly input").val();

        valSpend = valSpend.replace(/\,/g, '');
        if (valSpend < 5000 || valSpend > 1500000) {
            $("#error_msg_expend").show().text("Please enter spends value within the range");
            return false;
        }
        var someValue = 5000;
        var rem = parseInt(valSpend) % someValue;
        console.log('.spend_choose rem>>'+rem);
        if (rem != 0)
            $("#error_msg_expend").show().text("Please enter spends value in multiples of 5000");
        else
            $("#error_msg_expend").show().text("");
    });
    $('.goTo_annualIncome').on('click', function () {
        if ($("#error_msg_expend").text() == "") {
            $('.spendCapability_content').addClass('hidden');
            $('.annualIncome_content').removeClass('hidden');
//              alert('test2'+$("#monthlySpend").val());
            //Adobe code starts here
            var cardSpendsClick = $("#monthlySpend").val();
            $.fn.SpendsOnCardClick(cardSpendsClick);
            //Adobe code ends here  
        }
    });
    /* Pop up navigations */

    /* Modals in Recomm.cards page */

    $('.recommend_card').on('click', function () {
        $('.no_of_flights').html(flightFreq);
        $('#userDetails').modal('show');
    });

    /* filter modal for mobile view */
    $('.filter_img').on('click', function () {
        $('#filterPopup').modal('show');
    });
    /* filter modal for mobile view */

    $('.modal_overflow').on('shown.bs.modal', function () {
        $('.modal-open').css("overflow", "hidden");
    });
    $('.modal_overflow').on('hidden.bs.modal', function () {
        $('body').removeAttr('style');
    });

    /* close icon */
    $('.proceedPopup_close').on('click', function () {
        $(this).parents('.modal').modal('hide');
    });
    $('.close_filters').on('click', function () {
        $(this).parents('.modal').modal('hide');
    });
    /* close icon */

    $("#CrdHP_choose-own-card").click(function () {

        if (navigator.userAgent.match(/(iPod|iPhone|Android)/)) {
            window.scrollTo(200, 950) // first value for left offset, second value for top offset
        } else if (navigator.userAgent.match(/(iPad)/)) {
            window.scrollTo(200, 650) // first value for left offset, second value for top offset
        } else {
            $('html,body').animate({
                scrollTop: $(".filter_div").offset().top - 320},
                    2000);
        }
    });

    /* compare_card_holder freeze [ Calculating number of cards selected ] */
    $(document).on('click', '.select_compare input:checkbox', function () {
        var checks = [];
        selCards = "";
        $.each($(".select_compare input[name='check']:checked"), function () {
            checks.push($(this).val());
            //Adobe code start

            selCards = selCards + $('#cardName' + $(this).val()).text() + "|"
            //Adobe code end
        });
        checked_cards = checks.length;
    });
    /* compare_card_holder freeze [ Calculating number of cards selected ] - END */

    $(window).scroll(function () {
//    	console.log("scroll window");
        var scroll_top = $(window).scrollTop();
        var window_height = $(window).height();
        var docHeight = $(document).height();
        var body_height = docHeight - (scroll_top + window_height);

        /* compare_card_holder freeze - on scroll*/
        if ($('.compare_card_holder').length) {
            if (checked_cards >= 1) {
                if (scroll_top >= 600 && body_height > 400) {
                    $('.compare_card_holder').addClass('fixed_compare_card_holder');
                } else {
                    $('.compare_card_holder').removeClass('fixed_compare_card_holder');
                }

            } else {
                $('.compare_card_holder').removeClass('fixed_compare_card_holder');
                $('.compare_card_holder').addClass('hidden');
            }
        }
        /* compare_card_holder freeze */

        if ($('div.title_header').length) {

//        	console.log("$(div.title_header).length>>>>"+$('div.title_header').length);
            /* Header freeze */
            if ((navigator.userAgent.match(/(iPad)/)) && (scroll_top >= 700)) {
                $('.header_freeze').addClass('fixed');
            } else if ((scroll_top >= 630 && (scroll_top <= 4590)) && ($(window).width() > 768)) {
//                console.log("inside else if");
            	$('.header_freeze').addClass('fixed');
            } else {
                $('.header_freeze').removeClass('fixed');
            }
            /* Header freeze */
        }

        /* Page back to top */
        if (scroll_top >= 300) {
            $('.back_to_top').addClass('show_back_top');
            return false;
        } else {
            $('.back_to_top').removeClass('show_back_top');
        }
        /* Page back to top */
    });

    /* Page back to top */
    $('.back_to_top').on('click', function () {
        $('html,body').animate({scrollTop: 0}, 'slow');
        return false;
    });
    /* Page back to top */

    $(".joiningBenefit").on('click', function () {
        $(this).next().toggle();
    });

    /*Log in  by anand*/
    $(".logout li.arrow_down").on("click", function () {

        $(this).parents(".clearfix").next(".logout_btn").toggleClass("hide");
    });

    /**/

    /* Enroll now pop up []*/
    $('.enroll_div .title_select').on('change', function () {
        console.log("inside enroll div title select");
        var selected = $('.enroll_div 	.title_select').val();
        if (selected != 'Mr' && selected != 'Ms' && selected != 'Mrs' && selected != "") {
            $('.enroll_div .gender').removeClass('hidden');
            $('.enroll_div .title').css({'width': '37%'});
            $('.enroll_div .firstName_input').css({"margin-left": "0", "display": "block", "width": "100%"});
            if ($(window).width() <= 360) {
                $('.enroll_div .title').css({'width': '45%'});
            }
        } else {
            $('.enroll_div .gender').addClass('hidden');
            $('.error_box2').addClass('hidden');
            $('.enroll_div .title').css({'width': '37%'});
            $('.enroll_div .firstName_input').css({'width': '60%', 'margin-left': '2%', 'display': 'inline-block'});
            if ($(window).width() <= 360) {
                $('.enroll_div .title').css({'width': '45%'});
                $('.enroll_div .firstName_input').css({'width': '51%', 'display': 'inline-block'});
            }
        }
    });
    /* Enroll now pop up []*/

    /* Show All in Recommend Cards*/
    $('.showAll').click(function () {
        $('.rec_features').removeClass('hidden');
        $('.btn_holder').addClass("hidden");
    });

// function CheckAddress(addrDetails)
//{
//    console.log('CheckAddress.............'+addrDetails)
//   if(addrDetails =='RES')
//   {
//       var lResAddrss =$('#resedentialAddress').val()
//                                        var bRValue =/^[A-Za-z0-9#',\/\s]*$/.test(lResAddrss)
//                                       console.log('Residentail Address Valid: '+bPRValue);
//                                     if(bRValue)
// 					{
//                                             $('.resedentialAddress-error-div').html('');  //  CheckAddress('RES')
//                                            $('.resedentialAddress-error-div').addClass('hidden');
//                                         }
//                                 
//   }
//    if(addrDetails =='PRES')
//   {
//       var lPResAddrss =$('#permanentAddress').val()
//                                        var bPRValue =/^[A-Za-z0-9#',\/\s]*$/.test(lPResAddrss)
//                                       console.log('Permanant Residentail Address Valid: '+bPRValue);
//                                     if(bPRValue)
// 					{
//                                             $('.permanentAddr-error-div').html('');  // CheckAddress('PRES');
//                                            $('.permanentAddr-error-div').addClass('hidden');
//                                         }
//                                 
//   }
//  if(addrDetails =='COMP')
//   {
//       var lComAddrss =$('#companyAddress').val()
//                                        var bCValue =/^[A-Za-z0-9#',\/\s]*$/.test(lComAddrss )
//                                       console.log('Company Address Valid: '+bCValue);
//                                     if(bCValue)
// 					{
//                                             $('.companyAddr_div-error-div').html('');  // CheckAddress('COMP');
//                                            $('.companyAddr_div-error-div').addClass('hidden');
//                                         }
//                                 
//   }
//
//}

    /* Show All in Recommend Cards*/

    function getValue(param) {
//        console.log("inside get value method "+param);
        var yearly = (param * 12).toString();
        var monthly = param.toString();
        var lastThree = monthly.substring((monthly.length) - 3);
        var otherNumbers = monthly.substring(0, monthly.length - 3);
        if (otherNumbers != '')
            lastThree = ',' + lastThree;
        result_Value = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
//         alert("get value  :"+result_Value);
        lastThree = yearly.substring((yearly.length) - 3);
        otherNumbers = yearly.substring(0, yearly.length - 3);
        if (otherNumbers != '')
            lastThree = ',' + lastThree;
        result_Value_year = otherNumbers.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

    }

    function slider(slider_name) {
//        alert("slicer name "+slider_name);
    	console.log('slider_name>>'+slider_name);
        $('input[type="range"]').rangeslider({

            polyfill: false,
            // Default CSS classes
            rangeClass: 'rangeslider',
            disabledClass: 'rangeslider--disabled',
            horizontalClass: 'rangeslider--horizontal',
            verticalClass: 'rangeslider--vertical',
            fillClass: 'rangeslider__fill',
            handleClass: 'rangeslider__handle',
            // Callback function
            onInit: function () {

                $('.rangeslider__fill').css('width', '0');
                $('.rangeslider__handle').css('left', '0');


            },
            // Callback function  


            onSlide: function (position, value) {
//                console.log("Value :"+value);
                getValue(value);

                $('.spendCapability_monthly input').val(result_Value);
                $('.annualSpends_forFlights').html('<img alt="" class="rupee_white_icon" src=' + ctx + 'static/images/co-brand/rupee_white_icon.png>' + result_Value_year);
                if (slider_name == 'freeFlights_calc') {
                    freeFlights_calc = value;


                    $('.freeFlights_calc .rangeslider__handle').html("<span>" + result_Value + "</span>");
                    $('.freeFlights_calc .annualSpends_value').html('<img alt="" class="rupee_white_icon" src=' + ctx + 'static/images/co-brand/rupee_black_icon.jpg>' + result_Value_year);
                    $('.spendsSlider_popup .rangeslider__handle').html("<span>" + result_Value + "</span>");
                    $('.annualSpends .annualSpends_value').html('<img alt="" class="rupee_white_icon" src=' + ctx + 'static/images/co-brand/rupee_black_icon.jpg>' + result_Value_year);


                }
                if (slider_name == 'flightFreq') {
                	
                    flightFreq = value;
                    $('.no_of_flights').html(result_Value);
                    console.log('result_Value>>'+result_Value);
                    console.log('flightFreq>>'+flightFreq);
                }
                if (slider_name == 'spendCapability') {
                    spendCapability = value;
                    $('.spendCapability_slider  .rangeslider__handle').html("<span>" + result_Value + "</span>");
                    $('.spendCapability_monthly input').val(result_Value);
                }
                if (slider_name == 'joiningFees') {
                    joiningFees = value;
                    console.log("joiningFees>>"+joiningFees);
                    $('.slider_div  .rangeslider__handle').html("<span>" + result_Value + "</span>");
                }
                if (slider_name == 'spend_per_month') {
//                    console.log("slider_name before" + spend_per_month);
                    spend_per_month = value;
//                    console.log(spend_per_month + "  :slider_name" + value);
//                    if(!isNaN(spend_per_month)){
//                        console.log("inside if condition");
////                      tempval=value; 
//                      spend_per_month=tempval;
//                    }else{
//                        spend_per_month = value;
//                        tempval=value;  
//                        console.log("inside else condition");
//                    }
//                    console.log("outside else condition"+tempval);

//                    console.log(result_Value+"inside value"+value);
//                    $('.freeFlights_calc_compare  .rangeslider__handle').html("<span>" + result_Value + "</span>");
//                    $('.freeFlights_calc_compare .annualSpends_value').html('<img alt="" class="rupee_white_icon" src=' + ctx + 'static/images/co-brand/rupee_black_icon.jpg>' + result_Value_year);
//                    $('.freeFlights_calc  .rangeslider__handle').html("<span>" + result_Value + "</span>");
//                    $('.freeFlights_calc .annualSpends_value').html('<img alt="" class="rupee_white_icon" src=' + ctx + 'static/images/co-brand/rupee_black_icon.jpg>' + result_Value_year);
                }

                value_common = value;

            },
            // Callback function
            onSlideEnd: function (position, value) {

                if (slider_name == 'spendCapability') {
                    var spendTextValue = $('.spendCapability_monthly input').val().replace(/\,/g, '');
                    var rem = parseInt(spendTextValue) % 5000;
                    if (rem != 0) {
                        $("#error_msg_expend").show().text("Please enter spends value in multiples of 5000");
                    } else {
                        $("#error_msg_expend").show().text("");
                    }
                }
            }
        });
        return value_common;
    }

    /* moble view filter and sort functions*/
    $('.sort').on('click', function () {
        $('.sort').css('background', '#ccc');
        $('.filter').css('background', '#fff');
        $('.filter_area').addClass('hidden');
        $('.filter_holder').addClass('hidden');
        $('.sort_tab').removeClass('hidden');
        $('.banks_listing').addClass('hidden');
        $('.slider_div').addClass('hidden');
        $('.life_benefits_list').addClass('hidden');

    });
    $('.filter').on('click', function () {
        console.log('inside filter');
        $('.filter').css('background', '#ccc');
        $('.sort').css('background', '#fff');
        $('.filter').css('background', '#ccc');
        $('.filter_area').removeClass('hidden');
        $('.sort_tab').addClass('hidden');
        $('.filter_holder').removeClass('hidden');
        $('.sort_listing').addClass('hidden');
    });
    /* moble view filter and sort functions*/

    /* Hide & Show of Filters */
    $('.banks').click(function () {
        console.log(".bank called");
    	$('.banks_listing').removeClass('hidden');
        $('.slider_div').addClass('hidden');
        $('.sort_listing').addClass('hidden');
        $('.life_benefits_list').addClass('hidden');
        $('.freeFlight_spends_calc').addClass('hidden');
        $('.tooltip_content').addClass('hidden');
    });

    $('input[type="range"]').rangeslider('destroy');
    slider("freeFlights_calc");
    $('.freeFlights_calc .rangeslider__handle').html("<span>" + freeFlights_calc.toLocaleString() + '</span>');
    $('.freeFlights_calc input[type="range"]').val(freeFlights_calc).change();


    $(document).ready(function () {

        $('.freeFlights_calc_compare').click();
        $('#CrdRC_btn_cal-ur-ff-asper-spend').click();

    });
    var temp = 0;
    $('#CrdRC_btn_cal-ur-ff-asper-spend').click(function () {
//      alert("tetsfsdfshj");
        $('.tooltip_content').addClass('hidden');
        $('.spends_calc input[type="range"]').rangeslider('destroy');
//        temp = spend_per_month;
        slider("spend_per_month");
//        $('.spends_calc .rangeslider__handle').html("<span>" + spend_per_month.toLocaleString() + "</span>");
//        $('.spends_calc input[type="range"]').val(spend_per_month).change();
    });

    $('.freeFlights_calc_compare').click(function () {
        $('.tooltip_content').addClass('hidden');

        $('.spends_calc input[type="range"]').rangeslider('destroy');
//        slider("spend_per_month");
//        $('.spends_calc .rangeslider__handle').html("<span>" + spend_per_month.toLocaleString() + "</span>");
//        $('.spends_calc input[type="range"]').val(spend_per_month).change();
//        alert("change "+spend_per_month);
//        $('.freeFlights_calc_compare .annualSpends_value').html('<img alt="" class="rupee_white_icon" src=' + ctx + 'static/images/co-brand/rupee_black_icon.jpg>' + result_Value_year);
    });

    $('.sort_by').click(function () {
        
        if($('.sort_listing').hasClass('hidden')){
            console.log('inside remove');
            $('.sort_listing').removeClass('hidden');
        }
        else{
            console.log('inside add');
            $('.sort_listing').addClass('hidden');
        }
        
        
        $('.banks_listing').addClass('hidden');
        $('.slider_div').addClass('hidden');
        $('.life_benefits_list').addClass('hidden');
        $('.freeFlight_spends_calc').addClass('hidden');
        $('.tooltip_content').addClass('hidden');
    });

    $('.life_benefits').click(function () {
        $('.life_benefits_list').removeClass('hidden');
        $('.slider_div').addClass('hidden');
        $('.banks_listing').addClass('hidden');
        $('.freeFlight_spends_calc').addClass('hidden');
        $('.sort_listing').addClass('hidden');
        $('.tooltip_content').addClass('hidden');
    });

    $("#sort_list_div, #recommendedCards_div, .features_desc").on("click", ".free_flights_desc", function () {
        $('#spends_slider').modal('show');
        $('.spendsSlider_popup .rangeslider__handle').html("<span>" + freeFlights_calc.toLocaleString() + '</span>');
        $('.spendsSlider_popup input[type="range"]').val(freeFlights_calc).change();
        compareCards_boolean = 0;
    });

    $('document').on('click', function () {
        console.log('documnet');
        $('.free_flights_desc').css('display', 'none');
        $('.free_flights_div').css('display', 'block');
        $('.close_filter').trigger('click');
    });

    $('.sort_div').click(function () {
        console.log('inside sort_listing hidden remove');
        if($('.sort_listing').hasClass('hidden') === true){
            console.log('inside if');
            $('.sort_listing').removeClass('hidden');
//              $('.parent_div').removeClass('hidden');
        }
        else{
            console.log('inside else');
        }
        
    });

    /* Close slider after selection */
    $('.filter_button').click(function () {
        console.log('isndie filter button');
        $(this).parents('.parent_div').addClass('hidden');
    });
    /* Close slider after selection */

    /* Tooltip */
    $(document).on('click', '.tooltips', function () {
    	console.log("tool tips click");
        $('.tooltip_content').removeClass('hidden');
        $('.slider_div').addClass('hidden');
        $('.banks_listing').addClass('hidden');
        $('.sort_listing').addClass('hidden');
        $('.freeFlight_spends_calc').addClass('hidden');
        $('.life_benefits_list').addClass('hidden');
        $('.tooltipIcon').attr('src', ctx + 'static/images/co-brand/info_grayicon1.png');
        $(this).children('.tooltipIcon').attr('src', ctx + 'static/images/co-brand/inrimg.png');
        $('.tooltips .tooltip_content').css("visibility", "hidden");
        $(this).children('.tooltip_content').css({"visibility": "visible", "z-index": "3"});
    });
    $(document).on('click', '.close_toolTip', function (e) {
    	console.log("close tool tips click");
        $('.tooltips .tooltip_content').css("visibility", "hidden");
        $('.tooltipIcon').attr('src', ctx + 'static/images/co-brand/info_grayicon1.png');
        e.stopImmediatePropagation();
    });
    /* Tooltip */

    $(document).on('click', '.close_filter', function () {
        $(this).parents('.parent_div').addClass('hidden');
    });
    $(document).on('click', '.close_div', function () {
        $(this).parents().modal('hide');
    });

    $('.joiningFees').click(function () {
        console.log("joiningFees called");
    	$('.slider_div').removeClass('hidden');
        $('.banks_listing').addClass('hidden');
        $('.freeFlight_spends_calc').addClass('hidden');
        $('.sort_listing').addClass('hidden');
        $('.life_benefits_list').addClass('hidden');
        $('.tooltip_content').addClass('hidden');
        $('.slider_div .rangeslider__fill').css('width', '0');
        $('.slider_div .rangeslider__handle').css('left', '0');
        $('.slider_div input[type="range"]').rangeslider('destroy');
        slider("joiningFees");
        $('.slider_div .rangeslider__handle').html("<span>" + joiningFees.toLocaleString() + '</span>');
        $('.slider_div input[type="range"]').val(joiningFees).change();
        $('.freeFlights_calc input[type="range"]').val(freeFlights_calc).change();
        $('#CrdRC_btn_cal-ur-ff-asper-spend input[type="range"]').val(temp).change();
    });
    /* Hide & Show of Filters [end]*/

    /* About yourself page - validations*/

    $('.addressProof input[type="radio"]').change(function () {
        var address = ($(this).val());
        if (address == "No") {
//            alert('test')
            $('.permanentAddr').removeClass('hidden');
            $('.permanentAddr-error-div').removeClass('hidden');
            $('.permanentAddr_div').css('display','block');
            $('.permanentAddr_div').addClass('hidden'); // rpm 21
        }
        if (address == "Yes") {
//            alert('test2')
            $('.permanentAddr').addClass('hidden');
            $('.permanentAddr-error-div').addClass('hidden');
            $('.permanentAddr_div').css('display','none');
//           $('.permanentAddr-error-div').hide();


        }
    });
    $(document).on('focus', '.name_inputBox', function () {

        console.log("Inside name-inputBox");
    	$('.name_ul').removeClass('hidden');
        $('.name_inputBox').addClass('hidden');
        $('.firstName').focus();
        $('#fullName-error').addClass('hidden');
    });

    $(document).on('focus', '.currentResidentialAddress', function () {
        $('.residentialAddr_div').removeClass('hidden');
        $('.currentResidentialAddress').addClass('hidden');
        $('.residentAddressField1').focus();
        $('#resedentialAddress-error').addClass('hidden');
    });
    $(document).on('focus', '.permanentAddr', function () {
        $('.permanentAddr_div').removeClass('hidden');
        $('.permanentResidentialAddress').addClass('hidden');
        $('.permanent_AddressField1').focus();
    });
    $(document).on('focus', '.companyAddress_box', function () {
       
        $('.companyAddr_div').removeClass('hidden');
        $('.companyAddress_box').addClass('hidden');
        $('.companyAddressField1').focus();
        $('#companyAddress-error').addClass('hidden');
   
    });

    $(document).on('focus', '.homeNumber', function () {
        console.log('home number');
        $('.phoneNum_div').removeClass('hidden');
       
        $('.homeNumber').addClass('hidden');
        $('.companyAddr_div').addClass('hidden');
        $('.companyAddress_box').removeClass('hidden');
//        companyFullAddress = residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin;
//        $('.companyAddress_box').val(companyFullAddress);
        $('.std input').focus();
        $('#homeNumber-error').css('display', 'none');
    });
    $(document).on('focus', '.officeNumber', function () {
        $('.officePhoneNum_div').removeClass('hidden');
        $('.officeNumber').addClass('hidden');
        $('.std_office input').focus();
    });
    $(document).on('focus', '#OPhone', function () {
        $("#OStd-error").hide();
        $('.officeNumber-error-div').addClass('hidden');

    });
    $(document).on('focus', '#OStd', function () {
        $("#OPhone-error").hide();
        $('.officeNumber-error-div').addClass('hidden');

    });

    $(document).on('focus', '#phone', function () {

        $('.homeNumber-error-div').addClass('hidden');

    });
    $(document).on('focus', '#std', function () {

        $('.homeNumber-error-div').addClass('hidden');

    });

    $('#state1,#city').on('focus', function () {
        console.log("at city state focus")
        $('.resedentialAddress-error-div').addClass('hidden');

    });
    $('#PState, #PCity').on('focus', function () {

        $('.permanentAddr-error-div').addClass('hidden');

    });

    $('#OState, #OCity').on('focus', function () {
    
        $('.companyAddr_div-error-div').addClass('hidden');

    });
    
    function hasalphabet(str) {
        return /^[a-zA-Z()]+$/.test(str);
    }
    $('.firstName').focusout(function () {
//         console.log("firstname focusout")
        $('#fullName-error').addClass('hidden');
        var fName = $('.firstName').val();
        var lName = $('.lastName').val();
        var form = document.getElementById('iciciBean');
        if (form != null) {
            // for icici
            var fNameLength = fName.length;
            var appendQuery = fNameLength >= 27 ? "First Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (fName != "" && fNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.firstName').val(fName.substring(0, 26));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }

            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
        } else {
            //for amex
            var fNameLength = fName.length;
            var appendQuery = fNameLength >= 16 ? "First Name" : "";
            var msg = nameMsg.replace('keyword', appendQuery);
            if (fName != "" && fNameLength >= 16) {
                if (loggedInJPnumber == "") {
                    $('.firstName').val(fName.substring(0, 15));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
//            if(fName != "" && fNameLength <=2){
//               
//                var msg = "Please en limited to 15 characters";
//                 $('.fullName-error-div').html(msg);
//                $('.fullName-error-div').addClass('hidden');
//            }else{
//                
//                
//            }

//            else {
//           
//                
//                if (fName == "") {
//                    $('.fullName-error-div').html('Please Enter Full Name').css("color", "#ff0000");
//                    $('.fullName-error-div').removeClass('hidden');
//                }
//                else {
//                    $('.fullName-error-div').html('');
//                    $('.fullName-error-div').addClass('hidden');
//                }
//            }
        }
    });
    $('.firstName').on('click', function () {
//         console.log("firstname click")
        $('#fullName-error').addClass('hidden');
        var fName = $('.firstName').val();
        var lName = $('.lastName').val();
        var form = document.getElementById('iciciBean');

        if (form != null) {
            //for icici
            var fNameLength = fName.length;
            var appendQuery = fNameLength >= 27 ? "First Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if ((fName != "" && fNameLength >= 27)) {
                if (loggedInJPnumber == "") {
                    $('.firstName').val(fName.substring(0, 26));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }





        } else {
            //for amex
            var fNameLength = fName.length;
            var appendQuery = fNameLength >= 16 ? "First Name" : "";
            var msg = nameMsg.replace('keyword', appendQuery);
            if ((fName != "" && fNameLength >= 16)) {
                if (loggedInJPnumber == "") {
                    $('.firstName').val(fName.substring(0, 15));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
//            else {
//             
//                if (fName == "" ) {
//                    $('.fullName-error-div').html('Please Enter Full Name').css("color", "#ff0000");
//                    $('.fullName-error-div').removeClass('hidden');
//                }
//                else {
//                    $('.fullName-error-div').html('');
//                    $('.fullName-error-div').addClass('hidden');
//                }
//            }
        }
    });

    $('.firstName').on('keyup', function () {
//         console.log("firstname keyup")
        var fName = $('.firstName').val();
        var lName = $('.lastName').val();
        var form = document.getElementById('iciciBean');
        $('.fullName-error-div').addClass('hidden');
        if (form != null) {
            // for icici
            var fNameLength = fName.length;
            var appendQuery = fNameLength >= 27 ? "First Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if ((fName != "" && fNameLength >= 27)) {
                if (loggedInJPnumber == "") {
                    $('.firstName').val(fName.substring(0, 26));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
        } else {
            //for amex
            var fNameLength = fName.length;
            var appendQuery = fNameLength >= 16 ? "First Name" : "";
            var msg = nameMsg.replace('keyword', appendQuery);
            if ((fName != "" && fNameLength >= 16)) {
                if (loggedInJPnumber == "") {
                    $('.firstName').val(fName.substring(0, 15));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
//            else {
//             
//                if (fName == "" ) {
//                    $('.fullName-error-div').html('Please Enter Full Name').css("color", "#ff0000");
//                    $('.fullName-error-div').removeClass('hidden');
//                }
//                else {
//                    $('.fullName-error-div').html('');
//                    $('.fullName-error-div').addClass('hidden');
//                }
//            }

        }
    });
    $('.middleName').focusout(function () {
//         console.log("middlename focusout")
        var mName = $('.middleName').val();
        var mNameLength = mName.length;
        var form = document.getElementById('iciciBean');
        $('.fullName-error-div').addClass('hidden');
        if (form != null) {
            //for icici
            var appendQuery = mNameLength >= 27 ? "Middle Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (mName != "" && mNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.middleName').val(mName.substring(0, 26));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                if (mName != "" && mNameLength <= 26) {
                    $('.fullName-error-div').html('');
                    $('.fullName-error-div').addClass('hidden');
                }
            }
        } else {
            //for amex
            var mNameLength = mName.length;
            var appendQuery = mNameLength >= 16 ? "Middle Name" : "";
            var msg = nameMsg.replace('keyword', appendQuery);
            if (mName != "" && mNameLength >= 16) {
                if (loggedInJPnumber == "") {
                    $('.middleName').val(mName.substring(0, 15));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                if (mName != "" && mNameLength <= 15) {
                    $('.fullName-error-div').html('');
                    $('.fullName-error-div').addClass('hidden');
                }
//                if(mName==""){
//                   $('.fullName-error-div').html('');
//                    $('.fullName-error-div').addClass('hidden');  
//                }
            }
        }
    });

    $('.middleName').on('click', function () {
//        console.log("midename click")
        var mName = $('.middleName').val();
        var mNameLength = mName.length;
        var form = document.getElementById('iciciBean');
        $('.fullName-error-div').addClass('hidden');
        if (form != null) {
            //for icici
            var appendQuery = mNameLength >= 27 ? "Middle Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (mName != "" && mNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.middleName').val(mName.substring(0, 26));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                if (mName != "" && mNameLength <= 26) {
                    $('.fullName-error-div').html('');
                    $('.fullName-error-div').addClass('hidden');
                }
            }
        } else {
            // for amex
            var mNameLength = mName.length;
            var appendQuery = mNameLength >= 16 ? "Middle Name" : "";
            var msg = nameMsg.replace('keyword', appendQuery);

//            if (mName == "") {
//                $('.fullName-error-div').html('');
//                $('.fullName-error-div').addClass('hidden');
//            }
            if (mName != "" && mNameLength >= 16) {
                if (loggedInJPnumber == "") {
                    $('.middleName').val(mName.substring(0, 15));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                if (mName != "" && mNameLength <= 15) {
                    $('.fullName-error-div').html('');
                    $('.fullName-error-div').addClass('hidden');
                }
//                 if(mName==""){
//                   $('.fullName-error-div').html('');
//                    $('.fullName-error-div').addClass('hidden');  
//                }

            }
        }
    });

    $('.middleName').on('keyup', function () {
//        console.log("midlename keyup")
        var mName = $('.middleName').val();
        var mNameLength = mName.length;
        var form = document.getElementById('iciciBean');
        $('.fullName-error-div').addClass('hidden');
        if (form != null) {
            //for icici
            var appendQuery = mNameLength >= 27 ? "Middle Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (mName != "" && mNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.middleName').val(mName.substring(0, 26));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                if (mName != "" && mNameLength <= 26) {
                    $('.fullName-error-div').html('');
                    $('.fullName-error-div').addClass('hidden');
                }
            }
        } else {
            //for amex
            var mNameLength = mName.length;
            var appendQuery = mNameLength >= 16 ? "Middle Name" : "";
            var msg = nameMsg.replace('keyword', appendQuery);
            if (mName != "" && mNameLength >= 16) {
                if (loggedInJPnumber == "") {
                    $('.middleName').val(mName.substring(0, 15));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                if (mName != "" && mNameLength <= 15) {
                    $('.fullName-error-div').html('');
                    $('.fullName-error-div').addClass('hidden');
                }
//                if(mName==""){
//                   $('.fullName-error-div').html('');
//                    $('.fullName-error-div').addClass('hidden');  
//                }
            }
        }
    });

    $('.lastName').focusout(function () {
//        console.log("lastname focusout")
        var fName = $('.firstName').val();
        var lName = $('.lastName').val();
        var lNameLength = lName.length;
        var form = document.getElementById('iciciBean');
        $('.fullName-error-div').addClass('hidden');
        if (form != null) {
            //for icici
            var appendQuery = lNameLength >= 27 ? "Last Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (lName != "" && lNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.lastName').val(lName.substring(0, 26));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
        } else {
            //for amex
            var lNameLength = lName.length;
            var appendQuery = lNameLength >= 16 ? "Last Name" : "";
            var msg = nameMsg.replace('keyword', appendQuery);
            if (lName != "" && lNameLength >= 16) {
                if (loggedInJPnumber == "") {
                    $('.lastName').val(lName.substring(0, 15));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
//            else {
//                if (lName == "") {
//                    
//                    $('.fullName-error-div').html('Please Enter Full Name').css("color", "#ff0000");
//                    $('.fullName-error-div').removeClass('hidden');
//                }
//                else {
//                    $('.fullName-error-div').html('');
//                    $('.fullName-error-div').addClass('hidden');
//                }
//            }
        }
    });
    $('.lastName').on('click', function () {
//        console.log("lastname click")
        var fName = $('.firstName').val();
        var lName = $('.lastName').val();
        var lNameLength = lName.length;
        var form = document.getElementById('iciciBean');
        $('.fullName-error-div').addClass('hidden');
        if (form != null) {
            //for icici
            var appendQuery = lNameLength >= 27 ? "Last Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (lName != "" && lNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.lastName').val(lName.substring(0, 26));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
        } else {
            //for amex
            var lNameLength = lName.length;
            var appendQuery = lNameLength >= 16 ? "Last Name" : "";
            var msg = nameMsg.replace('keyword', appendQuery);
            if (lName != "" && lNameLength >= 16) {
                if (loggedInJPnumber == "") {
                    $('.lastName').val(lName.substring(0, 15));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
//            else {
//              
//                if (lName == "") {
//                    $('.fullName-error-div').html('Please Enter Full Name').css("color", "#ff0000");
//                    $('.fullName-error-div').removeClass('hidden');
//                }
//                else {
//                    $('.fullName-error-div').html('');
//                    $('.fullName-error-div').addClass('hidden');
//                }
//            }
        }
    });



    $('.lastName').on('keyup', function () {
//        console.log("lastname keyup")
        var fName = $('.firstName').val();
        var lName = $('.lastName').val();
        var lNameLength = lName.length;
        var form = document.getElementById('iciciBean');
        $('.fullName-error-div').addClass('hidden');
        if (form != null) {
            //for icici
            var appendQuery = lNameLength >= 27 ? "Last Name" : "";
            var msg = nameMsgICICI.replace('keyword', appendQuery);
            if (lName != "" && lNameLength >= 27) {
                if (loggedInJPnumber == "") {
                    $('.lastName').val(lName.substring(0, 26));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
        } else {
            //for amex
            var lNameLength = lName.length;
            var appendQuery = lNameLength >= 16 ? "Last Name" : "";
            var msg = nameMsg.replace('keyword', appendQuery);
            if (lName != "" && lNameLength >= 16) {
                if (loggedInJPnumber == "") {
                    $('.lastName').val(lName.substring(0, 15));
                    $('.fullName-error-div').html(msg).css("color", "#ff6e00");
                    $('.fullName-error-div').removeClass('hidden');
                } else {
                    $('.fullName-error-div').html(msg).css("color", "#ff0000");
                    $('.fullName-error-div').removeClass('hidden');
                }
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
//            else {
//            
//                if (lName == "") {
//                    $('.fullName-error-div').html('Please Enter Full Name').css("color", "#ff0000");
//                    $('.fullName-error-div').removeClass('hidden');
//                }
//                else {
//                    $('.fullName-error-div').html('');
//                    $('.fullName-error-div').addClass('hidden');
//                }
//            }
        }
    });

    /*********END***********/



    $('.name_ul').focusout(function () {
//        console.log(".name_ul focusout")
        var fName = $('.firstName').val();
        var lName = $('.lastName').val();
        var mName = $('.middleName').val();

        $('#fullName-error').addClass('hidden');
        $("#fname-error").addClass('hidden');
        var form = document.getElementById('iciciBean');
        var fullname = fName + lName + mName;
        if (form != null) {
            var checkName = hasalphabet(fullname);
//            
            if (fullname != "" && checkName == false) {

                $('.fullName-error-div').html("Please Enter Alphabets only.").css("color", "#ff0000");
                $('.fullName-error-div').removeClass('hidden');
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
            if (fName == "") {

                $('.fullName-error-div').html('Please Enter Full Name').css("color", "#ff0000");
                $('.fullName-error-div').removeClass('hidden');
            } else if (fName == "") {

                $('.fullName-error-div').html('Please Enter first Name');
                $('.fullName-error-div').removeClass('hidden');
            } else if (lName == "") {

                $('.fullName-error-div').html('Please Enter Full Name').css("color", "#ff0000");
                $('.fullName-error-div').removeClass('hidden');

            }
//       
        } else {
            var checkName = hasalphabet(fullname);

            if (fullname != "" && checkName == false) {
                console.log("iuuiui")
                $('.fullName-error-div').html("Please Enter Alphabets only.").css("color", "#ff0000");
                $('.fullName-error-div').removeClass('hidden');
            } else {
                $('.fullName-error-div').html('');
                $('.fullName-error-div').addClass('hidden');
            }
            if (fName == "" || lName == "") {

                $('.fullName-error-div').html('Please Enter Full Name').css("color", "#ff0000");
                $('.fullName-error-div').removeClass('hidden');
            } else if (fName.length <= 0) {
                //code by Aravind 
                $('.fullName-error-div').html('First Name must be Minimum 1 charecters').css("color", "#ff0000");
                $('.fullName-error-div').removeClass('hidden');
            } else if (lName.length <= 1) {
                $('.fullName-error-div').html('Last Name cannot be less than 2 characters').css("color", "#ff0000");
                $('.fullName-error-div').removeClass('hidden');
            }
        }

    });

    /*code by vernost Team*/
    $('.firstName, .lastName').on('keyup', function () {
        var lName = $('.lastName').val();
        var fName = $('.firstName').val();

    });
    /***END**/
    function permIciciPinCheck(pinCode){
//        var pinCode = $(this).val();
            var cpNo = $("#cpNo").val();
            if (pinCode != "" && pinCode.length == 6) {
//                $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
//                    if (status == "success" && data != "excluded") {
                       
                        $.get(ctx+"getCity1?pinCode=" + pinCode, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                var json = JSON.parse(data);
//                                $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
//                                    if (status == "success" && data.length != 2) {
//                                        $("#PCity").val(json.city);
//                                        $("#PState").val(json.state);
//                                        $("#hcity1").val(json.city);
//                                        $("#hstate1").val(json.state);
//                                        $('#PCity').attr("disabled", true);
//                                        $('#PState').attr("disabled", true);
//                                       
//                                           CheckAddress('PRES');
//                                        //$('.permanentAddr-error-div').addClass('hidden');
//                                        //$(".permanentAddr-error-div").html('');
//                                       
//                                        if ($("#PCity").val() == null || $("#PCity").val() == '') {
//                                            $('#PCity').empty();
//                                            $('select#PCity').append('<option>' + json.city + '</option>');
//                                        }
//                                    } else {
//                                        $('.permanentAddr-error-div').html(pincodemsg);
//                                        $('.permanentAddr-error-div').removeClass('hidden');
//                                        $("#PCity").val("");
//                                        $("#PState").val("");
//                                        $('#PCity').attr("disabled", false);
//                                        $('#PState').attr("disabled", false);
//                                    }
//                                });
residentAddressField1 = $('.permanent_AddressField1').val();
        residentAddressField2 = $('.permanent_AddressField2').val();
        residentAddressField3 = $('.permanent_AddressField3').val();
        
                /**
                 * Validation of address and length between 3 to 40;
                 */
                 var isValidAddress = checkAddressLengthICICIPermant(residentAddressField1, residentAddressField2, residentAddressField3);
                 if (!isValidAddress) {
                    $('.permanentAddr-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line').css("color", "#ff0000");
                    $('.permanentAddr-error-div').removeClass('hidden');
                 }
                
                /**
                 * Validation of special characters
                 */
                 if (!addressFieldCheckingicici(residentAddressField1) || !addressFieldCheckingicici(residentAddressField2) || !addressFieldCheckingicici(residentAddressField3)) {
                    console.log("55")
                    $('.permanentAddr-error-div').html(invalidAddrMsgICICI).css("color", "#ff0000");
                    $('.permanentAddr-error-div').removeClass('hidden');
                }

                            } else {
                                $('.permanentAddr-error-div').html(negativeIciciPincodeMsg);
                                $('.permanentAddr-error-div').removeClass('hidden');
                                $("#PState").val("");
                                //---------change here------------
                                $('#PCity').attr("disabled", true);
                                $('#PState').attr("disabled", true);
                                //--------change here end----------
                            }
                        });
//                    } else {
//                        $('.permanentAddr-error-div').removeClass('hidden');
//                        $(".permanentAddr-error-div").html(pincodemsg);
//                        $("#PCity").val("");
//                        $("#PState").val("");
//                    }
//                });
            } else {
                $("#PCity").val("");
                $("#PState").val("");
                $('#PCity').attr("disabled", false);
                $('#PState').attr("disabled", false);
            }
    }
    function companyIciciPinCheck(pinCode){
        
//        var pinCode = $(this).val();
            var cpNo = $("#cpNo").val();
            if (pinCode != "" && pinCode.length == 6) {
//                $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
//                    if (status == "success" && data != "excluded") {
                        $.get(ctx+"getCity1?pinCode=" + pinCode, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                var json = JSON.parse(data);
//                                $.get("${applicationURL}getICICIStateByCity?city=" + json.city, function (data, status) {
//                                    if (status == "success" && data.length != 2) {
//                                        $("#OCity").val(json.city);
//                                        $("#OState").val(json.state);
//                                        $("#hcity2").val(json.city);
//                                        $("#hstate2").val(json.state);
//                                        $('#OCity').attr("disabled", true);
//                                        $('#OState').attr("disabled", true);
//                                       
//                                     
//                                        //$('.companyAddr_div-error-div').html('');
//                                        //$('.companyAddr_div-error-div').addClass('hidden');
//                                        CheckAddress('COMP');
//                                    
//                                        if ($("#OCity").val() == null || $("#OCity").val() == '') {
//                                            $('#OCity').empty();
//                                            $('select#OCity').append('<option>' + json.city + '</option>');
//                                        }
//                                    } else {
//                                        $('.companyAddr_div-error-div').html(pincodemsg);
//                                        $('.companyAddr_div-error-div').removeClass('hidden');
//                                        $("#OCity").val("");
//                                        $("#OState").val("");
//                                        $('#OCity').attr("disabled", false);
//                                        $('#OState').attr("disabled", false);
//                                    }
//                                });
                                          var residentAddressField1 = $('.companyAddressField1').val();
					 var residentAddressField2 = $('.companyAddressField2').val();
					 var residentAddressField3 = $('.companyAddressField3').val();
        
				 /**
				 * Validation of address and length between 3 to 40;
				 */
				 var isValidAddress = checkAddressLengthICICICompany(residentAddressField1, residentAddressField2, residentAddressField3);
				 if (!isValidAddress) {
					$('.companyAddr_div-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line').css("color", "#ff0000");
					$('.companyAddr_div-error-div').removeClass('hidden');
				 }
                
				/**
				 * Validation of special characters
				 */
				 if (!addressFieldCheckingicici(residentAddressField1) || !addressFieldCheckingicici(residentAddressField2) || !addressFieldCheckingicici(residentAddressField3)) {
						  console.log("55","===>",$('.companyAddr_div-error-div').is(':hidden'))
//                          if($('.companyAddr_div-error-div').is(':hidden') == true){
					   $('.companyAddr_div-error-div').html(invalidAddrMsgICICI).css("color", "#ff0000");
					   $('.companyAddr_div-error-div').removeClass('hidden');
//                     }
//                     else{
//                         console.log("55","===>",$('.companyAddr_div-error-div').is(':hidden'))
//                         console.log("already used")
//                     }
				 }
                            } else {
                                $('.companyAddr_div-error-div').html(negativeIciciPincodeMsg);
                                $('.companyAddr_div-error-div').removeClass('hidden');
                                $("#OCity").val("");
                                $("#OState").val("");
                                //---------change here--------
                                $('#OCity').attr("disabled", true);
                                $('#OState').attr("disabled", true);
                                //----------change here end--------
                            }
                        });
//                    } else {
//                        $('.companyAddr_div-error-div').html(pincodemsg);
//                        $('.companyAddr_div-error-div').removeClass('hidden');
//                        $("#OCity").val("");
//                        $("#OState").val("");
//                    }
//                });
            } else {
                $("#OCity").val("");
                $("#OState").val("");
                $('#OCity').attr("disabled", false);
                $('#OState').attr("disabled", false);
            }
    }
function residentIciciPinCheck(pinCode){
//            var pinCode = $(this).val();
            var cpNo = $("#cpNo").val();
            var cCity;
            var cState;
            if (pinCode != "" && pinCode.length == 6) {
                //$(".ui-menu-item").hide();
//                $.get("${applicationURL}icicicheckExcludePincode?pinCode=" + pinCode + "&cpNo=" + cpNo, function (data, status) {
//                    if (status == "success" && data != "excluded") {
                        $.get(ctx+"getCity1?pinCode=" + pinCode, function (data, status) {
                            if (status == "success" && data.length != 2) {
                                console.log("$$$$$$$$$$  in if  $$$$$")
//                                var json = JSON.parse(data);
//                                $.get(ctx+"getICICIStateByCity?city=" + json.city, function (data, status) {
//                                    if (status == "success" && data.length != 2) {
//                                       
//                                    } else {
//                                        $('.resedentialAddress-error-div').html(negativeIciciPincodeMsg);
//                                        $('.resedentialAddress-error-div').removeClass('hidden');
//                                        $("#city").val("");
//                                        $("#state1").val("");
//                                        $('#state1').attr("disabled", false);
//                                        $('#city').attr("disabled", false);
//                                    }
//                                });
             residentAddressField1 = $('.residentAddressField1').val();
                residentAddressField2 = $('.residentAddressField2').val();
                residentAddressField3 = $('.residentAddressField3').val();
       

                 var isValidAddress = checkAddressLengthICICIResident(residentAddressField1, residentAddressField2, residentAddressField3);
                 if (!isValidAddress) {
                    $('.resedentialAddress-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line').css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                 }
                
                /**
                 * Validation of special characters
                 */
                 if (!addressFieldCheckingicici(residentAddressField1) || !addressFieldCheckingicici(residentAddressField2) || !addressFieldCheckingicici(residentAddressField3)) {
                    console.log("55")
                    $('.resedentialAddress-error-div').html(invalidAddrMsgICICI).css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                }
                            } else {
                                 console.log("$$$$  at else invalid  $$$$");
                                $('.resedentialAddress-error-div').html(negativeIciciPincodeMsg);
                                $('.resedentialAddress-error-div').removeClass('hidden');
                                $("#city").val("");
                                $("#state1").val("");
                                //------change here---------
                                $('#state1').attr("disabled", true);
                                $('#city').attr("disabled", true);
                                //---------change end here------
                            }
                        });
//                    }
//                    else {
//                        $('.resedentialAddress-error-div').html(pincodemsg);
//                        $('.resedentialAddress-error-div').removeClass('hidden');
//                        $("#city").val("");
//                        $("#state1").val("");
//                    }
//                });


            } else {
                $("#city").val("");
                $("#state1").val("");
                $('#state1').attr("disabled", false);
                $('#city').attr("disabled", false);
            }
}

    //Current resedential address
    $('.residentialAddr_div,.residentAddress').on('focusout',function () {
        // $('.residentialAddr_div').focusout(function () {
        console.log("Called Focus Out outside ");
        residentAddressField1 = $('.residentAddressField1').val();
        residentAddressField2 = $('.residentAddressField2').val();
        residentAddressField3 = $('.residentAddressField3').val();
        pin = $('.residentAddress .pincode').val();

        city = "";
        state = "";
        if (($('.residentAddress .city option:selected').val()) != 0) {
            city = $('.residentAddress .city option:selected').text();
        }
        if (($('.residentAddress .state option:selected').val()) != 0) {
            state = $('.residentAddress .state option:selected').text();
        }
        $('#resedentialAddress-error').addClass('hidden');
        if (document.getElementById("iciciBean"))
        {
            console.log("*******************Called ICICI**********************");
            pin = $("#pinCodeICICI").val();
//            //icici form validation residential address            
//            console.log("-----------------------############---------> ",!isValidAddress);
// if (residentAddressField1 == ""  || residentAddressField2 == ""){  
//                console.log("*******************Called ICICI inside if**********************");
//                $('.resedentialAddress-error-div').html(addressMsg + ' Current Residential Address').css("color", "#ff0000");
//                $('.resedentialAddress-error-div').removeClass('hidden');
//            }else
            if (residentAddressField1 == "" || residentAddressField2 == "")
            {
                console.log("*******************Called ICICI inside else if**********************");
                $('.resedentialAddress-error-div').html(addressMsg + ' Current Residential Address').css("color", "#ff0000");
                $('.resedentialAddress-error-div').removeClass('hidden');
            }
            else {
                if(pin == "")
                {
                    $('.resedentialAddress-error-div').html('Please enter Pincode').css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                }else{
                               
 
                 if (pin != "" && !validPinCode(pin)) {
                    console.log("2")
                    $(".resedentialAddress-error-div").html("Please enter valid Pincode").css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                }
                 
                 /**
                  * Validation PIN for ICICI
                  */
           if (validPinCode(pin)) {
                     console.log("inside the validpincode");
                        residentIciciPinCheck(pin)
//                    $.get(ctx + "icicicheckExcludePincode?pinCode=" + pin + "&cpNo=" + $("#cpNo").val(), function (data, status) {
//                        if (!(status == "success" && data != "excluded")) {
//                            $('.resedentialAddress-error-div').html(negativePincodeMsg).css("color", "#ff0000");
//                            $('.resedentialAddress-error-div').removeClass('hidden');
//                            $('#state1').attr("disabled", true);
//                            $('#city').attr("disabled", true);
//                        } else {
//                            pin = $('.residentAddress .pincode').val();
//                            $.get(ctx + "getCity1?pinCode=" + pin, function (data, status) {
//                                if (status == "success" && data.length != 2) {
//                                    var json = JSON.parse(data);
//                                    $.get(ctx + "getICICIStateByCity?city=" + json.city, function (data, status) {
//                                        if (status == "success" && data.length == 2) {
//                                            $('.resedentialAddress-error-div').html(negativePincodeMsg).css("color", "#ff0000");
//                                            $('.resedentialAddress-error-div').removeClass('hidden');
//                                            $('#state1').attr("disabled", true);
//                                            $('#city').attr("disabled", true);
//                                        }
//                                    });
//                                }
////                                else{
////                                    $('.resedentialAddress-error-div').html(negativeIciciPincodeMsg).css("color", "#ff0000");
////                                            $('.resedentialAddress-error-div').removeClass('hidden');
////                                            $('#state1').attr("disabled", true);
////                                            $('#city').attr("disabled", true);
////                                       
////                                    console.log("at js else=====")
////                                }
//                            });
//                        }
//                    });
                } 
//                
                /**
                 * Validation of address and length between 3 to 40;
                 */
                 var isValidAddress = checkAddressLengthICICIResident(residentAddressField1, residentAddressField2, residentAddressField3);
                 if (!isValidAddress) {
                    $('.resedentialAddress-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line').css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                 }
                
                /**
                 * Validation of special characters
                 */
                 if (!addressFieldCheckingicici(residentAddressField1) || !addressFieldCheckingicici(residentAddressField2) || !addressFieldCheckingicici(residentAddressField3)) {
                    console.log("5")
                    $('.resedentialAddress-error-div').html(invalidAddrMsgICICI).css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                }
              }
            }

        } else
        {
            if (residentAddressField1 == "" && pin == "") {
                console.log("----1----")
                $('.resedentialAddress-error-div').html(addressMsg + ' Current Residential Address').css("color", "#ff0000");
                $('.resedentialAddress-error-div').removeClass('hidden');
            } else {
                if (validPinCode(pin)) {
                                    console.log("----2----");

                    $.get(ctx + "checkExcludePincode?pinCode=" + pin + "&cpNo=" + $("#cpNo").val(), function (data, status) {
//                        console.log("data in js",data);
                        if (!(status == "success" && data != "excluded")) {
                            console.log("data in js exclude",data)
                            $('.resedentialAddress-error-div').html(negativePincodeMsg).css("color", "#ff0000");
                            $('.resedentialAddress-error-div').removeClass('hidden');
                            $('#state1').attr("disabled", true);
                            $('#city').attr("disabled", true);
                        } else {
                            pin = $('.residentAddress .pincode').val();
                            $.get(ctx + "getCity?pinCode=" + pin, function (data, status) {
                                if (status == "success" && data.length != 2) {
                                    var json = JSON.parse(data);
                                    $.get(ctx + "getStateByCity?city=" + json.city, function (data, status) {
                                        if (status == "success" && data.length == 2) {
                                            console.log("----3----");
                                            $('.resedentialAddress-error-div').html(negativePincodeMsg).css("color", "#ff0000");
                                            $('.resedentialAddress-error-div').removeClass('hidden');
                                            $('#state1').attr("disabled", true);
                                            $('#city').attr("disabled", true);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }

                if (!addressFieldChecking(residentAddressField1) || !addressFieldChecking(residentAddressField2) || !addressFieldChecking(residentAddressField3)) {
                    console.log("----4----");
                    $('.resedentialAddress-error-div').html(invalidAddrMsg).css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                }

//                if (pin == "" && residentAddressField1 != "" && residentAddressField2 != "" && residentAddressField3 != "") {
//                    $('.resedentialAddress-error-div').html('Please enter Pincode').css("color", "#ff0000");
//                    $('.resedentialAddress-error-div').removeClass('hidden');
//                }

                if (pin != "" && !validPinCode(pin)) {
                    console.log("----5----");
                    $(".resedentialAddress-error-div").html("Please enter valid Pincode").css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                }
                if (residentAddressField1 == "" && city != "") {
                    console.log("----6----");
                    $('.resedentialAddress-error-div').html(addressMsg + ' Current Residential Address').css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                }
                if (pin == "") {
                    console.log("----7----");
                    $('.resedentialAddress-error-div').html('Please enter Pincode').css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                }
//                address length validation
                var isValidAddress = checkAddressLength(residentAddressField1, residentAddressField2, residentAddressField3);
                 if (!isValidAddress) {
                    $('.resedentialAddress-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line').css("color", "#ff0000");
                    $('.resedentialAddress-error-div').removeClass('hidden');
                 }

            }
//            checkAddressLength(residentAddressField1, residentAddressField2, residentAddressField3);
        }
    
        });


    function checkAddressLength(residentAddressField1, residentAddressField2, residentAddressField3) {
        var addressMsg = "Address cannot be greater than 24 characters in each address line";
        console.log("checkAddressLength");
       var isValid = true;
        if (residentAddressField1 != "" && !maxLength(residentAddressField1)) {
            $('.resedentialAddress-error-div').html(addressMsg).css("color", "#ff0000");
            $('.resedentialAddress-error-div').removeClass('hidden');
             isValid = false;
        }
         if (residentAddressField2 != "" && !maxLength(residentAddressField2)) {
            $('.resedentialAddress-error-div').html(addressMsg).css("color", "#ff0000");
            $('.resedentialAddress-error-div').removeClass('hidden');
             isValid = false;
        }
         if (residentAddressField3 != "" && !maxLength(residentAddressField3)) {
            $('.resedentialAddress-error-div').html(addressMsg).css("color", "#ff0000");
            $('.resedentialAddress-error-div').removeClass('hidden');
             isValid = false;
        }
        return isValid;
    }

//    $('.residentAddressField1').on('keyup', function () {
//        residentAddressField1 = $('.residentAddressField1').val();
//        pin = $('.residentAddress .pincode').val();
//        city = $('.residentAddress .city option:selected').text();
//        $('#resedentialAddress-error').addClass('hidden');
//        $('.resedentialAddress-error-div').addClass('hidden');
//        $('.resedentialAddress-error-div').html('');
//        if (residentAddressField1 != "" || pin != "" && city != "") {
//            if (document.getElementById("iciciBean"))
//            {
////                CheckAddress('RES');
//            } else
//            {
//                CheckAddress('RES');
////            $('.resedentialAddress-error-div').html('');
//            }
//        }
//    });
//
//    $('.residentAddressField2').on('keyup', function () {
//        residentAddressField2 = $('.residentAddressField2').val();
//        pin = $('.residentAddress .pincode').val();
//        city = $('.residentAddress .city option:selected').text();
//        $('#resedentialAddress-error').addClass('hidden');
//        $('.resedentialAddress-error-div').addClass('hidden');
//        $('.resedentialAddress-error-div').html('');
//        if (residentAddressField2 != "" || pin != "" && city != "") {
//            if (document.getElementById("iciciBean"))
//            {
////                CheckAddress('RES');
//            } else {
//                CheckAddress('RES');
////            $('.resedentialAddress-error-div').html('');
//            }
//        }
//    });
//    $('.residentAddress .pincode').on('focusout', function () {
////        console.log("---")
//        pin = $('.residentAddress .pincode').val();
//        if (pin == "") {
//            $('.resedentialAddress-error-div').html('Please enter Pincode').css("color", "#ff0000");
////            $('#resedentialAddress-error').removeClass('hidden');
//            $('.resedentialAddress-error-div').removeClass('hidden');
//        }
////         $('#resedentialAddress-error').addClass('hidden');
//    });
//    $('.residentAddressField3').on('keyup', function () {
//        residentAddressField3 = $('.residentAddressField3').val();
//        pin = $('.residentAddress .pincode').val();
//        city = $('.residentAddress .city option:selected').text();
//        $('#resedentialAddress-error').addClass('hidden');
//        $('.resedentialAddress-error-div').html('');
//        $('.resedentialAddress-error-div').addClass('hidden');
//        if (residentAddressField3 != "" || pin != "" && city != "") {
//            if (document.getElementById("iciciBean"))
//            {
////                CheckAddress('RES');
//            } else
//            {
//                CheckAddress('RES');
////            $('.resedentialAddress-error-div').html('');
//
//            }
//        }
//    });
//   
    
    
    /*code by vernost Team*/
    function checkAddressLengthICICIResident(residentAddressField1, residentAddressField2, residentAddressField3) {

        var isValid = true;
        if (!icicimaxLength(residentAddressField1) && residentAddressField1 != "") {
            $('.resedentialAddress-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
            $('.resedentialAddress-error-div').removeClass('hidden');

            isValid = false;

        }

        if (residentAddressField2 != "" && !icicimaxLength(residentAddressField2)) {
            $('.resedentialAddress-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
            $('.resedentialAddress-error-div').removeClass('hidden');
            isValid = false;
        }
        if (residentAddressField3 != "" && !icicimaxLength(residentAddressField3)) {
            $('.resedentialAddress-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
            $('.resedentialAddress-error-div').removeClass('hidden');
            isValid = false;

        }

        return isValid
    }
    function checkAddressLengthICICIPermant(residentAddressField1, residentAddressField2, residentAddressField3) {
        var isValid = true;
        if (!icicimaxLength(residentAddressField1) && residentAddressField1 != "") {
            $('.permanentAddr-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
            $('.permanentAddr-error-div').removeClass('hidden');

            isValid = false;

        }

        if (residentAddressField2 != "" && !icicimaxLength(residentAddressField2)) {
            $('.permanentAddr-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
            $('.permanentAddr-error-div').removeClass('hidden');
            isValid = false;

        }
        if (residentAddressField3 != "" && !icicimaxLength(residentAddressField3)) {
            $('.permanentAddr-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
            $('.permanentAddr-error-div').removeClass('hidden');
            isValid = false;

        }

//        }
        return isValid
    }
    function checkAddressLengthICICICompany(residentAddressField1, residentAddressField2, residentAddressField3) {

        var isValid = true;
        if (!icicimaxLength(residentAddressField1) && residentAddressField1 != "") {
            $('.companyAddr_div-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
            $('.companyAddr_div-error-div').removeClass('hidden');

            isValid = false;

        }

        if (residentAddressField2 != "" && !icicimaxLength(residentAddressField2)) {
            $('.companyAddr_div-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
            $('.companyAddr_div-error-div').removeClass('hidden');
            isValid = false;

        }
        if (residentAddressField3 != "" && !icicimaxLength(residentAddressField3)) {
            $('.companyAddr_div-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line');
            $('.companyAddr_div-error-div').removeClass('hidden');
            isValid = false;

        }


        return isValid
    }
    /*----------End------------*/
    $('.permanent_residentAddress').focusout(function () {
        residentAddressField1 = $('.permanent_AddressField1').val();
        residentAddressField2 = $('.permanent_AddressField2').val();
        residentAddressField3 = $('.permanent_AddressField3').val();
        pin = $('.permanent_residentAddress .pincode').val();
        city = "";
        state = "";
        if (($('.permanent_residentAddress .city option:selected').val()) != 0) {
            city = $('.permanent_residentAddress .city option:selected').text();
        }
        if (($('.permanent_residentAddress .state option:selected').val()) != 0) {
            state = $('.permanent_residentAddress .state option:selected').text();
        }
        $('.permanentAddr-error-div').removeClass('hidden');
        if (document.getElementById("iciciBean"))
        {

            $('#permanentAddr-error').addClass('hidden');
            $('.permanentAddr-error-div').addClass('hidden');
            pin = $("#PpinCodeICICI").val();
//        var radioValue = $(".radio_btn_yes input[type='radio']:checked").val();

            var radioValue = $(".addressProof input[type='radio']:checked").val();
            if (radioValue == 'No') {
               if (residentAddressField1 == "" || residentAddressField2 == "")
            {
                console.log("*******************Called ICICI inside else if**********************");
                $('.permanentAddr-error-div').html(addressMsg + ' Permanent Residential Address').css("color", "#ff0000");
                $('.permanentAddr-error-div').removeClass('hidden');
            }
            else {
                if(pin == "")
                {
                    $('.permanentAddr-error-div').html('Please enter Pincode').css("color", "#ff0000");
                    $('.permanentAddr-error-div').removeClass('hidden');
                }else{
                               
 
                 if (pin != "" && !validPinCode(pin)) {
                    console.log("2")
                    $(".permanentAddr-error-div").html("Please enter valid Pincode").css("color", "#ff0000");
                    $('.permanentAddr-error-div').removeClass('hidden');
                }
                 
                 /**
                  * Validation PIN for ICICI
                  */
           if (validPinCode(pin)) {
                     console.log("inside the validpincode");
                        permIciciPinCheck(pin)

                } 
//                
                /**
                 * Validation of address and length between 3 to 40;
                 */
                 var isValidAddress = checkAddressLengthICICIPermant(residentAddressField1, residentAddressField2, residentAddressField3);
                 if (!isValidAddress) {
                    $('.permanentAddr-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line').css("color", "#ff0000");
                    $('.permanentAddr-error-div').removeClass('hidden');
                 }
                
                /**
                 * Validation of special characters
                 */
                 if (!addressFieldCheckingicici(residentAddressField1) || !addressFieldCheckingicici(residentAddressField2) || !addressFieldCheckingicici(residentAddressField3)) {
                    console.log("5")
                    $('.permanentAddr-error-div').html(invalidAddrMsgICICI).css("color", "#ff0000");
                    $('.permanentAddr-error-div').removeClass('hidden');
                }
              }
            }
            if(residentAddressField1 == "" && residentAddressField2 == "" && pin == ""){
                $('.permanentAddr-error-div').html("").css("color", "#ff0000");
                    $('.permanentAddr-error-div').addClass('hidden');
            }
            } else if (radioValue == 'Yes') {
                $('.permanentAddr-error-div').addClass('hidden');
            }
        } else {
            if (($('.permanent_residentAddress .city option:selected').val()) != 0) {
                city = $('.permanent_residentAddress .city option:selected').text();
            }
            if (($('.permanent_residentAddress .state option:selected').val()) != 0) {
                state = $('.permanent_residentAddress .state option:selected').text();
            }
//        var radioValue = $(".radio_btn_yes input[type='radio']:checked").val();
            var radioValue = $(".addressProof input[type='radio']:checked").val();
            if (radioValue == 'No') {
                if ((residentAddressField1 == "" && pin == "")) {
                    $('.permanentAddr-error-div').removeClass('hidden');
                    $(".permanentAddr-error-div").html(addressMsg + ' Permanent Residential Address').css("color", "#ff0000");
                } else {
                    if (validPinCode(pin)) {
                        $.get(ctx + "checkExcludePincode?pinCode=" + pin + "&cpNo=" + $("#cpNo").val(), function (data, status) {
                            if (!(status == "success" && data != "excluded")) {
//                                $('.permanentAddr-error-div').html(negativePincodeMsg).css("color", "#ff0000");
//                                $('.permanentAddr-error-div').removeClass('hidden');
//                                $("#PCity").attr("disabled", true);
//                                $("#PState").attr("disabled", true);
                            } else {
                                $.get(ctx + "getCity?pinCode=" + pin, function (data, status) {
                                    if (status == "success" && data.length != 2) {
                                        var json = JSON.parse(data);
                                        $.get(ctx + "getStateByCity?city=" + json.city, function (data, status) {
                                            if (status == "success" && data.length == 2) {
//                                                $('.permanentAddr-error-div').html(negativePincodeMsg).css("color", "#ff0000");
//                                                $('.permanentAddr-error-div').removeClass('hidden');
//                                                $('#PState').attr("disabled", true);
//                                                $('#PCity').attr("disabled", true);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                    if (!addressFieldChecking(residentAddressField1) || !addressFieldChecking(residentAddressField2) || !addressFieldChecking(residentAddressField3))
                    {
                        $(".permanentAddr-error-div").html(invalidAddrMsg).css("color", "#ff0000");
                        $('.permanentAddr-error-div').removeClass('hidden');
                    }
//                if (pin == "" && residentAddressField1!="" && residentAddressField2!="" && residentAddressField3!="") {
//                    $('.permanentAddr-error-div').html('Please enter Pincode').css("color", "#ff0000");
//                    $('.permanentAddr-error-div').removeClass('hidden');
//                }
                    if (pin != "" && !validPinCode(pin)) {
                        $(".permanentAddr-error-div").html("Please enter valid Pincode").css("color", "#ff0000");
                        $('.permanentAddr-error-div').removeClass('hidden');
                    }
                    if (residentAddressField1 == "" && city != "") {
                        $('.permanentAddr-error-div').html(addressMsg + ' Permanent Residential Address').css("color", "#ff0000");
                        $('.permanentAddr-error-div').removeClass('hidden');
                    }
                    if (pin == "") {
                        $('.permanentAddr-error-div').html('Please enter Pincode').css("color", "#ff0000");
                        $('.permanentAddr-error-div').removeClass('hidden');
                    }
                }
            } else if (radioValue == 'Yes') {
                $('.permanentAddr-error-div').addClass('hidden');
            }
        }
    });
//    $('.permanent_residentAddress .pincode').on('focusout', function () {
////        console.log("-2--")
//        pin = $('.permanent_residentAddress .pincode').val();
//        if (pin == "") {
//            $('.permanentAddr-error-div').html('Please enter Pincode').css("color", "#ff0000");
////            $('#resedentialAddress-error').removeClass('hidden');
//            $('.permanentAddr-error-div').removeClass('hidden');
//        }
////         $('#resedentialAddress-error').addClass('hidden');
//    });
//    $('.permanent_AddressField1').on('keyup', function () {
//        residentAddressField1 = $('.permanent_AddressField1').val();
//        pin = $('.permanent_residentAddress .pincode').val();
//        city = $('.permanent_residentAddress .city option:selected').text();
//        $('.permanentAddr-error-div').addClass('hidden');
//        $('#permanentAddr-error').addClass('hidden');
//        $('.permanentAddr-error-div').html('');
//
//        if (residentAddressField1 != "" || pin != "" && city != "")
//            if (document.getElementById("iciciBean"))
//            {
//                CheckAddress('PRES');
//            } else {
//                CheckAddress('PRES');
////            $('.permanentAddr-error-div').html('');
//            }
//    });
//    $('.permanent_AddressField2').on('keyup', function () {
//        residentAddressField2 = $('.permanent_AddressField2').val();
//        pin = $('.permanent_residentAddress .pincode').val();
//        city = $('.permanent_residentAddress .city option:selected').text();
////        $('.permanentAddr-error-div').removeClass('hidden');
//        $('.permanentAddr-error-div').addClass('hidden');
//        $('#permanentAddr-error').addClass('hidden');
//        $('.permanentAddr-error-div').html('');
//        if (residentAddressField2 != "" || pin != "" && city != "")
//            if (document.getElementById("iciciBean"))
//            {
//                CheckAddress('PRES');
//            } else {
//                CheckAddress('PRES');
////            $('.permanentAddr-error-div').html('');
//            }
//    });
//    $('.permanent_AddressField3').on('keyup', function () {
//        residentAddressField3 = $('.permanent_AddressField3').val();
//        pin = $('.permanent_residentAddress .pincode').val();
//        city = $('.permanent_residentAddress .city option:selected').text();
////        $('.permanentAddr-error-div').removeClass('hidden');
//        $('.permanentAddr-error-div').addClass('hidden');
//        $('#permanentAddr-error').addClass('hidden');
//        $('.permanentAddr-error-div').html('');
//        if (residentAddressField3 != "" || pin != "" && city != "")
//            if (document.getElementById("iciciBean"))
//            {
//                CheckAddress('PRES');
//            } else {
//                CheckAddress('PRES');
////            $('.permanentAddr-error-div').html('');
//            }
//    });

    //company address
    $('.companyAddr_div').focusout(function () {
        residentAddressField1 = $('.companyAddressField1').val();
        residentAddressField2 = $('.companyAddressField2').val();
        residentAddressField3 = $('.companyAddressField3').val();
        pin = $('.companyAddress .pincode').val();
        city = "";
        state = "";
        if (($('.companyAddress .city option:selected').val()) != 0) {
            city = $('.companyAddress .city option:selected').text();
        }
        if (($('.companyAddress .state option:selected').val()) != 0) {
            state = $('.companyAddress .state option:selected').text();
        }
        $('#companyAddress-error').addClass('hidden');
        /*code by vernost Team----- */
        if (document.getElementById("iciciBean"))
        {
             pin = $("#OPincodeICICI").val();
             if (residentAddressField1 == "" || residentAddressField2 == "")
            {
                console.log("*******************Called ICICI inside else if**********************");
                $('.companyAddr_div-error-div').html(addressMsg + ' company address').css("color", "#ff0000");
                $('.companyAddr_div-error-div').removeClass('hidden');
            }
            else {
                if(pin == "")
                {
                    $('.companyAddr_div-error-div').html('Please enter Pincode').css("color", "#ff0000");
                    $('.companyAddr_div-error-div').removeClass('hidden');
                }else{
                               
 
                 if (pin != "" && !validPinCode(pin)) {
                    console.log("2")
                    $(".companyAddr_div-error-div").html("Please enter valid Pincode").css("color", "#ff0000");
                    $('.companyAddr_div-error-div').removeClass('hidden');
                }
                 
                 /**
                  * Validation PIN for ICICI
                  */
           if (validPinCode(pin)) {
                     console.log("inside the validpincode at company");
                        companyIciciPinCheck(pin);

                } 
//                
                /**
                 * Validation of address and length between 3 to 40;
                 */
                 var isValidAddress = checkAddressLengthICICICompany(residentAddressField1, residentAddressField2, residentAddressField3);
                 if (!isValidAddress) {
                    $('.companyAddr_div-error-div').html('Address cannot be greater than 40 and less than 3 characters in each address line').css("color", "#ff0000");
                    $('.companyAddr_div-error-div').removeClass('hidden');
                 }
                
                /**
                 * Validation of special characters
                 */
                 if (!addressFieldCheckingicici(residentAddressField1) || !addressFieldCheckingicici(residentAddressField2) || !addressFieldCheckingicici(residentAddressField3)) {
                    console.log("5")
                    $('.companyAddr_div-error-div').html(invalidAddrMsgICICI).css("color", "#ff0000");
                    $('.companyAddr_div-error-div').removeClass('hidden');
                }
              }
            }

//            companyFullAddress = residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin;
            //---------end icici validation company address-----------
        } else {
            if (residentAddressField1 == "" && pin == "") {
                $('.companyAddr_div-error-div').html(addressMsg + ' Company Address').css("color", "#ff0000");
                $('.companyAddr_div-error-div').removeClass('hidden');
            } else {
                if (validPinCode(pin)) {
                    $.get(ctx + "checkExcludePincode?pinCode=" + pin + "&cpNo=" + $("#cpNo").val(), function (data, status) {
                        if (!(status == "success" && data != "excluded")) {
                            $('.companyAddr_div-error-div').html(negativePincodeMsg).css("color", "#ff0000");
                            $('.companyAddr_div-error-div').removeClass('hidden');
                            $("#OCity").attr("disabled", true);
                            $("#OState").attr("disabled", true);
                        } else {
                            $.get(ctx + "getCity?pinCode=" + pin, function (data, status) {
                                if (status == "success" && data.length != 2) {
                                    var json = JSON.parse(data);
                                    $.get(ctx + "getStateByCity?city=" + json.city, function (data, status) {
                                        if (status == "success" && data.length == 2) {
                                            $('.companyAddr_div-error-div').html(negativePincodeMsg).css("color", "#ff0000");
                                            $('.companyAddr_div-error-div').removeClass('hidden');
                                            $('#OState').attr("disabled", true);
                                            $('#OCity').attr("disabled", true);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
                if (!addressFieldChecking(residentAddressField1) || !addressFieldChecking(residentAddressField2) || !addressFieldChecking(residentAddressField3)) {
                    $('.companyAddr_div-error-div').html(invalidAddrMsg).css("color", "#ff0000");
                    $('.companyAddr_div-error-div').removeClass('hidden');
                }
//                if (pin == "" && residentAddressField1!="" && residentAddressField2!="" && residentAddressField3!="") {
//                    $('.companyAddr_div-error-div').html('Please enter Pincode').css("color", "#ff0000");
//                    $('.companyAddr_div-error-div').removeClass('hidden');
//                }
                if (pin != "" && !validPinCode(pin)) {
                    $(".companyAddr_div-error-div").html("Please enter valid Pincode").css("color", "#ff0000");
                    $('.companyAddr_div-error-div').removeClass('hidden');
                }
                if (residentAddressField1 == "" && city != "") {
                    $('.companyAddr_div-error-div').html(addressMsg + ' Company Address').css("color", "#ff0000");
                    $('.companyAddr_div-error-div').removeClass('hidden');
                }
                if (pin == "") {
                    $('.companyAddr_div-error-div').html('Please enter Pincode').css("color", "#ff0000");
                    $('.companyAddr_div-error-div').removeClass('hidden');
                }
            }


        }
        companyFullAddress = residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin;
    });
    /*===END=====*/
//    $('.companyAddress .pincode').on('focusout', function () {
////        console.log("-3--")
//        pin = $('.companyAddress .pincode').val();
//        if (pin == "") {
//            $('.companyAddr_div-error-div').html('Please enter Pincode').css("color", "#ff0000");
////            $('#resedentialAddress-error').removeClass('hidden');
//            $('.companyAddr_div-error-div').removeClass('hidden');
//        }
////         $('#resedentialAddress-error').addClass('hidden');
//    });
//    $('.companyAddressField1').on('keyup', function () {
//        residentAddressField1 = $('.companyAddressField1').val();
//        pin = $('.companyAddress .pincode').val();
//        city = $('.companyAddress .city option:selected').text();
//        $('#companyAddress-error').addClass('hidden');
//        $('.companyAddr_div-error-div').addClass('hidden');
//        $('.companyAddr_div-error-div').html('');
//        if (residentAddressField1 != "" || pin != "" && city != "") {
//            if (document.getElementById("iciciBean"))
//            {
//                CheckAddress('COMP');
//            } else {
//                CheckAddress('COMP');
////            $('.companyAddr_div-error-div').html('');
//            }
//        }
//    });
//    $('.companyAddressField2').on('keyup', function () {
//        residentAddressField2 = $('.companyAddressField2').val();
//        pin = $('.companyAddress .pincode').val();
//        city = $('.companyAddress .city option:selected').text();
//        $('#companyAddress-error').addClass('hidden');
//        $('.companyAddr_div-error-div').addClass('hidden');
//        $('.companyAddr_div-error-div').html('');
//        if (residentAddressField2 != "" || pin != "" && city != "") {
//            if (document.getElementById("iciciBean"))
//            {
//                CheckAddress('COMP');
//            } else
//            {
//                CheckAddress('COMP');
////            $('.companyAddr_div-error-div').html('');
//            }
//        }
//    })
//    $('.companyAddressField3').on('keyup', function () {
//        residentAddressField3 = $('.companyAddressField3').val();
//        pin = $('.companyAddress .pincode').val();
//        city = $('.companyAddress .city option:selected').text();
//        $('#companyAddress-error').addClass('hidden');
//        $('.companyAddr_div-error-div').addClass('hidden');
//        $('.companyAddr_div-error-div').html('');
//        if (residentAddressField3 != "" || pin != "" && city != "") {
//            if (document.getElementById("iciciBean"))
//            {
//                CheckAddress('COMP');
//            } else
//            {
//                CheckAddress('COMP');
////            $('.companyAddr_div-error-div').html('');
//            }
//        }
//    })


    $('.phoneNum_div').focusout(function () {
        std = $('.std input').val();
        phone = $('.phNum input').val();
        var ostd = $('.std_office input').val();
        var ophone = $('.phNum_office input').val();

        $('#homeNumber-error').addClass('hidden');
        if (ostd != "" || ophone != "") {
            $('.homeNumber-error-div').addClass('hidden');
        } else {
            if (std == "" || phone == "") {

                $('.homeNumber-error-div').html('Please enter Home Landline number and must have exact 11 digits including STD code.');
                $('.homeNumber-error-div').removeClass('hidden');
            } else if (ostd == "" || ophone == "") {
                $('.officeNumber-error-div').html('');
            }

        }
        if (std != "" && phone == "") {

            $('.homeNumber-error-div').html('Please enter Home Landline number and must have exact 11 digits including STD code.');
            $('.homeNumber-error-div').removeClass('hidden');

        }

        if (std != "" || phone != "") {

            if ((onlyNumeric(std) && startsWithZero(std)) && onlyNumeric(phone)) {
                $('.homeNumber-error-div').addClass('hidden');
            } else {
                $('.homeNumber-error-div').removeClass('hidden');
                if (!startsWithZero(std)) {
                    $('.homeNumber-error-div').html('STD code must start with zero');
                }

            }
        }
        if (std != "" && phone != "") {
            var total = std + phone;
//            console.log("length of total-->", total.length)
            if (total.length > 11 || total.length < 11) {
//                     console.log("if")
                $('.homeNumber-error-div').html('Please enter Home Landline number and must have exact 11 digits including STD code.');

                $('.homeNumber-error-div').removeClass('hidden');
            } else {
//                     console.log("else")
                $('.homeNumber-error-div').addClass('hidden');
            }
        }
        if (std != "" && std.length <= 4) {
            $('.homeNumber-error-div').removeClass('hidden');
            if (std.length < 3) {

                $('.homeNumber-error-div').html('Home std should be Min 3 digits');
            } else if (isNaN(std)) {

                $('.homeNumber-error-div').html('Please enter Digits only');

            }
        }
        if (phone != "" && phone.length <= 8) {
            $('.homeNumber-error-div').removeClass('hidden');

            if (phone.length < 7) {

                $('.homeNumber-error-div').html('Home Phone should be Min 7 digits');
            } else if (isNaN(phone)) {

                $('.homeNumber-error-div').html('Please enter Digits only');

            }

        }

    });

    $('.std input, .phNum input').on('keyup', function () {
        std = $('.std input').val();
        phone = $('.phNum input').val();
        if (std != "" || phone != "")
            $('.homeNumber-error-div').html('');
    });

    $('.officePhoneNum_div').focusout(function () {
        std = $('.std_office input').val();
        phone = $('.phNum_office input').val();
        var hstd = $('.std input').val();
        var hphone = $('.phNum input').val();
//        console.log("focusout")
        $("#OStd-error").hide();
        $("#OPhone-error").hide();
        $('#homeNumber-error').addClass('hidden');
//        $('#homeNumber-error').html('');
//        $('.homeNumber-error-div').addClass('hidden')
//        $('.homeNumber-error-div').html('');
        if (hstd != "" || hphone != "") {
            $('.officeNumber-error-div').addClass('hidden');
            $("#OStd-error").hide();
            $("#OPhone-error").hide();
        } else {
            if (std == "" || phone == "") {
                $('.officeNumber-error-div').html('Please enter Office Landline number and must have exact 11 digits including STD code.');
                $('.officeNumber-error-div').removeClass('hidden');
                $("#OStd-error").hide();
                $("#OPhone-error").hide();
            } else if (hstd == "" || hphone == "") {
                $('.homeNumber-error-div').html('');
                $("#OStd-error").hide();
                $("#OPhone-error").hide();
            }
        }
        if (std != "" && phone == "") {
            $('.officeNumber-error-div').html('Please enter Office Landline number and must have exact 11 digits including STD code.');
            $('.officeNumber-error-div').removeClass('hidden');
            $("#OStd-error").hide();
            $("#OPhone-error").hide();
        }

        if (std != "" || phone != "") {
            if ((onlyNumeric(std) && startsWithZero(std)) && onlyNumeric(phone)) {
                $('.officeNumber-error-div').addClass('hidden');
                $("#OStd-error").hide();
                $("#OPhone-error").hide();
            } else {
                $('.officeNumber-error-div').removeClass('hidden');
                $("#OStd-error").hide();
                $("#OPhone-error").hide();
                if (!startsWithZero(std)) {
                    $('.officeNumber-error-div').html('STD code must start with zero');
                    $("#OStd-error").hide();
                    $("#OPhone-error").hide();
                }
            }
        }
        if (std != "" && phone != "") {
            var total = std + phone;
//            console.log("length of total-office->", total.length)
            if (total.length > 11 || total.length < 11) {
//                console.log("if office")
                $('.officeNumber-error-div').html('Please enter Office Landline number and must have exact 11 digits including STD code.');
                $('.officeNumber-error-div').removeClass('hidden');
                $("#OStd-error").hide();
                $("#OPhone-error").hide();
            } else {
//                console.log("else office")
                $('.officeNumber-error-div').addClass('hidden');
                $("#OStd-error").hide();
                $("#OPhone-error").hide();
            }
        }

        if (std != "" && std.length <= 4) {
            $('.officeNumber-error-div').removeClass('hidden');
            if (std.length < 3) {
                $('.officeNumber-error-div').html('Office std should be Min 3 digits');
                $("#OStd-error").hide();
                $("#OPhone-error").hide();
            } else if (isNaN(std)) {

                $('.officeNumber-error-div').html('Please enter Digits only');
                $("#OStd-error").hide();
                $("#OPhone-error").hide();

            }
        }
        if (phone != "" && phone.length <= 8) {
            $('.officeNumber-error-div').removeClass('hidden');
            $("#OStd-error").hide();
            $("#OPhone-error").hide();

            if (phone.length < 7) {

                $('.officeNumber-error-div').html('Office phone should be Min 7 digits');
                $("#OStd-error").hide();
                $("#OPhone-error").hide();
            } else if (isNaN(phone)) {

                $('.officeNumber-error-div').html('Please enter Digits only');
                $("#OStd-error").hide();
                $("#OPhone-error").hide();

            }

        }





    });

    $('.std_office input, .phNum_office input').on('keyup', function () {
        std = $('.std input').val();
        phone = $('.phNum_office input').val();
        if (std != "" || phone != "")
            $('.officeNumber-error-div').html('');
        $("#OStd-error").hide();
        $("#OPhone-error").hide();
    });

    var lifeStyleBenefits = false;

    $('body').click(function (event) {

        if (!$(event.target).closest('.life_benefits_list').length)
        {

            if ($('.life_benefits_list').is(":visible")) {

                if (lifeStyleBenefits)
                {
                    $('.life_benefits_list').addClass('hidden');
                    lifeStyleBenefits = false;
                    return;
                }
                lifeStyleBenefits = true;
            }
        }

    });

    var joinFees = false;

    $('body').click(function (event) {

        if (!$(event.target).closest('.slider_div').length)
        {
            if ($('.slider_div').is(":visible")) {
                if (joinFees)
                {
                    $('.slider_div').addClass('hidden');
                    joinFees = false;
                    return;
                }
                joinFees = true;
            }
        }
    });

    var Banks = false;

    $('body').click(function (event) { // <---- "event" parameter here

        if (!$(event.target).closest('.banks_listing').length)
        {

            if ($('.banks_listing').is(":visible")) {

                if (Banks)
                {
                    $('.banks_listing').addClass('hidden');
                    Banks = false;
                    return;
                }
                Banks = true;
            }
        }
    });

//    $(document).click(function (event) {
        //chrome 73 changes
        $("#fullName").click(function () {
            $('.name_ul').removeClass('hidden');
            $('.name_inputBox').addClass('hidden');

        });

        $(document).mouseup(function (e)
        {
            var container = $('.name_ul');
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                var fName = $('.firstName').val();
                var mName = $('.middleName').val();
                var lName = $('.lastName').val();
                $('.name_inputBox').val(fName + " " + mName + " " + lName);
                $('.name_ul').addClass('hidden');
                $('.name_inputBox').removeClass('hidden');
                if (fName == "" && lName == "")
                    $('.name_inputBox').val('');
            }
        });
 $(".residentAddress").click(function () {
            $('.residentialAddr_div').removeClass('hidden');
            $('.currentResidentialAddress').addClass('hidden');

        });

        $(document).mouseup(function (e)
        {
            var container = $('.residentAddress');
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
            residentAddressField1 = $('.residentAddressField1').val();
            residentAddressField2 = $('.residentAddressField2').val();
            residentAddressField3 = $('.residentAddressField3').val();
            pin = $('.residentAddress .pincode').val();

            city = "";
            state = "";
            if (($('.residentAddress .city option:selected').val()) != 0) {
                city = $('.residentAddress .city option:selected').text();
            }
            if (($('.residentAddress .state option:selected').val()) != 0) {
                state = $('.residentAddress .state option:selected').text();
            }

            $('.residentialAddr_div').addClass('hidden');
            $('.currentResidentialAddress').removeClass('hidden');
            $('.currentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

            if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
                $('.currentResidentialAddress').val('');
            if (document.getElementById("iciciBean"))
            {
                pin = $("#pinCodeICICI").val();

                city = "";
                state = "";
                if (($('.residentAddress .city option:selected').val()) != 0) {
                    city = $('.residentAddress .city option:selected').text();
                }
                if (($('.residentAddress .state option:selected').val()) != 0) {
                    state = $('.residentAddress .state option:selected').text();
                }
                $('.residentialAddr_div').addClass('hidden');
                $('.currentResidentialAddress').removeClass('hidden');
                $('.currentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

                if (residentAddressField1 == "" && residentAddressField2 == "" && pin == "" && city == "" && state == "")
                    $('.currentResidentialAddress').val('');
            }
            }
        });

            
                   $(".permanent_residentAddress").click(function () {
            $('.permanentAddr_div').removeClass('hidden');
            //$('.permanent_residentAddress').addClass('hidden');

        });

        $(document).mouseup(function (e)
        {
            var container = $('.permanent_residentAddress');
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
            residentAddressField1 = $('.permanent_AddressField1').val();
            residentAddressField2 = $('.permanent_AddressField2').val();
            residentAddressField3 = $('.permanent_AddressField3').val();
            pin = $('.permanent_residentAddress .pincode').val();
            city = "";
            state = "";
            if (($('.permanent_residentAddress .city option:selected').val()) != 0) {
                city = $('.permanent_residentAddress .city option:selected').text();
            }
            if (($('.permanent_residentAddress .state option:selected').val()) != 0) {
                state = $('.permanent_residentAddress .state option:selected').text();
            }
            //$('.permanentAddr_div').addClass('hidden');
            $('.permanentResidentialAddress').removeClass('hidden');
            $('.permanentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

            if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
                $('.permanentResidentialAddress').val('');
            if (document.getElementById("iciciBean"))
            {
                pin = $("#PpinCodeICICI").val();
                city = "";
                state = "";
                if (($('.permanent_residentAddress .city option:selected').val()) != 0) {
                    city = $('.permanent_residentAddress .city option:selected').text();
                }
                if (($('.permanent_residentAddress .state option:selected').val()) != 0) {
                    state = $('.permanent_residentAddress .state option:selected').text();
                }
//                if (residentAddressField1 == "" || residentAddressField2 == "" || pin == "" || city == "" || state == "")
//                    $('#permanentAddr-error').css('display', 'block');
//                else
//                    $('#permanentAddr-error').css('display', 'none');
               // $('.permanentAddr_div').addClass('hidden');
                $('.permanentResidentialAddress').removeClass('hidden');
                $('.permanentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

                if (residentAddressField1 == "" && residentAddressField2 == "" && pin == "" && city == "" && state == "")
                    $('.permanentResidentialAddress').val('');
            }
            }
        });
   
  $(".companyAddress").click(function () {
            $('.companyAddr_div').removeClass('hidden');
            $('.companyAddress_box').addClass('hidden');

        });

        $(document).mouseup(function (e)
        {
            var container = $('.companyAddress');
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
           residentAddressField1 = $('.companyAddressField1').val();
            residentAddressField2 = $('.companyAddressField2').val();
            residentAddressField3 = $('.companyAddressField3').val();
            pin = $('.companyAddress .pincode').val();
            city = "";
            state = "";
            if (($('.companyAddress .city option:selected').val()) != 0) {
                city = $('.companyAddress .city option:selected').text();
            }
            if (($('.companyAddress .state option:selected').val()) != 0) {
                state = $('.companyAddress .state option:selected').text();
            }
//            if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
//                $('#companyAddress-error').css('display', 'block');
//            else
//                $('#companyAddress-error').css('display', 'none');

            $('.companyAddr_div').addClass('hidden');
            $('.companyAddress_box').removeClass('hidden');
            $('.companyAddress_box').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

            if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
                $('.companyAddress_box').val('');

            if (document.getElementById("iciciBean"))
            {
                pin = $("#OPincodeICICI").val();
                city = "";
                state = "";
                if (($('.companyAddress .city option:selected').val()) != 0) {
                    city = $('.companyAddress .city option:selected').text();
                }
                if (($('.companyAddress .state option:selected').val()) != 0) {
                    state = $('.companyAddress .state option:selected').text();
                }
//                if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
//                    $('#companyAddress-error').css('display', 'block');
//                else
//                    $('#companyAddress-error').css('display', 'none');

                $('.companyAddr_div').addClass('hidden');
                $('.companyAddress_box').removeClass('hidden');
                $('.companyAddress_box').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);

                if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
                    $('.companyAddress_box').val('');
            }
            }
        });
     $(".phoneNum_div").click(function () {
            $('.phoneNum_div').removeClass('hidden');
            $('.homeNumber').addClass('hidden');

        });
        
         $(document).mouseup(function (e)
        {
            var container = $('.phoneNum_div');
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                  var std = $('.std input').val();
            var phone = $('.phNum input').val();

            if (std == "" || phone == "")
                $('#homeNumber-error').css('display', 'block');
            else {
                if (onlyNumeric(std) && onlyNumeric(phone))
                    $('#homeNumber-error').css('display', 'none');
                else
                    $('#homeNumber-error').css('display', 'block');
            }
            $('.phoneNum_div').addClass('hidden');
            $('.homeNumber').removeClass('hidden');
            $('.homeNumber').val(std + " " + phone);

            if (std == "" && phone == "")
                $('.homeNumber').val('');
  
                
            }
        });
        
        $(".officePhoneNum_div").click(function () {
            $('.officePhoneNum_div').removeClass('hidden');
            $('.officeNumber').addClass('hidden');

        });
        
         $(document).mouseup(function (e)
        {
            var container = $('.officePhoneNum_div');
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
            std = $('.std_office input').val();
            phone = $('.phNum_office input').val();
            $('.officePhoneNum_div').addClass('hidden');
            $('.officeNumber').removeClass('hidden');
            $("#OStd-error").hide();
            $("#OPhone-error").hide();
            $('.officeNumber').val(std + " " + phone);

            if (std == "" && phone == "")
                $('.officeNumber').val('');
             
            }
        });
        
        
      
//      console.log('CLICKED!!!!!!!!!');
//        if (!$(event.target).closest('.name_ul').length) {
////            console.log("click function of target")
//            var fName = $('.firstName').val();
//            var mName = $('.middleName').val();
//            var lName = $('.lastName').val();
//
//            $("#fname-error").addClass('hidden');
//
//            $('.name_ul').addClass('hidden');
//            $('.name_inputBox').removeClass('hidden');
//            $('.name_inputBox').val(fName + " " + mName + " " + lName);
////            var fullname=$('.name_inputBox').val();
////            var checkName=hasalphabet(fullname);
////            
////            if(checkName == false){
////               
////                $('.fullName-error-div').html("Please enter Alphabets Only.").css("color", "#ff0000");
////                $('.fullName-error-div').removeClass('hidden');
////            }else{
////                $('.fullName-error-div').html('');
////                $('.fullName-error-div').addClass('hidden');
////            }
//            if (fName == "" && lName == "")
//                $('.name_inputBox').val('');
//        }
//        if (!$(event.target).closest('.residentAddress').length) {
//
//            residentAddressField1 = $('.residentAddressField1').val();
//            residentAddressField2 = $('.residentAddressField2').val();
//            residentAddressField3 = $('.residentAddressField3').val();
//            pin = $('.residentAddress .pincode').val();
//
//            city = "";
//            state = "";
//            if (($('.residentAddress .city option:selected').val()) != 0) {
//                city = $('.residentAddress .city option:selected').text();
//            }
//            if (($('.residentAddress .state option:selected').val()) != 0) {
//                state = $('.residentAddress .state option:selected').text();
//            }
//
////            if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
////                $('#resedentialAddress-error').css('display', 'block');
////            else
////                $('#resedentialAddress-error').css('display', 'none');
//
//            $('.residentialAddr_div').addClass('hidden');
//            $('.currentResidentialAddress').removeClass('hidden');
//            $('.currentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//
//            if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
//                $('.currentResidentialAddress').val('');
//            if (document.getElementById("iciciBean"))
//            {
//                pin = $("#pinCodeICICI").val();
//
//                city = "";
//                state = "";
//                if (($('.residentAddress .city option:selected').val()) != 0) {
//                    city = $('.residentAddress .city option:selected').text();
//                }
//                if (($('.residentAddress .state option:selected').val()) != 0) {
//                    state = $('.residentAddress .state option:selected').text();
//                }
//
////                if (residentAddressField1 == "" || residentAddressField2 == "" || pin == "" || city == "" || state == "")
////                    $('#resedentialAddress-error').css('display', 'block');
////                else
////                    $('#resedentialAddress-error').css('display', 'none');
//
//                $('.residentialAddr_div').addClass('hidden');
//                $('.currentResidentialAddress').removeClass('hidden');
//                $('.currentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//
//                if (residentAddressField1 == "" && residentAddressField2 == "" && pin == "" && city == "" && state == "")
//                    $('.currentResidentialAddress').val('');
//            }
//        }
//
//        if (!$(event.target).closest('.permanent_residentAddress').length) {
//            residentAddressField1 = $('.permanent_AddressField1').val();
//            residentAddressField2 = $('.permanent_AddressField2').val();
//            residentAddressField3 = $('.permanent_AddressField3').val();
//            pin = $('.permanent_residentAddress .pincode').val();
//            city = "";
//            state = "";
//            if (($('.permanent_residentAddress .city option:selected').val()) != 0) {
//                city = $('.permanent_residentAddress .city option:selected').text();
//            }
//            if (($('.permanent_residentAddress .state option:selected').val()) != 0) {
//                state = $('.permanent_residentAddress .state option:selected').text();
//            }
//            $('.permanentAddr_div').addClass('hidden');
//            $('.permanentResidentialAddress').removeClass('hidden');
//            $('.permanentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//
//            if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
//                $('.permanentResidentialAddress').val('');
//            if (document.getElementById("iciciBean"))
//            {
//                pin = $("#PpinCodeICICI").val();
//                city = "";
//                state = "";
//                if (($('.permanent_residentAddress .city option:selected').val()) != 0) {
//                    city = $('.permanent_residentAddress .city option:selected').text();
//                }
//                if (($('.permanent_residentAddress .state option:selected').val()) != 0) {
//                    state = $('.permanent_residentAddress .state option:selected').text();
//                }
////                if (residentAddressField1 == "" || residentAddressField2 == "" || pin == "" || city == "" || state == "")
////                    $('#permanentAddr-error').css('display', 'block');
////                else
////                    $('#permanentAddr-error').css('display', 'none');
//                $('.permanentAddr_div').addClass('hidden');
//                $('.permanentResidentialAddress').removeClass('hidden');
//                $('.permanentResidentialAddress').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//
//                if (residentAddressField1 == "" && residentAddressField2 == "" && pin == "" && city == "" && state == "")
//                    $('.permanentResidentialAddress').val('');
//            }
//        }
//
//        if (!$(event.target).closest('.companyAddress').length) {
//            residentAddressField1 = $('.companyAddressField1').val();
//            residentAddressField2 = $('.companyAddressField2').val();
//            residentAddressField3 = $('.companyAddressField3').val();
//            pin = $('.companyAddress .pincode').val();
//            city = "";
//            state = "";
//            if (($('.companyAddress .city option:selected').val()) != 0) {
//                city = $('.companyAddress .city option:selected').text();
//            }
//            if (($('.companyAddress .state option:selected').val()) != 0) {
//                state = $('.companyAddress .state option:selected').text();
//            }
////            if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
////                $('#companyAddress-error').css('display', 'block');
////            else
////                $('#companyAddress-error').css('display', 'none');
//
//            $('.companyAddr_div').addClass('hidden');
//            $('.companyAddress_box').removeClass('hidden');
//            $('.companyAddress_box').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//
//            if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
//                $('.companyAddress_box').val('');
//
//            if (document.getElementById("iciciBean"))
//            {
//                pin = $("#OPincodeICICI").val();
//                city = "";
//                state = "";
//                if (($('.companyAddress .city option:selected').val()) != 0) {
//                    city = $('.companyAddress .city option:selected').text();
//                }
//                if (($('.companyAddress .state option:selected').val()) != 0) {
//                    state = $('.companyAddress .state option:selected').text();
//                }
////                if (residentAddressField1 == "" || pin == "" || city == "" || state == "")
////                    $('#companyAddress-error').css('display', 'block');
////                else
////                    $('#companyAddress-error').css('display', 'none');
//
//                $('.companyAddr_div').addClass('hidden');
//                $('.companyAddress_box').removeClass('hidden');
//                $('.companyAddress_box').val(residentAddressField1 + " " + residentAddressField2 + " " + residentAddressField3 + " " + city + " " + state + " " + pin);
//
//                if (residentAddressField1 == "" && pin == "" && city == "" && state == "")
//                    $('.companyAddress_box').val('');
//            }
//        }
//        if (!$(event.target).closest('.phoneNum_div').length) {
//            var std = $('.std input').val();
//            var phone = $('.phNum input').val();
//
//            if (std == "" || phone == "")
//                $('#homeNumber-error').css('display', 'block');
//            else {
//                if (onlyNumeric(std) && onlyNumeric(phone))
//                    $('#homeNumber-error').css('display', 'none');
//                else
//                    $('#homeNumber-error').css('display', 'block');
//            }
//            $('.phoneNum_div').addClass('hidden');
//            $('.homeNumber').removeClass('hidden');
//            $('.homeNumber').val(std + " " + phone);
//
//            if (std == "" && phone == "")
//                $('.homeNumber').val('');
//        }
//        if (!$(event.target).closest('.officePhoneNum_div').length) {
//            std = $('.std_office input').val();
//            phone = $('.phNum_office input').val();
//            $('.officePhoneNum_div').addClass('hidden');
//            $('.officeNumber').removeClass('hidden');
//            $("#OStd-error").hide();
//            $("#OPhone-error").hide();
//            $('.officeNumber').val(std + " " + phone);
//
//            if (std == "" && phone == "")
//                $('.officeNumber').val('');
//        }
//    });

    $(document).on('click', ".CrdHP_btn_compare, .CrdRC_btn_compare", function () {

        //Adobe code starts here
        $.fn.CompareCardsClick(selCards);
        //Adobe code ends here

        var utm_source = $(this).attr('data-utm_source');
        var utm_medium = $(this).attr('data-utm_medium');
        var utm_campaign = $(this).attr('data-utm_campaign');
        compareCards_boolean = 1;
        if ($('.ff').html() == 'XX') {
            $("#error_msg").html("To compare cards, select value for your monthly spends");
            $("#error_msg").show();
            $('#spends_slider').modal('show');
            $('.spendsSlider_popup input[type="range"]').val(freeFlights_calc).change();
            if ($(window).width() < 768)
                $('html,body').stop(true, false).animate({scrollTop: $('.spendsVal_error').offset().top + 'px'}, 'fast');

            return false;
        }
        var cpidArr = new Array();
        $('.compareCards:checked').each(function () {
            cpidArr.push($(this).val());
        });
        if (cpidArr.length >= 2) {

            var spend = $("#yearly_bill").html().replace(/<\/?img[^>]*>/g, "");
            spend = spend.substring(spend.indexOf(' ') + 1);

            var cpids = cpidArr.join('|');
            var cardId = "cardNo";

            document.cookie = cardId + "=" + cpids + ";" + 60000 + ";path=/";

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ')
                        c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0)
                        return c.substring(nameEQ.length, c.length);
                }
                return null;
            }

            var value = readCookie('cardNo');

            var redirectUrl = ctx + 'compare-cards';
            $.redirect(redirectUrl, {'cpids': value, 'spend': spend, 'utm_source': utm_source, 'utm_medium': utm_medium, 'utm_campaign': utm_campaign});




        } else {
            return false;
        }

    });
    var newArr = new Array();
    $('.compare_card_holder').addClass('hidden');
    $(document).on("change", ".compareCards", function () {
        var this_val = $(this).val();
        var imgPath = $(this).parents(".row").children(".col-sm-2").find('.card_img img').attr("src");
        _checked = $(".compareCards:checked").length;
        var compareId;
        if ($('#pageName').val() == 'home')
            compareId = 'CrdHP_Compare';
        else
            compareId = 'CrdRC_Compare';

        if ($(this).is(':checked')) {
            $('.compare_card_holder').removeClass('hidden');
            if (_checked == 1) {
                $('.disable_comp_btn').attr("disabled", true);
                $('.disable_comp_btn').css({'background-color': '#cccccc'});
            } else {
                $('.disable_comp_btn').attr("disabled", false);
                $('.disable_comp_btn').css({'background-color': '#ffffff'});
            }
            if (_checked < 1) {
                $('.compare_card_holder').removeClass('fixed_compare_card_holder');
            }
            if (_checked > 0 && _checked <= 3) {
                $('.compare_card_holder').addClass('fixed_compare_card_holder');

                newArr.push({
                    id: this_val,
                    src: imgPath
                });

                for (i in newArr) {
                    $(".compare_card li").eq(i).children(".card_holder").html("<img id='" + newArr[i].id + "' src='" + newArr[i].src + "' alt='card image' />");
                }
            }
            if (_checked >= 3) {
                $('.compareCards:checkbox:not(:checked)').prop('disabled', true);
            }
        } else {
            var ind;
            for (i in newArr) {
                if (newArr[i].id == this_val) {
                    ind = i;
                }
            }
            newArr.splice(ind, 1);
            if (newArr.length == 0) {
                $('.compare_card_holder').addClass('hidden');
            }
            if (_checked == 1) {
                $('.disable_comp_btn').prop("disabled", true);
                $('.disable_comp_btn').css({'background-color': '#cccccc'});
            } else {
                $('.disable_comp_btn').prop("disabled", false);
                $('.disable_comp_btn').css({'background-color': '#ffffff'});
            }
            $(".compare_card li").children(".card_holder").empty();
            for (i in newArr) {
                $(".compare_card li").eq(i).children(".card_holder").html("<img id='" + newArr[i].id + "' src='" + newArr[i].src + "' alt='card image' />");
            }
            $('.compareCards:checkbox:not(:checked)').prop('disabled', false);
        }
        var compareId1 = '';
        $('.compareCards:checkbox:checked').each(function () {
            var cpNo = $(this).val();
            var cardName = $('#cardName' + cpNo).text();
            compareId1 += '_' + cardName;
        });
        compareId = compareId + compareId1;
        $('.CrdHP_btn_compare').attr('id', compareId);
        $('.CrdRC_btn_compare').attr('id', compareId);
    });

    /* for Listing*/

    $('.home_feature').each(function () {
        console.log('inside home features');
        var toggle_img = "<div class='collps_img coldown'><img src=" + ctx + "static/images/co-brand/collps_down.png></div>" +
                "<div class='collps_img colUp' style='display:none'><img src=" + ctx + "static/images/co-brand/collps_up.png></div>";
        if ($(this).height() > 200) {
            $(this).addClass("trimmed");
            $(this).closest(".list_cont").after(toggle_img);
        }
    });

    $(document).on("click", ".collps_img", function () {
        $(this).prev().parents(".row").find(".webView").each(function () {
            if ($(this).find(".collps_img").length == 2) {
                $(this).find(".home_feature").toggleClass("trimmed");
            }
            $(this).children('.coldown').toggle();
            $(this).children('.colUp').toggle();
        });

    });

    /* for Listing End*/


    $(document).on("click", ".applyAmex", function () {
        var imageTextName = $(this).attr('data-name');
        var spMailingID = $(this).attr('data-spMailingID');
        var spUserID = $(this).attr('data-spUserID');
        var spJobID = $(this).attr('data-spJobID');
        var spReportId = $(this).attr('data-spReportId');
        var utm_source = $(this).attr('data-utm_source');
        var utm_medium = $(this).attr('data-utm_medium');
        var utm_campaign = $(this).attr('data-utm_campaign');
        imageTextName = imageTextName.replace(/([^a-zA-Z ])/g, "");
        imageTextName = imageTextName.replace(/\s+/g, "-");
        var trackCode = spMailingID != undefined && spMailingID != "" ? "/?spMailingID=" + spMailingID + "&spUserID=" + spUserID + "&spJobID=" + spJobID + "&spReportId=" + spReportId : "";
        var utm_Param = utm_source != undefined && utm_source != "" ? "/?utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign : "";
        console.log("ctx>>>"+ctx);
        console.log("imageTextName amex>>>"+imageTextName);
        console.log("trackCode amex>>>"+trackCode);
        console.log("utm_Param amex>>>"+utm_Param);
        if (trackCode != "") {
            window.location.href = ctx + "apply-form/" + imageTextName + trackCode;
        } else if (utm_Param != "") {
            window.location.href = ctx + "apply-form/" + imageTextName + utm_Param;
        } else {
            window.location.href = ctx + "apply-form/" + imageTextName
        }
    });
    /* Code  start By Vernost Team*/
    $(document).on("click", ".applyIcici", function () {

        var imageTextName = $(this).attr('data-name');
        var spMailingID = $(this).attr('data-spMailingID');
        var spUserID = $(this).attr('data-spUserID');
        var spJobID = $(this).attr('data-spJobID');
        var spReportId = $(this).attr('data-spReportId');
        var utm_source = $(this).attr('data-utm_source');
        var utm_medium = $(this).attr('data-utm_medium');
        var utm_campaign = $(this).attr('data-utm_campaign');
        imageTextName = imageTextName.replace(/([^a-zA-Z ])/g, "");
        imageTextName = imageTextName.replace(/\s+/g, "-");
        var trackCode = spMailingID != undefined && spMailingID != "" ? "/?spMailingID=" + spMailingID + "&spUserID=" + spUserID + "&spJobID=" + spJobID + "&spReportId=" + spReportId : "";
        var utm_Param = utm_source != undefined && utm_source != "" ? "/?utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign : "";
        if (trackCode != "") {
            window.location.href = ctx + "icici-apply-form/" + imageTextName + trackCode;
        } else if (utm_Param != "") {
            window.location.href = ctx + "icici-apply-form/" + imageTextName + utm_Param;
        } else {
            window.location.href = ctx + "icici-apply-form/" + imageTextName
        }
//        window.location.href = ctx + "icici-apply-form/" + imageTextName + "/?spMailingID=" + spMailingID + "&spUserID=" + spUserID + "&spJobID=" + spJobID + "&spReportId=" + spReportId;
    });
    /* code  End By Vernost Team*/


    $(document).on("click", ".non_amex_apply", function () {
//        $("#otcpNo").val(parseInt($(this).attr('data-cpNo')));
//        $("#otbpNo").val(parseInt($(this).attr('data-bpNo')));

//        console.log('HDFC Bank  : ' + $("#otbpNo").val());
        if ($("#otbpNo").val() == '4') {
            bankNumber = $("#otbpNo").val();
            $("#otcpNo").val($("#otcpNo").val());
            $("#otbpNo").val($("#otbpNo").val());
            $('#askingUser').modal('hide');
            $('#abtYourself').modal('show');
            $("#otbpNo").val('');


        } else {
            $("#otcpNo").val(parseInt($(this).attr('data-cpNo')));
            $("#otbpNo").val(parseInt($(this).attr('data-bpNo')));
//            console.log("Indusind Bank :" + $("#otbpNo").val());
            bankNumber = $("#otbpNo").val();
            $('#askingUser').modal('hide');
            $('#abtYourself').modal('show');
        }




    });

    /*code for hdfc user asking */
    $(document).on('hidden.bs.modal', '.modal', function () {
        if ($('body').find('.modal.in').length > 0) {
            $('body').addClass('modal-open');
        }
    });
    var cardName, NetworkName;
    $(document).on("click", ".hdfc_asking_apply", function () {
        var bankName = $(this).attr('data-bpNo');
//        alert("---->"+$("#otbpNo").val()+"---y"+y);
        if (bankName == '4') {
//             console.log("in if")
            $("#otcpNo").val(parseInt($(this).attr('data-cpNo')));
            $("#otbpNo").val(parseInt($(this).attr('data-bpNo')));

            cardName = $(this).attr('data-cpName');
            NetworkName = $(this).attr('data-NoNO');
            var pageinfo = $(".checkname").attr('id');
//             console.log("a---->",cardName,"b----->",NetworkName,"t---->",pageinfo);
            if (pageinfo == "Home") {
                $('.non_amex_apply').attr('id', 'CrdHP_' + cardName + '_' + NetworkName + '_NO');
                $('.hdfc_credit_card_yes').attr('id', 'CrdHP_' + cardName + '_' + NetworkName + '_YES');
            } else if (pageinfo == "Recommend") {
                $('.non_amex_apply').attr('id', 'CrdRC_' + cardName + '_' + NetworkName + '_NO');
                $('.hdfc_credit_card_yes').attr('id', 'CrdRC_' + cardName + '_' + NetworkName + '_YES');
            } else if (pageinfo == "Compare") {
                $('.non_amex_apply').attr('id', 'CrdCPR_' + cardName + '_' + NetworkName + '_NO');
                $('.hdfc_credit_card_yes').attr('id', 'CrdCPR_' + cardName + '_' + NetworkName + '_YES');
            } else if (pageinfo == "Detail") {
                $('.non_amex_apply').attr('id', 'CrdDTLS_' + cardName + '_' + NetworkName + '_NO');
                $('.hdfc_credit_card_yes').attr('id', 'CrdDTLS_' + cardName + '_' + NetworkName + '_YES');
            }



            $('#askingUser').modal('show');

        } else {
            $("#otcpNo").val(parseInt($(this).attr('data-cpNo')));
            $("#otbpNo").val(parseInt($(this).attr('data-bpNo')));
            $('#askingUser').modal('show');

        }

    });

    $(document).on("click", ".hdfc_credit_card_yes", function () {
//        console.log("at yes button cli===a",cardName,"--b-->",NetworkName);
        if ($("#otbpNo").val() == '4') {
//             console.log("in if")


            var pageinfo = $(".checkname").attr('id');
//             console.log("a---->",cardName,"b----->",NetworkName,"t---->",pageinfo);
            if (pageinfo == "Home") {
                $('.viewCobranCards').attr('id', 'CrdHP_' + cardName + '_' + NetworkName + '_View-Others');
                $('.upgradeNow').attr('id', 'CrdHP_' + cardName + '_' + NetworkName + '_Upgrade');
            } else if (pageinfo == "Recommend") {
                $('.viewCobranCards').attr('id', 'CrdRC_' + cardName + '_' + NetworkName + '_View-Others');
                $('.upgradeNow').attr('id', 'CrdRC_' + cardName + '_' + NetworkName + '_Upgrade');
            } else if (pageinfo == "Compare") {
                $('.viewCobranCards').attr('id', 'CrdCPR_' + cardName + '_' + NetworkName + '_View-Others');
                $('.upgradeNow').attr('id', 'CrdCPR_' + cardName + '_' + NetworkName + '_Upgrade');
            } else if (pageinfo == "Detail") {
                $('.viewCobranCards').attr('id', 'CrdDTLS_' + cardName + '_' + NetworkName + '_View-Others');
                $('.upgradeNow').attr('id', 'CrdDTLS_' + cardName + '_' + NetworkName + '_Upgrade');
            }



            $('#askingUser').modal('hide');
            $('#userWithCreditCard').modal('show');

        } else {
            $('#askingUser').modal('hide');
            $('#userWithCreditCard').modal('show');
        }
    });

//    notofication popup
    $('.completenow').on("click", function () {
        var imageTextName = $(this).attr('data-name');

        var spMailingID = $(this).attr('data-spMailingID');

        var spUserID = $(this).attr('data-spUserID');

        var spJobID = $(this).attr('data-spJobID');

        var spReportId = $(this).attr('data-spReportId');

        var utm_source = $(this).attr('data-utm_source');

        var utm_medium = $(this).attr('data-utm_medium');

        var utm_campaign = $(this).attr('data-utm_campaign');

        imageTextName = imageTextName.replace(/([^a-zA-Z ])/g, "");

        imageTextName = imageTextName.replace(/\s+/g, "-");

        var randomNumber = $(this).attr('random');



        var trackCode = spMailingID != undefined && spMailingID != "" ? "/?spMailingID=" + spMailingID + "&spUserID=" + spUserID + "&spJobID=" + spJobID + "&spReportId=" + spReportId : "";
        var utm_Param = utm_source != undefined && utm_source != "" ? "/?utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign : "";
        if (randomNumber != "") {
            window.location.href = ctx + "apply-form/Jet-Airways-American-Express-Platinum-Credit-Card?randomNumber=" + randomNumber;

        } else if (trackCode != "") {
            window.location.href = ctx + "apply-form/" + imageTextName + trackCode;
        } else if (utm_Param != "") {
            window.location.href = ctx + "apply-form/" + imageTextName + utm_Param;
        } else {
            window.location.href = ctx + "apply-form/" + imageTextName
        }
        $('#notification').modal('hide');
    });



    /* Card details terms and condition*/
    var tcContentHeight = 0;

    if ($('.cont-tc').children().length > 1) {
        $('.cont-tc:lt(5)').children().each(function (i) {
            tcContentHeight += $(this).outerHeight();
            if (i > 0)
            {
                return false;
            }

        });
        $('.tc').css('height', tcContentHeight);
        $('.readMoreTc').removeClass('hidden');
    }
    $('.readMoreTc').click(function () {
        $('.cont-tc').toggleClass('tc');
        $('.cont-tc').css('height', 'auto');
        $('.readMoreTc').addClass('hidden');
        $('.readLessTc').removeClass('hidden');
    });

    $('.readLessTc').click(function () {
        $('.cont-tc').toggleClass('tc');
        $('.tc').css('height', tcContentHeight);
        $('.readMoreTc').removeClass('hidden');
        $('.readLessTc').addClass('hidden');
        $('html,body').stop(true, false).animate({scrollTop: $('.termsAndConditions').offset().top - 100 + 'px'}, 'fast');
    });

    $(document).on('click', '.lifestyle_benefits', function () {
            console.log('inside click llifestyle benefits');
        var cpNo = parseInt($(this).attr('data-cpNo'));
        var gmNo = $('#gmNo').val();
        console.log('gmNO>>'+gmNo);
        $.get(ctx + "getBenefitContent?cpNo=" + cpNo + "&gmNo=" + gmNo, function (data) {
            console.log("data>>>>>>>",data);
            $(data).removeClass('features_mobview');
            $('#benefitContent').html(data);
            $('#benefits').modal('show');
        });

    });
    /* auto suggestion function for enroll city */
    $(document).on('keyup keypress change', '#enrollCity', function () {
        var cityName = $('#enrollCity').val();
        $("#enrollCity").autocomplete({
            minLength: 2,
            source: function (request, response) {
                $.ajax({
                    dataType: "json",
                    data: request,
                    url: ctx + "getCityNames?cityName=" + cityName,
                    success: function (data) {
                        var array = $.map(data, function (key, value) {
                            return {
                                label: key,
                                value: value
                            }
                        });
                        response($.ui.autocomplete.filter(array, request.term));
                    },
                    error: function (data) {

                    }
                })
            },
            focus: function (event, ui) {

                event.preventDefault();
            },
            select: function (event, ui) {
                $("#enrollCity").val(ui.item.value);
                $("#enrollCity").val(ui.item.label);
                return false;
            }
        });
    });
    /* auto suggestion function for enroll city  */

    /*Hide Error Div Element*/
    $(".enroll_popup input").change(function () {
        var len = $('.error_list1 > li:visible').length;
        if (len >= 1) {
            $('.error_box1').removeClass('hidden');
        } else {
            $('.error_box1').addClass('hidden');
        }
    });
    /*Hide Error Div Element*/
$(document).on('click', '.creditfooter', function () {
//        window.open("${applicationURL}credit-score-application-form");
window.location.href=ctx+"credit-score-application-form";
    });
$(document).on('click', '#faq', function () {
//        window.open("${applicationURL}credit-score-application-form");
window.location.href=ctx+"faq";
    });
//$(document).on('click', '#gotohomepage', function () {
////        window.open("${applicationURL}credit-score-application-form");
//window.location.href=ctx;
//    });
    /* terms and condition opens in new tab */
    $(document).on('click', '#tcLink', function () {
        window.open("https://www.jetprivilege.com/terms-and-conditions/cards", '_blank');
    });
    
    
    
    
    /* terms and condition opens in new tab */

    /* For more detail */
    $(document).on('click', '.forMoreDetails', function () {
        var cpNo = $(this).attr('data-id');
        var utm_source = $(this).attr('data-utm_source');
        var utm_medium = $(this).attr('data-utm_medium');
        var utm_campaign = $(this).attr('data-utm_campaign');
        var imageTextName = $(this).attr('data-name').trim();
        imageTextName = imageTextName.replace(/([^a-zA-Z ])/g, "");
        imageTextName = imageTextName.replace(/\s+/g, "-");
        var redirectUrl = ctx + 'card-details/' + imageTextName;
        $.redirect(redirectUrl, {'cpNo': cpNo, 'cardName': imageTextName, 'utm_source': utm_source, 'utm_medium': utm_medium, 'utm_campaign': utm_campaign});
    });

});




/* Hide & Show of lifestyle bebefits */
function selectBenefits(element) {
    $(element).next().children().toggle();
}
/* Hide & Show of lifestyle bebefits */
