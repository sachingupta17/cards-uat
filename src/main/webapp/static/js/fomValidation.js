var dccf = (function () {
        var a = {
            init: function () {
                jppl.init();
                jppl.mobileMenu();
                jppl.masterPageProcess();

                this.domload();
            },
            domload: function () {
             	
            	$('#proceedBtn').click(function () {
           
            		var jpNum,jpNumLastDig,jpNoTemp, lurl_,partnerId_,prtcatno_,prtno_,prtprdctno_;
            		if ($('#jpNum_').val() != ''){
            			jpNum=$('#jpNum_').val();
            			if(jpNum.length!=9)
            			{
            				//$('#jpNum_').addClass('input_error');
            				//$('#jpNum_').parent().append("<div class='input_eror' style='margin:5px 0; position: absolute;padding: 5px;color: rgb(240, 0, 0);'>Invalid JpNumber, Please enter valid JpNumber</div>");
            				$("#error_msg").text("Invalid JPNumber, Please enter valid JPNumber");
            				return false;
            			}
            			jpNumLastDig=jpNum%10;
            			
            			jpNoTemp=eval(jpNum.slice(0,-1))%7;
            			if(jpNumLastDig!==jpNoTemp)
            			{
            				//$('#jpNum_').addClass('input_error');
            				//$('#jpNum_').parent().append("<div class='input_eror' style='margin:5px 0; position: absolute;padding: 5px;color: rgb(240, 0, 0);'>Invalid JpNumber, Please enter valid JpNumber</div>");
            				$("#error_msg").text("Invalid JPNumber, Please enter valid JPNumber");
            				return false;
            				/*validJpNumFlag = true;	
            				$("#jpNumValidation").validate().element('#jpNum_');
            		*/		
            			}
            			/*if(jpNumLastDig==jpNoTemp)
            			{
            				$('#jpNum_').removeClass('input_error');
            				$('.input_eror').empty();
            			}*/
            		}else{
            			jpNum='NA';
            		}
            		prtcatno_=$('#prtcatno_').val();
            		prtno_=$('#prtno_').val();
            		prtprdctno_=$('#prtprdctno_').val();
            		lurl_ = $('#lurl_').val();
            		lurl_=lurl_.replace(/&/g, '---');
            		partnerId_ = $('#partnerId_').val();
            		
            		$('#proceedBtn').attr("href",ctx+"redirectToPartner?partnerId="+partnerId_+"&jpNum="+jpNum+"&prtcatno_="+prtcatno_+"&prtno_="+prtno_+"&prtprdctno_="+prtprdctno_+"&redirectUrl="+lurl_);
            		jQuery('#item_popup').css("display","none");
            		jQuery('#dialog_mask').css("display","none");
            		jQuery('#error_msg').text('');
           		});
            	
            	$('#goToFlipcart').click(function () {
            		
            		var jpNum, lurl_,partnerId_,prtcatno_,prtno_,prtprdctno_;
            		
            		jpNum='NA';
            		prtcatno_=$('#prtcatno_').val();
            		prtno_=$('#prtno_').val();
            		lurl_ = $('#lurl_').val();
            		lurl_=lurl_.replace(/&/g, '---');
            		partnerId_ = $('#partnerId_').val();
            		$('#goToFlipcart').attr("href",ctx+"redirectToPartner?partnerId="+partnerId_+"&jpNum="+jpNum+"&prtcatno_="+prtcatno_+"&prtno_="+prtno_+"&redirectUrl="+lurl_);
            		jQuery('#item_popup').css("display","none");
            		jQuery('#dialog_mask').css("display","none");
            	});
            }
        };
        return a;

    })();