/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package mailmanagement.engageservice;


/**
 *  ExtensionMapper class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ExtensionMapper {
    public static java.lang.Object getTypeObject(
        java.lang.String namespaceURI, java.lang.String typeName,
        javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "MailingTemplateElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.MailingTemplateElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ListMgmtUserSuccess".equals(typeName)) {
            return useractions.listmgmt.engageservice.ListMgmtUserSuccess.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ListTableExportFormat".equals(typeName)) {
            return useractions.listmgmt.engageservice.ListTableExportFormat.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "JoinTableRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.JoinTableRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "FailuresElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.FailuresElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "InsertUpdateRelationalTableRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.InsertUpdateRelationalTableRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "CreateTableRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.CreateTableRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "GetAggregateTrackingForOrgRequestType".equals(typeName)) {
            return reporting.engageservice.GetAggregateTrackingForOrgRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "DateTime1".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.DateTime1.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.SessionMgmt.Login".equals(namespaceURI) &&
                "LoginRequestType".equals(typeName)) {
            return login.sessionmgmt.engageservice.LoginRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ColumnsElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ColumnsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "ImportRecipientFieldDataRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ImportRecipientFieldDataRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "AddContactToContactListRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.AddContactToContactListRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "TableElementType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.TableElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "MultiMailingsElementType".equals(typeName)) {
            return reporting.engageservice.MultiMailingsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) &&
                "TableColumnsElementType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.TableColumnsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "CriteriaElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.CriteriaElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "ExpressionType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ExpressionType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ListTypeId".equals(typeName)) {
            return useractions.listmgmt.engageservice.ListTypeId.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) &&
                "AddRecipientRequestType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.AddRecipientRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "FaultErrorType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.FaultErrorType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "PurgeTableRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.PurgeTableRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "DeleteJobRequestType".equals(typeName)) {
            return reporting.engageservice.DeleteJobRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ListElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ListElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ExpressionType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ExpressionType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "CriteriaElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.CriteriaElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "AddContactToProgramRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.AddContactToProgramRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "JobStatus".equals(typeName)) {
            return reporting.engageservice.JobStatus.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "AddDCRulesetRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.AddDCRulesetRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "FileEncoding".equals(typeName)) {
            return reporting.engageservice.FileEncoding.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.SessionMgmt.Login".equals(namespaceURI) &&
                "LogoutRequestType".equals(typeName)) {
            return login.sessionmgmt.engageservice.LogoutRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "DoubleOptInRequestType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.DoubleOptInRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "CreateContactListRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.CreateContactListRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "GetListsRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.GetListsRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "ValidateDCMailingRulesetType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ValidateDCMailingRulesetType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "FaultDetailType".equals(typeName)) {
            return useractions.listmgmt.engageservice.FaultDetailType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "FaultDetailType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.FaultDetailType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "ExportDCRulesetRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ExportDCRulesetRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.SessionMgmt.Login".equals(namespaceURI) &&
                "FaultErrorType".equals(typeName)) {
            return login.sessionmgmt.engageservice.FaultErrorType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "EmailType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.EmailType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "GetAggregateTrackingForUserRequestType".equals(typeName)) {
            return reporting.engageservice.GetAggregateTrackingForUserRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "TableColumnElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.TableColumnElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement".equals(namespaceURI) &&
                "FaultErrorType".equals(typeName)) {
            return mailmanagement.engageservice.FaultErrorType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "GetListMetaDataRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.GetListMetaDataRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "ParametersElementType".equals(typeName)) {
            return reporting.engageservice.ParametersElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ColumnNameElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ColumnNameElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "ListMgmtUserActionsResponseType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ListMgmtUserActionsResponseType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "DeleteRelationalTableDataRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.DeleteRelationalTableDataRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "AddListColumnRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.AddListColumnRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "ColumnElementType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.ColumnElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement".equals(namespaceURI) &&
                "ForwardToFriendRequestType".equals(typeName)) {
            return mailmanagement.engageservice.ForwardToFriendRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "GetSentMailingsForOrgRequestType".equals(typeName)) {
            return reporting.engageservice.GetSentMailingsForOrgRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "ImportDCRulesetRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ImportDCRulesetRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "ClicksElementType".equals(typeName)) {
            return reporting.engageservice.ClicksElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "ExportMailingTemplateRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ExportMailingTemplateRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "WebTrackingDataExportRequestType".equals(typeName)) {
            return reporting.engageservice.WebTrackingDataExportRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "DeleteRowsElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.DeleteRowsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "SelectionValuesElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.SelectionValuesElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "True".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.True.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "FileEncoding".equals(typeName)) {
            return useractions.listmgmt.engageservice.FileEncoding.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement".equals(namespaceURI) &&
                "MailMgmtSuccess".equals(typeName)) {
            return mailmanagement.engageservice.MailMgmtSuccess.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.SessionHeader".equals(namespaceURI) &&
                "sessionheadertype".equals(typeName)) {
            return sessionheader.engageservice.Sessionheadertype.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "CreatedFrom".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.CreatedFrom.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "FaultDetailType".equals(typeName)) {
            return reporting.engageservice.FaultDetailType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement".equals(namespaceURI) &&
                "FaultType".equals(typeName)) {
            return mailmanagement.engageservice.FaultType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "MailMgmtUserActionsResponseType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.MailMgmtUserActionsResponseType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "DomainsElementType".equals(typeName)) {
            return reporting.engageservice.DomainsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "SubstitutionsElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.SubstitutionsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement".equals(namespaceURI) &&
                "SendMailingRequestType".equals(typeName)) {
            return mailmanagement.engageservice.SendMailingRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "ListDCRulesetsForMailingRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ListDCRulesetsForMailingRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "GetSentMailingsForUserRequestType".equals(typeName)) {
            return reporting.engageservice.GetSentMailingsForUserRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "FailureElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.FailureElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "MapFieldElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.MapFieldElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "GetDCRulesetRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.GetDCRulesetRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.SessionMgmt.Login".equals(namespaceURI) &&
                "FaultType".equals(typeName)) {
            return login.sessionmgmt.engageservice.FaultType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.SessionMgmt.Login".equals(namespaceURI) &&
                "SessionMgmtResponseType".equals(typeName)) {
            return login.sessionmgmt.engageservice.SessionMgmtResponseType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ExportType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ExportType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "DefaultContentElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.DefaultContentElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "FaultType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.FaultType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "ListRecipientMailingsRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ListRecipientMailingsRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "AndOr".equals(typeName)) {
            return useractions.mailmanagement.engageservice.AndOr.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "InboxMonitoringElementType".equals(typeName)) {
            return reporting.engageservice.InboxMonitoringElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ImportTableRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ImportTableRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "AndOr".equals(typeName)) {
            return useractions.listmgmt.engageservice.AndOr.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "ListMgmtSuccess".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.ListMgmtSuccess.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "ColumnsElementType".equals(typeName)) {
            return reporting.engageservice.ColumnsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "CalculateQueryRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.CalculateQueryRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "CriteriaOperator".equals(typeName)) {
            return useractions.listmgmt.engageservice.CriteriaOperator.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "AllowFieldChange".equals(typeName)) {
            return useractions.listmgmt.engageservice.AllowFieldChange.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "RulesetElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.RulesetElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "ReportingResponseType".equals(typeName)) {
            return reporting.engageservice.ReportingResponseType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) &&
                "OptOutRecipientRequestType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.OptOutRecipientRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "FaultErrorType".equals(typeName)) {
            return reporting.engageservice.FaultErrorType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "ContentElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ContentElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "TablesElementType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.TablesElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "MailingElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.MailingElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "SuppressionListsElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.SuppressionListsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "DateTime3".equals(typeName)) {
            return reporting.engageservice.DateTime3.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "FaultType".equals(typeName)) {
            return useractions.listmgmt.engageservice.FaultType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "DateTime4".equals(typeName)) {
            return reporting.engageservice.DateTime4.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) &&
                "ContactListsElementType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.ContactListsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ExportTableRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ExportTableRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "Operators".equals(typeName)) {
            return useractions.mailmanagement.engageservice.Operators.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "RowColumnElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.RowColumnElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "Operators".equals(typeName)) {
            return useractions.listmgmt.engageservice.Operators.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "FaultType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.FaultType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "RemoveRecipientRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.RemoveRecipientRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "SendTimeOptimization".equals(typeName)) {
            return useractions.mailmanagement.engageservice.SendTimeOptimization.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "RuleElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.RuleElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "SubstitutionElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.SubstitutionElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "ColumnElementType".equals(typeName)) {
            return reporting.engageservice.ColumnElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "PreviewMailingRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.PreviewMailingRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "TableColumnType".equals(typeName)) {
            return useractions.listmgmt.engageservice.TableColumnType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "TopDomainElementType".equals(typeName)) {
            return reporting.engageservice.TopDomainElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) &&
                "SelectRecipientRequestType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.SelectRecipientRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ImportListRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ImportListRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "RowsElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.RowsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "Visibility".equals(typeName)) {
            return useractions.listmgmt.engageservice.Visibility.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ExportListRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ExportListRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "True".equals(typeName)) {
            return useractions.listmgmt.engageservice.True.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "ParameterElementType".equals(typeName)) {
            return reporting.engageservice.ParameterElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "InboxMonitoredElementType".equals(typeName)) {
            return reporting.engageservice.InboxMonitoredElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "GetSentMailingsForListRequestType".equals(typeName)) {
            return reporting.engageservice.GetSentMailingsForListRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "TypeOperator".equals(typeName)) {
            return useractions.listmgmt.engageservice.TypeOperator.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "Visibility".equals(typeName)) {
            return useractions.mailmanagement.engageservice.Visibility.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "BehaviorElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.BehaviorElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "ReportingSuccess".equals(typeName)) {
            return reporting.engageservice.ReportingSuccess.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "ReplaceDCRulesetRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ReplaceDCRulesetRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "ContentAreaElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ContentAreaElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ListColumnType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ListColumnType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "DeleteTableRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.DeleteTableRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "RulesElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.RulesElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "ListMgmtResponseType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.ListMgmtResponseType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ColumnElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ColumnElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "ExpressionElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ExpressionElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "ContentsElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ContentsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "ExpressionElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ExpressionElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "GetMailingTemplatesRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.GetMailingTemplatesRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "RawRecipientDataExportRequestType".equals(typeName)) {
            return reporting.engageservice.RawRecipientDataExportRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "EventExportFormat".equals(typeName)) {
            return reporting.engageservice.EventExportFormat.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "ContentAreaType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ContentAreaType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "ColumnNameValueElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.ColumnNameValueElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "SyncFieldsElementType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.SyncFieldsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "TemplateVisibility".equals(typeName)) {
            return useractions.mailmanagement.engageservice.TemplateVisibility.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "GetAggregateTrackingForMailingRequestType".equals(typeName)) {
            return reporting.engageservice.GetAggregateTrackingForMailingRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "RightParens".equals(typeName)) {
            return useractions.mailmanagement.engageservice.RightParens.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "MetricsExportFormat".equals(typeName)) {
            return reporting.engageservice.MetricsExportFormat.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "FaultDetailType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.FaultDetailType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "TrackingMetricMailingElementType".equals(typeName)) {
            return reporting.engageservice.TrackingMetricMailingElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement".equals(namespaceURI) &&
                "SendMailingResponseType".equals(typeName)) {
            return mailmanagement.engageservice.SendMailingResponseType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "RightParens".equals(typeName)) {
            return useractions.listmgmt.engageservice.RightParens.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) &&
                "UpdateRecipientRequestType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.UpdateRecipientRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "ScheduleMailingRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ScheduleMailingRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) &&
                "TableColumnsElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.TableColumnsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "ContentAreasElementType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.ContentAreasElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "MailMgmtUserSuccess".equals(typeName)) {
            return useractions.mailmanagement.engageservice.MailMgmtUserSuccess.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "FaultErrorType".equals(typeName)) {
            return useractions.listmgmt.engageservice.FaultErrorType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "FaultErrorType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.FaultErrorType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "TopDomainsElementType".equals(typeName)) {
            return reporting.engageservice.TopDomainsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "MailingElementType".equals(typeName)) {
            return reporting.engageservice.MailingElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.SessionMgmt.Login".equals(namespaceURI) &&
                "FaultDetailType".equals(typeName)) {
            return login.sessionmgmt.engageservice.FaultDetailType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) &&
                "DeleteDCRulesetRequestType".equals(typeName)) {
            return useractions.mailmanagement.engageservice.DeleteDCRulesetRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "RowElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.RowElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "FaultType".equals(typeName)) {
            return reporting.engageservice.FaultType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement".equals(namespaceURI) &&
                "FaultDetailType".equals(typeName)) {
            return mailmanagement.engageservice.FaultDetailType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "DateTime3".equals(typeName)) {
            return useractions.listmgmt.engageservice.DateTime3.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "DateTime2".equals(typeName)) {
            return useractions.mailmanagement.engageservice.DateTime2.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "SitesElementType".equals(typeName)) {
            return reporting.engageservice.SitesElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "TrackingMetricExportRequestType".equals(typeName)) {
            return reporting.engageservice.TrackingMetricExportRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "TagsElementType".equals(typeName)) {
            return reporting.engageservice.TagsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "PrivateShared".equals(typeName)) {
            return reporting.engageservice.PrivateShared.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "WhereOperator".equals(typeName)) {
            return useractions.listmgmt.engageservice.WhereOperator.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement".equals(namespaceURI) &&
                "ForwardToFriendResponseType".equals(typeName)) {
            return mailmanagement.engageservice.ForwardToFriendResponseType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "GetJobStatusRequestType".equals(typeName)) {
            return reporting.engageservice.GetJobStatusRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "DeleteRowElementType".equals(typeName)) {
            return useractions.listmgmt.engageservice.DeleteRowElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "LeftParens".equals(typeName)) {
            return useractions.listmgmt.engageservice.LeftParens.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.SessionMgmt.Login".equals(namespaceURI) &&
                "SessionMgmtSuccess".equals(typeName)) {
            return login.sessionmgmt.engageservice.SessionMgmtSuccess.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "LeftParens".equals(typeName)) {
            return useractions.mailmanagement.engageservice.LeftParens.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "Date1".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.Date1.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.Reporting".equals(namespaceURI) &&
                "GetReportIdByDateRequestType".equals(typeName)) {
            return reporting.engageservice.GetReportIdByDateRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "CreateQueryRequestType".equals(typeName)) {
            return useractions.listmgmt.engageservice.CreateQueryRequestType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "OptionOperator".equals(typeName)) {
            return useractions.listmgmt.engageservice.OptionOperator.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "CriteriaType".equals(typeName)) {
            return useractions.listmgmt.engageservice.CriteriaType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.MailManagement.UserActions".equals(
                    namespaceURI) && "DateTime3".equals(typeName)) {
            return useractions.mailmanagement.engageservice.DateTime3.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.RecipientActions".equals(
                    namespaceURI) && "ColumnsElementType".equals(typeName)) {
            return recipientactions.listmgmt.engageservice.ColumnsElementType.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "DateTime1".equals(typeName)) {
            return useractions.listmgmt.engageservice.DateTime1.Factory.parse(reader);
        }

        if ("SilverpopApi:EngageService.ListMgmt.UserActions".equals(
                    namespaceURI) && "DateTime2".equals(typeName)) {
            return useractions.listmgmt.engageservice.DateTime2.Factory.parse(reader);
        }

        throw new org.apache.axis2.databinding.ADBException("Unsupported type " +
            namespaceURI + " " + typeName);
    }
}
