
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Transaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Transaction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}CRISReadOnlyBase">
 *       &lt;sequence>
 *         &lt;element name="TransactionIDFamily" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PersonID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MemUID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ActivityID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ActivityDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="EnglishDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FamilyMemUID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CityPair" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartnerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartnerDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TransactionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RedemptionMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TierMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="QrsCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MilesUnExpired" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ExpiryActionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ActualPoints" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PmaType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityTypeCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FamilyID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FamilyTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PRBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="IsCancelled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MCDCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MergedCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsFamilyTransaction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionDateString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityDateString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastExpired" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="IsExpired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsMerged" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecordLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PmaId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TransactionCouponID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CouponDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PmaIdFamily" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TransactionCouponIDFamily" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CouponDetailsFamily" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketNumberTrans" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Mktcareer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Optcareer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Mktfltno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Optfltno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContribMemNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Transaction", propOrder = {
    "transactionIDFamily",
    "personID",
    "memUID",
    "activityID",
    "activityDate",
    "englishDescr",
    "familyMemUID",
    "activityType",
    "cityPair",
    "flightNumber",
    "partnerCode",
    "partnerDescr",
    "transactionDate",
    "transactionType",
    "transactionDescr",
    "redemptionMiles",
    "tierMiles",
    "qrsCode",
    "milesUnExpired",
    "expiryActionDate",
    "actualPoints",
    "pmaType",
    "activityMethod",
    "activityCode",
    "activityTypeCategory",
    "familyID",
    "transactionID",
    "ticketNumber",
    "familyTicketNumber",
    "prbr",
    "statementDate",
    "isCancelled",
    "mcdCardNumber",
    "mergedCardNumber",
    "isFamilyTransaction",
    "transactionDateString",
    "activityDateString",
    "lastExpired",
    "isExpired",
    "activityDescription",
    "isMerged",
    "recordLocator",
    "pmaId",
    "transactionCouponID",
    "couponDetails",
    "pmaIdFamily",
    "transactionCouponIDFamily",
    "couponDetailsFamily",
    "ticketNumberTrans",
    "mktcareer",
    "optcareer",
    "mktfltno",
    "optfltno",
    "contribMemNo"
})
public class Transaction
    extends CRISReadOnlyBase
{

    @XmlElement(name = "TransactionIDFamily")
    protected int transactionIDFamily;
    @XmlElement(name = "PersonID")
    protected int personID;
    @XmlElement(name = "MemUID")
    protected int memUID;
    @XmlElement(name = "ActivityID")
    protected int activityID;
    @XmlElement(name = "ActivityDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar activityDate;
    @XmlElement(name = "EnglishDescr")
    protected String englishDescr;
    @XmlElement(name = "FamilyMemUID")
    protected int familyMemUID;
    @XmlElement(name = "ActivityType")
    protected String activityType;
    @XmlElement(name = "CityPair")
    protected String cityPair;
    @XmlElement(name = "FlightNumber")
    protected String flightNumber;
    @XmlElement(name = "PartnerCode")
    protected String partnerCode;
    @XmlElement(name = "PartnerDescr")
    protected String partnerDescr;
    @XmlElement(name = "TransactionDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transactionDate;
    @XmlElement(name = "TransactionType")
    protected String transactionType;
    @XmlElement(name = "TransactionDescr")
    protected String transactionDescr;
    @XmlElement(name = "RedemptionMiles")
    protected int redemptionMiles;
    @XmlElement(name = "TierMiles")
    protected int tierMiles;
    @XmlElement(name = "QrsCode")
    protected String qrsCode;
    @XmlElement(name = "MilesUnExpired")
    protected int milesUnExpired;
    @XmlElement(name = "ExpiryActionDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expiryActionDate;
    @XmlElement(name = "ActualPoints")
    protected int actualPoints;
    @XmlElement(name = "PmaType")
    protected String pmaType;
    @XmlElement(name = "ActivityMethod")
    protected String activityMethod;
    @XmlElement(name = "ActivityCode")
    protected String activityCode;
    @XmlElement(name = "ActivityTypeCategory")
    protected String activityTypeCategory;
    @XmlElement(name = "FamilyID")
    protected int familyID;
    @XmlElement(name = "TransactionID")
    protected int transactionID;
    @XmlElement(name = "TicketNumber")
    protected String ticketNumber;
    @XmlElement(name = "FamilyTicketNumber")
    protected String familyTicketNumber;
    @XmlElement(name = "PRBR")
    protected String prbr;
    @XmlElement(name = "StatementDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statementDate;
    @XmlElement(name = "IsCancelled")
    protected String isCancelled;
    @XmlElement(name = "MCDCardNumber")
    protected String mcdCardNumber;
    @XmlElement(name = "MergedCardNumber")
    protected String mergedCardNumber;
    @XmlElement(name = "IsFamilyTransaction")
    protected String isFamilyTransaction;
    @XmlElement(name = "TransactionDateString")
    protected String transactionDateString;
    @XmlElement(name = "ActivityDateString")
    protected String activityDateString;
    @XmlElement(name = "LastExpired", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastExpired;
    @XmlElement(name = "IsExpired")
    protected String isExpired;
    @XmlElement(name = "ActivityDescription")
    protected String activityDescription;
    @XmlElement(name = "IsMerged")
    protected String isMerged;
    @XmlElement(name = "RecordLocator")
    protected String recordLocator;
    @XmlElement(name = "PmaId")
    protected int pmaId;
    @XmlElement(name = "TransactionCouponID")
    protected int transactionCouponID;
    @XmlElement(name = "CouponDetails")
    protected String couponDetails;
    @XmlElement(name = "PmaIdFamily")
    protected int pmaIdFamily;
    @XmlElement(name = "TransactionCouponIDFamily")
    protected int transactionCouponIDFamily;
    @XmlElement(name = "CouponDetailsFamily")
    protected String couponDetailsFamily;
    @XmlElement(name = "TicketNumberTrans")
    protected String ticketNumberTrans;
    @XmlElement(name = "Mktcareer")
    protected String mktcareer;
    @XmlElement(name = "Optcareer")
    protected String optcareer;
    @XmlElement(name = "Mktfltno")
    protected String mktfltno;
    @XmlElement(name = "Optfltno")
    protected String optfltno;
    @XmlElement(name = "ContribMemNo")
    protected String contribMemNo;

    /**
     * Gets the value of the transactionIDFamily property.
     * 
     */
    public int getTransactionIDFamily() {
        return transactionIDFamily;
    }

    /**
     * Sets the value of the transactionIDFamily property.
     * 
     */
    public void setTransactionIDFamily(int value) {
        this.transactionIDFamily = value;
    }

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     */
    public int getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     */
    public void setMemUID(int value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the activityID property.
     * 
     */
    public int getActivityID() {
        return activityID;
    }

    /**
     * Sets the value of the activityID property.
     * 
     */
    public void setActivityID(int value) {
        this.activityID = value;
    }

    /**
     * Gets the value of the activityDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActivityDate() {
        return activityDate;
    }

    /**
     * Sets the value of the activityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActivityDate(XMLGregorianCalendar value) {
        this.activityDate = value;
    }

    /**
     * Gets the value of the englishDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnglishDescr() {
        return englishDescr;
    }

    /**
     * Sets the value of the englishDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnglishDescr(String value) {
        this.englishDescr = value;
    }

    /**
     * Gets the value of the familyMemUID property.
     * 
     */
    public int getFamilyMemUID() {
        return familyMemUID;
    }

    /**
     * Sets the value of the familyMemUID property.
     * 
     */
    public void setFamilyMemUID(int value) {
        this.familyMemUID = value;
    }

    /**
     * Gets the value of the activityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * Sets the value of the activityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityType(String value) {
        this.activityType = value;
    }

    /**
     * Gets the value of the cityPair property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityPair() {
        return cityPair;
    }

    /**
     * Sets the value of the cityPair property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityPair(String value) {
        this.cityPair = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the partnerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerCode() {
        return partnerCode;
    }

    /**
     * Sets the value of the partnerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerCode(String value) {
        this.partnerCode = value;
    }

    /**
     * Gets the value of the partnerDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerDescr() {
        return partnerDescr;
    }

    /**
     * Sets the value of the partnerDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerDescr(String value) {
        this.partnerDescr = value;
    }

    /**
     * Gets the value of the transactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransactionDate() {
        return transactionDate;
    }

    /**
     * Sets the value of the transactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransactionDate(XMLGregorianCalendar value) {
        this.transactionDate = value;
    }

    /**
     * Gets the value of the transactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

    /**
     * Gets the value of the transactionDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionDescr() {
        return transactionDescr;
    }

    /**
     * Sets the value of the transactionDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionDescr(String value) {
        this.transactionDescr = value;
    }

    /**
     * Gets the value of the redemptionMiles property.
     * 
     */
    public int getRedemptionMiles() {
        return redemptionMiles;
    }

    /**
     * Sets the value of the redemptionMiles property.
     * 
     */
    public void setRedemptionMiles(int value) {
        this.redemptionMiles = value;
    }

    /**
     * Gets the value of the tierMiles property.
     * 
     */
    public int getTierMiles() {
        return tierMiles;
    }

    /**
     * Sets the value of the tierMiles property.
     * 
     */
    public void setTierMiles(int value) {
        this.tierMiles = value;
    }

    /**
     * Gets the value of the qrsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQrsCode() {
        return qrsCode;
    }

    /**
     * Sets the value of the qrsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQrsCode(String value) {
        this.qrsCode = value;
    }

    /**
     * Gets the value of the milesUnExpired property.
     * 
     */
    public int getMilesUnExpired() {
        return milesUnExpired;
    }

    /**
     * Sets the value of the milesUnExpired property.
     * 
     */
    public void setMilesUnExpired(int value) {
        this.milesUnExpired = value;
    }

    /**
     * Gets the value of the expiryActionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpiryActionDate() {
        return expiryActionDate;
    }

    /**
     * Sets the value of the expiryActionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpiryActionDate(XMLGregorianCalendar value) {
        this.expiryActionDate = value;
    }

    /**
     * Gets the value of the actualPoints property.
     * 
     */
    public int getActualPoints() {
        return actualPoints;
    }

    /**
     * Sets the value of the actualPoints property.
     * 
     */
    public void setActualPoints(int value) {
        this.actualPoints = value;
    }

    /**
     * Gets the value of the pmaType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPmaType() {
        return pmaType;
    }

    /**
     * Sets the value of the pmaType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPmaType(String value) {
        this.pmaType = value;
    }

    /**
     * Gets the value of the activityMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityMethod() {
        return activityMethod;
    }

    /**
     * Sets the value of the activityMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityMethod(String value) {
        this.activityMethod = value;
    }

    /**
     * Gets the value of the activityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityCode() {
        return activityCode;
    }

    /**
     * Sets the value of the activityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityCode(String value) {
        this.activityCode = value;
    }

    /**
     * Gets the value of the activityTypeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityTypeCategory() {
        return activityTypeCategory;
    }

    /**
     * Sets the value of the activityTypeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityTypeCategory(String value) {
        this.activityTypeCategory = value;
    }

    /**
     * Gets the value of the familyID property.
     * 
     */
    public int getFamilyID() {
        return familyID;
    }

    /**
     * Sets the value of the familyID property.
     * 
     */
    public void setFamilyID(int value) {
        this.familyID = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     */
    public int getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     */
    public void setTransactionID(int value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Sets the value of the ticketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Gets the value of the familyTicketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyTicketNumber() {
        return familyTicketNumber;
    }

    /**
     * Sets the value of the familyTicketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyTicketNumber(String value) {
        this.familyTicketNumber = value;
    }

    /**
     * Gets the value of the prbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRBR() {
        return prbr;
    }

    /**
     * Sets the value of the prbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRBR(String value) {
        this.prbr = value;
    }

    /**
     * Gets the value of the statementDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatementDate() {
        return statementDate;
    }

    /**
     * Sets the value of the statementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatementDate(XMLGregorianCalendar value) {
        this.statementDate = value;
    }

    /**
     * Gets the value of the isCancelled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCancelled() {
        return isCancelled;
    }

    /**
     * Sets the value of the isCancelled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCancelled(String value) {
        this.isCancelled = value;
    }

    /**
     * Gets the value of the mcdCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMCDCardNumber() {
        return mcdCardNumber;
    }

    /**
     * Sets the value of the mcdCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMCDCardNumber(String value) {
        this.mcdCardNumber = value;
    }

    /**
     * Gets the value of the mergedCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergedCardNumber() {
        return mergedCardNumber;
    }

    /**
     * Sets the value of the mergedCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergedCardNumber(String value) {
        this.mergedCardNumber = value;
    }

    /**
     * Gets the value of the isFamilyTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFamilyTransaction() {
        return isFamilyTransaction;
    }

    /**
     * Sets the value of the isFamilyTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFamilyTransaction(String value) {
        this.isFamilyTransaction = value;
    }

    /**
     * Gets the value of the transactionDateString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionDateString() {
        return transactionDateString;
    }

    /**
     * Sets the value of the transactionDateString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionDateString(String value) {
        this.transactionDateString = value;
    }

    /**
     * Gets the value of the activityDateString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityDateString() {
        return activityDateString;
    }

    /**
     * Sets the value of the activityDateString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityDateString(String value) {
        this.activityDateString = value;
    }

    /**
     * Gets the value of the lastExpired property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastExpired() {
        return lastExpired;
    }

    /**
     * Sets the value of the lastExpired property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastExpired(XMLGregorianCalendar value) {
        this.lastExpired = value;
    }

    /**
     * Gets the value of the isExpired property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsExpired() {
        return isExpired;
    }

    /**
     * Sets the value of the isExpired property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsExpired(String value) {
        this.isExpired = value;
    }

    /**
     * Gets the value of the activityDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityDescription() {
        return activityDescription;
    }

    /**
     * Sets the value of the activityDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityDescription(String value) {
        this.activityDescription = value;
    }

    /**
     * Gets the value of the isMerged property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMerged() {
        return isMerged;
    }

    /**
     * Sets the value of the isMerged property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMerged(String value) {
        this.isMerged = value;
    }

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordLocator(String value) {
        this.recordLocator = value;
    }

    /**
     * Gets the value of the pmaId property.
     * 
     */
    public int getPmaId() {
        return pmaId;
    }

    /**
     * Sets the value of the pmaId property.
     * 
     */
    public void setPmaId(int value) {
        this.pmaId = value;
    }

    /**
     * Gets the value of the transactionCouponID property.
     * 
     */
    public int getTransactionCouponID() {
        return transactionCouponID;
    }

    /**
     * Sets the value of the transactionCouponID property.
     * 
     */
    public void setTransactionCouponID(int value) {
        this.transactionCouponID = value;
    }

    /**
     * Gets the value of the couponDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponDetails() {
        return couponDetails;
    }

    /**
     * Sets the value of the couponDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponDetails(String value) {
        this.couponDetails = value;
    }

    /**
     * Gets the value of the pmaIdFamily property.
     * 
     */
    public int getPmaIdFamily() {
        return pmaIdFamily;
    }

    /**
     * Sets the value of the pmaIdFamily property.
     * 
     */
    public void setPmaIdFamily(int value) {
        this.pmaIdFamily = value;
    }

    /**
     * Gets the value of the transactionCouponIDFamily property.
     * 
     */
    public int getTransactionCouponIDFamily() {
        return transactionCouponIDFamily;
    }

    /**
     * Sets the value of the transactionCouponIDFamily property.
     * 
     */
    public void setTransactionCouponIDFamily(int value) {
        this.transactionCouponIDFamily = value;
    }

    /**
     * Gets the value of the couponDetailsFamily property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponDetailsFamily() {
        return couponDetailsFamily;
    }

    /**
     * Sets the value of the couponDetailsFamily property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponDetailsFamily(String value) {
        this.couponDetailsFamily = value;
    }

    /**
     * Gets the value of the ticketNumberTrans property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumberTrans() {
        return ticketNumberTrans;
    }

    /**
     * Sets the value of the ticketNumberTrans property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumberTrans(String value) {
        this.ticketNumberTrans = value;
    }

    /**
     * Gets the value of the mktcareer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMktcareer() {
        return mktcareer;
    }

    /**
     * Sets the value of the mktcareer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMktcareer(String value) {
        this.mktcareer = value;
    }

    /**
     * Gets the value of the optcareer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptcareer() {
        return optcareer;
    }

    /**
     * Sets the value of the optcareer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptcareer(String value) {
        this.optcareer = value;
    }

    /**
     * Gets the value of the mktfltno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMktfltno() {
        return mktfltno;
    }

    /**
     * Sets the value of the mktfltno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMktfltno(String value) {
        this.mktfltno = value;
    }

    /**
     * Gets the value of the optfltno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptfltno() {
        return optfltno;
    }

    /**
     * Sets the value of the optfltno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptfltno(String value) {
        this.optfltno = value;
    }

    /**
     * Gets the value of the contribMemNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContribMemNo() {
        return contribMemNo;
    }

    /**
     * Sets the value of the contribMemNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContribMemNo(String value) {
        this.contribMemNo = value;
    }

}
