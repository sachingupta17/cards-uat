/**
 * The Webservice for the Skywards Member
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://skywards.com/Mercator.CRIS.WS", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.skywards.mercator_cris;
