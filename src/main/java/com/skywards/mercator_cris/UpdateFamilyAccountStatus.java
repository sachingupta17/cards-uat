
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FamilyHeadCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HeadPersonid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="HeadMemuid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FamilyAccountStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "familyHeadCardNumber",
    "headPersonid",
    "headMemuid",
    "familyAccountStatus"
})
@XmlRootElement(name = "UpdateFamilyAccountStatus")
public class UpdateFamilyAccountStatus {

    @XmlElement(name = "FamilyHeadCardNumber")
    protected String familyHeadCardNumber;
    @XmlElement(name = "HeadPersonid")
    protected int headPersonid;
    @XmlElement(name = "HeadMemuid")
    protected int headMemuid;
    @XmlElement(name = "FamilyAccountStatus")
    protected String familyAccountStatus;

    /**
     * Gets the value of the familyHeadCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyHeadCardNumber() {
        return familyHeadCardNumber;
    }

    /**
     * Sets the value of the familyHeadCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyHeadCardNumber(String value) {
        this.familyHeadCardNumber = value;
    }

    /**
     * Gets the value of the headPersonid property.
     * 
     */
    public int getHeadPersonid() {
        return headPersonid;
    }

    /**
     * Sets the value of the headPersonid property.
     * 
     */
    public void setHeadPersonid(int value) {
        this.headPersonid = value;
    }

    /**
     * Gets the value of the headMemuid property.
     * 
     */
    public int getHeadMemuid() {
        return headMemuid;
    }

    /**
     * Sets the value of the headMemuid property.
     * 
     */
    public void setHeadMemuid(int value) {
        this.headMemuid = value;
    }

    /**
     * Gets the value of the familyAccountStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyAccountStatus() {
        return familyAccountStatus;
    }

    /**
     * Sets the value of the familyAccountStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyAccountStatus(String value) {
        this.familyAccountStatus = value;
    }

}
