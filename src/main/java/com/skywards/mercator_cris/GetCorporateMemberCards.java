
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sActiveCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PersonID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableOfInt32"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sActiveCardNo",
    "personID"
})
@XmlRootElement(name = "GetCorporateMemberCards")
public class GetCorporateMemberCards {

    protected String sActiveCardNo;
    @XmlElement(name = "PersonID", required = true)
    protected QueryableOfInt32 personID;

    /**
     * Gets the value of the sActiveCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSActiveCardNo() {
        return sActiveCardNo;
    }

    /**
     * Sets the value of the sActiveCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSActiveCardNo(String value) {
        this.sActiveCardNo = value;
    }

    /**
     * Gets the value of the personID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableOfInt32 }
     *     
     */
    public QueryableOfInt32 getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableOfInt32 }
     *     
     */
    public void setPersonID(QueryableOfInt32 value) {
        this.personID = value;
    }

}
