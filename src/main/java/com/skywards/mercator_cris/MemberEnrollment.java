
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="skywardsProfile" type="{http://skywards.com/Mercator.CRIS.WS}SkywardsProfile" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "skywardsProfile"
})
@XmlRootElement(name = "MemberEnrollment")
public class MemberEnrollment {

    protected SkywardsProfile skywardsProfile;

    /**
     * Gets the value of the skywardsProfile property.
     * 
     * @return
     *     possible object is
     *     {@link SkywardsProfile }
     *     
     */
    public SkywardsProfile getSkywardsProfile() {
        return skywardsProfile;
    }

    /**
     * Sets the value of the skywardsProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link SkywardsProfile }
     *     
     */
    public void setSkywardsProfile(SkywardsProfile value) {
        this.skywardsProfile = value;
    }

}
