
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="skywardsProfile" type="{http://skywards.com/Mercator.CRIS.WS}SkywardsProfile" minOccurs="0"/>
 *         &lt;element name="headActiveCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="headPersonID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "skywardsProfile",
    "headActiveCardNumber",
    "headPersonID"
})
@XmlRootElement(name = "AddFamilyMember")
public class AddFamilyMember {

    protected SkywardsProfile skywardsProfile;
    protected String headActiveCardNumber;
    protected int headPersonID;

    /**
     * Gets the value of the skywardsProfile property.
     * 
     * @return
     *     possible object is
     *     {@link SkywardsProfile }
     *     
     */
    public SkywardsProfile getSkywardsProfile() {
        return skywardsProfile;
    }

    /**
     * Sets the value of the skywardsProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link SkywardsProfile }
     *     
     */
    public void setSkywardsProfile(SkywardsProfile value) {
        this.skywardsProfile = value;
    }

    /**
     * Gets the value of the headActiveCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeadActiveCardNumber() {
        return headActiveCardNumber;
    }

    /**
     * Sets the value of the headActiveCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeadActiveCardNumber(String value) {
        this.headActiveCardNumber = value;
    }

    /**
     * Gets the value of the headPersonID property.
     * 
     */
    public int getHeadPersonID() {
        return headPersonID;
    }

    /**
     * Sets the value of the headPersonID property.
     * 
     */
    public void setHeadPersonID(int value) {
        this.headPersonID = value;
    }

}
