
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TicketCouponDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TicketCouponDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TktId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="TktCpnId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="CpnExvoidNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CouponNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckDigit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CouponStatusIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CouponStatusDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RevalHisIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PnrRecLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PnrCreationDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="CheckedPieces" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckedWeight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirlineCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RbdClassCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClassOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="ArrivalTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReservationPnrStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReservationTktStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReservationStatusIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NotValidBeforeDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="NotValidAfterDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="BaggageAllowance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BoardPointStopoverIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BoardPointCityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BoardPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OffpointStopoverIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OffpointCityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Offpoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BoardPointExtnLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BoardPointExtended" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OffpointExtnLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OffpointExtended" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareBasisDataSize" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareBasisData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IndustryDiscountData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MarsChangedDatetime" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="PrimaryDocNbr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VcrCreateDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="TransactionDatetime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntNbr" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="PreviousCouponStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SegmentTypeCCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SegmentStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TktDesignatorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareBreakInd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoluntaryInd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlownFlightNbr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlownServiceStartDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="FlownServiceStartCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlownServiceEndCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlownClassOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlownFlightOrigDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="FareBreakAmt" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="FareBreakDiscAmt" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="DiscountPctInd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PnrId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="TicketIssueDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="OperatingCarrierCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OperatingCarrierFltNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MembershipNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flyer_SFX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightSpecificBouns" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BlackListed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OnboardUpgrade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaseMiles" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TicketCouponDetail", propOrder = {
    "tktId",
    "tktCpnId",
    "cpnExvoidNo",
    "ticketNumber",
    "couponNumber",
    "checkDigit",
    "couponStatusIndicator",
    "couponStatusDescription",
    "revalHisIndicator",
    "pnrRecLocator",
    "pnrCreationDate",
    "checkedPieces",
    "checkedWeight",
    "airlineCode",
    "flightNumber",
    "rbdClassCode",
    "classOfService",
    "departureStatus",
    "departureDate",
    "departureTime",
    "arrivalDate",
    "arrivalTime",
    "reservationPnrStatusCode",
    "reservationTktStatusCode",
    "reservationStatusIndicator",
    "notValidBeforeDate",
    "notValidAfterDate",
    "baggageAllowance",
    "boardPointStopoverIndicator",
    "boardPointCityCode",
    "boardPoint",
    "offpointStopoverIndicator",
    "offpointCityCode",
    "offpoint",
    "boardPointExtnLength",
    "boardPointExtended",
    "offpointExtnLength",
    "offpointExtended",
    "fareBasisDataSize",
    "fareBasisData",
    "industryDiscountData",
    "marsChangedDatetime",
    "primaryDocNbr",
    "vcrCreateDate",
    "transactionDatetime",
    "entNbr",
    "previousCouponStatusCode",
    "segmentTypeCCode",
    "segmentStatusCode",
    "tktDesignatorCode",
    "fareBreakInd",
    "involuntaryInd",
    "flownFlightNbr",
    "flownServiceStartDate",
    "flownServiceStartCity",
    "flownServiceEndCity",
    "flownClassOfService",
    "flownFlightOrigDate",
    "fareBreakAmt",
    "fareBreakDiscAmt",
    "discountPctInd",
    "pnrId",
    "ticketIssueDate",
    "operatingCarrierCode",
    "operatingCarrierFltNum",
    "ticketType",
    "membershipNumber",
    "flyerSFX",
    "flightSpecificBouns",
    "blackListed",
    "onboardUpgrade",
    "baseMiles",
    "paxID"
})
public class TicketCouponDetail {

    @XmlElement(name = "TktId", required = true)
    protected QueryableDecimal tktId;
    @XmlElement(name = "TktCpnId", required = true)
    protected QueryableDecimal tktCpnId;
    @XmlElement(name = "CpnExvoidNo")
    protected String cpnExvoidNo;
    @XmlElement(name = "TicketNumber")
    protected String ticketNumber;
    @XmlElement(name = "CouponNumber")
    protected String couponNumber;
    @XmlElement(name = "CheckDigit")
    protected String checkDigit;
    @XmlElement(name = "CouponStatusIndicator")
    protected String couponStatusIndicator;
    @XmlElement(name = "CouponStatusDescription")
    protected String couponStatusDescription;
    @XmlElement(name = "RevalHisIndicator")
    protected String revalHisIndicator;
    @XmlElement(name = "PnrRecLocator")
    protected String pnrRecLocator;
    @XmlElement(name = "PnrCreationDate", required = true)
    protected QueryableDateTime pnrCreationDate;
    @XmlElement(name = "CheckedPieces")
    protected String checkedPieces;
    @XmlElement(name = "CheckedWeight")
    protected String checkedWeight;
    @XmlElement(name = "AirlineCode")
    protected String airlineCode;
    @XmlElement(name = "FlightNumber")
    protected String flightNumber;
    @XmlElement(name = "RbdClassCode")
    protected String rbdClassCode;
    @XmlElement(name = "ClassOfService")
    protected String classOfService;
    @XmlElement(name = "DepartureStatus")
    protected String departureStatus;
    @XmlElement(name = "DepartureDate", required = true)
    protected QueryableDateTime departureDate;
    @XmlElement(name = "DepartureTime")
    protected String departureTime;
    @XmlElement(name = "ArrivalDate", required = true)
    protected QueryableDateTime arrivalDate;
    @XmlElement(name = "ArrivalTime")
    protected String arrivalTime;
    @XmlElement(name = "ReservationPnrStatusCode")
    protected String reservationPnrStatusCode;
    @XmlElement(name = "ReservationTktStatusCode")
    protected String reservationTktStatusCode;
    @XmlElement(name = "ReservationStatusIndicator")
    protected String reservationStatusIndicator;
    @XmlElement(name = "NotValidBeforeDate", required = true)
    protected QueryableDateTime notValidBeforeDate;
    @XmlElement(name = "NotValidAfterDate", required = true)
    protected QueryableDateTime notValidAfterDate;
    @XmlElement(name = "BaggageAllowance")
    protected String baggageAllowance;
    @XmlElement(name = "BoardPointStopoverIndicator")
    protected String boardPointStopoverIndicator;
    @XmlElement(name = "BoardPointCityCode")
    protected String boardPointCityCode;
    @XmlElement(name = "BoardPoint")
    protected String boardPoint;
    @XmlElement(name = "OffpointStopoverIndicator")
    protected String offpointStopoverIndicator;
    @XmlElement(name = "OffpointCityCode")
    protected String offpointCityCode;
    @XmlElement(name = "Offpoint")
    protected String offpoint;
    @XmlElement(name = "BoardPointExtnLength")
    protected String boardPointExtnLength;
    @XmlElement(name = "BoardPointExtended")
    protected String boardPointExtended;
    @XmlElement(name = "OffpointExtnLength")
    protected String offpointExtnLength;
    @XmlElement(name = "OffpointExtended")
    protected String offpointExtended;
    @XmlElement(name = "FareBasisDataSize")
    protected String fareBasisDataSize;
    @XmlElement(name = "FareBasisData")
    protected String fareBasisData;
    @XmlElement(name = "IndustryDiscountData")
    protected String industryDiscountData;
    @XmlElement(name = "MarsChangedDatetime", required = true)
    protected QueryableDateTime marsChangedDatetime;
    @XmlElement(name = "PrimaryDocNbr")
    protected String primaryDocNbr;
    @XmlElement(name = "VcrCreateDate", required = true)
    protected QueryableDateTime vcrCreateDate;
    @XmlElement(name = "TransactionDatetime")
    protected String transactionDatetime;
    @XmlElement(name = "EntNbr", required = true)
    protected QueryableDecimal entNbr;
    @XmlElement(name = "PreviousCouponStatusCode")
    protected String previousCouponStatusCode;
    @XmlElement(name = "SegmentTypeCCode")
    protected String segmentTypeCCode;
    @XmlElement(name = "SegmentStatusCode")
    protected String segmentStatusCode;
    @XmlElement(name = "TktDesignatorCode")
    protected String tktDesignatorCode;
    @XmlElement(name = "FareBreakInd")
    protected String fareBreakInd;
    @XmlElement(name = "InvoluntaryInd")
    protected String involuntaryInd;
    @XmlElement(name = "FlownFlightNbr")
    protected String flownFlightNbr;
    @XmlElement(name = "FlownServiceStartDate", required = true)
    protected QueryableDateTime flownServiceStartDate;
    @XmlElement(name = "FlownServiceStartCity")
    protected String flownServiceStartCity;
    @XmlElement(name = "FlownServiceEndCity")
    protected String flownServiceEndCity;
    @XmlElement(name = "FlownClassOfService")
    protected String flownClassOfService;
    @XmlElement(name = "FlownFlightOrigDate", required = true)
    protected QueryableDateTime flownFlightOrigDate;
    @XmlElement(name = "FareBreakAmt", required = true)
    protected QueryableDecimal fareBreakAmt;
    @XmlElement(name = "FareBreakDiscAmt", required = true)
    protected QueryableDecimal fareBreakDiscAmt;
    @XmlElement(name = "DiscountPctInd")
    protected String discountPctInd;
    @XmlElement(name = "PnrId", required = true)
    protected QueryableDecimal pnrId;
    @XmlElement(name = "TicketIssueDate", required = true)
    protected QueryableDateTime ticketIssueDate;
    @XmlElement(name = "OperatingCarrierCode")
    protected String operatingCarrierCode;
    @XmlElement(name = "OperatingCarrierFltNum")
    protected String operatingCarrierFltNum;
    @XmlElement(name = "TicketType")
    protected String ticketType;
    @XmlElement(name = "MembershipNumber")
    protected String membershipNumber;
    @XmlElement(name = "Flyer_SFX")
    protected String flyerSFX;
    @XmlElement(name = "FlightSpecificBouns")
    protected String flightSpecificBouns;
    @XmlElement(name = "BlackListed")
    protected String blackListed;
    @XmlElement(name = "OnboardUpgrade")
    protected String onboardUpgrade;
    @XmlElement(name = "BaseMiles")
    protected String baseMiles;
    @XmlElement(name = "PaxID", required = true)
    protected QueryableDecimal paxID;

    /**
     * Gets the value of the tktId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getTktId() {
        return tktId;
    }

    /**
     * Sets the value of the tktId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setTktId(QueryableDecimal value) {
        this.tktId = value;
    }

    /**
     * Gets the value of the tktCpnId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getTktCpnId() {
        return tktCpnId;
    }

    /**
     * Sets the value of the tktCpnId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setTktCpnId(QueryableDecimal value) {
        this.tktCpnId = value;
    }

    /**
     * Gets the value of the cpnExvoidNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpnExvoidNo() {
        return cpnExvoidNo;
    }

    /**
     * Sets the value of the cpnExvoidNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpnExvoidNo(String value) {
        this.cpnExvoidNo = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Sets the value of the ticketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Gets the value of the couponNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponNumber() {
        return couponNumber;
    }

    /**
     * Sets the value of the couponNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponNumber(String value) {
        this.couponNumber = value;
    }

    /**
     * Gets the value of the checkDigit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckDigit() {
        return checkDigit;
    }

    /**
     * Sets the value of the checkDigit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckDigit(String value) {
        this.checkDigit = value;
    }

    /**
     * Gets the value of the couponStatusIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponStatusIndicator() {
        return couponStatusIndicator;
    }

    /**
     * Sets the value of the couponStatusIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponStatusIndicator(String value) {
        this.couponStatusIndicator = value;
    }

    /**
     * Gets the value of the couponStatusDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponStatusDescription() {
        return couponStatusDescription;
    }

    /**
     * Sets the value of the couponStatusDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponStatusDescription(String value) {
        this.couponStatusDescription = value;
    }

    /**
     * Gets the value of the revalHisIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevalHisIndicator() {
        return revalHisIndicator;
    }

    /**
     * Sets the value of the revalHisIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevalHisIndicator(String value) {
        this.revalHisIndicator = value;
    }

    /**
     * Gets the value of the pnrRecLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnrRecLocator() {
        return pnrRecLocator;
    }

    /**
     * Sets the value of the pnrRecLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnrRecLocator(String value) {
        this.pnrRecLocator = value;
    }

    /**
     * Gets the value of the pnrCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getPnrCreationDate() {
        return pnrCreationDate;
    }

    /**
     * Sets the value of the pnrCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setPnrCreationDate(QueryableDateTime value) {
        this.pnrCreationDate = value;
    }

    /**
     * Gets the value of the checkedPieces property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckedPieces() {
        return checkedPieces;
    }

    /**
     * Sets the value of the checkedPieces property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckedPieces(String value) {
        this.checkedPieces = value;
    }

    /**
     * Gets the value of the checkedWeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckedWeight() {
        return checkedWeight;
    }

    /**
     * Sets the value of the checkedWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckedWeight(String value) {
        this.checkedWeight = value;
    }

    /**
     * Gets the value of the airlineCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineCode() {
        return airlineCode;
    }

    /**
     * Sets the value of the airlineCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineCode(String value) {
        this.airlineCode = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the rbdClassCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRbdClassCode() {
        return rbdClassCode;
    }

    /**
     * Sets the value of the rbdClassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRbdClassCode(String value) {
        this.rbdClassCode = value;
    }

    /**
     * Gets the value of the classOfService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassOfService() {
        return classOfService;
    }

    /**
     * Sets the value of the classOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassOfService(String value) {
        this.classOfService = value;
    }

    /**
     * Gets the value of the departureStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureStatus() {
        return departureStatus;
    }

    /**
     * Sets the value of the departureStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureStatus(String value) {
        this.departureStatus = value;
    }

    /**
     * Gets the value of the departureDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getDepartureDate() {
        return departureDate;
    }

    /**
     * Sets the value of the departureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setDepartureDate(QueryableDateTime value) {
        this.departureDate = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureTime(String value) {
        this.departureTime = value;
    }

    /**
     * Gets the value of the arrivalDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getArrivalDate() {
        return arrivalDate;
    }

    /**
     * Sets the value of the arrivalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setArrivalDate(QueryableDateTime value) {
        this.arrivalDate = value;
    }

    /**
     * Gets the value of the arrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets the value of the arrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalTime(String value) {
        this.arrivalTime = value;
    }

    /**
     * Gets the value of the reservationPnrStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationPnrStatusCode() {
        return reservationPnrStatusCode;
    }

    /**
     * Sets the value of the reservationPnrStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationPnrStatusCode(String value) {
        this.reservationPnrStatusCode = value;
    }

    /**
     * Gets the value of the reservationTktStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationTktStatusCode() {
        return reservationTktStatusCode;
    }

    /**
     * Sets the value of the reservationTktStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationTktStatusCode(String value) {
        this.reservationTktStatusCode = value;
    }

    /**
     * Gets the value of the reservationStatusIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationStatusIndicator() {
        return reservationStatusIndicator;
    }

    /**
     * Sets the value of the reservationStatusIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationStatusIndicator(String value) {
        this.reservationStatusIndicator = value;
    }

    /**
     * Gets the value of the notValidBeforeDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getNotValidBeforeDate() {
        return notValidBeforeDate;
    }

    /**
     * Sets the value of the notValidBeforeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setNotValidBeforeDate(QueryableDateTime value) {
        this.notValidBeforeDate = value;
    }

    /**
     * Gets the value of the notValidAfterDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getNotValidAfterDate() {
        return notValidAfterDate;
    }

    /**
     * Sets the value of the notValidAfterDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setNotValidAfterDate(QueryableDateTime value) {
        this.notValidAfterDate = value;
    }

    /**
     * Gets the value of the baggageAllowance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaggageAllowance() {
        return baggageAllowance;
    }

    /**
     * Sets the value of the baggageAllowance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaggageAllowance(String value) {
        this.baggageAllowance = value;
    }

    /**
     * Gets the value of the boardPointStopoverIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPointStopoverIndicator() {
        return boardPointStopoverIndicator;
    }

    /**
     * Sets the value of the boardPointStopoverIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPointStopoverIndicator(String value) {
        this.boardPointStopoverIndicator = value;
    }

    /**
     * Gets the value of the boardPointCityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPointCityCode() {
        return boardPointCityCode;
    }

    /**
     * Sets the value of the boardPointCityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPointCityCode(String value) {
        this.boardPointCityCode = value;
    }

    /**
     * Gets the value of the boardPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPoint() {
        return boardPoint;
    }

    /**
     * Sets the value of the boardPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPoint(String value) {
        this.boardPoint = value;
    }

    /**
     * Gets the value of the offpointStopoverIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffpointStopoverIndicator() {
        return offpointStopoverIndicator;
    }

    /**
     * Sets the value of the offpointStopoverIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffpointStopoverIndicator(String value) {
        this.offpointStopoverIndicator = value;
    }

    /**
     * Gets the value of the offpointCityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffpointCityCode() {
        return offpointCityCode;
    }

    /**
     * Sets the value of the offpointCityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffpointCityCode(String value) {
        this.offpointCityCode = value;
    }

    /**
     * Gets the value of the offpoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffpoint() {
        return offpoint;
    }

    /**
     * Sets the value of the offpoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffpoint(String value) {
        this.offpoint = value;
    }

    /**
     * Gets the value of the boardPointExtnLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPointExtnLength() {
        return boardPointExtnLength;
    }

    /**
     * Sets the value of the boardPointExtnLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPointExtnLength(String value) {
        this.boardPointExtnLength = value;
    }

    /**
     * Gets the value of the boardPointExtended property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPointExtended() {
        return boardPointExtended;
    }

    /**
     * Sets the value of the boardPointExtended property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPointExtended(String value) {
        this.boardPointExtended = value;
    }

    /**
     * Gets the value of the offpointExtnLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffpointExtnLength() {
        return offpointExtnLength;
    }

    /**
     * Sets the value of the offpointExtnLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffpointExtnLength(String value) {
        this.offpointExtnLength = value;
    }

    /**
     * Gets the value of the offpointExtended property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffpointExtended() {
        return offpointExtended;
    }

    /**
     * Sets the value of the offpointExtended property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffpointExtended(String value) {
        this.offpointExtended = value;
    }

    /**
     * Gets the value of the fareBasisDataSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasisDataSize() {
        return fareBasisDataSize;
    }

    /**
     * Sets the value of the fareBasisDataSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasisDataSize(String value) {
        this.fareBasisDataSize = value;
    }

    /**
     * Gets the value of the fareBasisData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasisData() {
        return fareBasisData;
    }

    /**
     * Sets the value of the fareBasisData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasisData(String value) {
        this.fareBasisData = value;
    }

    /**
     * Gets the value of the industryDiscountData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustryDiscountData() {
        return industryDiscountData;
    }

    /**
     * Sets the value of the industryDiscountData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustryDiscountData(String value) {
        this.industryDiscountData = value;
    }

    /**
     * Gets the value of the marsChangedDatetime property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getMarsChangedDatetime() {
        return marsChangedDatetime;
    }

    /**
     * Sets the value of the marsChangedDatetime property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setMarsChangedDatetime(QueryableDateTime value) {
        this.marsChangedDatetime = value;
    }

    /**
     * Gets the value of the primaryDocNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryDocNbr() {
        return primaryDocNbr;
    }

    /**
     * Sets the value of the primaryDocNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryDocNbr(String value) {
        this.primaryDocNbr = value;
    }

    /**
     * Gets the value of the vcrCreateDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getVcrCreateDate() {
        return vcrCreateDate;
    }

    /**
     * Sets the value of the vcrCreateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setVcrCreateDate(QueryableDateTime value) {
        this.vcrCreateDate = value;
    }

    /**
     * Gets the value of the transactionDatetime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionDatetime() {
        return transactionDatetime;
    }

    /**
     * Sets the value of the transactionDatetime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionDatetime(String value) {
        this.transactionDatetime = value;
    }

    /**
     * Gets the value of the entNbr property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getEntNbr() {
        return entNbr;
    }

    /**
     * Sets the value of the entNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setEntNbr(QueryableDecimal value) {
        this.entNbr = value;
    }

    /**
     * Gets the value of the previousCouponStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousCouponStatusCode() {
        return previousCouponStatusCode;
    }

    /**
     * Sets the value of the previousCouponStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousCouponStatusCode(String value) {
        this.previousCouponStatusCode = value;
    }

    /**
     * Gets the value of the segmentTypeCCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentTypeCCode() {
        return segmentTypeCCode;
    }

    /**
     * Sets the value of the segmentTypeCCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentTypeCCode(String value) {
        this.segmentTypeCCode = value;
    }

    /**
     * Gets the value of the segmentStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentStatusCode() {
        return segmentStatusCode;
    }

    /**
     * Sets the value of the segmentStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentStatusCode(String value) {
        this.segmentStatusCode = value;
    }

    /**
     * Gets the value of the tktDesignatorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTktDesignatorCode() {
        return tktDesignatorCode;
    }

    /**
     * Sets the value of the tktDesignatorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTktDesignatorCode(String value) {
        this.tktDesignatorCode = value;
    }

    /**
     * Gets the value of the fareBreakInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBreakInd() {
        return fareBreakInd;
    }

    /**
     * Sets the value of the fareBreakInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBreakInd(String value) {
        this.fareBreakInd = value;
    }

    /**
     * Gets the value of the involuntaryInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoluntaryInd() {
        return involuntaryInd;
    }

    /**
     * Sets the value of the involuntaryInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoluntaryInd(String value) {
        this.involuntaryInd = value;
    }

    /**
     * Gets the value of the flownFlightNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlownFlightNbr() {
        return flownFlightNbr;
    }

    /**
     * Sets the value of the flownFlightNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlownFlightNbr(String value) {
        this.flownFlightNbr = value;
    }

    /**
     * Gets the value of the flownServiceStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getFlownServiceStartDate() {
        return flownServiceStartDate;
    }

    /**
     * Sets the value of the flownServiceStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setFlownServiceStartDate(QueryableDateTime value) {
        this.flownServiceStartDate = value;
    }

    /**
     * Gets the value of the flownServiceStartCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlownServiceStartCity() {
        return flownServiceStartCity;
    }

    /**
     * Sets the value of the flownServiceStartCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlownServiceStartCity(String value) {
        this.flownServiceStartCity = value;
    }

    /**
     * Gets the value of the flownServiceEndCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlownServiceEndCity() {
        return flownServiceEndCity;
    }

    /**
     * Sets the value of the flownServiceEndCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlownServiceEndCity(String value) {
        this.flownServiceEndCity = value;
    }

    /**
     * Gets the value of the flownClassOfService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlownClassOfService() {
        return flownClassOfService;
    }

    /**
     * Sets the value of the flownClassOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlownClassOfService(String value) {
        this.flownClassOfService = value;
    }

    /**
     * Gets the value of the flownFlightOrigDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getFlownFlightOrigDate() {
        return flownFlightOrigDate;
    }

    /**
     * Sets the value of the flownFlightOrigDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setFlownFlightOrigDate(QueryableDateTime value) {
        this.flownFlightOrigDate = value;
    }

    /**
     * Gets the value of the fareBreakAmt property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getFareBreakAmt() {
        return fareBreakAmt;
    }

    /**
     * Sets the value of the fareBreakAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setFareBreakAmt(QueryableDecimal value) {
        this.fareBreakAmt = value;
    }

    /**
     * Gets the value of the fareBreakDiscAmt property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getFareBreakDiscAmt() {
        return fareBreakDiscAmt;
    }

    /**
     * Sets the value of the fareBreakDiscAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setFareBreakDiscAmt(QueryableDecimal value) {
        this.fareBreakDiscAmt = value;
    }

    /**
     * Gets the value of the discountPctInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountPctInd() {
        return discountPctInd;
    }

    /**
     * Sets the value of the discountPctInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountPctInd(String value) {
        this.discountPctInd = value;
    }

    /**
     * Gets the value of the pnrId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getPnrId() {
        return pnrId;
    }

    /**
     * Sets the value of the pnrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setPnrId(QueryableDecimal value) {
        this.pnrId = value;
    }

    /**
     * Gets the value of the ticketIssueDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getTicketIssueDate() {
        return ticketIssueDate;
    }

    /**
     * Sets the value of the ticketIssueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setTicketIssueDate(QueryableDateTime value) {
        this.ticketIssueDate = value;
    }

    /**
     * Gets the value of the operatingCarrierCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatingCarrierCode() {
        return operatingCarrierCode;
    }

    /**
     * Sets the value of the operatingCarrierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatingCarrierCode(String value) {
        this.operatingCarrierCode = value;
    }

    /**
     * Gets the value of the operatingCarrierFltNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatingCarrierFltNum() {
        return operatingCarrierFltNum;
    }

    /**
     * Sets the value of the operatingCarrierFltNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatingCarrierFltNum(String value) {
        this.operatingCarrierFltNum = value;
    }

    /**
     * Gets the value of the ticketType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketType() {
        return ticketType;
    }

    /**
     * Sets the value of the ticketType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketType(String value) {
        this.ticketType = value;
    }

    /**
     * Gets the value of the membershipNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMembershipNumber() {
        return membershipNumber;
    }

    /**
     * Sets the value of the membershipNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMembershipNumber(String value) {
        this.membershipNumber = value;
    }

    /**
     * Gets the value of the flyerSFX property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlyerSFX() {
        return flyerSFX;
    }

    /**
     * Sets the value of the flyerSFX property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlyerSFX(String value) {
        this.flyerSFX = value;
    }

    /**
     * Gets the value of the flightSpecificBouns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightSpecificBouns() {
        return flightSpecificBouns;
    }

    /**
     * Sets the value of the flightSpecificBouns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightSpecificBouns(String value) {
        this.flightSpecificBouns = value;
    }

    /**
     * Gets the value of the blackListed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlackListed() {
        return blackListed;
    }

    /**
     * Sets the value of the blackListed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlackListed(String value) {
        this.blackListed = value;
    }

    /**
     * Gets the value of the onboardUpgrade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnboardUpgrade() {
        return onboardUpgrade;
    }

    /**
     * Sets the value of the onboardUpgrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnboardUpgrade(String value) {
        this.onboardUpgrade = value;
    }

    /**
     * Gets the value of the baseMiles property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseMiles() {
        return baseMiles;
    }

    /**
     * Sets the value of the baseMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseMiles(String value) {
        this.baseMiles = value;
    }

    /**
     * Gets the value of the paxID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getPaxID() {
        return paxID;
    }

    /**
     * Sets the value of the paxID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setPaxID(QueryableDecimal value) {
        this.paxID = value;
    }

}
