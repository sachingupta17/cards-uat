
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfMemberSalesOfficeDtl complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfMemberSalesOfficeDtl">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberSalesOfficeDtl" type="{http://skywards.com/Mercator.CRIS.WS}MemberSalesOfficeDtl" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfMemberSalesOfficeDtl", propOrder = {
    "memberSalesOfficeDtl"
})
public class ArrayOfMemberSalesOfficeDtl {

    @XmlElement(name = "MemberSalesOfficeDtl", nillable = true)
    protected List<MemberSalesOfficeDtl> memberSalesOfficeDtl;

    /**
     * Gets the value of the memberSalesOfficeDtl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the memberSalesOfficeDtl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMemberSalesOfficeDtl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MemberSalesOfficeDtl }
     * 
     * 
     */
    public List<MemberSalesOfficeDtl> getMemberSalesOfficeDtl() {
        if (memberSalesOfficeDtl == null) {
            memberSalesOfficeDtl = new ArrayList<MemberSalesOfficeDtl>();
        }
        return this.memberSalesOfficeDtl;
    }

}
