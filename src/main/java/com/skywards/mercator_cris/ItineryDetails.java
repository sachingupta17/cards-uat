
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ItineryDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItineryDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServiceClassCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BoardPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UtcDeparture" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="DepartureDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OffPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OffDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="BookingStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UtcArrival" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineryDetails", propOrder = {
    "serviceClassCode",
    "boardPoint",
    "arrivalTime",
    "utcDeparture",
    "departureDate",
    "departureTime",
    "offPoint",
    "offDate",
    "bookingStatus",
    "flightNumber",
    "utcArrival"
})
public class ItineryDetails {

    @XmlElement(name = "ServiceClassCode")
    protected String serviceClassCode;
    @XmlElement(name = "BoardPoint")
    protected String boardPoint;
    @XmlElement(name = "ArrivalTime")
    protected String arrivalTime;
    @XmlElement(name = "UtcDeparture", required = true)
    protected QueryableDateTime utcDeparture;
    @XmlElement(name = "DepartureDate", required = true)
    protected QueryableDateTime departureDate;
    @XmlElement(name = "DepartureTime")
    protected String departureTime;
    @XmlElement(name = "OffPoint")
    protected String offPoint;
    @XmlElement(name = "OffDate", required = true)
    protected QueryableDateTime offDate;
    @XmlElement(name = "BookingStatus")
    protected String bookingStatus;
    @XmlElement(name = "FlightNumber")
    protected String flightNumber;
    @XmlElement(name = "UtcArrival", required = true)
    protected QueryableDateTime utcArrival;

    /**
     * Gets the value of the serviceClassCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceClassCode() {
        return serviceClassCode;
    }

    /**
     * Sets the value of the serviceClassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceClassCode(String value) {
        this.serviceClassCode = value;
    }

    /**
     * Gets the value of the boardPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPoint() {
        return boardPoint;
    }

    /**
     * Sets the value of the boardPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPoint(String value) {
        this.boardPoint = value;
    }

    /**
     * Gets the value of the arrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets the value of the arrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalTime(String value) {
        this.arrivalTime = value;
    }

    /**
     * Gets the value of the utcDeparture property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getUtcDeparture() {
        return utcDeparture;
    }

    /**
     * Sets the value of the utcDeparture property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setUtcDeparture(QueryableDateTime value) {
        this.utcDeparture = value;
    }

    /**
     * Gets the value of the departureDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getDepartureDate() {
        return departureDate;
    }

    /**
     * Sets the value of the departureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setDepartureDate(QueryableDateTime value) {
        this.departureDate = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureTime(String value) {
        this.departureTime = value;
    }

    /**
     * Gets the value of the offPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffPoint() {
        return offPoint;
    }

    /**
     * Sets the value of the offPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffPoint(String value) {
        this.offPoint = value;
    }

    /**
     * Gets the value of the offDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getOffDate() {
        return offDate;
    }

    /**
     * Sets the value of the offDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setOffDate(QueryableDateTime value) {
        this.offDate = value;
    }

    /**
     * Gets the value of the bookingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * Sets the value of the bookingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingStatus(String value) {
        this.bookingStatus = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the utcArrival property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getUtcArrival() {
        return utcArrival;
    }

    /**
     * Sets the value of the utcArrival property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setUtcArrival(QueryableDateTime value) {
        this.utcArrival = value;
    }

}
