
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="milesItneryDetails" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfMilesItineryDetailWE" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "milesItneryDetails"
})
@XmlRootElement(name = "GetMilesAccelerators")
public class GetMilesAccelerators {

    protected ArrayOfMilesItineryDetailWE milesItneryDetails;

    /**
     * Gets the value of the milesItneryDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMilesItineryDetailWE }
     *     
     */
    public ArrayOfMilesItineryDetailWE getMilesItneryDetails() {
        return milesItneryDetails;
    }

    /**
     * Sets the value of the milesItneryDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMilesItineryDetailWE }
     *     
     */
    public void setMilesItneryDetails(ArrayOfMilesItineryDetailWE value) {
        this.milesItneryDetails = value;
    }

}
