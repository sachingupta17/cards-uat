
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="creditCardsList" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfCreditCard" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "personID",
    "creditCardsList"
})
@XmlRootElement(name = "UpdateCreditCards")
public class UpdateCreditCards {

    protected int personID;
    protected ArrayOfCreditCard creditCardsList;

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the creditCardsList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCreditCard }
     *     
     */
    public ArrayOfCreditCard getCreditCardsList() {
        return creditCardsList;
    }

    /**
     * Sets the value of the creditCardsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCreditCard }
     *     
     */
    public void setCreditCardsList(ArrayOfCreditCard value) {
        this.creditCardsList = value;
    }

}
