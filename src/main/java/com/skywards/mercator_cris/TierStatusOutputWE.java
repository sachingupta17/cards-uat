
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TierStatusOutputWE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TierStatusOutputWE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TierMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Miles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TierDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TierStatusType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TierType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MilesForUpgrade" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TierReviewDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TierStatusOutputWE", propOrder = {
    "tierMonth",
    "miles",
    "tierDate",
    "tierStatusType",
    "tierType",
    "milesForUpgrade",
    "tierReviewDate"
})
public class TierStatusOutputWE {

    @XmlElement(name = "TierMonth")
    protected String tierMonth;
    @XmlElement(name = "Miles")
    protected int miles;
    @XmlElement(name = "TierDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar tierDate;
    @XmlElement(name = "TierStatusType")
    protected String tierStatusType;
    @XmlElement(name = "TierType")
    protected String tierType;
    @XmlElement(name = "MilesForUpgrade")
    protected int milesForUpgrade;
    @XmlElement(name = "TierReviewDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar tierReviewDate;

    /**
     * Gets the value of the tierMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierMonth() {
        return tierMonth;
    }

    /**
     * Sets the value of the tierMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierMonth(String value) {
        this.tierMonth = value;
    }

    /**
     * Gets the value of the miles property.
     * 
     */
    public int getMiles() {
        return miles;
    }

    /**
     * Sets the value of the miles property.
     * 
     */
    public void setMiles(int value) {
        this.miles = value;
    }

    /**
     * Gets the value of the tierDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTierDate() {
        return tierDate;
    }

    /**
     * Sets the value of the tierDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTierDate(XMLGregorianCalendar value) {
        this.tierDate = value;
    }

    /**
     * Gets the value of the tierStatusType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierStatusType() {
        return tierStatusType;
    }

    /**
     * Sets the value of the tierStatusType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierStatusType(String value) {
        this.tierStatusType = value;
    }

    /**
     * Gets the value of the tierType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierType() {
        return tierType;
    }

    /**
     * Sets the value of the tierType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierType(String value) {
        this.tierType = value;
    }

    /**
     * Gets the value of the milesForUpgrade property.
     * 
     */
    public int getMilesForUpgrade() {
        return milesForUpgrade;
    }

    /**
     * Sets the value of the milesForUpgrade property.
     * 
     */
    public void setMilesForUpgrade(int value) {
        this.milesForUpgrade = value;
    }

    /**
     * Gets the value of the tierReviewDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTierReviewDate() {
        return tierReviewDate;
    }

    /**
     * Sets the value of the tierReviewDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTierReviewDate(XMLGregorianCalendar value) {
        this.tierReviewDate = value;
    }

}
