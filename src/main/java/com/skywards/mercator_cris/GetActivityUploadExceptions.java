
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="memUID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="aubID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="auetCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="partnerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="activityDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="activityNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="activityTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cityPair" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cabinClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recordLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aubtCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "personID",
    "memUID",
    "cardNumber",
    "id",
    "aubID",
    "auetCode",
    "partnerCode",
    "activityDate",
    "activityNo",
    "activityTypeCode",
    "cityPair",
    "cabinClass",
    "recordLocator",
    "aubtCode"
})
@XmlRootElement(name = "GetActivityUploadExceptions")
public class GetActivityUploadExceptions {

    protected int personID;
    protected int memUID;
    protected String cardNumber;
    protected int id;
    protected int aubID;
    protected String auetCode;
    protected String partnerCode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar activityDate;
    protected String activityNo;
    protected String activityTypeCode;
    protected String cityPair;
    protected String cabinClass;
    protected String recordLocator;
    protected String aubtCode;

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     */
    public int getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     */
    public void setMemUID(int value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the aubID property.
     * 
     */
    public int getAubID() {
        return aubID;
    }

    /**
     * Sets the value of the aubID property.
     * 
     */
    public void setAubID(int value) {
        this.aubID = value;
    }

    /**
     * Gets the value of the auetCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuetCode() {
        return auetCode;
    }

    /**
     * Sets the value of the auetCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuetCode(String value) {
        this.auetCode = value;
    }

    /**
     * Gets the value of the partnerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerCode() {
        return partnerCode;
    }

    /**
     * Sets the value of the partnerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerCode(String value) {
        this.partnerCode = value;
    }

    /**
     * Gets the value of the activityDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActivityDate() {
        return activityDate;
    }

    /**
     * Sets the value of the activityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActivityDate(XMLGregorianCalendar value) {
        this.activityDate = value;
    }

    /**
     * Gets the value of the activityNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityNo() {
        return activityNo;
    }

    /**
     * Sets the value of the activityNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityNo(String value) {
        this.activityNo = value;
    }

    /**
     * Gets the value of the activityTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityTypeCode() {
        return activityTypeCode;
    }

    /**
     * Sets the value of the activityTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityTypeCode(String value) {
        this.activityTypeCode = value;
    }

    /**
     * Gets the value of the cityPair property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityPair() {
        return cityPair;
    }

    /**
     * Sets the value of the cityPair property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityPair(String value) {
        this.cityPair = value;
    }

    /**
     * Gets the value of the cabinClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabinClass() {
        return cabinClass;
    }

    /**
     * Sets the value of the cabinClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabinClass(String value) {
        this.cabinClass = value;
    }

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordLocator(String value) {
        this.recordLocator = value;
    }

    /**
     * Gets the value of the aubtCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAubtCode() {
        return aubtCode;
    }

    /**
     * Sets the value of the aubtCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAubtCode(String value) {
        this.aubtCode = value;
    }

}
