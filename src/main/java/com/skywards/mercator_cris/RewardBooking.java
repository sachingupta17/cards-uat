
package com.skywards.mercator_cris;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RewardBooking complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RewardBooking">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PersonID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="MemUID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="ActiveCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PmaID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="RewardCode" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PointsRedeemable" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="SystemCalculated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cancelled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AtRiskBalance" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="MemberMiles" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="TotalBookingMiles" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PmaType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Merged" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareBasis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RefundPoints" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="IsQuoteOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsDoubleUpgrade" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PaxDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InfantBehaviour" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OriginatorSystem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JourneyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RedeemedStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ApplicableRuleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RedemptionActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BookableActivityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumberOfUnits" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PmaDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="InitilizationDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="PaxName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FLNApplicable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecordLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PnrCreationDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="ProgresStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MilesRefundPercentage" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PriorityLevel" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="TicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FollowUpDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="ActivityProgressStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketOnDeparture" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Remarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="TelephoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DirectBookableActivity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="AdminCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgentCityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgentDutyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MilesBalance" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt64"/>
 *         &lt;element name="VoucherNumber" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="ActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RIHID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="ExternalAuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartnerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Agent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssignTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PAARCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketIssueDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="PnrName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RewardBooking", propOrder = {
    "personID",
    "memUID",
    "activeCardNumber",
    "pmaID",
    "rewardCode",
    "pointsRedeemable",
    "systemCalculated",
    "cancelled",
    "billedFlag",
    "voucherCode",
    "atRiskBalance",
    "memberMiles",
    "totalBookingMiles",
    "pmaType",
    "merged",
    "fareBasis",
    "refundPoints",
    "invoiceNumber",
    "invoiceDate",
    "isQuoteOnly",
    "isDoubleUpgrade",
    "paxDescription",
    "infantBehaviour",
    "originatorSystem",
    "journeyType",
    "redeemedStatus",
    "applicableRuleCode",
    "redemptionActivityType",
    "bookableActivityCode",
    "numberOfUnits",
    "pmaDate",
    "initilizationDate",
    "paxName",
    "flnApplicable",
    "recordLocator",
    "pnrCreationDate",
    "progresStatus",
    "milesRefundPercentage",
    "priorityLevel",
    "ticketNumber",
    "followUpDate",
    "activityProgressStatus",
    "ticketOnDeparture",
    "remarks",
    "addressID",
    "telephoneNumber",
    "contactName",
    "directBookableActivity",
    "adminCenterCode",
    "agentCityCode",
    "agentInitial",
    "agentDutyCode",
    "userID",
    "milesBalance",
    "voucherNumber",
    "activityType",
    "rihid",
    "externalAuthorizationCode",
    "partnerCode",
    "agent",
    "assignTo",
    "paarCode",
    "ticketIssueDate",
    "pnrName",
    "emailId"
})
public class RewardBooking {

    @XmlElement(name = "PersonID", required = true)
    protected QueryableInt32 personID;
    @XmlElement(name = "MemUID", required = true)
    protected QueryableInt32 memUID;
    @XmlElement(name = "ActiveCardNumber")
    protected String activeCardNumber;
    @XmlElement(name = "PmaID", required = true)
    protected QueryableInt32 pmaID;
    @XmlElement(name = "RewardCode", required = true)
    protected QueryableInt32 rewardCode;
    @XmlElement(name = "PointsRedeemable", required = true)
    protected QueryableInt32 pointsRedeemable;
    @XmlElement(name = "SystemCalculated")
    protected String systemCalculated;
    @XmlElement(name = "Cancelled")
    protected String cancelled;
    @XmlElement(name = "BilledFlag")
    protected String billedFlag;
    @XmlElement(name = "VoucherCode")
    protected String voucherCode;
    @XmlElement(name = "AtRiskBalance", required = true)
    protected QueryableInt32 atRiskBalance;
    @XmlElement(name = "MemberMiles", required = true)
    protected QueryableInt32 memberMiles;
    @XmlElement(name = "TotalBookingMiles", required = true)
    protected QueryableInt32 totalBookingMiles;
    @XmlElement(name = "PmaType")
    protected String pmaType;
    @XmlElement(name = "Merged")
    protected String merged;
    @XmlElement(name = "FareBasis")
    protected String fareBasis;
    @XmlElement(name = "RefundPoints", required = true)
    protected BigDecimal refundPoints;
    @XmlElement(name = "InvoiceNumber")
    protected String invoiceNumber;
    @XmlElement(name = "InvoiceDate", required = true)
    protected QueryableDateTime invoiceDate;
    @XmlElement(name = "IsQuoteOnly")
    protected boolean isQuoteOnly;
    @XmlElement(name = "IsDoubleUpgrade")
    protected boolean isDoubleUpgrade;
    @XmlElement(name = "PaxDescription")
    protected String paxDescription;
    @XmlElement(name = "InfantBehaviour")
    protected String infantBehaviour;
    @XmlElement(name = "OriginatorSystem")
    protected String originatorSystem;
    @XmlElement(name = "JourneyType")
    protected String journeyType;
    @XmlElement(name = "RedeemedStatus")
    protected boolean redeemedStatus;
    @XmlElement(name = "ApplicableRuleCode")
    protected String applicableRuleCode;
    @XmlElement(name = "RedemptionActivityType")
    protected String redemptionActivityType;
    @XmlElement(name = "BookableActivityCode")
    protected String bookableActivityCode;
    @XmlElement(name = "NumberOfUnits", required = true)
    protected QueryableInt32 numberOfUnits;
    @XmlElement(name = "PmaDate", required = true)
    protected QueryableDateTime pmaDate;
    @XmlElement(name = "InitilizationDate", required = true)
    protected QueryableDateTime initilizationDate;
    @XmlElement(name = "PaxName")
    protected String paxName;
    @XmlElement(name = "FLNApplicable")
    protected String flnApplicable;
    @XmlElement(name = "RecordLocator")
    protected String recordLocator;
    @XmlElement(name = "PnrCreationDate", required = true)
    protected QueryableDateTime pnrCreationDate;
    @XmlElement(name = "ProgresStatus")
    protected String progresStatus;
    @XmlElement(name = "MilesRefundPercentage", required = true)
    protected QueryableInt32 milesRefundPercentage;
    @XmlElement(name = "PriorityLevel", required = true)
    protected QueryableInt32 priorityLevel;
    @XmlElement(name = "TicketNumber")
    protected String ticketNumber;
    @XmlElement(name = "FollowUpDate", required = true)
    protected QueryableDateTime followUpDate;
    @XmlElement(name = "ActivityProgressStatus")
    protected String activityProgressStatus;
    @XmlElement(name = "TicketOnDeparture")
    protected String ticketOnDeparture;
    @XmlElement(name = "Remarks")
    protected String remarks;
    @XmlElement(name = "AddressID", required = true)
    protected QueryableInt32 addressID;
    @XmlElement(name = "TelephoneNumber")
    protected String telephoneNumber;
    @XmlElement(name = "ContactName")
    protected String contactName;
    @XmlElement(name = "DirectBookableActivity")
    protected boolean directBookableActivity;
    @XmlElement(name = "AdminCenterCode")
    protected String adminCenterCode;
    @XmlElement(name = "AgentCityCode")
    protected String agentCityCode;
    @XmlElement(name = "AgentInitial")
    protected String agentInitial;
    @XmlElement(name = "AgentDutyCode")
    protected String agentDutyCode;
    @XmlElement(name = "UserID")
    protected String userID;
    @XmlElement(name = "MilesBalance", required = true)
    protected QueryableInt64 milesBalance;
    @XmlElement(name = "VoucherNumber", required = true)
    protected QueryableInt32 voucherNumber;
    @XmlElement(name = "ActivityType")
    protected String activityType;
    @XmlElement(name = "RIHID", required = true)
    protected QueryableInt32 rihid;
    @XmlElement(name = "ExternalAuthorizationCode")
    protected String externalAuthorizationCode;
    @XmlElement(name = "PartnerCode")
    protected String partnerCode;
    @XmlElement(name = "Agent")
    protected String agent;
    @XmlElement(name = "AssignTo")
    protected String assignTo;
    @XmlElement(name = "PAARCode")
    protected String paarCode;
    @XmlElement(name = "TicketIssueDate", required = true)
    protected QueryableDateTime ticketIssueDate;
    @XmlElement(name = "PnrName")
    protected String pnrName;
    @XmlElement(name = "EmailId")
    protected String emailId;

    /**
     * Gets the value of the personID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPersonID(QueryableInt32 value) {
        this.personID = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setMemUID(QueryableInt32 value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the activeCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveCardNumber() {
        return activeCardNumber;
    }

    /**
     * Sets the value of the activeCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveCardNumber(String value) {
        this.activeCardNumber = value;
    }

    /**
     * Gets the value of the pmaID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPmaID() {
        return pmaID;
    }

    /**
     * Sets the value of the pmaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPmaID(QueryableInt32 value) {
        this.pmaID = value;
    }

    /**
     * Gets the value of the rewardCode property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getRewardCode() {
        return rewardCode;
    }

    /**
     * Sets the value of the rewardCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setRewardCode(QueryableInt32 value) {
        this.rewardCode = value;
    }

    /**
     * Gets the value of the pointsRedeemable property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPointsRedeemable() {
        return pointsRedeemable;
    }

    /**
     * Sets the value of the pointsRedeemable property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPointsRedeemable(QueryableInt32 value) {
        this.pointsRedeemable = value;
    }

    /**
     * Gets the value of the systemCalculated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemCalculated() {
        return systemCalculated;
    }

    /**
     * Sets the value of the systemCalculated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemCalculated(String value) {
        this.systemCalculated = value;
    }

    /**
     * Gets the value of the cancelled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelled() {
        return cancelled;
    }

    /**
     * Sets the value of the cancelled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelled(String value) {
        this.cancelled = value;
    }

    /**
     * Gets the value of the billedFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledFlag() {
        return billedFlag;
    }

    /**
     * Sets the value of the billedFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledFlag(String value) {
        this.billedFlag = value;
    }

    /**
     * Gets the value of the voucherCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     * Sets the value of the voucherCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherCode(String value) {
        this.voucherCode = value;
    }

    /**
     * Gets the value of the atRiskBalance property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getAtRiskBalance() {
        return atRiskBalance;
    }

    /**
     * Sets the value of the atRiskBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setAtRiskBalance(QueryableInt32 value) {
        this.atRiskBalance = value;
    }

    /**
     * Gets the value of the memberMiles property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getMemberMiles() {
        return memberMiles;
    }

    /**
     * Sets the value of the memberMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setMemberMiles(QueryableInt32 value) {
        this.memberMiles = value;
    }

    /**
     * Gets the value of the totalBookingMiles property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getTotalBookingMiles() {
        return totalBookingMiles;
    }

    /**
     * Sets the value of the totalBookingMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setTotalBookingMiles(QueryableInt32 value) {
        this.totalBookingMiles = value;
    }

    /**
     * Gets the value of the pmaType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPmaType() {
        return pmaType;
    }

    /**
     * Sets the value of the pmaType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPmaType(String value) {
        this.pmaType = value;
    }

    /**
     * Gets the value of the merged property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerged() {
        return merged;
    }

    /**
     * Sets the value of the merged property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerged(String value) {
        this.merged = value;
    }

    /**
     * Gets the value of the fareBasis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasis() {
        return fareBasis;
    }

    /**
     * Sets the value of the fareBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasis(String value) {
        this.fareBasis = value;
    }

    /**
     * Gets the value of the refundPoints property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRefundPoints() {
        return refundPoints;
    }

    /**
     * Sets the value of the refundPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRefundPoints(BigDecimal value) {
        this.refundPoints = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNumber(String value) {
        this.invoiceNumber = value;
    }

    /**
     * Gets the value of the invoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Sets the value of the invoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setInvoiceDate(QueryableDateTime value) {
        this.invoiceDate = value;
    }

    /**
     * Gets the value of the isQuoteOnly property.
     * 
     */
    public boolean isIsQuoteOnly() {
        return isQuoteOnly;
    }

    /**
     * Sets the value of the isQuoteOnly property.
     * 
     */
    public void setIsQuoteOnly(boolean value) {
        this.isQuoteOnly = value;
    }

    /**
     * Gets the value of the isDoubleUpgrade property.
     * 
     */
    public boolean isIsDoubleUpgrade() {
        return isDoubleUpgrade;
    }

    /**
     * Sets the value of the isDoubleUpgrade property.
     * 
     */
    public void setIsDoubleUpgrade(boolean value) {
        this.isDoubleUpgrade = value;
    }

    /**
     * Gets the value of the paxDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaxDescription() {
        return paxDescription;
    }

    /**
     * Sets the value of the paxDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaxDescription(String value) {
        this.paxDescription = value;
    }

    /**
     * Gets the value of the infantBehaviour property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfantBehaviour() {
        return infantBehaviour;
    }

    /**
     * Sets the value of the infantBehaviour property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfantBehaviour(String value) {
        this.infantBehaviour = value;
    }

    /**
     * Gets the value of the originatorSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatorSystem() {
        return originatorSystem;
    }

    /**
     * Sets the value of the originatorSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatorSystem(String value) {
        this.originatorSystem = value;
    }

    /**
     * Gets the value of the journeyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJourneyType() {
        return journeyType;
    }

    /**
     * Sets the value of the journeyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJourneyType(String value) {
        this.journeyType = value;
    }

    /**
     * Gets the value of the redeemedStatus property.
     * 
     */
    public boolean isRedeemedStatus() {
        return redeemedStatus;
    }

    /**
     * Sets the value of the redeemedStatus property.
     * 
     */
    public void setRedeemedStatus(boolean value) {
        this.redeemedStatus = value;
    }

    /**
     * Gets the value of the applicableRuleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicableRuleCode() {
        return applicableRuleCode;
    }

    /**
     * Sets the value of the applicableRuleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicableRuleCode(String value) {
        this.applicableRuleCode = value;
    }

    /**
     * Gets the value of the redemptionActivityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedemptionActivityType() {
        return redemptionActivityType;
    }

    /**
     * Sets the value of the redemptionActivityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedemptionActivityType(String value) {
        this.redemptionActivityType = value;
    }

    /**
     * Gets the value of the bookableActivityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookableActivityCode() {
        return bookableActivityCode;
    }

    /**
     * Sets the value of the bookableActivityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookableActivityCode(String value) {
        this.bookableActivityCode = value;
    }

    /**
     * Gets the value of the numberOfUnits property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getNumberOfUnits() {
        return numberOfUnits;
    }

    /**
     * Sets the value of the numberOfUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setNumberOfUnits(QueryableInt32 value) {
        this.numberOfUnits = value;
    }

    /**
     * Gets the value of the pmaDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getPmaDate() {
        return pmaDate;
    }

    /**
     * Sets the value of the pmaDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setPmaDate(QueryableDateTime value) {
        this.pmaDate = value;
    }

    /**
     * Gets the value of the initilizationDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getInitilizationDate() {
        return initilizationDate;
    }

    /**
     * Sets the value of the initilizationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setInitilizationDate(QueryableDateTime value) {
        this.initilizationDate = value;
    }

    /**
     * Gets the value of the paxName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaxName() {
        return paxName;
    }

    /**
     * Sets the value of the paxName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaxName(String value) {
        this.paxName = value;
    }

    /**
     * Gets the value of the flnApplicable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLNApplicable() {
        return flnApplicable;
    }

    /**
     * Sets the value of the flnApplicable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLNApplicable(String value) {
        this.flnApplicable = value;
    }

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordLocator(String value) {
        this.recordLocator = value;
    }

    /**
     * Gets the value of the pnrCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getPnrCreationDate() {
        return pnrCreationDate;
    }

    /**
     * Sets the value of the pnrCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setPnrCreationDate(QueryableDateTime value) {
        this.pnrCreationDate = value;
    }

    /**
     * Gets the value of the progresStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgresStatus() {
        return progresStatus;
    }

    /**
     * Sets the value of the progresStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgresStatus(String value) {
        this.progresStatus = value;
    }

    /**
     * Gets the value of the milesRefundPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getMilesRefundPercentage() {
        return milesRefundPercentage;
    }

    /**
     * Sets the value of the milesRefundPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setMilesRefundPercentage(QueryableInt32 value) {
        this.milesRefundPercentage = value;
    }

    /**
     * Gets the value of the priorityLevel property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Sets the value of the priorityLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPriorityLevel(QueryableInt32 value) {
        this.priorityLevel = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Sets the value of the ticketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Gets the value of the followUpDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getFollowUpDate() {
        return followUpDate;
    }

    /**
     * Sets the value of the followUpDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setFollowUpDate(QueryableDateTime value) {
        this.followUpDate = value;
    }

    /**
     * Gets the value of the activityProgressStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityProgressStatus() {
        return activityProgressStatus;
    }

    /**
     * Sets the value of the activityProgressStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityProgressStatus(String value) {
        this.activityProgressStatus = value;
    }

    /**
     * Gets the value of the ticketOnDeparture property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketOnDeparture() {
        return ticketOnDeparture;
    }

    /**
     * Sets the value of the ticketOnDeparture property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketOnDeparture(String value) {
        this.ticketOnDeparture = value;
    }

    /**
     * Gets the value of the remarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * Sets the value of the remarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarks(String value) {
        this.remarks = value;
    }

    /**
     * Gets the value of the addressID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getAddressID() {
        return addressID;
    }

    /**
     * Sets the value of the addressID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setAddressID(QueryableInt32 value) {
        this.addressID = value;
    }

    /**
     * Gets the value of the telephoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * Sets the value of the telephoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelephoneNumber(String value) {
        this.telephoneNumber = value;
    }

    /**
     * Gets the value of the contactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * Sets the value of the contactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactName(String value) {
        this.contactName = value;
    }

    /**
     * Gets the value of the directBookableActivity property.
     * 
     */
    public boolean isDirectBookableActivity() {
        return directBookableActivity;
    }

    /**
     * Sets the value of the directBookableActivity property.
     * 
     */
    public void setDirectBookableActivity(boolean value) {
        this.directBookableActivity = value;
    }

    /**
     * Gets the value of the adminCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdminCenterCode() {
        return adminCenterCode;
    }

    /**
     * Sets the value of the adminCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdminCenterCode(String value) {
        this.adminCenterCode = value;
    }

    /**
     * Gets the value of the agentCityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentCityCode() {
        return agentCityCode;
    }

    /**
     * Sets the value of the agentCityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentCityCode(String value) {
        this.agentCityCode = value;
    }

    /**
     * Gets the value of the agentInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentInitial() {
        return agentInitial;
    }

    /**
     * Sets the value of the agentInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentInitial(String value) {
        this.agentInitial = value;
    }

    /**
     * Gets the value of the agentDutyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentDutyCode() {
        return agentDutyCode;
    }

    /**
     * Sets the value of the agentDutyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentDutyCode(String value) {
        this.agentDutyCode = value;
    }

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the milesBalance property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt64 }
     *     
     */
    public QueryableInt64 getMilesBalance() {
        return milesBalance;
    }

    /**
     * Sets the value of the milesBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt64 }
     *     
     */
    public void setMilesBalance(QueryableInt64 value) {
        this.milesBalance = value;
    }

    /**
     * Gets the value of the voucherNumber property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getVoucherNumber() {
        return voucherNumber;
    }

    /**
     * Sets the value of the voucherNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setVoucherNumber(QueryableInt32 value) {
        this.voucherNumber = value;
    }

    /**
     * Gets the value of the activityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * Sets the value of the activityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityType(String value) {
        this.activityType = value;
    }

    /**
     * Gets the value of the rihid property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getRIHID() {
        return rihid;
    }

    /**
     * Sets the value of the rihid property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setRIHID(QueryableInt32 value) {
        this.rihid = value;
    }

    /**
     * Gets the value of the externalAuthorizationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalAuthorizationCode() {
        return externalAuthorizationCode;
    }

    /**
     * Sets the value of the externalAuthorizationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalAuthorizationCode(String value) {
        this.externalAuthorizationCode = value;
    }

    /**
     * Gets the value of the partnerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerCode() {
        return partnerCode;
    }

    /**
     * Sets the value of the partnerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerCode(String value) {
        this.partnerCode = value;
    }

    /**
     * Gets the value of the agent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgent() {
        return agent;
    }

    /**
     * Sets the value of the agent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgent(String value) {
        this.agent = value;
    }

    /**
     * Gets the value of the assignTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignTo() {
        return assignTo;
    }

    /**
     * Sets the value of the assignTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignTo(String value) {
        this.assignTo = value;
    }

    /**
     * Gets the value of the paarCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAARCode() {
        return paarCode;
    }

    /**
     * Sets the value of the paarCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAARCode(String value) {
        this.paarCode = value;
    }

    /**
     * Gets the value of the ticketIssueDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getTicketIssueDate() {
        return ticketIssueDate;
    }

    /**
     * Sets the value of the ticketIssueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setTicketIssueDate(QueryableDateTime value) {
        this.ticketIssueDate = value;
    }

    /**
     * Gets the value of the pnrName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnrName() {
        return pnrName;
    }

    /**
     * Sets the value of the pnrName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnrName(String value) {
        this.pnrName = value;
    }

    /**
     * Gets the value of the emailId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Sets the value of the emailId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailId(String value) {
        this.emailId = value;
    }

}
