
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfStatusEnhancement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfStatusEnhancement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusEnhancement" type="{http://skywards.com/Mercator.CRIS.WS}StatusEnhancement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfStatusEnhancement", propOrder = {
    "statusEnhancement"
})
public class ArrayOfStatusEnhancement {

    @XmlElement(name = "StatusEnhancement", nillable = true)
    protected List<StatusEnhancement> statusEnhancement;

    /**
     * Gets the value of the statusEnhancement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the statusEnhancement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatusEnhancement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusEnhancement }
     * 
     * 
     */
    public List<StatusEnhancement> getStatusEnhancement() {
        if (statusEnhancement == null) {
            statusEnhancement = new ArrayList<StatusEnhancement>();
        }
        return this.statusEnhancement;
    }

}
