
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WebTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WebTransaction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}CRISReadOnlyBase">
 *       &lt;sequence>
 *         &lt;element name="TransactionForWebIDFamily" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MemUID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityCodeFamily" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityTypeFamily" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransTypeFamily" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartnerDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionForWebDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TransactionForWebDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RedemptionMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TierMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TransactionPmaId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FamilyTransactionPmaId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ActivityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionForWebID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FamilyTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="PmaId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TransactionForWebCouponID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CouponDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PmaIdFamily" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TransactionForWebCouponIDFamily" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CouponDetailsFamily" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CategoryTypeMem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CategoryTypeFam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemTransDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="FamTransDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WebTransaction", propOrder = {
    "transactionForWebIDFamily",
    "memUID",
    "activityType",
    "transType",
    "activityCodeFamily",
    "activityTypeFamily",
    "transTypeFamily",
    "partnerDescr",
    "transactionForWebDate",
    "transactionForWebDescr",
    "redemptionMiles",
    "tierMiles",
    "transactionPmaId",
    "familyTransactionPmaId",
    "activityCode",
    "transactionForWebID",
    "ticketNumber",
    "familyTicketNumber",
    "statementDate",
    "pmaId",
    "transactionForWebCouponID",
    "couponDetails",
    "pmaIdFamily",
    "transactionForWebCouponIDFamily",
    "couponDetailsFamily",
    "categoryTypeMem",
    "categoryTypeFam",
    "memTransDate",
    "famTransDate"
})
public class WebTransaction
    extends CRISReadOnlyBase
{

    @XmlElement(name = "TransactionForWebIDFamily")
    protected int transactionForWebIDFamily;
    @XmlElement(name = "MemUID")
    protected int memUID;
    @XmlElement(name = "ActivityType")
    protected String activityType;
    @XmlElement(name = "TransType")
    protected String transType;
    @XmlElement(name = "ActivityCodeFamily")
    protected String activityCodeFamily;
    @XmlElement(name = "ActivityTypeFamily")
    protected String activityTypeFamily;
    @XmlElement(name = "TransTypeFamily")
    protected String transTypeFamily;
    @XmlElement(name = "PartnerDescr")
    protected String partnerDescr;
    @XmlElement(name = "TransactionForWebDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transactionForWebDate;
    @XmlElement(name = "TransactionForWebDescr")
    protected String transactionForWebDescr;
    @XmlElement(name = "RedemptionMiles")
    protected int redemptionMiles;
    @XmlElement(name = "TierMiles")
    protected int tierMiles;
    @XmlElement(name = "TransactionPmaId")
    protected int transactionPmaId;
    @XmlElement(name = "FamilyTransactionPmaId")
    protected int familyTransactionPmaId;
    @XmlElement(name = "ActivityCode")
    protected String activityCode;
    @XmlElement(name = "TransactionForWebID")
    protected int transactionForWebID;
    @XmlElement(name = "TicketNumber")
    protected String ticketNumber;
    @XmlElement(name = "FamilyTicketNumber")
    protected String familyTicketNumber;
    @XmlElement(name = "StatementDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statementDate;
    @XmlElement(name = "PmaId")
    protected int pmaId;
    @XmlElement(name = "TransactionForWebCouponID")
    protected int transactionForWebCouponID;
    @XmlElement(name = "CouponDetails")
    protected String couponDetails;
    @XmlElement(name = "PmaIdFamily")
    protected int pmaIdFamily;
    @XmlElement(name = "TransactionForWebCouponIDFamily")
    protected int transactionForWebCouponIDFamily;
    @XmlElement(name = "CouponDetailsFamily")
    protected String couponDetailsFamily;
    @XmlElement(name = "CategoryTypeMem")
    protected String categoryTypeMem;
    @XmlElement(name = "CategoryTypeFam")
    protected String categoryTypeFam;
    @XmlElement(name = "MemTransDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar memTransDate;
    @XmlElement(name = "FamTransDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar famTransDate;

    /**
     * Gets the value of the transactionForWebIDFamily property.
     * 
     */
    public int getTransactionForWebIDFamily() {
        return transactionForWebIDFamily;
    }

    /**
     * Sets the value of the transactionForWebIDFamily property.
     * 
     */
    public void setTransactionForWebIDFamily(int value) {
        this.transactionForWebIDFamily = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     */
    public int getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     */
    public void setMemUID(int value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the activityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * Sets the value of the activityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityType(String value) {
        this.activityType = value;
    }

    /**
     * Gets the value of the transType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransType() {
        return transType;
    }

    /**
     * Sets the value of the transType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransType(String value) {
        this.transType = value;
    }

    /**
     * Gets the value of the activityCodeFamily property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityCodeFamily() {
        return activityCodeFamily;
    }

    /**
     * Sets the value of the activityCodeFamily property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityCodeFamily(String value) {
        this.activityCodeFamily = value;
    }

    /**
     * Gets the value of the activityTypeFamily property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityTypeFamily() {
        return activityTypeFamily;
    }

    /**
     * Sets the value of the activityTypeFamily property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityTypeFamily(String value) {
        this.activityTypeFamily = value;
    }

    /**
     * Gets the value of the transTypeFamily property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransTypeFamily() {
        return transTypeFamily;
    }

    /**
     * Sets the value of the transTypeFamily property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransTypeFamily(String value) {
        this.transTypeFamily = value;
    }

    /**
     * Gets the value of the partnerDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerDescr() {
        return partnerDescr;
    }

    /**
     * Sets the value of the partnerDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerDescr(String value) {
        this.partnerDescr = value;
    }

    /**
     * Gets the value of the transactionForWebDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransactionForWebDate() {
        return transactionForWebDate;
    }

    /**
     * Sets the value of the transactionForWebDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransactionForWebDate(XMLGregorianCalendar value) {
        this.transactionForWebDate = value;
    }

    /**
     * Gets the value of the transactionForWebDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionForWebDescr() {
        return transactionForWebDescr;
    }

    /**
     * Sets the value of the transactionForWebDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionForWebDescr(String value) {
        this.transactionForWebDescr = value;
    }

    /**
     * Gets the value of the redemptionMiles property.
     * 
     */
    public int getRedemptionMiles() {
        return redemptionMiles;
    }

    /**
     * Sets the value of the redemptionMiles property.
     * 
     */
    public void setRedemptionMiles(int value) {
        this.redemptionMiles = value;
    }

    /**
     * Gets the value of the tierMiles property.
     * 
     */
    public int getTierMiles() {
        return tierMiles;
    }

    /**
     * Sets the value of the tierMiles property.
     * 
     */
    public void setTierMiles(int value) {
        this.tierMiles = value;
    }

    /**
     * Gets the value of the transactionPmaId property.
     * 
     */
    public int getTransactionPmaId() {
        return transactionPmaId;
    }

    /**
     * Sets the value of the transactionPmaId property.
     * 
     */
    public void setTransactionPmaId(int value) {
        this.transactionPmaId = value;
    }

    /**
     * Gets the value of the familyTransactionPmaId property.
     * 
     */
    public int getFamilyTransactionPmaId() {
        return familyTransactionPmaId;
    }

    /**
     * Sets the value of the familyTransactionPmaId property.
     * 
     */
    public void setFamilyTransactionPmaId(int value) {
        this.familyTransactionPmaId = value;
    }

    /**
     * Gets the value of the activityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityCode() {
        return activityCode;
    }

    /**
     * Sets the value of the activityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityCode(String value) {
        this.activityCode = value;
    }

    /**
     * Gets the value of the transactionForWebID property.
     * 
     */
    public int getTransactionForWebID() {
        return transactionForWebID;
    }

    /**
     * Sets the value of the transactionForWebID property.
     * 
     */
    public void setTransactionForWebID(int value) {
        this.transactionForWebID = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Sets the value of the ticketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Gets the value of the familyTicketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyTicketNumber() {
        return familyTicketNumber;
    }

    /**
     * Sets the value of the familyTicketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyTicketNumber(String value) {
        this.familyTicketNumber = value;
    }

    /**
     * Gets the value of the statementDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatementDate() {
        return statementDate;
    }

    /**
     * Sets the value of the statementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatementDate(XMLGregorianCalendar value) {
        this.statementDate = value;
    }

    /**
     * Gets the value of the pmaId property.
     * 
     */
    public int getPmaId() {
        return pmaId;
    }

    /**
     * Sets the value of the pmaId property.
     * 
     */
    public void setPmaId(int value) {
        this.pmaId = value;
    }

    /**
     * Gets the value of the transactionForWebCouponID property.
     * 
     */
    public int getTransactionForWebCouponID() {
        return transactionForWebCouponID;
    }

    /**
     * Sets the value of the transactionForWebCouponID property.
     * 
     */
    public void setTransactionForWebCouponID(int value) {
        this.transactionForWebCouponID = value;
    }

    /**
     * Gets the value of the couponDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponDetails() {
        return couponDetails;
    }

    /**
     * Sets the value of the couponDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponDetails(String value) {
        this.couponDetails = value;
    }

    /**
     * Gets the value of the pmaIdFamily property.
     * 
     */
    public int getPmaIdFamily() {
        return pmaIdFamily;
    }

    /**
     * Sets the value of the pmaIdFamily property.
     * 
     */
    public void setPmaIdFamily(int value) {
        this.pmaIdFamily = value;
    }

    /**
     * Gets the value of the transactionForWebCouponIDFamily property.
     * 
     */
    public int getTransactionForWebCouponIDFamily() {
        return transactionForWebCouponIDFamily;
    }

    /**
     * Sets the value of the transactionForWebCouponIDFamily property.
     * 
     */
    public void setTransactionForWebCouponIDFamily(int value) {
        this.transactionForWebCouponIDFamily = value;
    }

    /**
     * Gets the value of the couponDetailsFamily property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponDetailsFamily() {
        return couponDetailsFamily;
    }

    /**
     * Sets the value of the couponDetailsFamily property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponDetailsFamily(String value) {
        this.couponDetailsFamily = value;
    }

    /**
     * Gets the value of the categoryTypeMem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryTypeMem() {
        return categoryTypeMem;
    }

    /**
     * Sets the value of the categoryTypeMem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryTypeMem(String value) {
        this.categoryTypeMem = value;
    }

    /**
     * Gets the value of the categoryTypeFam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryTypeFam() {
        return categoryTypeFam;
    }

    /**
     * Sets the value of the categoryTypeFam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryTypeFam(String value) {
        this.categoryTypeFam = value;
    }

    /**
     * Gets the value of the memTransDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMemTransDate() {
        return memTransDate;
    }

    /**
     * Sets the value of the memTransDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMemTransDate(XMLGregorianCalendar value) {
        this.memTransDate = value;
    }

    /**
     * Gets the value of the famTransDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFamTransDate() {
        return famTransDate;
    }

    /**
     * Sets the value of the famTransDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFamTransDate(XMLGregorianCalendar value) {
        this.famTransDate = value;
    }

}
