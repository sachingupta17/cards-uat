
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfMileageCalculator complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfMileageCalculator">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MileageCalculator" type="{http://skywards.com/Mercator.CRIS.WS}MileageCalculator" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfMileageCalculator", propOrder = {
    "mileageCalculator"
})
public class ArrayOfMileageCalculator {

    @XmlElement(name = "MileageCalculator", nillable = true)
    protected List<MileageCalculator> mileageCalculator;

    /**
     * Gets the value of the mileageCalculator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mileageCalculator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMileageCalculator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MileageCalculator }
     * 
     * 
     */
    public List<MileageCalculator> getMileageCalculator() {
        if (mileageCalculator == null) {
            mileageCalculator = new ArrayList<MileageCalculator>();
        }
        return this.mileageCalculator;
    }

}
