
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ItineryChaufferDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItineryChaufferDetail">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}CRISBusinessBaseOfItineryChaufferDetail">
 *       &lt;sequence>
 *         &lt;element name="VariableData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineryChaufferDetail", propOrder = {
    "variableData"
})
public class ItineryChaufferDetail
    extends CRISBusinessBaseOfItineryChaufferDetail
{

    @XmlElement(name = "VariableData")
    protected String variableData;

    /**
     * Gets the value of the variableData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVariableData() {
        return variableData;
    }

    /**
     * Sets the value of the variableData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVariableData(String value) {
        this.variableData = value;
    }

}
