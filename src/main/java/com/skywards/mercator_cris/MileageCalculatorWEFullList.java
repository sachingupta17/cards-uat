
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MileageCalculatorWEFull_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MileageCalculatorWEFull_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TierType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JourneyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CabinClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MileageCalcMain" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfMileageCalculatorWE" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MileageCalculatorWEFull_List", propOrder = {
    "tierType",
    "journeyType",
    "cabinClass",
    "mileageCalcMain"
})
public class MileageCalculatorWEFullList {

    @XmlElement(name = "TierType")
    protected String tierType;
    @XmlElement(name = "JourneyType")
    protected String journeyType;
    @XmlElement(name = "CabinClass")
    protected String cabinClass;
    @XmlElement(name = "MileageCalcMain")
    protected ArrayOfMileageCalculatorWE mileageCalcMain;

    /**
     * Gets the value of the tierType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierType() {
        return tierType;
    }

    /**
     * Sets the value of the tierType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierType(String value) {
        this.tierType = value;
    }

    /**
     * Gets the value of the journeyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJourneyType() {
        return journeyType;
    }

    /**
     * Sets the value of the journeyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJourneyType(String value) {
        this.journeyType = value;
    }

    /**
     * Gets the value of the cabinClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabinClass() {
        return cabinClass;
    }

    /**
     * Sets the value of the cabinClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabinClass(String value) {
        this.cabinClass = value;
    }

    /**
     * Gets the value of the mileageCalcMain property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMileageCalculatorWE }
     *     
     */
    public ArrayOfMileageCalculatorWE getMileageCalcMain() {
        return mileageCalcMain;
    }

    /**
     * Sets the value of the mileageCalcMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMileageCalculatorWE }
     *     
     */
    public void setMileageCalcMain(ArrayOfMileageCalculatorWE value) {
        this.mileageCalcMain = value;
    }

}
