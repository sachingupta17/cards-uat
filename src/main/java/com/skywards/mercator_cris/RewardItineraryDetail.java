
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RewardItineraryDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RewardItineraryDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PersonID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SeqNumberPK" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RIHID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ItineraryLegSequence" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AirlineDesignator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Origin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Destination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlownClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BookingClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BookingStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DayChangeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntryType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StopOver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Active" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaseMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BaseClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiffMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ZoneType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OriginZone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OriginZoneSequence" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DestinationZone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DestinationZoneSequence" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AircraftType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Differentiator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClassSequence" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RedemptionMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ActivityID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SSRUpgradeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Dropped" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Class" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CalcBaseMiles" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="TmpDiffMiles" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="NoteDetail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SeasonID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BacActUID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="Billed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RewardItineraryDetail", propOrder = {
    "personID",
    "seqNumberPK",
    "rihid",
    "itineraryLegSequence",
    "airlineDesignator",
    "flightNumber",
    "flightDate",
    "origin",
    "destination",
    "departureTime",
    "arrivalTime",
    "flownClass",
    "bookingClass",
    "bookingStatus",
    "dayChangeIndicator",
    "entryType",
    "stopOver",
    "active",
    "baseMiles",
    "baseClass",
    "diffMiles",
    "zoneType",
    "originZone",
    "originZoneSequence",
    "destinationZone",
    "destinationZoneSequence",
    "aircraftType",
    "differentiator",
    "classSequence",
    "redemptionMiles",
    "activityID",
    "ssrUpgradeIndicator",
    "dropped",
    "authorizationCode",
    "clazz",
    "calcBaseMiles",
    "tmpDiffMiles",
    "noteDetail",
    "seasonID",
    "bacActUID",
    "billed"
})
public class RewardItineraryDetail {

    @XmlElement(name = "PersonID")
    protected int personID;
    @XmlElement(name = "SeqNumberPK")
    protected int seqNumberPK;
    @XmlElement(name = "RIHID")
    protected int rihid;
    @XmlElement(name = "ItineraryLegSequence")
    protected int itineraryLegSequence;
    @XmlElement(name = "AirlineDesignator")
    protected String airlineDesignator;
    @XmlElement(name = "FlightNumber")
    protected String flightNumber;
    @XmlElement(name = "FlightDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar flightDate;
    @XmlElement(name = "Origin")
    protected String origin;
    @XmlElement(name = "Destination")
    protected String destination;
    @XmlElement(name = "DepartureTime")
    protected String departureTime;
    @XmlElement(name = "ArrivalTime")
    protected String arrivalTime;
    @XmlElement(name = "FlownClass")
    protected String flownClass;
    @XmlElement(name = "BookingClass")
    protected String bookingClass;
    @XmlElement(name = "BookingStatus")
    protected String bookingStatus;
    @XmlElement(name = "DayChangeIndicator")
    protected String dayChangeIndicator;
    @XmlElement(name = "EntryType")
    protected String entryType;
    @XmlElement(name = "StopOver")
    protected String stopOver;
    @XmlElement(name = "Active")
    protected String active;
    @XmlElement(name = "BaseMiles")
    protected int baseMiles;
    @XmlElement(name = "BaseClass")
    protected String baseClass;
    @XmlElement(name = "DiffMiles")
    protected int diffMiles;
    @XmlElement(name = "ZoneType")
    protected String zoneType;
    @XmlElement(name = "OriginZone")
    protected String originZone;
    @XmlElement(name = "OriginZoneSequence")
    protected int originZoneSequence;
    @XmlElement(name = "DestinationZone")
    protected String destinationZone;
    @XmlElement(name = "DestinationZoneSequence")
    protected int destinationZoneSequence;
    @XmlElement(name = "AircraftType")
    protected String aircraftType;
    @XmlElement(name = "Differentiator")
    protected String differentiator;
    @XmlElement(name = "ClassSequence")
    protected int classSequence;
    @XmlElement(name = "RedemptionMiles")
    protected int redemptionMiles;
    @XmlElement(name = "ActivityID")
    protected int activityID;
    @XmlElement(name = "SSRUpgradeIndicator")
    protected String ssrUpgradeIndicator;
    @XmlElement(name = "Dropped")
    protected String dropped;
    @XmlElement(name = "AuthorizationCode")
    protected String authorizationCode;
    @XmlElement(name = "Class")
    protected String clazz;
    @XmlElement(name = "CalcBaseMiles", required = true)
    protected QueryableInt32 calcBaseMiles;
    @XmlElement(name = "TmpDiffMiles", required = true)
    protected QueryableInt32 tmpDiffMiles;
    @XmlElement(name = "NoteDetail")
    protected String noteDetail;
    @XmlElement(name = "SeasonID")
    protected String seasonID;
    @XmlElement(name = "BacActUID", required = true)
    protected QueryableInt32 bacActUID;
    @XmlElement(name = "Billed")
    protected String billed;

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the seqNumberPK property.
     * 
     */
    public int getSeqNumberPK() {
        return seqNumberPK;
    }

    /**
     * Sets the value of the seqNumberPK property.
     * 
     */
    public void setSeqNumberPK(int value) {
        this.seqNumberPK = value;
    }

    /**
     * Gets the value of the rihid property.
     * 
     */
    public int getRIHID() {
        return rihid;
    }

    /**
     * Sets the value of the rihid property.
     * 
     */
    public void setRIHID(int value) {
        this.rihid = value;
    }

    /**
     * Gets the value of the itineraryLegSequence property.
     * 
     */
    public int getItineraryLegSequence() {
        return itineraryLegSequence;
    }

    /**
     * Sets the value of the itineraryLegSequence property.
     * 
     */
    public void setItineraryLegSequence(int value) {
        this.itineraryLegSequence = value;
    }

    /**
     * Gets the value of the airlineDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineDesignator() {
        return airlineDesignator;
    }

    /**
     * Sets the value of the airlineDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineDesignator(String value) {
        this.airlineDesignator = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the flightDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFlightDate() {
        return flightDate;
    }

    /**
     * Sets the value of the flightDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFlightDate(XMLGregorianCalendar value) {
        this.flightDate = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Gets the value of the destination property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureTime(String value) {
        this.departureTime = value;
    }

    /**
     * Gets the value of the arrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets the value of the arrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalTime(String value) {
        this.arrivalTime = value;
    }

    /**
     * Gets the value of the flownClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlownClass() {
        return flownClass;
    }

    /**
     * Sets the value of the flownClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlownClass(String value) {
        this.flownClass = value;
    }

    /**
     * Gets the value of the bookingClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingClass() {
        return bookingClass;
    }

    /**
     * Sets the value of the bookingClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingClass(String value) {
        this.bookingClass = value;
    }

    /**
     * Gets the value of the bookingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * Sets the value of the bookingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingStatus(String value) {
        this.bookingStatus = value;
    }

    /**
     * Gets the value of the dayChangeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDayChangeIndicator() {
        return dayChangeIndicator;
    }

    /**
     * Sets the value of the dayChangeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDayChangeIndicator(String value) {
        this.dayChangeIndicator = value;
    }

    /**
     * Gets the value of the entryType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntryType() {
        return entryType;
    }

    /**
     * Sets the value of the entryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntryType(String value) {
        this.entryType = value;
    }

    /**
     * Gets the value of the stopOver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopOver() {
        return stopOver;
    }

    /**
     * Sets the value of the stopOver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopOver(String value) {
        this.stopOver = value;
    }

    /**
     * Gets the value of the active property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActive(String value) {
        this.active = value;
    }

    /**
     * Gets the value of the baseMiles property.
     * 
     */
    public int getBaseMiles() {
        return baseMiles;
    }

    /**
     * Sets the value of the baseMiles property.
     * 
     */
    public void setBaseMiles(int value) {
        this.baseMiles = value;
    }

    /**
     * Gets the value of the baseClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseClass() {
        return baseClass;
    }

    /**
     * Sets the value of the baseClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseClass(String value) {
        this.baseClass = value;
    }

    /**
     * Gets the value of the diffMiles property.
     * 
     */
    public int getDiffMiles() {
        return diffMiles;
    }

    /**
     * Sets the value of the diffMiles property.
     * 
     */
    public void setDiffMiles(int value) {
        this.diffMiles = value;
    }

    /**
     * Gets the value of the zoneType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZoneType() {
        return zoneType;
    }

    /**
     * Sets the value of the zoneType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZoneType(String value) {
        this.zoneType = value;
    }

    /**
     * Gets the value of the originZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginZone() {
        return originZone;
    }

    /**
     * Sets the value of the originZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginZone(String value) {
        this.originZone = value;
    }

    /**
     * Gets the value of the originZoneSequence property.
     * 
     */
    public int getOriginZoneSequence() {
        return originZoneSequence;
    }

    /**
     * Sets the value of the originZoneSequence property.
     * 
     */
    public void setOriginZoneSequence(int value) {
        this.originZoneSequence = value;
    }

    /**
     * Gets the value of the destinationZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationZone() {
        return destinationZone;
    }

    /**
     * Sets the value of the destinationZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationZone(String value) {
        this.destinationZone = value;
    }

    /**
     * Gets the value of the destinationZoneSequence property.
     * 
     */
    public int getDestinationZoneSequence() {
        return destinationZoneSequence;
    }

    /**
     * Sets the value of the destinationZoneSequence property.
     * 
     */
    public void setDestinationZoneSequence(int value) {
        this.destinationZoneSequence = value;
    }

    /**
     * Gets the value of the aircraftType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAircraftType() {
        return aircraftType;
    }

    /**
     * Sets the value of the aircraftType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAircraftType(String value) {
        this.aircraftType = value;
    }

    /**
     * Gets the value of the differentiator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDifferentiator() {
        return differentiator;
    }

    /**
     * Sets the value of the differentiator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDifferentiator(String value) {
        this.differentiator = value;
    }

    /**
     * Gets the value of the classSequence property.
     * 
     */
    public int getClassSequence() {
        return classSequence;
    }

    /**
     * Sets the value of the classSequence property.
     * 
     */
    public void setClassSequence(int value) {
        this.classSequence = value;
    }

    /**
     * Gets the value of the redemptionMiles property.
     * 
     */
    public int getRedemptionMiles() {
        return redemptionMiles;
    }

    /**
     * Sets the value of the redemptionMiles property.
     * 
     */
    public void setRedemptionMiles(int value) {
        this.redemptionMiles = value;
    }

    /**
     * Gets the value of the activityID property.
     * 
     */
    public int getActivityID() {
        return activityID;
    }

    /**
     * Sets the value of the activityID property.
     * 
     */
    public void setActivityID(int value) {
        this.activityID = value;
    }

    /**
     * Gets the value of the ssrUpgradeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSRUpgradeIndicator() {
        return ssrUpgradeIndicator;
    }

    /**
     * Sets the value of the ssrUpgradeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSRUpgradeIndicator(String value) {
        this.ssrUpgradeIndicator = value;
    }

    /**
     * Gets the value of the dropped property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDropped() {
        return dropped;
    }

    /**
     * Sets the value of the dropped property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDropped(String value) {
        this.dropped = value;
    }

    /**
     * Gets the value of the authorizationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationCode() {
        return authorizationCode;
    }

    /**
     * Sets the value of the authorizationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationCode(String value) {
        this.authorizationCode = value;
    }

    /**
     * Gets the value of the clazz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Sets the value of the clazz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClazz(String value) {
        this.clazz = value;
    }

    /**
     * Gets the value of the calcBaseMiles property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getCalcBaseMiles() {
        return calcBaseMiles;
    }

    /**
     * Sets the value of the calcBaseMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setCalcBaseMiles(QueryableInt32 value) {
        this.calcBaseMiles = value;
    }

    /**
     * Gets the value of the tmpDiffMiles property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getTmpDiffMiles() {
        return tmpDiffMiles;
    }

    /**
     * Sets the value of the tmpDiffMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setTmpDiffMiles(QueryableInt32 value) {
        this.tmpDiffMiles = value;
    }

    /**
     * Gets the value of the noteDetail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoteDetail() {
        return noteDetail;
    }

    /**
     * Sets the value of the noteDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoteDetail(String value) {
        this.noteDetail = value;
    }

    /**
     * Gets the value of the seasonID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeasonID() {
        return seasonID;
    }

    /**
     * Sets the value of the seasonID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeasonID(String value) {
        this.seasonID = value;
    }

    /**
     * Gets the value of the bacActUID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getBacActUID() {
        return bacActUID;
    }

    /**
     * Sets the value of the bacActUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setBacActUID(QueryableInt32 value) {
        this.bacActUID = value;
    }

    /**
     * Gets the value of the billed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilled() {
        return billed;
    }

    /**
     * Sets the value of the billed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilled(String value) {
        this.billed = value;
    }

}
