
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceActionEvent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceActionEvent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PersonId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RefNo" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="CotCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RacCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Id" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="Priority" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="AevAssocId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="OlayId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="Completed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnderCompletion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActiveStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReminderSent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Escalated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RemindDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="EscalateDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="DepCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepCodeRaisedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SfmStaffNo" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="SfmStaffNoRaisedBy" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="CreatedDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="CompletionDueDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="CompletionDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="Remarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepStaffName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceActionEvent", propOrder = {
    "personId",
    "refNo",
    "cotCode",
    "status",
    "cardNo",
    "statusDescr",
    "racCode",
    "actDescr",
    "id",
    "priority",
    "aevAssocId",
    "olayId",
    "completed",
    "underCompletion",
    "activeStatus",
    "reminderSent",
    "escalated",
    "remindDate",
    "escalateDate",
    "depCode",
    "depCodeRaisedBy",
    "sfmStaffNo",
    "sfmStaffNoRaisedBy",
    "createdDate",
    "completionDueDate",
    "completionDate",
    "remarks",
    "depStaffName"
})
public class ServiceActionEvent {

    @XmlElement(name = "PersonId")
    protected int personId;
    @XmlElement(name = "RefNo", required = true)
    protected QueryableInt32 refNo;
    @XmlElement(name = "CotCode")
    protected String cotCode;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "CardNo")
    protected String cardNo;
    @XmlElement(name = "StatusDescr")
    protected String statusDescr;
    @XmlElement(name = "RacCode")
    protected String racCode;
    @XmlElement(name = "ActDescr")
    protected String actDescr;
    @XmlElement(name = "Id", required = true)
    protected QueryableInt32 id;
    @XmlElement(name = "Priority", required = true)
    protected QueryableInt32 priority;
    @XmlElement(name = "AevAssocId", required = true)
    protected QueryableInt32 aevAssocId;
    @XmlElement(name = "OlayId", required = true)
    protected QueryableInt32 olayId;
    @XmlElement(name = "Completed")
    protected String completed;
    @XmlElement(name = "UnderCompletion")
    protected String underCompletion;
    @XmlElement(name = "ActiveStatus")
    protected String activeStatus;
    @XmlElement(name = "ReminderSent")
    protected String reminderSent;
    @XmlElement(name = "Escalated")
    protected String escalated;
    @XmlElement(name = "RemindDate", required = true)
    protected QueryableDateTime remindDate;
    @XmlElement(name = "EscalateDate", required = true)
    protected QueryableDateTime escalateDate;
    @XmlElement(name = "DepCode")
    protected String depCode;
    @XmlElement(name = "DepCodeRaisedBy")
    protected String depCodeRaisedBy;
    @XmlElement(name = "SfmStaffNo", required = true)
    protected QueryableInt32 sfmStaffNo;
    @XmlElement(name = "SfmStaffNoRaisedBy", required = true)
    protected QueryableInt32 sfmStaffNoRaisedBy;
    @XmlElement(name = "CreatedDate", required = true)
    protected QueryableDateTime createdDate;
    @XmlElement(name = "CompletionDueDate", required = true)
    protected QueryableDateTime completionDueDate;
    @XmlElement(name = "CompletionDate", required = true)
    protected QueryableDateTime completionDate;
    @XmlElement(name = "Remarks")
    protected String remarks;
    @XmlElement(name = "DepStaffName")
    protected String depStaffName;

    /**
     * Gets the value of the personId property.
     * 
     */
    public int getPersonId() {
        return personId;
    }

    /**
     * Sets the value of the personId property.
     * 
     */
    public void setPersonId(int value) {
        this.personId = value;
    }

    /**
     * Gets the value of the refNo property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getRefNo() {
        return refNo;
    }

    /**
     * Sets the value of the refNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setRefNo(QueryableInt32 value) {
        this.refNo = value;
    }

    /**
     * Gets the value of the cotCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCotCode() {
        return cotCode;
    }

    /**
     * Sets the value of the cotCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotCode(String value) {
        this.cotCode = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the cardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNo() {
        return cardNo;
    }

    /**
     * Sets the value of the cardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNo(String value) {
        this.cardNo = value;
    }

    /**
     * Gets the value of the statusDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusDescr() {
        return statusDescr;
    }

    /**
     * Sets the value of the statusDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusDescr(String value) {
        this.statusDescr = value;
    }

    /**
     * Gets the value of the racCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRacCode() {
        return racCode;
    }

    /**
     * Sets the value of the racCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRacCode(String value) {
        this.racCode = value;
    }

    /**
     * Gets the value of the actDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActDescr() {
        return actDescr;
    }

    /**
     * Sets the value of the actDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActDescr(String value) {
        this.actDescr = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setId(QueryableInt32 value) {
        this.id = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPriority(QueryableInt32 value) {
        this.priority = value;
    }

    /**
     * Gets the value of the aevAssocId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getAevAssocId() {
        return aevAssocId;
    }

    /**
     * Sets the value of the aevAssocId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setAevAssocId(QueryableInt32 value) {
        this.aevAssocId = value;
    }

    /**
     * Gets the value of the olayId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getOlayId() {
        return olayId;
    }

    /**
     * Sets the value of the olayId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setOlayId(QueryableInt32 value) {
        this.olayId = value;
    }

    /**
     * Gets the value of the completed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompleted() {
        return completed;
    }

    /**
     * Sets the value of the completed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompleted(String value) {
        this.completed = value;
    }

    /**
     * Gets the value of the underCompletion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnderCompletion() {
        return underCompletion;
    }

    /**
     * Sets the value of the underCompletion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnderCompletion(String value) {
        this.underCompletion = value;
    }

    /**
     * Gets the value of the activeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveStatus() {
        return activeStatus;
    }

    /**
     * Sets the value of the activeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveStatus(String value) {
        this.activeStatus = value;
    }

    /**
     * Gets the value of the reminderSent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReminderSent() {
        return reminderSent;
    }

    /**
     * Sets the value of the reminderSent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReminderSent(String value) {
        this.reminderSent = value;
    }

    /**
     * Gets the value of the escalated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEscalated() {
        return escalated;
    }

    /**
     * Sets the value of the escalated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEscalated(String value) {
        this.escalated = value;
    }

    /**
     * Gets the value of the remindDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getRemindDate() {
        return remindDate;
    }

    /**
     * Sets the value of the remindDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setRemindDate(QueryableDateTime value) {
        this.remindDate = value;
    }

    /**
     * Gets the value of the escalateDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getEscalateDate() {
        return escalateDate;
    }

    /**
     * Sets the value of the escalateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setEscalateDate(QueryableDateTime value) {
        this.escalateDate = value;
    }

    /**
     * Gets the value of the depCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepCode() {
        return depCode;
    }

    /**
     * Sets the value of the depCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepCode(String value) {
        this.depCode = value;
    }

    /**
     * Gets the value of the depCodeRaisedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepCodeRaisedBy() {
        return depCodeRaisedBy;
    }

    /**
     * Sets the value of the depCodeRaisedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepCodeRaisedBy(String value) {
        this.depCodeRaisedBy = value;
    }

    /**
     * Gets the value of the sfmStaffNo property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getSfmStaffNo() {
        return sfmStaffNo;
    }

    /**
     * Sets the value of the sfmStaffNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setSfmStaffNo(QueryableInt32 value) {
        this.sfmStaffNo = value;
    }

    /**
     * Gets the value of the sfmStaffNoRaisedBy property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getSfmStaffNoRaisedBy() {
        return sfmStaffNoRaisedBy;
    }

    /**
     * Sets the value of the sfmStaffNoRaisedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setSfmStaffNoRaisedBy(QueryableInt32 value) {
        this.sfmStaffNoRaisedBy = value;
    }

    /**
     * Gets the value of the createdDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the value of the createdDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setCreatedDate(QueryableDateTime value) {
        this.createdDate = value;
    }

    /**
     * Gets the value of the completionDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getCompletionDueDate() {
        return completionDueDate;
    }

    /**
     * Sets the value of the completionDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setCompletionDueDate(QueryableDateTime value) {
        this.completionDueDate = value;
    }

    /**
     * Gets the value of the completionDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getCompletionDate() {
        return completionDate;
    }

    /**
     * Sets the value of the completionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setCompletionDate(QueryableDateTime value) {
        this.completionDate = value;
    }

    /**
     * Gets the value of the remarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * Sets the value of the remarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarks(String value) {
        this.remarks = value;
    }

    /**
     * Gets the value of the depStaffName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepStaffName() {
        return depStaffName;
    }

    /**
     * Sets the value of the depStaffName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepStaffName(String value) {
        this.depStaffName = value;
    }

}
