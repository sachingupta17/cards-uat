
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusEnhObj" type="{http://skywards.com/Mercator.CRIS.WS}StatusEnhancement" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "statusEnhObj"
})
@XmlRootElement(name = "AssignCardsToCorporateMember")
public class AssignCardsToCorporateMember {

    @XmlElement(name = "StatusEnhObj")
    protected StatusEnhancement statusEnhObj;

    /**
     * Gets the value of the statusEnhObj property.
     * 
     * @return
     *     possible object is
     *     {@link StatusEnhancement }
     *     
     */
    public StatusEnhancement getStatusEnhObj() {
        return statusEnhObj;
    }

    /**
     * Sets the value of the statusEnhObj property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusEnhancement }
     *     
     */
    public void setStatusEnhObj(StatusEnhancement value) {
        this.statusEnhObj = value;
    }

}
