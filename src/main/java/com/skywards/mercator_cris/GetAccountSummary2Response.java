
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAccountSummary2Result" type="{http://skywards.com/Mercator.CRIS.WS}AccountSummary" minOccurs="0"/>
 *         &lt;element name="wsResult" type="{http://skywards.com/Mercator.CRIS.WS}WSIResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAccountSummary2Result",
    "wsResult"
})
@XmlRootElement(name = "GetAccountSummary2Response")
public class GetAccountSummary2Response {

    @XmlElement(name = "GetAccountSummary2Result")
    protected AccountSummary getAccountSummary2Result;
    protected WSIResult wsResult;

    /**
     * Gets the value of the getAccountSummary2Result property.
     * 
     * @return
     *     possible object is
     *     {@link AccountSummary }
     *     
     */
    public AccountSummary getGetAccountSummary2Result() {
        return getAccountSummary2Result;
    }

    /**
     * Sets the value of the getAccountSummary2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountSummary }
     *     
     */
    public void setGetAccountSummary2Result(AccountSummary value) {
        this.getAccountSummary2Result = value;
    }

    /**
     * Gets the value of the wsResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSIResult }
     *     
     */
    public WSIResult getWsResult() {
        return wsResult;
    }

    /**
     * Sets the value of the wsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSIResult }
     *     
     */
    public void setWsResult(WSIResult value) {
        this.wsResult = value;
    }

}
