
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contactCentralFeedback" type="{http://skywards.com/Mercator.CRIS.WS}ContactCentralFeedback" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "contactCentralFeedback"
})
@XmlRootElement(name = "ContactCentralFeedback")
public class ContactCentralFeedback {

    protected ContactCentralFeedback2 contactCentralFeedback;

    /**
     * Gets the value of the contactCentralFeedback property.
     * 
     * @return
     *     possible object is
     *     {@link ContactCentralFeedback2 }
     *     
     */
    public ContactCentralFeedback2 getContactCentralFeedback() {
        return contactCentralFeedback;
    }

    /**
     * Sets the value of the contactCentralFeedback property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactCentralFeedback2 }
     *     
     */
    public void setContactCentralFeedback(ContactCentralFeedback2 value) {
        this.contactCentralFeedback = value;
    }

}
