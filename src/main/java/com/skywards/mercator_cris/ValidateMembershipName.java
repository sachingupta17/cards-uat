
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardNumner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pnrName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxIndividualName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cardNumner",
    "pnrName",
    "paxIndividualName"
})
@XmlRootElement(name = "ValidateMembershipName")
public class ValidateMembershipName {

    protected String cardNumner;
    protected String pnrName;
    protected String paxIndividualName;

    /**
     * Gets the value of the cardNumner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumner() {
        return cardNumner;
    }

    /**
     * Sets the value of the cardNumner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumner(String value) {
        this.cardNumner = value;
    }

    /**
     * Gets the value of the pnrName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnrName() {
        return pnrName;
    }

    /**
     * Sets the value of the pnrName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnrName(String value) {
        this.pnrName = value;
    }

    /**
     * Gets the value of the paxIndividualName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaxIndividualName() {
        return paxIndividualName;
    }

    /**
     * Sets the value of the paxIndividualName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaxIndividualName(String value) {
        this.paxIndividualName = value;
    }

}
