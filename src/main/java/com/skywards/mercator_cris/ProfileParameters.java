
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProfileParameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfileParameters">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AliasCards" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Addresses" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CreditCards" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Contacts" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Preferences" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Passports" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="TravelCoordinator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Guardians" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileParameters", propOrder = {
    "aliasCards",
    "addresses",
    "creditCards",
    "contacts",
    "preferences",
    "passports",
    "travelCoordinator",
    "guardians"
})
public class ProfileParameters {

    @XmlElement(name = "AliasCards")
    protected boolean aliasCards;
    @XmlElement(name = "Addresses")
    protected boolean addresses;
    @XmlElement(name = "CreditCards")
    protected boolean creditCards;
    @XmlElement(name = "Contacts")
    protected boolean contacts;
    @XmlElement(name = "Preferences")
    protected boolean preferences;
    @XmlElement(name = "Passports")
    protected boolean passports;
    @XmlElement(name = "TravelCoordinator")
    protected boolean travelCoordinator;
    @XmlElement(name = "Guardians")
    protected boolean guardians;

    /**
     * Gets the value of the aliasCards property.
     * 
     */
    public boolean isAliasCards() {
        return aliasCards;
    }

    /**
     * Sets the value of the aliasCards property.
     * 
     */
    public void setAliasCards(boolean value) {
        this.aliasCards = value;
    }

    /**
     * Gets the value of the addresses property.
     * 
     */
    public boolean isAddresses() {
        return addresses;
    }

    /**
     * Sets the value of the addresses property.
     * 
     */
    public void setAddresses(boolean value) {
        this.addresses = value;
    }

    /**
     * Gets the value of the creditCards property.
     * 
     */
    public boolean isCreditCards() {
        return creditCards;
    }

    /**
     * Sets the value of the creditCards property.
     * 
     */
    public void setCreditCards(boolean value) {
        this.creditCards = value;
    }

    /**
     * Gets the value of the contacts property.
     * 
     */
    public boolean isContacts() {
        return contacts;
    }

    /**
     * Sets the value of the contacts property.
     * 
     */
    public void setContacts(boolean value) {
        this.contacts = value;
    }

    /**
     * Gets the value of the preferences property.
     * 
     */
    public boolean isPreferences() {
        return preferences;
    }

    /**
     * Sets the value of the preferences property.
     * 
     */
    public void setPreferences(boolean value) {
        this.preferences = value;
    }

    /**
     * Gets the value of the passports property.
     * 
     */
    public boolean isPassports() {
        return passports;
    }

    /**
     * Sets the value of the passports property.
     * 
     */
    public void setPassports(boolean value) {
        this.passports = value;
    }

    /**
     * Gets the value of the travelCoordinator property.
     * 
     */
    public boolean isTravelCoordinator() {
        return travelCoordinator;
    }

    /**
     * Sets the value of the travelCoordinator property.
     * 
     */
    public void setTravelCoordinator(boolean value) {
        this.travelCoordinator = value;
    }

    /**
     * Gets the value of the guardians property.
     * 
     */
    public boolean isGuardians() {
        return guardians;
    }

    /**
     * Sets the value of the guardians property.
     * 
     */
    public void setGuardians(boolean value) {
        this.guardians = value;
    }

}
