
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRewardItineraryDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRewardItineraryDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RewardItineraryDetail" type="{http://skywards.com/Mercator.CRIS.WS}RewardItineraryDetail" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRewardItineraryDetail", propOrder = {
    "rewardItineraryDetail"
})
public class ArrayOfRewardItineraryDetail {

    @XmlElement(name = "RewardItineraryDetail", nillable = true)
    protected List<RewardItineraryDetail> rewardItineraryDetail;

    /**
     * Gets the value of the rewardItineraryDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rewardItineraryDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRewardItineraryDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RewardItineraryDetail }
     * 
     * 
     */
    public List<RewardItineraryDetail> getRewardItineraryDetail() {
        if (rewardItineraryDetail == null) {
            rewardItineraryDetail = new ArrayList<RewardItineraryDetail>();
        }
        return this.rewardItineraryDetail;
    }

}
