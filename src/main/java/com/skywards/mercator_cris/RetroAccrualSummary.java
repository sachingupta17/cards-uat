
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetroAccrualSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetroAccrualSummary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemUId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketConfirm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Delimeter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightSpecificBonus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OnBoardUpgrade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PromotionalBonus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccrualReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PostTransaction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Webyn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Q25yn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Q25errors" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetroMiles" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetroTierMiles" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketIssueDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetroAccrualSummary", propOrder = {
    "cardNumber",
    "memUId",
    "ticketNumber",
    "ticketConfirm",
    "delimeter",
    "flightSpecificBonus",
    "onBoardUpgrade",
    "promotionalBonus",
    "accrualReason",
    "postTransaction",
    "webyn",
    "q25Yn",
    "q25Errors",
    "description",
    "retroMiles",
    "retroTierMiles",
    "ticketIssueDate"
})
public class RetroAccrualSummary {

    @XmlElement(name = "CardNumber")
    protected String cardNumber;
    @XmlElement(name = "MemUId")
    protected String memUId;
    @XmlElement(name = "TicketNumber")
    protected String ticketNumber;
    @XmlElement(name = "TicketConfirm")
    protected String ticketConfirm;
    @XmlElement(name = "Delimeter")
    protected String delimeter;
    @XmlElement(name = "FlightSpecificBonus")
    protected String flightSpecificBonus;
    @XmlElement(name = "OnBoardUpgrade")
    protected String onBoardUpgrade;
    @XmlElement(name = "PromotionalBonus")
    protected String promotionalBonus;
    @XmlElement(name = "AccrualReason")
    protected String accrualReason;
    @XmlElement(name = "PostTransaction")
    protected String postTransaction;
    @XmlElement(name = "Webyn")
    protected String webyn;
    @XmlElement(name = "Q25yn")
    protected String q25Yn;
    @XmlElement(name = "Q25errors")
    protected String q25Errors;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "RetroMiles")
    protected String retroMiles;
    @XmlElement(name = "RetroTierMiles")
    protected String retroTierMiles;
    @XmlElement(name = "TicketIssueDate", required = true)
    protected QueryableDateTime ticketIssueDate;

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the memUId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemUId() {
        return memUId;
    }

    /**
     * Sets the value of the memUId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemUId(String value) {
        this.memUId = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Sets the value of the ticketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Gets the value of the ticketConfirm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketConfirm() {
        return ticketConfirm;
    }

    /**
     * Sets the value of the ticketConfirm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketConfirm(String value) {
        this.ticketConfirm = value;
    }

    /**
     * Gets the value of the delimeter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelimeter() {
        return delimeter;
    }

    /**
     * Sets the value of the delimeter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelimeter(String value) {
        this.delimeter = value;
    }

    /**
     * Gets the value of the flightSpecificBonus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightSpecificBonus() {
        return flightSpecificBonus;
    }

    /**
     * Sets the value of the flightSpecificBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightSpecificBonus(String value) {
        this.flightSpecificBonus = value;
    }

    /**
     * Gets the value of the onBoardUpgrade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnBoardUpgrade() {
        return onBoardUpgrade;
    }

    /**
     * Sets the value of the onBoardUpgrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnBoardUpgrade(String value) {
        this.onBoardUpgrade = value;
    }

    /**
     * Gets the value of the promotionalBonus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromotionalBonus() {
        return promotionalBonus;
    }

    /**
     * Sets the value of the promotionalBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromotionalBonus(String value) {
        this.promotionalBonus = value;
    }

    /**
     * Gets the value of the accrualReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccrualReason() {
        return accrualReason;
    }

    /**
     * Sets the value of the accrualReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccrualReason(String value) {
        this.accrualReason = value;
    }

    /**
     * Gets the value of the postTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostTransaction() {
        return postTransaction;
    }

    /**
     * Sets the value of the postTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostTransaction(String value) {
        this.postTransaction = value;
    }

    /**
     * Gets the value of the webyn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebyn() {
        return webyn;
    }

    /**
     * Sets the value of the webyn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebyn(String value) {
        this.webyn = value;
    }

    /**
     * Gets the value of the q25Yn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQ25Yn() {
        return q25Yn;
    }

    /**
     * Sets the value of the q25Yn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQ25Yn(String value) {
        this.q25Yn = value;
    }

    /**
     * Gets the value of the q25Errors property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQ25Errors() {
        return q25Errors;
    }

    /**
     * Sets the value of the q25Errors property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQ25Errors(String value) {
        this.q25Errors = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the retroMiles property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetroMiles() {
        return retroMiles;
    }

    /**
     * Sets the value of the retroMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetroMiles(String value) {
        this.retroMiles = value;
    }

    /**
     * Gets the value of the retroTierMiles property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetroTierMiles() {
        return retroTierMiles;
    }

    /**
     * Sets the value of the retroTierMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetroTierMiles(String value) {
        this.retroTierMiles = value;
    }

    /**
     * Gets the value of the ticketIssueDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getTicketIssueDate() {
        return ticketIssueDate;
    }

    /**
     * Sets the value of the ticketIssueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setTicketIssueDate(QueryableDateTime value) {
        this.ticketIssueDate = value;
    }

}
