
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SkywardsTierStatus_Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SkywardsTierStatus_Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SpecialOpCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MsgTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TierPoints" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CurrentMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TierDetailsFull" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfTierDetails" minOccurs="0"/>
 *         &lt;element name="RenewFull" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfRenew" minOccurs="0"/>
 *         &lt;element name="AchieveFull" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfAchieve" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkywardsTierStatus_Request", propOrder = {
    "specialOpCode",
    "msgTemplate",
    "tierPoints",
    "currentMonth",
    "tierDetailsFull",
    "renewFull",
    "achieveFull"
})
public class SkywardsTierStatusRequest {

    @XmlElement(name = "SpecialOpCode")
    protected String specialOpCode;
    @XmlElement(name = "MsgTemplate")
    protected String msgTemplate;
    @XmlElement(name = "TierPoints")
    protected int tierPoints;
    @XmlElement(name = "CurrentMonth")
    protected String currentMonth;
    @XmlElement(name = "TierDetailsFull")
    protected ArrayOfTierDetails tierDetailsFull;
    @XmlElement(name = "RenewFull")
    protected ArrayOfRenew renewFull;
    @XmlElement(name = "AchieveFull")
    protected ArrayOfAchieve achieveFull;

    /**
     * Gets the value of the specialOpCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialOpCode() {
        return specialOpCode;
    }

    /**
     * Sets the value of the specialOpCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialOpCode(String value) {
        this.specialOpCode = value;
    }

    /**
     * Gets the value of the msgTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgTemplate() {
        return msgTemplate;
    }

    /**
     * Sets the value of the msgTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgTemplate(String value) {
        this.msgTemplate = value;
    }

    /**
     * Gets the value of the tierPoints property.
     * 
     */
    public int getTierPoints() {
        return tierPoints;
    }

    /**
     * Sets the value of the tierPoints property.
     * 
     */
    public void setTierPoints(int value) {
        this.tierPoints = value;
    }

    /**
     * Gets the value of the currentMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentMonth() {
        return currentMonth;
    }

    /**
     * Sets the value of the currentMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentMonth(String value) {
        this.currentMonth = value;
    }

    /**
     * Gets the value of the tierDetailsFull property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTierDetails }
     *     
     */
    public ArrayOfTierDetails getTierDetailsFull() {
        return tierDetailsFull;
    }

    /**
     * Sets the value of the tierDetailsFull property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTierDetails }
     *     
     */
    public void setTierDetailsFull(ArrayOfTierDetails value) {
        this.tierDetailsFull = value;
    }

    /**
     * Gets the value of the renewFull property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRenew }
     *     
     */
    public ArrayOfRenew getRenewFull() {
        return renewFull;
    }

    /**
     * Sets the value of the renewFull property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRenew }
     *     
     */
    public void setRenewFull(ArrayOfRenew value) {
        this.renewFull = value;
    }

    /**
     * Gets the value of the achieveFull property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAchieve }
     *     
     */
    public ArrayOfAchieve getAchieveFull() {
        return achieveFull;
    }

    /**
     * Sets the value of the achieveFull property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAchieve }
     *     
     */
    public void setAchieveFull(ArrayOfAchieve value) {
        this.achieveFull = value;
    }

}
