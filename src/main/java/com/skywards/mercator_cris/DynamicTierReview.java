
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicTierReview complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicTierReview">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TCcriteria" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="TMcriteria" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="ActualTierCredit" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="ActualTierMiles" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="BalTierCredit" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="BalTierMiles" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="NextLevelCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TierCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReviewType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReviewPeriodFrom" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="ReviewPeriodTo" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicTierReview", propOrder = {
    "tCcriteria",
    "tMcriteria",
    "actualTierCredit",
    "actualTierMiles",
    "balTierCredit",
    "balTierMiles",
    "nextLevelCode",
    "tierCode",
    "reviewType",
    "reviewPeriodFrom",
    "reviewPeriodTo"
})
public class DynamicTierReview {

    @XmlElement(name = "TCcriteria", required = true)
    protected QueryableDecimal tCcriteria;
    @XmlElement(name = "TMcriteria", required = true)
    protected QueryableDecimal tMcriteria;
    @XmlElement(name = "ActualTierCredit", required = true)
    protected QueryableDecimal actualTierCredit;
    @XmlElement(name = "ActualTierMiles", required = true)
    protected QueryableDecimal actualTierMiles;
    @XmlElement(name = "BalTierCredit", required = true)
    protected QueryableDecimal balTierCredit;
    @XmlElement(name = "BalTierMiles", required = true)
    protected QueryableDecimal balTierMiles;
    @XmlElement(name = "NextLevelCode")
    protected String nextLevelCode;
    @XmlElement(name = "TierCode")
    protected String tierCode;
    @XmlElement(name = "ReviewType")
    protected String reviewType;
    @XmlElement(name = "ReviewPeriodFrom", required = true)
    protected QueryableDateTime reviewPeriodFrom;
    @XmlElement(name = "ReviewPeriodTo", required = true)
    protected QueryableDateTime reviewPeriodTo;

    /**
     * Gets the value of the tCcriteria property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getTCcriteria() {
        return tCcriteria;
    }

    /**
     * Sets the value of the tCcriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setTCcriteria(QueryableDecimal value) {
        this.tCcriteria = value;
    }

    /**
     * Gets the value of the tMcriteria property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getTMcriteria() {
        return tMcriteria;
    }

    /**
     * Sets the value of the tMcriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setTMcriteria(QueryableDecimal value) {
        this.tMcriteria = value;
    }

    /**
     * Gets the value of the actualTierCredit property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getActualTierCredit() {
        return actualTierCredit;
    }

    /**
     * Sets the value of the actualTierCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setActualTierCredit(QueryableDecimal value) {
        this.actualTierCredit = value;
    }

    /**
     * Gets the value of the actualTierMiles property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getActualTierMiles() {
        return actualTierMiles;
    }

    /**
     * Sets the value of the actualTierMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setActualTierMiles(QueryableDecimal value) {
        this.actualTierMiles = value;
    }

    /**
     * Gets the value of the balTierCredit property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getBalTierCredit() {
        return balTierCredit;
    }

    /**
     * Sets the value of the balTierCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setBalTierCredit(QueryableDecimal value) {
        this.balTierCredit = value;
    }

    /**
     * Gets the value of the balTierMiles property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getBalTierMiles() {
        return balTierMiles;
    }

    /**
     * Sets the value of the balTierMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setBalTierMiles(QueryableDecimal value) {
        this.balTierMiles = value;
    }

    /**
     * Gets the value of the nextLevelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextLevelCode() {
        return nextLevelCode;
    }

    /**
     * Sets the value of the nextLevelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextLevelCode(String value) {
        this.nextLevelCode = value;
    }

    /**
     * Gets the value of the tierCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierCode() {
        return tierCode;
    }

    /**
     * Sets the value of the tierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierCode(String value) {
        this.tierCode = value;
    }

    /**
     * Gets the value of the reviewType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReviewType() {
        return reviewType;
    }

    /**
     * Sets the value of the reviewType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReviewType(String value) {
        this.reviewType = value;
    }

    /**
     * Gets the value of the reviewPeriodFrom property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getReviewPeriodFrom() {
        return reviewPeriodFrom;
    }

    /**
     * Sets the value of the reviewPeriodFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setReviewPeriodFrom(QueryableDateTime value) {
        this.reviewPeriodFrom = value;
    }

    /**
     * Gets the value of the reviewPeriodTo property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getReviewPeriodTo() {
        return reviewPeriodTo;
    }

    /**
     * Sets the value of the reviewPeriodTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setReviewPeriodTo(QueryableDateTime value) {
        this.reviewPeriodTo = value;
    }

}
