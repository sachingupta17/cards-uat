
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactListing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactListing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RowId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RefNo" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PerPersonId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="AddAddId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="Third" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Main" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateInitiated" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="Priority" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShortDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SettlementDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="CotCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Descr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreatedDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="CreatedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ModifiedDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="ModifiedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComebackNo" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="RaisedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhysicalFileLoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LptCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LptName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HandledByStn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RefDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SecId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CbDateOpened" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="CbDateClosed" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="Legal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActiveStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HandledByDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompensationFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompensationMsgString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactListing", propOrder = {
    "rowId",
    "refNo",
    "perPersonId",
    "addAddId",
    "third",
    "main",
    "dateInitiated",
    "priority",
    "statusCode",
    "shortDescr",
    "settlementDate",
    "cotCode",
    "descr",
    "flag",
    "createdDate",
    "createdBy",
    "modifiedDate",
    "modifiedBy",
    "comebackNo",
    "raisedBy",
    "physicalFileLoc",
    "status",
    "contactType",
    "lptCode",
    "lptName",
    "handledByStn",
    "refDescr",
    "secId",
    "cbDateOpened",
    "cbDateClosed",
    "legal",
    "actStatusCode",
    "activeStatus",
    "handledByDesc",
    "compensationFlag",
    "compensationMsgString"
})
public class ContactListing {

    @XmlElement(name = "RowId")
    protected String rowId;
    @XmlElement(name = "RefNo", required = true)
    protected QueryableInt32 refNo;
    @XmlElement(name = "PerPersonId", required = true)
    protected QueryableInt32 perPersonId;
    @XmlElement(name = "AddAddId", required = true)
    protected QueryableInt32 addAddId;
    @XmlElement(name = "Third")
    protected String third;
    @XmlElement(name = "Main")
    protected String main;
    @XmlElement(name = "DateInitiated", required = true)
    protected QueryableDateTime dateInitiated;
    @XmlElement(name = "Priority", required = true)
    protected QueryableInt32 priority;
    @XmlElement(name = "StatusCode")
    protected String statusCode;
    @XmlElement(name = "ShortDescr")
    protected String shortDescr;
    @XmlElement(name = "SettlementDate", required = true)
    protected QueryableDateTime settlementDate;
    @XmlElement(name = "CotCode")
    protected String cotCode;
    @XmlElement(name = "Descr")
    protected String descr;
    @XmlElement(name = "Flag")
    protected String flag;
    @XmlElement(name = "CreatedDate", required = true)
    protected QueryableDateTime createdDate;
    @XmlElement(name = "CreatedBy")
    protected String createdBy;
    @XmlElement(name = "ModifiedDate", required = true)
    protected QueryableDateTime modifiedDate;
    @XmlElement(name = "ModifiedBy")
    protected String modifiedBy;
    @XmlElement(name = "ComebackNo", required = true)
    protected QueryableInt32 comebackNo;
    @XmlElement(name = "RaisedBy")
    protected String raisedBy;
    @XmlElement(name = "PhysicalFileLoc")
    protected String physicalFileLoc;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "ContactType")
    protected String contactType;
    @XmlElement(name = "LptCode")
    protected String lptCode;
    @XmlElement(name = "LptName")
    protected String lptName;
    @XmlElement(name = "HandledByStn")
    protected String handledByStn;
    @XmlElement(name = "RefDescr")
    protected String refDescr;
    @XmlElement(name = "SecId")
    protected String secId;
    @XmlElement(name = "CbDateOpened", required = true)
    protected QueryableDateTime cbDateOpened;
    @XmlElement(name = "CbDateClosed", required = true)
    protected QueryableDateTime cbDateClosed;
    @XmlElement(name = "Legal")
    protected String legal;
    @XmlElement(name = "ActStatusCode")
    protected String actStatusCode;
    @XmlElement(name = "ActiveStatus")
    protected String activeStatus;
    @XmlElement(name = "HandledByDesc")
    protected String handledByDesc;
    @XmlElement(name = "CompensationFlag")
    protected String compensationFlag;
    @XmlElement(name = "CompensationMsgString")
    protected String compensationMsgString;

    /**
     * Gets the value of the rowId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRowId() {
        return rowId;
    }

    /**
     * Sets the value of the rowId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRowId(String value) {
        this.rowId = value;
    }

    /**
     * Gets the value of the refNo property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getRefNo() {
        return refNo;
    }

    /**
     * Sets the value of the refNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setRefNo(QueryableInt32 value) {
        this.refNo = value;
    }

    /**
     * Gets the value of the perPersonId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPerPersonId() {
        return perPersonId;
    }

    /**
     * Sets the value of the perPersonId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPerPersonId(QueryableInt32 value) {
        this.perPersonId = value;
    }

    /**
     * Gets the value of the addAddId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getAddAddId() {
        return addAddId;
    }

    /**
     * Sets the value of the addAddId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setAddAddId(QueryableInt32 value) {
        this.addAddId = value;
    }

    /**
     * Gets the value of the third property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThird() {
        return third;
    }

    /**
     * Sets the value of the third property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThird(String value) {
        this.third = value;
    }

    /**
     * Gets the value of the main property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMain() {
        return main;
    }

    /**
     * Sets the value of the main property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMain(String value) {
        this.main = value;
    }

    /**
     * Gets the value of the dateInitiated property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getDateInitiated() {
        return dateInitiated;
    }

    /**
     * Sets the value of the dateInitiated property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setDateInitiated(QueryableDateTime value) {
        this.dateInitiated = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPriority(QueryableInt32 value) {
        this.priority = value;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the shortDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortDescr() {
        return shortDescr;
    }

    /**
     * Sets the value of the shortDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortDescr(String value) {
        this.shortDescr = value;
    }

    /**
     * Gets the value of the settlementDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getSettlementDate() {
        return settlementDate;
    }

    /**
     * Sets the value of the settlementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setSettlementDate(QueryableDateTime value) {
        this.settlementDate = value;
    }

    /**
     * Gets the value of the cotCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCotCode() {
        return cotCode;
    }

    /**
     * Sets the value of the cotCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotCode(String value) {
        this.cotCode = value;
    }

    /**
     * Gets the value of the descr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescr() {
        return descr;
    }

    /**
     * Sets the value of the descr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescr(String value) {
        this.descr = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlag(String value) {
        this.flag = value;
    }

    /**
     * Gets the value of the createdDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the value of the createdDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setCreatedDate(QueryableDateTime value) {
        this.createdDate = value;
    }

    /**
     * Gets the value of the createdBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the modifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getModifiedDate() {
        return modifiedDate;
    }

    /**
     * Sets the value of the modifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setModifiedDate(QueryableDateTime value) {
        this.modifiedDate = value;
    }

    /**
     * Gets the value of the modifiedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * Sets the value of the modifiedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModifiedBy(String value) {
        this.modifiedBy = value;
    }

    /**
     * Gets the value of the comebackNo property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getComebackNo() {
        return comebackNo;
    }

    /**
     * Sets the value of the comebackNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setComebackNo(QueryableInt32 value) {
        this.comebackNo = value;
    }

    /**
     * Gets the value of the raisedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRaisedBy() {
        return raisedBy;
    }

    /**
     * Sets the value of the raisedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRaisedBy(String value) {
        this.raisedBy = value;
    }

    /**
     * Gets the value of the physicalFileLoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhysicalFileLoc() {
        return physicalFileLoc;
    }

    /**
     * Sets the value of the physicalFileLoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhysicalFileLoc(String value) {
        this.physicalFileLoc = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the contactType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactType() {
        return contactType;
    }

    /**
     * Sets the value of the contactType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactType(String value) {
        this.contactType = value;
    }

    /**
     * Gets the value of the lptCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLptCode() {
        return lptCode;
    }

    /**
     * Sets the value of the lptCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLptCode(String value) {
        this.lptCode = value;
    }

    /**
     * Gets the value of the lptName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLptName() {
        return lptName;
    }

    /**
     * Sets the value of the lptName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLptName(String value) {
        this.lptName = value;
    }

    /**
     * Gets the value of the handledByStn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandledByStn() {
        return handledByStn;
    }

    /**
     * Sets the value of the handledByStn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandledByStn(String value) {
        this.handledByStn = value;
    }

    /**
     * Gets the value of the refDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefDescr() {
        return refDescr;
    }

    /**
     * Sets the value of the refDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefDescr(String value) {
        this.refDescr = value;
    }

    /**
     * Gets the value of the secId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecId() {
        return secId;
    }

    /**
     * Sets the value of the secId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecId(String value) {
        this.secId = value;
    }

    /**
     * Gets the value of the cbDateOpened property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getCbDateOpened() {
        return cbDateOpened;
    }

    /**
     * Sets the value of the cbDateOpened property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setCbDateOpened(QueryableDateTime value) {
        this.cbDateOpened = value;
    }

    /**
     * Gets the value of the cbDateClosed property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getCbDateClosed() {
        return cbDateClosed;
    }

    /**
     * Sets the value of the cbDateClosed property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setCbDateClosed(QueryableDateTime value) {
        this.cbDateClosed = value;
    }

    /**
     * Gets the value of the legal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegal() {
        return legal;
    }

    /**
     * Sets the value of the legal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegal(String value) {
        this.legal = value;
    }

    /**
     * Gets the value of the actStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActStatusCode() {
        return actStatusCode;
    }

    /**
     * Sets the value of the actStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActStatusCode(String value) {
        this.actStatusCode = value;
    }

    /**
     * Gets the value of the activeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveStatus() {
        return activeStatus;
    }

    /**
     * Sets the value of the activeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveStatus(String value) {
        this.activeStatus = value;
    }

    /**
     * Gets the value of the handledByDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandledByDesc() {
        return handledByDesc;
    }

    /**
     * Sets the value of the handledByDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandledByDesc(String value) {
        this.handledByDesc = value;
    }

    /**
     * Gets the value of the compensationFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompensationFlag() {
        return compensationFlag;
    }

    /**
     * Sets the value of the compensationFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompensationFlag(String value) {
        this.compensationFlag = value;
    }

    /**
     * Gets the value of the compensationMsgString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompensationMsgString() {
        return compensationMsgString;
    }

    /**
     * Sets the value of the compensationMsgString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompensationMsgString(String value) {
        this.compensationMsgString = value;
    }

}
