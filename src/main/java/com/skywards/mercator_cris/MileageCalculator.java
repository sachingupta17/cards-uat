
package com.skywards.mercator_cris;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MileageCalculator complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MileageCalculator">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Activity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PtnrCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Bcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Miles" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Sno" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MileageCalculator", propOrder = {
    "activity",
    "ptnrCode",
    "flightType",
    "bcp",
    "miles",
    "sno"
})
public class MileageCalculator {

    @XmlElement(name = "Activity")
    protected String activity;
    @XmlElement(name = "PtnrCode")
    protected String ptnrCode;
    @XmlElement(name = "FlightType")
    protected String flightType;
    @XmlElement(name = "Bcp")
    protected String bcp;
    @XmlElement(name = "Miles", required = true)
    protected BigDecimal miles;
    @XmlElement(name = "Sno")
    protected int sno;

    /**
     * Gets the value of the activity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivity() {
        return activity;
    }

    /**
     * Sets the value of the activity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivity(String value) {
        this.activity = value;
    }

    /**
     * Gets the value of the ptnrCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPtnrCode() {
        return ptnrCode;
    }

    /**
     * Sets the value of the ptnrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPtnrCode(String value) {
        this.ptnrCode = value;
    }

    /**
     * Gets the value of the flightType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightType() {
        return flightType;
    }

    /**
     * Sets the value of the flightType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightType(String value) {
        this.flightType = value;
    }

    /**
     * Gets the value of the bcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBcp() {
        return bcp;
    }

    /**
     * Sets the value of the bcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBcp(String value) {
        this.bcp = value;
    }

    /**
     * Gets the value of the miles property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMiles() {
        return miles;
    }

    /**
     * Sets the value of the miles property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMiles(BigDecimal value) {
        this.miles = value;
    }

    /**
     * Gets the value of the sno property.
     * 
     */
    public int getSno() {
        return sno;
    }

    /**
     * Sets the value of the sno property.
     * 
     */
    public void setSno(int value) {
        this.sno = value;
    }

}
