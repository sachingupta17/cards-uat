
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lastname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Middlename" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Mobilenumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Preferredphone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsSkywardsMember" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsProspect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tiercode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SkywardsNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "title",
    "firstname",
    "lastname",
    "middlename",
    "emailAddress",
    "birthDate",
    "mobilenumber",
    "preferredphone",
    "isSkywardsMember",
    "isProspect",
    "tiercode",
    "skywardsNo"
})
@XmlRootElement(name = "FindMember")
public class FindMember {

    @XmlElement(name = "Title")
    protected String title;
    protected String firstname;
    @XmlElement(name = "Lastname")
    protected String lastname;
    @XmlElement(name = "Middlename")
    protected String middlename;
    @XmlElement(name = "EmailAddress")
    protected String emailAddress;
    @XmlElement(name = "BirthDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar birthDate;
    @XmlElement(name = "Mobilenumber")
    protected String mobilenumber;
    @XmlElement(name = "Preferredphone")
    protected String preferredphone;
    @XmlElement(name = "IsSkywardsMember")
    protected String isSkywardsMember;
    @XmlElement(name = "IsProspect")
    protected String isProspect;
    @XmlElement(name = "Tiercode")
    protected String tiercode;
    @XmlElement(name = "SkywardsNo")
    protected String skywardsNo;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the firstname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Sets the value of the firstname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstname(String value) {
        this.firstname = value;
    }

    /**
     * Gets the value of the lastname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Sets the value of the lastname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastname(String value) {
        this.lastname = value;
    }

    /**
     * Gets the value of the middlename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddlename() {
        return middlename;
    }

    /**
     * Sets the value of the middlename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddlename(String value) {
        this.middlename = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDate(XMLGregorianCalendar value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the mobilenumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilenumber() {
        return mobilenumber;
    }

    /**
     * Sets the value of the mobilenumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilenumber(String value) {
        this.mobilenumber = value;
    }

    /**
     * Gets the value of the preferredphone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredphone() {
        return preferredphone;
    }

    /**
     * Sets the value of the preferredphone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredphone(String value) {
        this.preferredphone = value;
    }

    /**
     * Gets the value of the isSkywardsMember property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSkywardsMember() {
        return isSkywardsMember;
    }

    /**
     * Sets the value of the isSkywardsMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSkywardsMember(String value) {
        this.isSkywardsMember = value;
    }

    /**
     * Gets the value of the isProspect property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsProspect() {
        return isProspect;
    }

    /**
     * Sets the value of the isProspect property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsProspect(String value) {
        this.isProspect = value;
    }

    /**
     * Gets the value of the tiercode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTiercode() {
        return tiercode;
    }

    /**
     * Sets the value of the tiercode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTiercode(String value) {
        this.tiercode = value;
    }

    /**
     * Gets the value of the skywardsNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkywardsNo() {
        return skywardsNo;
    }

    /**
     * Sets the value of the skywardsNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkywardsNo(String value) {
        this.skywardsNo = value;
    }

}
