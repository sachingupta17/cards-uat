
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWhatCanIDoOutputWE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWhatCanIDoOutputWE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WhatCanIDoOutputWE" type="{http://skywards.com/Mercator.CRIS.WS}WhatCanIDoOutputWE" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWhatCanIDoOutputWE", propOrder = {
    "whatCanIDoOutputWE"
})
public class ArrayOfWhatCanIDoOutputWE {

    @XmlElement(name = "WhatCanIDoOutputWE", nillable = true)
    protected List<WhatCanIDoOutputWE> whatCanIDoOutputWE;

    /**
     * Gets the value of the whatCanIDoOutputWE property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the whatCanIDoOutputWE property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWhatCanIDoOutputWE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WhatCanIDoOutputWE }
     * 
     * 
     */
    public List<WhatCanIDoOutputWE> getWhatCanIDoOutputWE() {
        if (whatCanIDoOutputWE == null) {
            whatCanIDoOutputWE = new ArrayList<WhatCanIDoOutputWE>();
        }
        return this.whatCanIDoOutputWE;
    }

}
