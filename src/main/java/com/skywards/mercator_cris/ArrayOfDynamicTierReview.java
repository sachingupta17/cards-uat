
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDynamicTierReview complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDynamicTierReview">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DynamicTierReview" type="{http://skywards.com/Mercator.CRIS.WS}DynamicTierReview" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDynamicTierReview", propOrder = {
    "dynamicTierReview"
})
public class ArrayOfDynamicTierReview {

    @XmlElement(name = "DynamicTierReview", nillable = true)
    protected List<DynamicTierReview> dynamicTierReview;

    /**
     * Gets the value of the dynamicTierReview property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dynamicTierReview property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDynamicTierReview().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DynamicTierReview }
     * 
     * 
     */
    public List<DynamicTierReview> getDynamicTierReview() {
        if (dynamicTierReview == null) {
            dynamicTierReview = new ArrayList<DynamicTierReview>();
        }
        return this.dynamicTierReview;
    }

}
