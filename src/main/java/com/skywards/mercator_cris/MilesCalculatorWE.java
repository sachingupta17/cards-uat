
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MilesCalculatorWE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MilesCalculatorWE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SkywardsMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TierMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ActivityDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActUid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FlightDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Class" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Origin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Destination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TktType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MilesCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FromZone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ToZone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TierType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MilesCalculatorWE", propOrder = {
    "skywardsMiles",
    "tierMiles",
    "activityDesc",
    "actUid",
    "flightDate",
    "clazz",
    "origin",
    "destination",
    "tktType",
    "milesCategory",
    "fromZone",
    "toZone",
    "flightNumber",
    "tierType"
})
public class MilesCalculatorWE {

    @XmlElement(name = "SkywardsMiles")
    protected int skywardsMiles;
    @XmlElement(name = "TierMiles")
    protected int tierMiles;
    @XmlElement(name = "ActivityDesc")
    protected String activityDesc;
    @XmlElement(name = "ActUid")
    protected int actUid;
    @XmlElement(name = "FlightDate")
    protected String flightDate;
    @XmlElement(name = "Class")
    protected String clazz;
    @XmlElement(name = "Origin")
    protected String origin;
    @XmlElement(name = "Destination")
    protected String destination;
    @XmlElement(name = "TktType")
    protected String tktType;
    @XmlElement(name = "MilesCategory")
    protected String milesCategory;
    @XmlElement(name = "FromZone")
    protected String fromZone;
    @XmlElement(name = "ToZone")
    protected String toZone;
    @XmlElement(name = "FlightNumber")
    protected String flightNumber;
    @XmlElement(name = "TierType")
    protected String tierType;

    /**
     * Gets the value of the skywardsMiles property.
     * 
     */
    public int getSkywardsMiles() {
        return skywardsMiles;
    }

    /**
     * Sets the value of the skywardsMiles property.
     * 
     */
    public void setSkywardsMiles(int value) {
        this.skywardsMiles = value;
    }

    /**
     * Gets the value of the tierMiles property.
     * 
     */
    public int getTierMiles() {
        return tierMiles;
    }

    /**
     * Sets the value of the tierMiles property.
     * 
     */
    public void setTierMiles(int value) {
        this.tierMiles = value;
    }

    /**
     * Gets the value of the activityDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityDesc() {
        return activityDesc;
    }

    /**
     * Sets the value of the activityDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityDesc(String value) {
        this.activityDesc = value;
    }

    /**
     * Gets the value of the actUid property.
     * 
     */
    public int getActUid() {
        return actUid;
    }

    /**
     * Sets the value of the actUid property.
     * 
     */
    public void setActUid(int value) {
        this.actUid = value;
    }

    /**
     * Gets the value of the flightDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightDate() {
        return flightDate;
    }

    /**
     * Sets the value of the flightDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightDate(String value) {
        this.flightDate = value;
    }

    /**
     * Gets the value of the clazz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Sets the value of the clazz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClazz(String value) {
        this.clazz = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Gets the value of the destination property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Gets the value of the tktType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTktType() {
        return tktType;
    }

    /**
     * Sets the value of the tktType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTktType(String value) {
        this.tktType = value;
    }

    /**
     * Gets the value of the milesCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMilesCategory() {
        return milesCategory;
    }

    /**
     * Sets the value of the milesCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMilesCategory(String value) {
        this.milesCategory = value;
    }

    /**
     * Gets the value of the fromZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromZone() {
        return fromZone;
    }

    /**
     * Sets the value of the fromZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromZone(String value) {
        this.fromZone = value;
    }

    /**
     * Gets the value of the toZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToZone() {
        return toZone;
    }

    /**
     * Sets the value of the toZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToZone(String value) {
        this.toZone = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the tierType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierType() {
        return tierType;
    }

    /**
     * Sets the value of the tierType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierType(String value) {
        this.tierType = value;
    }

}
