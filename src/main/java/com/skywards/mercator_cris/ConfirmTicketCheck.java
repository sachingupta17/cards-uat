
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="retroAccrualSummary" type="{http://skywards.com/Mercator.CRIS.WS}RetroAccrualSummary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retroAccrualSummary"
})
@XmlRootElement(name = "ConfirmTicketCheck")
public class ConfirmTicketCheck {

    protected RetroAccrualSummary retroAccrualSummary;

    /**
     * Gets the value of the retroAccrualSummary property.
     * 
     * @return
     *     possible object is
     *     {@link RetroAccrualSummary }
     *     
     */
    public RetroAccrualSummary getRetroAccrualSummary() {
        return retroAccrualSummary;
    }

    /**
     * Sets the value of the retroAccrualSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetroAccrualSummary }
     *     
     */
    public void setRetroAccrualSummary(RetroAccrualSummary value) {
        this.retroAccrualSummary = value;
    }

}
