
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="headactivecardnumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="memberactivecardnumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="relationship" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="familyMemberStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headactivecardnumber",
    "memberactivecardnumber",
    "relationship",
    "familyMemberStatus"
})
@XmlRootElement(name = "LinkFamilyMember")
public class LinkFamilyMember {

    protected String headactivecardnumber;
    protected String memberactivecardnumber;
    protected String relationship;
    protected String familyMemberStatus;

    /**
     * Gets the value of the headactivecardnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeadactivecardnumber() {
        return headactivecardnumber;
    }

    /**
     * Sets the value of the headactivecardnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeadactivecardnumber(String value) {
        this.headactivecardnumber = value;
    }

    /**
     * Gets the value of the memberactivecardnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberactivecardnumber() {
        return memberactivecardnumber;
    }

    /**
     * Sets the value of the memberactivecardnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberactivecardnumber(String value) {
        this.memberactivecardnumber = value;
    }

    /**
     * Gets the value of the relationship property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationship() {
        return relationship;
    }

    /**
     * Sets the value of the relationship property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationship(String value) {
        this.relationship = value;
    }

    /**
     * Gets the value of the familyMemberStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyMemberStatus() {
        return familyMemberStatus;
    }

    /**
     * Sets the value of the familyMemberStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyMemberStatus(String value) {
        this.familyMemberStatus = value;
    }

}
