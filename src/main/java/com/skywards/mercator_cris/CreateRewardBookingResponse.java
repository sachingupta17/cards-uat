
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateRewardBookingResult" type="{http://skywards.com/Mercator.CRIS.WS}RewardBooking" minOccurs="0"/>
 *         &lt;element name="itineraryDetailsList" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfRewardItineraryDetail" minOccurs="0"/>
 *         &lt;element name="rewardCodes" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfInt" minOccurs="0"/>
 *         &lt;element name="redemptionItineraryDetailsList" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfRewardItineraryDetail" minOccurs="0"/>
 *         &lt;element name="specialItineraryDetailsList" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfRewardItineraryDetail" minOccurs="0"/>
 *         &lt;element name="wsResult" type="{http://skywards.com/Mercator.CRIS.WS}WSIResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createRewardBookingResult",
    "itineraryDetailsList",
    "rewardCodes",
    "redemptionItineraryDetailsList",
    "specialItineraryDetailsList",
    "wsResult"
})
@XmlRootElement(name = "CreateRewardBookingResponse")
public class CreateRewardBookingResponse {

    @XmlElement(name = "CreateRewardBookingResult")
    protected RewardBooking createRewardBookingResult;
    protected ArrayOfRewardItineraryDetail itineraryDetailsList;
    protected ArrayOfInt rewardCodes;
    protected ArrayOfRewardItineraryDetail redemptionItineraryDetailsList;
    protected ArrayOfRewardItineraryDetail specialItineraryDetailsList;
    protected WSIResult wsResult;

    /**
     * Gets the value of the createRewardBookingResult property.
     * 
     * @return
     *     possible object is
     *     {@link RewardBooking }
     *     
     */
    public RewardBooking getCreateRewardBookingResult() {
        return createRewardBookingResult;
    }

    /**
     * Sets the value of the createRewardBookingResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardBooking }
     *     
     */
    public void setCreateRewardBookingResult(RewardBooking value) {
        this.createRewardBookingResult = value;
    }

    /**
     * Gets the value of the itineraryDetailsList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRewardItineraryDetail }
     *     
     */
    public ArrayOfRewardItineraryDetail getItineraryDetailsList() {
        return itineraryDetailsList;
    }

    /**
     * Sets the value of the itineraryDetailsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRewardItineraryDetail }
     *     
     */
    public void setItineraryDetailsList(ArrayOfRewardItineraryDetail value) {
        this.itineraryDetailsList = value;
    }

    /**
     * Gets the value of the rewardCodes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInt }
     *     
     */
    public ArrayOfInt getRewardCodes() {
        return rewardCodes;
    }

    /**
     * Sets the value of the rewardCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInt }
     *     
     */
    public void setRewardCodes(ArrayOfInt value) {
        this.rewardCodes = value;
    }

    /**
     * Gets the value of the redemptionItineraryDetailsList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRewardItineraryDetail }
     *     
     */
    public ArrayOfRewardItineraryDetail getRedemptionItineraryDetailsList() {
        return redemptionItineraryDetailsList;
    }

    /**
     * Sets the value of the redemptionItineraryDetailsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRewardItineraryDetail }
     *     
     */
    public void setRedemptionItineraryDetailsList(ArrayOfRewardItineraryDetail value) {
        this.redemptionItineraryDetailsList = value;
    }

    /**
     * Gets the value of the specialItineraryDetailsList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRewardItineraryDetail }
     *     
     */
    public ArrayOfRewardItineraryDetail getSpecialItineraryDetailsList() {
        return specialItineraryDetailsList;
    }

    /**
     * Sets the value of the specialItineraryDetailsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRewardItineraryDetail }
     *     
     */
    public void setSpecialItineraryDetailsList(ArrayOfRewardItineraryDetail value) {
        this.specialItineraryDetailsList = value;
    }

    /**
     * Gets the value of the wsResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSIResult }
     *     
     */
    public WSIResult getWsResult() {
        return wsResult;
    }

    /**
     * Sets the value of the wsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSIResult }
     *     
     */
    public void setWsResult(WSIResult value) {
        this.wsResult = value;
    }

}
