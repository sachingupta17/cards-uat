
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MileageCalculator_Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MileageCalculator_Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Via" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MileageCalcs" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfMileageCalculator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MileageCalculator_Request", propOrder = {
    "via",
    "mileageCalcs"
})
public class MileageCalculatorRequest {

    @XmlElement(name = "Via")
    protected String via;
    @XmlElement(name = "MileageCalcs")
    protected ArrayOfMileageCalculator mileageCalcs;

    /**
     * Gets the value of the via property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVia() {
        return via;
    }

    /**
     * Sets the value of the via property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVia(String value) {
        this.via = value;
    }

    /**
     * Gets the value of the mileageCalcs property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMileageCalculator }
     *     
     */
    public ArrayOfMileageCalculator getMileageCalcs() {
        return mileageCalcs;
    }

    /**
     * Sets the value of the mileageCalcs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMileageCalculator }
     *     
     */
    public void setMileageCalcs(ArrayOfMileageCalculator value) {
        this.mileageCalcs = value;
    }

}
