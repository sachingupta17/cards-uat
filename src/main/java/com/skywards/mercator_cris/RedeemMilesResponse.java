
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RedeemMilesResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rewardCode" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfInt" minOccurs="0"/>
 *         &lt;element name="wsResult" type="{http://skywards.com/Mercator.CRIS.WS}WSIResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "redeemMilesResult",
    "rewardCode",
    "wsResult"
})
@XmlRootElement(name = "RedeemMilesResponse")
public class RedeemMilesResponse {

    @XmlElement(name = "RedeemMilesResult")
    protected String redeemMilesResult;
    protected ArrayOfInt rewardCode;
    protected WSIResult wsResult;

    /**
     * Gets the value of the redeemMilesResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedeemMilesResult() {
        return redeemMilesResult;
    }

    /**
     * Sets the value of the redeemMilesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedeemMilesResult(String value) {
        this.redeemMilesResult = value;
    }

    /**
     * Gets the value of the rewardCode property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInt }
     *     
     */
    public ArrayOfInt getRewardCode() {
        return rewardCode;
    }

    /**
     * Sets the value of the rewardCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInt }
     *     
     */
    public void setRewardCode(ArrayOfInt value) {
        this.rewardCode = value;
    }

    /**
     * Gets the value of the wsResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSIResult }
     *     
     */
    public WSIResult getWsResult() {
        return wsResult;
    }

    /**
     * Sets the value of the wsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSIResult }
     *     
     */
    public void setWsResult(WSIResult value) {
        this.wsResult = value;
    }

}
