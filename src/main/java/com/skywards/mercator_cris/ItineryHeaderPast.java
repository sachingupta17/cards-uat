
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ItineryHeaderPast complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItineryHeaderPast">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FfpNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmeID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmeStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceClassCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PnrCreationDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="PnrId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="PnrPaxId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="BoardPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OffPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OffDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="ArrivalTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UtcDeparture" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="BookingStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="E_Ticket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxIndividualName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineryHeaderPast", propOrder = {
    "ffpNumber",
    "smeID",
    "smeStatus",
    "serviceClassCode",
    "recLocator",
    "pnrCreationDate",
    "pnrId",
    "pnrPaxId",
    "boardPoint",
    "departureDate",
    "departureTime",
    "offPoint",
    "offDate",
    "arrivalTime",
    "utcDeparture",
    "bookingStatus",
    "eTicket",
    "email",
    "paxName",
    "paxIndividualName",
    "paymentType",
    "flightNumber"
})
public class ItineryHeaderPast {

    @XmlElement(name = "FfpNumber")
    protected String ffpNumber;
    @XmlElement(name = "SmeID")
    protected String smeID;
    @XmlElement(name = "SmeStatus")
    protected String smeStatus;
    @XmlElement(name = "ServiceClassCode")
    protected String serviceClassCode;
    @XmlElement(name = "RecLocator")
    protected String recLocator;
    @XmlElement(name = "PnrCreationDate", required = true)
    protected QueryableDateTime pnrCreationDate;
    @XmlElement(name = "PnrId", required = true)
    protected QueryableDecimal pnrId;
    @XmlElement(name = "PnrPaxId", required = true)
    protected QueryableDecimal pnrPaxId;
    @XmlElement(name = "BoardPoint")
    protected String boardPoint;
    @XmlElement(name = "DepartureDate", required = true)
    protected QueryableDateTime departureDate;
    @XmlElement(name = "DepartureTime")
    protected String departureTime;
    @XmlElement(name = "OffPoint")
    protected String offPoint;
    @XmlElement(name = "OffDate", required = true)
    protected QueryableDateTime offDate;
    @XmlElement(name = "ArrivalTime")
    protected String arrivalTime;
    @XmlElement(name = "UtcDeparture", required = true)
    protected QueryableDateTime utcDeparture;
    @XmlElement(name = "BookingStatus")
    protected String bookingStatus;
    @XmlElement(name = "E_Ticket")
    protected String eTicket;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "PaxName")
    protected String paxName;
    @XmlElement(name = "PaxIndividualName")
    protected String paxIndividualName;
    @XmlElement(name = "PaymentType")
    protected String paymentType;
    @XmlElement(name = "FlightNumber")
    protected String flightNumber;

    /**
     * Gets the value of the ffpNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFfpNumber() {
        return ffpNumber;
    }

    /**
     * Sets the value of the ffpNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFfpNumber(String value) {
        this.ffpNumber = value;
    }

    /**
     * Gets the value of the smeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmeID() {
        return smeID;
    }

    /**
     * Sets the value of the smeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmeID(String value) {
        this.smeID = value;
    }

    /**
     * Gets the value of the smeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmeStatus() {
        return smeStatus;
    }

    /**
     * Sets the value of the smeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmeStatus(String value) {
        this.smeStatus = value;
    }

    /**
     * Gets the value of the serviceClassCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceClassCode() {
        return serviceClassCode;
    }

    /**
     * Sets the value of the serviceClassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceClassCode(String value) {
        this.serviceClassCode = value;
    }

    /**
     * Gets the value of the recLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecLocator() {
        return recLocator;
    }

    /**
     * Sets the value of the recLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecLocator(String value) {
        this.recLocator = value;
    }

    /**
     * Gets the value of the pnrCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getPnrCreationDate() {
        return pnrCreationDate;
    }

    /**
     * Sets the value of the pnrCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setPnrCreationDate(QueryableDateTime value) {
        this.pnrCreationDate = value;
    }

    /**
     * Gets the value of the pnrId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getPnrId() {
        return pnrId;
    }

    /**
     * Sets the value of the pnrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setPnrId(QueryableDecimal value) {
        this.pnrId = value;
    }

    /**
     * Gets the value of the pnrPaxId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getPnrPaxId() {
        return pnrPaxId;
    }

    /**
     * Sets the value of the pnrPaxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setPnrPaxId(QueryableDecimal value) {
        this.pnrPaxId = value;
    }

    /**
     * Gets the value of the boardPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPoint() {
        return boardPoint;
    }

    /**
     * Sets the value of the boardPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPoint(String value) {
        this.boardPoint = value;
    }

    /**
     * Gets the value of the departureDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getDepartureDate() {
        return departureDate;
    }

    /**
     * Sets the value of the departureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setDepartureDate(QueryableDateTime value) {
        this.departureDate = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureTime(String value) {
        this.departureTime = value;
    }

    /**
     * Gets the value of the offPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffPoint() {
        return offPoint;
    }

    /**
     * Sets the value of the offPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffPoint(String value) {
        this.offPoint = value;
    }

    /**
     * Gets the value of the offDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getOffDate() {
        return offDate;
    }

    /**
     * Sets the value of the offDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setOffDate(QueryableDateTime value) {
        this.offDate = value;
    }

    /**
     * Gets the value of the arrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets the value of the arrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalTime(String value) {
        this.arrivalTime = value;
    }

    /**
     * Gets the value of the utcDeparture property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getUtcDeparture() {
        return utcDeparture;
    }

    /**
     * Sets the value of the utcDeparture property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setUtcDeparture(QueryableDateTime value) {
        this.utcDeparture = value;
    }

    /**
     * Gets the value of the bookingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * Sets the value of the bookingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingStatus(String value) {
        this.bookingStatus = value;
    }

    /**
     * Gets the value of the eTicket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getETicket() {
        return eTicket;
    }

    /**
     * Sets the value of the eTicket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setETicket(String value) {
        this.eTicket = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the paxName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaxName() {
        return paxName;
    }

    /**
     * Sets the value of the paxName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaxName(String value) {
        this.paxName = value;
    }

    /**
     * Gets the value of the paxIndividualName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaxIndividualName() {
        return paxIndividualName;
    }

    /**
     * Sets the value of the paxIndividualName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaxIndividualName(String value) {
        this.paxIndividualName = value;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

}
