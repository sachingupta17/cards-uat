
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfAchieve complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAchieve">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Achieve" type="{http://skywards.com/Mercator.CRIS.WS}Achieve" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAchieve", propOrder = {
    "achieve"
})
public class ArrayOfAchieve {

    @XmlElement(name = "Achieve", nillable = true)
    protected List<Achieve> achieve;

    /**
     * Gets the value of the achieve property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the achieve property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAchieve().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Achieve }
     * 
     * 
     */
    public List<Achieve> getAchieve() {
        if (achieve == null) {
            achieve = new ArrayList<Achieve>();
        }
        return this.achieve;
    }

}
