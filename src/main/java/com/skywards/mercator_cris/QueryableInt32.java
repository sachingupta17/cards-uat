
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryableInt32 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryableInt32">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IsQuery" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="QueryString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContainsQueryOperators" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="HasValue" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryableInt32", propOrder = {
    "isQuery",
    "queryString",
    "containsQueryOperators",
    "hasValue",
    "value"
})
public class QueryableInt32 {

    @XmlElement(name = "IsQuery")
    protected boolean isQuery;
    @XmlElement(name = "QueryString")
    protected String queryString;
    @XmlElement(name = "ContainsQueryOperators")
    protected boolean containsQueryOperators;
    @XmlElement(name = "HasValue")
    protected boolean hasValue;
    @XmlElement(name = "Value")
    protected int value;

    /**
     * Gets the value of the isQuery property.
     * 
     */
    public boolean isIsQuery() {
        return isQuery;
    }

    /**
     * Sets the value of the isQuery property.
     * 
     */
    public void setIsQuery(boolean value) {
        this.isQuery = value;
    }

    /**
     * Gets the value of the queryString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryString() {
        return queryString;
    }

    /**
     * Sets the value of the queryString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryString(String value) {
        this.queryString = value;
    }

    /**
     * Gets the value of the containsQueryOperators property.
     * 
     */
    public boolean isContainsQueryOperators() {
        return containsQueryOperators;
    }

    /**
     * Sets the value of the containsQueryOperators property.
     * 
     */
    public void setContainsQueryOperators(boolean value) {
        this.containsQueryOperators = value;
    }

    /**
     * Gets the value of the hasValue property.
     * 
     */
    public boolean isHasValue() {
        return hasValue;
    }

    /**
     * Sets the value of the hasValue property.
     * 
     */
    public void setHasValue(boolean value) {
        this.hasValue = value;
    }

    /**
     * Gets the value of the value property.
     * 
     */
    public int getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     */
    public void setValue(int value) {
        this.value = value;
    }

}
