
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Member complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Member">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PersonID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PasswordHint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActiveCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FwyMember" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WebMember" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryOfResidence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PreferencesChanged" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="MemUID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FamilyRelationship" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DayOfDob" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="MonthOfDob" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="YearOfDob" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PreferredAirlineCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PreferredLanguage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PcoCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnsubscribeMail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvalidMail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobDesignation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StaffNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WebActive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FamilyStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SecondaryFamHead" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendFFPMail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendNonFFPMail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PointsBalanace" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="TierPointsBalance" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MergedToMemUID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CorrespondViaEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecommendersCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MothersMaidenName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActiveStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FormalSal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InformalSal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnvelopeSal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArabicSal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ImpRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateJoined" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="PersonRowID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompanyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumberOfFamilyMember" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="EnrolmentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IntCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrsCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EcmCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PostalStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Department" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LptCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NickName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Honours" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PmpType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GroupingNumber" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="OgranisationID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="TermDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="NonExpEnd" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="UpgradeProhib" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="DgradeProhib" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="LastDgradeReviewDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="LastExp" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="WelcomeLetterDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="EubID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="DealCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumberOfEmployees" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="IntyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyW" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyJ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NccCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PostalStatusChangeReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PerUserCreated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PerCreatedDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="PerUserChanged" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PmpUserCreated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PmpCreatedDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="PmpUserChanged" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UploadID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="StatusEnhancement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfilingCampaignCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccOnlineStatusDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="MergedCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlagQualifyRuleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlagCustomerRelation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlagProspect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlagSiteRegistered" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccessToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsProspect" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsMinorEnrolment" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="RegistrationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PreferredCardName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OtherMemberCardNo" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="FamilyHeadMemUID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CRContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Weight" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="DuplicateId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="ForceDuplicateEnrolment" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PcoCodeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NominatedDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="NumberOfPendingActivities" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="AuthorisedPerson" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GuardianMatchRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlyingCoAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsAdmin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrgOrgId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="CorpMemUid" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="CorpPointsBal" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="CorpNoOfMembersLinked" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="CorpMaxMembers" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CorpCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CorpJoinDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="NationalityDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PreferredLanguageDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemberScore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemberId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FamilyHeadCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FamilyHeadPreferredCardName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AnniversaryDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="AlternateEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastLoginDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="LoginSuccess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NoOfAttempts" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FamilyAccStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPrimaryContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemProgram" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsMobileVerified" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsEmailVerified" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FamilyHeadPersonID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="FamilyHeadEmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CorpLinkedDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Member", propOrder = {
    "personID",
    "username",
    "password",
    "passwordHint",
    "activeCardNo",
    "fwyMember",
    "webMember",
    "firstName",
    "tier",
    "countryOfResidence",
    "preferencesChanged",
    "memUID",
    "familyRelationship",
    "title",
    "middleName",
    "lastName",
    "birthDate",
    "dayOfDob",
    "monthOfDob",
    "yearOfDob",
    "preferredAirlineCode",
    "nationality",
    "emailAddress",
    "preferredLanguage",
    "gender",
    "pcoCode",
    "unsubscribeMail",
    "invalidMail",
    "jobDesignation",
    "staffNumber",
    "webActive",
    "familyStatus",
    "secondaryFamHead",
    "sendFFPMail",
    "sendNonFFPMail",
    "pointsBalanace",
    "tierPointsBalance",
    "mergedToMemUID",
    "correspondViaEmail",
    "recommendersCardNo",
    "nameSuffix",
    "mothersMaidenName",
    "activeStatus",
    "formalSal",
    "informalSal",
    "envelopeSal",
    "arabicSal",
    "impRating",
    "dateJoined",
    "personRowID",
    "jobTitle",
    "companyName",
    "mobileNumber",
    "numberOfFamilyMember",
    "enrolmentMethod",
    "intCode",
    "prsCode",
    "ecmCode",
    "postalStatus",
    "department",
    "lptCode",
    "nickName",
    "honours",
    "pmpType",
    "groupingNumber",
    "ogranisationID",
    "termDate",
    "nonExpEnd",
    "upgradeProhib",
    "dgradeProhib",
    "lastDgradeReviewDate",
    "lastExp",
    "welcomeLetterDate",
    "eubID",
    "dealCode",
    "numberOfEmployees",
    "intyCode",
    "policyY",
    "policyW",
    "policyJ",
    "agentCode",
    "nccCode",
    "postalStatusChangeReason",
    "perUserCreated",
    "perCreatedDate",
    "perUserChanged",
    "pmpUserCreated",
    "pmpCreatedDate",
    "pmpUserChanged",
    "uploadID",
    "statusEnhancement",
    "profilingCampaignCode",
    "accOnlineStatusDate",
    "mergedCardNo",
    "flagQualifyRuleCode",
    "flagCustomerRelation",
    "flagProspect",
    "flagSiteRegistered",
    "errorMessage",
    "accessToken",
    "isProspect",
    "isMinorEnrolment",
    "registrationType",
    "preferredCardName",
    "otherMemberCardNo",
    "familyHeadMemUID",
    "crContact",
    "weight",
    "duplicateId",
    "forceDuplicateEnrolment",
    "pcoCodeDesc",
    "nominatedDate",
    "numberOfPendingActivities",
    "authorisedPerson",
    "guardianMatchRule",
    "flyingCoAccount",
    "isAdmin",
    "orgOrgId",
    "corpMemUid",
    "corpPointsBal",
    "corpNoOfMembersLinked",
    "corpMaxMembers",
    "corpCardNo",
    "corpName",
    "corpJoinDate",
    "nationalityDescription",
    "preferredLanguageDescription",
    "memberScore",
    "memberId",
    "familyHeadCardNo",
    "familyHeadPreferredCardName",
    "anniversaryDate",
    "alternateEmail",
    "lastLoginDate",
    "loginSuccess",
    "noOfAttempts",
    "familyAccStatus",
    "isPrimaryContact",
    "memProgram",
    "isMobileVerified",
    "isEmailVerified",
    "familyHeadPersonID",
    "familyHeadEmailAddress",
    "corpLinkedDate"
})
public class Member {

    @XmlElement(name = "PersonID")
    protected int personID;
    @XmlElement(name = "Username")
    protected String username;
    @XmlElement(name = "Password")
    protected String password;
    @XmlElement(name = "PasswordHint")
    protected String passwordHint;
    @XmlElement(name = "ActiveCardNo")
    protected String activeCardNo;
    @XmlElement(name = "FwyMember")
    protected String fwyMember;
    @XmlElement(name = "WebMember")
    protected String webMember;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "Tier")
    protected String tier;
    @XmlElement(name = "CountryOfResidence")
    protected String countryOfResidence;
    @XmlElement(name = "PreferencesChanged")
    protected boolean preferencesChanged;
    @XmlElement(name = "MemUID")
    protected int memUID;
    @XmlElement(name = "FamilyRelationship")
    protected String familyRelationship;
    @XmlElement(name = "Title")
    protected String title;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "BirthDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar birthDate;
    @XmlElement(name = "DayOfDob", required = true)
    protected QueryableInt32 dayOfDob;
    @XmlElement(name = "MonthOfDob", required = true)
    protected QueryableInt32 monthOfDob;
    @XmlElement(name = "YearOfDob", required = true)
    protected QueryableInt32 yearOfDob;
    @XmlElement(name = "PreferredAirlineCode")
    protected String preferredAirlineCode;
    @XmlElement(name = "Nationality")
    protected String nationality;
    @XmlElement(name = "EmailAddress")
    protected String emailAddress;
    @XmlElement(name = "PreferredLanguage")
    protected int preferredLanguage;
    @XmlElement(name = "Gender")
    protected String gender;
    @XmlElement(name = "PcoCode")
    protected String pcoCode;
    @XmlElement(name = "UnsubscribeMail")
    protected String unsubscribeMail;
    @XmlElement(name = "InvalidMail")
    protected String invalidMail;
    @XmlElement(name = "JobDesignation")
    protected String jobDesignation;
    @XmlElement(name = "StaffNumber")
    protected String staffNumber;
    @XmlElement(name = "WebActive")
    protected String webActive;
    @XmlElement(name = "FamilyStatus")
    protected String familyStatus;
    @XmlElement(name = "SecondaryFamHead")
    protected String secondaryFamHead;
    @XmlElement(name = "SendFFPMail")
    protected String sendFFPMail;
    @XmlElement(name = "SendNonFFPMail")
    protected String sendNonFFPMail;
    @XmlElement(name = "PointsBalanace")
    protected long pointsBalanace;
    @XmlElement(name = "TierPointsBalance")
    protected int tierPointsBalance;
    @XmlElement(name = "MergedToMemUID")
    protected int mergedToMemUID;
    @XmlElement(name = "CorrespondViaEmail")
    protected String correspondViaEmail;
    @XmlElement(name = "RecommendersCardNo")
    protected String recommendersCardNo;
    @XmlElement(name = "NameSuffix")
    protected String nameSuffix;
    @XmlElement(name = "MothersMaidenName")
    protected String mothersMaidenName;
    @XmlElement(name = "ActiveStatus")
    protected String activeStatus;
    @XmlElement(name = "FormalSal")
    protected String formalSal;
    @XmlElement(name = "InformalSal")
    protected String informalSal;
    @XmlElement(name = "EnvelopeSal")
    protected String envelopeSal;
    @XmlElement(name = "ArabicSal")
    protected String arabicSal;
    @XmlElement(name = "ImpRating")
    protected String impRating;
    @XmlElement(name = "DateJoined", required = true)
    protected QueryableDateTime dateJoined;
    @XmlElement(name = "PersonRowID")
    protected String personRowID;
    @XmlElement(name = "JobTitle")
    protected String jobTitle;
    @XmlElement(name = "CompanyName")
    protected String companyName;
    @XmlElement(name = "MobileNumber")
    protected String mobileNumber;
    @XmlElement(name = "NumberOfFamilyMember", required = true)
    protected QueryableInt32 numberOfFamilyMember;
    @XmlElement(name = "EnrolmentMethod")
    protected String enrolmentMethod;
    @XmlElement(name = "IntCode")
    protected String intCode;
    @XmlElement(name = "PrsCode")
    protected String prsCode;
    @XmlElement(name = "EcmCode")
    protected String ecmCode;
    @XmlElement(name = "PostalStatus")
    protected String postalStatus;
    @XmlElement(name = "Department")
    protected String department;
    @XmlElement(name = "LptCode")
    protected String lptCode;
    @XmlElement(name = "NickName")
    protected String nickName;
    @XmlElement(name = "Honours")
    protected String honours;
    @XmlElement(name = "PmpType")
    protected String pmpType;
    @XmlElement(name = "GroupingNumber", required = true)
    protected QueryableInt32 groupingNumber;
    @XmlElement(name = "OgranisationID", required = true)
    protected QueryableInt32 ogranisationID;
    @XmlElement(name = "TermDate", required = true)
    protected QueryableDateTime termDate;
    @XmlElement(name = "NonExpEnd", required = true)
    protected QueryableDateTime nonExpEnd;
    @XmlElement(name = "UpgradeProhib", required = true)
    protected QueryableDateTime upgradeProhib;
    @XmlElement(name = "DgradeProhib", required = true)
    protected QueryableDateTime dgradeProhib;
    @XmlElement(name = "LastDgradeReviewDate", required = true)
    protected QueryableDateTime lastDgradeReviewDate;
    @XmlElement(name = "LastExp", required = true)
    protected QueryableDateTime lastExp;
    @XmlElement(name = "WelcomeLetterDate", required = true)
    protected QueryableDateTime welcomeLetterDate;
    @XmlElement(name = "EubID", required = true)
    protected QueryableInt32 eubID;
    @XmlElement(name = "DealCode")
    protected String dealCode;
    @XmlElement(name = "NumberOfEmployees", required = true)
    protected QueryableInt32 numberOfEmployees;
    @XmlElement(name = "IntyCode")
    protected String intyCode;
    @XmlElement(name = "PolicyY")
    protected String policyY;
    @XmlElement(name = "PolicyW")
    protected String policyW;
    @XmlElement(name = "PolicyJ")
    protected String policyJ;
    @XmlElement(name = "AgentCode")
    protected String agentCode;
    @XmlElement(name = "NccCode")
    protected String nccCode;
    @XmlElement(name = "PostalStatusChangeReason")
    protected String postalStatusChangeReason;
    @XmlElement(name = "PerUserCreated")
    protected String perUserCreated;
    @XmlElement(name = "PerCreatedDate", required = true)
    protected QueryableDateTime perCreatedDate;
    @XmlElement(name = "PerUserChanged")
    protected String perUserChanged;
    @XmlElement(name = "PmpUserCreated")
    protected String pmpUserCreated;
    @XmlElement(name = "PmpCreatedDate", required = true)
    protected QueryableDateTime pmpCreatedDate;
    @XmlElement(name = "PmpUserChanged")
    protected String pmpUserChanged;
    @XmlElement(name = "UploadID", required = true)
    protected QueryableInt32 uploadID;
    @XmlElement(name = "StatusEnhancement")
    protected String statusEnhancement;
    @XmlElement(name = "ProfilingCampaignCode")
    protected String profilingCampaignCode;
    @XmlElement(name = "AccOnlineStatusDate", required = true)
    protected QueryableDateTime accOnlineStatusDate;
    @XmlElement(name = "MergedCardNo")
    protected String mergedCardNo;
    @XmlElement(name = "FlagQualifyRuleCode")
    protected String flagQualifyRuleCode;
    @XmlElement(name = "FlagCustomerRelation")
    protected String flagCustomerRelation;
    @XmlElement(name = "FlagProspect")
    protected String flagProspect;
    @XmlElement(name = "FlagSiteRegistered")
    protected String flagSiteRegistered;
    @XmlElement(name = "ErrorMessage")
    protected String errorMessage;
    @XmlElement(name = "AccessToken")
    protected String accessToken;
    @XmlElement(name = "IsProspect")
    protected boolean isProspect;
    @XmlElement(name = "IsMinorEnrolment")
    protected boolean isMinorEnrolment;
    @XmlElement(name = "RegistrationType")
    protected String registrationType;
    @XmlElement(name = "PreferredCardName")
    protected String preferredCardName;
    @XmlElement(name = "OtherMemberCardNo", required = true)
    protected QueryableInt32 otherMemberCardNo;
    @XmlElement(name = "FamilyHeadMemUID")
    protected int familyHeadMemUID;
    @XmlElement(name = "CRContact")
    protected String crContact;
    @XmlElement(name = "Weight", required = true)
    protected QueryableInt32 weight;
    @XmlElement(name = "DuplicateId", required = true)
    protected QueryableInt32 duplicateId;
    @XmlElement(name = "ForceDuplicateEnrolment")
    protected boolean forceDuplicateEnrolment;
    @XmlElement(name = "PcoCodeDesc")
    protected String pcoCodeDesc;
    @XmlElement(name = "NominatedDate", required = true)
    protected QueryableDateTime nominatedDate;
    @XmlElement(name = "NumberOfPendingActivities", required = true)
    protected QueryableInt32 numberOfPendingActivities;
    @XmlElement(name = "AuthorisedPerson")
    protected String authorisedPerson;
    @XmlElement(name = "GuardianMatchRule")
    protected String guardianMatchRule;
    @XmlElement(name = "FlyingCoAccount")
    protected String flyingCoAccount;
    @XmlElement(name = "IsAdmin")
    protected String isAdmin;
    @XmlElement(name = "OrgOrgId", required = true)
    protected QueryableInt32 orgOrgId;
    @XmlElement(name = "CorpMemUid", required = true)
    protected QueryableInt32 corpMemUid;
    @XmlElement(name = "CorpPointsBal", required = true)
    protected QueryableInt32 corpPointsBal;
    @XmlElement(name = "CorpNoOfMembersLinked", required = true)
    protected QueryableInt32 corpNoOfMembersLinked;
    @XmlElement(name = "CorpMaxMembers")
    protected String corpMaxMembers;
    @XmlElement(name = "CorpCardNo")
    protected String corpCardNo;
    @XmlElement(name = "CorpName")
    protected String corpName;
    @XmlElement(name = "CorpJoinDate", required = true)
    protected QueryableDateTime corpJoinDate;
    @XmlElement(name = "NationalityDescription")
    protected String nationalityDescription;
    @XmlElement(name = "PreferredLanguageDescription")
    protected String preferredLanguageDescription;
    @XmlElement(name = "MemberScore")
    protected String memberScore;
    @XmlElement(name = "MemberId")
    protected String memberId;
    @XmlElement(name = "FamilyHeadCardNo")
    protected String familyHeadCardNo;
    @XmlElement(name = "FamilyHeadPreferredCardName")
    protected String familyHeadPreferredCardName;
    @XmlElement(name = "AnniversaryDate", required = true)
    protected QueryableDateTime anniversaryDate;
    @XmlElement(name = "AlternateEmail")
    protected String alternateEmail;
    @XmlElement(name = "LastLoginDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastLoginDate;
    @XmlElement(name = "LoginSuccess")
    protected String loginSuccess;
    @XmlElement(name = "NoOfAttempts")
    protected int noOfAttempts;
    @XmlElement(name = "FamilyAccStatus")
    protected String familyAccStatus;
    @XmlElement(name = "IsPrimaryContact")
    protected String isPrimaryContact;
    @XmlElement(name = "MemProgram")
    protected String memProgram;
    @XmlElement(name = "IsMobileVerified")
    protected String isMobileVerified;
    @XmlElement(name = "IsEmailVerified")
    protected String isEmailVerified;
    @XmlElement(name = "FamilyHeadPersonID", required = true)
    protected QueryableInt32 familyHeadPersonID;
    @XmlElement(name = "FamilyHeadEmailAddress")
    protected String familyHeadEmailAddress;
    @XmlElement(name = "CorpLinkedDate", required = true)
    protected QueryableDateTime corpLinkedDate;

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the passwordHint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasswordHint() {
        return passwordHint;
    }

    /**
     * Sets the value of the passwordHint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasswordHint(String value) {
        this.passwordHint = value;
    }

    /**
     * Gets the value of the activeCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveCardNo() {
        return activeCardNo;
    }

    /**
     * Sets the value of the activeCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveCardNo(String value) {
        this.activeCardNo = value;
    }

    /**
     * Gets the value of the fwyMember property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFwyMember() {
        return fwyMember;
    }

    /**
     * Sets the value of the fwyMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFwyMember(String value) {
        this.fwyMember = value;
    }

    /**
     * Gets the value of the webMember property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebMember() {
        return webMember;
    }

    /**
     * Sets the value of the webMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebMember(String value) {
        this.webMember = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the tier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTier() {
        return tier;
    }

    /**
     * Sets the value of the tier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTier(String value) {
        this.tier = value;
    }

    /**
     * Gets the value of the countryOfResidence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    /**
     * Sets the value of the countryOfResidence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfResidence(String value) {
        this.countryOfResidence = value;
    }

    /**
     * Gets the value of the preferencesChanged property.
     * 
     */
    public boolean isPreferencesChanged() {
        return preferencesChanged;
    }

    /**
     * Sets the value of the preferencesChanged property.
     * 
     */
    public void setPreferencesChanged(boolean value) {
        this.preferencesChanged = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     */
    public int getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     */
    public void setMemUID(int value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the familyRelationship property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyRelationship() {
        return familyRelationship;
    }

    /**
     * Sets the value of the familyRelationship property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyRelationship(String value) {
        this.familyRelationship = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDate(XMLGregorianCalendar value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the dayOfDob property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getDayOfDob() {
        return dayOfDob;
    }

    /**
     * Sets the value of the dayOfDob property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setDayOfDob(QueryableInt32 value) {
        this.dayOfDob = value;
    }

    /**
     * Gets the value of the monthOfDob property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getMonthOfDob() {
        return monthOfDob;
    }

    /**
     * Sets the value of the monthOfDob property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setMonthOfDob(QueryableInt32 value) {
        this.monthOfDob = value;
    }

    /**
     * Gets the value of the yearOfDob property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getYearOfDob() {
        return yearOfDob;
    }

    /**
     * Sets the value of the yearOfDob property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setYearOfDob(QueryableInt32 value) {
        this.yearOfDob = value;
    }

    /**
     * Gets the value of the preferredAirlineCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredAirlineCode() {
        return preferredAirlineCode;
    }

    /**
     * Sets the value of the preferredAirlineCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredAirlineCode(String value) {
        this.preferredAirlineCode = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the preferredLanguage property.
     * 
     */
    public int getPreferredLanguage() {
        return preferredLanguage;
    }

    /**
     * Sets the value of the preferredLanguage property.
     * 
     */
    public void setPreferredLanguage(int value) {
        this.preferredLanguage = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the pcoCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPcoCode() {
        return pcoCode;
    }

    /**
     * Sets the value of the pcoCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPcoCode(String value) {
        this.pcoCode = value;
    }

    /**
     * Gets the value of the unsubscribeMail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnsubscribeMail() {
        return unsubscribeMail;
    }

    /**
     * Sets the value of the unsubscribeMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnsubscribeMail(String value) {
        this.unsubscribeMail = value;
    }

    /**
     * Gets the value of the invalidMail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvalidMail() {
        return invalidMail;
    }

    /**
     * Sets the value of the invalidMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvalidMail(String value) {
        this.invalidMail = value;
    }

    /**
     * Gets the value of the jobDesignation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobDesignation() {
        return jobDesignation;
    }

    /**
     * Sets the value of the jobDesignation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobDesignation(String value) {
        this.jobDesignation = value;
    }

    /**
     * Gets the value of the staffNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStaffNumber() {
        return staffNumber;
    }

    /**
     * Sets the value of the staffNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStaffNumber(String value) {
        this.staffNumber = value;
    }

    /**
     * Gets the value of the webActive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebActive() {
        return webActive;
    }

    /**
     * Sets the value of the webActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebActive(String value) {
        this.webActive = value;
    }

    /**
     * Gets the value of the familyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyStatus() {
        return familyStatus;
    }

    /**
     * Sets the value of the familyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyStatus(String value) {
        this.familyStatus = value;
    }

    /**
     * Gets the value of the secondaryFamHead property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryFamHead() {
        return secondaryFamHead;
    }

    /**
     * Sets the value of the secondaryFamHead property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryFamHead(String value) {
        this.secondaryFamHead = value;
    }

    /**
     * Gets the value of the sendFFPMail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendFFPMail() {
        return sendFFPMail;
    }

    /**
     * Sets the value of the sendFFPMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendFFPMail(String value) {
        this.sendFFPMail = value;
    }

    /**
     * Gets the value of the sendNonFFPMail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendNonFFPMail() {
        return sendNonFFPMail;
    }

    /**
     * Sets the value of the sendNonFFPMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendNonFFPMail(String value) {
        this.sendNonFFPMail = value;
    }

    /**
     * Gets the value of the pointsBalanace property.
     * 
     */
    public long getPointsBalanace() {
        return pointsBalanace;
    }

    /**
     * Sets the value of the pointsBalanace property.
     * 
     */
    public void setPointsBalanace(long value) {
        this.pointsBalanace = value;
    }

    /**
     * Gets the value of the tierPointsBalance property.
     * 
     */
    public int getTierPointsBalance() {
        return tierPointsBalance;
    }

    /**
     * Sets the value of the tierPointsBalance property.
     * 
     */
    public void setTierPointsBalance(int value) {
        this.tierPointsBalance = value;
    }

    /**
     * Gets the value of the mergedToMemUID property.
     * 
     */
    public int getMergedToMemUID() {
        return mergedToMemUID;
    }

    /**
     * Sets the value of the mergedToMemUID property.
     * 
     */
    public void setMergedToMemUID(int value) {
        this.mergedToMemUID = value;
    }

    /**
     * Gets the value of the correspondViaEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrespondViaEmail() {
        return correspondViaEmail;
    }

    /**
     * Sets the value of the correspondViaEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrespondViaEmail(String value) {
        this.correspondViaEmail = value;
    }

    /**
     * Gets the value of the recommendersCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecommendersCardNo() {
        return recommendersCardNo;
    }

    /**
     * Sets the value of the recommendersCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecommendersCardNo(String value) {
        this.recommendersCardNo = value;
    }

    /**
     * Gets the value of the nameSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameSuffix() {
        return nameSuffix;
    }

    /**
     * Sets the value of the nameSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameSuffix(String value) {
        this.nameSuffix = value;
    }

    /**
     * Gets the value of the mothersMaidenName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMothersMaidenName() {
        return mothersMaidenName;
    }

    /**
     * Sets the value of the mothersMaidenName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMothersMaidenName(String value) {
        this.mothersMaidenName = value;
    }

    /**
     * Gets the value of the activeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveStatus() {
        return activeStatus;
    }

    /**
     * Sets the value of the activeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveStatus(String value) {
        this.activeStatus = value;
    }

    /**
     * Gets the value of the formalSal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormalSal() {
        return formalSal;
    }

    /**
     * Sets the value of the formalSal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormalSal(String value) {
        this.formalSal = value;
    }

    /**
     * Gets the value of the informalSal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformalSal() {
        return informalSal;
    }

    /**
     * Sets the value of the informalSal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformalSal(String value) {
        this.informalSal = value;
    }

    /**
     * Gets the value of the envelopeSal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnvelopeSal() {
        return envelopeSal;
    }

    /**
     * Sets the value of the envelopeSal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnvelopeSal(String value) {
        this.envelopeSal = value;
    }

    /**
     * Gets the value of the arabicSal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArabicSal() {
        return arabicSal;
    }

    /**
     * Sets the value of the arabicSal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArabicSal(String value) {
        this.arabicSal = value;
    }

    /**
     * Gets the value of the impRating property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImpRating() {
        return impRating;
    }

    /**
     * Sets the value of the impRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImpRating(String value) {
        this.impRating = value;
    }

    /**
     * Gets the value of the dateJoined property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getDateJoined() {
        return dateJoined;
    }

    /**
     * Sets the value of the dateJoined property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setDateJoined(QueryableDateTime value) {
        this.dateJoined = value;
    }

    /**
     * Gets the value of the personRowID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonRowID() {
        return personRowID;
    }

    /**
     * Sets the value of the personRowID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonRowID(String value) {
        this.personRowID = value;
    }

    /**
     * Gets the value of the jobTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Sets the value of the jobTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobTitle(String value) {
        this.jobTitle = value;
    }

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the numberOfFamilyMember property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getNumberOfFamilyMember() {
        return numberOfFamilyMember;
    }

    /**
     * Sets the value of the numberOfFamilyMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setNumberOfFamilyMember(QueryableInt32 value) {
        this.numberOfFamilyMember = value;
    }

    /**
     * Gets the value of the enrolmentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnrolmentMethod() {
        return enrolmentMethod;
    }

    /**
     * Sets the value of the enrolmentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnrolmentMethod(String value) {
        this.enrolmentMethod = value;
    }

    /**
     * Gets the value of the intCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntCode() {
        return intCode;
    }

    /**
     * Sets the value of the intCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntCode(String value) {
        this.intCode = value;
    }

    /**
     * Gets the value of the prsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrsCode() {
        return prsCode;
    }

    /**
     * Sets the value of the prsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrsCode(String value) {
        this.prsCode = value;
    }

    /**
     * Gets the value of the ecmCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcmCode() {
        return ecmCode;
    }

    /**
     * Sets the value of the ecmCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcmCode(String value) {
        this.ecmCode = value;
    }

    /**
     * Gets the value of the postalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalStatus() {
        return postalStatus;
    }

    /**
     * Sets the value of the postalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalStatus(String value) {
        this.postalStatus = value;
    }

    /**
     * Gets the value of the department property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Sets the value of the department property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartment(String value) {
        this.department = value;
    }

    /**
     * Gets the value of the lptCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLptCode() {
        return lptCode;
    }

    /**
     * Sets the value of the lptCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLptCode(String value) {
        this.lptCode = value;
    }

    /**
     * Gets the value of the nickName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * Sets the value of the nickName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNickName(String value) {
        this.nickName = value;
    }

    /**
     * Gets the value of the honours property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHonours() {
        return honours;
    }

    /**
     * Sets the value of the honours property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHonours(String value) {
        this.honours = value;
    }

    /**
     * Gets the value of the pmpType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPmpType() {
        return pmpType;
    }

    /**
     * Sets the value of the pmpType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPmpType(String value) {
        this.pmpType = value;
    }

    /**
     * Gets the value of the groupingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getGroupingNumber() {
        return groupingNumber;
    }

    /**
     * Sets the value of the groupingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setGroupingNumber(QueryableInt32 value) {
        this.groupingNumber = value;
    }

    /**
     * Gets the value of the ogranisationID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getOgranisationID() {
        return ogranisationID;
    }

    /**
     * Sets the value of the ogranisationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setOgranisationID(QueryableInt32 value) {
        this.ogranisationID = value;
    }

    /**
     * Gets the value of the termDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getTermDate() {
        return termDate;
    }

    /**
     * Sets the value of the termDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setTermDate(QueryableDateTime value) {
        this.termDate = value;
    }

    /**
     * Gets the value of the nonExpEnd property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getNonExpEnd() {
        return nonExpEnd;
    }

    /**
     * Sets the value of the nonExpEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setNonExpEnd(QueryableDateTime value) {
        this.nonExpEnd = value;
    }

    /**
     * Gets the value of the upgradeProhib property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getUpgradeProhib() {
        return upgradeProhib;
    }

    /**
     * Sets the value of the upgradeProhib property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setUpgradeProhib(QueryableDateTime value) {
        this.upgradeProhib = value;
    }

    /**
     * Gets the value of the dgradeProhib property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getDgradeProhib() {
        return dgradeProhib;
    }

    /**
     * Sets the value of the dgradeProhib property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setDgradeProhib(QueryableDateTime value) {
        this.dgradeProhib = value;
    }

    /**
     * Gets the value of the lastDgradeReviewDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getLastDgradeReviewDate() {
        return lastDgradeReviewDate;
    }

    /**
     * Sets the value of the lastDgradeReviewDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setLastDgradeReviewDate(QueryableDateTime value) {
        this.lastDgradeReviewDate = value;
    }

    /**
     * Gets the value of the lastExp property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getLastExp() {
        return lastExp;
    }

    /**
     * Sets the value of the lastExp property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setLastExp(QueryableDateTime value) {
        this.lastExp = value;
    }

    /**
     * Gets the value of the welcomeLetterDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getWelcomeLetterDate() {
        return welcomeLetterDate;
    }

    /**
     * Sets the value of the welcomeLetterDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setWelcomeLetterDate(QueryableDateTime value) {
        this.welcomeLetterDate = value;
    }

    /**
     * Gets the value of the eubID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getEubID() {
        return eubID;
    }

    /**
     * Sets the value of the eubID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setEubID(QueryableInt32 value) {
        this.eubID = value;
    }

    /**
     * Gets the value of the dealCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealCode() {
        return dealCode;
    }

    /**
     * Sets the value of the dealCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealCode(String value) {
        this.dealCode = value;
    }

    /**
     * Gets the value of the numberOfEmployees property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getNumberOfEmployees() {
        return numberOfEmployees;
    }

    /**
     * Sets the value of the numberOfEmployees property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setNumberOfEmployees(QueryableInt32 value) {
        this.numberOfEmployees = value;
    }

    /**
     * Gets the value of the intyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntyCode() {
        return intyCode;
    }

    /**
     * Sets the value of the intyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntyCode(String value) {
        this.intyCode = value;
    }

    /**
     * Gets the value of the policyY property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyY() {
        return policyY;
    }

    /**
     * Sets the value of the policyY property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyY(String value) {
        this.policyY = value;
    }

    /**
     * Gets the value of the policyW property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyW() {
        return policyW;
    }

    /**
     * Sets the value of the policyW property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyW(String value) {
        this.policyW = value;
    }

    /**
     * Gets the value of the policyJ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyJ() {
        return policyJ;
    }

    /**
     * Sets the value of the policyJ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyJ(String value) {
        this.policyJ = value;
    }

    /**
     * Gets the value of the agentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentCode() {
        return agentCode;
    }

    /**
     * Sets the value of the agentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentCode(String value) {
        this.agentCode = value;
    }

    /**
     * Gets the value of the nccCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNccCode() {
        return nccCode;
    }

    /**
     * Sets the value of the nccCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNccCode(String value) {
        this.nccCode = value;
    }

    /**
     * Gets the value of the postalStatusChangeReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalStatusChangeReason() {
        return postalStatusChangeReason;
    }

    /**
     * Sets the value of the postalStatusChangeReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalStatusChangeReason(String value) {
        this.postalStatusChangeReason = value;
    }

    /**
     * Gets the value of the perUserCreated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerUserCreated() {
        return perUserCreated;
    }

    /**
     * Sets the value of the perUserCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerUserCreated(String value) {
        this.perUserCreated = value;
    }

    /**
     * Gets the value of the perCreatedDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getPerCreatedDate() {
        return perCreatedDate;
    }

    /**
     * Sets the value of the perCreatedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setPerCreatedDate(QueryableDateTime value) {
        this.perCreatedDate = value;
    }

    /**
     * Gets the value of the perUserChanged property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerUserChanged() {
        return perUserChanged;
    }

    /**
     * Sets the value of the perUserChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerUserChanged(String value) {
        this.perUserChanged = value;
    }

    /**
     * Gets the value of the pmpUserCreated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPmpUserCreated() {
        return pmpUserCreated;
    }

    /**
     * Sets the value of the pmpUserCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPmpUserCreated(String value) {
        this.pmpUserCreated = value;
    }

    /**
     * Gets the value of the pmpCreatedDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getPmpCreatedDate() {
        return pmpCreatedDate;
    }

    /**
     * Sets the value of the pmpCreatedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setPmpCreatedDate(QueryableDateTime value) {
        this.pmpCreatedDate = value;
    }

    /**
     * Gets the value of the pmpUserChanged property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPmpUserChanged() {
        return pmpUserChanged;
    }

    /**
     * Sets the value of the pmpUserChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPmpUserChanged(String value) {
        this.pmpUserChanged = value;
    }

    /**
     * Gets the value of the uploadID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getUploadID() {
        return uploadID;
    }

    /**
     * Sets the value of the uploadID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setUploadID(QueryableInt32 value) {
        this.uploadID = value;
    }

    /**
     * Gets the value of the statusEnhancement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusEnhancement() {
        return statusEnhancement;
    }

    /**
     * Sets the value of the statusEnhancement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusEnhancement(String value) {
        this.statusEnhancement = value;
    }

    /**
     * Gets the value of the profilingCampaignCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfilingCampaignCode() {
        return profilingCampaignCode;
    }

    /**
     * Sets the value of the profilingCampaignCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfilingCampaignCode(String value) {
        this.profilingCampaignCode = value;
    }

    /**
     * Gets the value of the accOnlineStatusDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getAccOnlineStatusDate() {
        return accOnlineStatusDate;
    }

    /**
     * Sets the value of the accOnlineStatusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setAccOnlineStatusDate(QueryableDateTime value) {
        this.accOnlineStatusDate = value;
    }

    /**
     * Gets the value of the mergedCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergedCardNo() {
        return mergedCardNo;
    }

    /**
     * Sets the value of the mergedCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergedCardNo(String value) {
        this.mergedCardNo = value;
    }

    /**
     * Gets the value of the flagQualifyRuleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagQualifyRuleCode() {
        return flagQualifyRuleCode;
    }

    /**
     * Sets the value of the flagQualifyRuleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagQualifyRuleCode(String value) {
        this.flagQualifyRuleCode = value;
    }

    /**
     * Gets the value of the flagCustomerRelation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagCustomerRelation() {
        return flagCustomerRelation;
    }

    /**
     * Sets the value of the flagCustomerRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagCustomerRelation(String value) {
        this.flagCustomerRelation = value;
    }

    /**
     * Gets the value of the flagProspect property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagProspect() {
        return flagProspect;
    }

    /**
     * Sets the value of the flagProspect property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagProspect(String value) {
        this.flagProspect = value;
    }

    /**
     * Gets the value of the flagSiteRegistered property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagSiteRegistered() {
        return flagSiteRegistered;
    }

    /**
     * Sets the value of the flagSiteRegistered property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagSiteRegistered(String value) {
        this.flagSiteRegistered = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

    /**
     * Gets the value of the accessToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * Sets the value of the accessToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessToken(String value) {
        this.accessToken = value;
    }

    /**
     * Gets the value of the isProspect property.
     * 
     */
    public boolean isIsProspect() {
        return isProspect;
    }

    /**
     * Sets the value of the isProspect property.
     * 
     */
    public void setIsProspect(boolean value) {
        this.isProspect = value;
    }

    /**
     * Gets the value of the isMinorEnrolment property.
     * 
     */
    public boolean isIsMinorEnrolment() {
        return isMinorEnrolment;
    }

    /**
     * Sets the value of the isMinorEnrolment property.
     * 
     */
    public void setIsMinorEnrolment(boolean value) {
        this.isMinorEnrolment = value;
    }

    /**
     * Gets the value of the registrationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationType() {
        return registrationType;
    }

    /**
     * Sets the value of the registrationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationType(String value) {
        this.registrationType = value;
    }

    /**
     * Gets the value of the preferredCardName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredCardName() {
        return preferredCardName;
    }

    /**
     * Sets the value of the preferredCardName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredCardName(String value) {
        this.preferredCardName = value;
    }

    /**
     * Gets the value of the otherMemberCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getOtherMemberCardNo() {
        return otherMemberCardNo;
    }

    /**
     * Sets the value of the otherMemberCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setOtherMemberCardNo(QueryableInt32 value) {
        this.otherMemberCardNo = value;
    }

    /**
     * Gets the value of the familyHeadMemUID property.
     * 
     */
    public int getFamilyHeadMemUID() {
        return familyHeadMemUID;
    }

    /**
     * Sets the value of the familyHeadMemUID property.
     * 
     */
    public void setFamilyHeadMemUID(int value) {
        this.familyHeadMemUID = value;
    }

    /**
     * Gets the value of the crContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRContact() {
        return crContact;
    }

    /**
     * Sets the value of the crContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRContact(String value) {
        this.crContact = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setWeight(QueryableInt32 value) {
        this.weight = value;
    }

    /**
     * Gets the value of the duplicateId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getDuplicateId() {
        return duplicateId;
    }

    /**
     * Sets the value of the duplicateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setDuplicateId(QueryableInt32 value) {
        this.duplicateId = value;
    }

    /**
     * Gets the value of the forceDuplicateEnrolment property.
     * 
     */
    public boolean isForceDuplicateEnrolment() {
        return forceDuplicateEnrolment;
    }

    /**
     * Sets the value of the forceDuplicateEnrolment property.
     * 
     */
    public void setForceDuplicateEnrolment(boolean value) {
        this.forceDuplicateEnrolment = value;
    }

    /**
     * Gets the value of the pcoCodeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPcoCodeDesc() {
        return pcoCodeDesc;
    }

    /**
     * Sets the value of the pcoCodeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPcoCodeDesc(String value) {
        this.pcoCodeDesc = value;
    }

    /**
     * Gets the value of the nominatedDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getNominatedDate() {
        return nominatedDate;
    }

    /**
     * Sets the value of the nominatedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setNominatedDate(QueryableDateTime value) {
        this.nominatedDate = value;
    }

    /**
     * Gets the value of the numberOfPendingActivities property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getNumberOfPendingActivities() {
        return numberOfPendingActivities;
    }

    /**
     * Sets the value of the numberOfPendingActivities property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setNumberOfPendingActivities(QueryableInt32 value) {
        this.numberOfPendingActivities = value;
    }

    /**
     * Gets the value of the authorisedPerson property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorisedPerson() {
        return authorisedPerson;
    }

    /**
     * Sets the value of the authorisedPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorisedPerson(String value) {
        this.authorisedPerson = value;
    }

    /**
     * Gets the value of the guardianMatchRule property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuardianMatchRule() {
        return guardianMatchRule;
    }

    /**
     * Sets the value of the guardianMatchRule property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuardianMatchRule(String value) {
        this.guardianMatchRule = value;
    }

    /**
     * Gets the value of the flyingCoAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlyingCoAccount() {
        return flyingCoAccount;
    }

    /**
     * Sets the value of the flyingCoAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlyingCoAccount(String value) {
        this.flyingCoAccount = value;
    }

    /**
     * Gets the value of the isAdmin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAdmin() {
        return isAdmin;
    }

    /**
     * Sets the value of the isAdmin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAdmin(String value) {
        this.isAdmin = value;
    }

    /**
     * Gets the value of the orgOrgId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getOrgOrgId() {
        return orgOrgId;
    }

    /**
     * Sets the value of the orgOrgId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setOrgOrgId(QueryableInt32 value) {
        this.orgOrgId = value;
    }

    /**
     * Gets the value of the corpMemUid property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getCorpMemUid() {
        return corpMemUid;
    }

    /**
     * Sets the value of the corpMemUid property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setCorpMemUid(QueryableInt32 value) {
        this.corpMemUid = value;
    }

    /**
     * Gets the value of the corpPointsBal property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getCorpPointsBal() {
        return corpPointsBal;
    }

    /**
     * Sets the value of the corpPointsBal property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setCorpPointsBal(QueryableInt32 value) {
        this.corpPointsBal = value;
    }

    /**
     * Gets the value of the corpNoOfMembersLinked property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getCorpNoOfMembersLinked() {
        return corpNoOfMembersLinked;
    }

    /**
     * Sets the value of the corpNoOfMembersLinked property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setCorpNoOfMembersLinked(QueryableInt32 value) {
        this.corpNoOfMembersLinked = value;
    }

    /**
     * Gets the value of the corpMaxMembers property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpMaxMembers() {
        return corpMaxMembers;
    }

    /**
     * Sets the value of the corpMaxMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpMaxMembers(String value) {
        this.corpMaxMembers = value;
    }

    /**
     * Gets the value of the corpCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpCardNo() {
        return corpCardNo;
    }

    /**
     * Sets the value of the corpCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpCardNo(String value) {
        this.corpCardNo = value;
    }

    /**
     * Gets the value of the corpName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpName() {
        return corpName;
    }

    /**
     * Sets the value of the corpName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpName(String value) {
        this.corpName = value;
    }

    /**
     * Gets the value of the corpJoinDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getCorpJoinDate() {
        return corpJoinDate;
    }

    /**
     * Sets the value of the corpJoinDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setCorpJoinDate(QueryableDateTime value) {
        this.corpJoinDate = value;
    }

    /**
     * Gets the value of the nationalityDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalityDescription() {
        return nationalityDescription;
    }

    /**
     * Sets the value of the nationalityDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalityDescription(String value) {
        this.nationalityDescription = value;
    }

    /**
     * Gets the value of the preferredLanguageDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredLanguageDescription() {
        return preferredLanguageDescription;
    }

    /**
     * Sets the value of the preferredLanguageDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredLanguageDescription(String value) {
        this.preferredLanguageDescription = value;
    }

    /**
     * Gets the value of the memberScore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberScore() {
        return memberScore;
    }

    /**
     * Sets the value of the memberScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberScore(String value) {
        this.memberScore = value;
    }

    /**
     * Gets the value of the memberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * Sets the value of the memberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberId(String value) {
        this.memberId = value;
    }

    /**
     * Gets the value of the familyHeadCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyHeadCardNo() {
        return familyHeadCardNo;
    }

    /**
     * Sets the value of the familyHeadCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyHeadCardNo(String value) {
        this.familyHeadCardNo = value;
    }

    /**
     * Gets the value of the familyHeadPreferredCardName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyHeadPreferredCardName() {
        return familyHeadPreferredCardName;
    }

    /**
     * Sets the value of the familyHeadPreferredCardName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyHeadPreferredCardName(String value) {
        this.familyHeadPreferredCardName = value;
    }

    /**
     * Gets the value of the anniversaryDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getAnniversaryDate() {
        return anniversaryDate;
    }

    /**
     * Sets the value of the anniversaryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setAnniversaryDate(QueryableDateTime value) {
        this.anniversaryDate = value;
    }

    /**
     * Gets the value of the alternateEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateEmail() {
        return alternateEmail;
    }

    /**
     * Sets the value of the alternateEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateEmail(String value) {
        this.alternateEmail = value;
    }

    /**
     * Gets the value of the lastLoginDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastLoginDate() {
        return lastLoginDate;
    }

    /**
     * Sets the value of the lastLoginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastLoginDate(XMLGregorianCalendar value) {
        this.lastLoginDate = value;
    }

    /**
     * Gets the value of the loginSuccess property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoginSuccess() {
        return loginSuccess;
    }

    /**
     * Sets the value of the loginSuccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoginSuccess(String value) {
        this.loginSuccess = value;
    }

    /**
     * Gets the value of the noOfAttempts property.
     * 
     */
    public int getNoOfAttempts() {
        return noOfAttempts;
    }

    /**
     * Sets the value of the noOfAttempts property.
     * 
     */
    public void setNoOfAttempts(int value) {
        this.noOfAttempts = value;
    }

    /**
     * Gets the value of the familyAccStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyAccStatus() {
        return familyAccStatus;
    }

    /**
     * Sets the value of the familyAccStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyAccStatus(String value) {
        this.familyAccStatus = value;
    }

    /**
     * Gets the value of the isPrimaryContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimaryContact() {
        return isPrimaryContact;
    }

    /**
     * Sets the value of the isPrimaryContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimaryContact(String value) {
        this.isPrimaryContact = value;
    }

    /**
     * Gets the value of the memProgram property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemProgram() {
        return memProgram;
    }

    /**
     * Sets the value of the memProgram property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemProgram(String value) {
        this.memProgram = value;
    }

    /**
     * Gets the value of the isMobileVerified property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMobileVerified() {
        return isMobileVerified;
    }

    /**
     * Sets the value of the isMobileVerified property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMobileVerified(String value) {
        this.isMobileVerified = value;
    }

    /**
     * Gets the value of the isEmailVerified property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsEmailVerified() {
        return isEmailVerified;
    }

    /**
     * Sets the value of the isEmailVerified property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsEmailVerified(String value) {
        this.isEmailVerified = value;
    }

    /**
     * Gets the value of the familyHeadPersonID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getFamilyHeadPersonID() {
        return familyHeadPersonID;
    }

    /**
     * Sets the value of the familyHeadPersonID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setFamilyHeadPersonID(QueryableInt32 value) {
        this.familyHeadPersonID = value;
    }

    /**
     * Gets the value of the familyHeadEmailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyHeadEmailAddress() {
        return familyHeadEmailAddress;
    }

    /**
     * Sets the value of the familyHeadEmailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyHeadEmailAddress(String value) {
        this.familyHeadEmailAddress = value;
    }

    /**
     * Gets the value of the corpLinkedDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getCorpLinkedDate() {
        return corpLinkedDate;
    }

    /**
     * Sets the value of the corpLinkedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setCorpLinkedDate(QueryableDateTime value) {
        this.corpLinkedDate = value;
    }

}
