
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="aliasCardsList" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfAliasCard" minOccurs="0"/>
 *         &lt;element name="updateFlags" type="{http://skywards.com/Mercator.CRIS.WS}UpdateFlags" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "personID",
    "aliasCardsList",
    "updateFlags"
})
@XmlRootElement(name = "UpdateAlaisCards")
public class UpdateAlaisCards {

    protected int personID;
    protected ArrayOfAliasCard aliasCardsList;
    protected UpdateFlags updateFlags;

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the aliasCardsList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAliasCard }
     *     
     */
    public ArrayOfAliasCard getAliasCardsList() {
        return aliasCardsList;
    }

    /**
     * Sets the value of the aliasCardsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAliasCard }
     *     
     */
    public void setAliasCardsList(ArrayOfAliasCard value) {
        this.aliasCardsList = value;
    }

    /**
     * Gets the value of the updateFlags property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateFlags }
     *     
     */
    public UpdateFlags getUpdateFlags() {
        return updateFlags;
    }

    /**
     * Sets the value of the updateFlags property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateFlags }
     *     
     */
    public void setUpdateFlags(UpdateFlags value) {
        this.updateFlags = value;
    }

}
