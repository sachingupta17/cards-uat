
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Achieve complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Achieve">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}BusinessBaseOfAchieve">
 *       &lt;sequence>
 *         &lt;element name="Month" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TierMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Achieve", propOrder = {
    "month",
    "tierMiles"
})
public class Achieve
    extends BusinessBaseOfAchieve
{

    @XmlElement(name = "Month")
    protected String month;
    @XmlElement(name = "TierMiles")
    protected int tierMiles;

    /**
     * Gets the value of the month property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonth() {
        return month;
    }

    /**
     * Sets the value of the month property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonth(String value) {
        this.month = value;
    }

    /**
     * Gets the value of the tierMiles property.
     * 
     */
    public int getTierMiles() {
        return tierMiles;
    }

    /**
     * Sets the value of the tierMiles property.
     * 
     */
    public void setTierMiles(int value) {
        this.tierMiles = value;
    }

}
