
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberActiveCardnumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Personid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Memuid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FamilyMemberStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "memberActiveCardnumber",
    "personid",
    "memuid",
    "familyMemberStatus"
})
@XmlRootElement(name = "UpdateFamilyMemberStatus")
public class UpdateFamilyMemberStatus {

    @XmlElement(name = "MemberActiveCardnumber")
    protected String memberActiveCardnumber;
    @XmlElement(name = "Personid")
    protected int personid;
    @XmlElement(name = "Memuid")
    protected int memuid;
    @XmlElement(name = "FamilyMemberStatus")
    protected String familyMemberStatus;

    /**
     * Gets the value of the memberActiveCardnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberActiveCardnumber() {
        return memberActiveCardnumber;
    }

    /**
     * Sets the value of the memberActiveCardnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberActiveCardnumber(String value) {
        this.memberActiveCardnumber = value;
    }

    /**
     * Gets the value of the personid property.
     * 
     */
    public int getPersonid() {
        return personid;
    }

    /**
     * Sets the value of the personid property.
     * 
     */
    public void setPersonid(int value) {
        this.personid = value;
    }

    /**
     * Gets the value of the memuid property.
     * 
     */
    public int getMemuid() {
        return memuid;
    }

    /**
     * Sets the value of the memuid property.
     * 
     */
    public void setMemuid(int value) {
        this.memuid = value;
    }

    /**
     * Gets the value of the familyMemberStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyMemberStatus() {
        return familyMemberStatus;
    }

    /**
     * Sets the value of the familyMemberStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyMemberStatus(String value) {
        this.familyMemberStatus = value;
    }

}
