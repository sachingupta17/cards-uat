
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfItineryEmailAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfItineryEmailAddress">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItineryEmailAddress" type="{http://skywards.com/Mercator.CRIS.WS}ItineryEmailAddress" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfItineryEmailAddress", propOrder = {
    "itineryEmailAddress"
})
public class ArrayOfItineryEmailAddress {

    @XmlElement(name = "ItineryEmailAddress", nillable = true)
    protected List<ItineryEmailAddress> itineryEmailAddress;

    /**
     * Gets the value of the itineryEmailAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itineryEmailAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItineryEmailAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItineryEmailAddress }
     * 
     * 
     */
    public List<ItineryEmailAddress> getItineryEmailAddress() {
        if (itineryEmailAddress == null) {
            itineryEmailAddress = new ArrayList<ItineryEmailAddress>();
        }
        return this.itineryEmailAddress;
    }

}
