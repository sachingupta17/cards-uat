
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetMemberProfileResult" type="{http://skywards.com/Mercator.CRIS.WS}SkywardsProfile" minOccurs="0"/>
 *         &lt;element name="wsResult" type="{http://skywards.com/Mercator.CRIS.WS}WSIResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getMemberProfileResult",
    "wsResult"
})
@XmlRootElement(name = "GetMemberProfileResponse")
public class GetMemberProfileResponse {

    @XmlElement(name = "GetMemberProfileResult")
    protected SkywardsProfile getMemberProfileResult;
    protected WSIResult wsResult;

    /**
     * Gets the value of the getMemberProfileResult property.
     * 
     * @return
     *     possible object is
     *     {@link SkywardsProfile }
     *     
     */
    public SkywardsProfile getGetMemberProfileResult() {
        return getMemberProfileResult;
    }

    /**
     * Sets the value of the getMemberProfileResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SkywardsProfile }
     *     
     */
    public void setGetMemberProfileResult(SkywardsProfile value) {
        this.getMemberProfileResult = value;
    }

    /**
     * Gets the value of the wsResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSIResult }
     *     
     */
    public WSIResult getWsResult() {
        return wsResult;
    }

    /**
     * Sets the value of the wsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSIResult }
     *     
     */
    public void setWsResult(WSIResult value) {
        this.wsResult = value;
    }

}
