
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TierDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TierDetails">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}BusinessBaseOfTierDetails">
 *       &lt;sequence>
 *         &lt;element name="COLUMN1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="COLUMN2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="COLUMN3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="COLUMN4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="COLUMN5" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="COLUMN6" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SEQNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TierDetails", propOrder = {
    "column1",
    "column2",
    "column3",
    "column4",
    "column5",
    "column6",
    "seqno"
})
public class TierDetails
    extends BusinessBaseOfTierDetails
{

    @XmlElement(name = "COLUMN1")
    protected String column1;
    @XmlElement(name = "COLUMN2")
    protected String column2;
    @XmlElement(name = "COLUMN3")
    protected String column3;
    @XmlElement(name = "COLUMN4")
    protected String column4;
    @XmlElement(name = "COLUMN5")
    protected int column5;
    @XmlElement(name = "COLUMN6")
    protected int column6;
    @XmlElement(name = "SEQNO")
    protected int seqno;

    /**
     * Gets the value of the column1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOLUMN1() {
        return column1;
    }

    /**
     * Sets the value of the column1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOLUMN1(String value) {
        this.column1 = value;
    }

    /**
     * Gets the value of the column2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOLUMN2() {
        return column2;
    }

    /**
     * Sets the value of the column2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOLUMN2(String value) {
        this.column2 = value;
    }

    /**
     * Gets the value of the column3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOLUMN3() {
        return column3;
    }

    /**
     * Sets the value of the column3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOLUMN3(String value) {
        this.column3 = value;
    }

    /**
     * Gets the value of the column4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOLUMN4() {
        return column4;
    }

    /**
     * Sets the value of the column4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOLUMN4(String value) {
        this.column4 = value;
    }

    /**
     * Gets the value of the column5 property.
     * 
     */
    public int getCOLUMN5() {
        return column5;
    }

    /**
     * Sets the value of the column5 property.
     * 
     */
    public void setCOLUMN5(int value) {
        this.column5 = value;
    }

    /**
     * Gets the value of the column6 property.
     * 
     */
    public int getCOLUMN6() {
        return column6;
    }

    /**
     * Sets the value of the column6 property.
     * 
     */
    public void setCOLUMN6(int value) {
        this.column6 = value;
    }

    /**
     * Gets the value of the seqno property.
     * 
     */
    public int getSEQNO() {
        return seqno;
    }

    /**
     * Sets the value of the seqno property.
     * 
     */
    public void setSEQNO(int value) {
        this.seqno = value;
    }

}
