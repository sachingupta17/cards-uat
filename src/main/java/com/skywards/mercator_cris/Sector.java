
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Sector complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Sector">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}CRISReadOnlyBase">
 *       &lt;sequence>
 *         &lt;element name="Ticket_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Coupon_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tkt_Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BonusMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FlightSpecBonus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Is_OND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PersonID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PnrItnID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="AirlineDesignator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirlineName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BoardPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BoardingPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DestinationPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DestinationAirportName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ArrivalTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BookingClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReservationClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BookingStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecordLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RedeemMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TierMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Chauffeur" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceCodes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StopOver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Duration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrBr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sector", propOrder = {
    "ticketNo",
    "couponNo",
    "tktType",
    "bonusMiles",
    "flightSpecBonus",
    "isOND",
    "personID",
    "pnrItnID",
    "airlineDesignator",
    "airlineName",
    "flightNumber",
    "boardPoint",
    "boardingPoint",
    "departureDate",
    "departureTime",
    "destinationPoint",
    "destinationAirportName",
    "arrivalDate",
    "arrivalTime",
    "bookingClass",
    "reservationClass",
    "bookingStatus",
    "recordLocator",
    "redeemMiles",
    "tierMiles",
    "chauffeur",
    "serviceCodes",
    "stopOver",
    "duration",
    "prBr",
    "flightType"
})
public class Sector
    extends CRISReadOnlyBase
{

    @XmlElement(name = "Ticket_No")
    protected String ticketNo;
    @XmlElement(name = "Coupon_No")
    protected String couponNo;
    @XmlElement(name = "Tkt_Type")
    protected String tktType;
    @XmlElement(name = "BonusMiles")
    protected int bonusMiles;
    @XmlElement(name = "FlightSpecBonus")
    protected int flightSpecBonus;
    @XmlElement(name = "Is_OND")
    protected String isOND;
    @XmlElement(name = "PersonID")
    protected int personID;
    @XmlElement(name = "PnrItnID")
    protected long pnrItnID;
    @XmlElement(name = "AirlineDesignator")
    protected String airlineDesignator;
    @XmlElement(name = "AirlineName")
    protected String airlineName;
    @XmlElement(name = "FlightNumber")
    protected String flightNumber;
    @XmlElement(name = "BoardPoint")
    protected String boardPoint;
    @XmlElement(name = "BoardingPoint")
    protected String boardingPoint;
    @XmlElement(name = "DepartureDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar departureDate;
    @XmlElement(name = "DepartureTime")
    protected String departureTime;
    @XmlElement(name = "DestinationPoint")
    protected String destinationPoint;
    @XmlElement(name = "DestinationAirportName")
    protected String destinationAirportName;
    @XmlElement(name = "ArrivalDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar arrivalDate;
    @XmlElement(name = "ArrivalTime")
    protected String arrivalTime;
    @XmlElement(name = "BookingClass")
    protected String bookingClass;
    @XmlElement(name = "ReservationClass")
    protected String reservationClass;
    @XmlElement(name = "BookingStatus")
    protected String bookingStatus;
    @XmlElement(name = "RecordLocator")
    protected String recordLocator;
    @XmlElement(name = "RedeemMiles")
    protected int redeemMiles;
    @XmlElement(name = "TierMiles")
    protected int tierMiles;
    @XmlElement(name = "Chauffeur")
    protected String chauffeur;
    @XmlElement(name = "ServiceCodes")
    protected String serviceCodes;
    @XmlElement(name = "StopOver")
    protected String stopOver;
    @XmlElement(name = "Duration")
    protected String duration;
    @XmlElement(name = "PrBr")
    protected String prBr;
    @XmlElement(name = "FlightType")
    protected String flightType;

    /**
     * Gets the value of the ticketNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNo() {
        return ticketNo;
    }

    /**
     * Sets the value of the ticketNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNo(String value) {
        this.ticketNo = value;
    }

    /**
     * Gets the value of the couponNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponNo() {
        return couponNo;
    }

    /**
     * Sets the value of the couponNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponNo(String value) {
        this.couponNo = value;
    }

    /**
     * Gets the value of the tktType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTktType() {
        return tktType;
    }

    /**
     * Sets the value of the tktType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTktType(String value) {
        this.tktType = value;
    }

    /**
     * Gets the value of the bonusMiles property.
     * 
     */
    public int getBonusMiles() {
        return bonusMiles;
    }

    /**
     * Sets the value of the bonusMiles property.
     * 
     */
    public void setBonusMiles(int value) {
        this.bonusMiles = value;
    }

    /**
     * Gets the value of the flightSpecBonus property.
     * 
     */
    public int getFlightSpecBonus() {
        return flightSpecBonus;
    }

    /**
     * Sets the value of the flightSpecBonus property.
     * 
     */
    public void setFlightSpecBonus(int value) {
        this.flightSpecBonus = value;
    }

    /**
     * Gets the value of the isOND property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOND() {
        return isOND;
    }

    /**
     * Sets the value of the isOND property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOND(String value) {
        this.isOND = value;
    }

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the pnrItnID property.
     * 
     */
    public long getPnrItnID() {
        return pnrItnID;
    }

    /**
     * Sets the value of the pnrItnID property.
     * 
     */
    public void setPnrItnID(long value) {
        this.pnrItnID = value;
    }

    /**
     * Gets the value of the airlineDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineDesignator() {
        return airlineDesignator;
    }

    /**
     * Sets the value of the airlineDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineDesignator(String value) {
        this.airlineDesignator = value;
    }

    /**
     * Gets the value of the airlineName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineName() {
        return airlineName;
    }

    /**
     * Sets the value of the airlineName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineName(String value) {
        this.airlineName = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the boardPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPoint() {
        return boardPoint;
    }

    /**
     * Sets the value of the boardPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPoint(String value) {
        this.boardPoint = value;
    }

    /**
     * Gets the value of the boardingPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardingPoint() {
        return boardingPoint;
    }

    /**
     * Sets the value of the boardingPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardingPoint(String value) {
        this.boardingPoint = value;
    }

    /**
     * Gets the value of the departureDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepartureDate() {
        return departureDate;
    }

    /**
     * Sets the value of the departureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepartureDate(XMLGregorianCalendar value) {
        this.departureDate = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureTime(String value) {
        this.departureTime = value;
    }

    /**
     * Gets the value of the destinationPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationPoint() {
        return destinationPoint;
    }

    /**
     * Sets the value of the destinationPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationPoint(String value) {
        this.destinationPoint = value;
    }

    /**
     * Gets the value of the destinationAirportName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationAirportName() {
        return destinationAirportName;
    }

    /**
     * Sets the value of the destinationAirportName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationAirportName(String value) {
        this.destinationAirportName = value;
    }

    /**
     * Gets the value of the arrivalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getArrivalDate() {
        return arrivalDate;
    }

    /**
     * Sets the value of the arrivalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setArrivalDate(XMLGregorianCalendar value) {
        this.arrivalDate = value;
    }

    /**
     * Gets the value of the arrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets the value of the arrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalTime(String value) {
        this.arrivalTime = value;
    }

    /**
     * Gets the value of the bookingClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingClass() {
        return bookingClass;
    }

    /**
     * Sets the value of the bookingClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingClass(String value) {
        this.bookingClass = value;
    }

    /**
     * Gets the value of the reservationClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationClass() {
        return reservationClass;
    }

    /**
     * Sets the value of the reservationClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationClass(String value) {
        this.reservationClass = value;
    }

    /**
     * Gets the value of the bookingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * Sets the value of the bookingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingStatus(String value) {
        this.bookingStatus = value;
    }

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordLocator(String value) {
        this.recordLocator = value;
    }

    /**
     * Gets the value of the redeemMiles property.
     * 
     */
    public int getRedeemMiles() {
        return redeemMiles;
    }

    /**
     * Sets the value of the redeemMiles property.
     * 
     */
    public void setRedeemMiles(int value) {
        this.redeemMiles = value;
    }

    /**
     * Gets the value of the tierMiles property.
     * 
     */
    public int getTierMiles() {
        return tierMiles;
    }

    /**
     * Sets the value of the tierMiles property.
     * 
     */
    public void setTierMiles(int value) {
        this.tierMiles = value;
    }

    /**
     * Gets the value of the chauffeur property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChauffeur() {
        return chauffeur;
    }

    /**
     * Sets the value of the chauffeur property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChauffeur(String value) {
        this.chauffeur = value;
    }

    /**
     * Gets the value of the serviceCodes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceCodes() {
        return serviceCodes;
    }

    /**
     * Sets the value of the serviceCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceCodes(String value) {
        this.serviceCodes = value;
    }

    /**
     * Gets the value of the stopOver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopOver() {
        return stopOver;
    }

    /**
     * Sets the value of the stopOver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopOver(String value) {
        this.stopOver = value;
    }

    /**
     * Gets the value of the duration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDuration(String value) {
        this.duration = value;
    }

    /**
     * Gets the value of the prBr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrBr() {
        return prBr;
    }

    /**
     * Sets the value of the prBr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrBr(String value) {
        this.prBr = value;
    }

    /**
     * Gets the value of the flightType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightType() {
        return flightType;
    }

    /**
     * Sets the value of the flightType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightType(String value) {
        this.flightType = value;
    }

}
