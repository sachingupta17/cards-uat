
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardNoScm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardNoFtm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="newMember" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cardNoScm",
    "cardNoFtm",
    "newMember"
})
@XmlRootElement(name = "LinkFourthTierNominee")
public class LinkFourthTierNominee {

    protected String cardNoScm;
    protected String cardNoFtm;
    protected boolean newMember;

    /**
     * Gets the value of the cardNoScm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNoScm() {
        return cardNoScm;
    }

    /**
     * Sets the value of the cardNoScm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNoScm(String value) {
        this.cardNoScm = value;
    }

    /**
     * Gets the value of the cardNoFtm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNoFtm() {
        return cardNoFtm;
    }

    /**
     * Sets the value of the cardNoFtm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNoFtm(String value) {
        this.cardNoFtm = value;
    }

    /**
     * Gets the value of the newMember property.
     * 
     */
    public boolean isNewMember() {
        return newMember;
    }

    /**
     * Sets the value of the newMember property.
     * 
     */
    public void setNewMember(boolean value) {
        this.newMember = value;
    }

}
