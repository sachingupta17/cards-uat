
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfAliasCard complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAliasCard">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AliasCard" type="{http://skywards.com/Mercator.CRIS.WS}AliasCard" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAliasCard", propOrder = {
    "aliasCard"
})
public class ArrayOfAliasCard {

    @XmlElement(name = "AliasCard", nillable = true)
    protected List<AliasCard> aliasCard;

    /**
     * Gets the value of the aliasCard property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the aliasCard property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAliasCard().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AliasCard }
     * 
     * 
     */
    public List<AliasCard> getAliasCard() {
        if (aliasCard == null) {
            aliasCard = new ArrayList<AliasCard>();
        }
        return this.aliasCard;
    }

}
