
package com.skywards.mercator_cris;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ecmCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nextNumber" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ecmCode",
    "nextNumber"
})
@XmlRootElement(name = "UpdateInflightNumber")
public class UpdateInflightNumber {

    protected String ecmCode;
    @XmlElement(required = true)
    protected BigDecimal nextNumber;

    /**
     * Gets the value of the ecmCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEcmCode() {
        return ecmCode;
    }

    /**
     * Sets the value of the ecmCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEcmCode(String value) {
        this.ecmCode = value;
    }

    /**
     * Gets the value of the nextNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNextNumber() {
        return nextNumber;
    }

    /**
     * Sets the value of the nextNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNextNumber(BigDecimal value) {
        this.nextNumber = value;
    }

}
