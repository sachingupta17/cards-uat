
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetFuturePNRsResult" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfFuturePNR" minOccurs="0"/>
 *         &lt;element name="TotalPNR" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsResult" type="{http://skywards.com/Mercator.CRIS.WS}WSIResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getFuturePNRsResult",
    "totalPNR",
    "wsResult"
})
@XmlRootElement(name = "GetFuturePNRsResponse")
public class GetFuturePNRsResponse {

    @XmlElement(name = "GetFuturePNRsResult")
    protected ArrayOfFuturePNR getFuturePNRsResult;
    @XmlElement(name = "TotalPNR")
    protected int totalPNR;
    protected WSIResult wsResult;

    /**
     * Gets the value of the getFuturePNRsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFuturePNR }
     *     
     */
    public ArrayOfFuturePNR getGetFuturePNRsResult() {
        return getFuturePNRsResult;
    }

    /**
     * Sets the value of the getFuturePNRsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFuturePNR }
     *     
     */
    public void setGetFuturePNRsResult(ArrayOfFuturePNR value) {
        this.getFuturePNRsResult = value;
    }

    /**
     * Gets the value of the totalPNR property.
     * 
     */
    public int getTotalPNR() {
        return totalPNR;
    }

    /**
     * Sets the value of the totalPNR property.
     * 
     */
    public void setTotalPNR(int value) {
        this.totalPNR = value;
    }

    /**
     * Gets the value of the wsResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSIResult }
     *     
     */
    public WSIResult getWsResult() {
        return wsResult;
    }

    /**
     * Sets the value of the wsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSIResult }
     *     
     */
    public void setWsResult(WSIResult value) {
        this.wsResult = value;
    }

}
