
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSkywardsTierStatusResult" type="{http://skywards.com/Mercator.CRIS.WS}SkywardsTierStatus_Request" minOccurs="0"/>
 *         &lt;element name="wsResult" type="{http://skywards.com/Mercator.CRIS.WS}WSIResult" minOccurs="0"/>
 *         &lt;element name="specialCrit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSkywardsTierStatusResult",
    "wsResult",
    "specialCrit"
})
@XmlRootElement(name = "GetSkywardsTierStatusResponse")
public class GetSkywardsTierStatusResponse {

    @XmlElement(name = "GetSkywardsTierStatusResult")
    protected SkywardsTierStatusRequest getSkywardsTierStatusResult;
    protected WSIResult wsResult;
    protected String specialCrit;

    /**
     * Gets the value of the getSkywardsTierStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link SkywardsTierStatusRequest }
     *     
     */
    public SkywardsTierStatusRequest getGetSkywardsTierStatusResult() {
        return getSkywardsTierStatusResult;
    }

    /**
     * Sets the value of the getSkywardsTierStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SkywardsTierStatusRequest }
     *     
     */
    public void setGetSkywardsTierStatusResult(SkywardsTierStatusRequest value) {
        this.getSkywardsTierStatusResult = value;
    }

    /**
     * Gets the value of the wsResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSIResult }
     *     
     */
    public WSIResult getWsResult() {
        return wsResult;
    }

    /**
     * Sets the value of the wsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSIResult }
     *     
     */
    public void setWsResult(WSIResult value) {
        this.wsResult = value;
    }

    /**
     * Gets the value of the specialCrit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialCrit() {
        return specialCrit;
    }

    /**
     * Sets the value of the specialCrit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialCrit(String value) {
        this.specialCrit = value;
    }

}
