
package com.skywards.mercator_cris;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Activity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Activity">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}CRISReadOnlyBase">
 *       &lt;sequence>
 *         &lt;element name="ActivityID" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="TransactionArray" type="{http://skywards.com/Mercator.CRIS.WS}Transaction" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FamMemTransactionArray" type="{http://skywards.com/Mercator.CRIS.WS}Transaction" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CouponDetails" type="{http://skywards.com/Mercator.CRIS.WS}Transaction" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CouponFamilyDetails" type="{http://skywards.com/Mercator.CRIS.WS}Transaction" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PersonID" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="MemUID" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="ActivityType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ActivityTypeCategory" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Activity", propOrder = {
    "activityID",
    "transactionArray",
    "famMemTransactionArray",
    "couponDetails",
    "couponFamilyDetails"
})
public class Activity
    extends CRISReadOnlyBase
{

    @XmlElement(name = "ActivityID", required = true)
    protected BigDecimal activityID;
    @XmlElement(name = "TransactionArray")
    protected List<Transaction> transactionArray;
    @XmlElement(name = "FamMemTransactionArray")
    protected List<Transaction> famMemTransactionArray;
    @XmlElement(name = "CouponDetails")
    protected List<Transaction> couponDetails;
    @XmlElement(name = "CouponFamilyDetails")
    protected List<Transaction> couponFamilyDetails;
    @XmlAttribute(name = "PersonID", required = true)
    protected int personID;
    @XmlAttribute(name = "MemUID", required = true)
    protected int memUID;
    @XmlAttribute(name = "ActivityType")
    protected String activityType;
    @XmlAttribute(name = "ActivityTypeCategory")
    protected String activityTypeCategory;

    /**
     * Gets the value of the activityID property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getActivityID() {
        return activityID;
    }

    /**
     * Sets the value of the activityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setActivityID(BigDecimal value) {
        this.activityID = value;
    }

    /**
     * Gets the value of the transactionArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Transaction }
     * 
     * 
     */
    public List<Transaction> getTransactionArray() {
        if (transactionArray == null) {
            transactionArray = new ArrayList<Transaction>();
        }
        return this.transactionArray;
    }

    /**
     * Gets the value of the famMemTransactionArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the famMemTransactionArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFamMemTransactionArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Transaction }
     * 
     * 
     */
    public List<Transaction> getFamMemTransactionArray() {
        if (famMemTransactionArray == null) {
            famMemTransactionArray = new ArrayList<Transaction>();
        }
        return this.famMemTransactionArray;
    }

    /**
     * Gets the value of the couponDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the couponDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCouponDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Transaction }
     * 
     * 
     */
    public List<Transaction> getCouponDetails() {
        if (couponDetails == null) {
            couponDetails = new ArrayList<Transaction>();
        }
        return this.couponDetails;
    }

    /**
     * Gets the value of the couponFamilyDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the couponFamilyDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCouponFamilyDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Transaction }
     * 
     * 
     */
    public List<Transaction> getCouponFamilyDetails() {
        if (couponFamilyDetails == null) {
            couponFamilyDetails = new ArrayList<Transaction>();
        }
        return this.couponFamilyDetails;
    }

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     */
    public int getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     */
    public void setMemUID(int value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the activityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * Sets the value of the activityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityType(String value) {
        this.activityType = value;
    }

    /**
     * Gets the value of the activityTypeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityTypeCategory() {
        return activityTypeCategory;
    }

    /**
     * Sets the value of the activityTypeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityTypeCategory(String value) {
        this.activityTypeCategory = value;
    }

}
