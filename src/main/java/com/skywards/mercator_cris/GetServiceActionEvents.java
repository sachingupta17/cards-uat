
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="memUID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="sfmStaffNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="sfmStaffNoRaisedBy" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="depCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depRaisedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cotCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statusType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "personID",
    "memUID",
    "cardNumber",
    "refNo",
    "sfmStaffNo",
    "sfmStaffNoRaisedBy",
    "depCode",
    "depRaisedBy",
    "actCode",
    "cotCode",
    "statusType",
    "status"
})
@XmlRootElement(name = "GetServiceActionEvents")
public class GetServiceActionEvents {

    protected int personID;
    protected int memUID;
    protected String cardNumber;
    protected int refNo;
    protected int sfmStaffNo;
    protected int sfmStaffNoRaisedBy;
    protected String depCode;
    protected String depRaisedBy;
    protected String actCode;
    protected String cotCode;
    protected String statusType;
    protected String status;

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     */
    public int getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     */
    public void setMemUID(int value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the refNo property.
     * 
     */
    public int getRefNo() {
        return refNo;
    }

    /**
     * Sets the value of the refNo property.
     * 
     */
    public void setRefNo(int value) {
        this.refNo = value;
    }

    /**
     * Gets the value of the sfmStaffNo property.
     * 
     */
    public int getSfmStaffNo() {
        return sfmStaffNo;
    }

    /**
     * Sets the value of the sfmStaffNo property.
     * 
     */
    public void setSfmStaffNo(int value) {
        this.sfmStaffNo = value;
    }

    /**
     * Gets the value of the sfmStaffNoRaisedBy property.
     * 
     */
    public int getSfmStaffNoRaisedBy() {
        return sfmStaffNoRaisedBy;
    }

    /**
     * Sets the value of the sfmStaffNoRaisedBy property.
     * 
     */
    public void setSfmStaffNoRaisedBy(int value) {
        this.sfmStaffNoRaisedBy = value;
    }

    /**
     * Gets the value of the depCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepCode() {
        return depCode;
    }

    /**
     * Sets the value of the depCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepCode(String value) {
        this.depCode = value;
    }

    /**
     * Gets the value of the depRaisedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepRaisedBy() {
        return depRaisedBy;
    }

    /**
     * Sets the value of the depRaisedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepRaisedBy(String value) {
        this.depRaisedBy = value;
    }

    /**
     * Gets the value of the actCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActCode() {
        return actCode;
    }

    /**
     * Sets the value of the actCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActCode(String value) {
        this.actCode = value;
    }

    /**
     * Gets the value of the cotCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCotCode() {
        return cotCode;
    }

    /**
     * Sets the value of the cotCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotCode(String value) {
        this.cotCode = value;
    }

    /**
     * Gets the value of the statusType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusType() {
        return statusType;
    }

    /**
     * Sets the value of the statusType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusType(String value) {
        this.statusType = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
