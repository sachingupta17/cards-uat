
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getTierStatusInputDetails" type="{http://skywards.com/Mercator.CRIS.WS}SkywardsTierStatusInputWE" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTierStatusInputDetails"
})
@XmlRootElement(name = "GetSkywardsTierStatus")
public class GetSkywardsTierStatus {

    protected SkywardsTierStatusInputWE getTierStatusInputDetails;

    /**
     * Gets the value of the getTierStatusInputDetails property.
     * 
     * @return
     *     possible object is
     *     {@link SkywardsTierStatusInputWE }
     *     
     */
    public SkywardsTierStatusInputWE getGetTierStatusInputDetails() {
        return getTierStatusInputDetails;
    }

    /**
     * Sets the value of the getTierStatusInputDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link SkywardsTierStatusInputWE }
     *     
     */
    public void setGetTierStatusInputDetails(SkywardsTierStatusInputWE value) {
        this.getTierStatusInputDetails = value;
    }

}
