
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransferMilesResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="return_Response" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rewardid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="wsResult" type="{http://skywards.com/Mercator.CRIS.WS}WSIResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferMilesResult",
    "returnResponse",
    "rewardid",
    "wsResult"
})
@XmlRootElement(name = "TransferMilesResponse")
public class TransferMilesResponse {

    @XmlElement(name = "TransferMilesResult")
    protected boolean transferMilesResult;
    @XmlElement(name = "return_Response")
    protected String returnResponse;
    protected int rewardid;
    protected WSIResult wsResult;

    /**
     * Gets the value of the transferMilesResult property.
     * 
     */
    public boolean isTransferMilesResult() {
        return transferMilesResult;
    }

    /**
     * Sets the value of the transferMilesResult property.
     * 
     */
    public void setTransferMilesResult(boolean value) {
        this.transferMilesResult = value;
    }

    /**
     * Gets the value of the returnResponse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnResponse() {
        return returnResponse;
    }

    /**
     * Sets the value of the returnResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnResponse(String value) {
        this.returnResponse = value;
    }

    /**
     * Gets the value of the rewardid property.
     * 
     */
    public int getRewardid() {
        return rewardid;
    }

    /**
     * Sets the value of the rewardid property.
     * 
     */
    public void setRewardid(int value) {
        this.rewardid = value;
    }

    /**
     * Gets the value of the wsResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSIResult }
     *     
     */
    public WSIResult getWsResult() {
        return wsResult;
    }

    /**
     * Sets the value of the wsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSIResult }
     *     
     */
    public void setWsResult(WSIResult value) {
        this.wsResult = value;
    }

}
