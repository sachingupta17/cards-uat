
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BusinessBaseOfItineryEmailAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessBaseOfItineryEmailAddress">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}UndoableBase">
 *       &lt;sequence>
 *         &lt;element name="Parent" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfItineryEmailAddress" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessBaseOfItineryEmailAddress", propOrder = {
    "parent"
})
@XmlSeeAlso({
    CRISBusinessBaseOfItineryEmailAddress.class
})
public abstract class BusinessBaseOfItineryEmailAddress
    extends UndoableBase
{

    @XmlElement(name = "Parent")
    protected ArrayOfItineryEmailAddress parent;

    /**
     * Gets the value of the parent property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfItineryEmailAddress }
     *     
     */
    public ArrayOfItineryEmailAddress getParent() {
        return parent;
    }

    /**
     * Sets the value of the parent property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfItineryEmailAddress }
     *     
     */
    public void setParent(ArrayOfItineryEmailAddress value) {
        this.parent = value;
    }

}
