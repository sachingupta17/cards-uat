
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="memberactivecardnumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="personid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="memuid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="memberstatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "memberactivecardnumber",
    "personid",
    "memuid",
    "memberstatus"
})
@XmlRootElement(name = "DeLinkFamilyMember")
public class DeLinkFamilyMember {

    protected String memberactivecardnumber;
    protected int personid;
    protected int memuid;
    protected String memberstatus;

    /**
     * Gets the value of the memberactivecardnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberactivecardnumber() {
        return memberactivecardnumber;
    }

    /**
     * Sets the value of the memberactivecardnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberactivecardnumber(String value) {
        this.memberactivecardnumber = value;
    }

    /**
     * Gets the value of the personid property.
     * 
     */
    public int getPersonid() {
        return personid;
    }

    /**
     * Sets the value of the personid property.
     * 
     */
    public void setPersonid(int value) {
        this.personid = value;
    }

    /**
     * Gets the value of the memuid property.
     * 
     */
    public int getMemuid() {
        return memuid;
    }

    /**
     * Sets the value of the memuid property.
     * 
     */
    public void setMemuid(int value) {
        this.memuid = value;
    }

    /**
     * Gets the value of the memberstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberstatus() {
        return memberstatus;
    }

    /**
     * Sets the value of the memberstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberstatus(String value) {
        this.memberstatus = value;
    }

}
