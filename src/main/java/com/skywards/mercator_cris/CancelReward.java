
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RewardsID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ReasonForRefund" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cardNumber",
    "rewardsID",
    "reasonForRefund"
})
@XmlRootElement(name = "CancelReward")
public class CancelReward {

    @XmlElement(name = "CardNumber")
    protected String cardNumber;
    @XmlElement(name = "RewardsID")
    protected int rewardsID;
    @XmlElement(name = "ReasonForRefund")
    protected String reasonForRefund;

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the rewardsID property.
     * 
     */
    public int getRewardsID() {
        return rewardsID;
    }

    /**
     * Sets the value of the rewardsID property.
     * 
     */
    public void setRewardsID(int value) {
        this.rewardsID = value;
    }

    /**
     * Gets the value of the reasonForRefund property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForRefund() {
        return reasonForRefund;
    }

    /**
     * Sets the value of the reasonForRefund property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForRefund(String value) {
        this.reasonForRefund = value;
    }

}
