
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fromCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="toCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromMemUid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="toMemUid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fromPersonId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="toPersonId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Originator_system" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="bbitRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="cardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emdRequired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MpdRefNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fromCardNo",
    "toCardNo",
    "fromMemUid",
    "toMemUid",
    "fromPersonId",
    "toPersonId",
    "tMiles",
    "tStatus",
    "originatorSystem",
    "amount",
    "bbitRef",
    "accountNo",
    "expDate",
    "cardType",
    "paymentMethod",
    "orderCode",
    "emdRequired",
    "mpdRefNo",
    "userId"
})
@XmlRootElement(name = "TransferMiles")
public class TransferMiles {

    protected String fromCardNo;
    protected String toCardNo;
    protected int fromMemUid;
    protected int toMemUid;
    protected int fromPersonId;
    protected int toPersonId;
    protected int tMiles;
    protected String tStatus;
    @XmlElement(name = "Originator_system")
    protected String originatorSystem;
    @XmlElement(name = "Amount")
    protected double amount;
    protected String bbitRef;
    protected String accountNo;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expDate;
    protected String cardType;
    protected String paymentMethod;
    protected String orderCode;
    protected String emdRequired;
    @XmlElement(name = "MpdRefNo")
    protected String mpdRefNo;
    @XmlElement(name = "UserId")
    protected String userId;

    /**
     * Gets the value of the fromCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromCardNo() {
        return fromCardNo;
    }

    /**
     * Sets the value of the fromCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromCardNo(String value) {
        this.fromCardNo = value;
    }

    /**
     * Gets the value of the toCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToCardNo() {
        return toCardNo;
    }

    /**
     * Sets the value of the toCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToCardNo(String value) {
        this.toCardNo = value;
    }

    /**
     * Gets the value of the fromMemUid property.
     * 
     */
    public int getFromMemUid() {
        return fromMemUid;
    }

    /**
     * Sets the value of the fromMemUid property.
     * 
     */
    public void setFromMemUid(int value) {
        this.fromMemUid = value;
    }

    /**
     * Gets the value of the toMemUid property.
     * 
     */
    public int getToMemUid() {
        return toMemUid;
    }

    /**
     * Sets the value of the toMemUid property.
     * 
     */
    public void setToMemUid(int value) {
        this.toMemUid = value;
    }

    /**
     * Gets the value of the fromPersonId property.
     * 
     */
    public int getFromPersonId() {
        return fromPersonId;
    }

    /**
     * Sets the value of the fromPersonId property.
     * 
     */
    public void setFromPersonId(int value) {
        this.fromPersonId = value;
    }

    /**
     * Gets the value of the toPersonId property.
     * 
     */
    public int getToPersonId() {
        return toPersonId;
    }

    /**
     * Sets the value of the toPersonId property.
     * 
     */
    public void setToPersonId(int value) {
        this.toPersonId = value;
    }

    /**
     * Gets the value of the tMiles property.
     * 
     */
    public int getTMiles() {
        return tMiles;
    }

    /**
     * Sets the value of the tMiles property.
     * 
     */
    public void setTMiles(int value) {
        this.tMiles = value;
    }

    /**
     * Gets the value of the tStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTStatus() {
        return tStatus;
    }

    /**
     * Sets the value of the tStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTStatus(String value) {
        this.tStatus = value;
    }

    /**
     * Gets the value of the originatorSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatorSystem() {
        return originatorSystem;
    }

    /**
     * Sets the value of the originatorSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatorSystem(String value) {
        this.originatorSystem = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAmount(double value) {
        this.amount = value;
    }

    /**
     * Gets the value of the bbitRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBbitRef() {
        return bbitRef;
    }

    /**
     * Sets the value of the bbitRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBbitRef(String value) {
        this.bbitRef = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNo(String value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the expDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpDate() {
        return expDate;
    }

    /**
     * Sets the value of the expDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpDate(XMLGregorianCalendar value) {
        this.expDate = value;
    }

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardType(String value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the orderCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderCode() {
        return orderCode;
    }

    /**
     * Sets the value of the orderCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderCode(String value) {
        this.orderCode = value;
    }

    /**
     * Gets the value of the emdRequired property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmdRequired() {
        return emdRequired;
    }

    /**
     * Sets the value of the emdRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmdRequired(String value) {
        this.emdRequired = value;
    }

    /**
     * Gets the value of the mpdRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMpdRefNo() {
        return mpdRefNo;
    }

    /**
     * Sets the value of the mpdRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMpdRefNo(String value) {
        this.mpdRefNo = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

}
