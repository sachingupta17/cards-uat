
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BusinessBaseOfAchieve complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessBaseOfAchieve">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}UndoableBase">
 *       &lt;sequence>
 *         &lt;element name="Parent" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfAchieve" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessBaseOfAchieve", propOrder = {
    "parent"
})
@XmlSeeAlso({
    Achieve.class
})
public abstract class BusinessBaseOfAchieve
    extends UndoableBase
{

    @XmlElement(name = "Parent")
    protected ArrayOfAchieve parent;

    /**
     * Gets the value of the parent property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAchieve }
     *     
     */
    public ArrayOfAchieve getParent() {
        return parent;
    }

    /**
     * Sets the value of the parent property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAchieve }
     *     
     */
    public void setParent(ArrayOfAchieve value) {
        this.parent = value;
    }

}
