
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rewardBookingEntity" type="{http://skywards.com/Mercator.CRIS.WS}RewardBooking" minOccurs="0"/>
 *         &lt;element name="itineraryDetailsList" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfRewardItineraryDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rewardBookingEntity",
    "itineraryDetailsList"
})
@XmlRootElement(name = "CreateRewardBooking")
public class CreateRewardBooking {

    protected RewardBooking rewardBookingEntity;
    protected ArrayOfRewardItineraryDetail itineraryDetailsList;

    /**
     * Gets the value of the rewardBookingEntity property.
     * 
     * @return
     *     possible object is
     *     {@link RewardBooking }
     *     
     */
    public RewardBooking getRewardBookingEntity() {
        return rewardBookingEntity;
    }

    /**
     * Sets the value of the rewardBookingEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardBooking }
     *     
     */
    public void setRewardBookingEntity(RewardBooking value) {
        this.rewardBookingEntity = value;
    }

    /**
     * Gets the value of the itineraryDetailsList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRewardItineraryDetail }
     *     
     */
    public ArrayOfRewardItineraryDetail getItineraryDetailsList() {
        return itineraryDetailsList;
    }

    /**
     * Sets the value of the itineraryDetailsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRewardItineraryDetail }
     *     
     */
    public void setItineraryDetailsList(ArrayOfRewardItineraryDetail value) {
        this.itineraryDetailsList = value;
    }

}
