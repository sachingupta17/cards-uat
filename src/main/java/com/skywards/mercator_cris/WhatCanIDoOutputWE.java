
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WhatCanIDoOutputWE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WhatCanIDoOutputWE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TravelDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Origin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Destingation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Class_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Miles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Descr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Mile_category" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MileType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flight_number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tkt_type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Direct_routes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WhatCanIDoOutputWE", propOrder = {
    "travelDate",
    "origin",
    "destingation",
    "className",
    "miles",
    "descr",
    "mileCategory",
    "mileType",
    "flightNumber",
    "tktType",
    "directRoutes"
})
public class WhatCanIDoOutputWE {

    @XmlElement(name = "TravelDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar travelDate;
    @XmlElement(name = "Origin")
    protected String origin;
    @XmlElement(name = "Destingation")
    protected String destingation;
    @XmlElement(name = "Class_name")
    protected String className;
    @XmlElement(name = "Miles")
    protected int miles;
    @XmlElement(name = "Descr")
    protected String descr;
    @XmlElement(name = "Mile_category")
    protected String mileCategory;
    @XmlElement(name = "MileType")
    protected String mileType;
    @XmlElement(name = "Flight_number")
    protected String flightNumber;
    @XmlElement(name = "Tkt_type")
    protected String tktType;
    @XmlElement(name = "Direct_routes")
    protected String directRoutes;

    /**
     * Gets the value of the travelDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTravelDate() {
        return travelDate;
    }

    /**
     * Sets the value of the travelDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTravelDate(XMLGregorianCalendar value) {
        this.travelDate = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Gets the value of the destingation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestingation() {
        return destingation;
    }

    /**
     * Sets the value of the destingation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestingation(String value) {
        this.destingation = value;
    }

    /**
     * Gets the value of the className property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassName() {
        return className;
    }

    /**
     * Sets the value of the className property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassName(String value) {
        this.className = value;
    }

    /**
     * Gets the value of the miles property.
     * 
     */
    public int getMiles() {
        return miles;
    }

    /**
     * Sets the value of the miles property.
     * 
     */
    public void setMiles(int value) {
        this.miles = value;
    }

    /**
     * Gets the value of the descr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescr() {
        return descr;
    }

    /**
     * Sets the value of the descr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescr(String value) {
        this.descr = value;
    }

    /**
     * Gets the value of the mileCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMileCategory() {
        return mileCategory;
    }

    /**
     * Sets the value of the mileCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMileCategory(String value) {
        this.mileCategory = value;
    }

    /**
     * Gets the value of the mileType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMileType() {
        return mileType;
    }

    /**
     * Sets the value of the mileType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMileType(String value) {
        this.mileType = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the tktType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTktType() {
        return tktType;
    }

    /**
     * Sets the value of the tktType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTktType(String value) {
        this.tktType = value;
    }

    /**
     * Gets the value of the directRoutes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectRoutes() {
        return directRoutes;
    }

    /**
     * Sets the value of the directRoutes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectRoutes(String value) {
        this.directRoutes = value;
    }

}
