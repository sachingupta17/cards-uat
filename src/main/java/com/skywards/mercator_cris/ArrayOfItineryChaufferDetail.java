
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfItineryChaufferDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfItineryChaufferDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItineryChaufferDetail" type="{http://skywards.com/Mercator.CRIS.WS}ItineryChaufferDetail" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfItineryChaufferDetail", propOrder = {
    "itineryChaufferDetail"
})
public class ArrayOfItineryChaufferDetail {

    @XmlElement(name = "ItineryChaufferDetail", nillable = true)
    protected List<ItineryChaufferDetail> itineryChaufferDetail;

    /**
     * Gets the value of the itineryChaufferDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itineryChaufferDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItineryChaufferDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItineryChaufferDetail }
     * 
     * 
     */
    public List<ItineryChaufferDetail> getItineryChaufferDetail() {
        if (itineryChaufferDetail == null) {
            itineryChaufferDetail = new ArrayList<ItineryChaufferDetail>();
        }
        return this.itineryChaufferDetail;
    }

}
