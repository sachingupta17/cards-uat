
package com.skywards.mercator_cris;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MyStatement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MyStatement">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}CRISReadOnlyBase">
 *       &lt;sequence>
 *         &lt;element name="MyStatementID" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="TransactionForWebArray" type="{http://skywards.com/Mercator.CRIS.WS}WebTransaction" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FamMemTransactionForWebArray" type="{http://skywards.com/Mercator.CRIS.WS}WebTransaction" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CouponDetails" type="{http://skywards.com/Mercator.CRIS.WS}WebTransaction" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CouponFamilyDetails" type="{http://skywards.com/Mercator.CRIS.WS}WebTransaction" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PersonID" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="MemUID" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="MyStatementType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="MyStatementTypeCategory" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MyStatement", propOrder = {
    "myStatementID",
    "transactionForWebArray",
    "famMemTransactionForWebArray",
    "couponDetails",
    "couponFamilyDetails"
})
public class MyStatement2
    extends CRISReadOnlyBase
{

    @XmlElement(name = "MyStatementID", required = true)
    protected BigDecimal myStatementID;
    @XmlElement(name = "TransactionForWebArray")
    protected List<WebTransaction> transactionForWebArray;
    @XmlElement(name = "FamMemTransactionForWebArray")
    protected List<WebTransaction> famMemTransactionForWebArray;
    @XmlElement(name = "CouponDetails")
    protected List<WebTransaction> couponDetails;
    @XmlElement(name = "CouponFamilyDetails")
    protected List<WebTransaction> couponFamilyDetails;
    @XmlAttribute(name = "PersonID", required = true)
    protected int personID;
    @XmlAttribute(name = "MemUID", required = true)
    protected int memUID;
    @XmlAttribute(name = "MyStatementType")
    protected String myStatementType;
    @XmlAttribute(name = "MyStatementTypeCategory")
    protected String myStatementTypeCategory;

    /**
     * Gets the value of the myStatementID property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMyStatementID() {
        return myStatementID;
    }

    /**
     * Sets the value of the myStatementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMyStatementID(BigDecimal value) {
        this.myStatementID = value;
    }

    /**
     * Gets the value of the transactionForWebArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionForWebArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionForWebArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebTransaction }
     * 
     * 
     */
    public List<WebTransaction> getTransactionForWebArray() {
        if (transactionForWebArray == null) {
            transactionForWebArray = new ArrayList<WebTransaction>();
        }
        return this.transactionForWebArray;
    }

    /**
     * Gets the value of the famMemTransactionForWebArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the famMemTransactionForWebArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFamMemTransactionForWebArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebTransaction }
     * 
     * 
     */
    public List<WebTransaction> getFamMemTransactionForWebArray() {
        if (famMemTransactionForWebArray == null) {
            famMemTransactionForWebArray = new ArrayList<WebTransaction>();
        }
        return this.famMemTransactionForWebArray;
    }

    /**
     * Gets the value of the couponDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the couponDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCouponDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebTransaction }
     * 
     * 
     */
    public List<WebTransaction> getCouponDetails() {
        if (couponDetails == null) {
            couponDetails = new ArrayList<WebTransaction>();
        }
        return this.couponDetails;
    }

    /**
     * Gets the value of the couponFamilyDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the couponFamilyDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCouponFamilyDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebTransaction }
     * 
     * 
     */
    public List<WebTransaction> getCouponFamilyDetails() {
        if (couponFamilyDetails == null) {
            couponFamilyDetails = new ArrayList<WebTransaction>();
        }
        return this.couponFamilyDetails;
    }

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     */
    public int getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     */
    public void setMemUID(int value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the myStatementType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMyStatementType() {
        return myStatementType;
    }

    /**
     * Sets the value of the myStatementType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMyStatementType(String value) {
        this.myStatementType = value;
    }

    /**
     * Gets the value of the myStatementTypeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMyStatementTypeCategory() {
        return myStatementTypeCategory;
    }

    /**
     * Sets the value of the myStatementTypeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMyStatementTypeCategory(String value) {
        this.myStatementTypeCategory = value;
    }

}
