
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateFlags complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateFlags">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ADD_UPDATE" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DELETE" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="REPLACE" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateFlags", propOrder = {
    "addupdate",
    "delete",
    "replace"
})
public class UpdateFlags {

    @XmlElement(name = "ADD_UPDATE")
    protected boolean addupdate;
    @XmlElement(name = "DELETE")
    protected boolean delete;
    @XmlElement(name = "REPLACE")
    protected boolean replace;

    /**
     * Gets the value of the addupdate property.
     * 
     */
    public boolean isADDUPDATE() {
        return addupdate;
    }

    /**
     * Sets the value of the addupdate property.
     * 
     */
    public void setADDUPDATE(boolean value) {
        this.addupdate = value;
    }

    /**
     * Gets the value of the delete property.
     * 
     */
    public boolean isDELETE() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     * 
     */
    public void setDELETE(boolean value) {
        this.delete = value;
    }

    /**
     * Gets the value of the replace property.
     * 
     */
    public boolean isREPLACE() {
        return replace;
    }

    /**
     * Sets the value of the replace property.
     * 
     */
    public void setREPLACE(boolean value) {
        this.replace = value;
    }

}
