
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetItineryEmailAddressResult" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfItineryEmailAddress" minOccurs="0"/>
 *         &lt;element name="wsResult" type="{http://skywards.com/Mercator.CRIS.WS}WSIResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getItineryEmailAddressResult",
    "wsResult"
})
@XmlRootElement(name = "GetItineryEmailAddressResponse")
public class GetItineryEmailAddressResponse {

    @XmlElement(name = "GetItineryEmailAddressResult")
    protected ArrayOfItineryEmailAddress getItineryEmailAddressResult;
    protected WSIResult wsResult;

    /**
     * Gets the value of the getItineryEmailAddressResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfItineryEmailAddress }
     *     
     */
    public ArrayOfItineryEmailAddress getGetItineryEmailAddressResult() {
        return getItineryEmailAddressResult;
    }

    /**
     * Sets the value of the getItineryEmailAddressResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfItineryEmailAddress }
     *     
     */
    public void setGetItineryEmailAddressResult(ArrayOfItineryEmailAddress value) {
        this.getItineryEmailAddressResult = value;
    }

    /**
     * Gets the value of the wsResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSIResult }
     *     
     */
    public WSIResult getWsResult() {
        return wsResult;
    }

    /**
     * Sets the value of the wsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSIResult }
     *     
     */
    public void setWsResult(WSIResult value) {
        this.wsResult = value;
    }

}
