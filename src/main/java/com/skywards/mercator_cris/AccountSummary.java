
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountSummary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PersonID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TotalEarned" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="TotalSpent" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="TotalExpired" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="TierMiles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TierMilesLastYear" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TierSectorsFlown" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ExpirySummary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NextMilesExpiryDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="NextMilesExpiryMiles" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt64"/>
 *         &lt;element name="CardPullDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="CurrentYearEarning" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ActiveCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemUID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RemainingMilesPurchase" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MilesReceived" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="LastActivityDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="MilesTransferred" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PointsBal" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="Tier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TierValidityDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="RemainingMilesTransfer" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TierReviewDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountSummary", propOrder = {
    "personID",
    "totalEarned",
    "totalSpent",
    "totalExpired",
    "tierMiles",
    "tierMilesLastYear",
    "tierSectorsFlown",
    "expirySummary",
    "nextMilesExpiryDate",
    "nextMilesExpiryMiles",
    "cardPullDate",
    "currentYearEarning",
    "activeCardNumber",
    "memUID",
    "userID",
    "remainingMilesPurchase",
    "milesReceived",
    "lastActivityDate",
    "milesTransferred",
    "pointsBal",
    "tier",
    "tierValidityDate",
    "remainingMilesTransfer",
    "tierReviewDate"
})
public class AccountSummary {

    @XmlElement(name = "PersonID")
    protected int personID;
    @XmlElement(name = "TotalEarned")
    protected long totalEarned;
    @XmlElement(name = "TotalSpent")
    protected long totalSpent;
    @XmlElement(name = "TotalExpired")
    protected long totalExpired;
    @XmlElement(name = "TierMiles")
    protected int tierMiles;
    @XmlElement(name = "TierMilesLastYear")
    protected int tierMilesLastYear;
    @XmlElement(name = "TierSectorsFlown")
    protected int tierSectorsFlown;
    @XmlElement(name = "ExpirySummary")
    protected String expirySummary;
    @XmlElement(name = "NextMilesExpiryDate", required = true)
    protected QueryableDateTime nextMilesExpiryDate;
    @XmlElement(name = "NextMilesExpiryMiles", required = true)
    protected QueryableInt64 nextMilesExpiryMiles;
    @XmlElement(name = "CardPullDate", required = true)
    protected QueryableDateTime cardPullDate;
    @XmlElement(name = "CurrentYearEarning")
    protected long currentYearEarning;
    @XmlElement(name = "ActiveCardNumber")
    protected String activeCardNumber;
    @XmlElement(name = "MemUID")
    protected int memUID;
    @XmlElement(name = "UserID")
    protected String userID;
    @XmlElement(name = "RemainingMilesPurchase")
    protected int remainingMilesPurchase;
    @XmlElement(name = "MilesReceived")
    protected int milesReceived;
    @XmlElement(name = "LastActivityDate", required = true)
    protected QueryableDateTime lastActivityDate;
    @XmlElement(name = "MilesTransferred")
    protected int milesTransferred;
    @XmlElement(name = "PointsBal")
    protected long pointsBal;
    @XmlElement(name = "Tier")
    protected String tier;
    @XmlElement(name = "TierValidityDate", required = true)
    protected QueryableDateTime tierValidityDate;
    @XmlElement(name = "RemainingMilesTransfer")
    protected int remainingMilesTransfer;
    @XmlElement(name = "TierReviewDate", required = true)
    protected QueryableDateTime tierReviewDate;

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the totalEarned property.
     * 
     */
    public long getTotalEarned() {
        return totalEarned;
    }

    /**
     * Sets the value of the totalEarned property.
     * 
     */
    public void setTotalEarned(long value) {
        this.totalEarned = value;
    }

    /**
     * Gets the value of the totalSpent property.
     * 
     */
    public long getTotalSpent() {
        return totalSpent;
    }

    /**
     * Sets the value of the totalSpent property.
     * 
     */
    public void setTotalSpent(long value) {
        this.totalSpent = value;
    }

    /**
     * Gets the value of the totalExpired property.
     * 
     */
    public long getTotalExpired() {
        return totalExpired;
    }

    /**
     * Sets the value of the totalExpired property.
     * 
     */
    public void setTotalExpired(long value) {
        this.totalExpired = value;
    }

    /**
     * Gets the value of the tierMiles property.
     * 
     */
    public int getTierMiles() {
        return tierMiles;
    }

    /**
     * Sets the value of the tierMiles property.
     * 
     */
    public void setTierMiles(int value) {
        this.tierMiles = value;
    }

    /**
     * Gets the value of the tierMilesLastYear property.
     * 
     */
    public int getTierMilesLastYear() {
        return tierMilesLastYear;
    }

    /**
     * Sets the value of the tierMilesLastYear property.
     * 
     */
    public void setTierMilesLastYear(int value) {
        this.tierMilesLastYear = value;
    }

    /**
     * Gets the value of the tierSectorsFlown property.
     * 
     */
    public int getTierSectorsFlown() {
        return tierSectorsFlown;
    }

    /**
     * Sets the value of the tierSectorsFlown property.
     * 
     */
    public void setTierSectorsFlown(int value) {
        this.tierSectorsFlown = value;
    }

    /**
     * Gets the value of the expirySummary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirySummary() {
        return expirySummary;
    }

    /**
     * Sets the value of the expirySummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirySummary(String value) {
        this.expirySummary = value;
    }

    /**
     * Gets the value of the nextMilesExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getNextMilesExpiryDate() {
        return nextMilesExpiryDate;
    }

    /**
     * Sets the value of the nextMilesExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setNextMilesExpiryDate(QueryableDateTime value) {
        this.nextMilesExpiryDate = value;
    }

    /**
     * Gets the value of the nextMilesExpiryMiles property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt64 }
     *     
     */
    public QueryableInt64 getNextMilesExpiryMiles() {
        return nextMilesExpiryMiles;
    }

    /**
     * Sets the value of the nextMilesExpiryMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt64 }
     *     
     */
    public void setNextMilesExpiryMiles(QueryableInt64 value) {
        this.nextMilesExpiryMiles = value;
    }

    /**
     * Gets the value of the cardPullDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getCardPullDate() {
        return cardPullDate;
    }

    /**
     * Sets the value of the cardPullDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setCardPullDate(QueryableDateTime value) {
        this.cardPullDate = value;
    }

    /**
     * Gets the value of the currentYearEarning property.
     * 
     */
    public long getCurrentYearEarning() {
        return currentYearEarning;
    }

    /**
     * Sets the value of the currentYearEarning property.
     * 
     */
    public void setCurrentYearEarning(long value) {
        this.currentYearEarning = value;
    }

    /**
     * Gets the value of the activeCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveCardNumber() {
        return activeCardNumber;
    }

    /**
     * Sets the value of the activeCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveCardNumber(String value) {
        this.activeCardNumber = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     */
    public int getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     */
    public void setMemUID(int value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the remainingMilesPurchase property.
     * 
     */
    public int getRemainingMilesPurchase() {
        return remainingMilesPurchase;
    }

    /**
     * Sets the value of the remainingMilesPurchase property.
     * 
     */
    public void setRemainingMilesPurchase(int value) {
        this.remainingMilesPurchase = value;
    }

    /**
     * Gets the value of the milesReceived property.
     * 
     */
    public int getMilesReceived() {
        return milesReceived;
    }

    /**
     * Sets the value of the milesReceived property.
     * 
     */
    public void setMilesReceived(int value) {
        this.milesReceived = value;
    }

    /**
     * Gets the value of the lastActivityDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getLastActivityDate() {
        return lastActivityDate;
    }

    /**
     * Sets the value of the lastActivityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setLastActivityDate(QueryableDateTime value) {
        this.lastActivityDate = value;
    }

    /**
     * Gets the value of the milesTransferred property.
     * 
     */
    public int getMilesTransferred() {
        return milesTransferred;
    }

    /**
     * Sets the value of the milesTransferred property.
     * 
     */
    public void setMilesTransferred(int value) {
        this.milesTransferred = value;
    }

    /**
     * Gets the value of the pointsBal property.
     * 
     */
    public long getPointsBal() {
        return pointsBal;
    }

    /**
     * Sets the value of the pointsBal property.
     * 
     */
    public void setPointsBal(long value) {
        this.pointsBal = value;
    }

    /**
     * Gets the value of the tier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTier() {
        return tier;
    }

    /**
     * Sets the value of the tier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTier(String value) {
        this.tier = value;
    }

    /**
     * Gets the value of the tierValidityDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getTierValidityDate() {
        return tierValidityDate;
    }

    /**
     * Sets the value of the tierValidityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setTierValidityDate(QueryableDateTime value) {
        this.tierValidityDate = value;
    }

    /**
     * Gets the value of the remainingMilesTransfer property.
     * 
     */
    public int getRemainingMilesTransfer() {
        return remainingMilesTransfer;
    }

    /**
     * Sets the value of the remainingMilesTransfer property.
     * 
     */
    public void setRemainingMilesTransfer(int value) {
        this.remainingMilesTransfer = value;
    }

    /**
     * Gets the value of the tierReviewDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getTierReviewDate() {
        return tierReviewDate;
    }

    /**
     * Sets the value of the tierReviewDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setTierReviewDate(QueryableDateTime value) {
        this.tierReviewDate = value;
    }

}
