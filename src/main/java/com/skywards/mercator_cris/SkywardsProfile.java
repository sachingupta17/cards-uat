
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SkywardsProfile complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SkywardsProfile">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Member" type="{http://skywards.com/Mercator.CRIS.WS}Member" minOccurs="0"/>
 *         &lt;element name="AliasCards" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfAliasCard" minOccurs="0"/>
 *         &lt;element name="Addresses" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfAddress" minOccurs="0"/>
 *         &lt;element name="Contacts" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfContact" minOccurs="0"/>
 *         &lt;element name="CreditCards" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfCreditCard" minOccurs="0"/>
 *         &lt;element name="Passports" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfPassport" minOccurs="0"/>
 *         &lt;element name="Preferences" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfPreference" minOccurs="0"/>
 *         &lt;element name="TravelCoordinator" type="{http://skywards.com/Mercator.CRIS.WS}TravelCoordinator" minOccurs="0"/>
 *         &lt;element name="Guardians" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfGuardian" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkywardsProfile", propOrder = {
    "member",
    "aliasCards",
    "addresses",
    "contacts",
    "creditCards",
    "passports",
    "preferences",
    "travelCoordinator",
    "guardians"
})
public class SkywardsProfile {

    @XmlElement(name = "Member")
    protected Member member;
    @XmlElement(name = "AliasCards")
    protected ArrayOfAliasCard aliasCards;
    @XmlElement(name = "Addresses")
    protected ArrayOfAddress addresses;
    @XmlElement(name = "Contacts")
    protected ArrayOfContact contacts;
    @XmlElement(name = "CreditCards")
    protected ArrayOfCreditCard creditCards;
    @XmlElement(name = "Passports")
    protected ArrayOfPassport passports;
    @XmlElement(name = "Preferences")
    protected ArrayOfPreference preferences;
    @XmlElement(name = "TravelCoordinator")
    protected TravelCoordinator travelCoordinator;
    @XmlElement(name = "Guardians")
    protected ArrayOfGuardian guardians;

    /**
     * Gets the value of the member property.
     * 
     * @return
     *     possible object is
     *     {@link Member }
     *     
     */
    public Member getMember() {
        return member;
    }

    /**
     * Sets the value of the member property.
     * 
     * @param value
     *     allowed object is
     *     {@link Member }
     *     
     */
    public void setMember(Member value) {
        this.member = value;
    }

    /**
     * Gets the value of the aliasCards property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAliasCard }
     *     
     */
    public ArrayOfAliasCard getAliasCards() {
        return aliasCards;
    }

    /**
     * Sets the value of the aliasCards property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAliasCard }
     *     
     */
    public void setAliasCards(ArrayOfAliasCard value) {
        this.aliasCards = value;
    }

    /**
     * Gets the value of the addresses property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAddress }
     *     
     */
    public ArrayOfAddress getAddresses() {
        return addresses;
    }

    /**
     * Sets the value of the addresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAddress }
     *     
     */
    public void setAddresses(ArrayOfAddress value) {
        this.addresses = value;
    }

    /**
     * Gets the value of the contacts property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContact }
     *     
     */
    public ArrayOfContact getContacts() {
        return contacts;
    }

    /**
     * Sets the value of the contacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContact }
     *     
     */
    public void setContacts(ArrayOfContact value) {
        this.contacts = value;
    }

    /**
     * Gets the value of the creditCards property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCreditCard }
     *     
     */
    public ArrayOfCreditCard getCreditCards() {
        return creditCards;
    }

    /**
     * Sets the value of the creditCards property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCreditCard }
     *     
     */
    public void setCreditCards(ArrayOfCreditCard value) {
        this.creditCards = value;
    }

    /**
     * Gets the value of the passports property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPassport }
     *     
     */
    public ArrayOfPassport getPassports() {
        return passports;
    }

    /**
     * Sets the value of the passports property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPassport }
     *     
     */
    public void setPassports(ArrayOfPassport value) {
        this.passports = value;
    }

    /**
     * Gets the value of the preferences property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPreference }
     *     
     */
    public ArrayOfPreference getPreferences() {
        return preferences;
    }

    /**
     * Sets the value of the preferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPreference }
     *     
     */
    public void setPreferences(ArrayOfPreference value) {
        this.preferences = value;
    }

    /**
     * Gets the value of the travelCoordinator property.
     * 
     * @return
     *     possible object is
     *     {@link TravelCoordinator }
     *     
     */
    public TravelCoordinator getTravelCoordinator() {
        return travelCoordinator;
    }

    /**
     * Sets the value of the travelCoordinator property.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelCoordinator }
     *     
     */
    public void setTravelCoordinator(TravelCoordinator value) {
        this.travelCoordinator = value;
    }

    /**
     * Gets the value of the guardians property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGuardian }
     *     
     */
    public ArrayOfGuardian getGuardians() {
        return guardians;
    }

    /**
     * Sets the value of the guardians property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGuardian }
     *     
     */
    public void setGuardians(ArrayOfGuardian value) {
        this.guardians = value;
    }

}
