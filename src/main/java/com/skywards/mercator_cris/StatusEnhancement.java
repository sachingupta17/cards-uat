
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StatusEnhancement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StatusEnhancement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PersonId" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="MemberID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SenderCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivationDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="SenderCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SenderDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SenderDateCeased" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="SenderUid" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="SenderCardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SenderCardTypeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SenderCustomerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SenderCardStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SenderCancelReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SourceFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TierCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecordCreationDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="ActiveCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CorporateCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CorpCardSeqNo" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusEnhancement", propOrder = {
    "personId",
    "memberID",
    "senderCode",
    "activationDate",
    "senderCardNumber",
    "senderDescription",
    "senderDateCeased",
    "senderUid",
    "senderCardType",
    "senderCardTypeDesc",
    "senderCustomerName",
    "senderCardStatus",
    "senderCancelReasonCode",
    "sourceFlag",
    "tierCode",
    "recordCreationDate",
    "activeCardNo",
    "title",
    "firstName",
    "middleName",
    "lastName",
    "corporateCardNo",
    "corpCardSeqNo"
})
public class StatusEnhancement {

    @XmlElement(name = "PersonId", required = true)
    protected QueryableInt32 personId;
    @XmlElement(name = "MemberID")
    protected int memberID;
    @XmlElement(name = "SenderCode")
    protected String senderCode;
    @XmlElement(name = "ActivationDate", required = true)
    protected QueryableDateTime activationDate;
    @XmlElement(name = "SenderCardNumber")
    protected String senderCardNumber;
    @XmlElement(name = "SenderDescription")
    protected String senderDescription;
    @XmlElement(name = "SenderDateCeased", required = true)
    protected QueryableDateTime senderDateCeased;
    @XmlElement(name = "SenderUid", required = true)
    protected QueryableInt32 senderUid;
    @XmlElement(name = "SenderCardType")
    protected String senderCardType;
    @XmlElement(name = "SenderCardTypeDesc")
    protected String senderCardTypeDesc;
    @XmlElement(name = "SenderCustomerName")
    protected String senderCustomerName;
    @XmlElement(name = "SenderCardStatus")
    protected String senderCardStatus;
    @XmlElement(name = "SenderCancelReasonCode")
    protected String senderCancelReasonCode;
    @XmlElement(name = "SourceFlag")
    protected String sourceFlag;
    @XmlElement(name = "TierCode")
    protected String tierCode;
    @XmlElement(name = "RecordCreationDate", required = true)
    protected QueryableDateTime recordCreationDate;
    @XmlElement(name = "ActiveCardNo")
    protected String activeCardNo;
    @XmlElement(name = "Title")
    protected String title;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "CorporateCardNo")
    protected String corporateCardNo;
    @XmlElement(name = "CorpCardSeqNo", required = true)
    protected QueryableInt32 corpCardSeqNo;

    /**
     * Gets the value of the personId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPersonId() {
        return personId;
    }

    /**
     * Sets the value of the personId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPersonId(QueryableInt32 value) {
        this.personId = value;
    }

    /**
     * Gets the value of the memberID property.
     * 
     */
    public int getMemberID() {
        return memberID;
    }

    /**
     * Sets the value of the memberID property.
     * 
     */
    public void setMemberID(int value) {
        this.memberID = value;
    }

    /**
     * Gets the value of the senderCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderCode() {
        return senderCode;
    }

    /**
     * Sets the value of the senderCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderCode(String value) {
        this.senderCode = value;
    }

    /**
     * Gets the value of the activationDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getActivationDate() {
        return activationDate;
    }

    /**
     * Sets the value of the activationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setActivationDate(QueryableDateTime value) {
        this.activationDate = value;
    }

    /**
     * Gets the value of the senderCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderCardNumber() {
        return senderCardNumber;
    }

    /**
     * Sets the value of the senderCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderCardNumber(String value) {
        this.senderCardNumber = value;
    }

    /**
     * Gets the value of the senderDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderDescription() {
        return senderDescription;
    }

    /**
     * Sets the value of the senderDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderDescription(String value) {
        this.senderDescription = value;
    }

    /**
     * Gets the value of the senderDateCeased property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getSenderDateCeased() {
        return senderDateCeased;
    }

    /**
     * Sets the value of the senderDateCeased property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setSenderDateCeased(QueryableDateTime value) {
        this.senderDateCeased = value;
    }

    /**
     * Gets the value of the senderUid property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getSenderUid() {
        return senderUid;
    }

    /**
     * Sets the value of the senderUid property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setSenderUid(QueryableInt32 value) {
        this.senderUid = value;
    }

    /**
     * Gets the value of the senderCardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderCardType() {
        return senderCardType;
    }

    /**
     * Sets the value of the senderCardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderCardType(String value) {
        this.senderCardType = value;
    }

    /**
     * Gets the value of the senderCardTypeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderCardTypeDesc() {
        return senderCardTypeDesc;
    }

    /**
     * Sets the value of the senderCardTypeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderCardTypeDesc(String value) {
        this.senderCardTypeDesc = value;
    }

    /**
     * Gets the value of the senderCustomerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderCustomerName() {
        return senderCustomerName;
    }

    /**
     * Sets the value of the senderCustomerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderCustomerName(String value) {
        this.senderCustomerName = value;
    }

    /**
     * Gets the value of the senderCardStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderCardStatus() {
        return senderCardStatus;
    }

    /**
     * Sets the value of the senderCardStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderCardStatus(String value) {
        this.senderCardStatus = value;
    }

    /**
     * Gets the value of the senderCancelReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderCancelReasonCode() {
        return senderCancelReasonCode;
    }

    /**
     * Sets the value of the senderCancelReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderCancelReasonCode(String value) {
        this.senderCancelReasonCode = value;
    }

    /**
     * Gets the value of the sourceFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceFlag() {
        return sourceFlag;
    }

    /**
     * Sets the value of the sourceFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceFlag(String value) {
        this.sourceFlag = value;
    }

    /**
     * Gets the value of the tierCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierCode() {
        return tierCode;
    }

    /**
     * Sets the value of the tierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierCode(String value) {
        this.tierCode = value;
    }

    /**
     * Gets the value of the recordCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getRecordCreationDate() {
        return recordCreationDate;
    }

    /**
     * Sets the value of the recordCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setRecordCreationDate(QueryableDateTime value) {
        this.recordCreationDate = value;
    }

    /**
     * Gets the value of the activeCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveCardNo() {
        return activeCardNo;
    }

    /**
     * Sets the value of the activeCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveCardNo(String value) {
        this.activeCardNo = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the corporateCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporateCardNo() {
        return corporateCardNo;
    }

    /**
     * Sets the value of the corporateCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporateCardNo(String value) {
        this.corporateCardNo = value;
    }

    /**
     * Gets the value of the corpCardSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getCorpCardSeqNo() {
        return corpCardSeqNo;
    }

    /**
     * Sets the value of the corpCardSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setCorpCardSeqNo(QueryableInt32 value) {
        this.corpCardSeqNo = value;
    }

}
