
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="myStatementInputDetails" type="{http://skywards.com/Mercator.CRIS.WS}MyStatementInputWE" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "myStatementInputDetails"
})
@XmlRootElement(name = "MyStatement")
public class MyStatement {

    protected MyStatementInputWE myStatementInputDetails;

    /**
     * Gets the value of the myStatementInputDetails property.
     * 
     * @return
     *     possible object is
     *     {@link MyStatementInputWE }
     *     
     */
    public MyStatementInputWE getMyStatementInputDetails() {
        return myStatementInputDetails;
    }

    /**
     * Sets the value of the myStatementInputDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link MyStatementInputWE }
     *     
     */
    public void setMyStatementInputDetails(MyStatementInputWE value) {
        this.myStatementInputDetails = value;
    }

}
