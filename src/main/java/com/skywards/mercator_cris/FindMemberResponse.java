
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FindMemberResult" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfSkywardsProfile" minOccurs="0"/>
 *         &lt;element name="wsResult" type="{http://skywards.com/Mercator.CRIS.WS}WSIResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "findMemberResult",
    "wsResult"
})
@XmlRootElement(name = "FindMemberResponse")
public class FindMemberResponse {

    @XmlElement(name = "FindMemberResult")
    protected ArrayOfSkywardsProfile findMemberResult;
    protected WSIResult wsResult;

    /**
     * Gets the value of the findMemberResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSkywardsProfile }
     *     
     */
    public ArrayOfSkywardsProfile getFindMemberResult() {
        return findMemberResult;
    }

    /**
     * Sets the value of the findMemberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSkywardsProfile }
     *     
     */
    public void setFindMemberResult(ArrayOfSkywardsProfile value) {
        this.findMemberResult = value;
    }

    /**
     * Gets the value of the wsResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSIResult }
     *     
     */
    public WSIResult getWsResult() {
        return wsResult;
    }

    /**
     * Sets the value of the wsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSIResult }
     *     
     */
    public void setWsResult(WSIResult value) {
        this.wsResult = value;
    }

}
