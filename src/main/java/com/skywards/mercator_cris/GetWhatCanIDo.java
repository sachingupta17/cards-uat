
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="whatCanIdoInputDetails" type="{http://skywards.com/Mercator.CRIS.WS}WhatCanIDoInputWE" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "whatCanIdoInputDetails"
})
@XmlRootElement(name = "GetWhatCanIDo")
public class GetWhatCanIDo {

    protected WhatCanIDoInputWE whatCanIdoInputDetails;

    /**
     * Gets the value of the whatCanIdoInputDetails property.
     * 
     * @return
     *     possible object is
     *     {@link WhatCanIDoInputWE }
     *     
     */
    public WhatCanIDoInputWE getWhatCanIdoInputDetails() {
        return whatCanIdoInputDetails;
    }

    /**
     * Sets the value of the whatCanIdoInputDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link WhatCanIDoInputWE }
     *     
     */
    public void setWhatCanIdoInputDetails(WhatCanIDoInputWE value) {
        this.whatCanIdoInputDetails = value;
    }

}
