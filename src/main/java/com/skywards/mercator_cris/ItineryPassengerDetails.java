
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ItineryPassengerDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItineryPassengerDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaxID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="PnrPaxName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxIndividualName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApisInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ticketno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineryPassengerDetails", propOrder = {
    "paxID",
    "pnrPaxName",
    "paxIndividualName",
    "apisInfo",
    "ticketno"
})
public class ItineryPassengerDetails {

    @XmlElement(name = "PaxID", required = true)
    protected QueryableDecimal paxID;
    @XmlElement(name = "PnrPaxName")
    protected String pnrPaxName;
    @XmlElement(name = "PaxIndividualName")
    protected String paxIndividualName;
    @XmlElement(name = "ApisInfo")
    protected String apisInfo;
    @XmlElement(name = "Ticketno")
    protected String ticketno;

    /**
     * Gets the value of the paxID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getPaxID() {
        return paxID;
    }

    /**
     * Sets the value of the paxID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setPaxID(QueryableDecimal value) {
        this.paxID = value;
    }

    /**
     * Gets the value of the pnrPaxName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnrPaxName() {
        return pnrPaxName;
    }

    /**
     * Sets the value of the pnrPaxName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnrPaxName(String value) {
        this.pnrPaxName = value;
    }

    /**
     * Gets the value of the paxIndividualName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaxIndividualName() {
        return paxIndividualName;
    }

    /**
     * Sets the value of the paxIndividualName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaxIndividualName(String value) {
        this.paxIndividualName = value;
    }

    /**
     * Gets the value of the apisInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApisInfo() {
        return apisInfo;
    }

    /**
     * Sets the value of the apisInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApisInfo(String value) {
        this.apisInfo = value;
    }

    /**
     * Gets the value of the ticketno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketno() {
        return ticketno;
    }

    /**
     * Sets the value of the ticketno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketno(String value) {
        this.ticketno = value;
    }

}
