
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfItineryHeaderCurrent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfItineryHeaderCurrent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItineryHeaderCurrent" type="{http://skywards.com/Mercator.CRIS.WS}ItineryHeaderCurrent" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfItineryHeaderCurrent", propOrder = {
    "itineryHeaderCurrent"
})
public class ArrayOfItineryHeaderCurrent {

    @XmlElement(name = "ItineryHeaderCurrent", nillable = true)
    protected List<ItineryHeaderCurrent> itineryHeaderCurrent;

    /**
     * Gets the value of the itineryHeaderCurrent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itineryHeaderCurrent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItineryHeaderCurrent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItineryHeaderCurrent }
     * 
     * 
     */
    public List<ItineryHeaderCurrent> getItineryHeaderCurrent() {
        if (itineryHeaderCurrent == null) {
            itineryHeaderCurrent = new ArrayList<ItineryHeaderCurrent>();
        }
        return this.itineryHeaderCurrent;
    }

}
