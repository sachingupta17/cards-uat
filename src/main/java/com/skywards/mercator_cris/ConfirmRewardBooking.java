
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RewardID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExternalAuthCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rewardID",
    "ticketNumber",
    "externalAuthCode"
})
@XmlRootElement(name = "ConfirmRewardBooking")
public class ConfirmRewardBooking {

    @XmlElement(name = "RewardID")
    protected int rewardID;
    @XmlElement(name = "TicketNumber")
    protected String ticketNumber;
    @XmlElement(name = "ExternalAuthCode")
    protected String externalAuthCode;

    /**
     * Gets the value of the rewardID property.
     * 
     */
    public int getRewardID() {
        return rewardID;
    }

    /**
     * Sets the value of the rewardID property.
     * 
     */
    public void setRewardID(int value) {
        this.rewardID = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Sets the value of the ticketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Gets the value of the externalAuthCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalAuthCode() {
        return externalAuthCode;
    }

    /**
     * Sets the value of the externalAuthCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalAuthCode(String value) {
        this.externalAuthCode = value;
    }

}
