
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numberOfRecords" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="showCancel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="showZero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="showRewards" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="showFamily" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "personID",
    "numberOfRecords",
    "showCancel",
    "showZero",
    "showRewards",
    "showFamily"
})
@XmlRootElement(name = "GetActivities")
public class GetActivities {

    protected int personID;
    protected int numberOfRecords;
    protected String showCancel;
    protected String showZero;
    protected String showRewards;
    protected String showFamily;

    /**
     * Gets the value of the personID property.
     * 
     */
    public int getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     */
    public void setPersonID(int value) {
        this.personID = value;
    }

    /**
     * Gets the value of the numberOfRecords property.
     * 
     */
    public int getNumberOfRecords() {
        return numberOfRecords;
    }

    /**
     * Sets the value of the numberOfRecords property.
     * 
     */
    public void setNumberOfRecords(int value) {
        this.numberOfRecords = value;
    }

    /**
     * Gets the value of the showCancel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShowCancel() {
        return showCancel;
    }

    /**
     * Sets the value of the showCancel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShowCancel(String value) {
        this.showCancel = value;
    }

    /**
     * Gets the value of the showZero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShowZero() {
        return showZero;
    }

    /**
     * Sets the value of the showZero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShowZero(String value) {
        this.showZero = value;
    }

    /**
     * Gets the value of the showRewards property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShowRewards() {
        return showRewards;
    }

    /**
     * Sets the value of the showRewards property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShowRewards(String value) {
        this.showRewards = value;
    }

    /**
     * Gets the value of the showFamily property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShowFamily() {
        return showFamily;
    }

    /**
     * Sets the value of the showFamily property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShowFamily(String value) {
        this.showFamily = value;
    }

}
