
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UndoableBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UndoableBase">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}BindableBase">
 *       &lt;sequence>
 *         &lt;element name="UserFriendlyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UndoableBase", propOrder = {
    "userFriendlyName"
})
@XmlSeeAlso({
    BusinessBaseOfActivityUploadException.class,
    BusinessBaseOfItineryChaufferDetail.class,
    BusinessBaseOfItineryEmailAddress.class,
    BusinessBaseOfTierDetails.class,
    BusinessBaseOfAchieve.class,
    BusinessBaseOfRenew.class
})
public abstract class UndoableBase
    extends BindableBase
{

    @XmlElement(name = "UserFriendlyName")
    protected String userFriendlyName;

    /**
     * Gets the value of the userFriendlyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserFriendlyName() {
        return userFriendlyName;
    }

    /**
     * Sets the value of the userFriendlyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserFriendlyName(String value) {
        this.userFriendlyName = value;
    }

}
