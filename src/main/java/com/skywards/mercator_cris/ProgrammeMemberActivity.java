
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProgrammeMemberActivity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProgrammeMemberActivity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PersonID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PmaID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PmaType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemUID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="BookableActivityUID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="NumberOfActivityUnits" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="RbeRewardID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PriorityLevel" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="FollowupDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="Merged" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cancelled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecordLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProgressStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BookingDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="Bcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PmaDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="RedeemedStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareBasis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Inv_by_ptnr_date" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="Ptnr_inv_paid_date" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="Act_unit_yield" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PointsRedeemable" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="AgentIataCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ipa_ipa_id" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="AubID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PmaMergedFromID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="AssignedTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Pfp_flown_pax_id" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt64"/>
 *         &lt;element name="CabinClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlownClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MktCarrier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MktFlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OprCarrier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OprFlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DownloadForBilling" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillApproved" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Uma_rec_number" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt64"/>
 *         &lt;element name="BilledDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="AgentCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RevCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PNRName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HuetCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RpsCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rps_code_after_time_limit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rps_date_after_time_limit" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="NccCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AarCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="BilledCancelledAct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rejected_from_aub_id" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="PtaNumber" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="CouponNumber" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="SystemCalculated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DirectBookableActivity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Dsp_bac_code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OnHold" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Miles" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="TierMiles" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="CancelDesp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MrgCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MrgMemberName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityMethodDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SetActivityMethodDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApiType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutoCredit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartnerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirlineCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Origin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Destination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Diff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActiveCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsertRejectedActivities" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NewRowStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ForceUpdate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="TerminalID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExternalAuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumberOfUnits" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="IsCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Accrual" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="BalanceInPoints" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="BalanceInCurrency" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MobileNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProgrammeMemberActivity", propOrder = {
    "personID",
    "pmaID",
    "pmaType",
    "memUID",
    "bookableActivityUID",
    "numberOfActivityUnits",
    "rbeRewardID",
    "priorityLevel",
    "followupDate",
    "merged",
    "cancelled",
    "recordLocator",
    "progressStatus",
    "bookingDate",
    "bcp",
    "pmaDate",
    "redeemedStatus",
    "fareBasis",
    "invByPtnrDate",
    "ptnrInvPaidDate",
    "actUnitYield",
    "pointsRedeemable",
    "agentIataCode",
    "ipaIpaId",
    "aubID",
    "pmaMergedFromID",
    "assignedTo",
    "pfpFlownPaxId",
    "cabinClass",
    "ticketNumber",
    "flownClass",
    "mktCarrier",
    "mktFlightNumber",
    "oprCarrier",
    "oprFlightNumber",
    "downloadForBilling",
    "billApproved",
    "umaRecNumber",
    "billedDate",
    "agentCity",
    "revCode",
    "pnrName",
    "huetCode",
    "rpsCode",
    "rpsCodeAfterTimeLimit",
    "rpsDateAfterTimeLimit",
    "nccCode",
    "activityMethod",
    "aarCode",
    "invoiceNumber",
    "invoiceDate",
    "billedCancelledAct",
    "rejectedFromAubId",
    "ptaNumber",
    "couponNumber",
    "systemCalculated",
    "paxDescription",
    "directBookableActivity",
    "dspBacCode",
    "onHold",
    "miles",
    "tierMiles",
    "cancelDesp",
    "mrgCardNumber",
    "mrgMemberName",
    "activityMethodDescription",
    "setActivityMethodDescription",
    "activityType",
    "apiType",
    "autoCredit",
    "partnerCode",
    "airlineCode",
    "activityNumber",
    "origin",
    "destination",
    "diff",
    "activeCardNo",
    "insertRejectedActivities",
    "newRowStatus",
    "forceUpdate",
    "terminalID",
    "externalAuthorizationCode",
    "numberOfUnits",
    "isCurrency",
    "accrual",
    "balanceInPoints",
    "balanceInCurrency",
    "source",
    "emailId",
    "mobileNo"
})
public class ProgrammeMemberActivity {

    @XmlElement(name = "PersonID", required = true)
    protected QueryableInt32 personID;
    @XmlElement(name = "PmaID", required = true)
    protected QueryableInt32 pmaID;
    @XmlElement(name = "PmaType")
    protected String pmaType;
    @XmlElement(name = "MemUID", required = true)
    protected QueryableInt32 memUID;
    @XmlElement(name = "BookableActivityUID", required = true)
    protected QueryableInt32 bookableActivityUID;
    @XmlElement(name = "NumberOfActivityUnits", required = true)
    protected QueryableInt32 numberOfActivityUnits;
    @XmlElement(name = "RbeRewardID", required = true)
    protected QueryableInt32 rbeRewardID;
    @XmlElement(name = "PriorityLevel", required = true)
    protected QueryableInt32 priorityLevel;
    @XmlElement(name = "FollowupDate", required = true)
    protected QueryableDateTime followupDate;
    @XmlElement(name = "Merged")
    protected String merged;
    @XmlElement(name = "Cancelled")
    protected String cancelled;
    @XmlElement(name = "RecordLocator")
    protected String recordLocator;
    @XmlElement(name = "ProgressStatus")
    protected String progressStatus;
    @XmlElement(name = "BookingDate", required = true)
    protected QueryableDateTime bookingDate;
    @XmlElement(name = "Bcp")
    protected String bcp;
    @XmlElement(name = "PmaDate", required = true)
    protected QueryableDateTime pmaDate;
    @XmlElement(name = "RedeemedStatus")
    protected String redeemedStatus;
    @XmlElement(name = "FareBasis")
    protected String fareBasis;
    @XmlElement(name = "Inv_by_ptnr_date", required = true)
    protected QueryableDateTime invByPtnrDate;
    @XmlElement(name = "Ptnr_inv_paid_date", required = true)
    protected QueryableDateTime ptnrInvPaidDate;
    @XmlElement(name = "Act_unit_yield", required = true)
    protected QueryableInt32 actUnitYield;
    @XmlElement(name = "PointsRedeemable", required = true)
    protected QueryableInt32 pointsRedeemable;
    @XmlElement(name = "AgentIataCode")
    protected String agentIataCode;
    @XmlElement(name = "Ipa_ipa_id", required = true)
    protected QueryableInt32 ipaIpaId;
    @XmlElement(name = "AubID", required = true)
    protected QueryableInt32 aubID;
    @XmlElement(name = "PmaMergedFromID", required = true)
    protected QueryableInt32 pmaMergedFromID;
    @XmlElement(name = "AssignedTo")
    protected String assignedTo;
    @XmlElement(name = "Pfp_flown_pax_id", required = true)
    protected QueryableInt64 pfpFlownPaxId;
    @XmlElement(name = "CabinClass")
    protected String cabinClass;
    @XmlElement(name = "TicketNumber")
    protected String ticketNumber;
    @XmlElement(name = "FlownClass")
    protected String flownClass;
    @XmlElement(name = "MktCarrier")
    protected String mktCarrier;
    @XmlElement(name = "MktFlightNumber")
    protected String mktFlightNumber;
    @XmlElement(name = "OprCarrier")
    protected String oprCarrier;
    @XmlElement(name = "OprFlightNumber")
    protected String oprFlightNumber;
    @XmlElement(name = "DownloadForBilling")
    protected String downloadForBilling;
    @XmlElement(name = "BillApproved")
    protected String billApproved;
    @XmlElement(name = "Uma_rec_number", required = true)
    protected QueryableInt64 umaRecNumber;
    @XmlElement(name = "BilledDate", required = true)
    protected QueryableDateTime billedDate;
    @XmlElement(name = "AgentCity")
    protected String agentCity;
    @XmlElement(name = "RevCode")
    protected String revCode;
    @XmlElement(name = "PNRName")
    protected String pnrName;
    @XmlElement(name = "HuetCode")
    protected String huetCode;
    @XmlElement(name = "RpsCode")
    protected String rpsCode;
    @XmlElement(name = "Rps_code_after_time_limit")
    protected String rpsCodeAfterTimeLimit;
    @XmlElement(name = "Rps_date_after_time_limit", required = true)
    protected QueryableDateTime rpsDateAfterTimeLimit;
    @XmlElement(name = "NccCode")
    protected String nccCode;
    @XmlElement(name = "ActivityMethod")
    protected String activityMethod;
    @XmlElement(name = "AarCode")
    protected String aarCode;
    @XmlElement(name = "InvoiceNumber")
    protected String invoiceNumber;
    @XmlElement(name = "InvoiceDate", required = true)
    protected QueryableDateTime invoiceDate;
    @XmlElement(name = "BilledCancelledAct")
    protected String billedCancelledAct;
    @XmlElement(name = "Rejected_from_aub_id", required = true)
    protected QueryableInt32 rejectedFromAubId;
    @XmlElement(name = "PtaNumber", required = true)
    protected QueryableInt32 ptaNumber;
    @XmlElement(name = "CouponNumber", required = true)
    protected QueryableInt32 couponNumber;
    @XmlElement(name = "SystemCalculated")
    protected String systemCalculated;
    @XmlElement(name = "PaxDescription")
    protected String paxDescription;
    @XmlElement(name = "DirectBookableActivity")
    protected String directBookableActivity;
    @XmlElement(name = "Dsp_bac_code")
    protected String dspBacCode;
    @XmlElement(name = "OnHold")
    protected String onHold;
    @XmlElement(name = "Miles", required = true)
    protected QueryableInt32 miles;
    @XmlElement(name = "TierMiles", required = true)
    protected QueryableInt32 tierMiles;
    @XmlElement(name = "CancelDesp")
    protected String cancelDesp;
    @XmlElement(name = "MrgCardNumber")
    protected String mrgCardNumber;
    @XmlElement(name = "MrgMemberName")
    protected String mrgMemberName;
    @XmlElement(name = "ActivityMethodDescription")
    protected String activityMethodDescription;
    @XmlElement(name = "SetActivityMethodDescription")
    protected String setActivityMethodDescription;
    @XmlElement(name = "ActivityType")
    protected String activityType;
    @XmlElement(name = "ApiType")
    protected String apiType;
    @XmlElement(name = "AutoCredit")
    protected String autoCredit;
    @XmlElement(name = "PartnerCode")
    protected String partnerCode;
    @XmlElement(name = "AirlineCode")
    protected String airlineCode;
    @XmlElement(name = "ActivityNumber")
    protected String activityNumber;
    @XmlElement(name = "Origin")
    protected String origin;
    @XmlElement(name = "Destination")
    protected String destination;
    @XmlElement(name = "Diff")
    protected String diff;
    @XmlElement(name = "ActiveCardNo")
    protected String activeCardNo;
    @XmlElement(name = "InsertRejectedActivities")
    protected boolean insertRejectedActivities;
    @XmlElement(name = "NewRowStatus")
    protected boolean newRowStatus;
    @XmlElement(name = "ForceUpdate")
    protected boolean forceUpdate;
    @XmlElement(name = "TerminalID")
    protected String terminalID;
    @XmlElement(name = "ExternalAuthorizationCode")
    protected String externalAuthorizationCode;
    @XmlElement(name = "NumberOfUnits", required = true)
    protected QueryableInt32 numberOfUnits;
    @XmlElement(name = "IsCurrency")
    protected String isCurrency;
    @XmlElement(name = "Accrual", required = true)
    protected QueryableDecimal accrual;
    @XmlElement(name = "BalanceInPoints", required = true)
    protected QueryableDecimal balanceInPoints;
    @XmlElement(name = "BalanceInCurrency", required = true)
    protected QueryableDecimal balanceInCurrency;
    @XmlElement(name = "Source")
    protected String source;
    @XmlElement(name = "EmailId")
    protected String emailId;
    @XmlElement(name = "MobileNo")
    protected String mobileNo;

    /**
     * Gets the value of the personID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPersonID(QueryableInt32 value) {
        this.personID = value;
    }

    /**
     * Gets the value of the pmaID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPmaID() {
        return pmaID;
    }

    /**
     * Sets the value of the pmaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPmaID(QueryableInt32 value) {
        this.pmaID = value;
    }

    /**
     * Gets the value of the pmaType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPmaType() {
        return pmaType;
    }

    /**
     * Sets the value of the pmaType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPmaType(String value) {
        this.pmaType = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setMemUID(QueryableInt32 value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the bookableActivityUID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getBookableActivityUID() {
        return bookableActivityUID;
    }

    /**
     * Sets the value of the bookableActivityUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setBookableActivityUID(QueryableInt32 value) {
        this.bookableActivityUID = value;
    }

    /**
     * Gets the value of the numberOfActivityUnits property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getNumberOfActivityUnits() {
        return numberOfActivityUnits;
    }

    /**
     * Sets the value of the numberOfActivityUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setNumberOfActivityUnits(QueryableInt32 value) {
        this.numberOfActivityUnits = value;
    }

    /**
     * Gets the value of the rbeRewardID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getRbeRewardID() {
        return rbeRewardID;
    }

    /**
     * Sets the value of the rbeRewardID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setRbeRewardID(QueryableInt32 value) {
        this.rbeRewardID = value;
    }

    /**
     * Gets the value of the priorityLevel property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Sets the value of the priorityLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPriorityLevel(QueryableInt32 value) {
        this.priorityLevel = value;
    }

    /**
     * Gets the value of the followupDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getFollowupDate() {
        return followupDate;
    }

    /**
     * Sets the value of the followupDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setFollowupDate(QueryableDateTime value) {
        this.followupDate = value;
    }

    /**
     * Gets the value of the merged property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerged() {
        return merged;
    }

    /**
     * Sets the value of the merged property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerged(String value) {
        this.merged = value;
    }

    /**
     * Gets the value of the cancelled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelled() {
        return cancelled;
    }

    /**
     * Sets the value of the cancelled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelled(String value) {
        this.cancelled = value;
    }

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordLocator(String value) {
        this.recordLocator = value;
    }

    /**
     * Gets the value of the progressStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgressStatus() {
        return progressStatus;
    }

    /**
     * Sets the value of the progressStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgressStatus(String value) {
        this.progressStatus = value;
    }

    /**
     * Gets the value of the bookingDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getBookingDate() {
        return bookingDate;
    }

    /**
     * Sets the value of the bookingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setBookingDate(QueryableDateTime value) {
        this.bookingDate = value;
    }

    /**
     * Gets the value of the bcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBcp() {
        return bcp;
    }

    /**
     * Sets the value of the bcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBcp(String value) {
        this.bcp = value;
    }

    /**
     * Gets the value of the pmaDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getPmaDate() {
        return pmaDate;
    }

    /**
     * Sets the value of the pmaDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setPmaDate(QueryableDateTime value) {
        this.pmaDate = value;
    }

    /**
     * Gets the value of the redeemedStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedeemedStatus() {
        return redeemedStatus;
    }

    /**
     * Sets the value of the redeemedStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedeemedStatus(String value) {
        this.redeemedStatus = value;
    }

    /**
     * Gets the value of the fareBasis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasis() {
        return fareBasis;
    }

    /**
     * Sets the value of the fareBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasis(String value) {
        this.fareBasis = value;
    }

    /**
     * Gets the value of the invByPtnrDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getInvByPtnrDate() {
        return invByPtnrDate;
    }

    /**
     * Sets the value of the invByPtnrDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setInvByPtnrDate(QueryableDateTime value) {
        this.invByPtnrDate = value;
    }

    /**
     * Gets the value of the ptnrInvPaidDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getPtnrInvPaidDate() {
        return ptnrInvPaidDate;
    }

    /**
     * Sets the value of the ptnrInvPaidDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setPtnrInvPaidDate(QueryableDateTime value) {
        this.ptnrInvPaidDate = value;
    }

    /**
     * Gets the value of the actUnitYield property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getActUnitYield() {
        return actUnitYield;
    }

    /**
     * Sets the value of the actUnitYield property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setActUnitYield(QueryableInt32 value) {
        this.actUnitYield = value;
    }

    /**
     * Gets the value of the pointsRedeemable property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPointsRedeemable() {
        return pointsRedeemable;
    }

    /**
     * Sets the value of the pointsRedeemable property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPointsRedeemable(QueryableInt32 value) {
        this.pointsRedeemable = value;
    }

    /**
     * Gets the value of the agentIataCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentIataCode() {
        return agentIataCode;
    }

    /**
     * Sets the value of the agentIataCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentIataCode(String value) {
        this.agentIataCode = value;
    }

    /**
     * Gets the value of the ipaIpaId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getIpaIpaId() {
        return ipaIpaId;
    }

    /**
     * Sets the value of the ipaIpaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setIpaIpaId(QueryableInt32 value) {
        this.ipaIpaId = value;
    }

    /**
     * Gets the value of the aubID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getAubID() {
        return aubID;
    }

    /**
     * Sets the value of the aubID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setAubID(QueryableInt32 value) {
        this.aubID = value;
    }

    /**
     * Gets the value of the pmaMergedFromID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPmaMergedFromID() {
        return pmaMergedFromID;
    }

    /**
     * Sets the value of the pmaMergedFromID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPmaMergedFromID(QueryableInt32 value) {
        this.pmaMergedFromID = value;
    }

    /**
     * Gets the value of the assignedTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedTo() {
        return assignedTo;
    }

    /**
     * Sets the value of the assignedTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedTo(String value) {
        this.assignedTo = value;
    }

    /**
     * Gets the value of the pfpFlownPaxId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt64 }
     *     
     */
    public QueryableInt64 getPfpFlownPaxId() {
        return pfpFlownPaxId;
    }

    /**
     * Sets the value of the pfpFlownPaxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt64 }
     *     
     */
    public void setPfpFlownPaxId(QueryableInt64 value) {
        this.pfpFlownPaxId = value;
    }

    /**
     * Gets the value of the cabinClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabinClass() {
        return cabinClass;
    }

    /**
     * Sets the value of the cabinClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabinClass(String value) {
        this.cabinClass = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Sets the value of the ticketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Gets the value of the flownClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlownClass() {
        return flownClass;
    }

    /**
     * Sets the value of the flownClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlownClass(String value) {
        this.flownClass = value;
    }

    /**
     * Gets the value of the mktCarrier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMktCarrier() {
        return mktCarrier;
    }

    /**
     * Sets the value of the mktCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMktCarrier(String value) {
        this.mktCarrier = value;
    }

    /**
     * Gets the value of the mktFlightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMktFlightNumber() {
        return mktFlightNumber;
    }

    /**
     * Sets the value of the mktFlightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMktFlightNumber(String value) {
        this.mktFlightNumber = value;
    }

    /**
     * Gets the value of the oprCarrier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOprCarrier() {
        return oprCarrier;
    }

    /**
     * Sets the value of the oprCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOprCarrier(String value) {
        this.oprCarrier = value;
    }

    /**
     * Gets the value of the oprFlightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOprFlightNumber() {
        return oprFlightNumber;
    }

    /**
     * Sets the value of the oprFlightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOprFlightNumber(String value) {
        this.oprFlightNumber = value;
    }

    /**
     * Gets the value of the downloadForBilling property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDownloadForBilling() {
        return downloadForBilling;
    }

    /**
     * Sets the value of the downloadForBilling property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDownloadForBilling(String value) {
        this.downloadForBilling = value;
    }

    /**
     * Gets the value of the billApproved property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillApproved() {
        return billApproved;
    }

    /**
     * Sets the value of the billApproved property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillApproved(String value) {
        this.billApproved = value;
    }

    /**
     * Gets the value of the umaRecNumber property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt64 }
     *     
     */
    public QueryableInt64 getUmaRecNumber() {
        return umaRecNumber;
    }

    /**
     * Sets the value of the umaRecNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt64 }
     *     
     */
    public void setUmaRecNumber(QueryableInt64 value) {
        this.umaRecNumber = value;
    }

    /**
     * Gets the value of the billedDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getBilledDate() {
        return billedDate;
    }

    /**
     * Sets the value of the billedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setBilledDate(QueryableDateTime value) {
        this.billedDate = value;
    }

    /**
     * Gets the value of the agentCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentCity() {
        return agentCity;
    }

    /**
     * Sets the value of the agentCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentCity(String value) {
        this.agentCity = value;
    }

    /**
     * Gets the value of the revCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevCode() {
        return revCode;
    }

    /**
     * Sets the value of the revCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevCode(String value) {
        this.revCode = value;
    }

    /**
     * Gets the value of the pnrName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPNRName() {
        return pnrName;
    }

    /**
     * Sets the value of the pnrName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPNRName(String value) {
        this.pnrName = value;
    }

    /**
     * Gets the value of the huetCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHuetCode() {
        return huetCode;
    }

    /**
     * Sets the value of the huetCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHuetCode(String value) {
        this.huetCode = value;
    }

    /**
     * Gets the value of the rpsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRpsCode() {
        return rpsCode;
    }

    /**
     * Sets the value of the rpsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRpsCode(String value) {
        this.rpsCode = value;
    }

    /**
     * Gets the value of the rpsCodeAfterTimeLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRpsCodeAfterTimeLimit() {
        return rpsCodeAfterTimeLimit;
    }

    /**
     * Sets the value of the rpsCodeAfterTimeLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRpsCodeAfterTimeLimit(String value) {
        this.rpsCodeAfterTimeLimit = value;
    }

    /**
     * Gets the value of the rpsDateAfterTimeLimit property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getRpsDateAfterTimeLimit() {
        return rpsDateAfterTimeLimit;
    }

    /**
     * Sets the value of the rpsDateAfterTimeLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setRpsDateAfterTimeLimit(QueryableDateTime value) {
        this.rpsDateAfterTimeLimit = value;
    }

    /**
     * Gets the value of the nccCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNccCode() {
        return nccCode;
    }

    /**
     * Sets the value of the nccCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNccCode(String value) {
        this.nccCode = value;
    }

    /**
     * Gets the value of the activityMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityMethod() {
        return activityMethod;
    }

    /**
     * Sets the value of the activityMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityMethod(String value) {
        this.activityMethod = value;
    }

    /**
     * Gets the value of the aarCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAarCode() {
        return aarCode;
    }

    /**
     * Sets the value of the aarCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAarCode(String value) {
        this.aarCode = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNumber(String value) {
        this.invoiceNumber = value;
    }

    /**
     * Gets the value of the invoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Sets the value of the invoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setInvoiceDate(QueryableDateTime value) {
        this.invoiceDate = value;
    }

    /**
     * Gets the value of the billedCancelledAct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledCancelledAct() {
        return billedCancelledAct;
    }

    /**
     * Sets the value of the billedCancelledAct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledCancelledAct(String value) {
        this.billedCancelledAct = value;
    }

    /**
     * Gets the value of the rejectedFromAubId property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getRejectedFromAubId() {
        return rejectedFromAubId;
    }

    /**
     * Sets the value of the rejectedFromAubId property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setRejectedFromAubId(QueryableInt32 value) {
        this.rejectedFromAubId = value;
    }

    /**
     * Gets the value of the ptaNumber property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPtaNumber() {
        return ptaNumber;
    }

    /**
     * Sets the value of the ptaNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPtaNumber(QueryableInt32 value) {
        this.ptaNumber = value;
    }

    /**
     * Gets the value of the couponNumber property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getCouponNumber() {
        return couponNumber;
    }

    /**
     * Sets the value of the couponNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setCouponNumber(QueryableInt32 value) {
        this.couponNumber = value;
    }

    /**
     * Gets the value of the systemCalculated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemCalculated() {
        return systemCalculated;
    }

    /**
     * Sets the value of the systemCalculated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemCalculated(String value) {
        this.systemCalculated = value;
    }

    /**
     * Gets the value of the paxDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaxDescription() {
        return paxDescription;
    }

    /**
     * Sets the value of the paxDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaxDescription(String value) {
        this.paxDescription = value;
    }

    /**
     * Gets the value of the directBookableActivity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectBookableActivity() {
        return directBookableActivity;
    }

    /**
     * Sets the value of the directBookableActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectBookableActivity(String value) {
        this.directBookableActivity = value;
    }

    /**
     * Gets the value of the dspBacCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDspBacCode() {
        return dspBacCode;
    }

    /**
     * Sets the value of the dspBacCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDspBacCode(String value) {
        this.dspBacCode = value;
    }

    /**
     * Gets the value of the onHold property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnHold() {
        return onHold;
    }

    /**
     * Sets the value of the onHold property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnHold(String value) {
        this.onHold = value;
    }

    /**
     * Gets the value of the miles property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getMiles() {
        return miles;
    }

    /**
     * Sets the value of the miles property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setMiles(QueryableInt32 value) {
        this.miles = value;
    }

    /**
     * Gets the value of the tierMiles property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getTierMiles() {
        return tierMiles;
    }

    /**
     * Sets the value of the tierMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setTierMiles(QueryableInt32 value) {
        this.tierMiles = value;
    }

    /**
     * Gets the value of the cancelDesp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelDesp() {
        return cancelDesp;
    }

    /**
     * Sets the value of the cancelDesp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelDesp(String value) {
        this.cancelDesp = value;
    }

    /**
     * Gets the value of the mrgCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMrgCardNumber() {
        return mrgCardNumber;
    }

    /**
     * Sets the value of the mrgCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMrgCardNumber(String value) {
        this.mrgCardNumber = value;
    }

    /**
     * Gets the value of the mrgMemberName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMrgMemberName() {
        return mrgMemberName;
    }

    /**
     * Sets the value of the mrgMemberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMrgMemberName(String value) {
        this.mrgMemberName = value;
    }

    /**
     * Gets the value of the activityMethodDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityMethodDescription() {
        return activityMethodDescription;
    }

    /**
     * Sets the value of the activityMethodDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityMethodDescription(String value) {
        this.activityMethodDescription = value;
    }

    /**
     * Gets the value of the setActivityMethodDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetActivityMethodDescription() {
        return setActivityMethodDescription;
    }

    /**
     * Sets the value of the setActivityMethodDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetActivityMethodDescription(String value) {
        this.setActivityMethodDescription = value;
    }

    /**
     * Gets the value of the activityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * Sets the value of the activityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityType(String value) {
        this.activityType = value;
    }

    /**
     * Gets the value of the apiType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiType() {
        return apiType;
    }

    /**
     * Sets the value of the apiType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiType(String value) {
        this.apiType = value;
    }

    /**
     * Gets the value of the autoCredit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoCredit() {
        return autoCredit;
    }

    /**
     * Sets the value of the autoCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoCredit(String value) {
        this.autoCredit = value;
    }

    /**
     * Gets the value of the partnerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerCode() {
        return partnerCode;
    }

    /**
     * Sets the value of the partnerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerCode(String value) {
        this.partnerCode = value;
    }

    /**
     * Gets the value of the airlineCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineCode() {
        return airlineCode;
    }

    /**
     * Sets the value of the airlineCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineCode(String value) {
        this.airlineCode = value;
    }

    /**
     * Gets the value of the activityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityNumber() {
        return activityNumber;
    }

    /**
     * Sets the value of the activityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityNumber(String value) {
        this.activityNumber = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Gets the value of the destination property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Gets the value of the diff property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiff() {
        return diff;
    }

    /**
     * Sets the value of the diff property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiff(String value) {
        this.diff = value;
    }

    /**
     * Gets the value of the activeCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveCardNo() {
        return activeCardNo;
    }

    /**
     * Sets the value of the activeCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveCardNo(String value) {
        this.activeCardNo = value;
    }

    /**
     * Gets the value of the insertRejectedActivities property.
     * 
     */
    public boolean isInsertRejectedActivities() {
        return insertRejectedActivities;
    }

    /**
     * Sets the value of the insertRejectedActivities property.
     * 
     */
    public void setInsertRejectedActivities(boolean value) {
        this.insertRejectedActivities = value;
    }

    /**
     * Gets the value of the newRowStatus property.
     * 
     */
    public boolean isNewRowStatus() {
        return newRowStatus;
    }

    /**
     * Sets the value of the newRowStatus property.
     * 
     */
    public void setNewRowStatus(boolean value) {
        this.newRowStatus = value;
    }

    /**
     * Gets the value of the forceUpdate property.
     * 
     */
    public boolean isForceUpdate() {
        return forceUpdate;
    }

    /**
     * Sets the value of the forceUpdate property.
     * 
     */
    public void setForceUpdate(boolean value) {
        this.forceUpdate = value;
    }

    /**
     * Gets the value of the terminalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalID() {
        return terminalID;
    }

    /**
     * Sets the value of the terminalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalID(String value) {
        this.terminalID = value;
    }

    /**
     * Gets the value of the externalAuthorizationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalAuthorizationCode() {
        return externalAuthorizationCode;
    }

    /**
     * Sets the value of the externalAuthorizationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalAuthorizationCode(String value) {
        this.externalAuthorizationCode = value;
    }

    /**
     * Gets the value of the numberOfUnits property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getNumberOfUnits() {
        return numberOfUnits;
    }

    /**
     * Sets the value of the numberOfUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setNumberOfUnits(QueryableInt32 value) {
        this.numberOfUnits = value;
    }

    /**
     * Gets the value of the isCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCurrency() {
        return isCurrency;
    }

    /**
     * Sets the value of the isCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCurrency(String value) {
        this.isCurrency = value;
    }

    /**
     * Gets the value of the accrual property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getAccrual() {
        return accrual;
    }

    /**
     * Sets the value of the accrual property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setAccrual(QueryableDecimal value) {
        this.accrual = value;
    }

    /**
     * Gets the value of the balanceInPoints property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getBalanceInPoints() {
        return balanceInPoints;
    }

    /**
     * Sets the value of the balanceInPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setBalanceInPoints(QueryableDecimal value) {
        this.balanceInPoints = value;
    }

    /**
     * Gets the value of the balanceInCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getBalanceInCurrency() {
        return balanceInCurrency;
    }

    /**
     * Sets the value of the balanceInCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setBalanceInCurrency(QueryableDecimal value) {
        this.balanceInCurrency = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the emailId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Sets the value of the emailId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailId(String value) {
        this.emailId = value;
    }

    /**
     * Gets the value of the mobileNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * Sets the value of the mobileNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileNo(String value) {
        this.mobileNo = value;
    }

}
