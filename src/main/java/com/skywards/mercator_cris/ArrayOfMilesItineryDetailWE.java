
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfMilesItineryDetailWE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfMilesItineryDetailWE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MilesItineryDetailWE" type="{http://skywards.com/Mercator.CRIS.WS}MilesItineryDetailWE" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfMilesItineryDetailWE", propOrder = {
    "milesItineryDetailWE"
})
public class ArrayOfMilesItineryDetailWE {

    @XmlElement(name = "MilesItineryDetailWE", nillable = true)
    protected List<MilesItineryDetailWE> milesItineryDetailWE;

    /**
     * Gets the value of the milesItineryDetailWE property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the milesItineryDetailWE property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMilesItineryDetailWE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MilesItineryDetailWE }
     * 
     * 
     */
    public List<MilesItineryDetailWE> getMilesItineryDetailWE() {
        if (milesItineryDetailWE == null) {
            milesItineryDetailWE = new ArrayList<MilesItineryDetailWE>();
        }
        return this.milesItineryDetailWE;
    }

}
