
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConfirmTicketCheckResult" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfRetroAccrualSummary" minOccurs="0"/>
 *         &lt;element name="wsResult" type="{http://skywards.com/Mercator.CRIS.WS}WSIResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "confirmTicketCheckResult",
    "wsResult"
})
@XmlRootElement(name = "ConfirmTicketCheckResponse")
public class ConfirmTicketCheckResponse {

    @XmlElement(name = "ConfirmTicketCheckResult")
    protected ArrayOfRetroAccrualSummary confirmTicketCheckResult;
    protected WSIResult wsResult;

    /**
     * Gets the value of the confirmTicketCheckResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRetroAccrualSummary }
     *     
     */
    public ArrayOfRetroAccrualSummary getConfirmTicketCheckResult() {
        return confirmTicketCheckResult;
    }

    /**
     * Sets the value of the confirmTicketCheckResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRetroAccrualSummary }
     *     
     */
    public void setConfirmTicketCheckResult(ArrayOfRetroAccrualSummary value) {
        this.confirmTicketCheckResult = value;
    }

    /**
     * Gets the value of the wsResult property.
     * 
     * @return
     *     possible object is
     *     {@link WSIResult }
     *     
     */
    public WSIResult getWsResult() {
        return wsResult;
    }

    /**
     * Sets the value of the wsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSIResult }
     *     
     */
    public void setWsResult(WSIResult value) {
        this.wsResult = value;
    }

}
