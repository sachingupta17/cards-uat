
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="programmeMemberEntities" type="{http://skywards.com/Mercator.CRIS.WS}ArrayOfProgrammeMemberActivity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "programmeMemberEntities"
})
@XmlRootElement(name = "CreateAccrualActivity")
public class CreateAccrualActivity {

    protected ArrayOfProgrammeMemberActivity programmeMemberEntities;

    /**
     * Gets the value of the programmeMemberEntities property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProgrammeMemberActivity }
     *     
     */
    public ArrayOfProgrammeMemberActivity getProgrammeMemberEntities() {
        return programmeMemberEntities;
    }

    /**
     * Sets the value of the programmeMemberEntities property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProgrammeMemberActivity }
     *     
     */
    public void setProgrammeMemberEntities(ArrayOfProgrammeMemberActivity value) {
        this.programmeMemberEntities = value;
    }

}
