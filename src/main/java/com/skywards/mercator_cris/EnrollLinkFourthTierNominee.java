
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FourthTierMemcardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="skywardsProfile" type="{http://skywards.com/Mercator.CRIS.WS}SkywardsProfile" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fourthTierMemcardNo",
    "skywardsProfile"
})
@XmlRootElement(name = "EnrollLinkFourthTierNominee")
public class EnrollLinkFourthTierNominee {

    @XmlElement(name = "FourthTierMemcardNo")
    protected String fourthTierMemcardNo;
    protected SkywardsProfile skywardsProfile;

    /**
     * Gets the value of the fourthTierMemcardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFourthTierMemcardNo() {
        return fourthTierMemcardNo;
    }

    /**
     * Sets the value of the fourthTierMemcardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFourthTierMemcardNo(String value) {
        this.fourthTierMemcardNo = value;
    }

    /**
     * Gets the value of the skywardsProfile property.
     * 
     * @return
     *     possible object is
     *     {@link SkywardsProfile }
     *     
     */
    public SkywardsProfile getSkywardsProfile() {
        return skywardsProfile;
    }

    /**
     * Sets the value of the skywardsProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link SkywardsProfile }
     *     
     */
    public void setSkywardsProfile(SkywardsProfile value) {
        this.skywardsProfile = value;
    }

}
