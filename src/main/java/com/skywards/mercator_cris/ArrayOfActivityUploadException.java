
package com.skywards.mercator_cris;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfActivityUploadException complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfActivityUploadException">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActivityUploadException" type="{http://skywards.com/Mercator.CRIS.WS}ActivityUploadException" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfActivityUploadException", propOrder = {
    "activityUploadException"
})
public class ArrayOfActivityUploadException {

    @XmlElement(name = "ActivityUploadException", nillable = true)
    protected List<ActivityUploadException> activityUploadException;

    /**
     * Gets the value of the activityUploadException property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activityUploadException property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivityUploadException().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityUploadException }
     * 
     * 
     */
    public List<ActivityUploadException> getActivityUploadException() {
        if (activityUploadException == null) {
            activityUploadException = new ArrayList<ActivityUploadException>();
        }
        return this.activityUploadException;
    }

}
