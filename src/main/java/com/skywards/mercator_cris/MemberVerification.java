
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MembershipCardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VerifyEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VerifyMobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "membershipCardNo",
    "verifyEmail",
    "verifyMobile"
})
@XmlRootElement(name = "MemberVerification")
public class MemberVerification {

    @XmlElement(name = "MembershipCardNo")
    protected String membershipCardNo;
    @XmlElement(name = "VerifyEmail")
    protected String verifyEmail;
    @XmlElement(name = "VerifyMobile")
    protected String verifyMobile;

    /**
     * Gets the value of the membershipCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMembershipCardNo() {
        return membershipCardNo;
    }

    /**
     * Sets the value of the membershipCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMembershipCardNo(String value) {
        this.membershipCardNo = value;
    }

    /**
     * Gets the value of the verifyEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerifyEmail() {
        return verifyEmail;
    }

    /**
     * Sets the value of the verifyEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerifyEmail(String value) {
        this.verifyEmail = value;
    }

    /**
     * Gets the value of the verifyMobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerifyMobile() {
        return verifyMobile;
    }

    /**
     * Sets the value of the verifyMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerifyMobile(String value) {
        this.verifyMobile = value;
    }

}
