
package com.skywards.mercator_cris;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ActivityUploadException complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityUploadException">
 *   &lt;complexContent>
 *     &lt;extension base="{http://skywards.com/Mercator.CRIS.WS}CRISBusinessBaseOfActivityUploadException">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AubId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AubDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemUid" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PersonId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ProfileLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfileFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfileTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfileNameSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MbtCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PtnrCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ClassOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SgmtCityPair" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareBasis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Diff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfActUnits" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AgentIataCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AmexNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AmexTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Market" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AmexDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CabinClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UmaLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UmaFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlownClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UmaTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MktCarrier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MktFltNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OprCarrier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OprFltNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgentCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UpldStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HbackStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cancelled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PfpFlownPaxId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="UmaNameSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PnrName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RejectedFromAubId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RevCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfileMiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityUploadException", propOrder = {
    "id",
    "status",
    "aubId",
    "aubDesc",
    "memUid",
    "personId",
    "profileLastName",
    "profileFirstName",
    "profileTitle",
    "profileNameSuffix",
    "mbtCode",
    "cardNo",
    "ptnrCode",
    "actTypeCode",
    "actNo",
    "actDate",
    "classOfService",
    "sgmtCityPair",
    "fareBasis",
    "diff",
    "numOfActUnits",
    "agentIataCode",
    "recLocator",
    "amexNo",
    "amexTime",
    "recType",
    "recNumber",
    "region",
    "market",
    "amexDate",
    "cabinClass",
    "umaLastName",
    "umaFirstName",
    "flownClass",
    "umaTitle",
    "mktCarrier",
    "mktFltNo",
    "oprCarrier",
    "oprFltNo",
    "agentCity",
    "upldStatus",
    "hbackStatus",
    "cancelled",
    "pfpFlownPaxId",
    "umaNameSuffix",
    "pnrName",
    "ticketNumber",
    "rejectedFromAubId",
    "revCode",
    "profileMiddleName"
})
public class ActivityUploadException
    extends CRISBusinessBaseOfActivityUploadException
{

    @XmlElement(name = "Id")
    protected int id;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "AubId")
    protected int aubId;
    @XmlElement(name = "AubDesc")
    protected String aubDesc;
    @XmlElement(name = "MemUid", required = true)
    protected BigDecimal memUid;
    @XmlElement(name = "PersonId", required = true)
    protected BigDecimal personId;
    @XmlElement(name = "ProfileLastName")
    protected String profileLastName;
    @XmlElement(name = "ProfileFirstName")
    protected String profileFirstName;
    @XmlElement(name = "ProfileTitle")
    protected String profileTitle;
    @XmlElement(name = "ProfileNameSuffix")
    protected String profileNameSuffix;
    @XmlElement(name = "MbtCode")
    protected String mbtCode;
    @XmlElement(name = "CardNo")
    protected String cardNo;
    @XmlElement(name = "PtnrCode")
    protected String ptnrCode;
    @XmlElement(name = "ActTypeCode")
    protected String actTypeCode;
    @XmlElement(name = "ActNo")
    protected String actNo;
    @XmlElement(name = "ActDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actDate;
    @XmlElement(name = "ClassOfService")
    protected String classOfService;
    @XmlElement(name = "SgmtCityPair")
    protected String sgmtCityPair;
    @XmlElement(name = "FareBasis")
    protected String fareBasis;
    @XmlElement(name = "Diff")
    protected String diff;
    @XmlElement(name = "NumOfActUnits")
    protected int numOfActUnits;
    @XmlElement(name = "AgentIataCode")
    protected String agentIataCode;
    @XmlElement(name = "RecLocator")
    protected String recLocator;
    @XmlElement(name = "AmexNo")
    protected int amexNo;
    @XmlElement(name = "AmexTime")
    protected String amexTime;
    @XmlElement(name = "RecType")
    protected String recType;
    @XmlElement(name = "RecNumber")
    protected int recNumber;
    @XmlElement(name = "Region")
    protected String region;
    @XmlElement(name = "Market")
    protected String market;
    @XmlElement(name = "AmexDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar amexDate;
    @XmlElement(name = "CabinClass")
    protected String cabinClass;
    @XmlElement(name = "UmaLastName")
    protected String umaLastName;
    @XmlElement(name = "UmaFirstName")
    protected String umaFirstName;
    @XmlElement(name = "FlownClass")
    protected String flownClass;
    @XmlElement(name = "UmaTitle")
    protected String umaTitle;
    @XmlElement(name = "MktCarrier")
    protected String mktCarrier;
    @XmlElement(name = "MktFltNo")
    protected String mktFltNo;
    @XmlElement(name = "OprCarrier")
    protected String oprCarrier;
    @XmlElement(name = "OprFltNo")
    protected String oprFltNo;
    @XmlElement(name = "AgentCity")
    protected String agentCity;
    @XmlElement(name = "UpldStatus")
    protected String upldStatus;
    @XmlElement(name = "HbackStatus")
    protected String hbackStatus;
    @XmlElement(name = "Cancelled")
    protected String cancelled;
    @XmlElement(name = "PfpFlownPaxId")
    protected long pfpFlownPaxId;
    @XmlElement(name = "UmaNameSuffix")
    protected String umaNameSuffix;
    @XmlElement(name = "PnrName")
    protected String pnrName;
    @XmlElement(name = "TicketNumber")
    protected String ticketNumber;
    @XmlElement(name = "RejectedFromAubId")
    protected int rejectedFromAubId;
    @XmlElement(name = "RevCode")
    protected String revCode;
    @XmlElement(name = "ProfileMiddleName")
    protected String profileMiddleName;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the aubId property.
     * 
     */
    public int getAubId() {
        return aubId;
    }

    /**
     * Sets the value of the aubId property.
     * 
     */
    public void setAubId(int value) {
        this.aubId = value;
    }

    /**
     * Gets the value of the aubDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAubDesc() {
        return aubDesc;
    }

    /**
     * Sets the value of the aubDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAubDesc(String value) {
        this.aubDesc = value;
    }

    /**
     * Gets the value of the memUid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMemUid() {
        return memUid;
    }

    /**
     * Sets the value of the memUid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMemUid(BigDecimal value) {
        this.memUid = value;
    }

    /**
     * Gets the value of the personId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPersonId() {
        return personId;
    }

    /**
     * Sets the value of the personId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPersonId(BigDecimal value) {
        this.personId = value;
    }

    /**
     * Gets the value of the profileLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileLastName() {
        return profileLastName;
    }

    /**
     * Sets the value of the profileLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileLastName(String value) {
        this.profileLastName = value;
    }

    /**
     * Gets the value of the profileFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileFirstName() {
        return profileFirstName;
    }

    /**
     * Sets the value of the profileFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileFirstName(String value) {
        this.profileFirstName = value;
    }

    /**
     * Gets the value of the profileTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileTitle() {
        return profileTitle;
    }

    /**
     * Sets the value of the profileTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileTitle(String value) {
        this.profileTitle = value;
    }

    /**
     * Gets the value of the profileNameSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileNameSuffix() {
        return profileNameSuffix;
    }

    /**
     * Sets the value of the profileNameSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileNameSuffix(String value) {
        this.profileNameSuffix = value;
    }

    /**
     * Gets the value of the mbtCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMbtCode() {
        return mbtCode;
    }

    /**
     * Sets the value of the mbtCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMbtCode(String value) {
        this.mbtCode = value;
    }

    /**
     * Gets the value of the cardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNo() {
        return cardNo;
    }

    /**
     * Sets the value of the cardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNo(String value) {
        this.cardNo = value;
    }

    /**
     * Gets the value of the ptnrCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPtnrCode() {
        return ptnrCode;
    }

    /**
     * Sets the value of the ptnrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPtnrCode(String value) {
        this.ptnrCode = value;
    }

    /**
     * Gets the value of the actTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActTypeCode() {
        return actTypeCode;
    }

    /**
     * Sets the value of the actTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActTypeCode(String value) {
        this.actTypeCode = value;
    }

    /**
     * Gets the value of the actNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActNo() {
        return actNo;
    }

    /**
     * Sets the value of the actNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActNo(String value) {
        this.actNo = value;
    }

    /**
     * Gets the value of the actDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActDate() {
        return actDate;
    }

    /**
     * Sets the value of the actDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActDate(XMLGregorianCalendar value) {
        this.actDate = value;
    }

    /**
     * Gets the value of the classOfService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassOfService() {
        return classOfService;
    }

    /**
     * Sets the value of the classOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassOfService(String value) {
        this.classOfService = value;
    }

    /**
     * Gets the value of the sgmtCityPair property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSgmtCityPair() {
        return sgmtCityPair;
    }

    /**
     * Sets the value of the sgmtCityPair property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSgmtCityPair(String value) {
        this.sgmtCityPair = value;
    }

    /**
     * Gets the value of the fareBasis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasis() {
        return fareBasis;
    }

    /**
     * Sets the value of the fareBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasis(String value) {
        this.fareBasis = value;
    }

    /**
     * Gets the value of the diff property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiff() {
        return diff;
    }

    /**
     * Sets the value of the diff property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiff(String value) {
        this.diff = value;
    }

    /**
     * Gets the value of the numOfActUnits property.
     * 
     */
    public int getNumOfActUnits() {
        return numOfActUnits;
    }

    /**
     * Sets the value of the numOfActUnits property.
     * 
     */
    public void setNumOfActUnits(int value) {
        this.numOfActUnits = value;
    }

    /**
     * Gets the value of the agentIataCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentIataCode() {
        return agentIataCode;
    }

    /**
     * Sets the value of the agentIataCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentIataCode(String value) {
        this.agentIataCode = value;
    }

    /**
     * Gets the value of the recLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecLocator() {
        return recLocator;
    }

    /**
     * Sets the value of the recLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecLocator(String value) {
        this.recLocator = value;
    }

    /**
     * Gets the value of the amexNo property.
     * 
     */
    public int getAmexNo() {
        return amexNo;
    }

    /**
     * Sets the value of the amexNo property.
     * 
     */
    public void setAmexNo(int value) {
        this.amexNo = value;
    }

    /**
     * Gets the value of the amexTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmexTime() {
        return amexTime;
    }

    /**
     * Sets the value of the amexTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmexTime(String value) {
        this.amexTime = value;
    }

    /**
     * Gets the value of the recType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecType() {
        return recType;
    }

    /**
     * Sets the value of the recType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecType(String value) {
        this.recType = value;
    }

    /**
     * Gets the value of the recNumber property.
     * 
     */
    public int getRecNumber() {
        return recNumber;
    }

    /**
     * Sets the value of the recNumber property.
     * 
     */
    public void setRecNumber(int value) {
        this.recNumber = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the market property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarket() {
        return market;
    }

    /**
     * Sets the value of the market property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarket(String value) {
        this.market = value;
    }

    /**
     * Gets the value of the amexDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAmexDate() {
        return amexDate;
    }

    /**
     * Sets the value of the amexDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAmexDate(XMLGregorianCalendar value) {
        this.amexDate = value;
    }

    /**
     * Gets the value of the cabinClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabinClass() {
        return cabinClass;
    }

    /**
     * Sets the value of the cabinClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabinClass(String value) {
        this.cabinClass = value;
    }

    /**
     * Gets the value of the umaLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUmaLastName() {
        return umaLastName;
    }

    /**
     * Sets the value of the umaLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUmaLastName(String value) {
        this.umaLastName = value;
    }

    /**
     * Gets the value of the umaFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUmaFirstName() {
        return umaFirstName;
    }

    /**
     * Sets the value of the umaFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUmaFirstName(String value) {
        this.umaFirstName = value;
    }

    /**
     * Gets the value of the flownClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlownClass() {
        return flownClass;
    }

    /**
     * Sets the value of the flownClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlownClass(String value) {
        this.flownClass = value;
    }

    /**
     * Gets the value of the umaTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUmaTitle() {
        return umaTitle;
    }

    /**
     * Sets the value of the umaTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUmaTitle(String value) {
        this.umaTitle = value;
    }

    /**
     * Gets the value of the mktCarrier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMktCarrier() {
        return mktCarrier;
    }

    /**
     * Sets the value of the mktCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMktCarrier(String value) {
        this.mktCarrier = value;
    }

    /**
     * Gets the value of the mktFltNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMktFltNo() {
        return mktFltNo;
    }

    /**
     * Sets the value of the mktFltNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMktFltNo(String value) {
        this.mktFltNo = value;
    }

    /**
     * Gets the value of the oprCarrier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOprCarrier() {
        return oprCarrier;
    }

    /**
     * Sets the value of the oprCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOprCarrier(String value) {
        this.oprCarrier = value;
    }

    /**
     * Gets the value of the oprFltNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOprFltNo() {
        return oprFltNo;
    }

    /**
     * Sets the value of the oprFltNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOprFltNo(String value) {
        this.oprFltNo = value;
    }

    /**
     * Gets the value of the agentCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentCity() {
        return agentCity;
    }

    /**
     * Sets the value of the agentCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentCity(String value) {
        this.agentCity = value;
    }

    /**
     * Gets the value of the upldStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpldStatus() {
        return upldStatus;
    }

    /**
     * Sets the value of the upldStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpldStatus(String value) {
        this.upldStatus = value;
    }

    /**
     * Gets the value of the hbackStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHbackStatus() {
        return hbackStatus;
    }

    /**
     * Sets the value of the hbackStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHbackStatus(String value) {
        this.hbackStatus = value;
    }

    /**
     * Gets the value of the cancelled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelled() {
        return cancelled;
    }

    /**
     * Sets the value of the cancelled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelled(String value) {
        this.cancelled = value;
    }

    /**
     * Gets the value of the pfpFlownPaxId property.
     * 
     */
    public long getPfpFlownPaxId() {
        return pfpFlownPaxId;
    }

    /**
     * Sets the value of the pfpFlownPaxId property.
     * 
     */
    public void setPfpFlownPaxId(long value) {
        this.pfpFlownPaxId = value;
    }

    /**
     * Gets the value of the umaNameSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUmaNameSuffix() {
        return umaNameSuffix;
    }

    /**
     * Sets the value of the umaNameSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUmaNameSuffix(String value) {
        this.umaNameSuffix = value;
    }

    /**
     * Gets the value of the pnrName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnrName() {
        return pnrName;
    }

    /**
     * Sets the value of the pnrName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnrName(String value) {
        this.pnrName = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Sets the value of the ticketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Gets the value of the rejectedFromAubId property.
     * 
     */
    public int getRejectedFromAubId() {
        return rejectedFromAubId;
    }

    /**
     * Sets the value of the rejectedFromAubId property.
     * 
     */
    public void setRejectedFromAubId(int value) {
        this.rejectedFromAubId = value;
    }

    /**
     * Gets the value of the revCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevCode() {
        return revCode;
    }

    /**
     * Sets the value of the revCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevCode(String value) {
        this.revCode = value;
    }

    /**
     * Gets the value of the profileMiddleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileMiddleName() {
        return profileMiddleName;
    }

    /**
     * Sets the value of the profileMiddleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileMiddleName(String value) {
        this.profileMiddleName = value;
    }

}
