
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SkywardsTierStatusInputWE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SkywardsTierStatusInputWE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CardNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemUID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PersonId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="WebCall" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkywardsTierStatusInputWE", propOrder = {
    "cardNo",
    "memUID",
    "personId",
    "webCall"
})
public class SkywardsTierStatusInputWE {

    @XmlElement(name = "CardNo")
    protected String cardNo;
    @XmlElement(name = "MemUID")
    protected int memUID;
    @XmlElement(name = "PersonId")
    protected int personId;
    @XmlElement(name = "WebCall")
    protected String webCall;

    /**
     * Gets the value of the cardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNo() {
        return cardNo;
    }

    /**
     * Sets the value of the cardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNo(String value) {
        this.cardNo = value;
    }

    /**
     * Gets the value of the memUID property.
     * 
     */
    public int getMemUID() {
        return memUID;
    }

    /**
     * Sets the value of the memUID property.
     * 
     */
    public void setMemUID(int value) {
        this.memUID = value;
    }

    /**
     * Gets the value of the personId property.
     * 
     */
    public int getPersonId() {
        return personId;
    }

    /**
     * Sets the value of the personId property.
     * 
     */
    public void setPersonId(int value) {
        this.personId = value;
    }

    /**
     * Gets the value of the webCall property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebCall() {
        return webCall;
    }

    /**
     * Sets the value of the webCall property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebCall(String value) {
        this.webCall = value;
    }

}
