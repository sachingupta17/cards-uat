
package com.skywards.mercator_cris;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.skywards.mercator_cris package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AuthenticationHeader_QNAME = new QName("http://skywards.com/Mercator.CRIS.WS", "AuthenticationHeader");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.skywards.mercator_cris
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckInvalidEmailResponse }
     * 
     */
    public CheckInvalidEmailResponse createCheckInvalidEmailResponse() {
        return new CheckInvalidEmailResponse();
    }

    /**
     * Create an instance of {@link WSIResult }
     * 
     */
    public WSIResult createWSIResult() {
        return new WSIResult();
    }

    /**
     * Create an instance of {@link ManualOverrideSetResponse }
     * 
     */
    public ManualOverrideSetResponse createManualOverrideSetResponse() {
        return new ManualOverrideSetResponse();
    }

    /**
     * Create an instance of {@link MemberVerificationResponse }
     * 
     */
    public MemberVerificationResponse createMemberVerificationResponse() {
        return new MemberVerificationResponse();
    }

    /**
     * Create an instance of {@link GetProspectMilesResponse }
     * 
     */
    public GetProspectMilesResponse createGetProspectMilesResponse() {
        return new GetProspectMilesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfProspectMiles }
     * 
     */
    public ArrayOfProspectMiles createArrayOfProspectMiles() {
        return new ArrayOfProspectMiles();
    }

    /**
     * Create an instance of {@link ActivateAccountForWeb }
     * 
     */
    public ActivateAccountForWeb createActivateAccountForWeb() {
        return new ActivateAccountForWeb();
    }

    /**
     * Create an instance of {@link GetJetMileageCalculatorResponse }
     * 
     */
    public GetJetMileageCalculatorResponse createGetJetMileageCalculatorResponse() {
        return new GetJetMileageCalculatorResponse();
    }

    /**
     * Create an instance of {@link MileageCalculatorRequest }
     * 
     */
    public MileageCalculatorRequest createMileageCalculatorRequest() {
        return new MileageCalculatorRequest();
    }

    /**
     * Create an instance of {@link GetMember }
     * 
     */
    public GetMember createGetMember() {
        return new GetMember();
    }

    /**
     * Create an instance of {@link ProfileParameters }
     * 
     */
    public ProfileParameters createProfileParameters() {
        return new ProfileParameters();
    }

    /**
     * Create an instance of {@link ValidateMembershipNoResponse }
     * 
     */
    public ValidateMembershipNoResponse createValidateMembershipNoResponse() {
        return new ValidateMembershipNoResponse();
    }

    /**
     * Create an instance of {@link LinkFourthTierNomineeResponse }
     * 
     */
    public LinkFourthTierNomineeResponse createLinkFourthTierNomineeResponse() {
        return new LinkFourthTierNomineeResponse();
    }

    /**
     * Create an instance of {@link CheckTicketResponse }
     * 
     */
    public CheckTicketResponse createCheckTicketResponse() {
        return new CheckTicketResponse();
    }

    /**
     * Create an instance of {@link AddFamilyMember }
     * 
     */
    public AddFamilyMember createAddFamilyMember() {
        return new AddFamilyMember();
    }

    /**
     * Create an instance of {@link SkywardsProfile }
     * 
     */
    public SkywardsProfile createSkywardsProfile() {
        return new SkywardsProfile();
    }

    /**
     * Create an instance of {@link MemberLoginResponse }
     * 
     */
    public MemberLoginResponse createMemberLoginResponse() {
        return new MemberLoginResponse();
    }

    /**
     * Create an instance of {@link GetProspectMiles }
     * 
     */
    public GetProspectMiles createGetProspectMiles() {
        return new GetProspectMiles();
    }

    /**
     * Create an instance of {@link CreateRewardBookingResponse }
     * 
     */
    public CreateRewardBookingResponse createCreateRewardBookingResponse() {
        return new CreateRewardBookingResponse();
    }

    /**
     * Create an instance of {@link RewardBooking }
     * 
     */
    public RewardBooking createRewardBooking() {
        return new RewardBooking();
    }

    /**
     * Create an instance of {@link ArrayOfRewardItineraryDetail }
     * 
     */
    public ArrayOfRewardItineraryDetail createArrayOfRewardItineraryDetail() {
        return new ArrayOfRewardItineraryDetail();
    }

    /**
     * Create an instance of {@link ArrayOfInt }
     * 
     */
    public ArrayOfInt createArrayOfInt() {
        return new ArrayOfInt();
    }

    /**
     * Create an instance of {@link DeLinkFamilyMemberResponse }
     * 
     */
    public DeLinkFamilyMemberResponse createDeLinkFamilyMemberResponse() {
        return new DeLinkFamilyMemberResponse();
    }

    /**
     * Create an instance of {@link GetItineryHeaderCurrentDetails }
     * 
     */
    public GetItineryHeaderCurrentDetails createGetItineryHeaderCurrentDetails() {
        return new GetItineryHeaderCurrentDetails();
    }

    /**
     * Create an instance of {@link UpdateProfileResponse }
     * 
     */
    public UpdateProfileResponse createUpdateProfileResponse() {
        return new UpdateProfileResponse();
    }

    /**
     * Create an instance of {@link ConfirmRewardBookingResponse }
     * 
     */
    public ConfirmRewardBookingResponse createConfirmRewardBookingResponse() {
        return new ConfirmRewardBookingResponse();
    }

    /**
     * Create an instance of {@link DuplicateTicketFound }
     * 
     */
    public DuplicateTicketFound createDuplicateTicketFound() {
        return new DuplicateTicketFound();
    }

    /**
     * Create an instance of {@link GetItineryEmailAddress }
     * 
     */
    public GetItineryEmailAddress createGetItineryEmailAddress() {
        return new GetItineryEmailAddress();
    }

    /**
     * Create an instance of {@link EmailPassword2 }
     * 
     */
    public EmailPassword2 createEmailPassword2() {
        return new EmailPassword2();
    }

    /**
     * Create an instance of {@link GetItineryDetailsResponse }
     * 
     */
    public GetItineryDetailsResponse createGetItineryDetailsResponse() {
        return new GetItineryDetailsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfItineryDetails }
     * 
     */
    public ArrayOfItineryDetails createArrayOfItineryDetails() {
        return new ArrayOfItineryDetails();
    }

    /**
     * Create an instance of {@link RetrievePasswordHintResponse }
     * 
     */
    public RetrievePasswordHintResponse createRetrievePasswordHintResponse() {
        return new RetrievePasswordHintResponse();
    }

    /**
     * Create an instance of {@link MemberCampaignSet }
     * 
     */
    public MemberCampaignSet createMemberCampaignSet() {
        return new MemberCampaignSet();
    }

    /**
     * Create an instance of {@link UpdatePassports }
     * 
     */
    public UpdatePassports createUpdatePassports() {
        return new UpdatePassports();
    }

    /**
     * Create an instance of {@link ArrayOfPassport }
     * 
     */
    public ArrayOfPassport createArrayOfPassport() {
        return new ArrayOfPassport();
    }

    /**
     * Create an instance of {@link GetCorporateMemberCardsResponse }
     * 
     */
    public GetCorporateMemberCardsResponse createGetCorporateMemberCardsResponse() {
        return new GetCorporateMemberCardsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfStatusEnhancement }
     * 
     */
    public ArrayOfStatusEnhancement createArrayOfStatusEnhancement() {
        return new ArrayOfStatusEnhancement();
    }

    /**
     * Create an instance of {@link GetItineryHeaderPastDetails }
     * 
     */
    public GetItineryHeaderPastDetails createGetItineryHeaderPastDetails() {
        return new GetItineryHeaderPastDetails();
    }

    /**
     * Create an instance of {@link EmailMembershipNumberResponse }
     * 
     */
    public EmailMembershipNumberResponse createEmailMembershipNumberResponse() {
        return new EmailMembershipNumberResponse();
    }

    /**
     * Create an instance of {@link AssignCardsToCorporateMemberResponse }
     * 
     */
    public AssignCardsToCorporateMemberResponse createAssignCardsToCorporateMemberResponse() {
        return new AssignCardsToCorporateMemberResponse();
    }

    /**
     * Create an instance of {@link StatusEnhancement }
     * 
     */
    public StatusEnhancement createStatusEnhancement() {
        return new StatusEnhancement();
    }

    /**
     * Create an instance of {@link UpdateFamilyAccountStatus }
     * 
     */
    public UpdateFamilyAccountStatus createUpdateFamilyAccountStatus() {
        return new UpdateFamilyAccountStatus();
    }

    /**
     * Create an instance of {@link GetCorporateMemberCards }
     * 
     */
    public GetCorporateMemberCards createGetCorporateMemberCards() {
        return new GetCorporateMemberCards();
    }

    /**
     * Create an instance of {@link QueryableOfInt32 }
     * 
     */
    public QueryableOfInt32 createQueryableOfInt32() {
        return new QueryableOfInt32();
    }

    /**
     * Create an instance of {@link ConfirmTicketCheck }
     * 
     */
    public ConfirmTicketCheck createConfirmTicketCheck() {
        return new ConfirmTicketCheck();
    }

    /**
     * Create an instance of {@link RetroAccrualSummary }
     * 
     */
    public RetroAccrualSummary createRetroAccrualSummary() {
        return new RetroAccrualSummary();
    }

    /**
     * Create an instance of {@link GetItinerariesResponse }
     * 
     */
    public GetItinerariesResponse createGetItinerariesResponse() {
        return new GetItinerariesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfItinerary }
     * 
     */
    public ArrayOfItinerary createArrayOfItinerary() {
        return new ArrayOfItinerary();
    }

    /**
     * Create an instance of {@link CheckRedemptionResponse }
     * 
     */
    public CheckRedemptionResponse createCheckRedemptionResponse() {
        return new CheckRedemptionResponse();
    }

    /**
     * Create an instance of {@link GetMileageCalculator }
     * 
     */
    public GetMileageCalculator createGetMileageCalculator() {
        return new GetMileageCalculator();
    }

    /**
     * Create an instance of {@link MileageCalculatorWEFullList }
     * 
     */
    public MileageCalculatorWEFullList createMileageCalculatorWEFullList() {
        return new MileageCalculatorWEFullList();
    }

    /**
     * Create an instance of {@link UpdateFamilyMemberStatus }
     * 
     */
    public UpdateFamilyMemberStatus createUpdateFamilyMemberStatus() {
        return new UpdateFamilyMemberStatus();
    }

    /**
     * Create an instance of {@link RedeemMilesResponse }
     * 
     */
    public RedeemMilesResponse createRedeemMilesResponse() {
        return new RedeemMilesResponse();
    }

    /**
     * Create an instance of {@link RetrievePasswordHint }
     * 
     */
    public RetrievePasswordHint createRetrievePasswordHint() {
        return new RetrievePasswordHint();
    }

    /**
     * Create an instance of {@link TransferMiles }
     * 
     */
    public TransferMiles createTransferMiles() {
        return new TransferMiles();
    }

    /**
     * Create an instance of {@link GetVoucherDetails }
     * 
     */
    public GetVoucherDetails createGetVoucherDetails() {
        return new GetVoucherDetails();
    }

    /**
     * Create an instance of {@link AddFamilyMemberResponse }
     * 
     */
    public AddFamilyMemberResponse createAddFamilyMemberResponse() {
        return new AddFamilyMemberResponse();
    }

    /**
     * Create an instance of {@link CheckInvalidEmail }
     * 
     */
    public CheckInvalidEmail createCheckInvalidEmail() {
        return new CheckInvalidEmail();
    }

    /**
     * Create an instance of {@link GetItineryEmailAddressResponse }
     * 
     */
    public GetItineryEmailAddressResponse createGetItineryEmailAddressResponse() {
        return new GetItineryEmailAddressResponse();
    }

    /**
     * Create an instance of {@link ArrayOfItineryEmailAddress }
     * 
     */
    public ArrayOfItineryEmailAddress createArrayOfItineryEmailAddress() {
        return new ArrayOfItineryEmailAddress();
    }

    /**
     * Create an instance of {@link AuthenticateMember }
     * 
     */
    public AuthenticateMember createAuthenticateMember() {
        return new AuthenticateMember();
    }

    /**
     * Create an instance of {@link CreateRewardBooking }
     * 
     */
    public CreateRewardBooking createCreateRewardBooking() {
        return new CreateRewardBooking();
    }

    /**
     * Create an instance of {@link LinkFamilyMember }
     * 
     */
    public LinkFamilyMember createLinkFamilyMember() {
        return new LinkFamilyMember();
    }

    /**
     * Create an instance of {@link UpdateProfile }
     * 
     */
    public UpdateProfile createUpdateProfile() {
        return new UpdateProfile();
    }

    /**
     * Create an instance of {@link GetItineryChaufferDetailsResponse }
     * 
     */
    public GetItineryChaufferDetailsResponse createGetItineryChaufferDetailsResponse() {
        return new GetItineryChaufferDetailsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfItineryChaufferDetail }
     * 
     */
    public ArrayOfItineryChaufferDetail createArrayOfItineryChaufferDetail() {
        return new ArrayOfItineryChaufferDetail();
    }

    /**
     * Create an instance of {@link GetMinorsResponse }
     * 
     */
    public GetMinorsResponse createGetMinorsResponse() {
        return new GetMinorsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMinor }
     * 
     */
    public ArrayOfMinor createArrayOfMinor() {
        return new ArrayOfMinor();
    }

    /**
     * Create an instance of {@link GetAuthenticatedMember }
     * 
     */
    public GetAuthenticatedMember createGetAuthenticatedMember() {
        return new GetAuthenticatedMember();
    }

    /**
     * Create an instance of {@link ValidateRedemptionMiles }
     * 
     */
    public ValidateRedemptionMiles createValidateRedemptionMiles() {
        return new ValidateRedemptionMiles();
    }

    /**
     * Create an instance of {@link GetServiceActionEvents }
     * 
     */
    public GetServiceActionEvents createGetServiceActionEvents() {
        return new GetServiceActionEvents();
    }

    /**
     * Create an instance of {@link EmailPassword }
     * 
     */
    public EmailPassword createEmailPassword() {
        return new EmailPassword();
    }

    /**
     * Create an instance of {@link GetServiceActionEventsResponse }
     * 
     */
    public GetServiceActionEventsResponse createGetServiceActionEventsResponse() {
        return new GetServiceActionEventsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfServiceActionEvent }
     * 
     */
    public ArrayOfServiceActionEvent createArrayOfServiceActionEvent() {
        return new ArrayOfServiceActionEvent();
    }

    /**
     * Create an instance of {@link GetItineryHeaderCurrentDetailsResponse }
     * 
     */
    public GetItineryHeaderCurrentDetailsResponse createGetItineryHeaderCurrentDetailsResponse() {
        return new GetItineryHeaderCurrentDetailsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfItineryHeaderCurrent }
     * 
     */
    public ArrayOfItineryHeaderCurrent createArrayOfItineryHeaderCurrent() {
        return new ArrayOfItineryHeaderCurrent();
    }

    /**
     * Create an instance of {@link EnrollMember }
     * 
     */
    public EnrollMember createEnrollMember() {
        return new EnrollMember();
    }

    /**
     * Create an instance of {@link GetContactListing }
     * 
     */
    public GetContactListing createGetContactListing() {
        return new GetContactListing();
    }

    /**
     * Create an instance of {@link UpdatePreferencesResponse }
     * 
     */
    public UpdatePreferencesResponse createUpdatePreferencesResponse() {
        return new UpdatePreferencesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPreference }
     * 
     */
    public ArrayOfPreference createArrayOfPreference() {
        return new ArrayOfPreference();
    }

    /**
     * Create an instance of {@link UpdateFamilyMemberStatusResponse }
     * 
     */
    public UpdateFamilyMemberStatusResponse createUpdateFamilyMemberStatusResponse() {
        return new UpdateFamilyMemberStatusResponse();
    }

    /**
     * Create an instance of {@link AssignCardsToCorporateMember }
     * 
     */
    public AssignCardsToCorporateMember createAssignCardsToCorporateMember() {
        return new AssignCardsToCorporateMember();
    }

    /**
     * Create an instance of {@link GetItineryPassengerDetailsResponse }
     * 
     */
    public GetItineryPassengerDetailsResponse createGetItineryPassengerDetailsResponse() {
        return new GetItineryPassengerDetailsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfItineryPassengerDetails }
     * 
     */
    public ArrayOfItineryPassengerDetails createArrayOfItineryPassengerDetails() {
        return new ArrayOfItineryPassengerDetails();
    }

    /**
     * Create an instance of {@link MemberLogin }
     * 
     */
    public MemberLogin createMemberLogin() {
        return new MemberLogin();
    }

    /**
     * Create an instance of {@link GetActivityUploadExceptions }
     * 
     */
    public GetActivityUploadExceptions createGetActivityUploadExceptions() {
        return new GetActivityUploadExceptions();
    }

    /**
     * Create an instance of {@link EmailPasswordResponse }
     * 
     */
    public EmailPasswordResponse createEmailPasswordResponse() {
        return new EmailPasswordResponse();
    }

    /**
     * Create an instance of {@link UpdateAliasCards }
     * 
     */
    public UpdateAliasCards createUpdateAliasCards() {
        return new UpdateAliasCards();
    }

    /**
     * Create an instance of {@link ArrayOfAliasCard }
     * 
     */
    public ArrayOfAliasCard createArrayOfAliasCard() {
        return new ArrayOfAliasCard();
    }

    /**
     * Create an instance of {@link UpdateFlags }
     * 
     */
    public UpdateFlags createUpdateFlags() {
        return new UpdateFlags();
    }

    /**
     * Create an instance of {@link UpdateCreditCardsResponse }
     * 
     */
    public UpdateCreditCardsResponse createUpdateCreditCardsResponse() {
        return new UpdateCreditCardsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCreditCard }
     * 
     */
    public ArrayOfCreditCard createArrayOfCreditCard() {
        return new ArrayOfCreditCard();
    }

    /**
     * Create an instance of {@link UpdateContactsResponse }
     * 
     */
    public UpdateContactsResponse createUpdateContactsResponse() {
        return new UpdateContactsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfContact }
     * 
     */
    public ArrayOfContact createArrayOfContact() {
        return new ArrayOfContact();
    }

    /**
     * Create an instance of {@link CheckTicket }
     * 
     */
    public CheckTicket createCheckTicket() {
        return new CheckTicket();
    }

    /**
     * Create an instance of {@link UpdateInflightNumber }
     * 
     */
    public UpdateInflightNumber createUpdateInflightNumber() {
        return new UpdateInflightNumber();
    }

    /**
     * Create an instance of {@link ValidateMembershipNo }
     * 
     */
    public ValidateMembershipNo createValidateMembershipNo() {
        return new ValidateMembershipNo();
    }

    /**
     * Create an instance of {@link GetWhatCanIDoResponse }
     * 
     */
    public GetWhatCanIDoResponse createGetWhatCanIDoResponse() {
        return new GetWhatCanIDoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfWhatCanIDoOutputWE }
     * 
     */
    public ArrayOfWhatCanIDoOutputWE createArrayOfWhatCanIDoOutputWE() {
        return new ArrayOfWhatCanIDoOutputWE();
    }

    /**
     * Create an instance of {@link ConfirmRewardBooking }
     * 
     */
    public ConfirmRewardBooking createConfirmRewardBooking() {
        return new ConfirmRewardBooking();
    }

    /**
     * Create an instance of {@link GetRewardMileageCalculator }
     * 
     */
    public GetRewardMileageCalculator createGetRewardMileageCalculator() {
        return new GetRewardMileageCalculator();
    }

    /**
     * Create an instance of {@link LinkFamilyMemberResponse }
     * 
     */
    public LinkFamilyMemberResponse createLinkFamilyMemberResponse() {
        return new LinkFamilyMemberResponse();
    }

    /**
     * Create an instance of {@link UpdateCardActivationFourthTierResponse }
     * 
     */
    public UpdateCardActivationFourthTierResponse createUpdateCardActivationFourthTierResponse() {
        return new UpdateCardActivationFourthTierResponse();
    }

    /**
     * Create an instance of {@link GetSkywardsTierStatusResponse }
     * 
     */
    public GetSkywardsTierStatusResponse createGetSkywardsTierStatusResponse() {
        return new GetSkywardsTierStatusResponse();
    }

    /**
     * Create an instance of {@link SkywardsTierStatusRequest }
     * 
     */
    public SkywardsTierStatusRequest createSkywardsTierStatusRequest() {
        return new SkywardsTierStatusRequest();
    }

    /**
     * Create an instance of {@link EmailMembershipNumber }
     * 
     */
    public EmailMembershipNumber createEmailMembershipNumber() {
        return new EmailMembershipNumber();
    }

    /**
     * Create an instance of {@link GetTierStatusResponse }
     * 
     */
    public GetTierStatusResponse createGetTierStatusResponse() {
        return new GetTierStatusResponse();
    }

    /**
     * Create an instance of {@link ArrayOfTierStatusOutputWE }
     * 
     */
    public ArrayOfTierStatusOutputWE createArrayOfTierStatusOutputWE() {
        return new ArrayOfTierStatusOutputWE();
    }

    /**
     * Create an instance of {@link UpdatePersonalDetails }
     * 
     */
    public UpdatePersonalDetails createUpdatePersonalDetails() {
        return new UpdatePersonalDetails();
    }

    /**
     * Create an instance of {@link Member }
     * 
     */
    public Member createMember() {
        return new Member();
    }

    /**
     * Create an instance of {@link GetMemberProfileResponse }
     * 
     */
    public GetMemberProfileResponse createGetMemberProfileResponse() {
        return new GetMemberProfileResponse();
    }

    /**
     * Create an instance of {@link MemberCampaignSetResponse }
     * 
     */
    public MemberCampaignSetResponse createMemberCampaignSetResponse() {
        return new MemberCampaignSetResponse();
    }

    /**
     * Create an instance of {@link CreateAccrualActivityResponse }
     * 
     */
    public CreateAccrualActivityResponse createCreateAccrualActivityResponse() {
        return new CreateAccrualActivityResponse();
    }

    /**
     * Create an instance of {@link ArrayOfProgrammeMemberActivity }
     * 
     */
    public ArrayOfProgrammeMemberActivity createArrayOfProgrammeMemberActivity() {
        return new ArrayOfProgrammeMemberActivity();
    }

    /**
     * Create an instance of {@link UpdateMemberGiftDetailsResponse }
     * 
     */
    public UpdateMemberGiftDetailsResponse createUpdateMemberGiftDetailsResponse() {
        return new UpdateMemberGiftDetailsResponse();
    }

    /**
     * Create an instance of {@link UpdateCardActivationFourthTier }
     * 
     */
    public UpdateCardActivationFourthTier createUpdateCardActivationFourthTier() {
        return new UpdateCardActivationFourthTier();
    }

    /**
     * Create an instance of {@link LinkFourthTierNominee }
     * 
     */
    public LinkFourthTierNominee createLinkFourthTierNominee() {
        return new LinkFourthTierNominee();
    }

    /**
     * Create an instance of {@link UpdateAlaisCardsResponse }
     * 
     */
    public UpdateAlaisCardsResponse createUpdateAlaisCardsResponse() {
        return new UpdateAlaisCardsResponse();
    }

    /**
     * Create an instance of {@link MemberEnrollmentResponse }
     * 
     */
    public MemberEnrollmentResponse createMemberEnrollmentResponse() {
        return new MemberEnrollmentResponse();
    }

    /**
     * Create an instance of {@link GetAccountSummary2Response }
     * 
     */
    public GetAccountSummary2Response createGetAccountSummary2Response() {
        return new GetAccountSummary2Response();
    }

    /**
     * Create an instance of {@link AccountSummary }
     * 
     */
    public AccountSummary createAccountSummary() {
        return new AccountSummary();
    }

    /**
     * Create an instance of {@link ContactCentralFeedbackResponse }
     * 
     */
    public ContactCentralFeedbackResponse createContactCentralFeedbackResponse() {
        return new ContactCentralFeedbackResponse();
    }

    /**
     * Create an instance of {@link GetAccountSummary2 }
     * 
     */
    public GetAccountSummary2 createGetAccountSummary2() {
        return new GetAccountSummary2();
    }

    /**
     * Create an instance of {@link GetTierStatus }
     * 
     */
    public GetTierStatus createGetTierStatus() {
        return new GetTierStatus();
    }

    /**
     * Create an instance of {@link ValidateMembershipName }
     * 
     */
    public ValidateMembershipName createValidateMembershipName() {
        return new ValidateMembershipName();
    }

    /**
     * Create an instance of {@link RedeemMiles }
     * 
     */
    public RedeemMiles createRedeemMiles() {
        return new RedeemMiles();
    }

    /**
     * Create an instance of {@link AuthenticationHeader }
     * 
     */
    public AuthenticationHeader createAuthenticationHeader() {
        return new AuthenticationHeader();
    }

    /**
     * Create an instance of {@link GetFuturePNRs }
     * 
     */
    public GetFuturePNRs createGetFuturePNRs() {
        return new GetFuturePNRs();
    }

    /**
     * Create an instance of {@link UpdateContacts }
     * 
     */
    public UpdateContacts createUpdateContacts() {
        return new UpdateContacts();
    }

    /**
     * Create an instance of {@link TransferMilesResponse }
     * 
     */
    public TransferMilesResponse createTransferMilesResponse() {
        return new TransferMilesResponse();
    }

    /**
     * Create an instance of {@link ConfirmTicketCheckResponse }
     * 
     */
    public ConfirmTicketCheckResponse createConfirmTicketCheckResponse() {
        return new ConfirmTicketCheckResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRetroAccrualSummary }
     * 
     */
    public ArrayOfRetroAccrualSummary createArrayOfRetroAccrualSummary() {
        return new ArrayOfRetroAccrualSummary();
    }

    /**
     * Create an instance of {@link OnlineRetroNameCheckResponse }
     * 
     */
    public OnlineRetroNameCheckResponse createOnlineRetroNameCheckResponse() {
        return new OnlineRetroNameCheckResponse();
    }

    /**
     * Create an instance of {@link EmailPassword2Response }
     * 
     */
    public EmailPassword2Response createEmailPassword2Response() {
        return new EmailPassword2Response();
    }

    /**
     * Create an instance of {@link EnrollLinkFourthTierNominee }
     * 
     */
    public EnrollLinkFourthTierNominee createEnrollLinkFourthTierNominee() {
        return new EnrollLinkFourthTierNominee();
    }

    /**
     * Create an instance of {@link ManualOverrideSet }
     * 
     */
    public ManualOverrideSet createManualOverrideSet() {
        return new ManualOverrideSet();
    }

    /**
     * Create an instance of {@link ValidateRedemptionMilesResponse }
     * 
     */
    public ValidateRedemptionMilesResponse createValidateRedemptionMilesResponse() {
        return new ValidateRedemptionMilesResponse();
    }

    /**
     * Create an instance of {@link ChangePassword }
     * 
     */
    public ChangePassword createChangePassword() {
        return new ChangePassword();
    }

    /**
     * Create an instance of {@link GetDTRdetailsResponse }
     * 
     */
    public GetDTRdetailsResponse createGetDTRdetailsResponse() {
        return new GetDTRdetailsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfDynamicTierReview }
     * 
     */
    public ArrayOfDynamicTierReview createArrayOfDynamicTierReview() {
        return new ArrayOfDynamicTierReview();
    }

    /**
     * Create an instance of {@link GetItineryPassengerDetails }
     * 
     */
    public GetItineryPassengerDetails createGetItineryPassengerDetails() {
        return new GetItineryPassengerDetails();
    }

    /**
     * Create an instance of {@link AuthenticateMemberResponse }
     * 
     */
    public AuthenticateMemberResponse createAuthenticateMemberResponse() {
        return new AuthenticateMemberResponse();
    }

    /**
     * Create an instance of {@link UpdateAddresses }
     * 
     */
    public UpdateAddresses createUpdateAddresses() {
        return new UpdateAddresses();
    }

    /**
     * Create an instance of {@link ArrayOfAddress }
     * 
     */
    public ArrayOfAddress createArrayOfAddress() {
        return new ArrayOfAddress();
    }

    /**
     * Create an instance of {@link GetActivitiesResponse }
     * 
     */
    public GetActivitiesResponse createGetActivitiesResponse() {
        return new GetActivitiesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfActivity }
     * 
     */
    public ArrayOfActivity createArrayOfActivity() {
        return new ArrayOfActivity();
    }

    /**
     * Create an instance of {@link MyStatement }
     * 
     */
    public MyStatement createMyStatement() {
        return new MyStatement();
    }

    /**
     * Create an instance of {@link MyStatementInputWE }
     * 
     */
    public MyStatementInputWE createMyStatementInputWE() {
        return new MyStatementInputWE();
    }

    /**
     * Create an instance of {@link UpdatePostalStatus }
     * 
     */
    public UpdatePostalStatus createUpdatePostalStatus() {
        return new UpdatePostalStatus();
    }

    /**
     * Create an instance of {@link GetDTRdetails }
     * 
     */
    public GetDTRdetails createGetDTRdetails() {
        return new GetDTRdetails();
    }

    /**
     * Create an instance of {@link GetMinors }
     * 
     */
    public GetMinors createGetMinors() {
        return new GetMinors();
    }

    /**
     * Create an instance of {@link GetWebTicketCouponDetailsResponse }
     * 
     */
    public GetWebTicketCouponDetailsResponse createGetWebTicketCouponDetailsResponse() {
        return new GetWebTicketCouponDetailsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfTicketCouponDetail }
     * 
     */
    public ArrayOfTicketCouponDetail createArrayOfTicketCouponDetail() {
        return new ArrayOfTicketCouponDetail();
    }

    /**
     * Create an instance of {@link GetMilesAccelerators }
     * 
     */
    public GetMilesAccelerators createGetMilesAccelerators() {
        return new GetMilesAccelerators();
    }

    /**
     * Create an instance of {@link ArrayOfMilesItineryDetailWE }
     * 
     */
    public ArrayOfMilesItineryDetailWE createArrayOfMilesItineryDetailWE() {
        return new ArrayOfMilesItineryDetailWE();
    }

    /**
     * Create an instance of {@link UpdateCreditCards }
     * 
     */
    public UpdateCreditCards createUpdateCreditCards() {
        return new UpdateCreditCards();
    }

    /**
     * Create an instance of {@link MyStatementResponse }
     * 
     */
    public MyStatementResponse createMyStatementResponse() {
        return new MyStatementResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMyStatement }
     * 
     */
    public ArrayOfMyStatement createArrayOfMyStatement() {
        return new ArrayOfMyStatement();
    }

    /**
     * Create an instance of {@link GetAuthenticatedMemberResponse }
     * 
     */
    public GetAuthenticatedMemberResponse createGetAuthenticatedMemberResponse() {
        return new GetAuthenticatedMemberResponse();
    }

    /**
     * Create an instance of {@link GetSkywardsProfile }
     * 
     */
    public GetSkywardsProfile createGetSkywardsProfile() {
        return new GetSkywardsProfile();
    }

    /**
     * Create an instance of {@link CheckUniqueMember }
     * 
     */
    public CheckUniqueMember createCheckUniqueMember() {
        return new CheckUniqueMember();
    }

    /**
     * Create an instance of {@link MemberVerification }
     * 
     */
    public MemberVerification createMemberVerification() {
        return new MemberVerification();
    }

    /**
     * Create an instance of {@link ValidateMembershipNameResponse }
     * 
     */
    public ValidateMembershipNameResponse createValidateMembershipNameResponse() {
        return new ValidateMembershipNameResponse();
    }

    /**
     * Create an instance of {@link GetItineraries }
     * 
     */
    public GetItineraries createGetItineraries() {
        return new GetItineraries();
    }

    /**
     * Create an instance of {@link EnrollLinkFourthTierNomineeResponse }
     * 
     */
    public EnrollLinkFourthTierNomineeResponse createEnrollLinkFourthTierNomineeResponse() {
        return new EnrollLinkFourthTierNomineeResponse();
    }

    /**
     * Create an instance of {@link GetMemberSalesOffDetails }
     * 
     */
    public GetMemberSalesOffDetails createGetMemberSalesOffDetails() {
        return new GetMemberSalesOffDetails();
    }

    /**
     * Create an instance of {@link GetContactListingResponse }
     * 
     */
    public GetContactListingResponse createGetContactListingResponse() {
        return new GetContactListingResponse();
    }

    /**
     * Create an instance of {@link ArrayOfContactListing }
     * 
     */
    public ArrayOfContactListing createArrayOfContactListing() {
        return new ArrayOfContactListing();
    }

    /**
     * Create an instance of {@link FindMember }
     * 
     */
    public FindMember createFindMember() {
        return new FindMember();
    }

    /**
     * Create an instance of {@link CreateAccrualActivity }
     * 
     */
    public CreateAccrualActivity createCreateAccrualActivity() {
        return new CreateAccrualActivity();
    }

    /**
     * Create an instance of {@link GetSkywardsTierStatus }
     * 
     */
    public GetSkywardsTierStatus createGetSkywardsTierStatus() {
        return new GetSkywardsTierStatus();
    }

    /**
     * Create an instance of {@link SkywardsTierStatusInputWE }
     * 
     */
    public SkywardsTierStatusInputWE createSkywardsTierStatusInputWE() {
        return new SkywardsTierStatusInputWE();
    }

    /**
     * Create an instance of {@link ContactCentralFeedback }
     * 
     */
    public ContactCentralFeedback createContactCentralFeedback() {
        return new ContactCentralFeedback();
    }

    /**
     * Create an instance of {@link ContactCentralFeedback2 }
     * 
     */
    public ContactCentralFeedback2 createContactCentralFeedback2() {
        return new ContactCentralFeedback2();
    }

    /**
     * Create an instance of {@link DeLinkFamilyMember }
     * 
     */
    public DeLinkFamilyMember createDeLinkFamilyMember() {
        return new DeLinkFamilyMember();
    }

    /**
     * Create an instance of {@link GetUpgradeKISMilesResponse }
     * 
     */
    public GetUpgradeKISMilesResponse createGetUpgradeKISMilesResponse() {
        return new GetUpgradeKISMilesResponse();
    }

    /**
     * Create an instance of {@link GetItineryChaufferDetails }
     * 
     */
    public GetItineryChaufferDetails createGetItineryChaufferDetails() {
        return new GetItineryChaufferDetails();
    }

    /**
     * Create an instance of {@link GetMilesAcceleratorsResponse }
     * 
     */
    public GetMilesAcceleratorsResponse createGetMilesAcceleratorsResponse() {
        return new GetMilesAcceleratorsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMilesAcceleratorWE }
     * 
     */
    public ArrayOfMilesAcceleratorWE createArrayOfMilesAcceleratorWE() {
        return new ArrayOfMilesAcceleratorWE();
    }

    /**
     * Create an instance of {@link GetItineryHeaderPastDetailsResponse }
     * 
     */
    public GetItineryHeaderPastDetailsResponse createGetItineryHeaderPastDetailsResponse() {
        return new GetItineryHeaderPastDetailsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfItineryHeaderPast }
     * 
     */
    public ArrayOfItineryHeaderPast createArrayOfItineryHeaderPast() {
        return new ArrayOfItineryHeaderPast();
    }

    /**
     * Create an instance of {@link UpdatePersonalDetailsResponse }
     * 
     */
    public UpdatePersonalDetailsResponse createUpdatePersonalDetailsResponse() {
        return new UpdatePersonalDetailsResponse();
    }

    /**
     * Create an instance of {@link UpdateEmailAddress }
     * 
     */
    public UpdateEmailAddress createUpdateEmailAddress() {
        return new UpdateEmailAddress();
    }

    /**
     * Create an instance of {@link GetVoucherDetailsResponse }
     * 
     */
    public GetVoucherDetailsResponse createGetVoucherDetailsResponse() {
        return new GetVoucherDetailsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCustomerVoucher }
     * 
     */
    public ArrayOfCustomerVoucher createArrayOfCustomerVoucher() {
        return new ArrayOfCustomerVoucher();
    }

    /**
     * Create an instance of {@link GetMemberProfile }
     * 
     */
    public GetMemberProfile createGetMemberProfile() {
        return new GetMemberProfile();
    }

    /**
     * Create an instance of {@link DuplicateTicketFoundResponse }
     * 
     */
    public DuplicateTicketFoundResponse createDuplicateTicketFoundResponse() {
        return new DuplicateTicketFoundResponse();
    }

    /**
     * Create an instance of {@link OnlineRetroNameCheck }
     * 
     */
    public OnlineRetroNameCheck createOnlineRetroNameCheck() {
        return new OnlineRetroNameCheck();
    }

    /**
     * Create an instance of {@link ActivateAccountForWebResponse }
     * 
     */
    public ActivateAccountForWebResponse createActivateAccountForWebResponse() {
        return new ActivateAccountForWebResponse();
    }

    /**
     * Create an instance of {@link GetFamilyMembersProfilesResponse }
     * 
     */
    public GetFamilyMembersProfilesResponse createGetFamilyMembersProfilesResponse() {
        return new GetFamilyMembersProfilesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSkywardsProfile }
     * 
     */
    public ArrayOfSkywardsProfile createArrayOfSkywardsProfile() {
        return new ArrayOfSkywardsProfile();
    }

    /**
     * Create an instance of {@link UpdateAddressesResponse }
     * 
     */
    public UpdateAddressesResponse createUpdateAddressesResponse() {
        return new UpdateAddressesResponse();
    }

    /**
     * Create an instance of {@link UpdateAlaisCards }
     * 
     */
    public UpdateAlaisCards createUpdateAlaisCards() {
        return new UpdateAlaisCards();
    }

    /**
     * Create an instance of {@link UpdateEmailAddressResponse }
     * 
     */
    public UpdateEmailAddressResponse createUpdateEmailAddressResponse() {
        return new UpdateEmailAddressResponse();
    }

    /**
     * Create an instance of {@link UpdatePassportsResponse }
     * 
     */
    public UpdatePassportsResponse createUpdatePassportsResponse() {
        return new UpdatePassportsResponse();
    }

    /**
     * Create an instance of {@link ReplaceCardResponse }
     * 
     */
    public ReplaceCardResponse createReplaceCardResponse() {
        return new ReplaceCardResponse();
    }

    /**
     * Create an instance of {@link CheckUniqueMemberResponse }
     * 
     */
    public CheckUniqueMemberResponse createCheckUniqueMemberResponse() {
        return new CheckUniqueMemberResponse();
    }

    /**
     * Create an instance of {@link MemberEnrollment }
     * 
     */
    public MemberEnrollment createMemberEnrollment() {
        return new MemberEnrollment();
    }

    /**
     * Create an instance of {@link FindMemberResponse }
     * 
     */
    public FindMemberResponse createFindMemberResponse() {
        return new FindMemberResponse();
    }

    /**
     * Create an instance of {@link CheckRedemption }
     * 
     */
    public CheckRedemption createCheckRedemption() {
        return new CheckRedemption();
    }

    /**
     * Create an instance of {@link GetAccountSummary }
     * 
     */
    public GetAccountSummary createGetAccountSummary() {
        return new GetAccountSummary();
    }

    /**
     * Create an instance of {@link GetActivityUploadExceptionsResponse }
     * 
     */
    public GetActivityUploadExceptionsResponse createGetActivityUploadExceptionsResponse() {
        return new GetActivityUploadExceptionsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfActivityUploadException }
     * 
     */
    public ArrayOfActivityUploadException createArrayOfActivityUploadException() {
        return new ArrayOfActivityUploadException();
    }

    /**
     * Create an instance of {@link GetMemberSalesOffDetailsResponse }
     * 
     */
    public GetMemberSalesOffDetailsResponse createGetMemberSalesOffDetailsResponse() {
        return new GetMemberSalesOffDetailsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMemberSalesOfficeDtl }
     * 
     */
    public ArrayOfMemberSalesOfficeDtl createArrayOfMemberSalesOfficeDtl() {
        return new ArrayOfMemberSalesOfficeDtl();
    }

    /**
     * Create an instance of {@link GetProspectsProfileResponse }
     * 
     */
    public GetProspectsProfileResponse createGetProspectsProfileResponse() {
        return new GetProspectsProfileResponse();
    }

    /**
     * Create an instance of {@link UpdatePostalStatusResponse }
     * 
     */
    public UpdatePostalStatusResponse createUpdatePostalStatusResponse() {
        return new UpdatePostalStatusResponse();
    }

    /**
     * Create an instance of {@link GetProspectsProfile }
     * 
     */
    public GetProspectsProfile createGetProspectsProfile() {
        return new GetProspectsProfile();
    }

    /**
     * Create an instance of {@link GetWhatCanIDo }
     * 
     */
    public GetWhatCanIDo createGetWhatCanIDo() {
        return new GetWhatCanIDo();
    }

    /**
     * Create an instance of {@link WhatCanIDoInputWE }
     * 
     */
    public WhatCanIDoInputWE createWhatCanIDoInputWE() {
        return new WhatCanIDoInputWE();
    }

    /**
     * Create an instance of {@link CancelReward }
     * 
     */
    public CancelReward createCancelReward() {
        return new CancelReward();
    }

    /**
     * Create an instance of {@link UpdateAliasCardsResponse }
     * 
     */
    public UpdateAliasCardsResponse createUpdateAliasCardsResponse() {
        return new UpdateAliasCardsResponse();
    }

    /**
     * Create an instance of {@link UpdateMemberGiftDetails }
     * 
     */
    public UpdateMemberGiftDetails createUpdateMemberGiftDetails() {
        return new UpdateMemberGiftDetails();
    }

    /**
     * Create an instance of {@link GetJetMileageCalculator }
     * 
     */
    public GetJetMileageCalculator createGetJetMileageCalculator() {
        return new GetJetMileageCalculator();
    }

    /**
     * Create an instance of {@link GetSkywardsProfileResponse }
     * 
     */
    public GetSkywardsProfileResponse createGetSkywardsProfileResponse() {
        return new GetSkywardsProfileResponse();
    }

    /**
     * Create an instance of {@link GetFuturePNRsResponse }
     * 
     */
    public GetFuturePNRsResponse createGetFuturePNRsResponse() {
        return new GetFuturePNRsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfFuturePNR }
     * 
     */
    public ArrayOfFuturePNR createArrayOfFuturePNR() {
        return new ArrayOfFuturePNR();
    }

    /**
     * Create an instance of {@link GetUpgradeKISMiles }
     * 
     */
    public GetUpgradeKISMiles createGetUpgradeKISMiles() {
        return new GetUpgradeKISMiles();
    }

    /**
     * Create an instance of {@link GetActivities }
     * 
     */
    public GetActivities createGetActivities() {
        return new GetActivities();
    }

    /**
     * Create an instance of {@link GetRewardMileageCalculatorResponse }
     * 
     */
    public GetRewardMileageCalculatorResponse createGetRewardMileageCalculatorResponse() {
        return new GetRewardMileageCalculatorResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMilesCalculatorWE }
     * 
     */
    public ArrayOfMilesCalculatorWE createArrayOfMilesCalculatorWE() {
        return new ArrayOfMilesCalculatorWE();
    }

    /**
     * Create an instance of {@link EnrollMemberResponse }
     * 
     */
    public EnrollMemberResponse createEnrollMemberResponse() {
        return new EnrollMemberResponse();
    }

    /**
     * Create an instance of {@link GetAccountSummaryResponse }
     * 
     */
    public GetAccountSummaryResponse createGetAccountSummaryResponse() {
        return new GetAccountSummaryResponse();
    }

    /**
     * Create an instance of {@link UpdatePreferences }
     * 
     */
    public UpdatePreferences createUpdatePreferences() {
        return new UpdatePreferences();
    }

    /**
     * Create an instance of {@link UpdateInflightNumberResponse }
     * 
     */
    public UpdateInflightNumberResponse createUpdateInflightNumberResponse() {
        return new UpdateInflightNumberResponse();
    }

    /**
     * Create an instance of {@link GetWebTicketCouponDetails }
     * 
     */
    public GetWebTicketCouponDetails createGetWebTicketCouponDetails() {
        return new GetWebTicketCouponDetails();
    }

    /**
     * Create an instance of {@link GetMileageCalculatorResponse }
     * 
     */
    public GetMileageCalculatorResponse createGetMileageCalculatorResponse() {
        return new GetMileageCalculatorResponse();
    }

    /**
     * Create an instance of {@link UpdateFamilyAccountStatusResponse }
     * 
     */
    public UpdateFamilyAccountStatusResponse createUpdateFamilyAccountStatusResponse() {
        return new UpdateFamilyAccountStatusResponse();
    }

    /**
     * Create an instance of {@link GetMemberResponse }
     * 
     */
    public GetMemberResponse createGetMemberResponse() {
        return new GetMemberResponse();
    }

    /**
     * Create an instance of {@link ReplaceCard }
     * 
     */
    public ReplaceCard createReplaceCard() {
        return new ReplaceCard();
    }

    /**
     * Create an instance of {@link GetFamilyMembersProfiles }
     * 
     */
    public GetFamilyMembersProfiles createGetFamilyMembersProfiles() {
        return new GetFamilyMembersProfiles();
    }

    /**
     * Create an instance of {@link GetItineryDetails }
     * 
     */
    public GetItineryDetails createGetItineryDetails() {
        return new GetItineryDetails();
    }

    /**
     * Create an instance of {@link CancelRewardResponse }
     * 
     */
    public CancelRewardResponse createCancelRewardResponse() {
        return new CancelRewardResponse();
    }

    /**
     * Create an instance of {@link ChangePasswordResponse }
     * 
     */
    public ChangePasswordResponse createChangePasswordResponse() {
        return new ChangePasswordResponse();
    }

    /**
     * Create an instance of {@link WebTransaction }
     * 
     */
    public WebTransaction createWebTransaction() {
        return new WebTransaction();
    }

    /**
     * Create an instance of {@link ActivityUploadException }
     * 
     */
    public ActivityUploadException createActivityUploadException() {
        return new ActivityUploadException();
    }

    /**
     * Create an instance of {@link MilesItineryDetailWE }
     * 
     */
    public MilesItineryDetailWE createMilesItineryDetailWE() {
        return new MilesItineryDetailWE();
    }

    /**
     * Create an instance of {@link MilesAcceleratorWE }
     * 
     */
    public MilesAcceleratorWE createMilesAcceleratorWE() {
        return new MilesAcceleratorWE();
    }

    /**
     * Create an instance of {@link ContactListing }
     * 
     */
    public ContactListing createContactListing() {
        return new ContactListing();
    }

    /**
     * Create an instance of {@link WhatCanIDoOutputWE }
     * 
     */
    public WhatCanIDoOutputWE createWhatCanIDoOutputWE() {
        return new WhatCanIDoOutputWE();
    }

    /**
     * Create an instance of {@link CRISReadOnlyBase }
     * 
     */
    public CRISReadOnlyBase createCRISReadOnlyBase() {
        return new CRISReadOnlyBase();
    }

    /**
     * Create an instance of {@link CRISBusinessBaseOfActivityUploadException }
     * 
     */
    public CRISBusinessBaseOfActivityUploadException createCRISBusinessBaseOfActivityUploadException() {
        return new CRISBusinessBaseOfActivityUploadException();
    }

    /**
     * Create an instance of {@link ProgrammeMemberActivity }
     * 
     */
    public ProgrammeMemberActivity createProgrammeMemberActivity() {
        return new ProgrammeMemberActivity();
    }

    /**
     * Create an instance of {@link ArrayOfMileageCalculatorWE }
     * 
     */
    public ArrayOfMileageCalculatorWE createArrayOfMileageCalculatorWE() {
        return new ArrayOfMileageCalculatorWE();
    }

    /**
     * Create an instance of {@link Activity }
     * 
     */
    public Activity createActivity() {
        return new Activity();
    }

    /**
     * Create an instance of {@link ItineryEmailAddress }
     * 
     */
    public ItineryEmailAddress createItineryEmailAddress() {
        return new ItineryEmailAddress();
    }

    /**
     * Create an instance of {@link QueryableInt64 }
     * 
     */
    public QueryableInt64 createQueryableInt64() {
        return new QueryableInt64();
    }

    /**
     * Create an instance of {@link CRISBusinessBaseOfItineryEmailAddress }
     * 
     */
    public CRISBusinessBaseOfItineryEmailAddress createCRISBusinessBaseOfItineryEmailAddress() {
        return new CRISBusinessBaseOfItineryEmailAddress();
    }

    /**
     * Create an instance of {@link ArrayOfRenew }
     * 
     */
    public ArrayOfRenew createArrayOfRenew() {
        return new ArrayOfRenew();
    }

    /**
     * Create an instance of {@link CRISBusinessBaseOfItineryChaufferDetail }
     * 
     */
    public CRISBusinessBaseOfItineryChaufferDetail createCRISBusinessBaseOfItineryChaufferDetail() {
        return new CRISBusinessBaseOfItineryChaufferDetail();
    }

    /**
     * Create an instance of {@link Minor }
     * 
     */
    public Minor createMinor() {
        return new Minor();
    }

    /**
     * Create an instance of {@link ArrayOfTierDetails }
     * 
     */
    public ArrayOfTierDetails createArrayOfTierDetails() {
        return new ArrayOfTierDetails();
    }

    /**
     * Create an instance of {@link QueryableDateTime }
     * 
     */
    public QueryableDateTime createQueryableDateTime() {
        return new QueryableDateTime();
    }

    /**
     * Create an instance of {@link ArrayOfGuardian }
     * 
     */
    public ArrayOfGuardian createArrayOfGuardian() {
        return new ArrayOfGuardian();
    }

    /**
     * Create an instance of {@link MilesCalculatorWE }
     * 
     */
    public MilesCalculatorWE createMilesCalculatorWE() {
        return new MilesCalculatorWE();
    }

    /**
     * Create an instance of {@link Guardian }
     * 
     */
    public Guardian createGuardian() {
        return new Guardian();
    }

    /**
     * Create an instance of {@link ItineryDetails }
     * 
     */
    public ItineryDetails createItineryDetails() {
        return new ItineryDetails();
    }

    /**
     * Create an instance of {@link CustomerVoucher }
     * 
     */
    public CustomerVoucher createCustomerVoucher() {
        return new CustomerVoucher();
    }

    /**
     * Create an instance of {@link ItineryChaufferDetail }
     * 
     */
    public ItineryChaufferDetail createItineryChaufferDetail() {
        return new ItineryChaufferDetail();
    }

    /**
     * Create an instance of {@link MileageCalculatorWE }
     * 
     */
    public MileageCalculatorWE createMileageCalculatorWE() {
        return new MileageCalculatorWE();
    }

    /**
     * Create an instance of {@link RewardItineraryDetail }
     * 
     */
    public RewardItineraryDetail createRewardItineraryDetail() {
        return new RewardItineraryDetail();
    }

    /**
     * Create an instance of {@link DynamicTierReview }
     * 
     */
    public DynamicTierReview createDynamicTierReview() {
        return new DynamicTierReview();
    }

    /**
     * Create an instance of {@link TicketCouponDetail }
     * 
     */
    public TicketCouponDetail createTicketCouponDetail() {
        return new TicketCouponDetail();
    }

    /**
     * Create an instance of {@link ItineryHeaderPast }
     * 
     */
    public ItineryHeaderPast createItineryHeaderPast() {
        return new ItineryHeaderPast();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link ArrayOfMileageCalculator }
     * 
     */
    public ArrayOfMileageCalculator createArrayOfMileageCalculator() {
        return new ArrayOfMileageCalculator();
    }

    /**
     * Create an instance of {@link Preference }
     * 
     */
    public Preference createPreference() {
        return new Preference();
    }

    /**
     * Create an instance of {@link Achieve }
     * 
     */
    public Achieve createAchieve() {
        return new Achieve();
    }

    /**
     * Create an instance of {@link Sector }
     * 
     */
    public Sector createSector() {
        return new Sector();
    }

    /**
     * Create an instance of {@link QueryableDecimal }
     * 
     */
    public QueryableDecimal createQueryableDecimal() {
        return new QueryableDecimal();
    }

    /**
     * Create an instance of {@link AliasCard }
     * 
     */
    public AliasCard createAliasCard() {
        return new AliasCard();
    }

    /**
     * Create an instance of {@link CreditCard }
     * 
     */
    public CreditCard createCreditCard() {
        return new CreditCard();
    }

    /**
     * Create an instance of {@link Renew }
     * 
     */
    public Renew createRenew() {
        return new Renew();
    }

    /**
     * Create an instance of {@link MyStatement2 }
     * 
     */
    public MyStatement2 createMyStatement2() {
        return new MyStatement2();
    }

    /**
     * Create an instance of {@link QueryableInt32 }
     * 
     */
    public QueryableInt32 createQueryableInt32() {
        return new QueryableInt32();
    }

    /**
     * Create an instance of {@link Passport }
     * 
     */
    public Passport createPassport() {
        return new Passport();
    }

    /**
     * Create an instance of {@link TierDetails }
     * 
     */
    public TierDetails createTierDetails() {
        return new TierDetails();
    }

    /**
     * Create an instance of {@link ServiceActionEvent }
     * 
     */
    public ServiceActionEvent createServiceActionEvent() {
        return new ServiceActionEvent();
    }

    /**
     * Create an instance of {@link ProspectMiles }
     * 
     */
    public ProspectMiles createProspectMiles() {
        return new ProspectMiles();
    }

    /**
     * Create an instance of {@link TravelCoordinator }
     * 
     */
    public TravelCoordinator createTravelCoordinator() {
        return new TravelCoordinator();
    }

    /**
     * Create an instance of {@link TierStatusOutputWE }
     * 
     */
    public TierStatusOutputWE createTierStatusOutputWE() {
        return new TierStatusOutputWE();
    }

    /**
     * Create an instance of {@link ItineryPassengerDetails }
     * 
     */
    public ItineryPassengerDetails createItineryPassengerDetails() {
        return new ItineryPassengerDetails();
    }

    /**
     * Create an instance of {@link QueryableOfDateTime }
     * 
     */
    public QueryableOfDateTime createQueryableOfDateTime() {
        return new QueryableOfDateTime();
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link ItineryHeaderCurrent }
     * 
     */
    public ItineryHeaderCurrent createItineryHeaderCurrent() {
        return new ItineryHeaderCurrent();
    }

    /**
     * Create an instance of {@link Itinerary }
     * 
     */
    public Itinerary createItinerary() {
        return new Itinerary();
    }

    /**
     * Create an instance of {@link FuturePNR }
     * 
     */
    public FuturePNR createFuturePNR() {
        return new FuturePNR();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link MemberSalesOfficeDtl }
     * 
     */
    public MemberSalesOfficeDtl createMemberSalesOfficeDtl() {
        return new MemberSalesOfficeDtl();
    }

    /**
     * Create an instance of {@link ArrayOfAchieve }
     * 
     */
    public ArrayOfAchieve createArrayOfAchieve() {
        return new ArrayOfAchieve();
    }

    /**
     * Create an instance of {@link MileageCalculator }
     * 
     */
    public MileageCalculator createMileageCalculator() {
        return new MileageCalculator();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticationHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://skywards.com/Mercator.CRIS.WS", name = "AuthenticationHeader")
    public JAXBElement<AuthenticationHeader> createAuthenticationHeader(AuthenticationHeader value) {
        return new JAXBElement<AuthenticationHeader>(_AuthenticationHeader_QNAME, AuthenticationHeader.class, null, value);
    }

}
