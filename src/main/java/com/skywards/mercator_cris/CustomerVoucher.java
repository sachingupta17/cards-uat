
package com.skywards.mercator_cris;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerVoucher complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerVoucher">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PersonID" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt32"/>
 *         &lt;element name="ActiveCardno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerVoucherNo" type="{http://skywards.com/Mercator.CRIS.WS}QueryableInt64"/>
 *         &lt;element name="VoucherDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerVoucherCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IssueTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IssueDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="SalesOffice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PullDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="PullRequired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cancelled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UseCount" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="UtilizedCount" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDecimal"/>
 *         &lt;element name="VoucherUtilized" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExpiryDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *         &lt;element name="VoucherUtilizedDate" type="{http://skywards.com/Mercator.CRIS.WS}QueryableDateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerVoucher", propOrder = {
    "personID",
    "activeCardno",
    "customerVoucherNo",
    "voucherDescription",
    "voucherType",
    "customerVoucherCode",
    "issueTo",
    "issueDate",
    "salesOffice",
    "pullDate",
    "pullRequired",
    "cancelled",
    "useCount",
    "utilizedCount",
    "voucherUtilized",
    "expiryDate",
    "voucherUtilizedDate"
})
public class CustomerVoucher {

    @XmlElement(name = "PersonID", required = true)
    protected QueryableInt32 personID;
    @XmlElement(name = "ActiveCardno")
    protected String activeCardno;
    @XmlElement(name = "CustomerVoucherNo", required = true)
    protected QueryableInt64 customerVoucherNo;
    @XmlElement(name = "VoucherDescription")
    protected String voucherDescription;
    @XmlElement(name = "VoucherType")
    protected String voucherType;
    @XmlElement(name = "CustomerVoucherCode")
    protected String customerVoucherCode;
    @XmlElement(name = "IssueTo")
    protected String issueTo;
    @XmlElement(name = "IssueDate", required = true)
    protected QueryableDateTime issueDate;
    @XmlElement(name = "SalesOffice")
    protected String salesOffice;
    @XmlElement(name = "PullDate", required = true)
    protected QueryableDateTime pullDate;
    @XmlElement(name = "PullRequired")
    protected String pullRequired;
    @XmlElement(name = "Cancelled")
    protected String cancelled;
    @XmlElement(name = "UseCount", required = true)
    protected QueryableDecimal useCount;
    @XmlElement(name = "UtilizedCount", required = true)
    protected QueryableDecimal utilizedCount;
    @XmlElement(name = "VoucherUtilized")
    protected String voucherUtilized;
    @XmlElement(name = "ExpiryDate", required = true)
    protected QueryableDateTime expiryDate;
    @XmlElement(name = "VoucherUtilizedDate", required = true)
    protected QueryableDateTime voucherUtilizedDate;

    /**
     * Gets the value of the personID property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt32 }
     *     
     */
    public QueryableInt32 getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt32 }
     *     
     */
    public void setPersonID(QueryableInt32 value) {
        this.personID = value;
    }

    /**
     * Gets the value of the activeCardno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveCardno() {
        return activeCardno;
    }

    /**
     * Sets the value of the activeCardno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveCardno(String value) {
        this.activeCardno = value;
    }

    /**
     * Gets the value of the customerVoucherNo property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableInt64 }
     *     
     */
    public QueryableInt64 getCustomerVoucherNo() {
        return customerVoucherNo;
    }

    /**
     * Sets the value of the customerVoucherNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableInt64 }
     *     
     */
    public void setCustomerVoucherNo(QueryableInt64 value) {
        this.customerVoucherNo = value;
    }

    /**
     * Gets the value of the voucherDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherDescription() {
        return voucherDescription;
    }

    /**
     * Sets the value of the voucherDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherDescription(String value) {
        this.voucherDescription = value;
    }

    /**
     * Gets the value of the voucherType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherType() {
        return voucherType;
    }

    /**
     * Sets the value of the voucherType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherType(String value) {
        this.voucherType = value;
    }

    /**
     * Gets the value of the customerVoucherCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerVoucherCode() {
        return customerVoucherCode;
    }

    /**
     * Sets the value of the customerVoucherCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerVoucherCode(String value) {
        this.customerVoucherCode = value;
    }

    /**
     * Gets the value of the issueTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueTo() {
        return issueTo;
    }

    /**
     * Sets the value of the issueTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueTo(String value) {
        this.issueTo = value;
    }

    /**
     * Gets the value of the issueDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getIssueDate() {
        return issueDate;
    }

    /**
     * Sets the value of the issueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setIssueDate(QueryableDateTime value) {
        this.issueDate = value;
    }

    /**
     * Gets the value of the salesOffice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesOffice() {
        return salesOffice;
    }

    /**
     * Sets the value of the salesOffice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesOffice(String value) {
        this.salesOffice = value;
    }

    /**
     * Gets the value of the pullDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getPullDate() {
        return pullDate;
    }

    /**
     * Sets the value of the pullDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setPullDate(QueryableDateTime value) {
        this.pullDate = value;
    }

    /**
     * Gets the value of the pullRequired property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPullRequired() {
        return pullRequired;
    }

    /**
     * Sets the value of the pullRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPullRequired(String value) {
        this.pullRequired = value;
    }

    /**
     * Gets the value of the cancelled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelled() {
        return cancelled;
    }

    /**
     * Sets the value of the cancelled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelled(String value) {
        this.cancelled = value;
    }

    /**
     * Gets the value of the useCount property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getUseCount() {
        return useCount;
    }

    /**
     * Sets the value of the useCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setUseCount(QueryableDecimal value) {
        this.useCount = value;
    }

    /**
     * Gets the value of the utilizedCount property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDecimal }
     *     
     */
    public QueryableDecimal getUtilizedCount() {
        return utilizedCount;
    }

    /**
     * Sets the value of the utilizedCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDecimal }
     *     
     */
    public void setUtilizedCount(QueryableDecimal value) {
        this.utilizedCount = value;
    }

    /**
     * Gets the value of the voucherUtilized property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherUtilized() {
        return voucherUtilized;
    }

    /**
     * Sets the value of the voucherUtilized property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherUtilized(String value) {
        this.voucherUtilized = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setExpiryDate(QueryableDateTime value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the voucherUtilizedDate property.
     * 
     * @return
     *     possible object is
     *     {@link QueryableDateTime }
     *     
     */
    public QueryableDateTime getVoucherUtilizedDate() {
        return voucherUtilizedDate;
    }

    /**
     * Sets the value of the voucherUtilizedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryableDateTime }
     *     
     */
    public void setVoucherUtilizedDate(QueryableDateTime value) {
        this.voucherUtilizedDate = value;
    }

}
