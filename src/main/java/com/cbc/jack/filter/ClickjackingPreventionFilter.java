/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.jack.filter;

/**
 *
 * @author Aravind E
 */
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class ClickjackingPreventionFilter implements Filter{
	private String mode = "DENY";
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String configMode = filterConfig.getInitParameter("mode");
	       if ( configMode != null ) {
	           mode = configMode;
	       }
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
//            System.out.println("--------req.getScheme-------->"+req.getScheme());
            HttpServletResponse response = (HttpServletResponse) resp;
           HttpServletRequest hsr=(HttpServletRequest)req;
            response.addHeader("X-Frame-Options", "DENY");
            
//            response.addHeader("Server", "");
            response.setHeader("Server", "Server");
//            response.setHeader("Strict-Transport-Security", "max-age=31536000 ; includeSubDomains");
            if (response.containsHeader("SET-COOKIE")) {
                String sessionid = ""+hsr.getSession().getId();
                response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid
                        + ";Path=/; secure; HttpOnly");
            }
            
//            response.setHeader("X-Forwarded-Proto", "https");
           
            chain.doFilter(req, response);
	    
	}

	@Override
	public void destroy() {
		
	}

}
