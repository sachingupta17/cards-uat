package com.cbc.portal.DAO;

import java.util.List;

import com.cbc.portal.beans.CityMappingBean;
import com.cbc.portal.entity.CcAmexApplicationStatus;
import com.cbc.portal.entity.CcBankApplication;
import com.cbc.portal.entity.CcBankwiseResponseMapping;
import com.cbc.portal.entity.CcBannerManagement;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.cbc.portal.entity.CcCardVarientURL;
import com.cbc.portal.entity.CcEnumValues;
import com.cbc.portal.entity.CcIciciApplicationStatus;
import com.cbc.portal.entity.CcOTP;
import com.cbc.portal.entity.CcPageContent;
import com.cbc.portal.entity.CcPageGroupLink;
import com.cbc.portal.entity.CcPageMaster;
import com.cbc.portal.entity.CcTimer;

public interface CommonDAO {

    public List<CcEnumValues> getEnumValues(String typeName);

    public CityMappingBean getCityMapping(String pin);

    public CityMappingBean getICICICityMapping(String pin);

    public CcAmexApplicationStatus getApplicationFormByPCN(String pcnNumber);

    public List<CcAmexApplicationStatus> getApplicationForJPNumber(String jpNumber);

    public List<CcIciciApplicationStatus> getICICIApplicationForJPNumber(String jpNumber);

    public List<CcBankApplication> getOtherApplicationForJPNumber(String jpNumber);

    public List<CcPageGroupLink> getGroupHeaders(int id);

//    public CcBannerManagement getActiveBanner();
    
    public List<CcBannerManagement> getActiveBanner();

    public CcPageContent getPageContent();

    public List<String> getCities();

    public List<String> getStates();

    public List<String> getICICICities();

    public List<String> getICICIStates();

    public String addBankApplication(CcBankApplication ccBank);

    public List<String> getAllCardFeatures(String ccPageGroupLink);

    public List<String> getCityNames(String cityName);

    public CcTimer getTimer();

    public Boolean checkExcludePincode(String pinCode, Integer bpNo);

    public Boolean checkICICIExcludePincode(String pinCode, Integer bpNo);

    public List<String> getCitiesByState(String state);

    public List<String> getICICICitiesByState(String state);

    public String getStateByCity(String city);

    public String getICICIStateByCity(String city);

    public String getTermdAndCondition(int cpNo);

    public List<String> getPincode();

    public List<String> getUpgradeMessage();

    public List<String> getUpgradyUrl();

    public String getAmexStatus(String jetpriviligemembershipNumber);

    public String getRandomNumber(String jetpriviligemembershipNumber);

    public List<String> getICICISelectedValues();

    public List<String> getCardType();

    public List<String> getCoutry();

    public List<String> getCompany();

    public List<String> getemploymentstatusSalaried(Integer bpno, Integer cpno);

    public List<String> getemploymentstatusSelfemployed(Integer bpno, Integer cpno);

    public List getagecriteriaAgeabove(String bpNo, String cpNo, String EmpType, String City);

    public List getagecriteriaAgebelow(String bpNo, String cpNo, String EmpType, String City);

    public List<String> getCompanyByLetter(String companyName);

    public List<String> getAllCities();

    //new cr2
    public List<CcBankwiseResponseMapping> getResponseByStatus(String BpStatus, Integer bpNo);

    public int getBankStatusIdByName(String bsStatus);

    public int saveOTP(CcOTP ccOTP);

    //cr3
    public String getSMSContent(String bsno, String bankname);

//    public String getSMSContent(String smsName);
    public String getIATACode(String City);

    public String getOtpForValidate(String mobileNo, String bankNumber, String formNumber, String otpTransactionId);

    public String UpdateApplicationForOtp(CcOTP ccOTP);

    public List<CcOTP> getOtpId(String mobileNo, String bankNumber, String formNumber, String otp);

    public CcOTP getOTPById(Integer otpid);

    public String getPromoCodeError(String errorCode);

    public String getEnrollCodeError(String errorCode);

    public List<CcPageMaster> getPageMasterDetail(int id);

    public List<CcCardVarientURL> checkCardVarientRandomNo(Integer cpNo, String randomNumber);

    public List<CcBeApplicationFormDetails> checkFormStatus(String randomNumber);
}
