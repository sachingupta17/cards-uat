/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.DAO;

import com.cbc.portal.entity.CcCreditErrorMessage;
import com.cbc.portal.entity.CcCreditReport;
import java.util.List;

/**
 *
 * @author Aravind E
 */
public interface ExperianAPIDao {

    public String saveAPIDetails(CcCreditReport creditReport);

    public List<CcCreditReport> getExistedCreditReport(String jpNum, String mobile);

    public CcCreditReport getReportById(Integer id);

    public String updateAPIDetails(CcCreditReport creditReport);

    public String getErrorMessage(String errorMessage, String reason);
}
