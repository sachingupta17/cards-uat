/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.DAO.impl;

import com.cbc.portal.DAO.ExperianAPIDao;
import com.cbc.portal.constants.CcHibernateQueries;
import com.cbc.portal.entity.CcCreditErrorMessage;
import com.cbc.portal.entity.CcCreditReport;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Aravind E
 */
@Repository
public class ExperianAPIDaoImpl implements ExperianAPIDao {

    private Logger logger = Logger.getLogger(ExperianAPIDaoImpl.class.getName());

    @Autowired
    SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public String saveAPIDetails(CcCreditReport creditReport) {

        String status = "";
        try {
            sessionFactory.getCurrentSession().save(creditReport);
            status = "success";
        } catch (Exception e) {
            logger.error("@@@@ Exception in ExperianAPIDaoImpl saveAPIDetails() :", e);
            status = "failure";
        }
        return status;

    }

    @SuppressWarnings("unchecked")
    @Override
    public String updateAPIDetails(CcCreditReport creditReport) {

        String status = "";
        try {
            sessionFactory.getCurrentSession().merge(creditReport);
            status = "success";
        } catch (Exception e) {
            logger.error("@@@@ Exception in ExperianAPIDaoImpl saveAPIDetails() :", e);
            status = "failure";
        }
        return status;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCreditReport> getExistedCreditReport(String jpNum, String mobile) {
        List<CcCreditReport> ccList = new ArrayList<>();
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.GET_EXISTED_CREDIT_REPORT);
            query.setParameter("jpNum", jpNum);
            query.setParameter("mobile", mobile);
            query.setMaxResults(1);
            ccList = query.list();
            logger.info("ccList" + ccList + "length" + ccList.size());

        } catch (Exception e) {
            logger.error("@@@@ Exception in ExperianAPIDaoImpl getExistedCreditReport() :", e);
        }
        return ccList;

    }

    @SuppressWarnings("unchecked")
    @Override
    public CcCreditReport getReportById(Integer id) {
        CcCreditReport ccCreditReport = null;
        try {
            ccCreditReport = (CcCreditReport) sessionFactory.getCurrentSession().get(CcCreditReport.class, id);

        } catch (Exception e) {
            logger.error("@@@@ Exception in ExperianAPIDaoImpl getReportById() :", e);

        }
        return ccCreditReport;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getErrorMessage(String errorMessage, String reason) {
        List<CcCreditErrorMessage> ccList = new ArrayList<>();
        String message = "";
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.GET_CREDIT_ERROR_MESSAGE);
            query.setParameter("errorMessage", errorMessage);
            query.setParameter("reason", reason);
            ccList = query.list();
            if (!ccList.isEmpty()) {
                for (CcCreditErrorMessage c : ccList) {
                    message = c.getErrorMessageUser();
                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ExperianAPIDaoImpl getErrorMessage() :", e);
        }
        return message;

    }
}
