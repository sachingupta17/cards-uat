package com.cbc.portal.DAO.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cbc.portal.DAO.CommonDAO;
import com.cbc.portal.beans.CityMappingBean;
import com.cbc.portal.constants.CcHibernateQueries;
import com.cbc.portal.entity.CcAmexApplicationStatus;
import com.cbc.portal.entity.CcApplicationFormDetails;
import com.cbc.portal.entity.CcBankApplication;
import com.cbc.portal.entity.CcBankwiseResponseMapping;
import com.cbc.portal.entity.CcBannerManagement;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.cbc.portal.entity.CcCardFeatures;
import com.cbc.portal.entity.CcCardVarientURL;
import com.cbc.portal.entity.CcEnumValues;
import com.cbc.portal.entity.CcIciciApplicationStatus;
import com.cbc.portal.entity.CcLocationCriteria;
import com.cbc.portal.entity.CcOTP;
import com.cbc.portal.entity.CcPageContent;
import com.cbc.portal.entity.CcPageGroupLink;
import com.cbc.portal.entity.CcPageMaster;
import com.cbc.portal.entity.CcPincitystateMapping;
import com.cbc.portal.entity.CcTimer;
import com.cbc.portal.utils.CommonUtils;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CommonDAOImpl implements CommonDAO {

    @Autowired
    SessionFactory sessionFactory;

    private static Logger logger = Logger.getLogger(CommonDAOImpl.class.getName());

    @SuppressWarnings("unchecked")
    @Override
    public List<CcEnumValues> getEnumValues(String typeName) {
        List<CcEnumValues> enumList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_ENUM_VALUES);
        query.setParameter("typeName", typeName);
        try {
            enumList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getEnumValues() :", e);
        }
        return enumList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public CityMappingBean getCityMapping(String pin) {
        CityMappingBean cityBean = null;
        Session session = sessionFactory.getCurrentSession();
        List<CityMappingBean> beanList = null;
        Query query = session.createQuery(CcHibernateQueries.CC_GET_CITY_MAPPINGS_BY_PIN);
        query.setString("pinCode", pin);
        try {
            beanList = query.list();
            if (!beanList.isEmpty()) {
                cityBean = beanList.get(0);
            } else {
                cityBean = new CityMappingBean();
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getCityMapping() :", e);
        }
        return cityBean;
    }

    @SuppressWarnings("unchecked")
    @Override
    public CityMappingBean getICICICityMapping(String pin) {
        CityMappingBean cityBean = null;
        Session session = sessionFactory.getCurrentSession();
        List<CityMappingBean> beanList = null;
        Query query = session.createQuery(CcHibernateQueries.CC_GET_ICICI_CITY_MAPPINGS_BY_PIN);
        query.setString("pinCode", pin);
        try {
            beanList = query.list();
            if (!beanList.isEmpty()) {
                cityBean = beanList.get(0);
            } else {
                cityBean = new CityMappingBean();
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getICICICityMapping() :", e);
        }
        return cityBean;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCitiesByState(String state) {
        List<String> cityList = new ArrayList<>();
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_CITY_MAPPINGS_BY_STATE);
        query.setParameter("state", state);
        try {
            cityList = query.list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getCitiesByState() :", e);
        }
        return cityList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getICICICitiesByState(String state) {
        List<String> cityList = new ArrayList<>();
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_ICICI_CITY_MAPPINGS_BY_STATE);
        query.setParameter("state", state);
        try {
            cityList = query.list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getICICICitiesByState() :", e);
        }
        return cityList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getStateByCity(String city) {
        Session session = sessionFactory.getCurrentSession();
        List<String> beanList1 = null;
        String state = "";
        Query query = session.createQuery(CcHibernateQueries.CC_GET_STATE_MAPPINGS_BY_CITY);
        query.setString("city", city);
        try {
            beanList1 = query.list();
            if (!beanList1.isEmpty()) {
                state = beanList1.get(0);
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getStateByCity() :", e);
        }
        return state;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getICICIStateByCity(String city) {
        Session session = sessionFactory.getCurrentSession();
        List<String> beanList1 = null;
        String state = "";

        Query query = session.createQuery(CcHibernateQueries.CC_GET_ICICI_STATE_MAPPINGS_BY_CITY);
        query.setString("city", city);
        try {
            beanList1 = query.list();
            if (!beanList1.isEmpty()) {
                state = beanList1.get(0);
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getICICIStateByCity() :", e);
        }
        return state;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCities() {
        List<String> cities = null;

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_CITIES);

        try {
            cities = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getCities() :", e);
        }
        return cities;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getStates() {
        List<String> states = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_STATES);
        try {
            states = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getStates() :", e);
        }
        return states;
    }

    @Override
    public List<String> getICICICities() {
        List<String> cities = null;

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_ICICI_CITIES);

        try {
            cities = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getICICICities() :", e);
        }
        return cities;
    }

    @Override
    public List<String> getICICIStates() {
        List<String> states = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_ICICI_STATES);
        try {
            states = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getICICIStates() :", e);
        }
        return states;
    }

    @Override
    public CcAmexApplicationStatus getApplicationFormByPCN(String pcnNumber) {
        CcAmexApplicationStatus ccApplication = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_APPLICATION_PCN);
        query.setString("pcnNumber", pcnNumber);

        try {
            ccApplication = (CcAmexApplicationStatus) query.uniqueResult();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getApplicationFormByPCN() :", e);
        }
        return ccApplication;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcAmexApplicationStatus> getApplicationForJPNumber(String jpNumberStr) {

        String jpNumber = null != jpNumberStr ? jpNumberStr.trim() : "";

        jpNumber = CommonUtils.getJPNumber(jpNumber);
        List<CcAmexApplicationStatus> ccAmexList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_APPLICATION_JP);
        query.setString("jpNumber", jpNumber);

        try {
            ccAmexList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getApplicationForJPNumber() :", e);
        }
        return ccAmexList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcIciciApplicationStatus> getICICIApplicationForJPNumber(String jpNumberStr) {

        String jpNumber = null != jpNumberStr ? jpNumberStr.trim() : "";

        jpNumber = CommonUtils.getJPNumber(jpNumber);
        List<CcIciciApplicationStatus> ccICICIList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_ICICI_APPLICATION_JP);
        query.setString("jpNumber", jpNumber);

        try {
            ccICICIList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getICICIApplicationForJPNumber() :", e);
        }
        return ccICICIList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcBankApplication> getOtherApplicationForJPNumber(String jpNumberStr) {
        String jpNumber = null != jpNumberStr ? jpNumberStr.trim() : "";
        jpNumber = CommonUtils.getJPNumber(jpNumber);
        List<CcBankApplication> ccBankList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_BANK_APPLICATION_JP);
        query.setString("jpNumber", jpNumber);
        try {
            ccBankList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getOtherApplicationForJPNumber() :", e);
        }
        return ccBankList;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcPageGroupLink> getGroupHeaders(int id) {
        List<CcPageGroupLink> groupLinkList = null;
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(CcHibernateQueries.CC_GET_GROUPHEADERS_FOR_LISTING);
            query.setParameter("id", id);
            groupLinkList = query.list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getGroupHeaders() :", e);
        }
        return groupLinkList;

    }

//    @Override
//    public CcBannerManagement getActiveBanner() {
//        CcBannerManagement banner = null;
//        Session session = sessionFactory.getCurrentSession();
//        Query query = session.createQuery(CcHibernateQueries.CC_GET_ACTIVE_BANNER);
//        try {
//            banner = (CcBannerManagement) query.uniqueResult();
//
//        } catch (Exception e) {
//            logger.error("@@@@ Exception in CommonDAOImpl getActiveBanner() :", e);
//        }
//        return banner;
//    }

    
    @Override
    public  List<CcBannerManagement> getActiveBanner() {
    	 List<CcBannerManagement> bannerList = new ArrayList<CcBannerManagement>();
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_ACTIVE_BANNER);
        try {
        	bannerList = query.list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getActiveBanner() :", e);
        }
        return bannerList;
    }
    
    
    
    @Override
    public CcPageContent getPageContent() {
        CcPageContent ccPage = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_PAGE_CONTENT);
        try {
            ccPage = (CcPageContent) query.uniqueResult();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getPageContent() :", e);
        }

        return ccPage;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getAllCardFeatures(String ccPageGroupLink) {
        List<String> cCardFeatures = null;
        Set<String> yourSet = new LinkedHashSet<>();
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_ALL_CARD_FEATURES);
        query.setParameter("list", ccPageGroupLink);
        try {
            cCardFeatures = query.list();
            for (String cflist : cCardFeatures) {

                yourSet.add(cflist);

            }
            cCardFeatures = new ArrayList<>();
            cCardFeatures.addAll(yourSet);
            cCardFeatures.removeAll(Arrays.asList("", null));
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getAllCardFeatures() :", e);
        }
        return cCardFeatures;
    }

    @Override
    public String addBankApplication(CcBankApplication ccBank) {
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(ccBank);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl addBankApplication() :", e);
            return "failure";
        }
        return "success";
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCityNames(String cityName) {
        List<String> cityList = null;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_CITY_NAMES);
            query.setParameter("cityName", cityName);
            cityList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getCityNames() :", e);
        }
        return cityList;
    }

    @Override
    public CcTimer getTimer() {
        CcTimer ccTimer = null;
        try {
            ccTimer = (CcTimer) sessionFactory.getCurrentSession().get(CcTimer.class, 1);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getTimer() :", e);
        }
        return ccTimer;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Boolean checkExcludePincode(String pinCode, Integer bpNo) {
        Boolean isExcluded = false;
        List<CcLocationCriteria> ccList = null;
        CcLocationCriteria ccLocationCriteria = null;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_IS_PINCODE_EXCLUDED);
            query.setParameter("pinCode", Integer.parseInt(pinCode));
            query.setParameter("bpNo", bpNo);
            ccList = query.list();
            if (!ccList.isEmpty()) {
                ccLocationCriteria = ccList.get(0);
                isExcluded = ccLocationCriteria.getExcludeFlag() == 0 ? true : false;
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at checkExcludePincode() :", e);
        }
        return isExcluded;
    }

    @Override
    public Boolean checkICICIExcludePincode(String pinCode, Integer bpNo) {

        Boolean isExcluded = false;
        List<String> ccList = null;
        List<CcPincitystateMapping> ccLocationCriteria = null;

        try {

            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_ICICI_IS_PINCODE_EXCLUDED);
            query.setParameter("pinCode", Integer.parseInt(pinCode));
            query.setParameter("bpNo", bpNo); 
            ccList = query.list();
            if (!ccList.isEmpty()) {
                String ss = ccList.get(0);
                isExcluded = true;
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in icici CommonDAOImpl at checkICICIExcludePincode() :", e);
        }
        return isExcluded;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getTermdAndCondition(int cpNo) {
        List<CcPageGroupLink> ccPageGroupLinkList = null;
        List<CcCardFeatures> ccCardFeaturesList = null;
        int gmNo = 0;
        String termsAndCondition = "";
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_GROUPHEADERS_FOR_LISTING);
            query.setParameter("id", 4);
            ccPageGroupLinkList = query.list();
            if (!ccPageGroupLinkList.isEmpty()) {
                for (CcPageGroupLink ccPageGroupLink : ccPageGroupLinkList) {
                    gmNo = ccPageGroupLink.getCcGroupMaster().getGmNo();
                }
                Query query1 = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_CARD_FEATURES_BY_CP_GM_NO);
                query1.setParameter("cpNo", cpNo);
                query1.setParameter("groupNo", gmNo);
                ccCardFeaturesList = query1.list();
                if (!ccCardFeaturesList.isEmpty()) {
                    for (CcCardFeatures ccCardFeatures : ccCardFeaturesList) {
                        termsAndCondition = ccCardFeatures.getCfTermsAndConditions();
                    }
                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getTermdAndCondition() :", e);
        }
        return termsAndCondition;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getPincode() {
        List<String> pincode = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_PINCODE);
        try {
            pincode = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getPincode() :", e);
        }
        return pincode;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getUpgradeMessage() {
        List<String> message = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_BANK_UPGRADE_MASSAGE);
        try {
            message = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getUpgradeMessage() :", e);
        }
        return message;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getUpgradyUrl() {
        List<String> message = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_BANK_UPGRADE_URL);
        try {
            message = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getUpgradyUrl() :", e);
        }
        return message;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getAmexStatus(String jetpriviligemembershipNumber) {
        List<CcBeApplicationFormDetails> enumList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_AMEX_APP_STATUS);
        query.setParameter("jetpriviligemembershipNumber", jetpriviligemembershipNumber);
        query.setMaxResults(1);
        try {
            enumList = query.list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getAmexStatus() :", e);
        }
        return "" + enumList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getRandomNumber(String jetpriviligemembershipNumber) {

        List<CcBeApplicationFormDetails> enumList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_AMEX_APP_RANDOM);
        query.setParameter("jetpriviligemembershipNumber", jetpriviligemembershipNumber);
        query.setMaxResults(1);
        try {
            enumList = query.list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getRandomNumber() :", e);
        }
        return "" + enumList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getICICISelectedValues() {
        List<String> relationshipType = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_ENUM_SELECTED_VALUES);
        try {
            relationshipType = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getICICISelectedValues() :", e);
        }
        return relationshipType;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCardType() {
        List<String> cardType = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_CARDTYPE);
        try {
            cardType = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getCardType() :", e);
        }
        return cardType;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCoutry() {
        List<String> country = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_COUNTRY);
        try {
            country = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getCoutry() :", e);
        }
        return country;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCompany() {
        List<String> company = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_COMPANY_NAMES);
        try {
            company = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getCompany() :", e);
        }
        return company;
    }

    @Override
    public List<String> getemploymentstatusSalaried(Integer bpno, Integer cpno) {
        List status = null;
        List<String> sal = null;

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_EMPLOYMENT_STATUS_SALARIED);
        query.setParameter("bpno", bpno);
        query.setParameter("cpno", cpno);
        try {
            status = query.list();

            sal = new ArrayList(status);

        } catch (Exception e) {
            logger.info("getemploymentstatusSalaried Exception" + e);
        }

        return sal;
    }

    @Override
    public List<String> getemploymentstatusSelfemployed(Integer bpno, Integer cpno) {
        List status = null;

        List<String> self = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_EMPLOYMENT_STATUS_SELFEMP);
        query.setParameter("bpno", bpno);
        query.setParameter("cpno", cpno);
        try {
            status = query.list();
            self = new ArrayList(status);

        } catch (Exception e) {
            logger.info("getemploymentstatusSelfemployed Exception" + e);
        }

        return self;
    }

    @Override
    public List getagecriteriaAgeabove(String bpNo, String cpNo, String EmpType, String City) {
        List ageabove = null;
        try {
            if ("Salaried".equals(EmpType)) {
                EmpType = "Salaried";
            } else if ("Self Employed-Professional".equals(EmpType) || "Self Employed-Business".equals(EmpType)) {
                EmpType = "Self Employed";
            }

            Session session = sessionFactory.getCurrentSession();
            Query query1 = session.createQuery(CcHibernateQueries.CC_GET_ICICI_AGE_CRITERIA_MAPPED_CITY);
            query1.setParameter("bpno", 2);
            query1.setParameter("cpno", Integer.parseInt(cpNo));
            query1.setParameter("emptype", EmpType);
            List checkCity = query1.list();
            if (checkCity.indexOf(City) != -1) {
                Query query = session.createQuery(CcHibernateQueries.CC_GET_ICICI_AGE_CRITERIA_AGEABOVE_BY_CITY);
                query.setParameter("bpno", 2);
                query.setParameter("cpno", Integer.parseInt(cpNo));
                query.setParameter("emptype", EmpType);
                query.setParameter("city", City);

//    age=query.list();
                ageabove = query.list();
            } else {

                Query query = session.createQuery(CcHibernateQueries.CC_GET_ICICI_AGE_CRITERIA_AGEABOVE);
                query.setParameter("bpno", 2);
                query.setParameter("cpno", Integer.parseInt(cpNo));
                query.setParameter("emptype", EmpType);
                ageabove = query.list();
            }

        } catch (Exception e) {
            logger.info("Exception in getagecriteriaAgeabove" + e);
        }

        return ageabove;
    }

    @Override
    public List getagecriteriaAgebelow(String bpNo, String cpNo, String EmpType, String City) {
        List agebelow = null;
        try {
            if ("Salaried".equals(EmpType)) {
                EmpType = "Salaried";
            } else if ("Self Employed-Professional".equals(EmpType) || "Self Employed-Business".equals(EmpType)) {
                EmpType = "Self Employed";
            }

            Session session = sessionFactory.getCurrentSession();

            Query query1 = session.createQuery(CcHibernateQueries.CC_GET_ICICI_AGE_CRITERIA_MAPPED_CITY);
            query1.setParameter("bpno", 2);
            query1.setParameter("cpno", Integer.parseInt(cpNo));
            query1.setParameter("emptype", EmpType);
            List checkCity = query1.list();
            if (checkCity.indexOf(City) != -1) {
                Query query = session.createQuery(CcHibernateQueries.CC_GET_ICICI_AGE_CRITERIA_AGEBELOW_BY_CITY);
                query.setParameter("bpno", 2);
                query.setParameter("cpno", Integer.parseInt(cpNo));
                query.setParameter("emptype", EmpType);
                query.setParameter("city", City);

//    age=query.list();
                agebelow = query.list();
            } else {
                Query query = session.createQuery(CcHibernateQueries.CC_GET_ICICI_AGE_CRITERIA_AGEBELOW);
                query.setParameter("bpno", 2);
                query.setParameter("cpno", Integer.parseInt(cpNo));
                query.setParameter("emptype", EmpType);

                agebelow = query.list();
            }

//           
        } catch (Exception e) {
            logger.info("Exception in getagecriteriaAgebelow" + e);
        }

        return agebelow;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCompanyByLetter(String companyName) {
        List<String> company = null;
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(CcHibernateQueries.CC_GET_COMPANY_NAMES_BY_LETTER);
            query.setParameter("company", companyName + "%");
            company = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getCompanyByLetter() :", e);
        }
        return company;
    }

    
    
     @SuppressWarnings("unchecked")
 @Override
 public  List<String>  getAllCities() {
  List<String> cityList = new ArrayList<>();
  Session session = sessionFactory.getCurrentSession();
  Query query = session.createQuery(CcHibernateQueries.CC_GET_CITIES);
  
  try{
   cityList = query.list();

  }catch(Exception e){
   logger.error("@@@@ Exception in CommonDAOImpl at getAllCities() :",e);
  }
  return cityList;
}
   //new cr2
    @Override
    public List<CcBankwiseResponseMapping> getResponseByStatus(String BpStatus, Integer bpNo) {
     
       List<CcBankwiseResponseMapping> statusresponse1=null;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_BANKWISE_RESPONSE);
            query.setParameter("BpStatus", Integer.parseInt(BpStatus));
            query.setParameter("bpNo", bpNo);
          
            statusresponse1 = query.list();
           
            for(CcBankwiseResponseMapping b :statusresponse1){
            }
           
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getResponseByStatus() :", e);
        }
        return statusresponse1;
    }
     @Override
	public int getBankStatusIdByName(String bsStatus) {
		int cpNo = 0;
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_BANKSTATUS_ID_BY_STATUS);
			query.setParameter("bsStatus", bsStatus);
			cpNo = (int) query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in CommonDAOImpl getBankStatusIdByName() :",e);	
		}
		return cpNo;
	}

    @Override
    public int saveOTP(CcOTP ccOTP) {
    Integer id = null;
		try {
                    logger.info("Mobile Number"+ccOTP.getMobileNumber()+"Otp Value "+ccOTP.getOtpValue()+" Otp Date "+ccOTP.getDate()+"Otp Status "+ccOTP.getStatus());
                    id=(Integer)sessionFactory.getCurrentSession().save(ccOTP);
		}catch (Exception e) {
                  e.printStackTrace();
			logger.error("@@@@ Exception in  save OTP :",e);	
			return 0;
		}
		 return id;
        
        

    }
 //cr3 start
    @Override
	public String getSMSContent(String smsName, String bankName) {
		String msg = "";
                List sms=new ArrayList();
		try{
                    logger.info("smsName"+smsName);
                     Session session = sessionFactory.getCurrentSession();
                    Query query = session.createQuery(CcHibernateQueries.CC_GET_SMS_CONTENT);
            query.setParameter("smsName", smsName);
            query.setParameter("bankno", Integer.parseInt(bankName));
			sms = query.list();
                       if (!sms.isEmpty()) {
                            msg=(String) sms.get(0);
//                        for(CcSmsDetails ms:m){
                            System.out.println("msg"+msg);
//                            msg=ms.getSmsContent();
//                        }
                        }
		}catch(Exception e){
			logger.error("@@@@ Exception in CommonDAOImpl getSMSContent() ",e);
		}
		return msg;
	}
        //cr3 end
 @Override
    public String getOtpForValidate(String mobileNo, String bankNumber, String formNumber, String otpTransactionId){
     String resp = "";
     String a="";
    try{
       String[]otps;
        List<String> otp = new ArrayList();
        List<CcOTP> otpvalid=null;
         Session session = sessionFactory.getCurrentSession();
         Query query;
         if(bankNumber.equals("")){
          query = session.createQuery(CcHibernateQueries.CC_GET_OTP_VALID_CREDIT);
            query.setParameter("mobileNumber", mobileNo);
            query.setParameter("formNumber", formNumber);
            query.setParameter("otpTransactionId", otpTransactionId);
         
         }else{
              query = session.createQuery(CcHibernateQueries.CC_GET_OTP_VALID);
            query.setParameter("mobileNumber", mobileNo);
            query.setParameter("bankNumber", Integer.parseInt(bankNumber));
            query.setParameter("formNumber", formNumber);
            query.setParameter("otpTransactionId", otpTransactionId);
         }
                    
            otpvalid=query.list();
            for(CcOTP ccotp:otpvalid){
//                System.out.println("otp value: "+ccotp.getOtpValue()+" mobileno: "+ccotp.getMobileNumber()+" time: "+ccotp.getDate()+" bpno:"+ccotp.getCcBankPartner().getBpNo());
//                System.out.println("otp vvv"+ccotp.getOtpValue());
                a=ccotp.getOtpTransactionId();
                
                  otp.add(a);
                
            }
          
            logger.info("otplist"+otp);
            for(int i=0; i<=otp.size();i++){
           resp= otp.get(0);
            }
            logger.info("resp"+resp);
    }catch(Exception e){
        logger.error("@@@@ Exception in CommonDAOImpl getOtpForValidate() ",e);
		
    }
return resp;
}
    @Override
	public List<CcOTP> getOtpId(String mobileNo, String bankNumber, String formNumber, String otp) {
		CcOTP ccOTP=null;
                List<CcOTP> otpdata=null;
		try{
                     
			 Session session = sessionFactory.getCurrentSession();
                    Query query = session.createQuery(CcHibernateQueries.CC_GET_OTP_BY_ID);
            query.setParameter("mobileNumber", mobileNo);
            query.setParameter("bankNumber", Integer.parseInt(bankNumber));
            query.setParameter("formNumber", formNumber);
            query.setParameter("otp", otp);
           otpdata= query.list();
           
      }catch(Exception e){
			 logger.error("@@@@ Exception in BankPartnerDAOImpl getBankPartnerById() :",e);
		}
		return otpdata;
	}
    @Override
    public String UpdateApplicationForOtp(CcOTP ccOTP){
     String status = "";
     
    try{
        sessionFactory.getCurrentSession().merge(ccOTP);
       status="success";
    }catch(Exception e){
        logger.error("@@@@ Exception in CommonDAOImpl getOtpForValidate() ",e);
	status="failure";	
    }
return status;
}
    @Override
	public CcOTP getOTPById(Integer otpid) {
		CcOTP ccBankPartnerBean=null;
		try{
			ccBankPartnerBean =(CcOTP) sessionFactory.getCurrentSession().get(CcOTP.class,otpid);
		}catch(Exception e){
			 logger.error("@@@@ Exception in BankPartnerDAOImpl getBankPartnerById() :",e);
		}
		return ccBankPartnerBean;
	}
        
        @Override
    public String getIATACode(String City) {
        String iataCode = "";
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_IATACODE);
            query.setParameter("city", City);
            iataCode = (String) query.uniqueResult();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl getBankStatusIdByName() :", e);
        }
        return iataCode;
    }
    
    @Override
    public String getPromoCodeError(String errorCode) {
        
        String message="";
           try {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.GET_PROMOCODE_ERROR);
         query.setParameter("erroCode", errorCode);
     
            
            message =""+query.uniqueResult();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonDAOImpl at getCities() :", e);
        }
        
       logger.info("In DAO class message"+message);
    
        
    return message;
    
    }
    @Override
    public String getEnrollCodeError(String errorCode) {
        
        String message="";
           try {
               logger.info("errorCode DAO "+errorCode);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.GET_ENROLLCODE_ERROR);
               logger.info("GET_ENROLLCODE_ERROR  "+query);
         query.setParameter("erroCode", errorCode);
     
            
//            message =""+query.uniqueResult();
                   message=(String)query.setMaxResults(1).uniqueResult();
        } catch (Exception e) { 
            logger.error("@@@@ Exception in CommonDAOImpl at getCities() :", e);
        }
        
        logger.info("In DAO class message"+message);
    
        
    return message;
    
    }
    @Override
        public List<CcPageMaster> getPageMasterDetail(int id) {
            List<CcPageMaster> pageMasterList= null;
            try {
                	 Session session = sessionFactory.getCurrentSession();
                    Query query = session.createQuery(CcHibernateQueries.CC_GET_PAGE_MASTER_DETAIL);
            query.setParameter("id", id);
            pageMasterList= query.list();
           
            } catch (Exception e) {
                logger.error("Exception at CommonDAOImpl getPageMasterDetail(): ",e);
            }
        return pageMasterList;
        }
@Override
    public List<CcCardVarientURL> checkCardVarientRandomNo(Integer cpNo, String randomNumber) {
List<CcCardVarientURL> cardvarientlist=new ArrayList();
        try {
             Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CHECK_BE_RANDOM_NUMBER);
            query.setParameter("cpNo", cpNo);
            query.setParameter("randomNumber", randomNumber);
            cardvarientlist = query.list();
            
        } catch (Exception e) {
            logger.error("Exception at CommonServiceImpl checkCardVarientRandomNo(): ",e);
        }
        return cardvarientlist;
        
    }
    @Override
    public List<CcBeApplicationFormDetails> checkFormStatus(String randomNumber) {
List<CcBeApplicationFormDetails> cclist=new ArrayList();
        try {
             Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CHECK_BE_FORM_STATUS);
//            query.setParameter("cpNo", cpNo);
            query.setParameter("randomNumber", randomNumber);
            cclist = query.list();
            
        } catch (Exception e) {
            logger.error("Exception at CommonServiceImpl checkCardVarientRandomNo(): ",e);
        }
        return cclist;
        
    }
}
