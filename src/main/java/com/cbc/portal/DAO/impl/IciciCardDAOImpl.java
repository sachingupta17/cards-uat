/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.DAO.impl;

import com.cbc.portal.DAO.IciciCardDAO;
import com.cbc.portal.constants.CcHibernateQueries;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.entity.CcAmexApplicationStatus;
import com.cbc.portal.entity.CcApplicationFormDetails;
import com.cbc.portal.entity.CcApplicationFormDetails;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.cbc.portal.entity.CcIciciApplicationStatus;
import com.cbc.portal.entity.CcMapOffers;
import com.cbc.portal.entity.CcOffers;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Aravind E
 */
@Repository
public class IciciCardDAOImpl implements IciciCardDAO {

    @Autowired
    SessionFactory sessionFactory;

    private static Logger logger = Logger.getLogger(IciciCardDAOImpl.class.getName());

    @Override
    public Integer submitICICIApplicationForm(CcApplicationFormDetails ccApplicationForm) {
        Integer id = null;
        try {

            id = (Integer) sessionFactory.getCurrentSession().save(ccApplicationForm);

        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardDAOImpl submitICICIPageApplicationForm() :", e);
            return 0;
        }
        return id;
    }

    @Override
    public CcApplicationFormDetails getApplicationFormById(Integer id) {
        CcApplicationFormDetails ccBean = null;
        try {
            ccBean = (CcApplicationFormDetails) sessionFactory.getCurrentSession().get(CcApplicationFormDetails.class, id);
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardDAOImpl getApplicationFormById() :", e);
        }
        return ccBean;
    }

    @Override
    public void addICICIStatus(CcIciciApplicationStatus ccAmexStatus) {
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(ccAmexStatus);
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardDAOImpl ccAmexStatus() :", e);
        }

    }

    @Override
    public CcOffers getDefaultOfffer(Integer bpNo) {
        CcOffers ccOffers = null;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_DEFAULT_OFFER);
            query.setParameter("bpNo", bpNo);
            ccOffers = (CcOffers) query.uniqueResult();
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardDAOImpl getDefaultOfffer() :", e);
        }
        return ccOffers;
    }

    @Override
    public CcMapOffers getOfferByJPNum(String jpNumber, Integer bpNo) {
        CcMapOffers ccMapOffers = null;
        Date dt = new Date();
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_OFFER_BY_JPNUM);
            query.setParameter("jpNumber", jpNumber);
            query.setParameter("bpNo", bpNo);
            query.setDate("deactivationDate", dt);
            ccMapOffers = (CcMapOffers) query.uniqueResult();
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardDAOImpl getOfferByJPNum() :", e);
        }
        return ccMapOffers;
    }

    @Override
    public String saveOrUpdateFormData(CcBeApplicationFormDetails ccBeApplicationFormDetails) {
   
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(ccBeApplicationFormDetails);
			                 System.out.println("=====data updated =====>");
		}catch (Exception e) {
			logger.error("@@@@ Exception in AmexCardDAOImpl submitAmexApplicationForm() :",e);	
			 return CcPortalConstants.FAILURE;
		}
		 return CcPortalConstants.SUCCESS;
		
	   
    }
    
    
    @Override
	public CcBeApplicationFormDetails checkICICIFormNumber(String formNumber) {
		CcBeApplicationFormDetails  ccBeApplicationFormDetails = null;
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_FORM_NUMBER_EXIST);
			query.setMaxResults(1);
//                        System.out.println("======checkFormNumber======>");
                        query.setParameter("formNumber", formNumber);
			ccBeApplicationFormDetails =  (CcBeApplicationFormDetails) query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in AmexCardDAOImpl checkFormNumber() :",e);	
		}
		return ccBeApplicationFormDetails;
	}
@Override
	public CcBeApplicationFormDetails getBEIciciFormDetails(String randomNumber, Integer cpNO) {
		CcBeApplicationFormDetails ccBeApplicationFormDetails = null;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(CcHibernateQueries.CC_GET_BE_ICICI_FORM);
		query.setParameter("randomNumber", randomNumber);
                query.setParameter("cpNo", cpNO);
		try{
			ccBeApplicationFormDetails = (CcBeApplicationFormDetails) query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in AmexCardDAOImpl at getBEAmexFormDetails() :",e);
		}
		return ccBeApplicationFormDetails;
	}

}
