package com.cbc.portal.DAO.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.cbc.portal.DAO.RecommendCardDAO;
import com.cbc.portal.constants.CcHibernateQueries;
import com.cbc.portal.constants.CcPortalConstants;

@Repository
public class RecommendCardDAOImpl implements RecommendCardDAO{

	private Logger logger = Logger.getLogger(RecommendCardDAOImpl.class.getName());
	
	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getBankListByCity(String cityName) {
		List<Integer> bpNo = new ArrayList<>();
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_BANK_ID_BY_CITY);
			query.setParameter("cityName", cityName);
			bpNo = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendCardDAOImpl getBankListByCity() :",e);
		}
		return bpNo;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCardListByPref(List<Integer> lyfList) {
		List<String> cardNameList = new ArrayList<>();
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_CARDS_BY_PREF);
			query.setParameterList("lsbpNo", lyfList);
			cardNameList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendCardDAOImpl getCardListByPref() :",e);
		}
		return cardNameList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getEligibilityCardNames(List<Integer> bpNoList, int age, String annualEndPrice, List<String> cardName, String annualPrice) {
		List<String> cardNameList = new ArrayList<>();
		BigDecimal bg1 = new BigDecimal("12");
		BigDecimal monthlyEndPrice;
		Query query;
		try{
			if(annualPrice.length()>0){
				query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_ELIGIBILITY_CARDS_);
			}
			else{
				monthlyEndPrice	= new BigDecimal(annualEndPrice);
				monthlyEndPrice = monthlyEndPrice.divide(bg1,2,RoundingMode.CEILING);
				query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_ELIGIBILITY_CARDS);
				query.setParameter("monthlyEndPrice", monthlyEndPrice);
			}
			
			query.setParameterList("bpNo", bpNoList);
			query.setParameterList(CcPortalConstants.CARDNAME, cardName);
			query.setParameter("age", age);
			
			cardNameList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendCardDAOImpl getEligibilityCardNames() :",e);
		}
		return cardNameList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getCardNoByName(List<Integer> bpNoList, List<String> cardNameList) {
		List<Integer> cpNoList = new ArrayList<>();
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_CARD_NO_BY_NAME);
			query.setParameterList("bpNo", bpNoList);
			query.setParameterList(CcPortalConstants.CARDNAME, cardNameList);
			cpNoList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendCardDAOImpl getCardNoByName() :",e);
		}
		return cpNoList;
		}
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getLyfPrefNamesList(Integer cpNo, List<Integer> intLyfList) {
		List<String> namesList = new ArrayList<>();
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_LYF_PREF_NAME);
			query.setParameter("cpNo", cpNo);
			query.setParameterList("lsbpNo", intLyfList);
			namesList = query.list();
		}catch(Exception e){
			logger.error("@@@@ Exception in RecommendCardDAOImpl getLyfPrefNamesList() :",e);
		}
		return namesList;
	}
}
