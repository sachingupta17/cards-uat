package com.cbc.portal.DAO.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cbc.portal.DAO.AmexCardDAO;
import com.cbc.portal.constants.CcHibernateQueries;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.entity.CcAmexApplicationStatus;
import com.cbc.portal.entity.CcApplicationFormDetails;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.cbc.portal.entity.CcMapOffers;
import com.cbc.portal.entity.CcMemberDetails;
import com.cbc.portal.entity.CcOffers;
import java.util.ArrayList;

@Repository
public class AmexCardDAOImpl implements AmexCardDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	private static Logger logger = Logger.getLogger(AmexCardDAOImpl.class.getName());

	@Override
	public Integer submitAmexApplicationForm(CcApplicationFormDetails ccApplicationForm) {
		Integer id = null;
		try {
			id=(Integer)sessionFactory.getCurrentSession().save(ccApplicationForm);
			
		}catch (Exception e) {
			logger.error("@@@@ Exception in AmexCardDAOImpl submitAmexApplicationForm() :",e);	
			 return 0;
		}
		 return id;
	}

	@Override
	public CcApplicationFormDetails getApplicationFormById(Integer id) {
		CcApplicationFormDetails ccBean=null;
		try{
			ccBean=(CcApplicationFormDetails)sessionFactory.getCurrentSession().get(CcApplicationFormDetails.class,id);
		}catch(Exception e){
			logger.error("@@@@ Exception in AmexCardDAOImpl getApplicationFormById() :",e);	
		}
		return ccBean;
	}

	@Override
	public void addAmexStatus(CcAmexApplicationStatus ccAmexStatus) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(ccAmexStatus);
		} catch ( Exception e) {
			 logger.error("@@@@ Exception in AmexCardDAOImpl ccAmexStatus() :",e);
			
		}
	}
	@Override
	public CcOffers getDefaultOfffer(Integer bpNo) {
		CcOffers ccOffers = null;
		try{
                   
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_DEFAULT_OFFER);
			query.setParameter("bpNo", bpNo);
			ccOffers = (CcOffers) query.uniqueResult();
		}catch(Exception e){
			 logger.error("@@@@ Exception in AmexCardDAOImpl getDefaultOfffer() :",e);
		}
		return ccOffers;
	}

	@Override
	public CcMapOffers getOfferByJPNum(String jpNumber, Integer bpNo) {
		CcMapOffers ccMapOffers = null;
		Date dt = new Date();
		try{
                    
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_OFFER_BY_JPNUM);
			query.setParameter("jpNumber", jpNumber);
			query.setParameter("bpNo", bpNo);
			query.setDate("deactivationDate", dt);
			ccMapOffers = (CcMapOffers) query.uniqueResult();
                      
		}catch(Exception e){
			 logger.error("@@@@ Exception in AmexCardDAOImpl getOfferByJPNum() :",e);
		}
		return ccMapOffers;
	}

	@Override
	public CcBeApplicationFormDetails getBEAmexFormDetails(String randomNumber) {
		CcBeApplicationFormDetails ccBeApplicationFormDetails = null;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(CcHibernateQueries.CC_GET_BE_AMEX_FORM);
		query.setParameter("randomNumber", randomNumber);
		try{
			ccBeApplicationFormDetails = (CcBeApplicationFormDetails) query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in AmexCardDAOImpl at getBEAmexFormDetails() :",e);
		}
		return ccBeApplicationFormDetails;
	}

	@Override
	public String saveCallMeDetails(CcMemberDetails ccMemberDetails) {
		try{
			sessionFactory.getCurrentSession().saveOrUpdate(ccMemberDetails);
		}catch(Exception e){
			logger.error("@@@@ Exception in AmexCardDAOImpl saveCallMeDetails() :",e);
			return CcPortalConstants.FAILURE;
		}
		return CcPortalConstants.SUCCESS;
	}
	
	@Override
	public CcBeApplicationFormDetails checkFormNumber(String formNumber) {
		CcBeApplicationFormDetails  ccBeApplicationFormDetails = null;
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_FORM_NUMBER_EXIST);
			query.setMaxResults(1);
//                        System.out.println("======checkFormNumber======>");
                        query.setParameter("formNumber", formNumber);
			ccBeApplicationFormDetails =  (CcBeApplicationFormDetails) query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in AmexCardDAOImpl checkFormNumber() :",e);	
		}
		return ccBeApplicationFormDetails;
	}

	@Override
	public String saveOrUpdateFormData(CcBeApplicationFormDetails ccBeApplicationFormDetails) {
		try {
			sessionFactory.getCurrentSession().update(ccBeApplicationFormDetails);
//			                 System.out.println("=====data updated =====>");
		}catch (Exception e) {
			logger.error("@@@@ Exception in AmexCardDAOImpl submitAmexApplicationForm() :",e);	
			 return CcPortalConstants.FAILURE;
		}
		 return CcPortalConstants.SUCCESS;
		
	}
        
        
        @Override
	public String save(CcBeApplicationFormDetails ccBeApplicationFormDetails) {
		try {
			sessionFactory.getCurrentSession().save(ccBeApplicationFormDetails);
//			 System.out.println("=====data inserted =====>");
		}catch (Exception e) {
			logger.error("@@@@ Exception in AmexCardDAOImpl submitAmexApplicationForm() :",e);	
			 return CcPortalConstants.FAILURE;
		}
		 return CcPortalConstants.SUCCESS;
		
	}

	@Override
	public void updateUnicaFlag(String ufInterval) {
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_UPDATE_UNICA_FLAG);
			query.setParameter("unicaFlag", "N");
			query.setParameter("dateInterval", ufInterval);
			query.executeUpdate();
		}catch(Exception e){
			logger.error("@@@@ Exception in AmexCardDAOImpl updateUnicaFlag() :",e);	
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CcBeApplicationFormDetails> getUnicaFladData() {
		List<CcBeApplicationFormDetails> ccBeApplicationFormDetailsList = null;
		try{
			ccBeApplicationFormDetailsList = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_UNICA_FLAG_DATA).list();
		}catch(Exception e){
			logger.error("@@@@ Exception in AmexCardDAOImpl getUnicaFladData() :",e);
		}
		return ccBeApplicationFormDetailsList;
	}
        
        @SuppressWarnings("unchecked")
	@Override
       public List getFormNumber(String formNumber){
           List FormNumber=new ArrayList();
           try{
          Query query=sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_FORM_NUMBER_EXIST_NEW);
          query.setParameter("formNumber", formNumber);
          FormNumber=query.list();
          
           }catch(Exception e){
           logger.error("@@@@ Exception in AmexCardDAOImpl getFormNumber() :",e);
           }
           
        return FormNumber;
        }

}

