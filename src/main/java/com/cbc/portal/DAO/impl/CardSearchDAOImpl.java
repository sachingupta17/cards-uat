package com.cbc.portal.DAO.impl;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cbc.portal.DAO.CardSearchDAO;
import com.cbc.portal.constants.CcHibernateQueries;
import com.cbc.portal.entity.CcBankPartner;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcLifestyleBenefitPref;
import com.cbc.portal.entity.CcLifestyleBenefitPrefValues;

@Repository
public class CardSearchDAOImpl implements CardSearchDAO {

    private Logger logger = Logger.getLogger(CardSearchDAOImpl.class.getName());

    @Autowired
    SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<CcBankPartner> getBankList() {
        List<CcBankPartner> ccBankPartnerList = null;
        try {
            ccBankPartnerList = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_BANK_LIST).list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getBankList() :", e);
        }
        return ccBankPartnerList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcLifestyleBenefitPref> getPreferenceList() {
        List<CcLifestyleBenefitPref> ccCcLifestyleBenefitPrefList = null;
        try {
            ccCcLifestyleBenefitPrefList = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_LYFSTYLBENEFITPREF).list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getPreferenceList() :", e);
        }
        return ccCcLifestyleBenefitPrefList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardProduct> getAllCardList() {
        List<CcCardProduct> ccCardProductList = null;
        try {
            ccCardProductList = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_ALL_CARDS).list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getAllCardList() :", e);
        }
        return ccCardProductList;
    }
    
//    new code
    
    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardProduct> getAllTypeCardList() {
        List<CcCardProduct> ccCardProductList = null;
        try {
            ccCardProductList = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_ALL_TYPE_CARDS).list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getAllCardList() :", e);
        }
        return ccCardProductList;
    }
    

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardProduct> getCardListByBank(Integer bpNo) {
        List<CcCardProduct> cardList = null;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_CARDS_BY_BANK);
            query.setParameter("bpNo", bpNo);
            cardList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getCardListByBank() :", e);
        }
        return cardList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardProduct> getCardListByFees(BigDecimal fees) {
        List<CcCardProduct> cardList = null;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_CARDS_BY_FEES);
            query.setParameter("fees", fees);
            cardList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getCardListByFees() :", e);
        }
        return cardList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcLifestyleBenefitPrefValues> getCardListByLyfPref(Integer lsbpNo) {
        List<CcLifestyleBenefitPrefValues> prefList = null;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_CARDS_BY_LYFPREF);
            query.setParameter("lsbpNo", lsbpNo);
            prefList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getCardListByLyfPref() :", e);
        }
        return prefList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCity() {
        List<String> cityList = null;
        try {
            cityList = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_CITY_LIST).list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getCity() :", e);
        }
        return cityList;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Integer> getlsbpArray(Integer cpNo) {
        List<Integer> lsbpNo = null;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_LSBP_NO);
            query.setParameter("cpNo", cpNo);
            lsbpNo = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getlsbpArray() :", e);
        }
        return lsbpNo;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardProduct> getSelectedDebitCardList(Integer countryid) {

        List<CcCardProduct> ccCardProductList = null;
        try {

            System.out.println("Inside getSelectedDebitCardList DAOimpl called");
            System.out.println("countryid>>"+countryid);
        	Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_SELECTED_CARDS_CATEGORY);

            query.setParameter("countryid", countryid);

            ccCardProductList = query.list();
            System.out.println("ccCardProductList>>"+ccCardProductList.size());

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getAllCardList() :", e);
        }
        return ccCardProductList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardProduct> getSelectedCorporateCardList(Integer countryid) {

        List<CcCardProduct> ccCardProductList = null;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_SELECTED_CORPORATE_CATEGORY);

            query.setParameter("countryid", countryid);

            ccCardProductList = query.list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getAllCardList() :", e);
        }
        return ccCardProductList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcBankPartner> getbanklistcreditindia(Integer categoryid, Integer countryid) {
        List<CcBankPartner> ccBankPartnerList = null;
        try {
            Query query=null;
             if(categoryid == 5){
            query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_BANK_LIST_CRED_ALL);
            query.setParameter("country", countryid);
//            query.setParameter("category", categoryid);
            }else{
            query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_BANK_LIST_CRED_IND);
            query.setParameter("category", categoryid);
            query.setParameter("country", countryid);
            
            }
            

            ccBankPartnerList = query.list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getBankList() :", e);
        }
        return ccBankPartnerList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcBankPartner> getbanklistdebit(Integer categoryid) {
        List<CcBankPartner> ccBankPartnerList = null;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_BANK_LIST_DEB);

            query.setParameter("category", categoryid);
            ccBankPartnerList = query.list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getBankList() :", e);
        }
        return ccBankPartnerList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardProduct> getSelectedOtherCardList(Integer countryid) {

        List<CcCardProduct> ccCardProductList = null;
        try {
            System.out.println("Inside getSelectedOtherCardList called DAO IMPL");
        	Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_SELECTED_OTHER_CATEGORY);

            query.setParameter("countryid", countryid);
            System.out.println("countryid>>>>>>"+countryid);

            ccCardProductList = query.list();
            System.out.println("ccCardProductList>>>>>>>>>>>>>"+ccCardProductList.size());

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getAllCardList() :", e);
        }
        return ccCardProductList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardProduct> getSelectedBankAllCardList() {
        List<CcCardProduct> ccCardProductList = null;
        try {
            ccCardProductList = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_SELECTED_CARDS).list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchDAOImpl getAllCardList() :", e);
        }
        return ccCardProductList;
    }

}
