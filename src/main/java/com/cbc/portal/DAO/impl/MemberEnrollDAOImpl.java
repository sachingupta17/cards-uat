package com.cbc.portal.DAO.impl;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cbc.portal.DAO.MemberEnrollDAO;
import com.cbc.portal.constants.CcHibernateQueries;
import com.cbc.portal.entity.CcMemberEnrollment;

@Repository
public class MemberEnrollDAOImpl implements MemberEnrollDAO{

	private static Logger logger = Logger.getLogger(MemberEnrollDAOImpl.class.getName());
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public void memberEnrollment(CcMemberEnrollment ccMemberEnrollment) {
		try{
			sessionFactory.getCurrentSession().saveOrUpdate(ccMemberEnrollment);
		}catch(Exception e){
			logger.error("@@@@ Exception in MemberEnrollmentDAOImpl at memberEnrollment() ",e);
		}
		
	}

	@Override
	public String getEnrollErrorMessage(String message) {
		String enroll_error_msg = null;
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_ENROLL_ERROR_MESSAGE);
			query.setParameter("code", message);
			enroll_error_msg = (String) query.uniqueResult();
		}catch(Exception e){
			logger.error("@@@@ Exception in MemberEnrollmentDAOImpl at getEnrollErrorMessage() ",e);
		}
		return enroll_error_msg;
	}

	@Override
	public String getEnrollMediaCode(int enrollbp) {
		String enrollMediaCode = "";
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_ENROLL_MEDIA_CODE);
			query.setParameter("bpNo", enrollbp);
			enrollMediaCode = (String) query.uniqueResult();
                     
		}catch(Exception e){
			logger.error("@@@@ Exception in MemberEnrollmentDAOImpl at getEnrollMediaCode() ",e);
		}
		return enrollMediaCode;
	}

}
