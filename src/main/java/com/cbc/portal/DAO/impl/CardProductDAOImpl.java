package com.cbc.portal.DAO.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cbc.portal.DAO.CardProductDAO;
import com.cbc.portal.constants.CcHibernateQueries;
import com.cbc.portal.entity.CcBankPartner;
import com.cbc.portal.entity.CcCardFeatures;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcLifestyleBenefitPrefValues;

@Repository
public class CardProductDAOImpl implements CardProductDAO {

    @Autowired
    SessionFactory sessionFactory;

    private static Logger logger = Logger.getLogger(CardProductDAOImpl.class.getName());

    @Override
    public CcBankPartner getBankPartnerById(int id) {
        CcBankPartner ccBean = null;
        try {
            ccBean = (CcBankPartner) sessionFactory.getCurrentSession().get(CcBankPartner.class, id);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardProductDAOImpl getBankPartnerById() :", e);
        }
        return ccBean;
    }

    @Override
    public CcCardProduct getCardProductById(int id) {
        CcCardProduct ccCardProduct = null;
        try {

            ccCardProduct = (CcCardProduct) sessionFactory.getCurrentSession().get(CcCardProduct.class, id);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardProductDAOImpl getCardProductById() :", e);
        }
        return ccCardProduct;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardFeatures> getCardFeatureById(Integer cpNo) {
        List<CcCardFeatures> ccList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_CARD_FEATURES);
        query.setParameter("cpNo", cpNo);
        try {
            ccList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardProductDAOImpl at getCardFeatureById() :", e);
        }
        return ccList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardFeatures> getCardFeatureBygroup(Integer next) {
        List<CcCardFeatures> ccList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_CARD_FEATURES_BY_GROUP);
        query.setParameter("groupNo", next);
        try {
            ccList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardProductDAOImpl at getCardFeatureBygroup() :", e);
        }
        return ccList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardFeatures> getALLCardFeatures() {
        List<CcCardFeatures> ccList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_ALL_CARD_FEATURES);
        try {
            ccList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardProductDAOImpl at getALLCardFeatures() :", e);
        }
        return ccList;
    }

    @Override
    public CcCardFeatures getCardFeatureByrow(String gmNo, String feature, Integer cpNo) {
        CcCardFeatures ccList = null;

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_CARD_FEATURES_BY_ROW);
        query.setParameter("groupNo", gmNo);
        query.setParameter("cpNo", cpNo);
        query.setParameter("featureName", feature);
        try {
            ccList = (CcCardFeatures) query.uniqueResult();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardProductDAOImpl at getCardFeatureByrow() :", e);

        }
        return ccList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcLifestyleBenefitPrefValues> getLSBPValuesByCPID(Integer key) {
        List<CcLifestyleBenefitPrefValues> ccList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_LSBP_BY_CPNO);
        query.setParameter("cpNo", key);
        try {
            ccList = query.list();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardProductDAOImpl at getLSBPValuesByCPID() :", e);
        }
        return ccList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CcCardFeatures> getCardFeatureforDetails(Integer integer, Integer cpNo) {
        List<CcCardFeatures> ccList = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_CARD_FEATURES_FOR_DETAILS);
        query.setParameter("gmNo", integer);
        query.setParameter("cpNo", cpNo);
        try {
            ccList = query.list();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardProductDAOImpl at getCardFeatureforDetails() :", e);
        }
        return ccList;
    }

    @Override
    public CcCardFeatures getCardFeatureBycpNogmNo(Integer gmNo, Integer cpNo) {
        CcCardFeatures ccBean = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(CcHibernateQueries.CC_GET_CARD_FEATURES_BY_CP_GM_NO);
        query.setParameter("groupNo", gmNo);
        query.setParameter("cpNo", cpNo);
        try {
            ccBean = (CcCardFeatures) query.uniqueResult();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardProductDAOImpl at getCardFeatureBycpNogmNo() :", e);
        }
        return ccBean;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getBenefitContent(int cpNo, int gmNo) {
        List<CcCardFeatures> ccList = null;
        String benefitContent = "";
        Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_CARD_FEATURES_FOR_DETAILS);
        query.setParameter("gmNo", gmNo);
        query.setParameter("cpNo", cpNo);
        try {
            ccList = query.list();
            if (null != ccList) {
                for (CcCardFeatures features : ccList) {
                    benefitContent = features.getCfContent();
                }
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardProductDAOImpl at getCardFeatureforDetails() :", e);
        }
        return benefitContent;
    }

    @Override
    public int getCardIdByName(String cardName) {
        int cpNo = 0;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(CcHibernateQueries.CC_GET_CARD_ID_BY_CARD_NAME);
            query.setParameter("cardName", cardName);
            cpNo = (int) query.uniqueResult();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardDetailsServiceImpl getCardIdByName() :", e);
        }
        return cpNo;
    }

}
