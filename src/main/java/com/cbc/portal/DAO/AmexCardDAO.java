package com.cbc.portal.DAO;

import java.util.List;

import com.cbc.portal.entity.CcAmexApplicationStatus;
import com.cbc.portal.entity.CcApplicationFormDetails;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.cbc.portal.entity.CcMemberDetails;
import com.cbc.portal.entity.CcMapOffers;
import com.cbc.portal.entity.CcOffers;

public interface AmexCardDAO {

	Integer submitAmexApplicationForm(CcApplicationFormDetails ccApplicationForm);

	CcApplicationFormDetails getApplicationFormById(Integer id);

	void addAmexStatus(CcAmexApplicationStatus ccAmexStatus);

	public 	CcBeApplicationFormDetails getBEAmexFormDetails(String randomNumber);

	String saveCallMeDetails(CcMemberDetails ccMemberDetails);

	String saveOrUpdateFormData(CcBeApplicationFormDetails ccBeApplicationFormDetails);
	
        String save(CcBeApplicationFormDetails ccBeApplicationFormDetails);

	CcBeApplicationFormDetails checkFormNumber(String formNumber);

	CcOffers getDefaultOfffer(Integer bpNo);

	CcMapOffers getOfferByJPNum(String jpNumber, Integer bpNo);

	void updateUnicaFlag(String ufInterval);

	List<CcBeApplicationFormDetails> getUnicaFladData();

        List getFormNumber(String formNumber);
        }
