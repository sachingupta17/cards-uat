package com.cbc.portal.DAO;

import java.util.List;

import com.cbc.portal.entity.CcBankPartner;
import com.cbc.portal.entity.CcCardFeatures;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcLifestyleBenefitPrefValues;

public interface CardProductDAO {
	
	CcBankPartner getBankPartnerById(int i);

	CcCardProduct getCardProductById(int i);

	List<CcCardFeatures> getCardFeatureById(Integer cpNo);

	List<CcCardFeatures> getCardFeatureBygroup(Integer next);

	List<CcCardFeatures> getALLCardFeatures();

	CcCardFeatures getCardFeatureByrow(String groupNumber, String string, Integer cpNo);
	
	CcCardFeatures getCardFeatureBycpNogmNo(Integer integer2,Integer cpNo);

	List<CcLifestyleBenefitPrefValues> getLSBPValuesByCPID(Integer key);

	List<CcCardFeatures> getCardFeatureforDetails(Integer integer, Integer cpNo);

	String getBenefitContent(int cpNo, int gmNo);

	int getCardIdByName(String cardName);
}
