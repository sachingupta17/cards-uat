package com.cbc.portal.DAO;

import java.util.List;

public interface RecommendCardDAO {

	List<Integer> getBankListByCity(String cityName);

	List<String> getCardListByPref(List<Integer> intLyfList);

	List<String> getEligibilityCardNames(List<Integer> bpNoList, int age, String annualEndPrice, List<String> cardNameList, String annualPrice);

	List<Integer> getCardNoByName(List<Integer> bpNoList, List<String> cardNameList);

	List<String> getLyfPrefNamesList(Integer cpNo, List<Integer> intLyfList);

}
