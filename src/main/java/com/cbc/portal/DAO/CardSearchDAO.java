package com.cbc.portal.DAO;

import java.math.BigDecimal;
import java.util.List;

import com.cbc.portal.entity.CcBankPartner;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcLifestyleBenefitPref;
import com.cbc.portal.entity.CcLifestyleBenefitPrefValues;

public interface CardSearchDAO {

    List<CcBankPartner> getBankList();

    List<CcLifestyleBenefitPref> getPreferenceList();

    List<CcCardProduct> getAllCardList();
    
//    List<CcCardProduct> getAllTypeCardList();
    List<CcCardProduct> getAllTypeCardList();

    List<CcCardProduct> getCardListByBank(Integer bpNo);

    List<CcCardProduct> getSelectedBankAllCardList();

    List<CcCardProduct> getCardListByFees(BigDecimal fees);

    List<CcLifestyleBenefitPrefValues> getCardListByLyfPref(Integer lsbpNo);

    List<String> getCity();

    List<Integer> getlsbpArray(Integer cpNo);

    List<CcCardProduct> getSelectedDebitCardList(Integer countryid);

    List<CcCardProduct> getSelectedCorporateCardList(Integer countryid);

    List<CcBankPartner> getbanklistcreditindia(Integer categoryid, Integer countryid);

    List<CcBankPartner> getbanklistdebit(Integer categoryid);

    List<CcCardProduct> getSelectedOtherCardList(Integer countryid);

}
