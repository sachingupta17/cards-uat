/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.DAO;

import com.cbc.portal.entity.CcAmexApplicationStatus;
import com.cbc.portal.entity.CcApplicationFormDetails;
import com.cbc.portal.entity.CcApplicationFormDetails;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.cbc.portal.entity.CcIciciApplicationStatus;
import com.cbc.portal.entity.CcMapOffers;
import com.cbc.portal.entity.CcOffers;

/**
 *
 * @author Aravind E
 */
public interface IciciCardDAO {

    Integer submitICICIApplicationForm(CcApplicationFormDetails ccApplicationForm);

    CcApplicationFormDetails getApplicationFormById(Integer id);

    void addICICIStatus(CcIciciApplicationStatus applicationStatus);

//        void UpdateICICIStatus(CcIciciApplicationStatus applicationStatus,Integer id);
    CcOffers getDefaultOfffer(Integer bpNo);

    CcMapOffers getOfferByJPNum(String jpNumber, Integer bpNo);
    String saveOrUpdateFormData(CcBeApplicationFormDetails ccBeApplicationFormDetails);
    
    CcBeApplicationFormDetails checkICICIFormNumber(String formNumber);

	
    	public CcBeApplicationFormDetails getBEIciciFormDetails(String randomNumber, Integer cpNO) ;


}
