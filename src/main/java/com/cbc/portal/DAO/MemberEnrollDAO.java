package com.cbc.portal.DAO;

import com.cbc.portal.entity.CcMemberEnrollment;

public interface MemberEnrollDAO {
	
	public void memberEnrollment(CcMemberEnrollment ccMemberEnrollment);

	public String getEnrollErrorMessage(String message);

	public String getEnrollMediaCode(int enrollbp);
}
