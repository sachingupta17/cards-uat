package com.cbc.portal.beans;

public class CityMappingBean {
	private String pincode;
	private String city;
	private String state;
	
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		city = this.city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		state = this.state;
	}
	
	public CityMappingBean() {
		super();
	}
	
	public CityMappingBean(String pincode, String city, String state) {
		super();
		this.pincode = pincode;
		this.city = city;
		this.state = state;
	}
	
	@Override
	public String toString() {
		return "CityMappingBean [pincode=" + pincode + ", City=" + city
				+ ", State=" + state + "]";
	}
	

	
}
