package com.cbc.portal.beans;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.math.BigDecimal;

/**
 *
 * @author Aravind E
 */
public class IciciBean {

    private Integer appFormNo;
    private Integer bpNo;
    private Integer cpNo;
    private String title;
    private String fullName;
    private String fname;
    private String mname;
    private String lname;
    private String email;
    private String mobile;
    private String dateOfBirth;
    private String gender;
    private String eduQualification;
    private String aadharNumber;
    private String pancard;
    private BigDecimal monthlyIncome;
    private String address;
    private String address2;
    private String address3;
    private String city;
    private String state1;
    private String PCity;
    private String PState;
    private String PpinCode;
    private String pinCode;
    private String peraddresssameascurr;
    private String permaddress;
    private String permaddress2;
    private String permaddress3;
    private String jetpriviligemember;
    private String jetpriviligemembershipNumber;
    private String jetpriviligemembershipTier;
    private String employmentType;
    private String companyName;
    private String OAddress;
    private String OAddress2;
    private String OAddress3;
    private String OCity;
    private String OState;
    private String OPincode;
    private String phone;
    private String std;
    private String OStd;
    private String OPhone;
    private String appStatus;
    private String reason;
    private String stmtTosent;
    private String termsAndConditions;
    private String errorMsg;
    private String jpNum;
    private String jpTier;
    private String enrollTitle;
    private String enrollFname;
    private String enrollMname;
    private String enrollLname;
    private String enrollemail;
    private String enrollpassword;
    private String enrollPhone;
    private String enrollDob;
    private String message;
    private String permanentAddress;
    private String resedentialAddress;
    private String companyAddress;
    private String homeNumber;
    private String totalExperience;
    private String iciciBankRelationship;
    private String ICICIBankRelationShipNo;
    private String salaryBankWithOtherBank;
    private String salaryAccountOpened;
    private String officeNumber;

    private String channelType;

    private String ITRStaus;
    private String offerId;
    private String offerDesc;
    private String agentId;
    private String utmSource;
    private String utmMedium;
    private String utmCampaign;
    private String hcity;
    private String hstate;
    private String hcity1;
    private String hstate1;
    private String hcity2;
    private String hstate2;
    private String referenceId;
    private String Years;
    private String Months;
    //new
    private String pid;
    private String formNumber;
    private Integer otpVal;
    private String promocode;
    private String initiationDate;
    private String initiatedBy;
    private String emailInactiveFlag;
    private String sourceOfCall;
    private String randomNumber;
    private String beJpNumber;
    private String form_Status;
    private String otpTransactionId;
    private String token;
    private String pendingAttempt;

    public String getBeJpNumber() {
        return beJpNumber;
    }

    public void setBeJpNumber(String beJpNumber) {
        this.beJpNumber = beJpNumber;
    }

    public String getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(String randomNumber) {
        this.randomNumber = randomNumber;
    }

    public String getInitiationDate() {
        return initiationDate;
    }

    public void setInitiationDate(String initiationDate) {
        this.initiationDate = initiationDate;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public String getEmailInactiveFlag() {
        return emailInactiveFlag;
    }

    public void setEmailInactiveFlag(String emailInactiveFlag) {
        this.emailInactiveFlag = emailInactiveFlag;
    }

    public String getSourceOfCall() {
        return sourceOfCall;
    }

    public void setSourceOfCall(String sourceOfCall) {
        this.sourceOfCall = sourceOfCall;
    }

    public Integer getOtpVal() {
        return otpVal;
    }

    public void setOtpVal(Integer otpVal) {
        this.otpVal = otpVal;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
    // end    

    public String getIciciId() {
        return iciciId;
    }

    public void setIciciId(String iciciId) {
        this.iciciId = iciciId;
    }
    private String iciciId;

//------------
    public Integer getAppFormNo() {
        return appFormNo;
    }

    public void setAppFormNo(Integer appFormNo) {
        this.appFormNo = appFormNo;
    }

    public Integer getBpNo() {
        return bpNo;
    }

    public void setBpNo(Integer bpNo) {
        this.bpNo = bpNo;
    }

    public Integer getCpNo() {
        return cpNo;
    }

    public void setCpNo(Integer cpNo) {
        this.cpNo = cpNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEduQualification() {
        return eduQualification;
    }

    public void setEduQualification(String eduQualification) {
        this.eduQualification = eduQualification;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getPancard() {
        return pancard;
    }

    public void setPancard(String pancard) {
        this.pancard = pancard;
    }

    public BigDecimal getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(BigDecimal monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState1() {
        return state1;
    }

    public void setState1(String state1) {
        this.state1 = state1;
    }

    public String getPCity() {
        return PCity;
    }

    public void setPCity(String PCity) {
        this.PCity = PCity;
    }

    public String getPState() {
        return PState;
    }

    public void setPState(String PState) {
        this.PState = PState;
    }

    public String getPpinCode() {
        return PpinCode;
    }

    public void setPpinCode(String PpinCode) {
        this.PpinCode = PpinCode;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getPeraddresssameascurr() {
        return peraddresssameascurr;
    }

    public void setPeraddresssameascurr(String peraddresssameascurr) {
        this.peraddresssameascurr = peraddresssameascurr;
    }

    public String getPermaddress() {
        return permaddress;
    }

    public void setPermaddress(String permaddress) {
        this.permaddress = permaddress;
    }

    public String getPermaddress2() {
        return permaddress2;
    }

    public void setPermaddress2(String permaddress2) {
        this.permaddress2 = permaddress2;
    }

    public String getPermaddress3() {
        return permaddress3;
    }

    public void setPermaddress3(String permaddress3) {
        this.permaddress3 = permaddress3;
    }

    public String getJetpriviligemember() {
        return jetpriviligemember;
    }

    public void setJetpriviligemember(String jetpriviligemember) {
        this.jetpriviligemember = jetpriviligemember;
    }

    public String getJetpriviligemembershipNumber() {
        return jetpriviligemembershipNumber;
    }

    public void setJetpriviligemembershipNumber(String jetpriviligemembershipNumber) {
        this.jetpriviligemembershipNumber = jetpriviligemembershipNumber;
    }

    public String getJetpriviligemembershipTier() {
        return jetpriviligemembershipTier;
    }

    public void setJetpriviligemembershipTier(String jetpriviligemembershipTier) {
        this.jetpriviligemembershipTier = jetpriviligemembershipTier;
    }

    public String getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(String employmentType) {
        this.employmentType = employmentType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOAddress() {
        return OAddress;
    }

    public void setOAddress(String OAddress) {
        this.OAddress = OAddress;
    }

    public String getOAddress2() {
        return OAddress2;
    }

    public void setOAddress2(String OAddress2) {
        this.OAddress2 = OAddress2;
    }

    public String getOAddress3() {
        return OAddress3;
    }

    public void setOAddress3(String OAddress3) {
        this.OAddress3 = OAddress3;
    }

    public String getOCity() {
        return OCity;
    }

    public void setOCity(String OCity) {
        this.OCity = OCity;
    }

    public String getOState() {
        return OState;
    }

    public void setOState(String OState) {
        this.OState = OState;
    }

    public String getOPincode() {
        return OPincode;
    }

    public void setOPincode(String OPincode) {
        this.OPincode = OPincode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStd() {
        return std;
    }

    public void setStd(String std) {
        this.std = std;
    }

    public String getOStd() {
        return OStd;
    }

    public void setOStd(String OStd) {
        this.OStd = OStd;
    }

    public String getOPhone() {
        return OPhone;
    }

    public void setOPhone(String OPhone) {
        this.OPhone = OPhone;
    }

    public String getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(String appStatus) {
        this.appStatus = appStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStmtTosent() {
        return stmtTosent;
    }

    public void setStmtTosent(String stmtTosent) {
        this.stmtTosent = stmtTosent;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getJpNum() {
        return jpNum;
    }

    public void setJpNum(String jpNum) {
        this.jpNum = jpNum;
    }

    public String getJpTier() {
        return jpTier;
    }

    public void setJpTier(String jpTier) {
        this.jpTier = jpTier;
    }

    public String getEnrollTitle() {
        return enrollTitle;
    }

    public void setEnrollTitle(String enrollTitle) {
        this.enrollTitle = enrollTitle;
    }

    public String getEnrollFname() {
        return enrollFname;
    }

    public void setEnrollFname(String enrollFname) {
        this.enrollFname = enrollFname;
    }

    public String getEnrollMname() {
        return enrollMname;
    }

    public void setEnrollMname(String enrollMname) {
        this.enrollMname = enrollMname;
    }

    public String getEnrollLname() {
        return enrollLname;
    }

    public void setEnrollLname(String enrollLname) {
        this.enrollLname = enrollLname;
    }

    public String getEnrollemail() {
        return enrollemail;
    }

    public void setEnrollemail(String enrollemail) {
        this.enrollemail = enrollemail;
    }

    public String getEnrollpassword() {
        return enrollpassword;
    }

    public void setEnrollpassword(String enrollpassword) {
        this.enrollpassword = enrollpassword;
    }

    public String getEnrollPhone() {
        return enrollPhone;
    }

    public void setEnrollPhone(String enrollPhone) {
        this.enrollPhone = enrollPhone;
    }

    public String getEnrollDob() {
        return enrollDob;
    }

    public void setEnrollDob(String enrollDob) {
        this.enrollDob = enrollDob;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getResedentialAddress() {
        return resedentialAddress;
    }

    public void setResedentialAddress(String resedentialAddress) {
        this.resedentialAddress = resedentialAddress;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getTotalExperience() {
        return totalExperience;
    }

    public void setTotalExperience(String totalExperience) {
        this.totalExperience = totalExperience;
    }

    public String getIciciBankRelationship() {
        return iciciBankRelationship;
    }

    public void setIciciBankRelationship(String iciciBankRelationship) {
        this.iciciBankRelationship = iciciBankRelationship;
    }

    public String getICICIBankRelationShipNo() {
        return ICICIBankRelationShipNo;
    }

    public void setICICIBankRelationShipNo(String ICICIBankRelationShipNo) {
        this.ICICIBankRelationShipNo = ICICIBankRelationShipNo;
    }

    public String getSalaryBankWithOtherBank() {
        return salaryBankWithOtherBank;
    }

    public void setSalaryBankWithOtherBank(String salaryBankWithOtherBank) {
        this.salaryBankWithOtherBank = salaryBankWithOtherBank;
    }

    public String getSalaryAccountOpened() {
        return salaryAccountOpened;
    }

    public void setSalaryAccountOpened(String salaryAccountOpened) {
        this.salaryAccountOpened = salaryAccountOpened;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getOfferDesc() {
        return offerDesc;
    }

    public void setOfferDesc(String offerDesc) {
        this.offerDesc = offerDesc;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getHcity() {
        return hcity;
    }

    public void setHcity(String hcity) {
        this.hcity = hcity;
    }

    public String getHstate() {
        return hstate;
    }

    public void setHstate(String hstate) {
        this.hstate = hstate;
    }

    public String getHcity1() {
        return hcity1;
    }

    public void setHcity1(String hcity1) {
        this.hcity1 = hcity1;
    }

    public String getHstate1() {
        return hstate1;
    }

    public void setHstate1(String hstate1) {
        this.hstate1 = hstate1;
    }

    public String getHcity2() {
        return hcity2;
    }

    public void setHcity2(String hcity2) {
        this.hcity2 = hcity2;
    }

    public String getHstate2() {
        return hstate2;
    }

    public void setHstate2(String hstate2) {
        this.hstate2 = hstate2;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getITRStaus() {
        return ITRStaus;
    }

    public void setITRStaus(String ITRStaus) {
        this.ITRStaus = ITRStaus;
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getYears() {
        return Years;
    }

    public void setYears(String Years) {
        this.Years = Years;
    }

    public String getMonths() {
        return Months;
    }

    public void setMonths(String Months) {
        this.Months = Months;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public String getForm_Status() {
        return form_Status;
    }

    public void setForm_Status(String form_Status) {
        this.form_Status = form_Status;
    }

    public String getOtpTransactionId() {
        return otpTransactionId;
    }

    public void setOtpTransactionId(String otpTransactionId) {
        this.otpTransactionId = otpTransactionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPendingAttempt() {
        return pendingAttempt;
    }

    public void setPendingAttempt(String pendingAttempt) {
        this.pendingAttempt = pendingAttempt;
    }

}
