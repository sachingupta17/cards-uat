package com.cbc.portal.beans;

public class CallMeBean {

	private String socialName;
	private String socialPhone;
	private String socialEmail;
	private String socialjpNumber;
	private String formNumber;
	private String firstName;
	private String lastName;
        private String pageName;
        private String cardName;
	
	
	public String getSocialName() {
		return socialName;
	}
	public void setSocialName(String socialName) {
		this.socialName = socialName;
	}
	public String getSocialPhone() {
		return socialPhone;
	}
	public void setSocialPhone(String socialPhone) {
		this.socialPhone = socialPhone;
	}
	public String getSocialEmail() {
		return socialEmail;
	}
	public void setSocialEmail(String socialEmail) {
		this.socialEmail = socialEmail;
	}
	public String getSocialjpNumber() {
		return socialjpNumber;
	}
	public void setSocialjpNumber(String socialjpNumber) {
		this.socialjpNumber = socialjpNumber;
	}
	public String getFormNumber() {
		return formNumber;
	}
	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }
        
        
        
}
