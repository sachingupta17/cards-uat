package com.cbc.portal.beans;

import org.apache.commons.codec.binary.Base64;

public class BannerBean {

	private byte[] bannerImage;
	private String imageBase64;
	private String bannerAttachementPath;
	
	private Integer bpNo;
	private Integer cpNo;
	
	public byte[] getBannerImage() {
		return bannerImage;
	}
	public void setBannerImage(byte[] bannerImage) {
		this.bannerImage = bannerImage;
	}
	public String getImageBase64() {
		return new Base64().encodeToString( getBannerImage());
	}
	public void setImageBase64(String imageBase64) {
		this.imageBase64 = imageBase64;
	}
	public String getBannerAttachementPath() {
		return bannerAttachementPath;
	}
	public void setBannerAttachementPath(String bannerAttachementPath) {
		this.bannerAttachementPath = bannerAttachementPath;
	}
	
	public Integer getBpNo() {
		return bpNo;
	}
	public void setBpNo(Integer bpNo) {
		this.bpNo = bpNo;
	}
	public Integer getCpNo() {
		return cpNo;
	}
	public void setCpNo(Integer cpNo) {
		this.cpNo = cpNo;
	}
	
}
