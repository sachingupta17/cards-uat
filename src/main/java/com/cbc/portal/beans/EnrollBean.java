package com.cbc.portal.beans;

public class EnrollBean {

    private String enrollTitle;
    private String enrollFname;
    private String enrollMname;
    private String enrollLname;
    private String enrollemail;
    private String enrollPassword;
    private String enrollRenterPassword;
    private String hashPassword;
    private String enrollPhone;
    private String enrollDob;
    private String enrollGender;
    private String message;
    private String jpNumber;
    private String jpTier;
    private String enrollCity;
    private Integer enrollbpNo;
    private String responseCode;
    private String termsAndConditionsEnroll;
    private String recieveMarketingTeam;
    private Integer otpVal;
    private String enrollApplicationInitiatedBy;
    private String enrollAgentId;

    public String getJpNumber() {
        return jpNumber;
    }

    public void setJpNumber(String jpNumber) {
        this.jpNumber = jpNumber;
    }

    public String getJpTier() {
        return jpTier;
    }

    public void setJpTier(String jpTier) {
        this.jpTier = jpTier;
    }

    public String getEnrollTitle() {
        return enrollTitle;
    }

    public void setEnrollTitle(String enrollTitle) {
        this.enrollTitle = enrollTitle;
    }

    public String getEnrollFname() {
        return enrollFname;
    }

    public void setEnrollFname(String enrollFname) {
        this.enrollFname = enrollFname;
    }

    public String getEnrollMname() {
        return enrollMname;
    }

    public void setEnrollMname(String enrollMname) {
        this.enrollMname = enrollMname;
    }

    public String getEnrollLname() {
        return enrollLname;
    }

    public void setEnrollLname(String enrollLname) {
        this.enrollLname = enrollLname;
    }

    public String getEnrollemail() {
        return enrollemail;
    }

    public void setEnrollemail(String enrollemail) {
        this.enrollemail = enrollemail;
    }

    public String getEnrollPassword() {
        return enrollPassword;
    }

    public void setEnrollPassword(String enrollPassword) {
        this.enrollPassword = enrollPassword;
    }

    public String getEnrollRenterPassword() {
        return enrollRenterPassword;
    }

    public void setEnrollRenterPassword(String enrollRenterPassword) {
        this.enrollRenterPassword = enrollRenterPassword;
    }

    public String getHashPassword() {
        return hashPassword;
    }

    public void setHashPassword(String hashPassword) {
        this.hashPassword = hashPassword;
    }

    public String getEnrollPhone() {
        return enrollPhone;
    }

    public void setEnrollPhone(String enrollPhone) {
        this.enrollPhone = enrollPhone;
    }

    public String getEnrollDob() {
        return enrollDob;
    }

    public void setEnrollDob(String enrollDob) {
        this.enrollDob = enrollDob;
    }

    public String getEnrollGender() {
        return enrollGender;
    }

    public void setEnrollGender(String enrollGender) {
        this.enrollGender = enrollGender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEnrollCity() {
        return enrollCity;
    }

    public void setEnrollCity(String enrollCity) {
        this.enrollCity = enrollCity;
    }

    public Integer getEnrollbpNo() {
        return enrollbpNo;
    }

    public void setEnrollbpNo(Integer enrollbpNo) {
        this.enrollbpNo = enrollbpNo;
    }

    public String getTermsAndConditionsEnroll() {
        return termsAndConditionsEnroll;
    }

    public void setTermsAndConditionsEnroll(String termsAndConditionsEnroll) {
        this.termsAndConditionsEnroll = termsAndConditionsEnroll;
    }

    public String getRecieveMarketingTeam() {
        return recieveMarketingTeam;
    }

    public void setRecieveMarketingTeam(String recieveMarketingTeam) {
        this.recieveMarketingTeam = recieveMarketingTeam;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
    public Integer getOtpVal() {
        return otpVal;
    }

    public void setOtpVal(Integer otpVal) {
        this.otpVal = otpVal;
    }

    public String getEnrollApplicationInitiatedBy() {
        return enrollApplicationInitiatedBy;
    }

    public void setEnrollApplicationInitiatedBy(String enrollApplicationInitiatedBy) {
        this.enrollApplicationInitiatedBy = enrollApplicationInitiatedBy;
    }

    public String getEnrollAgentId() {
        return enrollAgentId;
    }

    public void setEnrollAgentId(String enrollAgentId) {
        this.enrollAgentId = enrollAgentId;
    }
    
    
}
