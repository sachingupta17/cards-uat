/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.beans;

import java.util.Date;

/**
 *
 * @author parvati
 */
public class CreditScoreBean {

    private String fname;
    private String lname;
    private String mname;
    private String mobileNo;
    private String emailId;
    private String JPnumber;
    private String fullName;
    private String termsAndConditions;
    private String formNumber;
    private String otpTransactionId;
    private String token;
    private String pendingAttempt;
    private String creditScore;
    private String creditScoreLevel;
    private String emailJPID;
    private String mobileNumberJP;
    private String reportNumber;
    private String reportDate;
    private String mergeJP;
    private String city;
    private String status;
    private String tier;
    private String promocode;
    private String dob;
    private String Gender;
    private String mobileVerified;
    private String emailVerified;
    private String creditTAndC;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getJPnumber() {
        return JPnumber;
    }

    public void setJPnumber(String JPnumber) {
        this.JPnumber = JPnumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public String getFormNumber() {
        return formNumber;
    }

    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    public String getOtpTransactionId() {
        return otpTransactionId;
    }

    public void setOtpTransactionId(String otpTransactionId) {
        this.otpTransactionId = otpTransactionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPendingAttempt() {
        return pendingAttempt;
    }

    public void setPendingAttempt(String pendingAttempt) {
        this.pendingAttempt = pendingAttempt;
    }

    public String getCreditScoreLevel() {
        return creditScoreLevel;
    }

    public void setCreditScoreLevel(String creditScoreLevel) {
        this.creditScoreLevel = creditScoreLevel;
    }

    public String getEmailJPID() {
        return emailJPID;
    }

    public void setEmailJPID(String emailJPID) {
        this.emailJPID = emailJPID;
    }

    public String getMobileNumberJP() {
        return mobileNumberJP;
    }

    public void setMobileNumberJP(String mobileNumberJP) {
        this.mobileNumberJP = mobileNumberJP;
    }

    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getMergeJP() {
        return mergeJP;
    }

    public void setMergeJP(String mergeJP) {
        this.mergeJP = mergeJP;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getCreditScore() {
        return creditScore;
    }

    public void setCreditScore(String creditScore) {
        this.creditScore = creditScore;
    }

    public String getMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(String mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    public String getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(String emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getCreditTAndC() {
        return creditTAndC;
    }

    public void setCreditTAndC(String creditTAndC) {
        this.creditTAndC = creditTAndC;
    }

}
