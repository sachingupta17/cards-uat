package com.cbc.portal.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JsonBean {

    private String firstName;
    private String middleName;
    private String lastName;
    private String email;
    private String mobile;
    private String dateOfBirth;
    private String gender;
    private String tier;
    private String jpnumber;
    private String eduQualification;
    private String aadharNumber;
    private String pancard;
    private String monthlyIncome;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String state;
    private String pinCode;
    private String peraddresssameascurr;
    private String permaddress1;
    private String permaddress2;
    private String permaddress3;
    private String permCity;
    private String permState;
    private String permPinCode;
    private String employmentType;
    private String companyName;
    private String officeAddress1;
    private String officeAddress2;
    private String officeAddress3;
    private String officeCity;
    private String officeState;
    private String officePincode;
    private String phone;
    private String std;
    private String officeStd;
    private String officePhone;
    private String bankName;
    private String appliedcardName;
    private String pcnNumber;
    private String applicationStatus;
    private String applicationDate;
    private String reason;
    private String decisionStatus;
    private String decisionDate;
    private String bankPartnerURL;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getJpnumber() {
        return jpnumber;
    }

    public void setJpnumber(String jpnumber) {
        this.jpnumber = jpnumber;
    }

    public String getEduQualification() {
        return eduQualification;
    }

    public void setEduQualification(String eduQualification) {
        this.eduQualification = eduQualification;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getPancard() {
        return pancard;
    }

    public void setPancard(String pancard) {
        this.pancard = pancard;
    }

    public String getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(String monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getPeraddresssameascurr() {
        return peraddresssameascurr;
    }

    public void setPeraddresssameascurr(String peraddresssameascurr) {
        this.peraddresssameascurr = peraddresssameascurr;
    }

    public String getPermaddress1() {
        return permaddress1;
    }

    public void setPermaddress1(String permaddress1) {
        this.permaddress1 = permaddress1;
    }

    public String getPermaddress2() {
        return permaddress2;
    }

    public void setPermaddress2(String permaddress2) {
        this.permaddress2 = permaddress2;
    }

    public String getPermaddress3() {
        return permaddress3;
    }

    public void setPermaddress3(String permaddress3) {
        this.permaddress3 = permaddress3;
    }

    public String getPermCity() {
        return permCity;
    }

    public void setPermCity(String permCity) {
        this.permCity = permCity;
    }

    public String getPermState() {
        return permState;
    }

    public void setPermState(String permState) {
        this.permState = permState;
    }

    public String getPermPinCode() {
        return permPinCode;
    }

    public void setPermPinCode(String permPinCode) {
        this.permPinCode = permPinCode;
    }

    public String getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(String employmentType) {
        this.employmentType = employmentType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOfficeAddress1() {
        return officeAddress1;
    }

    public void setOfficeAddress1(String officeAddress1) {
        this.officeAddress1 = officeAddress1;
    }

    public String getOfficeAddress2() {
        return officeAddress2;
    }

    public void setOfficeAddress2(String officeAddress2) {
        this.officeAddress2 = officeAddress2;
    }

    public String getOfficeAddress3() {
        return officeAddress3;
    }

    public void setOfficeAddress3(String officeAddress3) {
        this.officeAddress3 = officeAddress3;
    }

    public String getOfficeCity() {
        return officeCity;
    }

    public void setOfficeCity(String officeCity) {
        this.officeCity = officeCity;
    }

    public String getOfficeState() {
        return officeState;
    }

    public void setOfficeState(String officeState) {
        this.officeState = officeState;
    }

    public String getOfficePincode() {
        return officePincode;
    }

    public void setOfficePincode(String officePincode) {
        this.officePincode = officePincode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStd() {
        return std;
    }

    public void setStd(String std) {
        this.std = std;
    }

    public String getOfficeStd() {
        return officeStd;
    }

    public void setOfficeStd(String officeStd) {
        this.officeStd = officeStd;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAppliedcardName() {
        return appliedcardName;
    }

    public void setAppliedcardName(String appliedcardName) {
        this.appliedcardName = appliedcardName;
    }

    public String getPcnNumber() {
        return pcnNumber;
    }

    public void setPcnNumber(String pcnNumber) {
        this.pcnNumber = pcnNumber;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDecisionStatus() {
        return decisionStatus;
    }

    public void setDecisionStatus(String decisionStatus) {
        this.decisionStatus = decisionStatus;
    }

    public String getDecisionDate() {
        return decisionDate;
    }

    public void setDecisionDate(String decisionDate) {
        this.decisionDate = decisionDate;
    }

    public String getBankPartnerURL() {
        return bankPartnerURL;
    }

    public void setBankPartnerURL(String bankPartnerURL) {
        this.bankPartnerURL = bankPartnerURL;
    }

}
