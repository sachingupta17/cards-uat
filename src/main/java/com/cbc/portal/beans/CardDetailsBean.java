package com.cbc.portal.beans;

import java.util.ArrayList;
import java.util.List;

public class CardDetailsBean {

    private Integer cpNo;
    private Integer bpNo;
    private Integer gmNo;
    private String cardName;
    private String imageText;
    private String cardInfo;
    private String cardUrl;
    private String additionalCardInfo;
    private String partnerInfo;
    private String termsAndCondition;
    private String jpMiles;
    private String perspendonJp;
    private String fees;
    private String ticketMessage;
    private String lyfPrefMessage;
    private List<Integer> lsbpList;
    private int lsbpNo;
    private String bankName;
    private String lsbpImageText;
    private String imageAsIconText;
    private String welcomeBonus;
    private String perSpendsOnJpMiles;
    private List<String> featureList = new ArrayList<String>();
    private String cityName;
    private String dob;
    private Integer flightRange;
    private String monthlySpend;
    private String annualIncome;
    private String[] lsbpArray;
    private int age;
    private String homePageTitle;
    private String description;
    private String approvalMessage;
    private String title;
    private String fname;
    private String mname;
    private String lname;
    private String email;
    private String mobile;
    private String jpNumber;
    private String jpTier;
    private String gender;
    private String bankUrl;
    private String bankUrlName;
    private String freeTickets;
    private int lyfList;
    private String networkName;
    private String bankImagePath;
    private String cardImagePath;
    private String lsbpImagePath;
    private String lsbpImageIconPath;
    private String lsbpImageIconBluePath;
    private String offerId;
    private String offerDesc;
    private String utmSource;
    private String utmMedium;
    private String utmCampaign;
    private String milesDisplayMessage;
    private Integer freeFlightsMessageFlag;
    private String freeFlightsMessage;
    private Integer compareFlag;
    private String offerTitle;
    private String offerContent;
    private String country;
    private String category;
    private String PID;
    private Integer cardDisplayorder;
    private String messageContent;
    private String bpSatatuTitle;
     private Integer applyflag;
     private String defaultPromocode;

   
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getJpNumber() {
        return jpNumber;
    }

    public void setJpNumber(String jpNumber) {
        this.jpNumber = jpNumber;
    }
    private List<String> cardFeature = new ArrayList<String>();
    private List<String> groupheaders = new ArrayList<String>();

    public List<String> getGroupheaders() {
        return groupheaders;
    }

    public void setGroupheaders(List<String> groupheaders) {
        this.groupheaders = groupheaders;
    }

    public Integer getCpNo() {
        return cpNo;
    }

    public void setCpNo(Integer cpNo) {
        this.cpNo = cpNo;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardInfo() {
        return cardInfo;
    }

    public void setCardInfo(String cardInfo) {
        this.cardInfo = cardInfo;
    }

    public String getCardUrl() {
        return cardUrl;
    }

    public void setCardUrl(String cardUrl) {
        this.cardUrl = cardUrl;
    }

    public String getAdditionalCardInfo() {
        return additionalCardInfo;
    }

    public void setAdditionalCardInfo(String additionalCardInfo) {
        this.additionalCardInfo = additionalCardInfo;
    }

    public String getPartnerInfo() {
        return partnerInfo;
    }

    public void setPartnerInfo(String partnerInfo) {
        this.partnerInfo = partnerInfo;
    }

    public String getTermsAndCondition() {
        return termsAndCondition;
    }

    public void setTermsAndCondition(String termsAndCondition) {
        this.termsAndCondition = termsAndCondition;
    }

    public String getImageText() {
        return imageText;
    }

    public void setImageText(String imageText) {
        this.imageText = imageText;
    }

    public String getJpMiles() {
        return jpMiles;
    }

    public void setJpMiles(String jpMiles) {
        this.jpMiles = jpMiles;
    }

    public String getPerspendonJp() {
        return perspendonJp;
    }

    public void setPerspendonJp(String perspendonJp) {
        this.perspendonJp = perspendonJp;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getTicketMessage() {
        return ticketMessage;
    }

    public void setTicketMessage(String ticketMessage) {
        this.ticketMessage = ticketMessage;
    }

    public String getLyfPrefMessage() {
        return lyfPrefMessage;
    }

    public void setLyfPrefMessage(String lyfPrefMessage) {
        this.lyfPrefMessage = lyfPrefMessage;
    }

    public List<Integer> getLsbpList() {
        return lsbpList;
    }

    public void setLsbpList(List<Integer> lsbpList) {
        this.lsbpList = lsbpList;
    }

    public Integer getBpNo() {
        return bpNo;
    }

    public void setBpNo(Integer bpNo) {
        this.bpNo = bpNo;
    }

    public List<String> getCardFeature() {
        return cardFeature;
    }

    public void setCardFeature(List<String> cardFeature) {
        this.cardFeature = cardFeature;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getFlightRange() {
        return flightRange;
    }

    public void setFlightRange(Integer flightRange) {
        this.flightRange = flightRange;
    }

    public String getMonthlySpend() {
        return monthlySpend;
    }

    public void setMonthlySpend(String monthlySpend) {
        this.monthlySpend = monthlySpend;
    }

    public String getAnnualIncome() {
        return annualIncome;
    }

    public void setAnnualIncome(String annualIncome) {
        this.annualIncome = annualIncome;
    }

    public String[] getLsbpArray() {
        return lsbpArray;
    }

    public void setLsbpArray(String[] lsbpArray) {
        this.lsbpArray = lsbpArray;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHomePageTitle() {
        return homePageTitle;
    }

    public void setHomePageTitle(String homePageTitle) {
        this.homePageTitle = homePageTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLsbpNo() {
        return lsbpNo;
    }

    public void setLsbpNo(int lsbpNo) {
        this.lsbpNo = lsbpNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getLsbpImageText() {
        return lsbpImageText;
    }

    public void setLsbpImageText(String lsbpImageText) {
        this.lsbpImageText = lsbpImageText;
    }

    public String getImageAsIconText() {
        return imageAsIconText;
    }

    public void setImageAsIconText(String imageAsIconText) {
        this.imageAsIconText = imageAsIconText;
    }

    public String getWelcomeBonus() {
        return welcomeBonus;
    }

    public void setWelcomeBonus(String welcomeBonus) {
        this.welcomeBonus = welcomeBonus;
    }

    public String getPerSpendsOnJpMiles() {
        return perSpendsOnJpMiles;
    }

    public void setPerSpendsOnJpMiles(String perSpendsOnJpMiles) {
        this.perSpendsOnJpMiles = perSpendsOnJpMiles;
    }

    public List<String> getFeatureList() {
        return featureList;
    }

    public void setFeatureList(List<String> featureList) {
        this.featureList = featureList;
    }

    public String getApprovalMessage() {
        return approvalMessage;
    }

    public void setApprovalMessage(String approvalMessage) {
        this.approvalMessage = approvalMessage;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getJpTier() {
        return jpTier;
    }

    public void setJpTier(String jpTier) {
        this.jpTier = jpTier;
    }

    public String getBankUrl() {
        return bankUrl;
    }

    public void setBankUrl(String bankUrl) {
        this.bankUrl = bankUrl;
    }

    public String getBankUrlName() {
        return bankUrlName;
    }

    public void setBankUrlName(String bankUrlName) {
        this.bankUrlName = bankUrlName;
    }

    public String getFreeTickets() {
        return freeTickets;
    }

    public void setFreeTickets(String freeTickets) {
        this.freeTickets = freeTickets;
    }

    public Integer getGmNo() {
        return gmNo;
    }

    public void setGmNo(Integer gmNo) {
        this.gmNo = gmNo;
    }

    public int getLyfList() {
        return lyfList;
    }

    public void setLyfList(int lyfList) {
        this.lyfList = lyfList;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public String getBankImagePath() {
        return bankImagePath;
    }

    public void setBankImagePath(String bankImagePath) {
        this.bankImagePath = bankImagePath;
    }

    public String getCardImagePath() {
        return cardImagePath;
    }

    public void setCardImagePath(String cardImagePath) {
        this.cardImagePath = cardImagePath;
    }

    public String getLsbpImagePath() {
        return lsbpImagePath;
    }

    public void setLsbpImagePath(String lsbpImagePath) {
        this.lsbpImagePath = lsbpImagePath;
    }

    public String getLsbpImageIconPath() {
        return lsbpImageIconPath;
    }

    public void setLsbpImageIconPath(String lsbpImageIconPath) {
        this.lsbpImageIconPath = lsbpImageIconPath;
    }

    public String getLsbpImageIconBluePath() {
        return lsbpImageIconBluePath;
    }

    public void setLsbpImageIconBluePath(String lsbpImageIconBluePath) {
        this.lsbpImageIconBluePath = lsbpImageIconBluePath;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getOfferDesc() {
        return offerDesc;
    }

    public void setOfferDesc(String offerDesc) {
        this.offerDesc = offerDesc;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    public String getMilesDisplayMessage() {
        return milesDisplayMessage;
    }

    public void setMilesDisplayMessage(String milesDisplayMessage) {
        this.milesDisplayMessage = milesDisplayMessage;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getOfferContent() {
        return offerContent;
    }

    public void setOfferContent(String offerContent) {
        this.offerContent = offerContent;
    }

    public Integer getFreeFlightsMessageFlag() {
        return freeFlightsMessageFlag;
    }

    public void setFreeFlightsMessageFlag(Integer freeFlightsMessageFlag) {
        this.freeFlightsMessageFlag = freeFlightsMessageFlag;
    }

    public String getFreeFlightsMessage() {
        return freeFlightsMessage;
    }

    public void setFreeFlightsMessage(String freeFlightsMessage) {
        this.freeFlightsMessage = freeFlightsMessage;
    }

    public Integer getCompareFlag() {
        return compareFlag;
    }

    public void setCompareFlag(Integer compareFlag) {
        this.compareFlag = compareFlag;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPID() {
        return PID;
    }

    public void setPID(String PID) {
        this.PID = PID;
    }

    public Integer getCardDisplayorder() {
        return cardDisplayorder;
    }

    public void setCardDisplayorder(Integer cardDisplayorder) {
        this.cardDisplayorder = cardDisplayorder;
    }
     //new cr2
    
    public Integer getApplyflag() {
        return applyflag;
    }

    public void setApplyflag(Integer applyflag) {
        this.applyflag = applyflag;
    }
      public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getBpSatatuTitle() {
        return bpSatatuTitle;
    }

    public void setBpSatatuTitle(String bpSatatuTitle) {
        this.bpSatatuTitle = bpSatatuTitle;
    }
    
     public String getDefaultPromocode() {
        return defaultPromocode;
    }

    public void setDefaultPromocode(String defaultPromocode) {
        this.defaultPromocode = defaultPromocode;
    }
     
}
