package com.cbc.portal.constants;

public class CcHibernateQueries {

    public static final String CC_GET_BANK_LIST = "select ccbp from CcBankPartner ccbp where ccbp.status=1 and ccbp.bpStatus=1 order by ccbp.bankName";

    public static final String CC_GET_LYFSTYLBENEFITPREF = "select lsbp from CcLifestyleBenefitPref lsbp where lsbp.status=1 and lsbp.lsbpStatus=1 order by lsbpName";

//	public static final String CC_GET_ALL_CARDS ="select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardStatus=1 order by ccp.cardPosition";
    public static final String CC_GET_ALL_CARDS = "select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardStatus=1 and ccp.ccCountryMaster=1 and ccp.ccCardCatagory.ccCatagoryId=1 order by ccp.cardPosition ";
    
    
    public static final String CC_GET_ALLTYPE_CARDS = "select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardStatus=1 and ccp.ccCountryMaster=1 order by ccp.cardPosition ";
    
    public static final String CC_GET_ALL_TYPE_CARDS = "select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardStatus=1 and ccp.ccCountryMaster=1  order by ccp.cardPosition ";

    public static final String CC_GET_CARDS_BY_BANK = "select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardStatus=1 and ccp.ccBankPartner.bpNo=:bpNo order by ccp.cardPosition";

    public static final String CC_GET_CARDS_BY_FEES = "select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardStatus=1 and ccp.fees=:fees order by ccp.cardPosition";

    public static final String CC_GET_CARDS_BY_LYFPREF = "select cclbp from CcLifestyleBenefitPrefValues cclbp where cclbp.status=1 and cclbp.lsbpStatus=1 and cclbp.ccLifestyleBenefitPref.lsbpNo=:lsbpNo";

    public static final String CC_GET_CITY_LIST = "select ccm.city from CcCityMaster ccm where ccm.status=1 and ccm.cmNo not in (1,2,3,4,5) order by city";

    public static final String CC_GET_CITY_ID_BY_NAME = "select ccm.cmNo from CcCityMaster ccm where ccm.status=1 and ccm.city=:cityName";

    public static final String CC_GET_BANK_ID_BY_CITY = "select ccl.ccBankPartner.bpNo from CcLocationCriteria ccl where ccl.status=1 and ccl.ccCityMaster.city=:cityName";

    public static final String CC_ENUM_VALUES = "select iev from CcEnumValues iev where iev.typeName=:typeName";

    //public static final String CC_GET_CITIES = "select distinct cm.city from CcCityMapping cm where cm.status=1";
    //public static final String CC_GET_STATES = "select distinct cm.state from CcCityMapping cm where cm.status=1";
    public static final String CC_BANK_UPGRADE_MASSAGE = "select iev.upgradeMessage from CcBankPartner iev where iev.bpNo=4";
    public static final String CC_BANK_UPGRADE_URL = "select iev.upgradeURL from CcBankPartner iev where iev.bpNo=4";
//        public static final String CC_GET_PINCODE = "select distinct cm.pincode from CcCityMapping cm where cm.status=1";

    public static final String CC_GET_CITIES = "select distinct ccpcs.city from CcPincitystateMapping ccpcs where ccpcs.status=1 and ccpcs.ccBankPartner.bpNo=1 order by ccpcs.city";
//	 public static final String CC_GET_CITIES = "select distinct ccpcs.city from CcCityMapping ccpcs where ccpcs.status=1 order by ccpcs.city";
    public static final String CC_GET_ICICI_CITIES = "select distinct ccpcs.city from CcPincitystateMapping ccpcs where ccpcs.status=1 and ccpcs.ccBankPartner.bpNo=2 order by ccpcs.city";

    public static final String CC_GET_STATES = "select distinct ccpcs.state from CcPincitystateMapping ccpcs where ccpcs.status=1 and ccpcs.ccBankPartner.bpNo=1 order by ccpcs.state";//
//	public static final String CC_GET_STATES = "select distinct ccpcs.state from CcCityMapping ccpcs where ccpcs.status=1  order by ccpcs.state";//

    public static final String CC_GET_ICICI_STATES = "select distinct ccpcs.state from CcPincitystateMapping ccpcs where ccpcs.status=1 and ccpcs.ccBankPartner.bpNo=2 order by ccpcs.state";

    public static final String CC_GET_CITY_MAPPINGS_BY_PIN = "select new com.cbc.portal.beans.CityMappingBean(cm.pincode,cm.city,cm.state) from CcCityMapping cm where cm.status=1 and cm.pincode=:pinCode";

    public static final String CC_GET_ICICI_CITY_MAPPINGS_BY_PIN = "select new com.cbc.portal.beans.CityMappingBean(cm.pincode,cm.city,cm.state) from CcPincitystateMapping cm where cm.status=1 and cm.pincode=:pinCode and cm.ccBankPartner.bpNo=2";

    public static final String CC_GET_CITY_MAPPINGS_BY_STATE = "select distinct ccpcs.city from CcPincitystateMapping ccpcs where ccpcs.state=:state and ccpcs.status=1 and ccpcs.ccBankPartner.bpNo=1";
//	public static final String CC_GET_CITY_MAPPINGS_BY_STATE ="select distinct ccpcs.city from CcCityMapping ccpcs where ccpcs.state=:state and ccpcs.status=1";

    public static final String CC_GET_ICICI_CITY_MAPPINGS_BY_STATE = "select distinct ccpcs.city from CcPincitystateMapping ccpcs where ccpcs.state=:state and ccpcs.status=1 and ccpcs.ccBankPartner.bpNo=2";

    public static final String CC_GET_STATE_MAPPINGS_BY_CITY = "select distinct ccpcs.state from CcPincitystateMapping ccpcs where ccpcs.city=:city and ccpcs.status=1 and ccpcs.ccBankPartner.bpNo=1";
//	 public static final String CC_GET_STATE_MAPPINGS_BY_CITY ="select distinct ccpcs.state from  CcCityMapping ccpcs where ccpcs.city=:city and ccpcs.status=1 ";
    public static final String CC_GET_ICICI_STATE_MAPPINGS_BY_CITY = "select distinct ccpcs.state from CcPincitystateMapping ccpcs where ccpcs.city=:city and ccpcs.status=1 and ccpcs.ccBankPartner.bpNo=2";

    public static final String CC_GET_APPLICATION_PCN = "select ap from CcAmexApplicationStatus ap where ap.pcnNumber=:pcnNumber and ap.status=1";

    public static final String CC_GET_APPLICATION_JP = "select ap from CcAmexApplicationStatus ap where ap.jpNumber=:jpNumber and ap.status=1";

    public static final String CC_GET_ICICI_APPLICATION_JP = "select ap from CcIciciApplicationStatus ap where ap.jpNumber=:jpNumber and ap.status=1";

    public static final String CC_GET_BANK_APPLICATION_JP = "select ap from CcBankApplication ap where ap.jpnumber=:jpNumber and ap.status=1";

    public static final String CC_GET_GROUPHEADERS_FOR_LISTING = "select gp from CcPageGroupLink gp where gp.status=1 and gp.ccGroupMaster.gmStatus=1 and gp.ccPageMaster.pmNo=:id order by gp.groupDisplayOrder";

    public static final String CC_GET_ACTIVE_BANNER = "select ban from CcBannerManagement ban where ban.bmStatus=1 and ban.status=1 order by ban.position";

    public static final String CC_GET_CARD_FEATURES = "select cf from CcCardFeatures cf where cf.ccCardProduct.cpNo=:cpNo order by cf.ccGroupMaster.gmNo";

    public static final String CC_GET_CARD_FEATURES_BY_GROUP = "select cf from CcCardFeatures cf where cf.ccGroupMaster.gmNo=:groupNo and cf.status=1 and cf.cfStatus=1";

    public static final String CC_GET_ELIGIBILITY_CARDS = "select distinct ccec.ccCardProduct.cardName from CcEligibilityCriteria ccec where ccec.status=1 and ccec.ccBankPartner.bpNo IN(:bpNo) "
            + "  and ccec.ccCardProduct.cardName IN(:cardName) and ccec.salariedIncomePerMonth <=:monthlyEndPrice and :age>=ccec.ageAbove and (:age<=ccec.ageBelow or ccec.ageBelow is null)";

    public static final String CC_GET_ELIGIBILITY_CARDS_ = "select distinct ccec.ccCardProduct.cardName from CcEligibilityCriteria ccec where ccec.status=1 and ccec.ccBankPartner.bpNo IN(:bpNo) "
            + "  and ccec.ccCardProduct.cardName IN(:cardName) and :age>=ccec.ageAbove and (:age<=ccec.ageBelow or ccec.ageBelow is null)";

    public static final String CC_GET_CARD_NO_BY_NAME = "select ccp.cpNo from CcCardProduct ccp where ccp.status=1 and ccp.ccBankPartner.bpNo IN(:bpNo) and ccp.cardName IN(:cardName)";

    public static final String CC_GET_CARDS_BY_PREF = "select distinct cclbp.ccCardProduct.cardName from CcLifestyleBenefitPrefValues cclbp where cclbp.status=1 and cclbp.ccLifestyleBenefitPref.lsbpNo IN(:lsbpNo)";

    public static final String CC_GET_LYF_PREF_NAME = "select distinct cclbp.ccLifestyleBenefitPref.imageText from CcLifestyleBenefitPrefValues cclbp where cclbp.ccCardProduct.cpNo=:cpNo and cclbp.status=1 and cclbp.ccLifestyleBenefitPref.lsbpNo IN(:lsbpNo)";

    public static final String CC_GET_ALL_CARD_FEATURES = "select cf.cfDescription from CcCardFeatures cf where cf.status=1 and cf.cfStatus=1 and cf.ccGroupMaster.groupDescription=:list order by cf.cfDisplayOrder";

    public static final String CC_GET_PAGE_CONTENT = "select pc from CcPageContent pc where pc.status=1";

    public static final String CC_GET_LSBP_NO = "select distinct cclbp.ccLifestyleBenefitPref.lsbpNo from CcLifestyleBenefitPrefValues cclbp where cclbp.status=1 and cclbp.ccCardProduct.cpNo=:cpNo";

    public static final String CC_GET_CARD_FEATURES_BY_ROW = "select cf from CcCardFeatures cf where cf.ccGroupMaster.groupDescription=:groupNo and cf.cfDescription=:featureName and cf.ccCardProduct.cpNo=:cpNo and cf.status=1 and cf.cfStatus=1";

    public static final String CC_GET_CARD_FEATURES_BY_CP_GM_NO = "select cf from CcCardFeatures cf where cf.ccGroupMaster.gmNo=:groupNo and cf.ccCardProduct.cpNo=:cpNo and cf.status=1 and cf.cfStatus=1";

    public static final String CC_GET_LSBP_BY_CPNO = "select lsbp from CcLifestyleBenefitPrefValues lsbp where lsbp.ccCardProduct.cpNo=:cpNo and lsbp.status=1";

    public static final String CC_GET_CARD_FEATURES_FOR_DETAILS = "select cf from CcCardFeatures cf where cf.ccGroupMaster.gmNo=:gmNo and cf.ccCardProduct.cpNo=:cpNo and cf.status=1 and cf.cfStatus=1 order by cf.cfDisplayOrder";

    public static final String CC_GET_CITY_NAMES = "select cm.city from CcCityMaster cm where cm.city LIKE CONCAT(:cityName,'%') and cm.status=1";

    /*public static final String CC_GET_CARD_FEATURES_BY_GM_NO = "select cf.cfDescription from CcCardFeatures cf where cf.ccGroupMaster.gmNo=:groupNo and cf.status=1 and cf.cfStatus=1";*/
    public static final String CC_GET_CARD_ID_BY_CARD_NAME = "select ccp.cpNo from CcCardProduct ccp where ccp.cpImageText=:cardName and ccp.status=1";

    public static final String CC_GET_CARD_STATUS_BY_CARD_NAME = "select ccp.status from CcCardProduct ccp where ccp.cpNo=:cpNo";

    public static final String CC_IS_PINCODE_EXCLUDED = "select cclc from CcLocationCriteria cclc where cclc.status=1 and cclc.pincode=:pinCode and cclc.ccBankPartner.bpNo=:bpNo";

    public static final String CC_ICICI_IS_PINCODE_EXCLUDED = "select cclc from CcPincitystateMapping cclc where cclc.status=1 and cclc.pincode=:pinCode and cclc.ccBankPartner.bpNo=:bpNo";

    public static final String CC_GET_BE_AMEX_FORM = "select bcaf from CcBeApplicationFormDetails bcaf where bcaf.randomNumber=:randomNumber and bcaf.status=1";

    public static final String CC_FORM_NUMBER_EXIST = "select bcaf from CcBeApplicationFormDetails bcaf where bcaf.formNumber=:formNumber and bcaf.status=1 ORDER BY createdTime DESC";

    public static final String CC_GET_OFFER_BY_JPNUM = "select ccmo from CcMapOffers ccmo where ccmo.status=1 and ccmo.jpNumber=:jpNumber and ccmo.ccOffers.ccBankPartner.bpNo=:bpNo and ccmo.ccOffers.offersStatus=1 and ccmo.startDate<=:deactivationDate and  (ccmo.endDate>=:deactivationDate or ccmo.endDate is null)  and (ccmo.ccOffers.deactivationDate>=:deactivationDate or ccmo.ccOffers.deactivationDate is null)";

    public static final String CC_GET_DEFAULT_OFFER = "select cco from CcOffers cco where cco.status=1 and cco.offerTitle='Default Offer' and cco.ccBankPartner.bpNo=:bpNo";

    public static final String CC_GET_ENROLL_ERROR_MESSAGE = "select cceem.eemMessage from CcEnrollErrorMessage cceem where cceem.eemCode=:code and cceem.status=1";

    public static final String CC_GET_ENROLL_MEDIA_CODE = "select ccbp.ccMasterMediaCode.mediaCodeName from CcBankPartner ccbp where ccbp.bpStatus=1 and ccbp.bpNo=:bpNo";

    public static final String CC_UPDATE_UNICA_FLAG = "update CcBeApplicationFormDetails befd set befd.unicaFlag = 'Y' where befd.status=1 and befd.formStatus = 'Pending' and befd.unicaFlag=:unicaFlag and adddate(DATE_FORMAT(befd.createdTime,'%Y-%m-%d'), :dateInterval)  = CURDATE()";

    public static final String CC_GET_UNICA_FLAG_DATA = "select befd from CcBeApplicationFormDetails befd where befd.unicaFlag = 'N' and befd.status=1 and befd.formStatus = 'Pending' ";

    public static final String CC_GET_AMEX_APP_STATUS = "select aas.formStatus from CcBeApplicationFormDetails aas where aas.jetpriviligemembershipNumber=:jetpriviligemembershipNumber and aas.formStatus= 'Pending'  order by aas.createdTime DESC ";

    public static final String CC_GET_AMEX_APP_RANDOM = "select aas.randomNumber from CcBeApplicationFormDetails aas where aas.jetpriviligemembershipNumber=:jetpriviligemembershipNumber and aas.formStatus= 'Pending'  order by aas.createdTime DESC ";

    public static final String CC_ENUM_SELECTED_VALUES = "select iev.value from CcEnumValues iev where typeName='ICICI_REALATION_STATUS' and  intCode <>1";

    public static final String CC_GET_CARDTYPE = "select  ccc from CcCardCatagory ccc where ccc.status=1";

    public static final String CC_GET_COUNTRY = "select  cmm from CcCountryMaster cmm where cmm.status=1 and ccCountryId IN (1,3)";

//           public static final String CC_GET_SELECTED_CARDS_CATEGORY ="select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardCatagory=2 and ccp.ccCardStatus=1 order by ccp.cardPosition";
//           public static final String CC_GET_SELECTED_CORPORATE_CATEGORY ="select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardCatagory=3 and ccp.ccCardStatus=1 order by ccp.cardPosition";
    public static final String CC_GET_BANK_LIST_CRED_IND = "select distinct ccbp.ccBankPartner from CcCardProduct ccbp where ccbp.status=1 and ccbp.ccCardCatagory.ccCatagoryId=:category and ccbp.ccCountryMaster.ccCountryId=:country and ccbp.ccCardStatus=1";
    
    public static final String CC_GET_BANK_LIST_CRED_ALL = "select distinct ccbp.ccBankPartner from CcCardProduct ccbp where ccbp.status=1 and ccbp.ccCountryMaster.ccCountryId=:country and ccbp.ccCardStatus=1";

    public static final String CC_GET_BANK_LIST_DEB = "select distinct ccbp.ccBankPartner from CcCardProduct ccbp where ccbp.status=1 and ccbp.ccCardCatagory.ccCatagoryId=:category";

//              public static final String CC_GET_SELECTED_CARDS ="select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCountryMaster=3 and ccp.ccCardStatus=1 order by ccp.cardPosition";
    public static final String CC_GET_SELECTED_CARDS = "select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCountryMaster=3 and ccp.ccCardStatus=1 and ccp.ccCardCatagory.ccCatagoryId=1 order by ccp.cardPosition";
//          public static final String CC_GET_SELECTED_OTHER_CATEGORY ="select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardCatagory=4 and ccp.ccCardStatus=1 and ccp.ccCountryMaster.ccCountryId=:countryid order by ccp.cardPosition";

    public static final String CC_GET_SELECTED_CARDS_CATEGORY = "select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardCatagory=2 and ccp.ccCardStatus=1 and ccp.ccCountryMaster.ccCountryId=:countryid order by ccp.cardPosition";

    public static final String CC_GET_SELECTED_CORPORATE_CATEGORY = "select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardCatagory=3 and ccp.ccCardStatus=1 and ccp.ccCountryMaster.ccCountryId=:countryid order by ccp.cardPosition";

    public static final String CC_GET_SELECTED_OTHER_CATEGORY = "select ccp from CcCardProduct ccp where ccp.status=1 and ccp.ccCardCatagory=4 and ccp.ccCardStatus=1 and ccp.ccCountryMaster.ccCountryId=:countryid order by ccp.cardPosition";

    public static final String CC_GET_PINCODE = "SELECT DISTINCT pp.pincode FROM CcPincitystateMapping pp WHERE pp.ccBankPartner=2 and pp.status=1";

    public static final String CC_GET_COMPANY_NAMES = "select cn.companyname from CcCompanyList cn where status=1";
    public static final String CC_GET_EMPLOYMENT_STATUS_SALARIED = "select es.salariedIncomePerMonth  from CcEligibilityCriteria es where es.ccBankPartner.bpNo=:bpno and es.ccCardProduct.cpNo=:cpno";
    public static final String CC_GET_EMPLOYMENT_STATUS_SELFEMP = "select es.selfEmployedIncomePerMonth  from CcEligibilityCriteria es where es.ccBankPartner.bpNo=:bpno and es.ccCardProduct.cpNo=:cpno";
    public static final String CC_GET_ICICI_AGE_CRITERIA_MAPPED_CITY = "select ac.city from IciciAgeCriteria ac where ac.ccBankPartner.bpNo=:bpno and ac.ccCardProduct.cpNo=:cpno and ac.emptype=:emptype and ac.status=1 and ac.city is not null";
    public static final String CC_GET_ICICI_AGE_CRITERIA_AGEABOVE_BY_CITY = "select ac.ageabove from IciciAgeCriteria ac where ac.ccBankPartner.bpNo=:bpno and ac.ccCardProduct.cpNo=:cpno and ac.emptype=:emptype and ac.city=:city and ac.status=1";
    public static final String CC_GET_ICICI_AGE_CRITERIA_AGEBELOW_BY_CITY = "select ac.agebelow from IciciAgeCriteria ac where ac.ccBankPartner.bpNo=:bpno and ac.ccCardProduct.cpNo=:cpno and ac.emptype=:emptype and ac.city=:city and ac.status=1";

    public static final String CC_GET_COMPANY_NAMES_BY_LETTER = "select cn.companyname from CcCompanyList cn where cn.companyname like :company and cn.status=1";

    public static final String CC_GET_ICICI_AGE_CRITERIA_AGEBELOW = "select ac.agebelow from IciciAgeCriteria ac where ac.ccBankPartner.bpNo=:bpno and ac.ccCardProduct.cpNo=:cpno and ac.emptype=:emptype and ac.status=1";//cr2
    public static final String CC_GET_ICICI_AGE_CRITERIA_AGEABOVE = "select ac.ageabove from IciciAgeCriteria ac where ac.ccBankPartner.bpNo=:bpno and ac.ccCardProduct.cpNo=:cpno and ac.emptype=:emptype and ac.status=1";//cr2

    public static final String CC_FORM_NUMBER_EXIST_NEW = "select bcaf.formNumber from CcBeApplicationFormDetails bcaf where bcaf.formNumber=:formNumber and bcaf.status=1";

    //new cr2 
    public static final String CC_GET_BANKWISE_RESPONSE = "select br from CcBankwiseResponseMapping br where br.ccBankPartner.bpNo=:bpNo and br.ccBankwiseStatus.bsno=:BpStatus and br.status=1 and br.statusType=1";

    public static final String CC_GET_BANKSTATUS_ID_BY_STATUS = "select ccp.bsno from CcBankwiseStatus ccp where ccp.statusDesc=:bsStatus and ccp.status=1";
    //cr3
    public static final String CC_GET_SMS_CONTENT = "select c.smsContent from CcSmsDetails c where c.smsName=:smsName and c.status=1 and c.csdStatus=1 and c.ccBankPartner.bpNo=:bankno ";
    public static final String CC_GET_OTP_VALID = "select o from CcOTP o where o.mobileNumber=:mobileNumber and o.ccBankPartner.bpNo=:bankNumber and o.formNUmber=:formNumber  order by date desc";

    public static final String CC_GET_IATACODE = "select cciata.cityCode from CCIATACODE cciata where cciata.cityName=:city";
    public static final String CC_GET_OTP_BY_ID = "select o from CcOTP o where o.mobileNumber=:mobileNumber and o.ccBankPartner.bpNo=:bankNumber and o.formNUmber=:formNumber and o.otpValue=:otp order by date desc";
    // sprint 52
    public static final String CC_GET_ID_FOR_INACTIVE = "select cb from CcBeApplicationFormDetails cb where mobile=:mobile and fname=:fname and formNumber=:formNumber and status=1";

    public static final String GET_PROMOCODE_ERROR = "select pcer.pcReasonMessage from PromocodeError pcer where pcer.pcReasonName=:erroCode and pcer.status=1";

    public static final String GET_ENROLLCODE_ERROR = "select ecer.eemMessage  from CcEnrollErrorMessage ecer where ecer.eemCode=:erroCode and ecer.status=1";
    public static final String CC_GET_BE_ICICI_FORM = "select bcaf from CcBeApplicationFormDetails bcaf where bcaf.randomNumber=:randomNumber and bcaf.status=1 and bcaf.bpNo = 2 and bcaf.cpNo =:cpNo";
//   sprint 56
    public static final String CC_GET_PAGE_MASTER_DETAIL = "select pmd from CcPageMaster pmd where pmd.pmNo=:id and pmd.status=1";
    public static final String CHECK_BE_RANDOM_NUMBER = "select cvu from CcCardVarientURL cvu where cvu.ccCardProduct.cpNo=:cpNo and cvu.randomNumber=:randomNumber and cvu.status=1";
    public static final String CHECK_BE_FORM_STATUS = "select cvu from CcBeApplicationFormDetails cvu where cvu.randomNumber=:randomNumber and cvu.status=1";
    public static final String GET_EXISTED_CREDIT_REPORT = "select ecr from CcCreditReport ecr  where ecr.jpNumber=:jpNum or ecr.mobileNumber=:mobile order by ecr.createdDate desc";

    public static final String GET_CREDIT_ERROR_MESSAGE = "select cer from CcCreditErrorMessage cer  where cer.errorMessage=:errorMessage or cer.reason=:reason";
    public static final String CC_GET_OTP_VALID_CREDIT = "select o from CcOTP o where o.mobileNumber=:mobileNumber and o.ccBankPartner is null and o.formNUmber=:formNumber  and o.otpTransactionId=:otpTransactionId order by date desc";

}
