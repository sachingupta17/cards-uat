package com.cbc.portal.constants;

public class CcPortalConstants {

    private CcPortalConstants() {

    }

    public static final String GENDER_0 = "Male";

    public static final String GENDER_1 = "Female";

    public static final String STATEMENT_0 = "Home";

    public static final String STATEMENT_1 = "Office";

    public static final String TITLE = "title";

    public static final String DESCRIPTION = "description";

    public static final String KEYWORD = "keyword";

    public static final String CARDNAME = "cardName";

    public static final String SUCCESS = "success";

    public static final String FAILURE = "failure";

    public static final String EMPTY = "";
}
