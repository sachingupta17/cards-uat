package com.cbc.portal.entity;
// Generated Oct 28, 2016 10:52:35 AM by Hibernate Tools 5.1.0.Alpha1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CcNetworkList generated by hbm2java
 */
@Entity
@Table(name = "CC_NETWORK_LIST", catalog = "jpplcardsdb")
public class CcNetworkList implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer nlNo;
	private String networkName;
	private Date createdTime;
	private Integer createdBy;
	private Date modifiedTime;
	private Integer modifiedBy;
	private byte status;
	private Set<CcCardFeatures> ccCardFeatureses = new HashSet<CcCardFeatures>(0);
	private Set<CcCardProduct> ccCardProducts = new HashSet<CcCardProduct>(0);

	public CcNetworkList() {
	}

	public CcNetworkList(String networkName, byte status) {
		this.networkName = networkName;
		this.status = status;
	}

	public CcNetworkList(String networkName, Date createdTime, Integer createdBy, Date modifiedTime, Integer modifiedBy,
			byte status, Set<CcCardFeatures> ccCardFeatureses, Set<CcCardProduct> ccCardProducts) {
		this.networkName = networkName;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.modifiedTime = modifiedTime;
		this.modifiedBy = modifiedBy;
		this.status = status;
		this.ccCardFeatureses = ccCardFeatureses;
		this.ccCardProducts = ccCardProducts;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "NL_NO", unique = true, nullable = false)
	public Integer getNlNo() {
		return this.nlNo;
	}

	public void setNlNo(Integer nlNo) {
		this.nlNo = nlNo;
	}

	@Column(name = "NETWORK_NAME", nullable = false, length = 20)
	public String getNetworkName() {
		return this.networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME", length = 19)
	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Column(name = "CREATED_BY")
	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_TIME", length = 19)
	public Date getModifiedTime() {
		return this.modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	@Column(name = "MODIFIED_BY")
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "STATUS", nullable = false)
	public byte getStatus() {
		return this.status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ccNetworkList")
	public Set<CcCardFeatures> getCcCardFeatureses() {
		return this.ccCardFeatureses;
	}

	public void setCcCardFeatureses(Set<CcCardFeatures> ccCardFeatureses) {
		this.ccCardFeatureses = ccCardFeatureses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ccNetworkList")
	public Set<CcCardProduct> getCcCardProducts() {
		return this.ccCardProducts;
	}

	public void setCcCardProducts(Set<CcCardProduct> ccCardProducts) {
		this.ccCardProducts = ccCardProducts;
	}

}
