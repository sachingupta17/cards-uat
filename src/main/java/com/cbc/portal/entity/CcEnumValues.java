package com.cbc.portal.entity;
// Generated Oct 24, 2016 1:00:11 PM by Hibernate Tools 5.1.0.Alpha1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * CcEnumValues generated by hbm2java
 */
@Entity
@Table(name = "CC_ENUM_VALUES", catalog = "jpplcardsdb", uniqueConstraints = @UniqueConstraint(columnNames = {
		"TYPE_NAME", "INT_CODE" }))
public class CcEnumValues implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer enumNo;
	private String typeName;
	private byte intCode;
	private String value;

	public CcEnumValues() {
	}

	public CcEnumValues(String typeName, byte intCode, String value) {
		this.typeName = typeName;
		this.intCode = intCode;
		this.value = value;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ENUM_NO", unique = true, nullable = false)
	public Integer getEnumNo() {
		return this.enumNo;
	}

	public void setEnumNo(Integer enumNo) {
		this.enumNo = enumNo;
	}

	@Column(name = "TYPE_NAME", nullable = false, length = 30)
	public String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@Column(name = "INT_CODE", nullable = false)
	public byte getIntCode() {
		return this.intCode;
	}

	public void setIntCode(byte intCode) {
		this.intCode = intCode;
	}

	@Column(name = "VALUE", nullable = false)
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
