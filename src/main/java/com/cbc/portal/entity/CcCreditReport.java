/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Aravind E
 */
@Entity
@Table(name = "CC_CREDIT_REPORT", catalog = "jpplcardsdb")
public class CcCreditReport implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int crid;
    private String fName;
    private String mName;
    private String lName;
    private String mobileNumber;
    private String emailID;
//    private Date dateOfBirth;
    private String apiRequest;
    private String Response;
    private String stageoneID;
    private String stagtwoID;
    private String errorMsg;
    private String newuserID;
    private Date applicationDate;
    private String jpNumber;
    private String creditScore;
    private String creditScoreLevel;
    private String emailJPID;
    private String mobileNumberJP;
    private String reportNumber;
    private String reportDate;
    private String reportTime;
    private String mergeJP;
    private Date createdDate;
    private Date modifiedDate;
    private String city;
    private String status;
    private String tier;
    private String promocode;
    private String dob;
    private String Gender;
    private String requestFilePath;
    private String responseFilePath;

//private Date createdTime;
    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "CR_ID", unique = true, nullable = false)
    public int getCrid() {
        return crid;
    }

    public void setCrid(int crid) {
        this.crid = crid;
    }

    @Column(name = "FNAME", nullable = false)
    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    @Column(name = "MNAME")
    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    @Column(name = "LNAME", nullable = false)
    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    @Column(name = "MOBILE_NUMBER", nullable = false)
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Column(name = "EMAIL_ID", nullable = false)
    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

//   @Column(name = "DOB", nullable = false)
//    public Date getDateOfBirth() {
//        return dateOfBirth;
//    }
//
//    public void setDateOfBirth(Date dateOfBirth) {
//        this.dateOfBirth = dateOfBirth;
//    }
    @Column(name = "API_REQUEST", nullable = false)
    public String getApiRequest() {
        return apiRequest;
    }

    public void setApiRequest(String apiRequest) {
        this.apiRequest = apiRequest;
    }

    @Column(name = "API_RESPONSE", nullable = false,columnDefinition="TEXT")
    public String getResponse() {
        return Response;
    }

    public void setResponse(String Response) {
        this.Response = Response;
    }

    @Column(name = "STAGEONE_ID")
    public String getStageoneID() {
        return stageoneID;
    }

    public void setStageoneID(String stageoneID) {
        this.stageoneID = stageoneID;
    }

    @Column(name = "STAGETWO_ID")
    public String getStagtwoID() {
        return stagtwoID;
    }

    public void setStagtwoID(String stagtwoID) {
        this.stagtwoID = stagtwoID;
    }

    @Column(name = "ERROR_MSG")
    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Column(name = "NEW_USER_ID")
    public String getNewuserID() {
        return newuserID;
    }

    public void setNewuserID(String newuserID) {
        this.newuserID = newuserID;
    }

    @Column(name = "APPLICATION_DATE", nullable = false)
    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    @Column(name = "JPNUMBER", nullable = false)
    public String getJpNumber() {
        return jpNumber;
    }

    public void setJpNumber(String jpNumber) {
        this.jpNumber = jpNumber;
    }

    @Column(name = "CREDIT_SCORE", nullable = false)
    public String getCreditScore() {
        return creditScore;
    }

    public void setCreditScore(String creditScore) {
        this.creditScore = creditScore;
    }

//    @Temporal(TemporalType.TIMESTAMP)
//    @Column(name = "CREATED_TIME", nullable = false)
//    public Date getCreatedTime() {
//        return createdTime;
//    }
//
//    public void setCreatedTime(Date createdTime) {
//        this.createdTime = createdTime;
//    }
    @Column(name = "CREDIT_LEVEL", nullable = false)
    public String getCreditScoreLevel() {
        return creditScoreLevel;
    }

    public void setCreditScoreLevel(String creditScoreLevel) {
        this.creditScoreLevel = creditScoreLevel;
    }

    @Column(name = "JP_EMAIL", nullable = false)
    public String getEmailJPID() {
        return emailJPID;
    }

    public void setEmailJPID(String emailJPID) {
        this.emailJPID = emailJPID;
    }

    @Column(name = "JP_MOBILE", nullable = false)
    public String getMobileNumberJP() {
        return mobileNumberJP;
    }

    public void setMobileNumberJP(String mobileNumberJP) {
        this.mobileNumberJP = mobileNumberJP;
    }

    @Column(name = "REPORT_NUMBER", nullable = false)
    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    @Column(name = "REPORT_DATE", nullable = false)
    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    @Column(name = "MERGE_JPNUMBER", nullable = false)
    public String getMergeJP() {
        return mergeJP;
    }

    public void setMergeJP(String mergeJP) {
        this.mergeJP = mergeJP;
    }

    @Column(name = "CREATED_DATE", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "MODIFIED_DATE", nullable = false)
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Column(name = "CITY", nullable = false)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "STATUS", nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "TIER", nullable = false)
    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    @Column(name = "PROMOCODE", nullable = false)
    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    @Column(name = "DOB", nullable = false)
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Column(name = "GENDER", nullable = false)
    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    @Column(name = "REPORT_TIME", nullable = false)
    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    @Column(name = "REQUEST_FILE_PATH", nullable = false)
    public String getRequestFilePath() {
        return requestFilePath;
    }

    public void setRequestFilePath(String requestFilePath) {
        this.requestFilePath = requestFilePath;
    }

    @Column(name = "RESPONSE_FILE_PATH", nullable = false)
    public String getResponseFilePath() {
        return responseFilePath;
    }

    public void setResponseFilePath(String responseFilePath) {
        this.responseFilePath = responseFilePath;
    }

}
