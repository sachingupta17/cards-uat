/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Aravind E
 */
@Entity
@Table(name = "PROMOCODE_ERROR", catalog = "jpplcardsdb")
public class PromocodeError implements Serializable {

    private int pcreasonId;
    private String pcReasoncode;
    private String pcReasonName;
    private String pcReasonMessage;
    private Date createdTime;
    private byte createBy;
    private byte status;

    @Id
    @GeneratedValue(strategy = IDENTITY)
     @Column(name = "PC_REASON_ID", unique = true, nullable = false)
    public int getPcreasonId() {
        return pcreasonId;
    }

    public void setPcreasonId(int pcreasonId) {
        this.pcreasonId = pcreasonId;
    }

    @Column(name = "PC_REASON_CODE")
    public String getPcReasoncode() {
        return pcReasoncode;
    }

    public void setPcReasoncode(String pcReasoncode) {
        this.pcReasoncode = pcReasoncode;
    }

    @Column(name = "PC_REASON_NAME")
    public String getPcReasonName() { 
        return pcReasonName;
    }

    public void setPcReasonName(String pcReasonName) {
        this.pcReasonName = pcReasonName;
    }
    @Column(name = "PC_REASON_MESSAGE")
    public String getPcReasonMessage() {
        return pcReasonMessage;
    }

    public void setPcReasonMessage(String pcReasonMessage) {
        this.pcReasonMessage = pcReasonMessage;
    }
    
    
    @Column(name = "CREATED_TIME")
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
    @Column(name = "CREATED_BY")
    public byte getCreateBy() {
        return createBy;
    }

    public void setCreateBy(byte createBy) {
        this.createBy = createBy;
    }
    @Column(name = "STATUS" ,nullable = false)
    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }
    
    
    
    
    
    

}


