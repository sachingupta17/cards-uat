package com.cbc.portal.entity;
// Generated Jun 23, 2017 5:34:59 PM by Hibernate Tools 5.2.3.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CcMemberDetails generated by hbm2java
 */
@Entity
@Table(name = "CC_MEMBER_DETAILS", catalog = "jpplcardsdb")
public class CcMemberDetails implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer mdNo;
	private String name;
	private String mobile;
	private String email;
	private String jetpriviligemembershipNumber;
	private String formNumber;
	private Date createdTime;
	private Integer createdBy;
	private Date modifiedTime;
	private Integer modifiedBy;
	private byte status;
        private String pageName;
        private String cardName;


	public CcMemberDetails() {
	}

public CcMemberDetails(String name, String mobile, String email, String jetpriviligemembershipNumber, String formNumber, byte status,String pageName,String cardName) {
		this.name = name;
		this.mobile = mobile;
		this.email = email;
		this.jetpriviligemembershipNumber = jetpriviligemembershipNumber;
		this.setFormNumber(formNumber);
		this.status = status;
		this.pageName = pageName;
		this.cardName = cardName;
                
	}
	public CcMemberDetails(String name, String mobile, String email, String jetpriviligemembershipNumber, String formNumber, 
			Date createdTime, Integer createdBy, Date modifiedTime, Integer modifiedBy, byte status) {
		this.name = name;
		this.mobile = mobile;
		this.email = email;
		this.jetpriviligemembershipNumber = jetpriviligemembershipNumber;
		this.setFormNumber(formNumber);
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.modifiedTime = modifiedTime;
		this.modifiedBy = modifiedBy;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "MD_NO", unique = true, nullable = false)
	public Integer getMdNo() {
		return this.mdNo;
	}

	public void setMdNo(Integer mdNo) {
		this.mdNo = mdNo;
	}

	@Column(name = "NAME", nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "MOBILE", nullable = false, length = 15)
	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "EMAIL", nullable = false, length = 256)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "JETPRIVILIGEMEMBERSHIP_NUMBER", nullable = false, length = 11)
	public String getJetpriviligemembershipNumber() {
		return this.jetpriviligemembershipNumber;
	}

	public void setJetpriviligemembershipNumber(String jetpriviligemembershipNumber) {
		this.jetpriviligemembershipNumber = jetpriviligemembershipNumber;
	}
	
	@Column(name = "FORM_NUMBER", nullable = false, length = 20)
	public String getFormNumber() {
		return formNumber;
	}

	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME", length = 19)
	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Column(name = "CREATED_BY")
	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_TIME", length = 19)
	public Date getModifiedTime() {
		return this.modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	@Column(name = "MODIFIED_BY")
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
   @Column(name = "PAGE_NAME", nullable = false)
    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }
   @Column(name = "CARD_NAME", nullable = false)
    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }
        
        
        

	@Column(name = "STATUS", nullable = false)
	public byte getStatus() {
		return this.status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
}
