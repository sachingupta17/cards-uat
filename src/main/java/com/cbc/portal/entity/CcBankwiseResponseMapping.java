/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Parvti G
 */


@Entity
@Table(name = "CC_BANKWISE_RESPONSE_MAPPING", catalog = "jpplcardsdb")
public class CcBankwiseResponseMapping  implements Serializable{
    private static final long serialVersionUID = 1L;
    private Integer bpstno;
    private CcBankPartner ccBankPartner;

   
    private CcBankwiseStatus ccBankwiseStatus;
    private String title;
    private String description;
    private Integer createdby;
    private Date createdtime;
    private byte status;

  private byte statusType;

 
 private Integer modifiedby;
    private Date modifiedtime;
    
    public CcBankwiseResponseMapping() {
	}
public CcBankwiseResponseMapping(CcBankPartner ccBankPartner, CcBankwiseStatus ccBankwiseStatus, Date createdtime,String title,String description,
			byte status, Integer createdby, Integer modifiedby, Date modifiedtime, byte statusType) {
		this.ccBankPartner = ccBankPartner;
		this.ccBankwiseStatus = ccBankwiseStatus;
		this.createdtime = createdtime;
		this.status = status;
		this.createdby = createdby;
		this.title = title;
		this.description = description;
                this.modifiedtime=modifiedtime;
                this.modifiedby=modifiedby;
	this.statusType=statusType;
	}
@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "BPST_NO", unique = true, nullable = false)
    public Integer getBpstno() {
        return bpstno;
    }

    public void setBpstno(Integer bpstno) {
        this.bpstno = bpstno;
    }
@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BP_NO", nullable = false)
    public CcBankPartner getCcBankPartner() {
        return ccBankPartner;
    }

    public void setCcBankPartner(CcBankPartner ccBankPartner) {
        this.ccBankPartner = ccBankPartner;
    }
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BS_STATUSES_ID", nullable = false)
 public CcBankwiseStatus getCcBankwiseStatus() {
        return ccBankwiseStatus;
    }

    public void setCcBankwiseStatus(CcBankwiseStatus ccBankwiseStatus) {
        this.ccBankwiseStatus = ccBankwiseStatus;
    }
@Column(name = "BS_TITLE", nullable = false)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
@Column(name = "BS_DESC", nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @Column(name = "STATUS_TYPE", nullable = false)
       public byte getStatusType() {
        return statusType;
    }

    public void setStatusType(byte statusType) {
        this.statusType = statusType;
    }
    @Column(name = "CREATED_BY", nullable = false)
    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }
@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME")
    public Date getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }
     @Column(name = "MODIFIED_BY", nullable = false)
      public Integer getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(Integer modifiedby) {
        this.modifiedby = modifiedby;
    }
@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_TIME")
    public Date getModifiedtime() {
        return modifiedtime;
    }

    public void setModifiedtime(Date modifiedtime) {
        this.modifiedtime = modifiedtime;
    }
@Column(name = "STATUS")
    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }
}
