/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author Aravind E
 */
@Entity
@Table(name = "CC_ICICI_APPLICATION_STATUS", catalog = "jpplcardsdb")
public class CcIciciApplicationStatus implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private Integer asNo;
    private CcApplicationFormDetails ccApplicationFormDetails;
    private String cardName;
    private String jpNumber;
    private String pcnNumber;
    private String decisionStatus;
    private Date decisionDate;
    private String wsRequest;
    private String wsResponse;
    private Date createdTime;
    private Integer createdBy;
    private Date modifiedTime;
    private Integer modifiedBy;
    private byte status;
    private String iciciApiRequest;
    private String iciciApiResponse;
    private String iciciID;
    private String iciciResult;
    private String transUnionErrorMsg;
    private String transUnionReason;
    private String TrasunionID;
    private String iciciEligibility;
    private String iciciDescription;
    private String iciciDevice;
    private String transDecisionStatus;
    private String pid;
    
    
    
    
    public CcIciciApplicationStatus() {
    }

    public CcIciciApplicationStatus(CcApplicationFormDetails ccApplicationFormDetails, String cardName, String jpNumber,
            String pcnNumber, byte status, String pid) {
        this.ccApplicationFormDetails = ccApplicationFormDetails;
        this.cardName = cardName;
        this.jpNumber = jpNumber;
        this.pcnNumber = pcnNumber;
        this.status = status;
         this.pid=pid;
    }

    public CcIciciApplicationStatus(Integer asNo, CcApplicationFormDetails ccApplicationFormDetails, String cardName, String jpNumber, String pcnNumber, String decisionStatus, Date decisionDate, String wsRequest, String wsResponse, Date createdTime, Integer createdBy, Date modifiedTime, Integer modifiedBy, byte status, String iciciApiRequest, String iciciApiResponse, String iciciID, String iciciResult, String transUnionErrorMsg, String transUnionReason, String iciciEligibility, String iciciDescription, String iciciDevice,String transDecisionStatus ,String TrasunionID, String pid) {
        this.asNo = asNo;
        this.ccApplicationFormDetails = ccApplicationFormDetails;
        this.cardName = cardName;
        this.jpNumber = jpNumber;
        this.pcnNumber = pcnNumber;
        this.decisionStatus = decisionStatus;
        this.decisionDate = decisionDate;
        this.wsRequest = wsRequest;
        this.wsResponse = wsResponse;
        this.createdTime = createdTime;
        this.createdBy = createdBy;
        this.modifiedTime = modifiedTime;
        this.modifiedBy = modifiedBy;
        this.status = status;
        this.iciciApiRequest = iciciApiRequest;
        this.iciciApiResponse = iciciApiResponse;
        this.iciciID = iciciID;
        this.iciciResult = iciciResult;
        this.transUnionErrorMsg = transUnionErrorMsg;
        this.transUnionReason = transUnionReason;
        this.iciciEligibility = iciciEligibility;
        this.iciciDescription = iciciDescription;
        this.iciciDevice = iciciDevice;
        this.transDecisionStatus = transDecisionStatus;
         this.TrasunionID = TrasunionID;
         this.pid=pid;
         
    }

    
    
   

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "AS_NO", unique = true, nullable = false)
    public Integer getAsNo() {
        return this.asNo;
    }

    public void setAsNo(Integer asNo) {
        this.asNo = asNo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APP_FORM_NO", nullable = false)
    public CcApplicationFormDetails getCcApplicationFormDetails() {
        return this.ccApplicationFormDetails;
    }

    public void setCcApplicationFormDetails(CcApplicationFormDetails ccApplicationFormDetails) {
        this.ccApplicationFormDetails = ccApplicationFormDetails;
    }

    @Column(name = "CARD_NAME", nullable = false, length = 300)
    public String getCardName() {
        return this.cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    @Column(name = "JP_NUMBER", nullable = false, length = 11)
    public String getJpNumber() {
        return this.jpNumber;
    }

    public void setJpNumber(String jpNumber) {
        this.jpNumber = jpNumber;
    }

    @Column(name = "PCN_NUMBER", nullable = false, length = 50)
    public String getPcnNumber() {
        return this.pcnNumber;
    }

    public void setPcnNumber(String pcnNumber) {
        this.pcnNumber = pcnNumber;
    }

    @Column(name = "DECISION_STATUS", length = 50)
    public String getDecisionStatus() {
        return this.decisionStatus;
    }

    public void setDecisionStatus(String decisionStatus) {
        this.decisionStatus = decisionStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DECISION_DATE", length = 19)
    public Date getDecisionDate() {
        return this.decisionDate;
    }

    public void setDecisionDate(Date decisionDate) {
        this.decisionDate = decisionDate;
    }

    @Column(name = "WS_REQUEST", length = 65535)
    public String getWsRequest() {
        return this.wsRequest;
    }

    public void setWsRequest(String wsRequest) {
        this.wsRequest = wsRequest;
    }

    @Column(name = "WS_RESPONSE", length = 65535)
    public String getWsResponse() {
        return this.wsResponse;
    }

    public void setWsResponse(String wsResponse) {
        this.wsResponse = wsResponse;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_TIME", length = 19)
    public Date getCreatedTime() {
        return this.createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Column(name = "CREATED_BY")
    public Integer getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MODIFIED_TIME", length = 19)
    public Date getModifiedTime() {
        return this.modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    @Column(name = "MODIFIED_BY")
    public Integer getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Column(name = "STATUS", nullable = false)
    public byte getStatus() {
        return this.status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    @Column(name = "ICICI_REQUEST", length = 65535)
    public String getIciciApiRequest() {
        return iciciApiRequest;
    }

    public void setIciciApiRequest(String iciciApiRequest) {
        this.iciciApiRequest = iciciApiRequest;
    }

    @Column(name = "ICICI_RESPONSE", length = 65535)
    public String getIciciApiResponse() {
        return iciciApiResponse;
    }

    public void setIciciApiResponse(String iciciApiResponse) {
        this.iciciApiResponse = iciciApiResponse;
    }

    @Column(name = "ICICI_ID")
    public String getIciciID() {
        return iciciID;
    }

    public void setIciciID(String iciciID) {
        this.iciciID = iciciID;
    }

    @Column(name = "ICICI_RESULT")
    public String getIciciResult() {
        return iciciResult;
    }

    public void setIciciResult(String iciciResult) {
        this.iciciResult = iciciResult;
    }
     @Column(name = "ICICI_ELIGIBILITY")
    public String getIciciEligibility() {
        return iciciEligibility;
    }

    public void setIciciEligibility(String iciciEligibility) {
        this.iciciEligibility = iciciEligibility;
    }
    @Column(name = "ICICI_DESCRIPTION")
    public String getIciciDescription() {
        return iciciDescription;
    }

    public void setIciciDescription(String iciciDescription) {
        this.iciciDescription = iciciDescription;
    }
 @Column(name = "ICICI_DEVICE")
    public String getIciciDevice() {
        return iciciDevice;
    }

    public void setIciciDevice(String iciciDevice) {
        this.iciciDevice = iciciDevice;
    }
    @Column(name = "TRANS_UNION_ERROR")
    public String getTransUnionErrorMsg() {
        return transUnionErrorMsg;
    }

    public void setTransUnionErrorMsg(String transUnionErrorMsg) {
        this.transUnionErrorMsg = transUnionErrorMsg;
    }
   @Column(name = "TRANS_UNION_REASON")
    public String getTransUnionReason() {
        return transUnionReason;
    }

    public void setTransUnionReason(String transUnionReason) {
        this.transUnionReason = transUnionReason;
    }
    @Column(name = "TRANS_UNION_DECISION", length = 50)
    public String getTransDecisionStatus() {
        return transDecisionStatus;
    }

    public void setTransDecisionStatus(String transDecisionStatus) {
        this.transDecisionStatus = transDecisionStatus;
    }
   @Column(name = "PID")
	public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

}
