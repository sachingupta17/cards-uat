/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

/**
 *
 * @author Aravind E
 */
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name = "CC_COMPANY_LIST", catalog = "jpplcardsdb")
public class CcCompanyList implements Serializable{
    private static final long serialVersionUID = 1L;
	private Integer crNo;
        private Integer SrNo;
        private String companyname;
        private String category;
        private Date createdTime;
        private Integer createdBy;
        private Integer status;


   
        public CcCompanyList(){
        }
       public  CcCompanyList(Integer crNo,String companyname, Date createdTime,Integer createdBy,Integer SrNo,String category,Integer status ){
           this.crNo=crNo;
            this.companyname=companyname;
            this.createdTime=createdTime;
            this.createdBy=createdBy;
            this.SrNo=SrNo;
            this.category=category; 
            this.status=status;
        }

   
        @Id
	@GeneratedValue(strategy = IDENTITY)
        @Column(name = "CR_NO", unique = true, nullable = false)
public Integer getCrNo() {
        return crNo;
    }

    public void setCrNo(Integer crNo) {
        this.crNo = crNo;
    }
     @Column(name = "SR_NO")
     public Integer getSrNo() {
        return SrNo;
    }

    public void setSrNo(Integer SrNo) {
        this.SrNo = SrNo;
    }

    
      @Column(name = "COMPANY_NAME", nullable = false, length = 6)
    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }
     @Column(name = "CATEGORY")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME", length = 0)
    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
@Column(name = "CREATED_BY")
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }
    
    @Column(name = "STATUS")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
