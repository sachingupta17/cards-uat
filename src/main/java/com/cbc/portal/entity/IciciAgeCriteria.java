/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Parvti G
 */
@Entity
@Table(name = "ICICI_AGE_CRITERIA", catalog = "jpplcardsdb")
public class IciciAgeCriteria implements java.io.Serializable{
    private static final long serialVersionUID = 1L;
    private Integer iecno;
   private CcBankPartner ccBankPartner;
   private CcCardProduct ccCardProduct;
    private String emptype;
    private Integer ageabove;
    private Integer agebelow;
    private String city;
    private Date createdtime;
    private Integer createdby;
    private byte status;

    public IciciAgeCriteria(){}
    public IciciAgeCriteria(Integer iecno, CcBankPartner ccBankPartner, CcCardProduct ccCardProduct, String emptype, Integer ageabove, Integer agebelow,String city, Date createdtime ,Integer createdby, byte status){
    this.iecno=iecno;
    this.ccBankPartner=ccBankPartner;
    this.ccCardProduct=ccCardProduct;
    this.emptype=emptype;
    this.agebelow=agebelow;
    this.ageabove=ageabove;
    this.city=city;
    this.createdby=createdby;
    this.createdtime=createdtime;
    this.status=status;
    }
    
    
    
    
    @Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "IEC_NO", unique = true, nullable = false)
    public Integer getIecno() {
        return iecno;
    }

    public void setIecno(Integer iecno) {
        this.iecno = iecno;
    }

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BP_NO", nullable = false)
	public CcBankPartner getCcBankPartner() {
		return this.ccBankPartner;
	}

	public void setCcBankPartner(CcBankPartner ccBankPartner) {
		this.ccBankPartner = ccBankPartner;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CP_NO", nullable = false)
	public CcCardProduct getCcCardProduct() {
		return this.ccCardProduct;
	}

	public void setCcCardProduct(CcCardProduct ccCardProduct) {
		this.ccCardProduct = ccCardProduct;
	}
@Column(name = "EMP_TYPE")
    public String getEmptype() {
        return emptype;
    }

    public void setEmptype(String emptype) {
        this.emptype = emptype;
    }
@Column(name = "AGE_ABOVE")
    public Integer getAgeabove() {
        return ageabove;
    }

    public void setAgeabove(Integer ageabove) {
        this.ageabove = ageabove;
    }
@Column(name = "AGE_BELOW")
    public Integer getAgebelow() {
        return agebelow;
    }

    public void setAgebelow(Integer agebelow) {
        this.agebelow = agebelow;
    }
@Column(name = "CITY")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    @Temporal(TemporalType.TIMESTAMP)
@Column(name = "CREATED_TIME")
    public Date getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }
@Column(name = "CREATED_BY")
    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }
@Column(name = "STATUS")
    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }
    
}
