/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Parvti G
 */
@Entity
@Table(name = "CC_BANKWISE_STATUS", catalog = "jpplcardsdb")
public class CcBankwiseStatus implements Serializable{
     private static final long serialVersionUID = 1L;

    
    private Integer bsno;
    private String statusDesc;
    
   private Integer createdby;
    private Date createdtime;
    private byte status;
    
     public CcBankwiseStatus() {
	}
public CcBankwiseStatus( String statusDesc, Date createdtime,Integer createdby,
			byte status) {
		
		this.statusDesc = statusDesc;
		this.createdtime = createdtime;
		this.status = status;
		this.createdby = createdby;
		
	
	}
@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "BS_ID", unique = true, nullable = false)

public Integer getBsno() {
        return bsno;
    }

    public void setBsno(Integer bsno) {
        this.bsno = bsno;
    }
@Column(name = "STATUS_DESC", nullable = false)
    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }
 @Column(name = "CREATED_BY", nullable = false)
    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }
@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME")
    public Date getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }
@Column(name = "STATUS")
    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }
}
