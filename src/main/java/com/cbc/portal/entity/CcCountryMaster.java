/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Aravind E
 */
@Entity
@Table(name = "CC_COUNTRY_MASTER", catalog = "jpplcardsdb")
public class CcCountryMaster implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private Integer ccCountryId;
    private String cardName;
    private Date createdTime;
    private Integer createdBy;
    private Date modifiedTime;
    private Integer modifiedBy;
    private byte status;
    
    public CcCountryMaster() {
	}

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "CCM_NO", unique = true, nullable = false)
    public Integer getCcCountryId() {
        return ccCountryId;
    }

    public void setCcCountryId(Integer ccCountryId) {
        this.ccCountryId = ccCountryId;
    }
    @Column(name = "CCM_NAME", nullable = false, length = 300)
    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_TIME", nullable = false, length = 19)
    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
   @Column(name = "CREATED_BY")
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MODIFIED_TIME", length = 19)
    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
    @Column(name = "MODIFIED_BY")
    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
   @Column(name = "STATUS", nullable = false)
    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }
    
    
       
    
}