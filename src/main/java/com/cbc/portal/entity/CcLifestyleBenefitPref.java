package com.cbc.portal.entity;
// Generated Jan 17, 2017 3:26:45 PM by Hibernate Tools 5.1.0.Alpha1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CcLifestyleBenefitPref generated by hbm2java
 */
@Entity
@Table(name = "CC_LIFESTYLE_BENEFIT_PREF", catalog = "jpplcardsdb")
public class CcLifestyleBenefitPref implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer lsbpNo;
	private String lsbpName;
	private byte[] lsbpImage;
	private String imageText;
	private String imagePath;
	private byte[] imageAsIcon;
	private String imageAsIconText;
	private String imageAsIconPath;
	private byte[] imageAsIconBlue;
	private String imageAsIconBluePath;
	private byte lsbpStatus;
	private Date createdTime;
	private Integer createdBy;
	private Date modifiedTime;
	private Integer modifiedBy;
	private byte status;
	private Set<CcLifestyleBenefitPrefValues> ccLifestyleBenefitPrefValueses = new HashSet<CcLifestyleBenefitPrefValues>(0);

	public CcLifestyleBenefitPref() {
	}

	public CcLifestyleBenefitPref(String lsbpName, byte[] lsbpImage, String imageText, String imagePath,
			byte[] imageAsIcon, String imageAsIconText, String imageAsIconPath, byte[] imageAsIconBlue,
			String imageAsIconBluePath, byte lsbpStatus, byte status) {
		this.lsbpName = lsbpName;
		this.lsbpImage = lsbpImage;
		this.imageText = imageText;
		this.imagePath = imagePath;
		this.imageAsIcon = imageAsIcon;
		this.imageAsIconText = imageAsIconText;
		this.imageAsIconPath = imageAsIconPath;
		this.imageAsIconBlue = imageAsIconBlue;
		this.imageAsIconBluePath = imageAsIconBluePath;
		this.lsbpStatus = lsbpStatus;
		this.status = status;
	}

	public CcLifestyleBenefitPref(String lsbpName, byte[] lsbpImage, String imageText, String imagePath,
			byte[] imageAsIcon, String imageAsIconText, String imageAsIconPath, byte[] imageAsIconBlue,
			String imageAsIconBluePath, byte lsbpStatus, Date createdTime, Integer createdBy, Date modifiedTime,
			Integer modifiedBy, byte status, Set<CcLifestyleBenefitPrefValues> ccLifestyleBenefitPrefValueses) {
		this.lsbpName = lsbpName;
		this.lsbpImage = lsbpImage;
		this.imageText = imageText;
		this.imagePath = imagePath;
		this.imageAsIcon = imageAsIcon;
		this.imageAsIconText = imageAsIconText;
		this.imageAsIconPath = imageAsIconPath;
		this.imageAsIconBlue = imageAsIconBlue;
		this.imageAsIconBluePath = imageAsIconBluePath;
		this.lsbpStatus = lsbpStatus;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.modifiedTime = modifiedTime;
		this.modifiedBy = modifiedBy;
		this.status = status;
		this.ccLifestyleBenefitPrefValueses = ccLifestyleBenefitPrefValueses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "LSBP_NO", unique = true, nullable = false)
	public Integer getLsbpNo() {
		return this.lsbpNo;
	}

	public void setLsbpNo(Integer lsbpNo) {
		this.lsbpNo = lsbpNo;
	}

	@Column(name = "LSBP_NAME", unique = true, nullable = false, length = 300)
	public String getLsbpName() {
		return this.lsbpName;
	}

	public void setLsbpName(String lsbpName) {
		this.lsbpName = lsbpName;
	}

	@Column(name = "LSBP_IMAGE", nullable = false)
	public byte[] getLsbpImage() {
		return this.lsbpImage;
	}

	public void setLsbpImage(byte[] lsbpImage) {
		this.lsbpImage = lsbpImage;
	}

	@Column(name = "IMAGE_TEXT", nullable = false, length = 300)
	public String getImageText() {
		return this.imageText;
	}

	public void setImageText(String imageText) {
		this.imageText = imageText;
	}

	@Column(name = "IMAGE_PATH", nullable = false, length = 300)
	public String getImagePath() {
		return this.imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Column(name = "IMAGE_AS_ICON", nullable = false)
	public byte[] getImageAsIcon() {
		return this.imageAsIcon;
	}

	public void setImageAsIcon(byte[] imageAsIcon) {
		this.imageAsIcon = imageAsIcon;
	}

	@Column(name = "IMAGE_AS_ICON_TEXT", nullable = false, length = 300)
	public String getImageAsIconText() {
		return this.imageAsIconText;
	}

	public void setImageAsIconText(String imageAsIconText) {
		this.imageAsIconText = imageAsIconText;
	}

	@Column(name = "IMAGE_AS_ICON_PATH", nullable = false, length = 300)
	public String getImageAsIconPath() {
		return this.imageAsIconPath;
	}

	public void setImageAsIconPath(String imageAsIconPath) {
		this.imageAsIconPath = imageAsIconPath;
	}

	@Column(name = "IMAGE_AS_ICON_BLUE", nullable = false)
	public byte[] getImageAsIconBlue() {
		return this.imageAsIconBlue;
	}

	public void setImageAsIconBlue(byte[] imageAsIconBlue) {
		this.imageAsIconBlue = imageAsIconBlue;
	}

	@Column(name = "IMAGE_AS_ICON_BLUE_PATH", nullable = false, length = 300)
	public String getImageAsIconBluePath() {
		return this.imageAsIconBluePath;
	}

	public void setImageAsIconBluePath(String imageAsIconBluePath) {
		this.imageAsIconBluePath = imageAsIconBluePath;
	}

	@Column(name = "LSBP_STATUS", nullable = false)
	public byte getLsbpStatus() {
		return this.lsbpStatus;
	}

	public void setLsbpStatus(byte lsbpStatus) {
		this.lsbpStatus = lsbpStatus;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_TIME", length = 19)
	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Column(name = "CREATED_BY")
	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_TIME", length = 19)
	public Date getModifiedTime() {
		return this.modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	@Column(name = "MODIFIED_BY")
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "STATUS", nullable = false)
	public byte getStatus() {
		return this.status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ccLifestyleBenefitPref")
	public Set<CcLifestyleBenefitPrefValues> getCcLifestyleBenefitPrefValueses() {
		return this.ccLifestyleBenefitPrefValueses;
	}

	public void setCcLifestyleBenefitPrefValueses(Set<CcLifestyleBenefitPrefValues> ccLifestyleBenefitPrefValueses) {
		this.ccLifestyleBenefitPrefValueses = ccLifestyleBenefitPrefValueses;
	}

}
