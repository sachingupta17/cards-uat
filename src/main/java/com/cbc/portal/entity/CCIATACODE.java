/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

/**
 *
 * @author Aravind E
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CC_IATA_CODE")
public class CCIATACODE {
     private Integer ita_no;
    private String cityCode;
    private String cityName;
    private String countryCode;
    private Date modifiedDate;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ITA_NO", unique = true, nullable = false)
    public Integer getIta_no() {
        return ita_no;
    }

    public void setIta_no(Integer ita_no) {
        this.ita_no = ita_no;
    }
    
  
    @Column(name = "cty_code")
    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
   @Column(name = "cty_name")
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
   @Column(name = "cty_ctr_code")
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
   @Column(name = "cty_modify_dt")
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
   
    
}
