package com.cbc.portal.entity;

//Generated Jul 6, 2017 10:32:04 AM by Hibernate Tools 5.2.3.Final

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CcTimer generated by hbm2java
 */

@Entity
@Table(name = "CC_TIMER", catalog = "jpplcardsdb")
public class CcTimer implements java.io.Serializable {

		private static final long serialVersionUID = 1L;
		private Integer ctNo;
		private String ctName1;
		private Integer ctSeconds1;
		private Integer ctMinutes1;
		private Integer ctHours1;
		private Integer ctDays1;
		private String ctName2;
		private Integer ctSeconds2;
		private Integer ctMinutes2;
		private Integer ctHours2;
		private Integer ctDays2;
		private String ctName3;
		private Integer ctSeconds3;
		private Integer ctMinutes3;
		private Integer ctHours3;
		private Integer ctDays3;
		private String ctName4;
		private Integer ctSeconds4;
		private Integer ctMinutes4;
		private Integer ctHours4;
		private Integer ctDays4;
		private Date createdTime;
		private Integer createdBy;
		private Date modifiedTime;
		private Integer modifiedBy;
		private byte status;

		public CcTimer() {
		}

		public CcTimer(String ctName1, String ctName2, String ctName3, String ctName4, byte status) {
			this.ctName1 = ctName1;
			this.ctName2 = ctName2;
			this.ctName3 = ctName3;
			this.ctName4 = ctName4;
			this.status = status;
		}

		public CcTimer(String ctName1, Integer ctSeconds1, Integer ctMinutes1, Integer ctHours1, Integer ctDays1,
				String ctName2, Integer ctSeconds2, Integer ctMinutes2, Integer ctHours2, Integer ctDays2, String ctName3,
				Integer ctSeconds3, Integer ctMinutes3, Integer ctHours3, Integer ctDays3, String ctName4,
				Integer ctSeconds4, Integer ctMinutes4, Integer ctHours4, Integer ctDays4, Date createdTime,
				Integer createdBy, Date modifiedTime, Integer modifiedBy, byte status) {
			this.ctName1 = ctName1;
			this.ctSeconds1 = ctSeconds1;
			this.ctMinutes1 = ctMinutes1;
			this.ctHours1 = ctHours1;
			this.ctDays1 = ctDays1;
			this.ctName2 = ctName2;
			this.ctSeconds2 = ctSeconds2;
			this.ctMinutes2 = ctMinutes2;
			this.ctHours2 = ctHours2;
			this.ctDays2 = ctDays2;
			this.ctName3 = ctName3;
			this.ctSeconds3 = ctSeconds3;
			this.ctMinutes3 = ctMinutes3;
			this.ctHours3 = ctHours3;
			this.ctDays3 = ctDays3;
			this.ctName4 = ctName4;
			this.ctSeconds4 = ctSeconds4;
			this.ctMinutes4 = ctMinutes4;
			this.ctHours4 = ctHours4;
			this.ctDays4 = ctDays4;
			this.createdTime = createdTime;
			this.createdBy = createdBy;
			this.modifiedTime = modifiedTime;
			this.modifiedBy = modifiedBy;
			this.status = status;
		}

		@Id
		@GeneratedValue(strategy = IDENTITY)

		@Column(name = "CT_NO", unique = true, nullable = false)
		public Integer getCtNo() {
			return this.ctNo;
		}

		public void setCtNo(Integer ctNo) {
			this.ctNo = ctNo;
		}

		@Column(name = "CT_NAME1", nullable = false, length = 50)
		public String getCtName1() {
			return this.ctName1;
		}

		public void setCtName1(String ctName1) {
			this.ctName1 = ctName1;
		}

		@Column(name = "CT_SECONDS1")
		public Integer getCtSeconds1() {
			return this.ctSeconds1;
		}

		public void setCtSeconds1(Integer ctSeconds1) {
			this.ctSeconds1 = ctSeconds1;
		}

		@Column(name = "CT_MINUTES1")
		public Integer getCtMinutes1() {
			return this.ctMinutes1;
		}

		public void setCtMinutes1(Integer ctMinutes1) {
			this.ctMinutes1 = ctMinutes1;
		}

		@Column(name = "CT_HOURS1")
		public Integer getCtHours1() {
			return this.ctHours1;
		}

		public void setCtHours1(Integer ctHours1) {
			this.ctHours1 = ctHours1;
		}

		@Column(name = "CT_DAYS1")
		public Integer getCtDays1() {
			return this.ctDays1;
		}

		public void setCtDays1(Integer ctDays1) {
			this.ctDays1 = ctDays1;
		}

		@Column(name = "CT_NAME2", nullable = false, length = 50)
		public String getCtName2() {
			return this.ctName2;
		}

		public void setCtName2(String ctName2) {
			this.ctName2 = ctName2;
		}

		@Column(name = "CT_SECONDS2")
		public Integer getCtSeconds2() {
			return this.ctSeconds2;
		}

		public void setCtSeconds2(Integer ctSeconds2) {
			this.ctSeconds2 = ctSeconds2;
		}

		@Column(name = "CT_MINUTES2")
		public Integer getCtMinutes2() {
			return this.ctMinutes2;
		}

		public void setCtMinutes2(Integer ctMinutes2) {
			this.ctMinutes2 = ctMinutes2;
		}

		@Column(name = "CT_HOURS2")
		public Integer getCtHours2() {
			return this.ctHours2;
		}

		public void setCtHours2(Integer ctHours2) {
			this.ctHours2 = ctHours2;
		}

		@Column(name = "CT_DAYS2")
		public Integer getCtDays2() {
			return this.ctDays2;
		}

		public void setCtDays2(Integer ctDays2) {
			this.ctDays2 = ctDays2;
		}

		@Column(name = "CT_NAME3", nullable = false, length = 50)
		public String getCtName3() {
			return this.ctName3;
		}

		public void setCtName3(String ctName3) {
			this.ctName3 = ctName3;
		}

		@Column(name = "CT_SECONDS3")
		public Integer getCtSeconds3() {
			return this.ctSeconds3;
		}

		public void setCtSeconds3(Integer ctSeconds3) {
			this.ctSeconds3 = ctSeconds3;
		}

		@Column(name = "CT_MINUTES3")
		public Integer getCtMinutes3() {
			return this.ctMinutes3;
		}

		public void setCtMinutes3(Integer ctMinutes3) {
			this.ctMinutes3 = ctMinutes3;
		}

		@Column(name = "CT_HOURS3")
		public Integer getCtHours3() {
			return this.ctHours3;
		}

		public void setCtHours3(Integer ctHours3) {
			this.ctHours3 = ctHours3;
		}

		@Column(name = "CT_DAYS3")
		public Integer getCtDays3() {
			return this.ctDays3;
		}

		public void setCtDays3(Integer ctDays3) {
			this.ctDays3 = ctDays3;
		}

		@Column(name = "CT_NAME4", nullable = false, length = 50)
		public String getCtName4() {
			return this.ctName4;
		}

		public void setCtName4(String ctName4) {
			this.ctName4 = ctName4;
		}

		@Column(name = "CT_SECONDS4")
		public Integer getCtSeconds4() {
			return this.ctSeconds4;
		}

		public void setCtSeconds4(Integer ctSeconds4) {
			this.ctSeconds4 = ctSeconds4;
		}

		@Column(name = "CT_MINUTES4")
		public Integer getCtMinutes4() {
			return this.ctMinutes4;
		}

		public void setCtMinutes4(Integer ctMinutes4) {
			this.ctMinutes4 = ctMinutes4;
		}

		@Column(name = "CT_HOURS4")
		public Integer getCtHours4() {
			return this.ctHours4;
		}

		public void setCtHours4(Integer ctHours4) {
			this.ctHours4 = ctHours4;
		}

		@Column(name = "CT_DAYS4")
		public Integer getCtDays4() {
			return this.ctDays4;
		}

		public void setCtDays4(Integer ctDays4) {
			this.ctDays4 = ctDays4;
		}

		@Temporal(TemporalType.TIMESTAMP)
		@Column(name = "CREATED_TIME", length = 19)
		public Date getCreatedTime() {
			return this.createdTime;
		}

		public void setCreatedTime(Date createdTime) {
			this.createdTime = createdTime;
		}

		@Column(name = "CREATED_BY")
		public Integer getCreatedBy() {
			return this.createdBy;
		}

		public void setCreatedBy(Integer createdBy) {
			this.createdBy = createdBy;
		}

		@Temporal(TemporalType.TIMESTAMP)
		@Column(name = "MODIFIED_TIME", length = 19)
		public Date getModifiedTime() {
			return this.modifiedTime;
		}

		public void setModifiedTime(Date modifiedTime) {
			this.modifiedTime = modifiedTime;
		}

		@Column(name = "MODIFIED_BY")
		public Integer getModifiedBy() {
			return this.modifiedBy;
		}

		public void setModifiedBy(Integer modifiedBy) {
			this.modifiedBy = modifiedBy;
		}

		@Column(name = "STATUS", nullable = false)
		public byte getStatus() {
			return this.status;
		}

		public void setStatus(byte status) {
			this.status = status;
		}

}
