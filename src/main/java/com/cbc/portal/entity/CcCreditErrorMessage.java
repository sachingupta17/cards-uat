/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author parvati
 */
@Entity
@Table(name = "CC_CREDIT_ERROR_MESSAGE", catalog = "jpplcardsdb")
public class CcCreditErrorMessage implements Serializable {
        private static final long serialVersionUID = 1L;

    private Integer cerNo;
    private String reason;
    private String errorMessage;
    private String errorDescription;
    private String errorMessageUser;
    private String status;
    private String createdBy;

    public CcCreditErrorMessage() {
    }

    public CcCreditErrorMessage(Integer cerNo, String reason, String errorMessage, String errorDescription, String errorMessageUser, String status, String createdBy) {
        this.cerNo = cerNo;
        this.reason = reason;
        this.errorMessage = errorMessage;
        this.errorDescription = errorDescription;
        this.errorMessageUser = errorMessageUser;
        this.status = status;
        this.createdBy = createdBy;
    }
@Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "CEM_NO", unique = true, nullable = false)
    public Integer getCerNo() {
        return cerNo;
    }

    public void setCerNo(Integer cerNo) {
        this.cerNo = cerNo;
    }
    @Column(name = "REASON")
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    @Column(name = "ERROR_MESSAGE")
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    @Column(name = "ERROR_DESCRIPTION")
    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
    @Column(name = "MESSAGE_FOR_USER")
    public String getErrorMessageUser() {
        return errorMessageUser;
    }

    public void setErrorMessageUser(String errorMessageUser) {
        this.errorMessageUser = errorMessageUser;
    }
    @Column(name = "STATUS")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
}
