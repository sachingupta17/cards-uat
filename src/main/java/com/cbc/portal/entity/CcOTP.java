/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Aravind E
 */
@Entity
@Table(name = "CC_OTP", catalog = "jpplcardsdb")
public class CcOTP implements Serializable{
    
    private  int otpNo;
    private CcBankPartner ccBankPartner;
    private String mobileNumber;
    private String otpValue;
    private int status;
    private Date date;
    private String formNUmber;
private String otpTransactionId;

    private int applicationId;
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "OTP_NO", unique = true, nullable = false)
    public int getOtpNo() {
        return otpNo;
    }

    public void setOtpNo(int otpNo) {
        this.otpNo = otpNo;
    }
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BP_NO", nullable = false)
     public CcBankPartner getCcBankPartner() {
        return ccBankPartner;
    }

    public void setCcBankPartner(CcBankPartner ccBankPartner) {
        this.ccBankPartner = ccBankPartner;
    }
    @Column(name = "MOBILE_NUMBER")
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    @Column(name = "FORM_NUMBER")
      public String getFormNUmber() {
        return formNUmber;
    }

    public void setFormNUmber(String formNUmber) {
        this.formNUmber = formNUmber;
    }
     @Column(name = "OTP_VALUE")
     public String getOtpValue() {
        return otpValue;
    }

    public void setOtpValue(String otpValue) {
        this.otpValue = otpValue;
    }
     @Column(name = "STATUS")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    @Column(name = "CREATED_TIME")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    @Column(name = "APPLICATION_ID")
    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }
    @Column(name = "OTP_TRANSACTION_ID")

    public String getOtpTransactionId() {
        return otpTransactionId;
    }

    public void setOtpTransactionId(String otpTransactionId) {
        this.otpTransactionId = otpTransactionId;
    }
    
}
