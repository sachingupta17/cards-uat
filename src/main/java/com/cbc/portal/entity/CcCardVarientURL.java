/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author parvati
 */
@Entity
@Table(name = "CC_CARD_VARIENT_URL", catalog = "jpplcardsdb")

public class CcCardVarientURL implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer cvNo;
    private String jpNumber;
    private String cardVarientUrl;
    private CcCardProduct ccCardProduct;
    private byte status;
    private Date createdTime;
    private String randomNumber;
    private String errorMessage;

    public CcCardVarientURL(Integer cvNo, String jpNumber, String cardVarientUrl,
            CcCardProduct ccCardProduct, byte status, Date createdTime, String randomNumber, String errorMessage) {
        this.cvNo = cvNo;
        this.jpNumber = jpNumber;
        this.cardVarientUrl = cardVarientUrl;
        this.ccCardProduct = ccCardProduct;
        this.status = status;
        this.createdTime = createdTime;
        this.randomNumber = randomNumber;
        this.errorMessage=errorMessage;
    }

    public CcCardVarientURL() {
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "CV_NO", unique = true, nullable = false)

    public Integer getCvNo() {
        return cvNo;
    }

    public void setCvNo(Integer cvNo) {
        this.cvNo = cvNo;
    }

    @Column(name = "JP_NUMBER")

    public String getJpNumber() {
        return jpNumber;
    }

    public void setJpNumber(String jpNumber) {
        this.jpNumber = jpNumber;
    }

    @Column(name = "CARD_VARIENT_URL")

    public String getCardVarientUrl() {
        return cardVarientUrl;
    }

    public void setCardVarientUrl(String cardVarientUrl) {
        this.cardVarientUrl = cardVarientUrl;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CP_NO", nullable = false)

    public CcCardProduct getCcCardProduct() {
        return ccCardProduct;
    }

    public void setCcCardProduct(CcCardProduct ccCardProduct) {
        this.ccCardProduct = ccCardProduct;
    }

    @Column(name = "STATUS")

    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_TIME")

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Column(name = "RANDOM_NUMBER")

    public String getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(String randomNumber) {
        this.randomNumber = randomNumber;
    }

        @Column(name = "ERROR_MESSAGE")

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    
}
