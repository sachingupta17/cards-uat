package com.cbc.portal.utils;

import com.cbc.portal.Adobe.DigitalData;
import com.cbc.portal.Adobe.MarketingInfo;
import com.cbc.portal.Adobe.PageInfo;
import com.cbc.portal.Adobe.UserInfo;
import com.google.gson.Gson;
import com.skywards.mercator_cris.Address;
import com.skywards.mercator_cris.ArrayOfAddress;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.skywards.mercator_cris.GetSkywardsProfile;
import com.skywards.mercator_cris.GetSkywardsProfileResponse;
import com.skywards.mercator_cris.ProfileParameters;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import javax.xml.datatype.XMLGregorianCalendar;
import org.json.JSONException;
import org.json.simple.JSONObject;

@Component
@Transactional(readOnly = true)
public class CrisServicesUtils {

    private Logger logger = Logger.getLogger(CrisServicesUtils.class.getName());

    @Autowired
    private WebServiceTemplate webServiceTemplateCris;
    
    @Value("${application.pii.encryption.strSoapURL1}")
   	private String strSoapURL1;
   	@Value("${application.pii.encrypt.strSOAPAction1}")
   	private String soapActEnc;

   	EncryptDetail enc;
   	
    public GetSkywardsProfileResponse getMemberDetails(String jpNumber) {
        String jpNum = (null != jpNumber) ? jpNumber.trim() : "";
        if (jpNum.length() == 9) {
            jpNum = "00" + jpNum;
        }
//        jpNum="00210192916";
//        System.out.println("jpNum==>"+jpNum);
        GetSkywardsProfileResponse getSkywardsProfileResponse = new GetSkywardsProfileResponse();
        GetSkywardsProfile gwtskGetSkywardsProfile = new GetSkywardsProfile();
        ProfileParameters profileParameters = new ProfileParameters();
        gwtskGetSkywardsProfile.setActiveCardNumber(jpNum);
        gwtskGetSkywardsProfile.setMemUID(0);
        gwtskGetSkywardsProfile.setPersonID(0);
        profileParameters.setAddresses(true);
        profileParameters.setContacts(true);
        gwtskGetSkywardsProfile.setParam(profileParameters);

        try {
            System.out.println("getSkywardsProfileResponse  "+getSkywardsProfileResponse);
            getSkywardsProfileResponse = (GetSkywardsProfileResponse) webServiceTemplateCris.marshalSendAndReceive(gwtskGetSkywardsProfile, SoapServicesUtil.addHeaderToSoapCris("GetSkywardsProfile"));
        } catch (Exception e) {
            logger.error("@@@@ Exception in CrisServicesUtils at getMemberDetails() :", e);
        }
        return getSkywardsProfileResponse;
    }

    //Adobe code starts here
    public String GetDigitalDataJSON(String jpNumber, String strSoapURL1, String strSOAPActionencrypt) throws IOException, JSONException {
        
        
        String JPNumber = "";
        String pointBalance = "";
        String pid = "";
        String activeCardNo = "";
        String usernameType = "";
        String firstloginDate = "";
        String lastloginDate = "";
        String EmailVerified = "";
        String MobileVerified = "";
        String PersonId = "";
        String ActiveStatus = "";
        String Age = "";
        String Gender = "";
        String CityOfResidence = "";
        String CountryCountryResidence = "";
        String MemProgram = "";
        String Tier = "";
        String sourceCode = "";
        String mediaCode = "";
        String vitage = "";
        String campaignCode = "";
        String Category = "Cards";
        String sessionID = "";
        String sessionDateTime = "";
        String cookieAcceptedProperty = "";
        String jsondata = "";
        //To get point Balance
        try {

            DigitalData digitalData = new DigitalData();
            UserInfo userInfo = new UserInfo();
            MarketingInfo marketingInfo = new MarketingInfo();
            PageInfo pageInfo = new PageInfo();
            logger.info("jpNumber at crisutil getdigitaldata=======>"+jpNumber);
            if (jpNumber.length() == 9) {
                GetSkywardsProfileResponse objSky = getMemberDetails(jpNumber);
                JPNumber = jpNumber;

                pointBalance = "" + objSky.getGetSkywardsProfileResult().getMember().getPointsBalanace();
                activeCardNo = objSky.getGetSkywardsProfileResult().getMember().getActiveCardNo();

                if (objSky.getGetSkywardsProfileResult().getMember().getUsername().contains("@")) {
//                System.out.println("usernameType in cris service util"+objSky.getGetSkywardsProfileResult().getMember().getUsername());
                    usernameType = "EMAIL";
                } else {
                    String strPrefix = objSky.getGetSkywardsProfileResult().getMember().getUsername().substring(0, 2);
//                System.out.println("strPrefix in cris service util==>"+strPrefix);
                    if (strPrefix.equals("00") || objSky.getGetSkywardsProfileResult().getMember().getUsername().length() < 10) {
                        usernameType = "JPNO";
                    } else {
                        usernameType = "MOB";
                    }
                }

                EmailVerified = objSky.getGetSkywardsProfileResult().getMember().getIsEmailVerified();
                MobileVerified = objSky.getGetSkywardsProfileResult().getMember().getIsMobileVerified();
                PersonId = "" + objSky.getGetSkywardsProfileResult().getMember().getPersonID();
                ActiveStatus = objSky.getGetSkywardsProfileResult().getMember().getActiveStatus();
                Gender = objSky.getGetSkywardsProfileResult().getMember().getGender();
                ArrayOfAddress addresses = objSky.getGetSkywardsProfileResult().getAddresses();

                for (Address address : addresses.getAddress()) {
                    if (address.isIsPreferred()) {
                        CityOfResidence = address.getTown();
                        break;
                    }
                }

                CountryCountryResidence = objSky.getGetSkywardsProfileResult().getMember().getCountryOfResidence();
                MemProgram = "" + objSky.getGetSkywardsProfileResult().getMember().getMemProgram();// Not Reqiored in Adobe
                Tier = objSky.getGetSkywardsProfileResult().getMember().getTier();
                mediaCode = objSky.getGetSkywardsProfileResult().getMember().getEcmCode();
                campaignCode = objSky.getGetSkywardsProfileResult().getMember().getProfilingCampaignCode();
                // Age Calculation 
                try {
                    lastloginDate = "" + objSky.getGetSkywardsProfileResult().getMember().getLastLoginDate();
                    LocalDate memberdob = LocalDate.parse(objSky.getGetSkywardsProfileResult().getMember().getBirthDate().toString().split("T")[0]);

                    LocalDate curDate = LocalDate.now();

                    Age = "" + Period.between(memberdob, curDate).getYears();
                    vitage = objSky.getGetSkywardsProfileResult().getMember().getDateJoined().getValue().toString().split("T")[0];// Enrollment Date not available in CRIS

                } catch (Exception ex) {
                    Age = "";
                }
                enc = new EncryptDetail();
                logger.info("Age=" + enc.encryptData(Age, strSoapURL1, soapActEnc) + "=Gender=" + enc.encryptData(Gender, strSoapURL1, soapActEnc) + "=CountryCountryResidence=" + CountryCountryResidence + "=CityOfResidence=" + CityOfResidence);
                //Encrypt cris data for Adobe
                JSONObject obj = new JSONObject();
                obj.put("Age", Age);
                obj.put("Gender", Gender);
                obj.put("CountryCountryResidence", CountryCountryResidence);
                obj.put("CityOfResidence", CityOfResidence);

                StringWriter out = new StringWriter();
                obj.writeJSONString(out);

                String jsonText = out.toString();
                
                logger.info("jsonText=======>" + enc.encryptData(jsonText, strSoapURL1, soapActEnc));

                String encryptDataVar = enc.encryptData(jsonText, strSoapURL1, strSOAPActionencrypt);
                logger.info("encryptDataVar=======>" + encryptDataVar);
                org.json.JSONObject encryptData = new org.json.JSONObject(encryptDataVar);
                String encr_Age = encryptData.getString("Age");
                String encr_Gender = encryptData.getString("Gender");
                String encr_CountryCountryResidence = encryptData.getString("CountryCountryResidence");
                String encr_CityOfResidence = encryptData.getString("CityOfResidence");
                logger.info("encr_Age=" + encr_Age + "=encr_Gender=" + encr_Gender + "=encr_CountryCountryResidence=" + encr_CountryCountryResidence + "=encr_CityOfResidence=" + encr_CityOfResidence);

                //Set encrypted value to Digital data    
                pageInfo.setCategory(Category);
                digitalData.setPageInfo(pageInfo);

                userInfo.setAge(encr_Age);
                userInfo.setPointsBalance(pointBalance);
                userInfo.setActiveCardNo(activeCardNo);
                userInfo.setUsernameType(usernameType);
                userInfo.setLoginStatus("LOGGED-IN");
                userInfo.setMemProgram(MemProgram);
                userInfo.setLastloginDate(lastloginDate.split("T")[0]);
                userInfo.setMobileVerified(MobileVerified);
                userInfo.setEmailVerified(EmailVerified);

                userInfo.setPersonId(PersonId);
                userInfo.setActiveStatus(ActiveStatus);
                userInfo.setGender(encr_Gender);
                userInfo.setCityofResidence(encr_CityOfResidence);
                userInfo.setCountryofResidence(encr_CountryCountryResidence);
                userInfo.setTier(Tier);
                userInfo.setJPNumber(JPNumber);
                digitalData.setUserInfo(userInfo);

                marketingInfo.setSourceCode(sourceCode);
                marketingInfo.setMediaCode(mediaCode);
                marketingInfo.setVintage(vitage);
                marketingInfo.setCampaignCode(campaignCode);
                digitalData.setMarketingInfo(marketingInfo);

                Gson gson = new Gson();
                jsondata = gson.toJson(digitalData);
//            return jsondata;
            } else {

                pageInfo.setPageName("");
                pageInfo.setPartner("");
                pageInfo.setSubCategory1("");
                pageInfo.setSubCategory2("");
                pageInfo.setCurrency("");
                pageInfo.setCategory("");
                digitalData.setPageInfo(pageInfo);

                userInfo.setActiveCardNo("");
                userInfo.setActiveStatus("");
                userInfo.setAge("");
                userInfo.setCityofResidence("");
                userInfo.setCookieAccept("");
                userInfo.setCookieDateTime("");
                userInfo.setCountryofResidence("");
                userInfo.setEmailVerified("");
                userInfo.setFirstloginDate("");
                userInfo.setGender("");
                userInfo.setJPNumber("");
                userInfo.setLastloginDate("");
                userInfo.setLoginStatus("ANONYMOUS");
                userInfo.setMemProgram("");
                userInfo.setMobileVerified("");
                userInfo.setPersonId("");
                userInfo.setPid("");
                userInfo.setPointsBalance("");
                userInfo.setTier("");
                userInfo.setUsernameType("");
                digitalData.setUserInfo(userInfo);

                marketingInfo.setCampaignCode("");
                marketingInfo.setMediaCode("");
                marketingInfo.setSourceCode("");
                marketingInfo.setVintage("");
                digitalData.setMarketingInfo(marketingInfo);

                Gson gson = new Gson();
                jsondata = gson.toJson(digitalData);

//            return jsondata;
            }

        } catch (Exception Ex) {
            logger.error("Exception " + Ex);
        }
        return jsondata;
    }

    public String getUserDob(String jpNumber) {
        logger.info("jpNumber=getUserDob=======>" + jpNumber);
        String userDob = "";
        GetSkywardsProfileResponse getSkywardsProfileResponse = null;
        XMLGregorianCalendar xmlGregorianCalendar = null;
        Date mdDateOfBirth = null;
        try {
            getSkywardsProfileResponse = getMemberDetails(jpNumber);
            if (null != getSkywardsProfileResponse && null != getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember()) {
//                userDob = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getBirthDate();
                xmlGregorianCalendar = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getBirthDate();
                mdDateOfBirth = xmlGregorianCalendar.toGregorianCalendar().getTime();
                String dateStr = mdDateOfBirth.toString();
                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                Date date = formatter.parse(dateStr);

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                String formatedDate = (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE) + "-" + cal.get(Calendar.YEAR);
                logger.info("formatedDate at getUserDob()" +getEncValue(formatedDate, strSoapURL1, soapActEnc));
                userDob = (null != formatedDate) ? formatedDate.trim() : "";
            }//
        } catch (Exception ex) {
//       ex.printStackTrace();
            logger.error("exception at getUserDob()" + ex);
        }

        return userDob;
    }

    //Adobe code ends here 
    
    
//    public String  getEncValue(String strval)
//    {
//    	org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
//    obj.put("encVal", strval);
//    StringWriter out = new StringWriter();
//    String jsonText = out.toString();
//    EncryptDetail e = new EncryptDetail();
//    String encryptDataVar = e.encryptData(jsonText, strSoapURL1, soapActEnc);
//    org.json.JSONObject encryptData;
//    String encr_val = null;
//	try {
//		obj.writeJSONString(out);
//		encryptData = new org.json.JSONObject(encryptDataVar);
//	    encr_val = encryptData.getString("encVal");
//	} 
//	catch (IOException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
//	catch (JSONException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
//   
//    return encr_val;
//    }
    
    public String  getEncValue(String strval,String strSoapURL1,String soapActEnc)
    {
   	    ICICIEncryptDetail e = new ICICIEncryptDetail();
   	    String encryptDataVar = e.encryptData(strval, strSoapURL1, soapActEnc);
   	    return encryptDataVar;
    }
}
