package com.cbc.portal.utils;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.log4j.Logger;

public class CommonUtils {

	private static Logger logger = Logger.getLogger(CommonUtils.class.getName()); 

	public static Date setDateForAmexAppl(String dateStr){
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		Date date=null;
		try {
			date= sdf.parse(dateStr);
		} catch (ParseException e) {
			logger.error("@@@@ Exception in CommonUtils setDateForAmexAppl() :",e);
		}catch (Exception ex) {
			logger.error("@@@@ Exception in CommonUtils setDateForAmexAppl() :",ex);
		}
		return date;
	}
	
	public static Date getDateFromString(String dateStr){
	    
        Date dt = null;        
        DateFormat sourceDf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");            
        try {
        	if(!dateStr.isEmpty()){
	            dt=sourceDf.parse(dateStr);
        	}
        } catch (ParseException e) {
            logger.error("@@@@ Exception in CommonUtils getDateFromString() ParseException :",e);
        }catch (Exception ex) {
            logger.error("@@@@ Exception in CommonUtils getDateFromString() :",ex);
        }
        return dt;
    }
	
	public static String getDate(Date date, String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	public static String checkForDecimal(BigDecimal value){
		String data = "";
		int i = 0;
		try{
			if(null != value){
			i = value.intValue() % 1;
			if(i == 0){
				data = value.toString();
				data = data.replaceAll("\\.0*$", "");
			}
			else
				data = value.toString();
			}
		}catch(Exception e){
			logger.error("@@@@ Exception in CommonUtils checkForDecimal() :",e);
		}
		return data;
		
	}
	           /* --------------------code by Vernost Team---------------------*/ 
        public static Date ICICIDate(String date){
		String dob="";Date date1=null;
            try {
                 dob=new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("MM-dd-yyyy").parse(date));
                 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                 date1=(Date)formatter.parse(dob);
            } catch (ParseException ex) {
                java.util.logging.Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
                 
		return date1;
	}
             /*---------------End--------------------------*/
	public static String nullSafe(String givenString){
		String returnString;
		try{
			returnString = null!=givenString && !givenString.isEmpty()?givenString.trim():"";
			return returnString;
		}catch(Exception e){
			logger.error("Exception in nullsafe", e);
		}
		return "";
	}
	
	public static String nullSafe(String givenString, String defaultString){
		String returnString;
		try{
			returnString = null!=givenString && !givenString.isEmpty()?givenString.trim():defaultString;
			return returnString;
		}catch(Exception e){
			logger.error("Exception in nullsafe", e);
		}
		return "";
	}
	
	public static int getAge(String dob) {
		
		int years 	= 0;
		int month 	= 0;
		int date 	= 0;
		int age;
		String [] splitDob = dob.split("-");
		date 	= Integer.parseInt(splitDob[0]);
		month 	= Integer.parseInt(splitDob[1]);
		years 	= Integer.parseInt(splitDob[2]);
		age 	= MilesCalculationUtil.calculateAge(years, month, date);
		return age;
	}
	
	public static String replaceHtml(String givenString){
		String returnString = "";
		try{
			returnString = givenString.contains("&")?givenString.replace("&", "and").trim():givenString;
		}catch(Exception e){
			logger.error("Exception in replaceHtml", e);
		}
		return returnString;
	}
	
	public static String getJPNumber(String jpNumber){
		String jpNum = "";
		try{
			if(jpNumber!=null && !jpNumber.isEmpty() && jpNumber.length()>9 && jpNumber.startsWith("00")){
				jpNum = jpNumber.substring(2);
                        }else{
                        jpNum = jpNumber;
                        }
		}catch(Exception e){
			logger.error("Exception in getJPNumber", e);
		}
		return jpNum;
	}

	public static int getMilliSeconds(int days, int hours, int minutes, int seconds) {
		int milliSeconds = 0;
		try{
			days 	= 0!=days?(int) TimeUnit.DAYS.toMillis(days):0;   
			hours 	= 0!=hours?(int) TimeUnit.HOURS.toMillis(hours):0;   
			minutes = 0!=minutes?(int) TimeUnit.MINUTES.toMillis(minutes):0; 
			seconds = 0!=seconds?(int) TimeUnit.SECONDS.toMillis(seconds):0;
			milliSeconds = days+hours+minutes+seconds;
		}catch(Exception e){
			logger.error("Exception in getMilliSeconds", e);
		}
		return milliSeconds;
	}

	public static String checkStringLength(String name) {
		name = !name.isEmpty() && name.length() > 15 ? name.substring(0, 14) : name;
		return name;
	}
	public static BigDecimal bigdecnullSafe(BigDecimal givenString, BigDecimal defaultString){
		BigDecimal returnString = null;
		try{
//			returnString =
                                if(null!=givenString){
                                returnString=givenString;
                                
                                }else{
                                returnString=defaultString;
                                }
		}catch(Exception e){
			logger.error("Exception in nullsafe", e);
		}
		return returnString;
	}
	

	
}
