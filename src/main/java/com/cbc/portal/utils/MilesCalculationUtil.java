package com.cbc.portal.utils;

import java.util.Date;

import org.apache.log4j.Logger;

/**
 * 
 * @author abhishek.m
 *
 */
public class MilesCalculationUtil {
	
	private MilesCalculationUtil(){
		
	}
	
	private static Logger logger = Logger.getLogger(MilesCalculationUtil.class.getName());
	
	/**
	 * 
	 * @param spends
	 * @param perSpendsValue
	 * @param baseJPMiles
	 * @return
	 */
	public static double calculateMileEarnedOnSpends(double spends, double perSpendsValue, double baseJPMiles){
		double milesEarnedOnSpends = 0;
		try{
			milesEarnedOnSpends = (spends/perSpendsValue) * baseJPMiles;
		}catch(Exception e){
			logger.error("@@@@ Exception in MilesCalculationUtil calculateMileEarnedOnSpends() :",e);	
		}
		return milesEarnedOnSpends;
	}
	
	/**
	 * 
	 * @param milesEarnedOnSpends
	 * @param welcomeBonus
	 * @param activationBonus
	 * @param thresholdBonus
	 * @param JPChannel
	 * @param spendVal
	 * @param thresholdValue
	 * @return
	 */
	public static double calculateTotalMiles(double milesEarnedOnSpends, double welcomeBonus, double activationBonus, double thresholdBonus, double JPChannel, double spendVal, double thresholdValue){
		double totalMiles = 0;
		try{
			if(thresholdValue>0 && spendVal >= thresholdValue)
				totalMiles = milesEarnedOnSpends + welcomeBonus + activationBonus + thresholdBonus + JPChannel;
			else
				totalMiles = milesEarnedOnSpends + welcomeBonus + activationBonus + JPChannel;
		}catch(Exception e){  
			logger.error("@@@@ Exception in MilesCalculationUtil calculateTotalMiles() :",e);	
		}
		return totalMiles;
	}
	
	/**
	 * 
	 * @param totalMiles
	 * @return
	 */
	public static double calculateFreeTickets(double totalMiles){
		double freeTickets = 0;
		try{
			freeTickets = Math.floor(totalMiles/5000.00);
		}catch(Exception e){
			logger.error("@@@@ Exception in MilesCalculationUtil calculateFreeTickets() :",e);	
		}
		return freeTickets;
	}
	
	/**
	 * 
	 * @param years
	 * @param month
	 * @param days
	 * @return
	 */
	public static int calculateAge(int years, int month, int days){
		int age = 0;
		try{
			Date now = new Date();
			   int nowMonth = now.getMonth()+1;
			   int nowYear = now.getYear()+1900;
			   age = nowYear - years;

			   if (month > nowMonth) {
				   age--;
			   }
			   else if (month == nowMonth) {
			       int nowDay = now.getDate();

			       if (days > nowDay) {
			    	   age--;
			       }
			      
			   }

		}catch(Exception e){
			logger.error("@@@@ Exception in MilesCalculationUtil calculateAge() :",e);	
		}
		return age;
	}

}
