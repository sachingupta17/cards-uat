package com.cbc.portal.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Properties;
import javax.net.ssl.HttpsURLConnection;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.log4j.Logger;

public class SSLContextUtil {
	
	private SSLContextUtil(){
		
	}
	
	private static Logger logger = Logger.getLogger(SSLContextUtil.class.getName());
	
	public static void sslManager(){
//		Properties systemProps = System.getProperties();
//		try(InputStream is= new FileInputStream(systemProps.getProperty( "javax.net.ssl.trustStore"))){	
//				
//			KeyStore keyStore = KeyStore.getInstance("JKS");
//			keyStore.load(is,systemProps.getProperty( "javax.net.ssl.trustStorePassword").toCharArray());
//			TrustManagerFactory tmf=TrustManagerFactory.getInstance("SunX509");
//			tmf.init(keyStore);
//			SSLContext ssl=SSLContext.getInstance("TLS");
//			KeyManagerFactory kmf=KeyManagerFactory.getInstance("SunX509");
//			kmf.init(keyStore, systemProps.getProperty( "javax.net.ssl.trustStorePassword").toCharArray());
//			SecureRandom secureRandom=new SecureRandom();
//			ssl.init(kmf.getKeyManagers(), tmf.getTrustManagers(), secureRandom);
                       
                        
//			}
//		}catch(Exception e){
//			logger.error("@@@@ Exception in SSLContextUtil at sslManager() ",e);
//		}

                //New code by Parvati on 7th may
                try{ 
    
                    SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
                    URL url = new URL("https://gridserver:3049/cgi-bin/ls.py");
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setSSLSocketFactory(sslsocketfactory);
                    InputStream inputstream = conn.getInputStream();
                    InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
                    BufferedReader bufferedreader = new BufferedReader(inputstreamreader);

                    String string = null;
                    while ((string = bufferedreader.readLine()) != null) {
                        logger.info("Received " + string);
                    }
  
                    }catch(Exception e){
                  
                    logger.error("@@@@ Exception in SSLContextUtil at sslManager() ",e);
                    }
		
	}
}
