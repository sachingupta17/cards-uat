package com.cbc.portal.utils;

import com.google.gson.Gson;

public class GsonUtil {

private GsonUtil(){
		
	}
	public static String toJson(Object obj) {
		return new Gson().toJson(obj);
	}
}
