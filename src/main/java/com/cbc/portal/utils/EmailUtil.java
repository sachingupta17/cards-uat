package com.cbc.portal.utils;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;



@Component
public class EmailUtil {

	private static final Logger logger = Logger.getLogger(EmailUtil.class.getName());
	
	@Autowired
	private JavaMailSender mailSender;
	
	public void sendEmail(String from, String to, String[] strArray, String sub, String msg) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
			message.setFrom(from);
			message.setTo(to);
			message.setCc(strArray);
			message.setSubject(sub);
			
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(msg, "text/html; charset=utf-8");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			
			mimeMessage.setContent(multipart);
			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			logger.error("@@@ Exception in EmailUtil at sendEmail() ", e);
		}
		
	}

}
