package com.cbc.portal.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationPropertiesUtil {
	
	@Value("${application.cbc.application_URL}")
	private String  applicationURL;
	@Value("${hp_title}")
	private String hpTitle;
	@Value("${hp_description}")
	private String hpDescription;
	@Value("${hp_keyword}")
	private String hpKeyword;
	
	@Value("${rc_title}")
	private String rcTitle;
	@Value("${rc_description}")
	private String rcDescription;
	@Value("${rc_keyword}")
	private String rcKeyword;
	
	@Value("${ne_title}")
	private String neTitle;
	@Value("${ne_description}")
	private String neDescription;
	@Value("${ne_keyword}")
	private String neKeyword;
	
	@Value("${cd_title}")
	private String cdTitle;
	@Value("${cd_description}")
	private String cdDescription;
	@Value("${cd_keyword}")
	private String cdKeyword;
	
	@Value("${cc_title}")
	private String ccTitle;
	@Value("${cc_description}")
	private String ccDescription;
	@Value("${cc_keyword}")
	private String ccKeyword;
	
	@Value("${af_title}")
	private String afTitle;
	@Value("${af_description}")
	private String afDescription;
	@Value("${af_keyword}")
	private String afKeyword;
	
	@Value("${ra_title}")
	private String raTitle;
	@Value("${ra_description}")
	private String raDescription;
	@Value("${ra_keyword}")
	private String raKeyword;
	
	@Value("${rd_title}")
	private String rdTitle;
	@Value("${rd_description}")
	private String rdDescription;
	@Value("${rd_keyword}")
	private String rdKeyword;
	
	
	public String getApplicationURL(){
		applicationURL=(null!=applicationURL)?applicationURL.trim():"";
		return applicationURL;
	}
	public String getHpTitle(){
		hpTitle=(null!=hpTitle)?hpTitle.trim():"";
		return hpTitle;
	}
	public String getHpDescription(){
		hpDescription=(null!=hpDescription)?hpDescription.trim():"";
		return hpDescription;
	}
	public String getHpKeyword(){
		hpKeyword=(null!=hpKeyword)?hpKeyword.trim():"";
		return hpKeyword;
	}
	public String getRcTitle(){
		rcTitle=(null!=rcTitle)?rcTitle.trim():"";
		return rcTitle;
	}
	public String getRcDescription(){
		rcDescription=(null!=rcDescription)?rcDescription.trim():"";
		return rcDescription;
	}
	public String getRcKeyword(){
		rcKeyword=(null!=rcKeyword)?rcKeyword.trim():"";
		return rcKeyword;
	}
	public String getNeTitle(){
		neTitle=(null!=neTitle)?neTitle.trim():"";
		return neTitle;
	}
	public String getNeDescription(){
		neDescription=(null!=neDescription)?neDescription.trim():"";
		return neDescription;
	}
	public String getNeKeyword(){
		neKeyword=(null!=neKeyword)?neKeyword.trim():"";
		return neKeyword;
	}
	public String getCdTitle(){
		cdTitle=(null!=cdTitle)?cdTitle.trim():"";
		return cdTitle;
	}
	public String getCdDescription(){
		cdDescription=(null!=cdDescription)?cdDescription.trim():"";
		return cdDescription;
	}
	public String getCdKeyword(){
		cdKeyword=(null!=cdKeyword)?cdKeyword.trim():"";
		return cdKeyword;
	}
	public String getCcTitle(){
		ccTitle=(null!=ccTitle)?ccTitle.trim():"";
		return ccTitle;
	}
	public String getCcDescription(){
		ccDescription=(null!=ccDescription)?ccDescription.trim():"";
		return ccDescription;
	}
	public String getCcKeyword(){
		ccKeyword=(null!=ccKeyword)?ccKeyword.trim():"";
		return ccKeyword;
	}
	public String getAfTitle(){
		afTitle=(null!=afTitle)?afTitle.trim():"";
		return afTitle;
	}
	public String getAfDescription(){
		afDescription=(null!=afDescription)?afDescription.trim():"";
		return afDescription;
	}
	public String getAfKeyword(){
		afKeyword=(null!=afKeyword)?afKeyword.trim():"";
		return afKeyword;
	}
	public String getRaTitle(){
		raTitle=(null!=raTitle)?raTitle.trim():"";
		return raTitle;
	}
	public String getRaDescription(){
		raDescription=(null!=raDescription)?raDescription.trim():"";
		return raDescription;
	}
	public String getRaKeyword(){
		raKeyword=(null!=raKeyword)?raKeyword.trim():"";
		return raKeyword;
	}
	public String getRdTitle(){
		rdTitle=(null!=rdTitle)?rdTitle.trim():"";
		return rdTitle;
	}
	public String getRdDescription(){
		rdDescription=(null!=rdDescription)?rdDescription.trim():"";
		return rdDescription;
	}
	public String getRdKeyword(){
		rdKeyword=(null!=rdKeyword)?rdKeyword.trim():"";
		return rdKeyword;
	}
	
}
