/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 *
 */
public class SendGridMail {

    private Logger logger = Logger.getLogger(SendGridMail.class.getName());

    public void sendMail(String url, String api_key, HashMap<String, String> saveColumns, String campaignId, String emailId, String ref_id, String simplicaUserName, String simplicaPassword) {
        try {
            JsonObject json = new JsonObject(), from_obj = new JsonObject(), to_obj = new JsonObject(),
                    personalization = new JsonObject(), dynamic_template_data;
            JsonArray personalizations = new JsonArray(), to_array = new JsonArray();
            to_obj.addProperty("email", emailId);
            to_array.add(to_obj);
            personalization.add("to", to_array);

            from_obj.addProperty("email", "updates@alerts.intermiles.com");
            from_obj.addProperty("name", "Intermiles");
            json.add("from", from_obj);

            Gson gson = new Gson();
            dynamic_template_data = gson.toJsonTree(saveColumns).getAsJsonObject();

            personalization.add("dynamic_template_data", dynamic_template_data);

            personalizations.add(personalization);
            json.add("personalizations", personalizations);
            json.addProperty("template_id", campaignId);

            json.addProperty("prj_code", "Cards");
            json.addProperty("ref_id", ref_id);
            json.addProperty("sg_key", api_key);

            System.out.println("json:" + json.toString());
            callApi(url, json.toString(), simplicaUserName, simplicaPassword);
        } catch (Exception e) {
            logger.error("Exception in sendGridEmailer: " + e.getMessage());
            e.printStackTrace();
        }
    }
    private String callApi(String api_url, String request, String simplicaUserName, String simplicaPassword) {
        HttpURLConnection urlConn = null;
        OutputStream out = null;
        InputStream in = null;
        try {
            URL url = new URL(api_url);
            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setRequestMethod("POST");
            urlConn.setDoOutput(true);
            urlConn.setRequestProperty("Content-Type", "application/json");
            urlConn.setRequestProperty("Authorization", "Basic " + Base64.getEncoder().encodeToString((simplicaUserName + ":" + simplicaPassword).getBytes()));

            urlConn.connect();
            OutputStreamWriter writer = new OutputStreamWriter(urlConn.getOutputStream(), "UTF-8");
            writer.write(request);
            writer.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
            StringBuffer jsonString = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            br.close();
            System.out.println(urlConn.getResponseCode());
            urlConn.disconnect();
            logger.info("jsonString>>>>>" + jsonString);
            logger.info("callresponse>>>>>" + urlConn.getResponseMessage());

            JSONObject jsonObj = JSONObject.fromObject(jsonString.toString());
            System.out.println("jsonObj:" + jsonObj.toString());
//                            System.out.println("statuss:"+jsonObj.getString("stauts"));
//                        return urlConn.getResponseCode() + "";
            return jsonObj.toString();
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException Exception in CallTranscatApi: " + e.getMessage());
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            logger.error("UnsupportedEncodingException Exception in CallTranscatApi: " + e.getMessage());
            e.printStackTrace();
        } catch (ProtocolException e) {
            logger.error("ProtocolException Exception in CallTranscatApi: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("IOException Exception in CallTranscatApi: " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            logger.error("Exception in CallTranscatApi: " + e.getMessage());
            e.printStackTrace();
        } finally {
            logger.info("Inside Finally callApi");
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                }
            }
            if (urlConn != null) {
                urlConn.disconnect();
            }
        }
        return "";
    }

}