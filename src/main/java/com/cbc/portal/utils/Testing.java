/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;
import org.json.JSONException;
import org.json.JSONObject;

import com.jcraft.jsch.Logger;

import javax.net.ssl.TrustManager;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.OutputStream;
import java.security.Security;
/**
 *
 * @author Aravind E
 */
public class Testing {

    public  void test() throws JSONException, NoSuchAlgorithmException, KeyManagementException, Exception{
    
    try {
        doTrustToCertificates();
//             SSLContext sc = SSLContext.getInstance("SSL");
//           sc.init(null, null, new java.security.SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		URL url = new URL("https://www.test.transuniondecisioncentre.co.in/DC/TUDCGenericAPI/API/ICICICCEAS/Encryptdetails/");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
//                conn.setDefaultSSLSocketFactory(sc.getSocketFactory());
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");

		String input = "{\"UserID\":\"ICICI_CC_JetAirways\", \"Password\":\"Password@123\"}";
                
//              System.out.println("input"+input);

		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

		String output;
//		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
//			System.out.println(output);
                         JSONObject jsonObj = new JSONObject(output);
//                           System.out.println("jsonObj"+jsonObj);
//                           System.out.println("User Nmame"+jsonObj.getString("EncryptedUserId"));
//                           System.out.println("Password"+jsonObj.getString("EncryptedPassword"));
                          
                           
		}
               
              

		conn.disconnect();

	  } catch (MalformedURLException e) {

		e.printStackTrace();

	  } catch (IOException e) {

		e.printStackTrace();

	 }
    
    }
   
    
    public void doTrustToCertificates() throws Exception {
      Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        TrustManager[] trustAllCerts = new TrustManager[]{
    new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }
        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            //No need to implement. 
        }
        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            //No need to implement. 
        }
    }
};
// Install the all-trusting trust manager
try {
    SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
} catch (Exception e) {
	e.printStackTrace();
    System.out.println(e);
}
    }
}
