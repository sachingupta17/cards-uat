/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.utils;

import com.cbc.portal.controller.CompareCardController;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import com.cbc.portal.service.impl.CommonServiceImpl;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.Logger;

/**
 *
 * @author Aravind E
 */
public class OTPGeneration {
    
    private Logger logger = Logger.getLogger(OTPGeneration.class.getName());
	
    public String OTPGenertion(){
    
                 int randomPin   =(int)(Math.random()*900000)+1000;
		String otp  =String.valueOf(randomPin);
		      System.out.println("otp   :"+otp);
    
        return otp;
    }
    
    public  String SendSMStoUser(String mobileNumber,String otpValue){
    
      String requestUrl1 ="http://121.241.247.222:7501/failsafe/HttpLink";
           
         
        String smsApplicationId = "635348";
        String msg = "";
        String smsPin = "jet@12";
        String message = "OTP is "+otpValue+" for your card application on cards.intermiles.com. Do not share the OTP with anyone for security reasons.";
//        String mobile = "8291493992";

        try {

            msg = URLEncoder.encode(message, "utf-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("@@@@ Exception in AmexApplicationFormServiceImpl at buildSMSForAmex() UnsupportedEncodingException :", e);
        }
//        String requestUrl = requestUrl1.trim() + "?aid=" + smsApplicationId.trim() + "&pin=" + smsPin.trim() + "&mnumber=" + mobileNumber + "&message=" + msg;
//
        String responeMsg = "";
//        try {
//            URL url = new URL(requestUrl);
//            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
//            responeMsg = uc.getResponseMessage();
//            System.out.println("responeMsg" + responeMsg);
//            uc.disconnect();
//        } catch (Exception e) {
//            logger.error("@@@@ Exception in AmexApplicationFormServiceImpl at buildSMSForAmex() :", e);
//        }

      try{
          new Testing().doTrustToCertificates();
            String requestUrl = "https://api.mgage.solutions/SendSMS/sendmsg.php?uname=ccjetapi&pass=b~7Qn$4X&send=JETPRV&dest="+mobileNumber+"&msg="+msg;
           System.out.println("=======================================================");
          System.out.println("requestUrl   "+requestUrl);
       
            
            URL url = new URL(requestUrl);
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            responeMsg = uc.getResponseMessage();
             uc.disconnect();
        } catch (Exception e) {
            logger.error("@@@@ Exception in SmsService at sendSms() :", e);
            System.out.println("dsnjksfkjsfk"+e);
        }
//      


    
    return responeMsg;
    }
    
    
     public void callMac3service(String strJpplno,String resetUrl) {
        Date date = new Date();
          String strDatetime = null;
           try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                        System.out.println(dateFormat.format(date));
                        strDatetime = dateFormat.format(date);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
         
        String request = null;
        String result = "";
        String ApiId = "9a14447e-c9c9-4bef-91dc-719aeb8aa9fa";
        String ApiKey = "H3fbmUOABcN3LYro9XBWFXeSndCeXMrO2vG8Yu5pWJM=";
        String json = "";
        json = json + "{ UserName :'" +strJpplno+ "',Module :'o',ru :'/login'}";
        URL url;
        String requestContentBase64String = "";
        HttpURLConnection urlConnection = null;
        
        try {
             url = new URL(resetUrl);
            logger.info("Reset Password URL  :"+url);
            
            logger.info("Reset Password Request  :"+json);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setConnectTimeout(5000);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            
            Date epochStart = new Date(0);

            String requestTimeStamp = "" + ((new Date().getTime() - epochStart.getTime()) / 1000);
            
            String nonce = java.util.UUID.randomUUID().toString().replace("-", "");

            if (json != null) {
                byte[] content = json.getBytes();
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] requestContentHash = md.digest(content);
                requestContentBase64String = Base64.getEncoder().encodeToString(requestContentHash);
            }
            String signatureRawData = String.format("%s%s%s%s%s%s", ApiId, urlConnection.getRequestMethod(),
                    url.toString().toLowerCase(), requestTimeStamp, nonce, requestContentBase64String);

            //System.out.println("data: " + signatureRawData);
            byte[] secretKeyByteArray = ApiKey.getBytes();
            byte[] signature = signatureRawData.getBytes();
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secretKeyByteArray, "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] signatureBytes = sha256_HMAC.doFinal(signature);
            String requestSignatureBase64String = Base64.getEncoder().encodeToString(signatureBytes);

            String header = String.format("amx %s:%s:%s:%s", ApiId, requestSignatureBase64String, nonce,
                    requestTimeStamp);
            urlConnection.setRequestProperty("Authorization", header);

            OutputStream os = urlConnection.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();

            InputStream in = (InputStream) urlConnection.getInputStream();
            InputStreamReader reader = new InputStreamReader(in);

            int data = reader.read();
            while (data != -1) {
                char current = (char) data;
                result += current;
                data = reader.read();
            
            logger.info("Email sent response :"+data);
            }
            logger.info("Reset Password Request  :"+json);
            
            
        } catch (MalformedURLException e) {
            System.out.println(e);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e);
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            System.out.println(e);
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }

    }
    
    
    
    
    
    
    
    
    
    
    
    
}
