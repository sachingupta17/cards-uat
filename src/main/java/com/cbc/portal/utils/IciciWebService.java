/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import javax.ws.rs.core.MediaType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

//import com.auth0.jwt.internal.com.fasterxml.jackson.databind.JsonNode;
import com.cbc.portal.beans.IciciBean;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

/**
 *
 * @author Aravind E
 */
//@Component
//@Transactional(readOnly = true)
public class IciciWebService {

	
	 	@Value("${application.pii.encryption.strSoapURL1}")
	   	String strSoapURL1;
	   	@Value("${application.pii.encrypt.strSOAPAction1}")
	   	String soapActEnc;
	   	
    private String key = "", value = "", content = "", dob = "", iciciRequest = "", iciciRespose = "";
    private BigDecimal income;
    private Logger logger = Logger.getLogger(IciciWebService.class.getName());
    private String request;
    private String response;
   private String relationship="",totalexperience="";
//--
//    @Value("${application.icici.channel_Type}")
//    private String channelType;

    //------
    public String getTransUnionDetails(String UserID, String Password, String transURL, String newApplcationUrl, IciciBean bean, String channalType, String strSoapURL1, String soapActEnc) throws IOException, NoSuchAlgorithmException, KeyManagementException, JSONException, Exception {
//     ,String address3
         String userid1 = "", password1 = "";
//        ClientConfig clientConfig = new DefaultClientConfig();
//
//        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
//
//        Client client = Client.create(clientConfig);
//        WebResource webResource = client
//                .resource(transURL);
//        ClientResponse response = null;
//        String content = "{ \"UserID\":\"" + UserID + "\", \"Password\":\"" + Password + "\" }";
//
//        response = webResource
//                .type(MediaType.APPLICATION_JSON).post(ClientResponse.class, content);
//        String output = response.getEntity(String.class);
//
//        ObjectMapper obmentityuser = new ObjectMapper();
//
//        LinkedHashMap<String, String> tabledata_HM = new LinkedHashMap<String, String>();
//
//        tabledata_HM = (LinkedHashMap<String, String>) obmentityuser.readValue(output, Map.class);
//        for (Map.Entry<String, String> entry : tabledata_HM.entrySet()) {
//
//            if (entry.getKey().equals("EncryptedUserId")) {
//
//                userid1 = entry.getValue();
//            }
//            if (entry.getKey().equals("EncryptedPassword")) {
//                password1 = entry.getValue();
//            }
//
//        }

//new code

try{
    new Testing().doTrustToCertificates();
    //"https://www.test.transuniondecisioncentre.co.in/DC/TUDCGenericAPI/API/ICICICCEAS/Encryptdetails/"
          URL url = new URL(transURL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");

//		String input = "{\"UserID\":\"ICICI_CC_JetAirways\", \"Password\":\"Password@123\"}";
		String input = "{\"UserID\":\"" + UserID + "\", \"Password\":\""+ Password + "\"}";
                
//              System.out.println("input"+input);

		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();

		if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ conn.getResponseCode());
		}

		  BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
//    System.out.println("br" + br);
    String output;
//    System.out.println("Output from Server .... \n");
    while ((output = br.readLine()) != null) {
//        System.out.println(output);
        JSONObject jsonObj = new JSONObject(output);

        userid1 = jsonObj.getString("EncryptedUserId");
        password1 = jsonObj.getString("EncryptedPassword");
        

    }
    
    	//logger.info("transUser --  "+userid1+" --- transPwd --- "+password1);
              

		conn.disconnect();
      

	  } catch (MalformedURLException e) {

//		e.printStackTrace();

        

	  } catch (IOException e) {

//		e.printStackTrace();

	 }

        String startDateString = bean.getDateOfBirth().replaceAll("-", "/");
        SimpleDateFormat fromUser = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
        String salarywithotherbank="";
        String salwithotherbank="";
       
        if(bean.getSalaryAccountOpened()==null || bean.getSalaryAccountOpened().isEmpty()){
          
        salwithotherbank="";
        }else{
        salwithotherbank=bean.getSalaryAccountOpened();
        salarywithotherbank=salwithotherbank.replaceAll("\\s", "");
           
        
        }

        try {
            dob = myFormat.format(fromUser.parse(startDateString));
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(IciciWebService.class.getName()).log(Level.SEVERE, null, ex);
        }

        String gender = bean.getGender();
        String iciciRealtionship = "";
        String empstatus = bean.getEmploymentType();
        if (gender.equals("0")) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        int salary = bean.getMonthlyIncome().intValue();
        salary = salary / 12;

        if (empstatus.equalsIgnoreCase("Self Employed-Professional") || empstatus.equalsIgnoreCase("Self Employed-Business")) {
            empstatus = "Selfemployed";
            income = bean.getMonthlyIncome();
        } else {
            empstatus = "Salaried";
            income = new BigDecimal(salary);
        }
        if(bean.getIciciBankRelationship().equalsIgnoreCase("No Relationship")){
            
            relationship="Norelationship";
        }else if(bean.getIciciBankRelationship().equalsIgnoreCase("Salary Account")){
            relationship="Salary";
        
        }else if(bean.getIciciBankRelationship().equalsIgnoreCase("Current Account")){
            relationship="Current";
        }else if(bean.getIciciBankRelationship().equalsIgnoreCase("Savings Account")){
            relationship="Savings";
        }else if(bean.getIciciBankRelationship().equalsIgnoreCase("Loan Account")){
            relationship="Loan";
        }
        
        String totalexp=bean.getTotalExperience();
       
        String mo = null, years = null;
         if("".equals(totalexp)){
            mo="0";
            years="0";
         totalexperience=years+"."+mo;
        }
         
  else if(totalexp.contains("years") || totalexp.contains("year")){
            
        
         totalexp=totalexp.substring(0,totalexp.indexOf("m")).trim();
            if(!totalexp.contains("s")){
             totalexp=totalexp.replace(" ", "");
//             String months=totalexp.trim().substring(0, totalexp.indexOf("m"));
             String months=totalexp;
          
                
             mo=months.substring(months.indexOf("r")+1).trim();
             years=months.substring(0, months.indexOf("year"));
            totalexperience=years+"."+mo;
            
            }
              if(totalexp.contains("s")){
             totalexp=totalexp.replace(" ", "");
//             String months=totalexp.trim().substring(0, totalexp.indexOf("m"));
             String months=totalexp;
             mo=months.substring(months.indexOf("s")+1).trim();
            years=months.substring(0, months.indexOf("years"));
            totalexperience=years+"."+mo;
            }
          
        }
         if((Integer.parseInt(mo)) >=7)
           {
               int y=(Integer.parseInt(years));
               int totyear=y+1;
               totalexperience=String.valueOf(totyear);
           }else{
           totalexperience=years;
           }
        
        
        

        return transUnionformDetails(newApplcationUrl, userid1, password1, channalType, bean.getFname(), bean.getMname(), bean.getLname(), gender, dob, bean.getAddress(),
                bean.getAddress2(), bean.getAddress3(), bean.getCity(), bean.getPinCode(), bean.getState1(), bean.getStd(), bean.getPhone(), bean.getMobile(), bean.getPancard(), relationship,
                bean.getICICIBankRelationShipNo(), empstatus, bean.getCompanyName(), bean.getSalaryBankWithOtherBank(), totalexperience, income, salarywithotherbank, strSoapURL1, soapActEnc);

    }

    public String transUnionformDetails(String newApplcationUrl, String UserID, String Password,
            String ChannelType, String ApplicantFirstName, String ApplicantMiddleName, String ApplicantLastName, String Gender,
            String DateOfBirth, String ResidenceAddress1, String ResidenceAddress2, String ResidenceAddress3, String City, String ResidencePincode,
            String ResidenceState, String STDCode, String ResidencePhoneNumber, String ResidenceMobileNo, String PanNo, String ICICIBankRelationship,
            String ICICIRelationshipNumber, String CustomerProfile, String CompanyName, String SalaryAccountWithOtherBank, String Total_Exp,
            BigDecimal Income, String SalaryAccountOpened,String strSoapURL1,String soapActEnc) {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

        Client client = Client.create(clientConfig);
        WebResource webResource = client
                .resource(newApplcationUrl);
        ClientResponse response = null;
        
        content = "{ \"UserID\":\"" + UserID + "\", \"Password\":\"" + Password + "\",\"ChannelType\":\"" + ChannelType + "\",\"ApplicantFirstName\":\"" + ApplicantFirstName + "\","
                + "\"ApplicantMiddleName\":\"" + ApplicantMiddleName + "\",\"ApplicantLastName\":\"" + ApplicantLastName + "\",\"Gender\":\"" + Gender + "\",\"DateOfBirth\":\"" + DateOfBirth + "\","
                + "\"ResidenceAddress1\":\"" + ResidenceAddress1 + "\",\"ResidenceAddress2\":\"" + ResidenceAddress2 + "\",\"ResidenceAddress3\":\"" + ResidenceAddress3 + "\",\"City\":\"" + City + "\","
                + "\"ResidencePincode\":\"" + ResidencePincode + "\",\"ResidenceState\":\"" + ResidenceState + "\",\"STDCode\":\"" + STDCode + "\",\"ResidencePhoneNumber\":\"" + ResidencePhoneNumber + "\",\"ResidenceMobileNo\":\"" + ResidenceMobileNo + "\","
                + "\"PanNo\":\"" + PanNo + "\",\"ICICIBankRelationship\":\"" + ICICIBankRelationship + "\",\"ICICIRelationshipNumber\":\"" + ICICIRelationshipNumber + "\",\"CustomerProfile\":\"" + CustomerProfile + "\",\"CompanyName\":\"" + CompanyName + "\","
                + "\"SalaryAccountWithOtherBank\":\"" + SalaryAccountWithOtherBank + "\",\"Total_Exp\":\"" + Total_Exp + "\",\"Income\":\"" + Income + "\",\"SalaryAccountOpened\":\"" + SalaryAccountOpened + "\"  }";
        
//        content = "{ \"UserID\":\"" + UserID + "\", \"Password\":\"" + Password + "\",\"ChannelType\":\"" + ChannelType + "\",\"ApplicantFirstName\":\"" + ApplicantFirstName + "\","
//                + "\"ApplicantMiddleName\":\"" + ApplicantMiddleName + "\",\"ApplicantLastName\":\"" + ApplicantLastName + "\",\"Gender\":\"" + Gender + "\",\"DateOfBirth\":\"" + DateOfBirth + "\","
//                + "\"ResidenceAddress1\":\"" + ResidenceAddress1 + "\",\"ResidenceAddress2\":\"" + ResidenceAddress2 + "\",\"ResidenceAddress3\":\"" + ResidenceAddress3 + "\",\"City\":\"" + City + "\","
//                + "\"ResidencePincode\":\"" + ResidencePincode + "\",\"ResidenceState\":\"" + ResidenceState + "\", \"ApplicationNumber\":\""+ applnNo + "\", \"SalaryAcOpenDate\":\""+SalaryAcOpenDate+ "\", \"ApplicationDate\":\""+ApplicationDate+ "\", \"STDCode\":\"" + STDCode + "\",\"ResidencePhoneNumber\":\"" + ResidencePhoneNumber + "\",\"ResidenceMobileNo\":\"" + ResidenceMobileNo + "\","
//                + "\"PanNo\":\"" + PanNo + "\",\"ICICIBankRelationship\":\"" + ICICIBankRelationship + "\",\"CustomerProfile\":\"" + CustomerProfile + "\",\"CompanyName\":\"" + CompanyName + "\","
//                + "\"SalaryAccountWithOtherBank\":\"" + SalaryAccountWithOtherBank + "\", \"MonthlyIncome\":\"" + monInc + "\", \"Income\":\"" + Income + "\"  }";
        
        String printContentReq = "{ \"UserID\":\"" + UserID + "\", \"Password\":\"" + Password + "\",\"ChannelType\":\"" + ChannelType + "\",\"ApplicantFirstName\":\"" + getEncryptedData(ApplicantFirstName,strSoapURL1,soapActEnc) + "\","
                + "\"ApplicantMiddleName\":\"" + getEncryptedData(ApplicantMiddleName,strSoapURL1,soapActEnc) + "\",\"ApplicantLastName\":\"" + getEncryptedData(ApplicantLastName,strSoapURL1,soapActEnc) + "\",\"Gender\":\"" + getEncryptedData(Gender,strSoapURL1,soapActEnc) + "\",\"DateOfBirth\":\"" + getEncryptedData(DateOfBirth,strSoapURL1,soapActEnc) + "\","
                + "\"ResidenceAddress1\":\"" + ResidenceAddress1 + "\",\"ResidenceAddress2\":\"" + ResidenceAddress2 + "\",\"ResidenceAddress3\":\"" + ResidenceAddress3 + "\",\"City\":\"" + City + "\","
                + "\"ResidencePincode\":\"" + ResidencePincode + "\",\"ResidenceState\":\"" + ResidenceState + "\",\"STDCode\":\"" + STDCode + "\",\"ResidencePhoneNumber\":\"" + ResidencePhoneNumber + "\",\"ResidenceMobileNo\":\"" + getEncryptedData(ResidenceMobileNo,strSoapURL1,soapActEnc) + "\","
                + "\"PanNo\":\"" + getEncryptedData(PanNo,strSoapURL1,soapActEnc) + "\",\"ICICIBankRelationship\":\"" + ICICIBankRelationship + "\",\"ICICIRelationshipNumber\":\"" + ICICIRelationshipNumber + "\",\"CustomerProfile\":\"" + CustomerProfile + "\",\"CompanyName\":\"" + CompanyName + "\","
                + "\"SalaryAccountWithOtherBank\":\"" + SalaryAccountWithOtherBank + "\",\"Total_Exp\":\"" + Total_Exp + "\",\"Income\":\"" + Income + "\",\"SalaryAccountOpened\":\"" + SalaryAccountOpened + "\"  }";
        
        logger.info("requset service@@@@@@@" + printContentReq);
        response = webResource
                .type(MediaType.APPLICATION_JSON).post(ClientResponse.class, content);

        String output1 = response.getEntity(String.class);
        
        logger.info("Transunion Response --- " + output1);

        return output1;
    }

//    public String ICICIService(String iciciURL, IciciBean bean, int id,String iciciUsername,String iciciPassword,String apiCompanyName,String productName,String modeOfLead,
//                   String authorization,String contentType, String cardname) {
//System.out.println("cardname"+cardname);
//        System.out.println("address1->"+bean.getAddress());
//         System.out.println("address2->"+bean.getAddress2());
//         System.out.println("address3->"+bean.getAddress3());
//         System.out.println("pincode->"+bean.getPinCode());
//         System.out.println("state->"+bean.getState1());
//         System.out.println("city->"+bean.getCity());
//        String fullName = bean.getFname() + " " + bean.getLname();
//        String dateOfBirth = bean.getDateOfBirth().replaceAll("-", "/");
//        logger.info("--------ICICI----dateOfBirth-------->"+dateOfBirth);
//        String empType = bean.getEmploymentType();
//        String relationship = bean.getIciciBankRelationship();
//          if (empType.equals("Salaried")) 
//            empType = "salary";
//         else if (empType.equals("Self Employed-Professional")) 
//            empType = "self1";
//         else 
//           empType = "self2";
//
//        
//         if (relationship.equals("Salary Account")) {
//            relationship = "salary";
//        } else if (relationship.equals("Savings Account")) {
//            relationship = "current";
//        } else if (relationship.equals("Loan Account")) {
//            relationship = "loan";
//        } else if (relationship.equals("Current Account")) {
//            relationship = "current";
//        }else {
//            relationship = "norel";
//        }
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.set("authorization", authorization);
//        headers.set("content-type", contentType);
//        RestTemplate restTemplate = new RestTemplate();
//        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
//        map.add("username", iciciUsername);
//        map.add("Password", iciciPassword);
//        map.add("ApiCompanyName", apiCompanyName);
//        map.add("ApiProductName", productName);
//        map.add("ModeOfLead", modeOfLead);
//        map.add("Name", fullName);
//        map.add("EmailId", bean.getEmail());
//        map.add("Mobile", bean.getMobile());
//        map.add("City", bean.getCity());
//        map.add("CompanyName", bean.getCompanyName());
//        map.add("DOB", dateOfBirth);
//        map.add("TypeOfEmployment", empType);
//        map.add("RelationType", relationship);
//         map.add("IncomeSalary", "" + bean.getMonthlyIncome());
//         int salary = bean.getMonthlyIncome().intValue();
//            salary = salary / 12;
//            String monthly = "" + salary;
//            map.add("TakeHomeSalary", monthly);
////        if (empType.equalsIgnoreCase("Self Employed-Professional") || empType.equalsIgnoreCase("Self Employed-Business")) {
////            map.add("IncomeSalary", "" + bean.getMonthlyIncome());
////
////        } else {
////            int salary = bean.getMonthlyIncome().intValue();
////            salary = salary / 12;
////            String monthly = "" + salary;
////            map.add("TakeHomeSalary", monthly);
////        }
//        map.add("SearchEngine", "");
//        map.add("Campaign", "");
//        map.add("Adgroup", "");
//        map.add("Adtype", "");
//        map.add("Keyword", "");
//        map.add("Referer", "");
//        map.add("GclId", "");
//        map.add("Product", "");
//        map.add("Page", "");
//        map.add("Device", "");
//        map.add("UtmInfo", "");
//        map.add("CardType", cardname);
//        map.add("JpNumber", bean.getJetpriviligemembershipNumber());
//        map.add("JpplApplicatioId", "" + id);
//        map.add("Extra1", bean.getJetpriviligemembershipTier());
//        map.add("Extra2", "");
//        map.add("AddressLine1", bean.getAddress());
//        map.add("AddressLine2", bean.getAddress2());
//        map.add("AddressLine3", bean.getAddress3());
//        map.add("PinCode", bean.getPinCode());
//        map.add("JCity", bean.getCity());
//        map.add("State", bean.getState1());
//        logger.info("ICICI Request @@@@@@@@" + iciciURL);
//        HttpEntity<MultiValueMap<String, String>> icicirequest = new HttpEntity<MultiValueMap<String, String>>(map, headers);
//        logger.info("ICICI Request @@@@@@@@" + icicirequest);
//        ResponseEntity<String> iciciresponse = restTemplate.postForEntity(iciciURL, icicirequest, String.class);
//        logger.info("ICICI Response @@@@@@@@" + iciciresponse);
//        request = "" + icicirequest;
//        String res = iciciresponse.getBody().substring(1, iciciresponse.getBody().length());
//        response = res.substring(0, res.length() - 1);
//        return response;
//    }
    
	public String ICICIService(String iciciURL, IciciBean bean, int id, String iciciUsername, String iciciPassword,
			String apiCompanyName, String productName, String modeOfLead, String authorization, String contentType,
			String cardname,String strSoapURL1,String soapActEnc) {

		//String fullName = bean.getFname() + " " + bean.getLname();
		String dateOfBirth = bean.getDateOfBirth().replaceAll("-", "/");
		logger.info("--------ICICI----dateOfBirth-------->" + getEncryptedData(dateOfBirth,strSoapURL1,soapActEnc));
		String empType = bean.getEmploymentType();
		String relationship = bean.getIciciBankRelationship();
		if (empType.equals("Salaried"))
			empType = "salary";
		else if (empType.equals("Self Employed-Professional"))
			empType = "self1";
		else
			empType = "self2";

		if (relationship.equals("Salary Account")) {
			relationship = "salary";
		} else if (relationship.equals("Savings Account")) {
			relationship = "current";
		} else if (relationship.equals("Loan Account")) {
			relationship = "loan";
		} else if (relationship.equals("Current Account")) {
			relationship = "current";
		} else {
			relationship = "norel";
		}

		//HttpHeaders headers = new HttpHeaders();
		//headers.set("authorization", authorization);
		//headers.set("content-type", contentType);
		
	    //headers.set("content-type",MediaType.APPLICATION_JSON);
		
		//RestTemplate restTemplate = new RestTemplate();
		
		
		//MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		
		//JSONObject map = new JSONObject();
		
		int salary = bean.getMonthlyIncome().intValue();
		salary = salary / 12;
		String monthly = "" + salary;
		
		String icicReq = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +  
				  " <soap:Body> " +
				  " <InsertData xmlns='ICICI BANK -Hotleads-WebService'> " +
				  "    <FName>"+bean.getFname()+"</FName> " +
				  "    <LName>"+bean.getLname()+"</LName> " +
				   "   <Email>"+bean.getEmail()+"</Email> " +
				    "  <City>"+bean.getCity()+"</City> " +
				    "  <MobileNo>"+bean.getMobile()+"</MobileNo> " +
				    "  <AnnualIncome>"+bean.getMonthlyIncome()+"</AnnualIncome> " +
				    "  <data_status>"+bean.getGender()+"</data_status> " +
				    "  <relation>"+bean.getIciciBankRelationship()+"</relation> " +
				    "  <Offer_status></Offer_status> " +
				    "  <Product_type>"+productName+"</Product_type> " +
				    "  <Extrafreefield>"+empType+"</Extrafreefield> " +
				    "  <JpNumber>"+bean.getJpNum()+"</JpNumber> " +
				    "  <JpplApplicatioId>"+id+"</JpplApplicatioId> " +
				    "  <Jetextra1>"+monthly+"</Jetextra1> " +
				    "  <Jetextra2>"+bean.getSalaryBankWithOtherBank()+"</Jetextra2> " +
				    "  <camp>"+bean.getTotalExperience()+"</camp> " +
				    "  <product></product> " +
				    "  <search_eng></search_eng> " +
				    "  <adgroup></adgroup> " +
				    "  <keyword>"+bean.getSalaryAccountOpened()+"</keyword> " +
				    " <refe></refe>  " +
				    "  <extra1>"+apiCompanyName+"</extra1> " +
				    "  <extra2>"+bean.getDateOfBirth()+"</extra2> " +
				    "  <extra3>"+bean.getEmploymentType()+"</extra3> " +
				    "  <device></device> " +
				    "  <campaign>"+bean.getChannelType()+"</campaign> " +
				    "  <strid></strid> " +
				    "  <extra4>"+bean.getAddress()+"</extra4> " +
				    "  <extra5>"+bean.getAddress2()+"</extra5> " +
				    "  <extra6>"+bean.getAddress3()+"</extra6> " +
				    "  <extra7>"+bean.getPinCode()+"</extra7> " +
				    "  <extra8>"+bean.getState1()+"</extra8> " +
				    "  <partnername>MILES</partnername> " +
				    "  <campaigncode>MILES</campaigncode> " +
				    "</InsertData> " +
				  "</soap:Body> " +
			"	</soap:Envelope> " ;
		
		String icicReqPrint = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +  
				  " <soap:Body> " +
				  " <InsertData xmlns='ICICI BANK -Hotleads-WebService'> " +
				  "    <FName>"+getEncryptedData(bean.getFname(),strSoapURL1,soapActEnc)+"</FName> " +
				  "    <LName>"+getEncryptedData(bean.getLname(),strSoapURL1,soapActEnc)+"</LName> " +
				   "   <Email>"+getEncryptedData(bean.getEmail(),strSoapURL1,soapActEnc)+"</Email> " +
				    "  <City>"+bean.getCity()+"</City> " +
				    "  <MobileNo>"+getEncryptedData(bean.getMobile(),strSoapURL1,soapActEnc)+"</MobileNo> " +
				    "  <AnnualIncome>"+bean.getMonthlyIncome()+"</AnnualIncome> " +
				    "  <data_status>"+getEncryptedData(bean.getGender(),strSoapURL1,soapActEnc)+"</data_status> " +
				    "  <relation>"+bean.getIciciBankRelationship()+"</relation> " +
				    "  <Offer_status></Offer_status> " +
				    "  <Product_type>"+productName+"</Product_type> " +
				    "  <Extrafreefield>"+empType+"</Extrafreefield> " +
				    "  <JpNumber>"+bean.getJpNum()+"</JpNumber> " +
				    "  <JpplApplicatioId>"+id+"</JpplApplicatioId> " +
				    "  <Jetextra1>"+monthly+"</Jetextra1> " +
				    "  <Jetextra2>"+bean.getSalaryBankWithOtherBank()+"</Jetextra2> " +
				    "  <camp>"+bean.getTotalExperience()+"</camp> " +
				    "  <product></product> " +
				    "  <search_eng></search_eng> " +
				    "  <adgroup></adgroup> " +
				    "  <keyword>"+bean.getSalaryAccountOpened()+"</keyword> " +
				    " <refe></refe>  " +
				    "  <extra1>"+apiCompanyName+"</extra1> " +
				    "  <extra2>"+getEncryptedData(bean.getDateOfBirth(),strSoapURL1,soapActEnc)+"</extra2> " +
				    "  <extra3>"+bean.getEmploymentType()+"</extra3> " +
				    "  <device></device> " +
				    "  <campaign>"+bean.getChannelType()+"</campaign> " +
				    "  <strid></strid> " +
				    "  <extra4>"+bean.getAddress()+"</extra4> " +
				    "  <extra5>"+bean.getAddress2()+"</extra5> " +
				    "  <extra6>"+bean.getAddress3()+"</extra6> " +
				    "  <extra7>"+bean.getPinCode()+"</extra7> " +
				    "  <extra8>"+bean.getState1()+"</extra8> " +
				    "  <partnername>MILES</partnername> " +
				    "  <campaigncode>MILES</campaigncode> " +
				    "</InsertData> " +
				  "</soap:Body> " +
			"	</soap:Envelope> " ;
		
		logger.info("Request string ---> "+icicReqPrint);
		
		request = "" + icicReq;
		
		response = ICICIAPICall(icicReq,iciciURL, authorization);
		
		return response;
	}
	
	
	

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIciciRequest() {
        return iciciRequest;
    }

    public void setIciciRequest(String iciciRequest) {
        this.iciciRequest = iciciRequest;
    }

    public String getIciciRespose() {
        return iciciRespose;
    }

    public void setIciciRespose(String iciciRespose) {
        this.iciciRespose = iciciRespose;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
    
    
    
    
    
    
    @SuppressWarnings("resource")
	public String ICICIAPICall(String input,String URL, String authorization) {
	
		 JSONObject object = null;
		 String response ="";
		 String result = null;
			try {
            	  URL url = new URL(URL);
		          HttpURLConnection con = (HttpURLConnection) url.openConnection();
		          con.addRequestProperty("authorization", authorization);
			      con.setDoInput(true);
			      con.setDoOutput(true);
			      con.setUseCaches(false);
			      con.setConnectTimeout( 30000 );
			      con.setReadTimeout( 30000 );
			      con.setRequestMethod( "POST" );
			      con.setRequestProperty( "Content-Type", "text/xml; charset=utf-8" );

			      // Send the request
			      OutputStream outputStream = con.getOutputStream();
			      outputStream.write(input.toString().getBytes() );
			      outputStream.close();
			      
			      // Check for errors
			      int responseCode = con.getResponseCode();
			      InputStream inputStream;
			      if (responseCode == HttpURLConnection.HTTP_OK) {
			        inputStream = con.getInputStream();
			      } else {
			        inputStream = con.getErrorStream();
			      }

			      // Process the response
			      BufferedReader reader;
			      String line = null;
			      reader = new BufferedReader( new InputStreamReader( inputStream ) );
			      while( ( line = reader.readLine() ) != null )
			      {
			        response=response+line;
			      }
			      
                 //String res = iciciresponse.getBody().substring(1, iciciresponse.getBody().length());   
                 //ResponseEntity<String> iciciresponse = response;
                 
                 object=new JSONObject();
                 object.put("res", response);
                
                logger.info("Response string ---> "+response);
			     // System.out.println("Response string ---> "+response);
                
                DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
     			Document doc = db.parse(new InputSource(new StringReader(object.getString("res"))));
     		    NodeList list1 = doc.getElementsByTagName("InsertDataResponse");
     		   for (int temp = 0; temp < list1.getLength(); temp++) {
  		         Node nNode = list1.item(temp);
  		         if (nNode.getNodeType() == Node.ELEMENT_NODE) {
  		             Element eElement = (Element) nNode;
  		           result = eElement.getElementsByTagName("InsertDataResult").item(0).getTextContent();
  		         }
     		   }
     		   
                 logger.info("Encrypted Response JSON oBJ ---> "+object.toString());
			} catch (Exception e) {
			      e.printStackTrace();
				 logger.error("ICICI API Call Exception " + e);
			}
		 return response;
	 }
    
    public String getEncryptedData(String str,String strSoapURL1, String soapActEnc) {
		ICICIEncryptDetail encDtl = new ICICIEncryptDetail();
		String encData = encDtl.encryptData(str,strSoapURL1,soapActEnc);
		return encData;
	} 
    

}