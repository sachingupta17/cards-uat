package com.cbc.portal.utils;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import engageservice.EngageSoapApiClientService;
import engageservice.EngageSoapApiClientServiceStub;
import login.sessionmgmt.engageservice.Login;
import login.sessionmgmt.engageservice.LoginRequestType;
import login.sessionmgmt.engageservice.RESULT;
import login.sessionmgmt.engageservice.SessionMgmtResponseType;
import recipientactions.listmgmt.engageservice.AddRecipient;
import recipientactions.listmgmt.engageservice.AddRecipientRequestType;
import recipientactions.listmgmt.engageservice.ColumnElementType;
import recipientactions.listmgmt.engageservice.CreatedFrom;
import recipientactions.listmgmt.engageservice.ListMgmtResponseType;
import recipientactions.listmgmt.engageservice.True;
import sessionheader.engageservice.SessionHeader;
import sessionheader.engageservice.Sessionheadertype;

@Component
public class SilverPopUtil {
	private static Logger logger = Logger.getLogger(SilverPopUtil.class.getName());
	public static  String silverPopAPI(ColumnElementType[] elementArray, String silverPopuserName, String silverPoppassword, String silverPopListId){
	
	   try{
               System.out.println("silverPopListId"+silverPopListId);
               System.out.println("silverPopuserName"+silverPopuserName);
               System.out.println("silverPoppassword"+silverPoppassword);
               System.out.println("elementArray"+elementArray);
               for(int i=0;i<=elementArray.length; i++){
                   System.out.println("iii"+i);
               }
               for(ColumnElementType a:elementArray){
                   System.out.println("==name=>"+a.getNAME());
                    System.out.println("==value=>"+a.getVALUE());
                   
               }
			EngageSoapApiClientService engageService = new EngageSoapApiClientServiceStub();
			Login login = new Login();
			LoginRequestType loginRequestType = new LoginRequestType();
			loginRequestType.setUSERNAME(silverPopuserName);
			loginRequestType.setPASSWORD(silverPoppassword);
			login.setLogin(loginRequestType);
			RESULT result = engageService.login(login);
			SessionMgmtResponseType sessionResult = result.getRESULT();
			String sessionId = sessionResult.getSESSIONID();
			// Add Recipient
						AddRecipientRequestType addRecipientReqType = new AddRecipientRequestType();
						addRecipientReqType.setLIST_ID(new Long(silverPopListId));
						addRecipientReqType.setCREATED_FROM(new CreatedFrom(1, Boolean.TRUE));
						True trueParam = new True();
						True falseParam = new True();
						trueParam.setTrue(true);
						falseParam.setTrue(false);
						addRecipientReqType.setUPDATE_IF_FOUND(falseParam);
						addRecipientReqType.setALLOW_HTML(trueParam);
						addRecipientReqType.setSEND_AUTOREPLY(trueParam);
						addRecipientReqType.setSEND_AUTOREPLY(trueParam);
		
						addRecipientReqType.setCOLUMN(elementArray);
						AddRecipient addRecipient = new AddRecipient();
						addRecipient.setAddRecipient(addRecipientReqType);
		
						Sessionheadertype sessionHeaderType = new Sessionheadertype();
						sessionHeaderType.setSessionid(sessionId);
		
						SessionHeader sessionHeader = new SessionHeader();
						sessionHeader.setSessionHeader(sessionHeaderType);
		
						recipientactions.listmgmt.engageservice.RESULT recipientResult = engageService.addRecipient(addRecipient,
								sessionHeader);
						ListMgmtResponseType addSessResult = recipientResult.getRESULT();
                                                System.out.println("result==>"+recipientResult.getRESULT().toString());
                                                System.out.println("addSessResult==>"+addSessResult.getEMAIL());
                                                System.out.println("addSessResult 2==>"+addSessResult.getEmailE());
                                                System.out.println("addSessResult 3==>"+addSessResult.getORGANIZATION_ID());
                                                System.out.println("addSessResult 4==>"+addSessResult.getRecipientId());
                                                 System.out.println("addSessResult 5==>"+addSessResult.getVISITOR_ASSOCIATION());
                                                 System.out.println("addSessResult 6==>"+addSessResult.getCOLUMNS());
                                                  System.out.println("addSessResult 7==>"+addSessResult.getCONTACT_LISTS());
                                                  System.out.println("addSessResult 8==>"+addSessResult.getCreatedFrom());
                                                  System.out.println("addSessResult 9==>"+addSessResult.getEmailType());
                                                    System.out.println("addSessResult 10==>"+addSessResult.getFault());
                                                     System.out.println("addSessResult 11==>"+addSessResult.getLastModified());
                                                      System.out.println("addSessResult 12==>"+addSessResult.getOptedIn());
                                                      System.out.println("addSessResult 13==>"+addSessResult.getOptedOut());
                                                      System.out.println("addSessResult 14==>"+addSessResult.getResumeSendDate());
                                                       System.out.println("addSessResult 15==>"+addSessResult.getSUCCESS());
	   				}catch (AxisFault e) {
	   					logger.error("@@@@ Exception in SilverPopUtil silverPopAPI() :",e);
	   				} 
		   			catch (RemoteException ex) {
		   				logger.error("@@@@ Exception in SilverPopUtil silverPopAPI() :",ex);
		   			}
				return "";
		}

		
}
