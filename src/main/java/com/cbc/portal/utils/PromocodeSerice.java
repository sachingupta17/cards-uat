/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.utils;
import com.cbc.portal.service.impl.MemberEnrollServiceImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
//import org.apache.log4j.Loggers;
/**
 *
 * @author Aravind E
 */
public class PromocodeSerice {
    
    
    
     private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(MemberEnrollServiceImpl.class.getName());

    public String getPromoCodeData(String jpNumber,String voucherNo,String promourl,String pcSourceID,String pcTrasactionValue) {
        String response = "";
        String voucherStatus="";
         Map<String, String> map = new HashMap<String, String>();
       String input="";
       String voucherResponse="";
       
         try {
            URL url = new URL(promourl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestProperty("x-api-key", "6HG24SjFoE5lEA2xulm3r8berVSJi2wk6LB0uwjY");
//            conn.setRequestProperty("x-api-key", "wj7EkZOCdW9CRZZmIMySl4H8wmJfd6CS8QYCSmeo"); //forprod
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

             input = "{\n"
                    + "  \"voucherNo\": \""+voucherNo+"\",\n"
                    + "  \"sourceId\": \""+pcSourceID+"\",\n"
                    + "  \"transactionValue\": \""+pcTrasactionValue+"\"\n"
                    + "}";

              logger.info("promocode request=>"+input);
            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

//		if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
//			throw new RuntimeException("Failed : HTTP error code : "
//				+ conn.getResponseCode());
//		}
           BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
           
            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                voucherResponse=output;
                JSONObject jsonObj = new JSONObject(output);
                logger.info("promocode response=>"+ jsonObj);
                response = "" + jsonObj.getJSONArray("errorMessage");
                voucherStatus=""+jsonObj.getString("voucherStatus");

            }

            response = response.substring(1, response.length() - 1);

            conn.disconnect();
        } catch (MalformedURLException ex) {

            logger.error("MalformedURLException  :" + ex);
//            Logger.er(JavaApplication1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            logger.error("IOException  :" + ex);

//            Logger.getLogger(JavaApplication1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            logger.error("JSONException  :" + ex);
//            Logger.getLogger(JavaApplication1.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        map.put("request", input);
        map.put("voucherStatus", voucherStatus);
        map.put("voucherResponse", voucherResponse);
        map.put("error", response);
       
//        System.out.println("====inservice=>"+GsonUtil.toJson(map));
          return GsonUtil.toJson(map);
//        return voucherStatus;
    }
    
}
