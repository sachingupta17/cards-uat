
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.utils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
/**
 *
 * @author Parvti G
 */

public class DecryptJpNumberCards {
  
    
private String strSOAPXml = "";
    private String loginRequest;

    public String getStrSOAPXml() {
        return strSOAPXml;
    }

    public void setStrSOAPXml(String strSOAPXml) {
        this.strSOAPXml = strSOAPXml;
    }

    public String getLoginRequest() {
        return loginRequest;
    }

    public void setLoginRequest(String loginRequest) {
        this.loginRequest = loginRequest;
    }

    public String DecryptJpnumber(String Jpnumber,String strSoapURL1, String strSOAPAction1) {
        String DecryptedJpnumber = "";
        try {
//             System.out.println("strSoapURL1"+strSoapURL1);
//             System.out.println("strSOAPAction1"+strSOAPAction1);
//            String strSoapURL1 = "http://jppluat.jetprivilege.com/WebServices/GlobalEncryptDecryptService.asmx";
//            String strSoapURL1 = "https://www.jetprivilege.com/WebServices/GlobalEncryptDecryptService.asmx";
//            String strSOAPAction1 = "http://tempuri.org/Decrypt";
//              String strJpplno="yD96VRvSsVA3pA0kkoYF5QlfdBb225EwHV21ALypAJeoKF+yU80PhJQgvO+5sHUT";

            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            DecryptJpNumberCards decryptJpnumber = new DecryptJpNumberCards();

            SOAPMessage soapResponse = soapConnection.call(decryptJpnumber.getSoapMessageFromString(decryptJpnumber.getxmlData(Jpnumber), strSOAPAction1), strSoapURL1);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);

            strSOAPXml = new String(out.toByteArray());
          

            InputStream is = new ByteArrayInputStream(strSOAPXml.getBytes());

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("DecryptResponse");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String str = eElement.getElementsByTagName("DecryptResult").item(0).getTextContent();
                    JSONObject jsonObj = new JSONObject(str);

                    DecryptedJpnumber = jsonObj.getString("id");

                }

            }
            soapConnection.close();
        } catch (Exception e) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            e.printStackTrace();
//            new Maintainservicelog().maintainSoapserviceslog("CRIS");
        }
        return DecryptedJpnumber;
    }

    public String getxmlData(String Jpplno) {

        String soapXmlData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                + "  <soap:Header>\n"
                + "    <AuthHeader xmlns=\"http://tempuri.org/\">\n"
                + "      <UserName>f8c4c243-8870-4672-8f02-6898845643ec</UserName>\n"
                + "      <Password>cbb085be-7021-4c9b-80fd-0fc5c06815e2</Password>\n"
                + "    </AuthHeader>\n"
                + "  </soap:Header>\n"
                + "  <soap:Body>\n"
                + "    <Decrypt xmlns=\"http://tempuri.org/\">\n"
                + "      <textToDecrypt>{\n"
                + "  \"id\": \"" + Jpplno + "\",\n"
                + "}</textToDecrypt>\n"
                + "    </Decrypt>\n"
                + "  </soap:Body>\n"
                + "</soap:Envelope>";

        loginRequest = soapXmlData;
      
        return soapXmlData;
    }

    private SOAPMessage getSoapMessageFromString(String xml, String strSOAPAction) throws SOAPException, IOException {
        MessageFactory factory = MessageFactory.newInstance();

        SOAPMessage message = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8"))));

        MimeHeaders headers = message.getMimeHeaders();
        headers.addHeader("SOAPAction", strSOAPAction);

        message.saveChanges();

        return message;
    
    }
}
