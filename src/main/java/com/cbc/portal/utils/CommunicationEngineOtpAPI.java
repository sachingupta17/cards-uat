
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import static java.nio.charset.StandardCharsets.UTF_8;
import java.security.MessageDigest;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import static java.nio.charset.StandardCharsets.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.json.JSONException;

/**
 *
 * @author parvati
 */
public class CommunicationEngineOtpAPI {

    private Logger logger = Logger.getLogger(CommunicationEngineOtpAPI.class.getName());

    public String tokenGeneration(String username, String Password, String partnerTransactionId, String OtpType, String partnerCode, String mobileNo, String partnerHashKey, String reqToken, String reqotpTransactionID, String otpGenerationUrl, String tokenGenerationUrl) {
        String input = "";
//    username= "admin@jetprivilege.com";
//                Password = "Vernost@123";
        String output, token_id = "";
        String otpTransactionId = null;

        try {
            if (reqToken.equalsIgnoreCase("")) {
//            URL url = new URL("http://34.193.129.15:9991/api/Users/login");
                System.out.println("URL  " + tokenGenerationUrl);
                URL url = new URL(tokenGenerationUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");

                input = "          {\n"
                        + "            \"username\": \"" + username + "\",\n"
                        + "                \"password\": \"" + Password + "\"\n"
                        + " }";

                logger.info("tokenGeneration Requset===>" + input);
                System.out.println("request input " + input);
                OutputStream os = conn.getOutputStream();
                os.write(input.getBytes());
                os.flush();

                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    logger.info("tokenGeneration response====>" + output);
                    JSONObject jsonObj = new JSONObject(output);
                    logger.info("tokenGeneration json response====>" + jsonObj);
                    System.out.println("jsonObj  ===" + jsonObj.getString("id"));
                    System.out.println("-->" + jsonObj.isNull(output));
                    if (!jsonObj.getString("id").equals("")) {
                        token_id = "" + jsonObj.getString("id");
                    }
                    logger.info("token---------->" + token_id);
                    System.out.println("token---------->" + token_id);
                }
                conn.disconnect();

            } else {
                token_id = reqToken;
                System.out.println("token_id--------%%--->" + token_id);
            }

            System.out.println("token_id--------1--->" + token_id);
            if (!token_id.isEmpty()) {

                otpTransactionId = otpGeneartion(token_id, partnerTransactionId, OtpType, partnerCode, mobileNo, partnerHashKey, reqotpTransactionID, otpGenerationUrl);

                System.out.println("otpTransactionId");
            }
        } catch (Exception e) {
            logger.error("Exception at tokenGeneration", e);
        }

        return otpTransactionId;

    }

    public String otpGeneartion(String token, String partnerTransactionId, String OtpType, String partnerCode, String mobileNo, String partnerHashKey, String reqotpTransactionID, String otpGenerationUrl) {
        String input = "";
        String output, hash = token, response = "", inputOtp;
        String geneartedhash, otp_transaction_id = "";
        Map<String, String> finalResp = new HashMap<>();

        try {
            System.out.println("otpGeneartion====>");

//            URL url = new URL("http://34.193.129.15:9991/api/otp_transaction");
            URL url = new URL(otpGenerationUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            inputOtp = token + partnerCode + partnerTransactionId + "91" + mobileNo + "";
            logger.info("otpGeneartion hash String====>" + inputOtp);
            geneartedhash = getSecurePassword(inputOtp, partnerHashKey, "HmacSHA512");
            if (OtpType.equalsIgnoreCase("New")) {
                reqotpTransactionID = "";
            }

            logger.info("otpGeneartion generated hash-------_> " + getSecurePassword(inputOtp, partnerHashKey, "HmacSHA512"));
            input = "{\n"
                    + "        \"Token\": \"" + token + "\",\n"
                    + "        \"PartnerCode\": \"" + partnerCode + "\",\n"
                    + "        \"PartnerTransactionID\": \"" + partnerTransactionId + "\",\n"
                    + "        \"OtpTransactionID\": \"" + reqotpTransactionID + "\",\n"
                    + "        \"MobileNo\": \"91" + mobileNo + "\",\n"
                    + "        \"EmailId\": \"\",\n"
                    + "        \"OtpType\": \"" + OtpType + "\",\n"
                    + "        \"Hash\": \"" + geneartedhash + "\"\n"
                    + "    }";
            System.out.println("input  _________" + input);

            logger.info("otpGeneartion Request" + input);
            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
//            System.out.println("==>" + conn.getResponseMessage());
            byte[] b = null;
//        System.out.println("=1=>"+conn.getErrorStream());
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                otp_transaction_id = "";
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            long start = new Date().getTime();

            while ((output = br.readLine()) != null && new Date().getTime() - start < 1000L) {
                if (conn.getResponseMessage().equalsIgnoreCase("OK")) {
                    logger.info("otpGeneartion Response  " + output);
                    JSONObject jsonObj = new JSONObject(output);
                    logger.info("otpGeneartion output  " + jsonObj);
                    JSONObject jsonObj1 = new JSONObject(jsonObj.getString("data"));
                    System.out.println("jsonObj1  " + jsonObj1);

                    if(OtpType!=null && OtpType.equalsIgnoreCase("New"))
                    {
                    	otp_transaction_id = "" + jsonObj1.getString("Otp_transaction_id");
                    	logger.info("otp_transaction_id New  " + otp_transaction_id);
                    }
                    else if(OtpType!=null && OtpType.equalsIgnoreCase("ReSend"))
                    {
                    	otp_transaction_id = "" + jsonObj1.getString("OtpTransactionID");
                    	logger.info("otp_transaction_id Resend  " + otp_transaction_id);
                    }
                    
                    
                    //System.out.println("otp_transaction_id  " + otp_transaction_id);

                    finalResp.put("otp_transaction_id", otp_transaction_id);
                    finalResp.put("token", token);

                }
//                                JSONObject jsonObj = new JSONObject(output);
//                if(!jsonObj.isNull(output)){
//                token_id=""+jsonObj.getString("id");
//                }
//                System.out.println("token_id----------->"+token_id);
            }

            conn.disconnect();

        } catch (Exception e) {
            logger.error("Exception at otpGeneartion()", e);

        }

        System.out.println("Response  :   " + finalResp);
        return GsonUtil.toJson(finalResp);
    }

    public String getSecurePassword(String password, String salt, String algo)
            throws NoSuchAlgorithmException, InvalidKeyException {
        System.out.println("Hash Key --> " + salt);
        System.out.println("Input Key --> " + password);

        SecretKeySpec secretKeySpec = new SecretKeySpec(salt.getBytes(UTF_8), algo);
        Mac mac = Mac.getInstance(algo);
        mac.init(secretKeySpec);
        byte[] bytes = mac.doFinal(password.getBytes(UTF_8));

        return new BigInteger(1, bytes).toString(16);
    }

    public String verificationOtp(String username, String Password, String partnerTransactionId, String OtpType, String partnerCode, String mobileNo, String partnerHashKey, String otpTransactionId, String smsOtp, String reqtoken, String otpVerifyUrl) throws IOException {
        String input = "";
        String output, token_id = "", inputOtpVerify, geneartedhash, OTP_Verified = "", pendingAttempts = "";
        Map<String, String> finalResp = new HashMap<>();

        try {
            new Testing().doTrustToCertificates();

            logger.info("token at verificationOtp-------------->" + reqtoken);
            if (!reqtoken.isEmpty()) {
                inputOtpVerify = reqtoken + partnerCode + partnerTransactionId + otpTransactionId + "91" + mobileNo + "" + "" + smsOtp;
                System.out.println("OtpTransactionID");
                logger.info("verificationOtp hash String" + inputOtpVerify);
                geneartedhash = getSecurePassword(inputOtpVerify, partnerHashKey, "HmacSHA512");
                logger.info(" verificationOtp hash -----> " + getSecurePassword(inputOtpVerify, partnerHashKey, "HmacSHA512"));

                System.out.println("token_id--------1------->" + reqtoken);
//                    URL url1 = new URL("http://34.193.129.15:9991/api/otp-verification/otp-verification");
                URL url1 = new URL(otpVerifyUrl);
                HttpURLConnection conn1 = (HttpURLConnection) url1.openConnection();
                conn1.setDoOutput(true);
                conn1.setRequestMethod("POST");
                conn1.setRequestProperty("Content-Type", "application/json");
                String pattern = "yyyy-MM-dd";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

                String date = simpleDateFormat.format(new Date());
                System.out.println(date);

                input = "{\n"
                        + "\"token\":\"" + reqtoken + "\",\n"
                        + "\"PartnerCode\":\"" + partnerCode + "\",\n"
                        + "\"PartnerTransactionID\":\"" + partnerTransactionId + "\",\n"
                        + "\"OtpTransactionID\":\"" + otpTransactionId + "\",\n"
                        + "\"MobileNo\":\"91" + mobileNo + "\",\n"
                        + "\"EmailId\":\"\",\n"
                        + "\"SmsOtp\":\"" + smsOtp + "\",\n"
                        + "\"EmailOtp\":\"\",\n"
                        + "\"Hash\":\"" + geneartedhash + "\",\n"
                        + "\"partnerTimestamp\": \"" + date + "\",\n"
                        + "\"isAnyOtp\":false\n"
                        + "}";

                logger.info("verificationOtp Request======>" + input);
                OutputStream os1 = conn1.getOutputStream();
                os1.write(input.getBytes());
                os1.flush();
                System.out.println("=verify=======================>" + conn1.getResponseMessage());
                byte[] b = null;
//        System.out.println("=1=>"+conn.getErrorStream());
                if (conn1.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    OTP_Verified = "";
                    BufferedReader br1 = new BufferedReader(new InputStreamReader((conn1.getErrorStream())));

                    Long start = new Date().getTime();

                    while ((output = br1.readLine()) != null && new Date().getTime() - start < 1000L) {
                        System.out.println("*****************" + output.contains("Your number of attempts are exceeds"));
                        if (output.contains("Your number of attempts are exceeds") == true || output.contains("Number of attempts exceeds limit")) {
                            OTP_Verified = "false";
                            pendingAttempts = "Number of attempts exceeds limit";
                            finalResp.put("OTP_Verified", OTP_Verified);
                            finalResp.put("pendingAttempts", pendingAttempts);

                        }
                        System.out.println("===output=============>" + output);

                    }
                }

                BufferedReader br1 = new BufferedReader(new InputStreamReader((conn1.getInputStream())));

                while ((output = br1.readLine()) != null) {
                    if (conn1.getResponseMessage().equalsIgnoreCase("OK")) {
                        logger.info("verificationOtp Response=============>" + output);
                        JSONObject jsonObj1 = new JSONObject(output);
                        System.out.println("jsonObj  " + jsonObj1);
                        JSONObject jsonObj2 = new JSONObject(jsonObj1.getString("data"));
                        System.out.println("jsonObj1  " + jsonObj1);

                        OTP_Verified = "" + jsonObj2.getString("OTP_Verified");
                        System.out.println("OTP_Verified  " + OTP_Verified);
                        pendingAttempts = jsonObj2.getString("Pending_Attempts");
                        System.out.println("pendingAttempts  " + pendingAttempts);
                        finalResp.put("OTP_Verified", OTP_Verified);
                        finalResp.put("pendingAttempts", pendingAttempts);
                    }

                }
                conn1.disconnect();
            }

        } catch (NoSuchAlgorithmException ex) {
            java.util.logging.Logger.getLogger(CommunicationEngineOtpAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            java.util.logging.Logger.getLogger(CommunicationEngineOtpAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(CommunicationEngineOtpAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            logger.error(e);
        }
        return GsonUtil.toJson(finalResp);
    }
}
