package com.cbc.portal.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cbc.portal.DAO.AmexCardDAO;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.FileWriter;
import java.io.IOException;

@Transactional(readOnly=true)
public class SchedularService {
	
	
	@Autowired
	public AmexCardDAO amexCardDAO;
	
	@Value("${application.cbc.unica.flag.interval}")
	private String ufInterval;
	
	@Value("${application.cbc.unica.upload.localpath}")
	private String ufUploadLocalPath;
	
	@Value("${application.cbc.unica.upload.remotepath}")
	private String ufUploadRemotePath;
	
	private static Logger logger = Logger.getLogger(SchedularService.class.getName());
	
	
	private JSch jsch;
	private Session session;
	private Channel channel;
	private ChannelSftp sftpChannel;
	
	String SFTPHOST = "202.191.178.122";
        int    SFTPPORT = 22;
        String SFTPUSER = "unica_cards_sftp";
        String SFTPPASS = "O@6x&3%U";
	
	
	@Transactional(readOnly=false,propagation=Propagation.REQUIRED)
	public void updateUnicaFlag(){
		try{
			amexCardDAO.updateUnicaFlag(ufInterval);
		}catch(Exception e){
			logger.error("@@@@ Exception in SchedularService updateUnicaFlag() :",e);
		}
	}
	
	public void uploadUnicaFlagFile(){
		List<CcBeApplicationFormDetails> ccBeApplicationFormDetailsList = null;
		Map<Integer, Map<Integer, String>> unicaMap = null;
		try{
			File file = new File(ufUploadLocalPath);
			if(!file.exists()){
				file.mkdirs();
			}
			ccBeApplicationFormDetailsList = amexCardDAO.getUnicaFladData();
			if(null != ccBeApplicationFormDetailsList){
				unicaMap = getUnicaData(ccBeApplicationFormDetailsList);
			}
			saveExcelFile(unicaMap, ufUploadLocalPath);
			upload(ufUploadLocalPath+File.separator+"Call_Center_Pending_Applications.csv", ufUploadRemotePath);
		}catch(Exception e){
			logger.error("@@@@ Exception in SchedularService uploadUnicaFlagFile() :",e);
		}
	}

	private void upload(String fileName, String ufUploadRemotePath) throws IOException {
		FileInputStream fis = null;
		connect();
		try {
			// Change to output directory
			sftpChannel.cd(ufUploadRemotePath);

			// Upload file
			File file = new File(fileName);
			fis = new FileInputStream(file);
			sftpChannel.put(fis, file.getName());

			fis.close();

		} catch (Exception e) {
			logger.error("@@@@ Exception in SchedularService upload() :",e);
		}finally{
                    logger.info("@@@@ Final block in SchedularService upload()");
                fis.close();
                }
		disconnect();
		
	}
	
	private void connect() {
		try {
			jsch = new JSch();
			session = jsch.getSession(SFTPUSER, SFTPHOST,SFTPPORT);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(SFTPPASS);
			session.connect();

			channel = session.openChannel("sftp");
			channel.connect();
			sftpChannel = (ChannelSftp) channel;
		} catch (JSchException e) {
			logger.error("@@@@ Exception in SchedularService connect() :",e);
		}
		
	}

	private void disconnect() {
		sftpChannel.disconnect();
		channel.disconnect();	
		session.disconnect();
	}

	

	private void saveExcelFile(Map<Integer, Map<Integer, String>> unicaMap, String ufUploadLocalPath) throws IOException {
		Map<Integer,String> heading = null;
		Map<Integer,String> data = null;
                FileWriter fileWriter = null;
                
                

      String NEW_LINE_SEPARATOR = "\n";
      String COMMA_DELIMITER = ",";
    

		try{
                   
                  
     fileWriter = new FileWriter(ufUploadLocalPath+File.separator+"Call_Center_Pending_Applications.csv");

			
     
            int size =0;
               if(unicaMap!=null){
		        heading=unicaMap.get(0);
		      size = unicaMap.size()-1;
		        for (Map.Entry<Integer, String> entry : heading.entrySet()) {
                               
                                        
               
                fileWriter.append(entry.getValue());
                 if(!entry.getValue().equalsIgnoreCase("APPLICATION_DATE") ){
                fileWriter.append(COMMA_DELIMITER);
                 }
                
                
		        }
                        

               fileWriter.append(NEW_LINE_SEPARATOR);
               
               for(int i=1;i<=size;i++){
		        	data=unicaMap.get(i);
		        	 //XSSFRow aRow = sheet.createRow(i);
		        	for (Map.Entry<Integer, String> entry1 : data.entrySet()) {
                                fileWriter.append(entry1.getValue());
                                if(!entry1.getKey().equals(14)){
                                fileWriter.append(COMMA_DELIMITER);
                                } 
                                }
                fileWriter.append(NEW_LINE_SEPARATOR);
               }}

// create a new Excel sheet
//			FileOutputStream fileOut = new FileOutputStream(path+File.separator+"Call_Center_Pending_Applications.csv");
//			XSSFWorkbook workbook = new XSSFWorkbook();
//			XSSFSheet sheet = workbook.createSheet("UNICA_DATA");
//	        sheet.setDefaultColumnWidth(30);
//	        
//	     // create style for header cells
//	        CellStyle style = workbook.createCellStyle();
//	        Font font = workbook.createFont();
//	        font.setFontName("Arial");
//	        style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
//	        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
//	        style.setAlignment(style.ALIGN_CENTER);
//	        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
//	        font.setColor(HSSFColor.WHITE.index);
//	        style.setFont(font);
//	        
//	        XSSFRow header = sheet.createRow(0);
//	        if(unicaMap!=null){
//		        heading=unicaMap.get(0);
//		        int size = unicaMap.size()-1;
//		        for (Map.Entry<Integer, String> entry : heading.entrySet()) {
//		        	header.createCell(entry.getKey()).setCellValue(entry.getValue());
//		        	header.getCell(entry.getKey()).setCellStyle(style);
//		        }
//		               
//		        for(int i=1;i<=size;i++){
//		        	data=unicaMap.get(i);
//		        	 XSSFRow aRow = sheet.createRow(i);
//		        	for (Map.Entry<Integer, String> entry1 : data.entrySet()) {
//		        		  aRow.createCell(entry1.getKey()).setCellValue(entry1.getValue());
//		            }
//		        }
//	        }
//	        workbook.write(fileOut);
//	        fileOut.flush();
//			fileOut.close();
               fileWriter.close();
		}catch(Exception e){
			logger.error("@@@@ Exception in SchedularService saveExcelFile() :",e);
		}finally{
                    logger.info("@@@@ Final block in SchedularService saveExcelFile()");
                fileWriter.close();
                }
	}

	private Map<Integer, Map<Integer, String>> getUnicaData(List<CcBeApplicationFormDetails> ccBeApplicationFormDetailsList) {
		Map<Integer,String> header = new HashMap<>();
		Map<Integer,String> values = null;
		Map<Integer, Map<Integer, String>>  unicaMap = new HashMap<>();
		try{
			header.put(0, "SEQ_NO");
			header.put(1, "JP_NUMBER");
			header.put(2, "JP_TIER");
			header.put(3, "FIRST_NAME");
			header.put(4, "MIDDLE_NAME");
			header.put(5, "LAST_NAME");
			header.put(6, "DATE_OF_BIRTH");
			header.put(7, "GENDER");
			header.put(8, "EMAIL");
			header.put(9, "MOBILE");
			header.put(10, "FORM_STATUS");
			header.put(11, "FORM_NUMBER");
			header.put(12, "RANDOM_NUMBER");
			header.put(13, "INITIATION_DATE");
			header.put(14, "APPLICATION_DATE");
			
			unicaMap.put(0,header);
			int i = 1;
			do{
				for(CcBeApplicationFormDetails ccbeData : ccBeApplicationFormDetailsList){
					values=new HashMap<>();
						values.put(0, i+"");
						values.put(1, CommonUtils.nullSafe(ccbeData.getJetpriviligemembershipNumber(), "NA"));
						values.put(2, CommonUtils.nullSafe(ccbeData.getJetpriviligemembershipTier(), "NA"));
						values.put(3, CommonUtils.nullSafe(ccbeData.getFname(), "NA"));
						values.put(4, CommonUtils.nullSafe(ccbeData.getMname(), "NA"));
						values.put(5, CommonUtils.nullSafe(ccbeData.getLname(), "NA"));
						values.put(6, null!=ccbeData.getDateOfBirth() ? CommonUtils.getDate(ccbeData.getDateOfBirth(), "dd-MM-yyyy") : "NA");
						values.put(7, CommonUtils.nullSafe(ccbeData.getGender(), "NA"));
						values.put(8, CommonUtils.nullSafe(ccbeData.getEmail(), "NA"));
						values.put(9, CommonUtils.nullSafe(ccbeData.getMobile(), "NA"));
						values.put(10, CommonUtils.nullSafe(ccbeData.getFormStatus(), "NA"));
						values.put(11, CommonUtils.nullSafe(ccbeData.getFormNumber(), "NA"));
						values.put(12, CommonUtils.nullSafe(ccbeData.getRandomNumber(), "NA"));
						values.put(13, null!=ccbeData.getInitiationDate() ? CommonUtils.getDate(ccbeData.getInitiationDate(), "dd-MM-yyyy HH:mm:ss") : "NA");
						values.put(14, null!=ccbeData.getCreatedTime() ? CommonUtils.getDate(ccbeData.getCreatedTime(), "dd-MM-yyyy HH:mm:ss") : "NA");
						
						unicaMap.put(i, values);  
				i = i+1;
				}
			}while(i<=ccBeApplicationFormDetailsList.size());
		}catch(Exception e){
			logger.error("@@@@ Exception in SchedularService getUnicaData() :",e);
		}
		return unicaMap;
	}

}
