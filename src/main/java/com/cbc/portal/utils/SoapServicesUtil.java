package com.cbc.portal.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import org.apache.log4j.Logger;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.xml.transform.StringSource;

public class SoapServicesUtil {
	
	private SoapServicesUtil(){
		
	}
	
	private static Logger logger = Logger.getLogger(SoapServicesUtil.class.getName());
	
	public static WebServiceMessageCallback addActionToSoap(){

		WebServiceMessageCallback callBack = null;
		callBack = new WebServiceMessageCallback() {
			@Override
			public void doWithMessage(WebServiceMessage message) throws IOException,TransformerException {
			
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				((SoapMessage) message).setSoapAction("http://tempuri.org/IgnaJetAirways/SubmitGNAWithoutPrequalApplication");
		         message.writeTo(out);
		         String strMsg = new String(out.toByteArray());
		         logger.info("Message Request== "+strMsg);
			}

		};
		return callBack;
	}

	public static WebServiceMessageCallback addHeaderToSoapCris(final String methodName){
		WebServiceMessageCallback callBack=null;
		callBack=new WebServiceMessageCallback() {
			@Override
			public void doWithMessage(WebServiceMessage message) throws IOException,TransformerException {
			
				((SoapMessage) message).setSoapAction("http://skywards.com/Mercator.CRIS.WS/"+methodName.trim());
				SoapHeader header = ((SoapMessage)message).getSoapHeader();
				StringSource headerSource = new StringSource("<AuthenticationHeader MyAttribute=\"\" xmlns=\"http://skywards.com/Mercator.CRIS.WS\"><AuthenticationName>WEB</AuthenticationName>"
				+ "<AuthenticationPassword>WEB</AuthenticationPassword> </AuthenticationHeader>");
		        Transformer transformer = TransformerFactory.newInstance().newTransformer();
		        transformer.transform(headerSource, header.getResult());
		        logger.info("in message"+message);
			}
		};
		return callBack;
	}
}
