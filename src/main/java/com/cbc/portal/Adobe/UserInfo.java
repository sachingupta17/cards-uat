
package com.cbc.portal.Adobe;


public class UserInfo {

    private String JPNumber;
    private String pointsBalance;
    private String pid;
    private String activeCardNo;
    private String usernameType;
    private String firstloginDate;
    private String lastloginDate;
    private String cookieAccept;
    private String cookieDateTime;
    private String loginStatus;
    private String EmailVerified;
    private String MobileVerified;
    private String PersonId;
    private String activeStatus;
    private String age;
    private String gender;
    private String cityofResidence;
    private String countryofResidence;
    private String memProgram;
    private String tier;
    private String sessionID;
    private String sessionDateTime;
    private String cookieAcceptedProperty;

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getSessionDateTime() {
        return sessionDateTime;
    }

    public void setSessionDateTime(String sessionDateTime) {
        this.sessionDateTime = sessionDateTime;
    }

    public String getCookieAcceptedProperty() {
        return cookieAcceptedProperty;
    }

    public void setCookieAcceptedProperty(String cookieAcceptedProperty) {
        this.cookieAcceptedProperty = cookieAcceptedProperty;
    }

    public String getJPNumber() {
        return JPNumber;
    }

    public void setJPNumber(String JPNumber) {
        this.JPNumber = JPNumber;
    }

    public String getPointsBalance() {
        return pointsBalance;
    }

    public void setPointsBalance(String pointsBalance) {
        this.pointsBalance = pointsBalance;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getActiveCardNo() {
        return activeCardNo;
    }

    public void setActiveCardNo(String activeCardNo) {
        this.activeCardNo = activeCardNo;
    }

    public String getUsernameType() {
        return usernameType;
    }

    public void setUsernameType(String usernameType) {
        this.usernameType = usernameType;
    }

    public String getFirstloginDate() {
        return firstloginDate;
    }

    public void setFirstloginDate(String firstloginDate) {
        this.firstloginDate = firstloginDate;
    }

    public String getLastloginDate() {
        return lastloginDate;
    }

    public void setLastloginDate(String lastloginDate) {
        this.lastloginDate = lastloginDate;
    }

    public String getCookieAccept() {
        return cookieAccept;
    }

    public void setCookieAccept(String cookieAccept) {
        this.cookieAccept = cookieAccept;
    }

    public String getCookieDateTime() {
        return cookieDateTime;
    }

    public void setCookieDateTime(String cookieDateTime) {
        this.cookieDateTime = cookieDateTime;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getEmailVerified() {
        return EmailVerified;
    }

    public void setEmailVerified(String EmailVerified) {
        this.EmailVerified = EmailVerified;
    }

    public String getMobileVerified() {
        return MobileVerified;
    }

    public void setMobileVerified(String MobileVerified) {
        this.MobileVerified = MobileVerified;
    }

    public String getPersonId() {
        return PersonId;
    }

    public void setPersonId(String PersonId) {
        this.PersonId = PersonId;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCityofResidence() {
        return cityofResidence;
    }

    public void setCityofResidence(String cityofResidence) {
        this.cityofResidence = cityofResidence;
    }

    public String getCountryofResidence() {
        return countryofResidence;
    }

    public void setCountryofResidence(String countryofResidence) {
        this.countryofResidence = countryofResidence;
    }

    public String getMemProgram() {
        return memProgram;
    }

    public void setMemProgram(String memProgram) {
        this.memProgram = memProgram;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }
}
