
package com.cbc.portal.Adobe;


public class DigitalData {

    private PageInfo pageInfo;
    private UserInfo userInfo;
    private MarketingInfo marketingInfo;
    

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public MarketingInfo getMarketingInfo() {
        return marketingInfo;
    }

    public void setMarketingInfo(MarketingInfo marketingInfo) {
        this.marketingInfo = marketingInfo;
    }

    

    
}
