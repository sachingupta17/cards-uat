
package com.cbc.portal.Adobe;


public class AdobeData {

    private DigitalData digitalData;

    public DigitalData getDigitalData() {
        return digitalData;
    }

    public void setDigitalData(DigitalData digitalData) {
        this.digitalData = digitalData;
    }

}
