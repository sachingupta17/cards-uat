package com.cbc.portal.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.auth0.NonceGenerator;
import com.auth0.NonceStorage;
import com.auth0.RequestNonceStorage;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.CityMappingBean;
import com.cbc.portal.beans.EnrollBean;
import com.cbc.portal.beans.IciciBean;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.cbc.portal.entity.CcPageMaster;
import com.cbc.portal.service.CardDetailsService;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.service.IciciCardService;
import com.cbc.portal.service.MemberDetailsService;
import com.cbc.portal.service.MemberEnrollService;
import com.cbc.portal.service.RecommendCardService;
import com.cbc.portal.utils.ApplicationPropertiesUtil;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.EncryptDetail;
import com.cbc.portal.utils.GsonUtil;
import com.cbc.portal.utils.ICICIEncryptDetail;

@Controller
public class IciciCardController {

    private Logger logger = Logger.getLogger(IciciCardController.class.getName());
    private final NonceGenerator nonceGenerator = new NonceGenerator();

    @Autowired
    ApplicationPropertiesUtil applicationPropertiesUtil;

    @Autowired
    IciciCardService amexCardService;

    @Autowired
    CommonService commonService;

    @Autowired
    MemberEnrollService memberEnrollService;

    @Autowired
    MemberDetailsService memberDetailsService;

    @Autowired
    CardDetailsService cardDetailsService;

    @Autowired
    RecommendCardService recommendCardService;

    @Value("${application.icici.bankNumber}")
    private String bankPhoneNumber;

    @Value("${application.icici.days}")
    private String iciciDays;

    @Value("${application.silverpop.approved}")
    private Long approvedId;

    @Value("${application.silverpop.provisionallyApproved}")
    private Long proviApprovedId;

    @Value("${application.silverpop.reject}")
    private Long rejectId;

    @Value("${application.silverpop.tryAgain}")
    private Long tryAgainId;
    
    @Value("${application.sendGrid.provisionallyApproved}")
    private String sendGridProviApprovedId;

    @Value("${application.sendGrid.reject}")
    private String sendGridRejectId;

    @Value("${application.sendGrid.tryAgain}")
    private String sendGridTryAgainId;
    
    
    
    @Value("${application.pii.encryption.strSoapURL1}")
   	String strSoapURL1;
   	@Value("${application.pii.encrypt.strSOAPAction1}")
   	String soapActEnc;

    private static final String BEAN_NAME = "iciciBean";
    private static final String REDIRECT_URL = "redirect:/icici-apply-form/";
    private static final String ERROR_URL = "/jsp/portal/error.jsp";
    private static final String FLASH_CARDNAME = "cardName";
    private static final String ENROLL_BEAN = "enrollBean";
    private static final String JP_NUMBER = "jpNumber";
    private static final String RANDOM_NUMBER = "randomNumber";
    private static final String BE_RANDOM_NUMBER = "beRandomNumber";

    @RequestMapping("/icici-apply-form/{cardName}")
    public String applyICICIPage(@PathVariable("cardName") String cardName, HttpServletRequest request,
            ModelMap model, @ModelAttribute("errorMsg") String errorMsg,
            @ModelAttribute(BEAN_NAME) IciciBean iciciBean, @ModelAttribute(RANDOM_NUMBER) String randomNumber,
            @ModelAttribute(JP_NUMBER) String jpNum, @ModelAttribute("formNumber") String formNumber, @ModelAttribute(BE_RANDOM_NUMBER) String beRandomNumber) {
        String spMailingID = "";
        String spUserID = "";
        String spJobID = "";
        String spReportId = "";
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        String termsAndCondition = "";
        String user = getLoggedInUser(request);
        String cName = cardName.replaceAll("-", " ");
        String[] strArray = cName.split(" ");
        StringBuilder newCardName = new StringBuilder();
        for (String s : strArray) {
            if ("express".equalsIgnoreCase(s)) {
                s = s + "&reg;";
            }
            newCardName.append(s + " ");
        }
        try {
            spMailingID = CommonUtils.nullSafe(request.getParameter("spMailingID"), "");
            spUserID = CommonUtils.nullSafe(request.getParameter("spUserID"), "");
            spJobID = CommonUtils.nullSafe(request.getParameter("spJobID"), "");
            spReportId = CommonUtils.nullSafe(request.getParameter("spReportId"), "");

            utm_source = CommonUtils.nullSafe(request.getParameter("utm_source"), "");
            utm_medium = CommonUtils.nullSafe(request.getParameter("utm_medium"), "");
            utm_campaign = CommonUtils.nullSafe(request.getParameter("utm_campaign"), "");
            logger.info("IciciCardController ==> utm_source: " + utm_source + " utm_medium: " + utm_medium + " utm_campaign: " + utm_campaign);
            int cpNo = cardDetailsService.getCardIdByName(newCardName.toString().trim());
            if (0 != cpNo) {
                String cardVarientjpNumber = commonService.checkCardVarientRandomNo(cpNo, beRandomNumber);
                List<String> groupHeaders = null;
                iciciBean.setCpNo(cpNo);
                termsAndCondition = commonService.getTermdAndCondition(cpNo);
                groupHeaders = commonService.getGroupHeaders(1);
                CardDetailsBean cardDetailsBean = recommendCardService.getRecommendedCardById(cpNo);
                //--------new----
                if (!utm_source.isEmpty()) {
                    trackCode = "?utm_source=" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                    iciciBean.setUtmSource(utm_source);
                    iciciBean.setUtmMedium(utm_medium);
                    iciciBean.setUtmCampaign(utm_campaign);
                }
                //---end-----------
                if (groupHeaders.size() > 3) {
                    groupHeaders.subList(3, groupHeaders.size()).clear();
                }
                model.addAttribute("groupHeadings", groupHeaders);
                model.addAttribute("applicationURL", applicationPropertiesUtil.getApplicationURL());
                //-----append trackcode----------

                request.getSession().setAttribute("redirectUrl", "icici-apply-form/" + cardName + trackCode);
                if (!spMailingID.isEmpty()) {
                    trackCode = "lw_" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
                }
                if (!utm_source.isEmpty()) {
                    trackCode = "utm_" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                }
//				request.getSession().setAttribute("trackCode", trackCode);
                request.getSession().setAttribute("homeUrl", "icici-apply-form/" + cardName);

//                if (request.getSession().getAttribute("loggedInUser") == null) {
//                    request.getSession().setAttribute("redirectUrl", "icici-apply-form/" + cardName+trackCode);
//                } else {
//                    request.getSession().setAttribute("homeUrl", "icici-apply-form/" + cardName);
//                }
                //--------add-trackode ---
                request.getSession().setAttribute("trackCode", trackCode);

                String salariedincome = null, selfemployedincome = null;
                //                sprint 56
                List<CcPageMaster> pageMasterList = null;

                String callMeFlag = "", promocodeFlag = "";
                pageMasterList = commonService.getPageMasterDetail(5);
                for (CcPageMaster flag : pageMasterList) {
                    logger.info("callme" + flag.getCallmeflag() + " promo" + flag.getPromocodeflag());
                    callMeFlag = String.valueOf(flag.getCallmeflag());
                    promocodeFlag = String.valueOf(flag.getPromocodeflag());
                }
                logger.info("callMeFlag" + callMeFlag + "promocodeFlag" + promocodeFlag);

                model.addAttribute("callMeFlag", callMeFlag);
                model.addAttribute("promocodeFlag", promocodeFlag);
//            sprint 56 end
                model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getAfTitle().replace("{card name}", cardDetailsBean.getImageText()));
                model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getAfDescription().replace("{card name}", cardDetailsBean.getImageText()));
                model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getAfKeyword());
                model.addAttribute("genderStatus", commonService.getEnumValues("Gender"));
                model.addAttribute("timer", commonService.getTimer("Timer for Application form update"));

                model.addAttribute("cities", commonService.getICICICities());
                model.addAttribute("states", commonService.getICICIStates());
                model.addAttribute("qualification", commonService.getAmexValues("Qualification"));
                model.addAttribute("pincode", commonService.getPincode());
                model.addAttribute("salaryStatus", commonService.getICICIValues("ICICI_EMP_TYPES"));
                model.addAttribute("ICICIBankRelationshipstatus", commonService.getICICIValues("ICICI_REALATION_STATUS"));
                model.addAttribute("ICICIBankRelationshipstatus1", commonService.getICICISelectedValues());
                model.addAttribute("salaryBankWithOtherBank", commonService.getICICIValues("SALARY_ACC_WITH_OTHER_BANK"));
                model.addAttribute("salaryAccountOpened", commonService.getICICIValues("SAL_ACC_OPENED"));
                model.addAttribute("channelType", commonService.getICICIValues("Channel_Type"));
                model.addAttribute("iciciITR", commonService.getICICIValues("ICICI_ITR_STATUS"));
                model.addAttribute(ENROLL_BEAN, new EnrollBean());
                model.addAttribute("callMeBean", new CallMeBean());
                model.addAttribute("defaultPromocode", cardDetailsBean.getDefaultPromocode());
                model.addAttribute("termsAndCondition", termsAndCondition);
                model.addAttribute("SalariedIncome", commonService.getempstatusSalaried(2, cpNo));
                model.addAttribute("SelfEmpIncome", commonService.getempstatusSelfemp(2, cpNo));

                model.addAttribute("companynames", commonService.getCompanyNames());
                //cr 2
                model.addAttribute("agebelowrange", commonService.getDataForAgeRangeSalaried(String.valueOf(2), String.valueOf(cpNo)));
                model.addAttribute("ageaboverange", commonService.getDataForAgeRangeSelf(String.valueOf(2), String.valueOf(cpNo)));
                //cr 2 end
                iciciBean.setBeJpNumber(cardVarientjpNumber);
                model.addAttribute("beJpNumber", cardVarientjpNumber);
                model.addAttribute("beRandomNumber", beRandomNumber);
                if (errorMsg.isEmpty()) {
                    formNumber = RandomStringUtils.random(20, true, true);
                    logger.info("====formNumber" + formNumber);
                    iciciBean.setFormNumber(formNumber);
//                                                
                }
                logger.info("randomNumber  ::::::" + randomNumber);
                if (null != randomNumber && !randomNumber.isEmpty()) {
                    amexCardService.getBEIciciFormDetails(randomNumber, iciciBean);

                    if (null != user && !user.isEmpty()) {
                        if (iciciBean.getJetpriviligemembershipNumber().isEmpty()) {
                            iciciBean.setJetpriviligemembershipNumber(CommonUtils.getJPNumber(user));
                            cardDetailsBean = amexCardService.getICICIOfferByJPNum(CommonUtils.getJPNumber(user), cardDetailsBean);
                        } else {
                            cardDetailsBean = amexCardService.getICICIOfferByJPNum(iciciBean.getJetpriviligemembershipNumber(), cardDetailsBean);
                        }
                    } else {
                        if (!iciciBean.getJetpriviligemembershipNumber().isEmpty()) {
                            cardDetailsBean = amexCardService.getICICIOfferByJPNum(iciciBean.getJetpriviligemembershipNumber(), cardDetailsBean);
                        } else {
                            cardDetailsBean = amexCardService.getICICIOfferByJPNum(jpNum, cardDetailsBean);
                        }
                    }
                    model.addAttribute(BEAN_NAME, iciciBean);
                } else {
                    if (errorMsg.isEmpty()) {
                        formNumber = RandomStringUtils.random(20, true, true);
//                                                formNumber="XBLKBfcXP9Dm0nFrFZJS";
                        String dbFormNumber = "";
                        int i = 0;
                        logger.info("**** formNumber *****" + formNumber);
                        while (!formNumber.equalsIgnoreCase(null)) {
                            if (i == 2) {
                                formNumber = RandomStringUtils.random(20, true, true);

                                logger.info("inside formNumber" + formNumber);
                            }

                            logger.info("**** formNumber inside while ****" + formNumber);
                            dbFormNumber = amexCardService.getFormNumber(formNumber);
                            logger.info(" dbFormNumber  : " + dbFormNumber);
                            if (!dbFormNumber.equalsIgnoreCase(formNumber)) {
                                logger.info(" Inside  if condition   : " + dbFormNumber);
                                break;

                            }
                            i = 2;

                        }

                        iciciBean.setFormNumber(formNumber);
                        logger.info("icicibaen.getFormNumber at controller" + iciciBean.getFormNumber());
                    }
                    if (user == null) {
                    	logger.info("=====>>>> "+user);
                        NonceStorage nonceStorage = new RequestNonceStorage(request);
                        String nonce = nonceGenerator.generateNonce();
                        nonceStorage.setState(nonce);
                        model.addAttribute("state", nonce);
                        request.setAttribute("state", nonce);
                        if (!jpNum.isEmpty()) {
                            cardDetailsBean = amexCardService.getICICIOfferByJPNum(jpNum, cardDetailsBean);
                            iciciBean.setJetpriviligemembershipNumber(jpNum);
                            iciciBean.setJetpriviligemembershipTier(memberDetailsService.getTier(jpNum));
                        }
                        model.addAttribute(BEAN_NAME, iciciBean);
                    } else {

                        String jpNumber = getLoggedInUser(request);

                        if (errorMsg.isEmpty()) {
                            memberDetailsService.getMemberDetailsICICI(jpNumber, iciciBean);
                        }

                        model.addAttribute(BEAN_NAME, iciciBean);
                        model.addAttribute("isLoginUser", "Yes");
                    }
                }
                logger.info("formnumberr" + formNumber);
                model.addAttribute("formNumber", formNumber);
                model.addAttribute("cardBean", cardDetailsBean);
                iciciBean.setOfferId(cardDetailsBean.getOfferId());
            } else {
                return ERROR_URL;
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardController applyiciciCardPage() :", e);
        }
//                        return ERROR_URL;

       // logger.info("test logger---"+getEncValue("roshan"));
        return "applyIciciCard";

    }

    @RequestMapping(value = "/icici-apply-form/{cardName}", method = RequestMethod.POST)
    public String applyiciciCardSubmit(@PathVariable(FLASH_CARDNAME) String cardName, HttpServletRequest request,
            @ModelAttribute("iciciBean") IciciBean iciciBean, ModelMap model, RedirectAttributes redirectAttribute) {
        String response = "", tuResponse = "", iciciResponse = "";
        String spMailingID = "";
        String spUserID = "";
        String spJobID = "";
        String spReportId = "";
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        try {
//        	logger.info("test logger post---"+getEncryptedData("17-12-1987"));
//            System.out.println("---->"+iciciBean.getOtpVal());
            request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
            spMailingID = CommonUtils.nullSafe(request.getParameter("spMailingID"), "");
            spUserID = CommonUtils.nullSafe(request.getParameter("spUserID"), "");
            spJobID = CommonUtils.nullSafe(request.getParameter("spJobID"), "");
            spReportId = CommonUtils.nullSafe(request.getParameter("spReportId"), "");

            utm_source = CommonUtils.nullSafe(request.getParameter("utm_source"), "");
            utm_medium = CommonUtils.nullSafe(request.getParameter("utm_medium"), "");
            utm_campaign = CommonUtils.nullSafe(request.getParameter("utm_campaign"), "");
            if (!spMailingID.isEmpty()) {
                trackCode = "?spMailingID=" + spMailingID + "&spUserID=" + spUserID + "&spJobID=" + spJobID + "&spReportId=" + spReportId;
            }
            if (!utm_source.isEmpty()) {
                trackCode = "?utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
                iciciBean.setUtmSource(utm_source);
                iciciBean.setUtmMedium(utm_medium);
                iciciBean.setUtmCampaign(utm_campaign);
            }

            request.getSession().setAttribute("redirectUrl", "icici-apply-form/" + cardName + trackCode);
            String pid = (String) request.getSession().getAttribute("PID");//new
            logger.info(pid + "------PID FOR JP NUMBER----" + iciciBean.getJetpriviligemembershipNumber());
//                         logger.info("--------iciciBean.getRandomNumber()------>"+iciciBean.getRandomNumber());

            iciciBean.setPid(pid);//new
            Map<String, String> allApiResponse = new HashMap<String, String>();

            String random_no = iciciBean.getRandomNumber();
            String[] rndmno = random_no.split(",");
            iciciBean.setRandomNumber(rndmno[0]);
            logger.info("--------iciciBean.getRandomNumber()------>" + iciciBean.getRandomNumber());

            allApiResponse = amexCardService.applyIciciCardWebService(iciciBean, strSoapURL1, soapActEnc);
//System.out.println("con****11**"+!"GOLD".equalsIgnoreCase(iciciBean.getJetpriviligemembershipTier()));
            //System.out.println("con*$*11****"+!"PLATINUM".equalsIgnoreCase(iciciBean.getJetpriviligemembershipTier()));
            
//            System.out.println("allApiResponse   :"+allApiResponse.toString());
            for (Entry<String, String> entry : allApiResponse.entrySet()) {
                if (!"GOLD".equalsIgnoreCase(iciciBean.getJetpriviligemembershipTier()) && !"PLATINUM".equalsIgnoreCase(iciciBean.getJetpriviligemembershipTier())) {
                    System.out.println("---->1st insdie if ");

                    if (entry.getKey().equals("TuResponse")) {
//                System.out.println("TuResponse"+entry.getValue());
                        tuResponse = entry.getValue();
                    }
                }
                if (entry.getKey().equals("ICICIResponse")) {
//                System.out.println("ICICIResponse"+entry.getValue());
                    iciciResponse = entry.getValue();
                }
            }
            logger.info("tuResponse---->" + tuResponse);
//            logger.info("tuResponse Encrypted---->" + tuResponse);
            logger.info("iciciResponse---->" + iciciResponse);
//            logger.info("iciciResponse Encrypted---->" + iciciResponse);

//            response = amexCardService.applyIciciCardWebService(iciciBean);
            String resStr = "", pcnNumber = "", iciciresult = "", icicieligibility = "", icicidescription = "";

            ObjectMapper obmentityuser = new ObjectMapper();
            LinkedHashMap<String, String> tabledata_HM = new LinkedHashMap<String, String>();
//            System.out.println("con******"+!"GOLD".equalsIgnoreCase(iciciBean.getJetpriviligemembershipTier()));
//            System.out.println("con*$*****"+!"PLATINUM".equalsIgnoreCase(iciciBean.getJetpriviligemembershipTier()));
            
            if (!"GOLD".equalsIgnoreCase(iciciBean.getJetpriviligemembershipTier()) && !"PLATINUM".equalsIgnoreCase(iciciBean.getJetpriviligemembershipTier())) {
//                System.out.println("------controller---inside if-->" + iciciBean.getJetpriviligemembershipTier());

                tabledata_HM = (LinkedHashMap<String, String>) obmentityuser.readValue(tuResponse, Map.class);
                for (Map.Entry<String, String> entry : tabledata_HM.entrySet()) {

                    if (entry.getKey().equals("ApplicationId")) {

                        pcnNumber = entry.getValue();
                    }
                    if (entry.getKey().equals("Decision")) {
                        resStr = entry.getValue();
                    }

                }
            }
//            System.out.println("====>"+iciciResponse);
            try {
                if (iciciResponse != null && !("null").equalsIgnoreCase(iciciResponse)) {
//                System.out.println("inside if");

//                    ObjectMapper mapper = new ObjectMapper();
//                    Map<String, String> map1;
//                    map1 = mapper.readValue(iciciResponse.replace("\\", ""), Map.class);
//                    for (Map.Entry<String, String> entry : map1.entrySet()) {
//                        String key = entry.getKey();
//                        if (key.equals("Result")) {
//                            iciciresult = entry.getValue();
////                                System.out.println("iciciresult"+iciciresult);
//                        } else if (key.equals("Eligibility")) {
//                            icicieligibility = entry.getValue();
//                        } else if (key.equals("Description")) {
//                            icicidescription = entry.getValue();
//                        }
//
//                    }
                	if(!iciciResponse.isEmpty())
                	{
                	String iciciId = iciciResponse.substring(iciciResponse.indexOf("#") + 1, iciciResponse.indexOf("|"));
            		
            		boolean succFlg = iciciResponse.contains("Data Saved Successfully");
            		boolean dupFlg = iciciResponse.contains("Duplicate Data");
            		boolean blockedFlg = iciciResponse.contains("Data Blocked for Telecalling");
            		
            		if(succFlg)
            		{
            			iciciresult = "Success";
            			icicidescription = "Data Saved Successfully";
            			icicieligibility = "Eligible"; 
            		}
            		else if(dupFlg)
            		{
            			iciciresult = "Fail";
            			icicidescription = "Duplicate Data";
            			icicieligibility = "Not Eligible";
            		}
            		else if(blockedFlg)
            		{
            			iciciresult = "Fail";
            			icicidescription = "Data Blocked for Telecalling";
            			icicieligibility = "Not Eligible";
            		}
                	}
            		
                }
            } catch (Exception e) {
                logger.error("Exception at icici card controller" + e);
            }
            List<CardDetailsBean> failStatuslist = null;
            List<CardDetailsBean> successStatuslist = null;
            String statusForSms = "";
            //cr3 start
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("MM/DD/YYYY");
            String strDate = formatter.format(date);
            //cr3 end
//            if (iciciresult.equalsIgnoreCase("Success") && icicieligibility.equalsIgnoreCase("Eligible") && icicidescription.equalsIgnoreCase("Lead Captured Eligible")) {
            if (iciciresult.equalsIgnoreCase("Success") && icicieligibility.equalsIgnoreCase("Eligible")) {
                successStatuslist = commonService.getBankwiseRsponseByStatus("sucEliLeadCapt", 2);
                statusForSms = "Provisionally Approved";
// 1 sucEliLeadCapt

            } 
//            else if (iciciresult.equalsIgnoreCase("Success") && icicieligibility.equalsIgnoreCase("Not Eligible") && icicidescription.equalsIgnoreCase("Lead Captured Not Eligible")) {
            else if (iciciresult.equalsIgnoreCase("Success") && icicieligibility.equalsIgnoreCase("Not Eligible")) {
                successStatuslist = commonService.getBankwiseRsponseByStatus("sucNeliNleadCapt", 2);
                statusForSms = "Reject";
// 2 sucNeliNleadCapt

            } else if (iciciresult.equalsIgnoreCase("Fail") && icicieligibility.equalsIgnoreCase("Eligible")) {
                if (icicidescription.equalsIgnoreCase("Duplicate Entry Eligible")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEliDupliEntry", 2);
                    statusForSms = "Provisionally Approved";
// 3 failEliDupliEntry

                }
                if (icicidescription.equalsIgnoreCase("In-Valid Inputs - Blank parameters found")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEliBlankParam", 2);
                    statusForSms = "Try Again";
// 4 failEliBlankParam
                }
                if (icicidescription.equalsIgnoreCase("In-Valid Inputs - Server Side validation failed")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEliServSide", 2);
                    statusForSms = "Provisionally Approved";
// 5 failEliServSide
                }
            } else if (iciciresult.equalsIgnoreCase("Fail") && icicieligibility.equalsIgnoreCase("Not Eligible")) {
                if (icicidescription.equalsIgnoreCase("Duplicate Entry Not Eligible")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failNeliDupliEntry", 2);
                    statusForSms = "Reject";
// 6 failNeliDupliEntry
                }
                if (icicidescription.equalsIgnoreCase("In-Valid Inputs - Server Side validation failed")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failNeliServSide", 2);
                    statusForSms = "Try Again";
// 7 failNeliServSide
                }
                if (icicidescription.equalsIgnoreCase("In-Valid Inputs - Blank parameters found")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failNeliBlankParam", 2);
                    statusForSms = "Try Again";
// 8 failNeliBlankParam
                }
                if (icicidescription.equalsIgnoreCase("API credentials do not match. Please contact Administrator.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failNeliApiCred", 2);
                    statusForSms = "Try Again";
// 9 failNeliApiCred
                }
                if (icicidescription.equalsIgnoreCase("Please provide inputs.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failNeliprovideInput", 2);
                    statusForSms = "Try Again";
// 10 failNeliprovideInput
                }
            } else if (iciciresult.equalsIgnoreCase("Fail") && icicieligibility.equalsIgnoreCase("")) {

                if (icicidescription.equalsIgnoreCase("In-Valid Basic Authorization")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failBasicAuth", 2);
                    statusForSms = "Try Again";
// 11 failBasicAuth
                }
                if (icicidescription.equalsIgnoreCase("Error-0008. Unauthorised User. Please contact Administrator.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failUnauthUser", 2);
                    statusForSms = "Try Again";
// 12 failUnauthUser
                }
                if (icicidescription.equalsIgnoreCase("Error-0007.Please contact administrator. Timeout expired.  The timeout period elapsed prior to obtaining a connection from the pool.  This may have occurred because all pooled connections were in use and max pool size was reached.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failTimeout", 2);
                    statusForSms = "Try Again";
//     13  failTimeout
                }
                if (icicidescription.equalsIgnoreCase("Error-0007.Please contact administrator. String was not recognized as a valid DateTime.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failNvalidDate", 2);
                    statusForSms = "Try Again";
//  14     failNvalidDate
                }
                if (icicidescription.equalsIgnoreCase("Error-0001.Please contact Administrator.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEr1Adm", 2);
                    statusForSms = "Try Again";
// 15 failEr1Adm
                }
                if (icicidescription.equalsIgnoreCase("Error-0002.Please contact Administrator")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEr2Adm", 2);
                    statusForSms = "Try Again";
// 16 failEr2Adm
                }

                if (icicidescription.equalsIgnoreCase("Error-0003.Please contact Administrator.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEr3Adm", 2);
                    statusForSms = "Try Again";
//17  failEr3Adm
                }
                if (icicidescription.equalsIgnoreCase("Error-0004.Please contact Administrator.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEr4Adm", 2);
                    statusForSms = "Try Again";
// 18 failEr4Adm
                }
                if (icicidescription.equalsIgnoreCase("Error-0005.Please contact Administrator.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEr5Adm", 2);
                    statusForSms = "Try Again";
// 19 failEr5Adm
                }
                if (icicidescription.equalsIgnoreCase("Error-0006.Please contact Administrator.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEr6Adm", 2);
                    statusForSms = "Try Again";
// 20 failEr6Adm
                }
                if (icicidescription.equalsIgnoreCase("Error-0007.Please contact Administrator.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEr7Adm", 2);
                    statusForSms = "Try Again";
//21  failEr7Adm
                }
                if (icicidescription.equalsIgnoreCase("Error-0008.Unauthorised User. Please contact Administrator.")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEr8Adm", 2);
                    statusForSms = "Try Again";
// 22 failEr8Adm
                }
                if (icicidescription.equalsIgnoreCase("Error-0009 - Special characters found")) {
                    failStatuslist = commonService.getBankwiseRsponseByStatus("failEr9Adm", 2);
                    statusForSms = "Try Again";

// 23 failEr9Adm
                }
            }
            String cardNameSms = cardDetailsService.getCardDetailsById(iciciBean.getCpNo()).getCardName();
            if ("DVE".equalsIgnoreCase(pcnNumber)) {
                redirectAttribute.addFlashAttribute("iciciBean", iciciBean);
                redirectAttribute.addFlashAttribute("errorMsg", resStr);
//                 return "redirect:/request-icici-exception";
                return "redirect:/icici-apply-form/" + cardName;
            } else if ("Success".equals(iciciresult)) {

//                System.out.println("successStatuslist"+successStatuslist);
                String title = "", messagecontent = "";
                if (successStatuslist != null && !successStatuslist.isEmpty()) {
                    for (CardDetailsBean b : successStatuslist) {
                        title = b.getBpSatatuTitle();
                        messagecontent = b.getMessageContent();
                        if (messagecontent.contains("{applicationId}") == true) {
                            messagecontent = messagecontent.replace("{applicationId}", iciciBean.getReferenceId());
                        }
                        if (messagecontent.contains("{cardName}") == true) {
                            messagecontent = messagecontent.replace("{cardName}", cardDetailsService.getCardDetailsById(iciciBean.getCpNo()).getCardName());
                        }
                    }

                } else {
                    title = "Oops!";
                    messagecontent = "Your Transaction could not be processed. We regret the inconvenience. Please try again.";

                }
                if (statusForSms.equalsIgnoreCase("Try Again")) {                   
                	//commonService.buildSMSForApplication("", sendGridTryAgainId, iciciBean.getEmail(), iciciBean.getFname(), iciciBean.getLname(), "2", "Try Again", cardNameSms, iciciDays, iciciBean.getMobile(), iciciBean.getFullName(), String.valueOf(iciciBean.getAppFormNo()), iciciBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_icictry");
                } else if (statusForSms.equalsIgnoreCase("Reject")) {
                	//commonService.buildSMSForApplication("", sendGridRejectId, iciciBean.getEmail(), iciciBean.getFname(), iciciBean.getLname(), "2", "Reject", cardNameSms, iciciDays, iciciBean.getMobile(), iciciBean.getFullName(), String.valueOf(iciciBean.getAppFormNo()), iciciBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_icicrej");
                } else if (statusForSms.equalsIgnoreCase("Provisionally Approved")) {
                	//commonService.buildSMSForApplication("", sendGridProviApprovedId, iciciBean.getEmail(), iciciBean.getFname(), iciciBean.getLname(), "2", "Provisionally Approved", cardNameSms, iciciDays, iciciBean.getMobile(), iciciBean.getFullName(), String.valueOf(iciciBean.getAppFormNo()), iciciBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_icicprapp");

                } else {
                	//commonService.buildSMSForApplication("", sendGridTryAgainId, iciciBean.getEmail(), iciciBean.getFname(), iciciBean.getLname(), "2", "Try Again", cardNameSms, iciciDays, iciciBean.getMobile(), iciciBean.getFullName(), String.valueOf(iciciBean.getAppFormNo()), iciciBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_icictry");

                }
//                               System.out.println("title===>"+title+" messagecontent===>"+messagecontent);
                redirectAttribute.addFlashAttribute("cardName", cardDetailsService.getCardDetailsById(iciciBean.getCpNo()).getCardName());
                redirectAttribute.addFlashAttribute("ICICIID", iciciBean.getReferenceId());
                redirectAttribute.addFlashAttribute("Statustitle", title);
                redirectAttribute.addFlashAttribute("messagecontent", messagecontent);

                return "redirect:/request-icici-success";
            } else if ("Fail".equals(iciciresult)) {
                String title = "", messagecontent = "";
//             System.out.println("failStatuslist====>"+failStatuslist);
                if (failStatuslist != null && !failStatuslist.isEmpty()) {
                    for (CardDetailsBean b : failStatuslist) {
                        title = b.getBpSatatuTitle();
                        messagecontent = b.getMessageContent();
                        if (messagecontent.contains("{applicationId}") == true) {
                            messagecontent = messagecontent.replace("{applicationId}", iciciBean.getReferenceId());
                        }
                        if (messagecontent.contains("{cardName}") == true) {
                            messagecontent = messagecontent.replace("{cardName}", cardDetailsService.getCardDetailsById(iciciBean.getCpNo()).getCardName());
                        }
                    }

                } else {
                    title = "Oops!";
                    messagecontent = "Your Transaction could not be processed. We regret the inconvenience. Please try again.";

                }

                if (statusForSms.equalsIgnoreCase("Try Again")) {
                	 //commonService.buildSMSForApplication("", sendGridTryAgainId, iciciBean.getEmail(), iciciBean.getFname(), iciciBean.getLname(), "2", "Try Again", cardNameSms, iciciDays, iciciBean.getMobile(), iciciBean.getFullName(), String.valueOf(iciciBean.getAppFormNo()), iciciBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_icictry");
                } else if (statusForSms.equalsIgnoreCase("Reject")) {
                	//commonService.buildSMSForApplication("", sendGridRejectId, iciciBean.getEmail(), iciciBean.getFname(), iciciBean.getLname(), "2", "Reject", cardNameSms, iciciDays, iciciBean.getMobile(), iciciBean.getFullName(), String.valueOf(iciciBean.getAppFormNo()), iciciBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_icicrej");

                } else if (statusForSms.equalsIgnoreCase("Provisionally Approved")) {
                	//commonService.buildSMSForApplication("", sendGridProviApprovedId, iciciBean.getEmail(), iciciBean.getFname(), iciciBean.getLname(), "2", "Provisionally Approved", cardNameSms, iciciDays, iciciBean.getMobile(), iciciBean.getFullName(), String.valueOf(iciciBean.getAppFormNo()), iciciBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_icicprapp");

                } else {
                	//commonService.buildSMSForApplication("", sendGridTryAgainId, iciciBean.getEmail(), iciciBean.getFname(), iciciBean.getLname(), "2", "Try Again", cardNameSms, iciciDays, iciciBean.getMobile(), iciciBean.getFullName(), String.valueOf(iciciBean.getAppFormNo()), iciciBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_icictry");

                }
//                    System.out.println("title===>"+title+" messagecontent===>"+messagecontent);
                redirectAttribute.addFlashAttribute("cardName", cardDetailsService.getCardDetailsById(iciciBean.getCpNo()).getCardName());
                redirectAttribute.addFlashAttribute("ICICIID", iciciBean.getReferenceId());
                redirectAttribute.addFlashAttribute("Statustitle", title);
                redirectAttribute.addFlashAttribute("messagecontent", messagecontent);
//
//                  
                return "redirect:/request-icici-fail";
            } else if ("Declined".equals(resStr)) {
                List<CardDetailsBean> statuslist = null;
                statuslist = commonService.getBankwiseRsponseByStatus("Declined", 2);
                String title = "", messagecontent = "";
//             System.out.println("statuslist====>"+statuslist);
                if (statuslist != null && !statuslist.isEmpty()) {
                    for (CardDetailsBean b : statuslist) {
                        title = b.getBpSatatuTitle();
                        messagecontent = b.getMessageContent();
                        if (messagecontent.contains("{applicationId}") == true) {
                            messagecontent = messagecontent.replace("{applicationId}", iciciBean.getReferenceId());
                        }
                        if (messagecontent.contains("{cardName}") == true) {
                            messagecontent = messagecontent.replace("{cardName}", cardDetailsService.getCardDetailsById(iciciBean.getCpNo()).getCardName());
                        }
                    }

                } else {
                    title = "Oops!";
                    messagecontent = "Your Transaction could not be processed. We regret the inconvenience. Please try again.";

                }
                //commonService.buildSMSForApplication("", sendGridRejectId, iciciBean.getEmail(), iciciBean.getFname(), iciciBean.getLname(), "2", "Reject", cardNameSms, iciciDays, iciciBean.getMobile(), iciciBean.getFullName(), String.valueOf(iciciBean.getAppFormNo()), iciciBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_icicrej");

                redirectAttribute.addFlashAttribute("Statustitle", title);
                redirectAttribute.addFlashAttribute("messagecontent", messagecontent);
                redirectAttribute.addFlashAttribute("cardName", cardDetailsService.getCardDetailsById(iciciBean.getCpNo()).getCardName());
                return "redirect:/icici-approval-pending";

            } else {
                List<CardDetailsBean> statuslist = null;
                String title = "", messagecontent = "";

                statuslist = commonService.getBankwiseRsponseByStatus("Rejected", 2);
//              System.out.println("rejected statuslist====>"+statuslist);
                if (statuslist != null && !statuslist.isEmpty()) {
//              System.out.println("at if");
                    for (CardDetailsBean b : statuslist) {
                        title = b.getBpSatatuTitle();
                        messagecontent = b.getMessageContent();
                        if (messagecontent.contains("{applicationId}") == true) {
                            messagecontent = messagecontent.replace("{applicationId}", iciciBean.getReferenceId());
                        }
                        if (messagecontent.contains("{cardName}") == true) {
                            messagecontent = messagecontent.replace("{cardName}", cardDetailsService.getCardDetailsById(iciciBean.getCpNo()).getCardName());
                        }
                    }

                } else {
//              System.out.println("at elssed");

                    title = "Oops!";
                    messagecontent = "Your Transaction could not be processed. We regret the inconvenience. Please try again.";

                }
//                    System.out.println("title==>"+title+" -->message"+messagecontent);
                //commonService.buildSMSForApplication("", sendGridTryAgainId, iciciBean.getEmail(), iciciBean.getFname(), iciciBean.getLname(), "2", "Try Again", cardNameSms, iciciDays, iciciBean.getMobile(), iciciBean.getFullName(), String.valueOf(iciciBean.getAppFormNo()), iciciBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_icictry");

                redirectAttribute.addFlashAttribute("Statustitle", title);
                redirectAttribute.addFlashAttribute("messagecontent", messagecontent);
                redirectAttribute.addFlashAttribute("cardName", cardDetailsService.getCardDetailsById(iciciBean.getCpNo()).getCardName());
                redirectAttribute.addFlashAttribute("iciciException", "Exception");
                return "redirect:/request-icici-exception";

            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardController applyICICICardWebService() :", e);
        }
        return REDIRECT_URL + cardName;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/provisionally-approved")
    public String iciciRequestapprovedPage(HttpServletRequest request, ModelMap model, @ModelAttribute("cardName") String cardName) {
        model.addAttribute("title", "Co Brand Card Request Approved - JetPrivilege");
        model.addAttribute("description", "Congratulations your application has been approved subject to KYC document collection & verification. One of the bank representatives will shortly get in touch with you.");
        model.addAttribute("keyword", "co brand card approved, card application approved, co brand card request approved ");
        if (null != cardName && !cardName.isEmpty()) {
            return "iciciapproval";
        } else {
            return "/jsp/portal/error.jsp";
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/provisionally-icici-approved")
    public String iciciRequestRefferedPage(HttpServletRequest request, ModelMap model, @ModelAttribute("cardName") String cardName) {
        request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
        model.addAttribute("title", "Co Brand Card Request Approved - JetPrivilege");
        model.addAttribute("description", "Congratulations your application has been approved subject to KYC document collection & verification. One of the bank representatives will shortly get in touch with you.");
        model.addAttribute("keyword", "co brand card approved, card application approved, co brand card request approved ");
        if (null != cardName && !cardName.isEmpty()) {
            return "icicireferred";
        } else {
            return "/jsp/portal/error.jsp";
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/icici-approval-pending")
    public String iciciRequestdisapprovedPage(HttpServletRequest request, ModelMap model, @ModelAttribute("cardName") String cardName) {
        request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
        model.addAttribute("title", "Co Brand Card Request Disapproved - JetPrivilege");
        model.addAttribute("description", "Sorry, we regret that your card request has not been approved. However you can check out other cards you are eligible for.");
        model.addAttribute("keyword", "co brand card dis approved, card application dis approved, co brand card request dis approved ");
        if (null != cardName && !cardName.isEmpty()) {
            return "icicidecline";
        } else {
            return "/jsp/portal/error.jsp";
        }

    }

    @RequestMapping(method = RequestMethod.GET, value = "/request-icici-exception")
    public String serviceResponseEmpty(HttpServletRequest request, ModelMap model, @ModelAttribute("cardName") String cardName) {
        request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
        model.addAttribute("title", "Co Brand Card Request Disapproved - JetPrivilege");
        model.addAttribute("description", "Sorry, we regret that your card request has not been approved. However you can check out other cards you are eligible for.");
        model.addAttribute("keyword", "co brand card dis approved, card application dis approved, co brand card request dis approved ");
//        return "iciciException"; 
        if (null != cardName && !cardName.isEmpty()) {
            return "iciciException";
        } else {
            return "/jsp/portal/error.jsp";
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/request-icici-success")
    public String serviceResponseIciciSuccess(HttpServletRequest request, ModelMap model, @ModelAttribute("cardName") String cardName) {
        request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
        model.addAttribute("title", "Co Brand Card Request Disapproved - JetPrivilege");
        model.addAttribute("description", "Sorry, we regret that your card request has not been approved. However you can check out other cards you are eligible for.");
        model.addAttribute("keyword", "co brand card dis approved, card application dis approved, co brand card request dis approved ");
//        return "iciciException"; 
        if (null != cardName && !cardName.isEmpty()) {
            return "icicisuccess";
        } else {
            return "/jsp/portal/error.jsp";
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/request-icici-fail")
    public String serviceResponseIciciFail(HttpServletRequest request, ModelMap model, @ModelAttribute("cardName") String cardName) {
        request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
        model.addAttribute("title", "Co Brand Card Request Disapproved - JetPrivilege");
        model.addAttribute("description", "Sorry, we regret that your card request has not been approved. However you can check out other cards you are eligible for.");
        model.addAttribute("keyword", "co brand card dis approved, card application dis approved, co brand card request dis approved ");
//        return "iciciException"; 
        if (null != cardName && !cardName.isEmpty()) {
            return "icicifail";
        } else {
            return "/jsp/portal/error.jsp";
        }
    }

    @RequestMapping(value = "/icici-apply-form/{cardName}/{jpNumber}")
    public String iciciGetOfferDetailsApplicationForm(@PathVariable("cardName") String cardName, @PathVariable("jpNumber") String jpNumber, RedirectAttributes redirect) {
        try {
            redirect.addFlashAttribute("jpNumber", jpNumber);
            redirect.addFlashAttribute("isOffer", "Yes");
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardController getOfferDetailsApplicationForm() :", e);
        }

                       // return ERROR_URL;

        return "redirect:/icici-apply-form/" + cardName;
    }

    @RequestMapping(value = "/getTier1", method = RequestMethod.GET)
    @ResponseBody
    public String getICICIMemberTierFunction(@RequestParam("jpNum") String jpnumber) {
        IciciBean iciciBean = new IciciBean();
        String tier = "";
        try {

            memberDetailsService.getMemberDetailsICICI(jpnumber, iciciBean);
            if (null != iciciBean.getJetpriviligemembershipTier()) {
                tier = iciciBean.getJetpriviligemembershipTier();
            } else {
                tier = "";
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardController getMemberTierFunction() :", e);
        }
        return tier;
    }

    @RequestMapping(value = "/getOffer1", method = RequestMethod.GET)
    @ResponseBody
    public String getICICIOffer(@RequestParam("jpNum") String jpnumber, @RequestParam("cpNo") String cpNo) {
        JSONObject json = null;
        try {
            json = amexCardService.getICICIOfferByJPNumber(jpnumber, cpNo);
            if (null != json) {
                return json.toString();
            } else {
                return "";
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardController getOffer() :", e);
        }
        return "";
    }

    @RequestMapping(value = "/getCity1", method = RequestMethod.GET)
    @ResponseBody
    public String getICICICityMappings(@RequestParam("pinCode") String pinCode) {
        CityMappingBean bean = new CityMappingBean();
        try {
            bean = commonService.getICICICityMapping(pinCode);
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardController getCityMappings() :", e);
        }
        return GsonUtil.toJson(bean);
    }

    @RequestMapping(value = "/getCitiesByState1", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getICICICitiesByState(@RequestParam("state") String state) {
        Map<String, String> map = new HashMap<>();
        try {
            map = commonService.getICICICitiesByState(state);
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICadrController getCitiesByState() :", e);
        }
        return map;
    }

    @RequestMapping(value = "/getICICIStateByCity", method = RequestMethod.GET)
    @ResponseBody
    public String getICICIStateByCites(@RequestParam("city") String city) {
        String state = "";
        try {
            state = commonService.getICICIStateByCity(city);
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardController getStateByCity() :", e);
        }
        return state;
    }

    @RequestMapping(value = "/icicicheckExcludePincode", method = RequestMethod.GET)
    @ResponseBody
    public String checkiciciExcludePincode(@RequestParam("pinCode") String pinCode, @RequestParam("cpNo") String cpNo) {
        Boolean isExcluded = false;
        try {
            isExcluded = commonService.checkICICIExcludePincode(pinCode, cpNo);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexApplicationFormController checkExcludePincode() :", e);
        }
        return isExcluded ? "excluded" : "";
    }

    public String getLoggedInUser(HttpServletRequest request) {
        return (String) request.getSession().getAttribute("loggedInUser");

    }

    @RequestMapping(value = "/AgeCriteriaageabove", method = RequestMethod.GET)
    @ResponseBody
    public List checkiciciagecriteriaabove(@RequestParam("bpNo") String bpNo, @RequestParam("cpNo") String cpNo, @RequestParam("EmpType") String EmpType, @RequestParam("City") String City) {
        List ageabove = null;
        try {
            ageabove = commonService.getagecriteriaAgeabove(bpNo, cpNo, EmpType, City);

        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexApplicationFormController checkiciciagecriteria() :", e);
        }
        return ageabove;
    }

    @RequestMapping(value = "/AgeCriteriaagebelow", method = RequestMethod.GET)
    @ResponseBody
    public List checkiciciagecriteriabelow(@RequestParam("bpNo") String bpNo, @RequestParam("cpNo") String cpNo, @RequestParam("EmpType") String EmpType, @RequestParam("City") String City) {
        List agebelow = null;
        try {
            agebelow = commonService.getagecriteriaAgebelow(bpNo, cpNo, EmpType, City);

        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexApplicationFormController checkiciciagecriteria() :", e);
        }
        return agebelow;
    }

    @RequestMapping(value = "/companyNameByLetter", method = RequestMethod.GET)
    @ResponseBody
    public List companyNameByLetter(@RequestParam("companyName") String companyName) {
        List company = null;
        try {
            company = commonService.getCompanyNamesByLetter(companyName);

        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexApplicationFormController checkiciciagecriteria() :", e);
        }
        return company;
    }

    //         sprint 53 story
    @RequestMapping(value = "/saveIciciFormData", method = RequestMethod.POST)
    @ResponseBody
    public String saveIciciFormData(@ModelAttribute("iciciBean") IciciBean iciciBean) {
        String ststus = "success";
        try {
            logger.info("form number saveFormData()" + iciciBean.getFormNumber());
//            System.out.println("ICICI form number saveFormData()" + iciciBean.getFormNumber());

            amexCardService.saveOrUpdateFormData(iciciBean, 0);
            logger.info("------------>" + iciciBean.getBpNo());
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController saveFormData() :", e);
        }
        return ststus;
    }

    @RequestMapping(value = "/SilverpopIcici", method = RequestMethod.POST)
    @ResponseBody
    public String SilverpopAmex(@ModelAttribute("iciciBean") IciciBean iciciBean) {
        try {
            logger.info("EMAIL SEND ---- Amex Silver POP AMEX first name: " + iciciBean.getFname() + " mobile no: " + iciciBean.getMobile() + " Email-id: " + iciciBean.getEmail() + " Form No: " + iciciBean.getFormNumber());

            amexCardService.FormInactiveSilverPopApiIcici(iciciBean);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController SilverpopAmex() :", e);
        }
        return "";
    }

    @RequestMapping(value = "/getIciciMemberDetail", method = RequestMethod.GET)
    @ResponseBody
    public String getmembermobile(@RequestParam("jpNum") String jpNumber) {
        IciciBean icicibean = new IciciBean();
        memberDetailsService.getMemberDetailsICICI(jpNumber, icicibean);
        System.out.println("amexbean" + icicibean);
        return icicibean.getMobile();

    }

    @RequestMapping(value = "/getIcicicMemberData", method = RequestMethod.GET)
    @ResponseBody
    public IciciBean getmember(@RequestParam("jpNum") String jpNumber) {
        System.out.println("at getmemberdetail");
        IciciBean icicibean = new IciciBean();
        memberDetailsService.getMemberDetailsICICI(jpNumber, icicibean);
        System.out.println("amexbean" + icicibean + "fname" + icicibean.getFname());
        return icicibean;

    }

    
    @RequestMapping(value = "/getjpTier", method = RequestMethod.GET)
    @ResponseBody
    public String getJPMemberTier(@RequestParam("jpNum") String jpnumber, HttpServletRequest request) {
        String tier = "";
        try {
            tier = memberDetailsService.getJPTier(jpnumber);
        	//tier = "BASE";
            HttpSession tiersession = request.getSession();
             
        	logger.info("before session :" + tier);

            tiersession.setAttribute("userTier", tier);
            logger.info("after session  :" + request.getSession().getAttribute("userTier"));
        } catch (Exception e) {
            logger.error("@@@@ Exception in getMemberTierFunction() :", e);
        }
        return tier;
    }
    
    
    
    public String  getEncValue(String strval)
    {
	    ICICIEncryptDetail e = new ICICIEncryptDetail();
	    String encryptDataVar = e.encryptData(strval, strSoapURL1, soapActEnc);
	    return encryptDataVar;
    }
    
    @RequestMapping(value = "/getDataPostSignup", method = RequestMethod.GET)
    @ResponseBody
    public CcBeApplicationFormDetails fetchPostSignupData(@RequestParam("formNo") String formNo) {
    	CcBeApplicationFormDetails ccBeApplicationFormDetails = null;
    	logger.info("form number in controller - "+formNo);
    	ccBeApplicationFormDetails = amexCardService.fetchPostSignupMemberDtls(formNo, 0);
        return ccBeApplicationFormDetails;

    }
    
    
}
