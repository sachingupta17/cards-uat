package com.cbc.portal.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cbc.portal.service.CommonService;
import com.cbc.portal.beans.JsonBean;
import com.cbc.portal.utils.EncryptDetail;
import com.cbc.portal.utils.EncryptJpNumberCards;
import com.cbc.portal.utils.GsonUtil;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;

@Controller
public class JPWebserviceController {
	private static Logger logger = Logger.getLogger(JPWebserviceController.class.getName());
      
    @Value("${application.pii.encryption.strSoapURL1}")
	private String strSoapURL1;
    
    @Value("${application.pii.decrypt.strSOAPAction1}")
    private String strSOAPActiondecrypt;
    
    
    @Value("${application.pii.encrypt.strSOAPAction1}")
    private String strSOAPActionencrypt;
    
	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value="/detailsforpcn",method=RequestMethod.POST, produces={"application/json"})
	public @ResponseBody String detailsforPcnNumber(@RequestParam("pcnNumber") String pcnNumber){
		JsonBean amexStatusBean=null;
		try{
			amexStatusBean=commonService.getDetailsforPCN(pcnNumber);
			if(amexStatusBean!=null){
				return GsonUtil.toJson(amexStatusBean);
			}
		}catch(Exception e){
			logger.error("@@@@ Exception in JPWebserviceController at getDetailsforPCN() :",e);
		}
		return GsonUtil.toJson("{}");
	}
	
	
	
	@RequestMapping(value="/detailsforjpnumber",method=RequestMethod.POST, produces={"application/json"})
	public @ResponseBody String detailsforJpNumber(@RequestParam("jpNumber") String jpNumber){
		List<JsonBean> amexStatusBeanList=null;
		List<JsonBean> amexStatusBeanList1=null;
		try{
                        
			amexStatusBeanList=commonService.getDetailsforJpNumber(jpNumber);
//                        amexStatusBeanList1=commonService.getICICIDetailsforJpNumber(jpNumber);
			if(amexStatusBeanList!=null){
				return GsonUtil.toJson(amexStatusBeanList);
			}
//                        else if(amexStatusBeanList1!=null){
//                        return GsonUtil.toJson(amexStatusBeanList1);
//                        }
		}catch(Exception e){
			logger.error("@@@@ Exception in JPWebserviceController at getDetailsforPCN() :",e);
		}
		return GsonUtil.toJson("{}");
	}
        
        @RequestMapping(value="/encryptedAdobe",method=RequestMethod.GET, produces={"application/json"})
	public @ResponseBody String encryptedDataAdobe(@RequestParam("jsonData") String jsonToEncrypt) throws JSONException{
                String encodeURL="";
                String encryptDataVar = "";
                
            try {
                EncryptDetail e=new EncryptDetail();
                encryptDataVar =e.encryptData(jsonToEncrypt,strSoapURL1,strSOAPActionencrypt);
                

              
            } catch (Exception ex) {

                logger.error(ex);
            }
              return encryptDataVar;
        }  
     @RequestMapping(value = "/Encryptjpnumber")
    public @ResponseBody
    String EncryptJpNumber(@RequestParam("jpNumber") String jpNumber) {
        String encodeURL = "";
        try {
            EncryptJpNumberCards e = new EncryptJpNumberCards();
            jpNumber = e.encryptData(jpNumber,strSoapURL1,strSOAPActionencrypt);

            encodeURL = URLEncoder.encode(jpNumber, "UTF-8");

        } catch (Exception ex) {
            logger.error(ex);
        }
        return encodeURL;
    }
        
    
            
@RequestMapping(value = "/getmethod")
    public @ResponseBody String detailsforJpNumber1(HttpServletRequest request) {

        String jpnumber = "";
        String name = "";
        Map<String, String> map = new HashMap<String, String>();
        JSONObject obj = new JSONObject();

                         
        jpnumber = "" + request.getSession().getAttribute("loggedInUser");
        name = "" + request.getSession().getAttribute("loggedInUserName");
  
        map.put("Jpnumber", jpnumber);
        map.put("Name", name);
		return GsonUtil.toJson(map);
	}
    
    @RequestMapping(value = "/loginSessionSet")
    public  @ResponseBody String detailsfor(HttpServletRequest request,@RequestParam("req") String req) {
    
    logger.info("Login Session url : "+req);
//     request.getSession().setAttribute("loggedin", req);
//     request.getSession().setAttribute("login", "1");
//    
     return "";

    }
     @RequestMapping(value = "/currentSession",method=RequestMethod.GET)
    public @ResponseBody void currentSessionset(HttpServletRequest request,@RequestParam("request") String req) {

         
       logger.info("Current Session url : "+req);
     request.getSession().setAttribute("cuurenturl", req);
     logger.info("Current Session url 2: "+request.getSession().getAttribute("cuurenturl"));
     logger.info("Current Session url 2: "+request.getSession().getAttribute("cuurenturl"));
   }

    
    
}
