/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.controller;

import com.cbc.portal.DAO.CardProductDAO;
import com.cbc.portal.DAO.CommonDAO;
import com.cbc.portal.entity.CcOTP;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.utils.CommunicationEngineOtpAPI;
import com.cbc.portal.utils.GsonUtil;
import com.cbc.portal.utils.OTPGeneration;
import com.cbc.portal.utils.SmsService;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Aravind E
 */
@Controller
public class CommonController {

    @Autowired
    CommonDAO commonDAO;
    @Autowired
    private CardProductDAO cardProductDAO;
    @Autowired
    CommonService commonservice;
    @Value("${application.newSms.username}")
    private String smsUserName;

    @Value("${application.newSms.password}")
    private String smsPassword;

    @Value("${application.newSms.sendby}")
    private String smsSendby;

    @Value("${application.newSms.url}")
    private String smsUrl;

    @Value("${application.otpEngine.partnerCode}")
    String partnerCode;
    @Value("${application.otpEngine.partnerTransactionId}")
    String partnerTransactionId;
    @Value("${application.otpEngine.otpType}")
    String otpType;
    @Value("${application.otpEngine.PartnerHashKey}")
    String PartnerHashKey;
    @Value("${application.otpEngine.userName}")
    String userName;
    @Value("${application.otpEngine.password}")
    String password;
    @Value("${application.otpEngine.VerificationUrl}")
    String otpVerifyUrl;
    @Value("${application.otpEngine.GenerationUrl}")
    String otpGenerationUrl;
    @Value("${application.otpEngine.tokenGenearationUrl}")
    String tokenGenerationUrl;

    private Logger logger = Logger.getLogger(AmexCardController.class.getName());

    @Transactional(readOnly = false)
    @RequestMapping(value = "/generateOTP", method = RequestMethod.GET)
    @ResponseBody
    public String generateOTP(HttpServletRequest request, HttpServletResponse response, @RequestParam("mobileNumber") String mobileNumber, @RequestParam("bankNumber") String bankNumber, @RequestParam String formNumber) {
//      String mobieNumber="8291493992"; 
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Accept-Encoding", "gzip, deflate, br");
        response.setHeader("Accept-Language", "en-US,en;q=0.9");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Connection", "keep-alive");

        logger.info("mobilenumber  : " + mobileNumber);
        String str = null;
        String formattedDate = "", otp_transaction_id = "", token = "";
        OTPGeneration oTPGeneration = new OTPGeneration();
        CommunicationEngineOtpAPI c = new CommunicationEngineOtpAPI();
        String otpValue = oTPGeneration.OTPGenertion();
        
        logger.info("Generated OTP "+otpValue);
        Map<String, String> map = new HashMap<String, String>();
        int id = 0;
        try {
            Date d1 = new Date();
            SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
            formattedDate = df.format(d1);
            if (otpValue.length() == 6) {
                String message = "OTP is " + otpValue + " for your card application on cards.intermiles.com. Do not share the OTP with anyone for security reasons.";
                message = URLEncoder.encode(message.trim(), "utf-8");
//          str=SmsService.sendSms(mobileNumber, smsUserName, smsPassword, smsSendby, message, smsUrl);
                str = c.tokenGeneration(userName, password, partnerTransactionId, otpType, partnerCode, mobileNumber, PartnerHashKey, token, otp_transaction_id,otpGenerationUrl,tokenGenerationUrl);

                logger.info("generate otp response at controller==========>" + str);
                System.out.println("############  str  "+str);
//        str=oTPGeneration.SendSMStoUser(mobileNumber,otpValue);

            }
            if (str != null) {
            JSONObject j = new JSONObject(str);
            otp_transaction_id = j.getString("otp_transaction_id");
            token = j.getString("token");
            System.out.println("token---------->" + token);
            

                CcOTP ccOTP = new CcOTP();
                ccOTP.setMobileNumber(mobileNumber);
                ccOTP.setDate(new Date());
                ccOTP.setOtpValue("");
                ccOTP.setFormNUmber(formNumber);
                ccOTP.setStatus(1);
                ccOTP.setOtpTransactionId(otp_transaction_id);
                System.out.println("bankNumber" + bankNumber);
//          System.out.println("bank"+cardProductDAO.getBankPartnerById(Integer.parseInt(bankNumber)));
                if (!bankNumber.equals("")) {
                    ccOTP.setCcBankPartner(cardProductDAO.getBankPartnerById(Integer.parseInt(bankNumber)));
                }
                id = commonDAO.saveOTP(ccOTP);
                System.out.println("id====" + id);

                //str = "A new OTP has been sent to your mobile number " + mobileNumber + " at " + formattedDate;
            }

//      str="OTP Has been Sent your mobile number";
        } catch (Exception Ex) {
            
            
            Ex.printStackTrace();
            logger.error("Exception at CommonController generateOTP() :" + Ex);

            System.out.println("exception ex" + Ex);
        }

        if (id > 0) {
            map.put("mobileNumber", mobileNumber);
            map.put("Time", formattedDate);
            map.put("otpTransactionid", otp_transaction_id);
            map.put("token", token);
        } else {
            map.put("mobileNumber", "");
            map.put("Time", "");
            map.put("otpTransactionid", "");
            map.put("token", "");

        }

        logger.info("Generate OTP Final Response  : " + GsonUtil.toJson(map));
        return GsonUtil.toJson(map);

    }

    @Transactional(readOnly = false)
    @RequestMapping(value = "/regenerateOTP", method = RequestMethod.GET)
    @ResponseBody
    public String regenerateOTP(HttpServletRequest request, HttpServletResponse response, @RequestParam("mobileNumber") String mobileNumber, @RequestParam("bankNumber") String bankNumber, @RequestParam String formNumber, @RequestParam("token") String reqtoken, @RequestParam("otpTransactionId") String otpTransactionId) {
//      String mobieNumber="8291493992"; 
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Accept-Encoding", "gzip, deflate, br");
        response.setHeader("Accept-Language", "en-US,en;q=0.9");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Connection", "keep-alive");

        System.out.println("mobilenumber  : " + mobileNumber);
        String str = null;
        String formattedDate = "", otp_transaction_id = "", token = "";
        OTPGeneration oTPGeneration = new OTPGeneration();
        CommunicationEngineOtpAPI c = new CommunicationEngineOtpAPI();
        String otpValue = oTPGeneration.OTPGenertion();
        Map<String, String> map = new HashMap<String, String>();
        int id = 0;
        try {
            Date d1 = new Date();
            SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
            formattedDate = df.format(d1);
            if (otpValue.length() == 6) {
                String message = "OTP is " + otpValue + " for your card application on cards.intermiles.com. Do not share the OTP with anyone for security reasons.";
                message = URLEncoder.encode(message.trim(), "utf-8");
//          str=SmsService.sendSms(mobileNumber, smsUserName, smsPassword, smsSendby, message, smsUrl);
//                str = c.tokenGeneration(userName, password, partnerTransactionId, "ReGenerate", partnerCode, mobileNumber, PartnerHashKey, reqtoken, otpTransactionId,otpGenerationUrl,tokenGenerationUrl);
                str = c.tokenGeneration(userName, password, partnerTransactionId, "ReSend", partnerCode, mobileNumber, PartnerHashKey, reqtoken, otpTransactionId,otpGenerationUrl,tokenGenerationUrl); // roshan 14Jan-2021

                logger.info("Regenerate otp response at controller==========>" + str);
//        str=oTPGeneration.SendSMStoUser(mobileNumber,otpValue);

            }
            JSONObject j = new JSONObject(str);
            otp_transaction_id = j.getString("otp_transaction_id");
            token = j.getString("token");
            System.out.println("token---------->" + token);
            if (str != null) {

                CcOTP ccOTP = new CcOTP();
                ccOTP.setMobileNumber(mobileNumber);
                ccOTP.setDate(new Date());
                ccOTP.setOtpValue("");
                ccOTP.setFormNUmber(formNumber);
                ccOTP.setStatus(1);
//                ccOTP.setOtpTransactionId(otp_transaction_id);
                System.out.println("bankNumber" + bankNumber);
//          System.out.println("bank"+cardProductDAO.getBankPartnerById(Integer.parseInt(bankNumber)));
                if (!bankNumber.equals("")) {
                    ccOTP.setCcBankPartner(cardProductDAO.getBankPartnerById(Integer.parseInt(bankNumber)));
                }
                id = commonDAO.saveOTP(ccOTP);
                System.out.println("id====" + id);

                //str = "A new OTP has been sent to your mobile number " + mobileNumber + " at " + formattedDate;
            }

//      str="OTP Has been Sent your mobile number";
        } catch (Exception Ex) {
            logger.error("Exception at CommonController generateOTP() :" + Ex);

            System.out.println("exception ex" + Ex);
        }

        if (id > 0) {
            map.put("mobileNumber", mobileNumber);
            map.put("Time", formattedDate);
            map.put("otpTransactionid", otp_transaction_id);
            map.put("token", token);
        } else {
            map.put("mobileNumber", "");
            map.put("Time", "");
            map.put("otpTransactionid", "");
            map.put("token", "");

        }

        logger.info("Re-Generate OTP Final Response  : " + GsonUtil.toJson(map));
        return GsonUtil.toJson(map);

    }

    @RequestMapping(value = "/validateOTP", method = RequestMethod.GET)
    @ResponseBody
    public String validateOTP(@RequestParam("mobileNumber") String mobileNumber, @RequestParam("bankNumber") String bankNumber, @RequestParam String formNumber, @RequestParam("otpTransactionId") String otpTransactionId, @RequestParam("otp") String otp, @RequestParam("token") String reqtoken) {
//      String mobieNumber="8291493992";    
        System.out.println("mobilenumber validate  : " + mobileNumber + "----otpTransactionId-->" + otpTransactionId);
        String str = "";
        String formattedDate = "";
        String result = "", OTP_Verified = "", pendingAttempts = "";

        CommunicationEngineOtpAPI c = new CommunicationEngineOtpAPI();
        try {

            str = commonservice.getOtpForValidate(mobileNumber, bankNumber, formNumber, otpTransactionId);

            logger.info("validate otp response at controller==========>" + str);

            result = c.verificationOtp(userName, password, partnerTransactionId, otpType, partnerCode, mobileNumber, PartnerHashKey, otpTransactionId, otp, reqtoken,otpVerifyUrl);
//      str="OTP Has been Sent your mobile number";
//            JSONObject j = new JSONObject(result);
//            OTP_Verified = j.getString("OTP_Verified");
//            pendingAttempts = j.getString("pendingAttempts");

            logger.info("Validate OTP Final Response  : " + result);
        } catch (Exception Ex) {
            
            logger.error("Exception at CommonContoller validateOTP() :" + Ex);
        }

        return result;

    }

    @RequestMapping(value = "/checkFormStatus", method = RequestMethod.GET)
    @ResponseBody
    public String checkFormStatus(@RequestParam("randomNumber") String randomNumber) {
//      String mobieNumber="8291493992";    
        System.out.println("randomNumber validate  : " + randomNumber);
        String str = "";
        String formattedDate = "";

        Map<String, String> map = new HashMap<String, String>();
        try {

            str = commonservice.checkFormStatus(randomNumber);

//      str="OTP Has been Sent your mobile number";
        } catch (Exception Ex) {
            logger.error("Exception at CommonContoller validateOTP() :" + Ex);
        }

        return str;

    }

}
