/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.controller;

/**
 *
 * @author Aravind E
 */
import com.auth0.NonceGenerator;
import com.auth0.NonceStorage;
import com.auth0.RequestNonceStorage;
import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.EnrollBean;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.service.AmexCardService;
import com.cbc.portal.service.CardDetailsService;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.service.MemberDetailsService;
import com.cbc.portal.service.MemberEnrollService;
import com.cbc.portal.service.RecommendCardService;
import com.cbc.portal.utils.ApplicationPropertiesUtil;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.DecryptJpNumberCards;
import java.net.URLEncoder;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AmexWebViewController {
      private Logger logger = Logger.getLogger(AmexCardController.class.getName());
    private final NonceGenerator nonceGenerator = new NonceGenerator();

    @Autowired
    ApplicationPropertiesUtil applicationPropertiesUtil;

    @Autowired
    AmexCardService amexCardService;

    @Autowired
    CommonService commonService;

    @Autowired
    MemberEnrollService memberEnrollService;

    @Autowired
    MemberDetailsService memberDetailsService;

    @Autowired
    CardDetailsService cardDetailsService;

    @Autowired
    RecommendCardService recommendCardService;
    
     @Value("${application.pii.encryption.strSoapURL1}")
    private String strSoapURL1;

    @Value("${application.pii.decrypt.strSOAPAction1}")
    private String strSOAPActiondecrypt;

    @Value("${application.pii.encrypt.strSOAPAction1}")
    private String strSOAPActionencrypt;
    
    private static final String BEAN_NAME = "amexBean";
    private static final String REDIRECT_URL = "redirect:/apply-form/";
    private static final String ERROR_URL = "/jsp/portal/error.jsp";
    private static final String FLASH_CARDNAME = "cardName";
    private static final String ENROLL_BEAN = "enrollBean";
    private static final String JP_NUMBER = "jpNumber";
    private static final String RANDOM_NUMBER = "randomNumber";
    
     @RequestMapping("/apply---------------------/{cardName}")
    public String applyWebViewAmexCardPage(@PathVariable("cardName") String cardName, HttpServletRequest request,
            ModelMap model, @ModelAttribute("errorMsg") String errorMsg,
            @ModelAttribute(BEAN_NAME) AmexBean amexBean, @ModelAttribute(RANDOM_NUMBER) String randomNumber,
            @ModelAttribute("formNumber") String formNumber, @ModelAttribute(JP_NUMBER) String jpNum) {
        String spMailingID = "";
        String spUserID = "";
        String spJobID = "";
        String spReportId = "";
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        String termsAndCondition = "";
           
        String cName = cardName.replaceAll("-", " ");
        String[] strArray = cName.split(" ");
        StringBuilder newCardName = new StringBuilder();
        logger.info("formNumber at starting of controller===>" + formNumber);
        for (String s : strArray) {
            if ("express".equalsIgnoreCase(s)) {
                s = s + "&reg;";
            }
            newCardName.append(s + " ");
        }
        try {
            spMailingID = CommonUtils.nullSafe(request.getParameter("spMailingID"), "");
            spUserID = CommonUtils.nullSafe(request.getParameter("spUserID"), "");
            spJobID = CommonUtils.nullSafe(request.getParameter("spJobID"), "");
            spReportId = CommonUtils.nullSafe(request.getParameter("spReportId"), "");

            utm_source = CommonUtils.nullSafe(request.getParameter("utm_source"), "");
            utm_medium = CommonUtils.nullSafe(request.getParameter("utm_medium"), "");
            utm_campaign = CommonUtils.nullSafe(request.getParameter("utm_campaign"), "");
            logger.info("AmexCardController ==> utm_source: " + utm_source + " utm_medium: " + utm_medium + " utm_campaign: " + utm_campaign);
            int cpNo = cardDetailsService.getCardIdByName(newCardName.toString().trim());
            if (0 != cpNo) {
                List<String> groupHeaders = null;
                amexBean.setCpNo(cpNo);
                termsAndCondition = commonService.getTermdAndCondition(cpNo);
                groupHeaders = commonService.getGroupHeaders(1);
                CardDetailsBean cardDetailsBean = recommendCardService.getRecommendedCardById(cpNo);
                if (groupHeaders.size() > 3) {
                    groupHeaders.subList(3, groupHeaders.size()).clear();
                }
                model.addAttribute("groupHeadings", groupHeaders);
                model.addAttribute("applicationURL", applicationPropertiesUtil.getApplicationURL());

                if (!spMailingID.isEmpty()) {
                    trackCode = "?spMailingID=" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
                }
                if (!utm_source.isEmpty()) {
                    trackCode = "?utm_source=" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                    amexBean.setUtmSource(utm_source);
                    amexBean.setUtmMedium(utm_medium);
                    amexBean.setUtmCampaign(utm_campaign);
                }
                request.getSession().setAttribute("redirectUrl", "apply-form/" + cardName + trackCode);
                if (!spMailingID.isEmpty()) {
                    trackCode = "lw_" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
                }
                if (!utm_source.isEmpty()) {
                    trackCode = "utm_" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                }
                request.getSession().setAttribute("trackCode", trackCode);
                request.getSession().setAttribute("homeUrl", "apply-form/" + cardName);

                model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getAfTitle().replace("{card name}", cardDetailsBean.getImageText()));
                model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getAfDescription().replace("{card name}", cardDetailsBean.getImageText()));
                model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getAfKeyword());
                model.addAttribute("timer", commonService.getTimer("Timer for Application form update"));
                model.addAttribute("timer1", commonService.getTimer("Timer for Form Inactive"));
                model.addAttribute("genderStatus", commonService.getEnumValues("Gender"));
                model.addAttribute("cities", commonService.getCities());
                model.addAttribute("states", commonService.getStates());
                model.addAttribute("qualification", commonService.getAmexValues("Qualification"));
                model.addAttribute("salaryStatus", commonService.getAmexValues("EMP_STATUS"));
                model.addAttribute(ENROLL_BEAN, new EnrollBean());
                model.addAttribute("callMeBean", new CallMeBean());
                model.addAttribute("termsAndCondition", termsAndCondition);
               
                if (null != randomNumber && !randomNumber.isEmpty()) {
                    amexCardService.getBEAmexFormDetails(randomNumber, amexBean);
                    
                    model.addAttribute(BEAN_NAME, amexBean);
                } else {
                    if (errorMsg.isEmpty()) {
                        formNumber = RandomStringUtils.random(20, true, true);
                        amexBean.setFormNumber(formNumber);
                        logger.info("amexBean.getFormNumber at controller" + amexBean.getFormNumber());
                    }
                  
                        NonceStorage nonceStorage = new RequestNonceStorage(request);
                        String nonce = nonceGenerator.generateNonce();
                        nonceStorage.setState(nonce);
                        model.addAttribute("state", nonce);
                        request.setAttribute("state", nonce);
                        if (!jpNum.isEmpty()) {
                            jpNum = URLEncoder.encode(jpNum, "UTF-8");

                            DecryptJpNumberCards decryptJpnumber = new DecryptJpNumberCards();
                            jpNum = decryptJpnumber.DecryptJpnumber(jpNum, strSoapURL1, strSOAPActiondecrypt);
                            cardDetailsBean = amexCardService.getOfferByJPNum(jpNum, cardDetailsBean);
                            amexBean.setJetpriviligemembershipNumber(jpNum);
                            amexBean.setJetpriviligemembershipTier(memberDetailsService.getTier(jpNum));
                        }
                        model.addAttribute(BEAN_NAME, amexBean);
                    
                }
                model.addAttribute("formNumber", formNumber);
                model.addAttribute("cardBean", cardDetailsBean);
                amexBean.setOfferId(cardDetailsBean.getOfferId());
            } else {
                return ERROR_URL;
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController applyAmexCardPage() :", e);
        }
        
          return "applyWebViewAmexCard";
    }
    
    
//    @RequestMapping(value = "/apply-webview-form/{cardName}", method = RequestMethod.POST)
//	public String applyWebViewAmexCardSubmit(@PathVariable(FLASH_CARDNAME) String cardName, HttpServletRequest request,
//			@ModelAttribute("amexBean") AmexBean amexBean, ModelMap model, RedirectAttributes redirectAttributes) {
//        
//        
//        
//            
//            return "";
//        }
}
