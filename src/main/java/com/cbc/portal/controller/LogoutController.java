package com.cbc.portal.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cbc.portal.utils.ApplicationPropertiesUtil;

@Controller
@RequestMapping("/logout")
public class LogoutController {

	private static Logger logger = Logger.getLogger(LogoutController.class.getName());

	@Autowired
	ApplicationPropertiesUtil applicationPropertiesUtil;

	@RequestMapping(method = RequestMethod.GET)
	public String logout(HttpServletRequest request,RedirectAttributes redirectAttributes, @CookieValue("jpcdn") String jpcdn, HttpServletResponse response) {
		try {
			redirectAttributes.addFlashAttribute("applicationURL", applicationPropertiesUtil.getApplicationURL());
			String redirectUrl	= (String)request.getSession().getAttribute("redirectUrl");
			redirectUrl			= (null!=redirectUrl)?redirectUrl.trim():"";
			String trackcode    = (String)request.getSession().getAttribute("trackCode");
			
			//Track code changes start
			if(!trackcode.isEmpty()){
				StringBuilder builder = new StringBuilder();
				String [] buildArray = null;
				buildArray = trackcode.split("_");
				if("lw".equalsIgnoreCase(buildArray[0])){
					builder.append(request.getSession().getAttribute("homeUrl")).append("?spMailingID=").append(buildArray[1]).append("&spUserID=").append(buildArray[2]);
					builder.append("&spJobID=").append(buildArray[3]).append("&spReportId=").append(buildArray[4]);
				}
				if("utm".equalsIgnoreCase(buildArray[0])){
					builder.append(request.getSession().getAttribute("homeUrl")).append("?utm_source=").append(buildArray[1]).append("&utm_medium=").append(buildArray[2]).append("&utm_campaign=").append(buildArray[3]);
				}
				redirectUrl = builder.toString();
			}
			//Track code changes end
//                        System.out.println("redirect url in logout controller"+redirectUrl);
//			request.getSession().invalidate();
			if (redirectUrl.length() > 0) {
				return "redirect:/" + redirectUrl;
			}
			jpcdn=(null!=jpcdn)?jpcdn.trim():"";
			if(jpcdn.length()>0){
			Cookie cookie = new Cookie("jpcdn", "");
			       cookie.setMaxAge(0);
			       response.addCookie(cookie);
			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in LogoutController at logout() :", e);
		}
		return "redirect:/home";
	}

}
