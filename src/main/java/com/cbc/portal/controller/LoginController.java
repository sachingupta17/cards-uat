package com.cbc.portal.controller;

import com.auth0.Auth0User;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.auth0.NonceGenerator;
import com.auth0.NonceStorage;
import com.auth0.RequestNonceStorage;
import com.auth0.SessionUtils;

import com.cbc.portal.utils.ApplicationPropertiesUtil;

@Controller
@RequestMapping("/loginDetails")
public class LoginController {

	private static Logger logger = Logger.getLogger(LoginController.class.getName());

//	private final NonceGenerator nonceGenerator = new NonceGenerator();
        
        String test;

	@Autowired
	ApplicationPropertiesUtil applicationPropertiesUtil;

	@RequestMapping(method = RequestMethod.GET)
	public String loginMemberDetails(HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletResponse response) {
	           String appurl=applicationPropertiesUtil.getApplicationURL();	
                   System.out.println("----------->"+appurl);
                   System.out.println("------------------------------");
            try {
			redirectAttributes.addFlashAttribute("applicationURL", applicationPropertiesUtil.getApplicationURL());
                        
                        Cookie[] cookie = request.getCookies();
                        String number;
                        String name;
                        if (cookie != null) {
            for (int i = 0; i < cookie.length; i++) {
                        
                        if(cookie[i].getName().trim().equals("jpnum")){
                
                    number=cookie[i].getValue();
                    System.out.println("mgffdfd"+number);
                    request.getSession().setAttribute("loggedInUser", number);
               
                }
                
                if(cookie[i].getName().trim().equals("user")){
                
                    name=cookie[i].getValue();
                    System.out.println("namebbbb"+name);
               request.getSession().setAttribute("loggedInUserName",name);

                }
            }
                        }
                        
//			Auth0User user = SessionUtils.getAuth0User(request);
                        
                        
//                       String appurl=applicationPropertiesUtil.getApplicationURL();
//                       System.out.println("--------login controller------->"+test);
                      System.out.println("--------login controller------->"+request.getSession().getAttribute("loggedInUser"));
                   
//			if (null != user) {
//			System.out.println("--------login controller--lop----->"+user.getNickname());
//                                 request.getSession().setAttribute("loggedInUser", user.getNickname());
//				request.getSession().setAttribute("loggedInUserName", user.getName().contains(" ") ? user.getName().split(" ")[0] : user.getName());
//			}//if
//			else {
//				NonceStorage nonceStorage = new RequestNonceStorage(request);
//				String nonce = nonceGenerator.generateNonce();
//				nonceStorage.setState(nonce);
//				request.setAttribute("state", nonce);
//			}
			
//			Cookie ck=null;
//			ck = new Cookie("jpcdn","cardsjetprivilege.com");
//                        ck.setSecure(true);
//			response.addCookie(ck);
//			
//			HttpSession session=request.getSession();
//			String redirectUrl 	= (String)session.getAttribute("redirectUrl");
//			String trackcode	= (String)session.getAttribute("trackCode");
//			redirectUrl=(null!=redirectUrl)?redirectUrl.trim():"";
//                         if((String)request.getSession().getAttribute("login") == "1"){
//                           
//                        redirectUrl=(String)request.getSession().getAttribute("loggedin");
//                        request.getSession().setAttribute("login", "0");
//                                
//                        
//                        }
//			             System.out.println("trackcode==>"+trackcode);           
			//Track code changes start
//			if(!trackcode.isEmpty()){
//                           
//				StringBuilder builder = new StringBuilder();
//				String [] buildArray = null;
//				buildArray = trackcode.split("_");
//				if("lw".equalsIgnoreCase(buildArray[0])){
//					builder.append(session.getAttribute("homeUrl")).append("?spMailingID=").append(buildArray[1]).append("&spUserID=").append(buildArray[2]);
//					builder.append("&spJobID=").append(buildArray[3]).append("&spReportId=").append(buildArray[4]);
//				}
//				if("utm".equalsIgnoreCase(buildArray[0])){
//					builder.append(session.getAttribute("homeUrl")).append("?utm_source=").append(buildArray[1]).append("&utm_medium=").append(buildArray[2]).append("&utm_campaign=").append(buildArray[3]);
//				}
//				redirectUrl = builder.toString();
//			}
//                        System.out.println("in login controller"+redirectUrl);
			//Track code changes end
//			session.removeAttribute("redirectUrl");
                       
//			if (redirectUrl.length() > 0 && !redirectUrl.equalsIgnoreCase("home")) {
//                           
//				return "redirect:" +appurl+ redirectUrl;
//			}
		} catch (Exception e) {
			logger.error("@@@@ Exception in LoginController at loginMemberDetails() :", e);
		}
//		return "redirect:/";
                   
                appurl=appurl.substring(0, appurl.length()-1);
              
                 System.out.println("login url  "+appurl); 
                         
		return "redirect:"+appurl;
	}
        
        
        
//        public  void test(HttpServletRequest request,HttpServletResponse response,String jpnum,String user){
//        
//            test=jpnum;
//            request.getSession().setAttribute("loggedInUser", jpnum);
//            System.out.println("--------logincallbackloosdfp------->"+jpnum);
//            System.out.println("--------logincallbackloosdfp------->"+request.getSession().getAttribute("loggedInUser"));
//
//            request.getSession().setAttribute("loggedInUserName", user);
//        
//        }

}
