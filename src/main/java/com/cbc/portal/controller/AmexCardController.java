package com.cbc.portal.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.auth0.NonceGenerator;
import com.auth0.NonceStorage;
import com.auth0.RequestNonceStorage;
import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.CityMappingBean;
import com.cbc.portal.beans.EnrollBean;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.entity.CcPageMaster;
import com.cbc.portal.service.AmexCardService;
import com.cbc.portal.service.CardDetailsService;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.service.MemberDetailsService;
import com.cbc.portal.service.MemberEnrollService;
import com.cbc.portal.service.RecommendCardService;
import com.cbc.portal.utils.ApplicationPropertiesUtil;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.GsonUtil;
import java.util.Arrays;
import com.cbc.portal.utils.DecryptJpNumberCards;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Value;

@Controller
public class AmexCardController {

    private Logger logger = Logger.getLogger(AmexCardController.class.getName());
    private final NonceGenerator nonceGenerator = new NonceGenerator();

    @Autowired
    ApplicationPropertiesUtil applicationPropertiesUtil;

    @Autowired
    AmexCardService amexCardService;

    @Autowired
    CommonService commonService;

    @Autowired
    MemberEnrollService memberEnrollService;

    @Autowired
    MemberDetailsService memberDetailsService;

    @Autowired
    CardDetailsService cardDetailsService;

    @Autowired
    RecommendCardService recommendCardService;

    @Value("${application.pii.encryption.strSoapURL1}")
    private String strSoapURL1;

    @Value("${application.pii.decrypt.strSOAPAction1}")
    private String strSOAPActiondecrypt;

    @Value("${application.pii.encrypt.strSOAPAction1}")
    private String strSOAPActionencrypt;

    @Value("${application.amex.bankNumber}")
    private String bankPhoneNumber;

    @Value("${application.amex.days}")
    private String amexDays;

    @Value("${application.silverpop.approved}")
    private Long approvedId;

    @Value("${application.silverpop.provisionallyApproved}")
    private Long proviApprovedId;

    @Value("${application.silverpop.reject}")
    private Long rejectId;

    @Value("${application.silverpop.tryAgain}")
    private Long tryAgainId;
    
    @Value("${application.sendGrid.approved}")
    private String sendGridApprovedId;

    @Value("${application.sendGrid.provisionallyApproved}")
    private String sendGridProviApprovedId;

    @Value("${application.sendGrid.reject}")
    private String sendGridRejectId;

    @Value("${application.sendGrid.tryAgain}")
    private String sendGridTryAgainId;

    private static final String BEAN_NAME = "amexBean";
    private static final String REDIRECT_URL = "redirect:/apply-form/";
    private static final String ERROR_URL = "/jsp/portal/error.jsp";
    private static final String FLASH_CARDNAME = "cardName";
    private static final String ENROLL_BEAN = "enrollBean";
    private static final String JP_NUMBER = "jpNumber";
    private static final String RANDOM_NUMBER = "randomNumber";
    private static final String BE_RANDOM_NUMBER = "beRandomNumber";

    @RequestMapping("/apply-------------form/{cardName}")
    public String applyAmexCardPage(@PathVariable("cardName") String cardName, HttpServletRequest request,
            ModelMap model, @ModelAttribute("errorMsg") String errorMsg,
            @ModelAttribute(BEAN_NAME) AmexBean amexBean, @ModelAttribute(RANDOM_NUMBER) String randomNumber,
            @ModelAttribute("formNumber") String formNumber, @ModelAttribute(JP_NUMBER) String jpNum, @ModelAttribute(BE_RANDOM_NUMBER) String beRandomNumber) {
        String spMailingID = "";
        String spUserID = "";
        String spJobID = "";
        String spReportId = "";
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        String termsAndCondition = "";
        String user = getLoggedInUser(request);
        String cName = cardName.replaceAll("-", " ");
        String[] strArray = cName.split(" ");
        StringBuilder newCardName = new StringBuilder();
        
        logger.info("**********apply-form GET called**********");
        System.out.println("**********apply-form GET called**********");
        logger.info("formNumber at starting of controller===>" + formNumber);
        System.out.println("formNumber at starting of controller===>" + formNumber);
        System.out.println("cardName>>>>>>>"+cardName);
        
        for (String s : strArray) {
            if ("express".equalsIgnoreCase(s)) {
                s = s + "&reg;";
            }
            newCardName.append(s + " ");
        }
        
        System.out.println("newCardName>>>>>>>"+newCardName);
        
        try {

            String cardVarientjpNumber = commonService.checkCardVarientRandomNo(1, beRandomNumber);
            spMailingID = CommonUtils.nullSafe(request.getParameter("spMailingID"), "");
            spUserID = CommonUtils.nullSafe(request.getParameter("spUserID"), "");
            spJobID = CommonUtils.nullSafe(request.getParameter("spJobID"), "");
            spReportId = CommonUtils.nullSafe(request.getParameter("spReportId"), "");

            utm_source = CommonUtils.nullSafe(request.getParameter("utm_source"), "");
            utm_medium = CommonUtils.nullSafe(request.getParameter("utm_medium"), "");
            utm_campaign = CommonUtils.nullSafe(request.getParameter("utm_campaign"), "");
            
            logger.info("AmexCardController ==> utm_source: " + utm_source + " utm_medium: " + utm_medium + " utm_campaign: " + utm_campaign);
            System.out.println("AmexCardController ==> utm_source: " + utm_source + " utm_medium: " + utm_medium + " utm_campaign: " + utm_campaign);
            
            int cpNo = cardDetailsService.getCardIdByName(newCardName.toString().trim());
            
            System.out.println("cpNo>>"+cpNo);
         
            if (0 != cpNo) {
                List<String> groupHeaders = null;
                amexBean.setCpNo(cpNo);
                termsAndCondition = commonService.getTermdAndCondition(cpNo);
                groupHeaders = commonService.getGroupHeaders(1);
                CardDetailsBean cardDetailsBean = recommendCardService.getRecommendedCardById(cpNo);
                if (groupHeaders.size() > 3) {
                    groupHeaders.subList(3, groupHeaders.size()).clear();
                }
                model.addAttribute("groupHeadings", groupHeaders);
                model.addAttribute("applicationURL", applicationPropertiesUtil.getApplicationURL());

                if (!spMailingID.isEmpty()) {
                    trackCode = "?spMailingID=" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
                }
                if (!utm_source.isEmpty()) {
                    trackCode = "?utm_source=" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                    amexBean.setUtmSource(utm_source);
                    amexBean.setUtmMedium(utm_medium);
                    amexBean.setUtmCampaign(utm_campaign);
                }
                request.getSession().setAttribute("redirectUrl", "apply-form/" + cardName + trackCode);
                if (!spMailingID.isEmpty()) {
                    trackCode = "lw_" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
                }
                if (!utm_source.isEmpty()) {
                    trackCode = "utm_" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                }
                
                System.out.println("trackCode>>>"+trackCode);                
                request.getSession().setAttribute("trackCode", trackCode);
                request.getSession().setAttribute("homeUrl", "apply-form/" + cardName);
                
//                sprint 56
                List<CcPageMaster> pageMasterList = null;

                String callMeFlag = "", promocodeFlag = "";
                pageMasterList = commonService.getPageMasterDetail(4);
                for (CcPageMaster flag : pageMasterList) {
                    logger.info("callme" + flag.getCallmeflag() + " promo" + flag.getPromocodeflag());
                    System.out.println("callme" + flag.getCallmeflag() + " promo" + flag.getPromocodeflag());
                    callMeFlag = String.valueOf(flag.getCallmeflag());
                    promocodeFlag = String.valueOf(flag.getPromocodeflag());
                }
                logger.info("callMeFlag :" + callMeFlag + "promocodeFlag:" + promocodeFlag);
                System.out.println("callMeFlag :" + callMeFlag + "promocodeFlag:" + promocodeFlag);
                model.addAttribute("callMeFlag", callMeFlag);
                model.addAttribute("promocodeFlag", promocodeFlag);
//            sprint 56 end
                String jpNumber = getLoggedInUser(request);
//                                             
                System.out.println("jpNumber==============>" + jpNumber);
                model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getAfTitle().replace("{card name}", cardDetailsBean.getImageText()));
                model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getAfDescription().replace("{card name}", cardDetailsBean.getImageText()));
                model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getAfKeyword());
                model.addAttribute("timer", commonService.getTimer("Timer for Application form update"));
                model.addAttribute("timer1", commonService.getTimer("Timer for Form Inactive"));
                model.addAttribute("genderStatus", commonService.getEnumValues("Gender"));
                model.addAttribute("cities", commonService.getCities());
                model.addAttribute("states", commonService.getStates());
                model.addAttribute("qualification", commonService.getAmexValues("Qualification"));
                model.addAttribute("salaryStatus", commonService.getAmexValues("EMP_STATUS"));
                model.addAttribute(ENROLL_BEAN, new EnrollBean());
                model.addAttribute("callMeBean", new CallMeBean());
                model.addAttribute("defaultPromocode", cardDetailsBean.getDefaultPromocode());
                model.addAttribute("termsAndCondition", termsAndCondition);
                model.addAttribute("beJpNumber", cardVarientjpNumber);
                model.addAttribute("beRandomNumber", beRandomNumber);

                if (null != randomNumber && !randomNumber.isEmpty()) {
                    logger.info("*******inside if Condition*****" + randomNumber);
                    System.out.println("*******inside if Condition*****" + randomNumber);
                    amexCardService.getBEAmexFormDetails(randomNumber, amexBean);
                    
                    System.out.println("user>>>"+user);
                    
                    if (null != user && !user.isEmpty()) {
                        if (amexBean.getJetpriviligemembershipNumber().isEmpty()) {
                            amexBean.setJetpriviligemembershipNumber(CommonUtils.getJPNumber(user));
                            cardDetailsBean = amexCardService.getOfferByJPNum(CommonUtils.getJPNumber(user), cardDetailsBean);
                        } else {
                            cardDetailsBean = amexCardService.getOfferByJPNum(amexBean.getJetpriviligemembershipNumber(), cardDetailsBean);
                        }
                    } else {
                        if (!amexBean.getJetpriviligemembershipNumber().isEmpty()) {
                            cardDetailsBean = amexCardService.getOfferByJPNum(amexBean.getJetpriviligemembershipNumber(), cardDetailsBean);
                        }
                    }
                    System.out.println("cardDetailsBean>>>"+cardDetailsBean.getJpNumber());
                    model.addAttribute(BEAN_NAME, amexBean);
                } else {
                    if (errorMsg.isEmpty()) {
                        formNumber = RandomStringUtils.random(20, true, true);
//                                                formNumber="XBLKBfcXP9Dm0nFrFZJS";
                        String dbFormNumber = "";
                        int i = 0;
                        
                        logger.info("**** formNumber *****" + formNumber);
                        System.out.println("**** formNumber *****" + formNumber);
                        
                        while (!formNumber.equalsIgnoreCase(null)) {
                            if (i == 2) {
                                formNumber = RandomStringUtils.random(20, true, true);

                                logger.info("inside formNumber" + formNumber);
                                System.out.println("inside formNumber" + formNumber);
                            }

                            logger.info("**** formNumber inside while ****" + formNumber);
                            System.out.println("**** formNumber inside while ****" + formNumber);
                            
                            dbFormNumber = amexCardService.getFormNumber(formNumber);
                            logger.info(" dbFormNumber  : " + dbFormNumber);
                            System.out.println(" dbFormNumber  : " + dbFormNumber);
                            if (!dbFormNumber.equalsIgnoreCase(formNumber)) {
                                logger.info(" Inside  if condition   : " + dbFormNumber);
                                System.out.println(" Inside  if condition   : " + dbFormNumber);
                                break;

                            }
                            i = 2;

                        }

                        amexBean.setFormNumber(formNumber);
                        logger.info("amexBean.getFormNumber at controller" + amexBean.getFormNumber());
                        System.out.println("amexBean.getFormNumber at controller" + amexBean.getFormNumber());
                    }
                    if (user == null) {
                        NonceStorage nonceStorage = new RequestNonceStorage(request);
                        String nonce = nonceGenerator.generateNonce();
                        nonceStorage.setState(nonce);
                        model.addAttribute("state", nonce);
                        request.setAttribute("state", nonce);
                        if (!jpNum.isEmpty()) {
                            jpNum = URLEncoder.encode(jpNum, "UTF-8");

                            DecryptJpNumberCards decryptJpnumber = new DecryptJpNumberCards();
                            jpNum = decryptJpnumber.DecryptJpnumber(jpNum, strSoapURL1, strSOAPActiondecrypt);
                            cardDetailsBean = amexCardService.getOfferByJPNum(jpNum, cardDetailsBean);
                            amexBean.setJetpriviligemembershipNumber(jpNum);
                            amexBean.setJetpriviligemembershipTier(memberDetailsService.getTier(jpNum));
                        }
                        model.addAttribute(BEAN_NAME, amexBean);
                    } else {
                        if (errorMsg.isEmpty()) {
                            memberDetailsService.getMemberDetails(jpNumber, amexBean);
                            amexCardService.saveOrUpdateFormData(amexBean, 0);
                        }
                        jpNumber = CommonUtils.getJPNumber(jpNumber);
                        jpNumber = jpNum.isEmpty() ? jpNumber : jpNum;
                        cardDetailsBean = amexCardService.getOfferByJPNum(jpNumber, cardDetailsBean);
                        if (null == cardDetailsBean.getOfferId()) {
                            cardDetailsBean = amexCardService.getDefaultOfffer(cardDetailsBean);
                        }
                        model.addAttribute(BEAN_NAME, amexBean);
                        model.addAttribute("isLoginUser", "Yes");
                    }
                }
                model.addAttribute("formNumber", formNumber);
                model.addAttribute("cardBean", cardDetailsBean);
                amexBean.setOfferId(cardDetailsBean.getOfferId());
            } else {
                return ERROR_URL;
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController applyAmexCardPage() :", e);
        }
//                        return ERROR_URL;

        return "applyAmexCard";

    }

    @RequestMapping(value = "/apply-form/{cardName}", method = RequestMethod.POST)
    public String applyAmexCardSubmit(@PathVariable(FLASH_CARDNAME) String cardName, HttpServletRequest request,
            @ModelAttribute("amexBean") AmexBean amexBean, ModelMap model, RedirectAttributes redirectAttributes) {
        String response = "";
        String spMailingID = "";
        String spUserID = "";
        String spJobID = "";
        String spReportId = "";
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        try {
        	
        	logger.info("**********apply-form called POST**********");
        	
            spMailingID = CommonUtils.nullSafe(request.getParameter("spMailingID"), "");
            spUserID = CommonUtils.nullSafe(request.getParameter("spUserID"), "");
            spJobID = CommonUtils.nullSafe(request.getParameter("spJobID"), "");
            spReportId = CommonUtils.nullSafe(request.getParameter("spReportId"), "");

            utm_source = CommonUtils.nullSafe(request.getParameter("utm_source"), "");
            utm_medium = CommonUtils.nullSafe(request.getParameter("utm_medium"), "");
            utm_campaign = CommonUtils.nullSafe(request.getParameter("utm_campaign"), "");

                        logger.info("Amex Controller jpnumber:"+amexBean.getJetpriviligemembershipNumber()+" tier"+amexBean.getJetpriviligemembershipTier());
                        System.out.println("Amex Controller jpnumber:"+amexBean.getJetpriviligemembershipNumber()+" tier"+amexBean.getJetpriviligemembershipTier());
                        
            if (!spMailingID.isEmpty()) {
                trackCode = "?spMailingID=" + spMailingID + "&spUserID=" + spUserID + "&spJobID=" + spJobID + "&spReportId=" + spReportId;
            }
            if (!utm_source.isEmpty()) {
                trackCode = "?utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
            }
            request.getSession().setAttribute("redirectUrl", "apply-form/" + cardName + trackCode);

            String random_no = amexBean.getRandomNumber();
            String[] rndmno = random_no.split(",");
            amexBean.setRandomNumber(rndmno[0]);
            String pid = (String) request.getSession().getAttribute("PID");//new
            
            logger.info(pid + "------PID FOR JP NUMBER----" + amexBean.getJetpriviligemembershipNumber());
            System.out.println(pid + "------PID FOR JP NUMBER----" + amexBean.getJetpriviligemembershipNumber());
            
            amexBean.setPid(pid);//new 
            String userTier = (String) request.getSession().getAttribute("userTier");
            
            logger.info("userTier : " + userTier);
            System.out.println("userTier : " + userTier);
            
            response = amexCardService.applyAmexCardWebService(amexBean, userTier);
            String[] responseArr = response.split(",");
            String resStr = responseArr[0];
            String pcnNumber = responseArr[1];
            List<CardDetailsBean> statuslist = null;
            //cr3 start
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("MM/DD/YYYY");
            String strDate = formatter.format(date);

            String cardNameSms = cardDetailsService.getCardDetailsById(amexBean.getCpNo()).getCardName();
            //cr3 end
            if ("DVE".equalsIgnoreCase(pcnNumber)) {
                redirectAttributes.addFlashAttribute(BEAN_NAME, amexBean);
                redirectAttributes.addFlashAttribute("errorMsg", resStr);
                if (!amexBean.getRandomNumber().isEmpty()) {
                    redirectAttributes.addFlashAttribute(RANDOM_NUMBER, amexBean.getRandomNumber());
                    redirectAttributes.addFlashAttribute("formNumber", amexBean.getFormNumber());
                }
                if (!amexBean.getJetpriviligemembershipNumber().isEmpty()) {
                    redirectAttributes.addFlashAttribute(JP_NUMBER, amexBean.getJetpriviligemembershipNumber());
                }
                return REDIRECT_URL + cardName + trackCode;
            } else if ("approved".equalsIgnoreCase(resStr)) {
                String title = "", messagecontent = "";
                statuslist = commonService.getBankwiseRsponseByStatus("approved", 1);
                if (statuslist != null && !statuslist.isEmpty()) {
                    for (CardDetailsBean b : statuslist) {
                        title = b.getBpSatatuTitle();
                        messagecontent = b.getMessageContent();
                        String cardname = cardDetailsService.getCardDetailsById(amexBean.getCpNo()).getCardName();

                        if (messagecontent.contains("{pcnNumber}") == true) {
                            messagecontent = messagecontent.replace("{pcnNumber}", pcnNumber.trim());
                        }
                        if (messagecontent.contains("{cardName}") == true) {
                            messagecontent = messagecontent.replace("{cardName}", cardname);
                        }
                    }

                } else {
                    title = "Oops!";
                    messagecontent = "Your Transaction could not be processed. We regret the inconvenience. Please try again.";

                }
                
//                commonService.buildSMSForApplication(pcnNumber, approvedId, amexBean.getEmail(), amexBean.getFname(), amexBean.getLname(), "1", "Approved", cardNameSms, amexDays, amexBean.getMobile(), amexBean.getFullName(), String.valueOf(amexBean.getAppFormNo()), amexBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_amexapp");
                commonService.buildSMSForApplication(pcnNumber, sendGridApprovedId, amexBean.getEmail(), amexBean.getFname(), amexBean.getLname(), "1", "Approved", cardNameSms, amexDays, amexBean.getMobile(), amexBean.getFullName(), String.valueOf(amexBean.getAppFormNo()), amexBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_amexapp");

                redirectAttributes.addFlashAttribute("Statustitle", title);
                redirectAttributes.addFlashAttribute("messagecontent", messagecontent);
                redirectAttributes.addFlashAttribute(FLASH_CARDNAME, cardDetailsService.getCardDetailsById(amexBean.getCpNo()).getCardName());
                redirectAttributes.addFlashAttribute("pcnNumber", pcnNumber.trim());
                String redirectURL = "/request-approved";
                redirectURL = redirectURL + trackCode;
                return "redirect:" + redirectURL;

            } else if ("pending".equalsIgnoreCase(resStr)) {
                String title = "", messagecontent = "";
                statuslist = commonService.getBankwiseRsponseByStatus("pending", 1);
                if (statuslist != null && !statuslist.isEmpty()) {
                    for (CardDetailsBean b : statuslist) {
                        title = b.getBpSatatuTitle();
                        messagecontent = b.getMessageContent();

                        if (messagecontent.contains("{pcnNumber}") == true) {
                            messagecontent = messagecontent.replace("{pcnNumber}", pcnNumber.trim());
                        }
                        if (messagecontent.contains("{cardName}") == true) {
                            messagecontent = messagecontent.replace("{cardName}", cardDetailsService.getCardDetailsById(amexBean.getCpNo()).getCardName());
                        }
                    }

                } else {
                    title = "Oops!";
                    messagecontent = "Your Transaction could not be processed. We regret the inconvenience. Please try again.";

                }
                redirectAttributes.addFlashAttribute("Statustitle", title);
                redirectAttributes.addFlashAttribute("messagecontent", messagecontent);
                
//                commonService.buildSMSForApplication(pcnNumber, proviApprovedId, amexBean.getEmail(), amexBean.getFname(), amexBean.getLname(), "1", "Pending", cardNameSms, amexDays, amexBean.getMobile(), amexBean.getFullName(), String.valueOf(amexBean.getAppFormNo()), amexBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_amexprapp");
                commonService.buildSMSForApplication(pcnNumber, sendGridProviApprovedId, amexBean.getEmail(), amexBean.getFname(), amexBean.getLname(), "1", "Pending", cardNameSms, amexDays, amexBean.getMobile(), amexBean.getFullName(), String.valueOf(amexBean.getAppFormNo()), amexBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_amexprapp");

                redirectAttributes.addFlashAttribute(FLASH_CARDNAME, cardDetailsService.getCardDetailsById(amexBean.getCpNo()).getCardName());
                redirectAttributes.addFlashAttribute("pcnNumber", pcnNumber.trim());
                String redirectURL = "/request-approval-pending";
                redirectURL = redirectURL + trackCode;
                return "redirect:" + redirectURL;

            } else if ("amexdecline".equalsIgnoreCase(resStr)) {
                String title = "", messagecontent = "";
                statuslist = commonService.getBankwiseRsponseByStatus("amexdecline", 1);
                if (statuslist != null && !statuslist.isEmpty()) {
                    for (CardDetailsBean b : statuslist) {
                        title = b.getBpSatatuTitle();
                        messagecontent = b.getMessageContent();
                        if (messagecontent.contains("{pcnNumber}") == true) {
                            messagecontent = messagecontent.replace("{pcnNumber}", pcnNumber.trim());
                        }
                        if (messagecontent.contains("{cardName}") == true) {
                            messagecontent = messagecontent.replace("{cardName}", cardDetailsService.getCardDetailsById(amexBean.getCpNo()).getCardName());
                        }
                    }

                } else {
                    title = "Oops!";
                    messagecontent = "Your Transaction could not be processed. We regret the inconvenience. Please try again.";

                }
                
//                commonService.buildSMSForApplication(pcnNumber, rejectId, amexBean.getEmail(), amexBean.getFname(), amexBean.getLname(), "1", "Reject", cardNameSms, amexDays, amexBean.getMobile(), amexBean.getFullName(), String.valueOf(amexBean.getAppFormNo()), amexBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_amexrej");
                commonService.buildSMSForApplication(pcnNumber, sendGridRejectId, amexBean.getEmail(), amexBean.getFname(), amexBean.getLname(), "1", "Reject", cardNameSms, amexDays, amexBean.getMobile(), amexBean.getFullName(), String.valueOf(amexBean.getAppFormNo()), amexBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_amexrej");

                redirectAttributes.addFlashAttribute("Statustitle", title);
                redirectAttributes.addFlashAttribute("messagecontent", messagecontent);
                redirectAttributes.addFlashAttribute(FLASH_CARDNAME, cardDetailsService.getCardDetailsById(amexBean.getCpNo()).getCardName());
                String redirectURL = "/request-amex-approval-pending";
                redirectURL = redirectURL + trackCode;
                return "redirect:" + redirectURL;

            } else {
                String title = "", messagecontent = "";
                statuslist = commonService.getBankwiseRsponseByStatus("otherAmex", 1);
                if (statuslist != null && !statuslist.isEmpty()) {
                    for (CardDetailsBean b : statuslist) {
                        title = b.getBpSatatuTitle();
                        messagecontent = b.getMessageContent();
                        if (messagecontent.contains("{pcnNumber}") == true) {
                            messagecontent = messagecontent.replace("{pcnNumber}", pcnNumber.trim());
                        }
                        if (messagecontent.contains("{cardName}") == true) {
                            messagecontent = messagecontent.replace("{cardName}", cardDetailsService.getCardDetailsById(amexBean.getCpNo()).getCardName());
                        }
                    }

                } else {
                    title = "Oops!";
                    messagecontent = "Your Transaction could not be processed. We regret the inconvenience. Please try again.";

                }
//                commonService.buildSMSForApplication(pcnNumber, tryAgainId, amexBean.getEmail(), amexBean.getFname(), amexBean.getLname(), "1", "Try Again", cardNameSms, amexDays, amexBean.getMobile(), amexBean.getFullName(), String.valueOf(amexBean.getAppFormNo()), amexBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_amextry");
                commonService.buildSMSForApplication(pcnNumber, sendGridTryAgainId, amexBean.getEmail(), amexBean.getFname(), amexBean.getLname(), "1", "Try Again", cardNameSms, amexDays, amexBean.getMobile(), amexBean.getFullName(), String.valueOf(amexBean.getAppFormNo()), amexBean.getJetpriviligemembershipNumber(), strDate, bankPhoneNumber, "Cards_amextry");

                redirectAttributes.addFlashAttribute("Statustitle", title);
                redirectAttributes.addFlashAttribute("messagecontent", messagecontent);
                redirectAttributes.addFlashAttribute(FLASH_CARDNAME,
                        cardDetailsService.getCardDetailsById(amexBean.getCpNo()).getCardName());
                return "redirect:/request-disapproved";
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController applyAmexCardWebService() :", e);
        }
        return REDIRECT_URL + cardName;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/request-approved")
    public String requestapprovedPage(HttpServletRequest request, ModelMap model, @ModelAttribute("cardName") String cardName) {
        request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
        model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getRaTitle());
        model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getRaDescription());
        model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getRaKeyword());
        if (null != cardName && !cardName.isEmpty()) {
            return "approval";
        } else {
            return ERROR_URL;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/request-disapproved")
    public String requestdisapprovedPage(HttpServletRequest request, ModelMap model, @ModelAttribute("cardName") String cardName) {
        request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
        model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getRdTitle());
        model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getRdDescription());
        model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getRdKeyword());
        if (null != cardName && !cardName.isEmpty()) {
            return "decline";
        } else {
            return ERROR_URL;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/request-approval-pending")
    public String requestdisapprovedPendingPage(HttpServletRequest request, ModelMap model, @ModelAttribute("cardName") String cardName) {
        request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
        model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getRdTitle());
        model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getRdDescription());
        model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getRdKeyword());
        if (null != cardName && !cardName.isEmpty()) {
            return "pending";
        } else {
            return ERROR_URL;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/request-amex-approval-pending")
    public String requestdisapprovedAmexDeclinePage(HttpServletRequest request, ModelMap model, @ModelAttribute("cardName") String cardName) {
        request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
        model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getRdTitle());
        model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getRdDescription());
        model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getRdKeyword());
        if (null != cardName && !cardName.isEmpty()) {
            return "amexdecline";
        } else {
            return ERROR_URL;
        }
    }

    @RequestMapping(value = "/getTier", method = RequestMethod.GET)
    @ResponseBody
    public String getMemberTierFunction(@RequestParam("jpNum") String jpnumber, HttpServletRequest request) {
        String tier = "";
        try {
            tier = memberDetailsService.getTier(jpnumber);
            HttpSession tiersession = request.getSession();

            logger.info("before session :" + tier);
            tiersession.setAttribute("userTier", tier);
            logger.info("after session  :" + request.getSession().getAttribute("userTier"));
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController getMemberTierFunction() :", e);
        }
        return tier;
    }

    @RequestMapping(value = "/getOffer", method = RequestMethod.GET)
    @ResponseBody
    public String getOffer(@RequestParam("jpNum") String jpnumber, @RequestParam("cpNo") String cpNo) {
        JSONObject json = null;
        try {
            logger.info("**********getOffer Called GET**********");
        	json = amexCardService.getOfferByJPNumber(jpnumber, cpNo);
        	System.out.println("json>>>"+json.toString());
        	
            if (null != json) {
                return json.toString();
            } else {
                return "";
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController getOffer() :", e);
        }
        return "";
    }

    @RequestMapping(value = "/getCity", method = RequestMethod.GET)
    @ResponseBody
    public String getCityMappings(@RequestParam("pinCode") String pinCode) {
        CityMappingBean bean = null;
        try {
            bean = commonService.getCityMapping(pinCode);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController getCityMappings() :", e);
        }
        return GsonUtil.toJson(bean);
    }

    @RequestMapping(value = "/getCitiesByState", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getCitiesByState(@RequestParam("state") String state) {
        Map<String, String> map = new HashMap<>();
        try {
            map = commonService.getCitiesByState(state);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController getCitiesByState() :", e);
        }
        return map;
    }

    @RequestMapping(value = "/getStateByCity", method = RequestMethod.GET)
    @ResponseBody
    public String getStateByCity(@RequestParam("city") String city) {
        String state = "";
        try {
            state = commonService.getStateByCity(city);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController getStateByCity() :", e);
        }
        return state;
    }

    @RequestMapping(value = "/checkExcludePincode", method = RequestMethod.GET)
    @ResponseBody
    public String checkExcludePincode(@RequestParam("pinCode") String pinCode, @RequestParam("cpNo") String cpNo) {
        Boolean isExcluded = false;
        try {
            isExcluded = commonService.checkExcludePincode(pinCode, cpNo);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexApplicationFormController checkExcludePincode() :", e);
        }
        return isExcluded ? "excluded" : "";
    }

    @RequestMapping("/Apply-form/{cardName}")
    public String getBackEndApplicatioForm(@PathVariable("cardName") String cardName, HttpServletRequest request, RedirectAttributes redirect) {
        String randomNumber = "";
        String jpNumber = "";
        try {
            randomNumber = CommonUtils.nullSafe(request.getParameter(RANDOM_NUMBER));
            jpNumber = CommonUtils.nullSafe(request.getParameter(JP_NUMBER));
            redirect.addFlashAttribute(RANDOM_NUMBER, randomNumber);
            redirect.addFlashAttribute(JP_NUMBER, jpNumber);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController getBackEndApplicatioForm() :", e);
        }
//                        return ERROR_URL;

        return REDIRECT_URL + cardName;

    }

    public String getLoggedInUser(HttpServletRequest request) {
        return (String) request.getSession().getAttribute("loggedInUser");

    }

    @RequestMapping(value = "/callme", method = RequestMethod.POST)
    @ResponseBody
    public String saveCallMeDetails(@ModelAttribute("callMeBean") CallMeBean callMeBean) {
        String ststus = "";
//        try {
//            ststus = amexCardService.saveCallMeDetails(callMeBean);
//        } catch (Exception e) {
//            logger.error("@@@@ Exception in AmexCardController saveCallMeDetails() :", e);
//
//        }
        return ststus;
    }

    @RequestMapping(value = "/saveFormData", method = RequestMethod.POST)
    @ResponseBody
    public String saveFormData(@ModelAttribute("amexBean") AmexBean amexBean) {
        String ststus = "success";
        try {
            logger.info("form number saveFormData()" + amexBean.getFormNumber());

            amexCardService.saveOrUpdateFormData(amexBean, 0);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController saveFormData() :", e);
        }
        return ststus;
    }

    @RequestMapping(value = "/SilverpopAmex", method = RequestMethod.POST)
    @ResponseBody
    public String SilverpopAmex(@ModelAttribute("amexBean") AmexBean amexBean) {
        try {
            logger.info("EMAIL SEND ---- Amex Silver POP AMEX first name: " + amexBean.getFname() + " mobile no: " + amexBean.getMobile() + " Email-id: " + amexBean.getEmail() + " Form No: " + amexBean.getFormNumber());

            amexCardService.FormInactiveSilverPopApi(amexBean);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController SilverpopAmex() :", e);
        }
        return "";
    }

    //new implement
    @RequestMapping(value = "/getAllCity", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getAllCity() {
        Map<String, String> map = new HashMap<>();
        try {

            map = commonService.getAllCities();
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardController getStateByCity() :", e);
        }
        return map;
    }

    @RequestMapping(value = "/getmemberdetail", method = RequestMethod.GET)
    @ResponseBody
    public String getmembermobile(@RequestParam("jpNum") String jpNumber) {
        AmexBean amexbean = new AmexBean();
        memberDetailsService.getMemberDetails(jpNumber, amexbean);
        System.out.println("amexbean" + amexbean);
        return amexbean.getMobile();

    }

    @RequestMapping(value = "/getmemberdata", method = RequestMethod.GET)
    @ResponseBody
    public AmexBean getmember(@RequestParam("jpNum") String jpNumber) {
        System.out.println("at getmemberdetail");
        AmexBean amexbean = new AmexBean();
        memberDetailsService.getMemberDetails(jpNumber, amexbean);
        System.out.println("amexbean" + amexbean + "fname" + amexbean.getFname());
        return amexbean;

    }
}
