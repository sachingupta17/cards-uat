package com.cbc.portal.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.auth0.NonceGenerator;
import com.auth0.NonceStorage;
import com.auth0.RequestNonceStorage;
import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.EnrollBean;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.entity.CcPageMaster;
import com.cbc.portal.service.CardDetailsService;
import com.cbc.portal.service.CardSearchService;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.service.MemberDetailsService;
import com.cbc.portal.service.RecommendCardService;
import com.cbc.portal.service.impl.MemberDetailsServiceImpl;
import com.cbc.portal.utils.ApplicationPropertiesUtil;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.EncryptDetail;
import com.cbc.portal.utils.ICICIEncryptDetail;
import com.cbc.portal.utils.PemReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
//import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.json.simple.parser.ParseException;

import java.security.interfaces.RSAPublicKey;

@Controller
public class HomeController {

    private Logger logger = Logger.getLogger(HomeController.class.getName());
    private final NonceGenerator nonceGenerator = new NonceGenerator();

    @Autowired
    ApplicationPropertiesUtil applicationPropertiesUtil;

    @Autowired
    CardSearchService cardSearchService;

    @Autowired
    CommonService commonService;

    @Autowired
    RecommendCardService recommendCardService;

    @Autowired
    MemberDetailsService memberDetailsService;

    @Autowired
    CardDetailsService cardDetailsService;
    
    

    //for header-footer & fingerprint implementation
    @Value("${application.fingerprint.mac3.url}")
    String fingerprintMac3Url;

    @Value("${application.react.headerFooter.url}")
    String reactHeaderFooterUrl;

    @Value("${application.react.commonGlobal.url}")
    String reactCommonGlobalUrl;

    @Value("${auth0.domain}")
    String authDomain;
    
    @Value("${auth0.client_id}")
    String authClientId;
    
    @Value("${auth0.logincallback.url}")
    String authCallBackUrl;

    @Value("${application.pii.encryption.strSoapURL1}")
   	private String strSoapURL1;
   	@Value("${application.pii.encrypt.strSOAPAction1}")
   	private String soapActEnc;
   	
    private static final String BEAN_NAME = "cardDetailsBean";
    private static final String APPLICATION_URL = "applicationURL";
    private static final String REDIRECT_URL = "redirectUrl";
    private static final String UTM_SOURCE = "utm_source";
    private static final String UTM_MEDIUM = "utm_medium";
    private static final String UTM_CAMPAIGN = "utm_campaign";
    private String countrymm = "";
    String name = "";
    String category = "";
    String bankName = "";
    String lsBenefitsName = "";
    String joiningFeesName = "";
    String loggedInUserDob = "";
    String loggedInUser = "";
    String firstlogindate = "";
    String lastlogindate = "";
    //{"/", "/home"})

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String homePage(HttpServletRequest request, ModelMap model,
            @ModelAttribute("rcPage") String recPage,
            @ModelAttribute("bank") String bankFilter,
            @ModelAttribute("lsBenefits") String lsBenefitsFilter,
            @ModelAttribute("cardFilter") String cardFilter,
            @ModelAttribute("countryFilter") String countryFilter,
            @ModelAttribute("countryOption") String countryOption,
            @ModelAttribute("country") String countrydropdown,
            @ModelAttribute("joiningFees") String joiningFees,
            @ModelAttribute("utm_source") String utmsource,
            @ModelAttribute("utm_medium") String utmmedium,
            @ModelAttribute("utm_campaign") String utmcampaign,
            @ModelAttribute("cardtype") String cardtypetest) throws IOException {

        List<String> groupHeaders = null;
        CardDetailsBean cardDetailsBean = null;
        AmexBean amexBean = new AmexBean();
        String spMailingID = "";
        String spUserID = "";
        String spJobID = "";
        String spReportId = "";
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        
        String jpNumber = getLoggedInUser(request);
        
        System.out.println("jpNumber   Home "+jpNumber);
        
       
       
        
        model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getHpTitle());
        model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getHpDescription());
        model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getHpKeyword());
        try {

            
        	request.getSession().setAttribute("cat", cardtypetest);
            request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());

            spMailingID = CommonUtils.nullSafe(request.getParameter("spMailingID"), "");
            spUserID = CommonUtils.nullSafe(request.getParameter("spUserID"), "");
            spJobID = CommonUtils.nullSafe(request.getParameter("spJobID"), "");
            spReportId = CommonUtils.nullSafe(request.getParameter("spReportId"), "");

            utm_source = CommonUtils.nullSafe(request.getParameter(UTM_SOURCE), "");
            utm_medium = CommonUtils.nullSafe(request.getParameter(UTM_MEDIUM), "");
            utm_campaign = CommonUtils.nullSafe(request.getParameter(UTM_CAMPAIGN), "");
            if (!utmsource.isEmpty()) {
                utm_source = utmsource;
                utm_medium = utmmedium;
                utm_campaign = utmcampaign;
            }
            
            logger.info("HomeController ==> utm_source: " + utm_source + " utm_medium: " + utm_medium + " utm_campaign: " + utm_campaign);
            System.out.println("HomeController ==> utm_source: " + utm_source + " utm_medium: " + utm_medium + " utm_campaign: " + utm_campaign);
            
//            PemReader pmreader = new PemReader();
//            String pmkey =  pmreader.getKey();
//            PublicKey pubKey =  pmreader.getPublicKeyFromString(pmkey);
//            String publicKey = pmreader.getPublicKeyFromString(pmkey);
            
//            logger.info("pubKey>>>>>>>"+pubKey.getFormat());
//            logger.info("Pmreader>>>>>>>>"+pmkey);
            
            
            
            model.addAttribute(APPLICATION_URL, applicationPropertiesUtil.getApplicationURL());
            
            if (null != spMailingID && !spMailingID.isEmpty()) {
                trackCode = "?spMailingID=" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
            }
            
            if (null != utm_source && !utm_source.isEmpty()) {
                trackCode = "?utm_source=" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                amexBean.setUtmSource(utm_source);
                amexBean.setUtmMedium(utm_medium);
                amexBean.setUtmCampaign(utm_campaign);
            }
            
            request.getSession().setAttribute(REDIRECT_URL, "home" + trackCode);
            if (null != spMailingID && !spMailingID.isEmpty()) {
                trackCode = "lw_" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
            }
            if (null != utm_source && !utm_source.isEmpty()) {
                trackCode = "utm_" + utm_source + "_" + utm_medium + "_" + utm_campaign;
            }
            
            request.getSession().setAttribute("trackCode", trackCode);
            request.getSession().setAttribute("homeUrl", "home");

            groupHeaders = commonService.getGroupHeaders(1);

            if (groupHeaders.size() > 3) {
                groupHeaders.subList(3, groupHeaders.size()).clear();
            }
//            sprint 56
            List<CcPageMaster> pageMasterList = null;

            String callMeFlag = "";
            pageMasterList = commonService.getPageMasterDetail(1);
            
            for (CcPageMaster flag : pageMasterList) {
                logger.info("callme" + flag.getCallmeflag() + " promo" + flag.getPromocodeflag());
                System.out.println("callme" + flag.getCallmeflag() + " promo" + flag.getPromocodeflag());
                callMeFlag = String.valueOf(flag.getCallmeflag());
//            promocodeFlag=String.valueOf(flag.getPromocodeflag());
            }
            logger.info("callMeFlag" + callMeFlag);
            System.out.println("callMeFlag" + callMeFlag);

            model.addAttribute("callMeFlag", callMeFlag);
//            sprint 56 end
            System.out.println("groupHeaders>>>>>"+groupHeaders.toString());
            model.addAttribute("groupHeadings", groupHeaders);
            model.addAttribute("banner", commonService.getActiveBanner());

            model.addAttribute("cardType", commonService.getCardType());
            model.addAttribute("country", commonService.getCountry());
            
            
            System.out.println("countryFilter>>"+countryFilter);
            System.out.println("countrydropdown>>"+countrydropdown);
            System.out.println("countryOption>>"+countryOption);
            System.out.println("cardFilter>>"+cardFilter);

            if (countryFilter.equalsIgnoreCase("1")) {
            	System.out.println("countryFilter Inside India");
                name = "India";
            } else if (countryFilter.equalsIgnoreCase("3")) {

                name = "Bangladesh";
            }

            if (countrydropdown.equalsIgnoreCase("India")) {
            	System.out.println("countrydropdown Inside India");
            	name = "India";

            } else if (countrydropdown.equalsIgnoreCase("Bangladesh")) {
                name = "Bangladesh";

            } else {
            	System.out.println("Inside else countrydropdown");
            	name = "India";

            }
            if (countryOption.equalsIgnoreCase("1")) {
            	System.out.println("countryOption Inside India");
                name = "India";
            } else if (countryOption.equalsIgnoreCase("3")) {

                name = "Bangladesh";
            }
            
            String c = (String) request.getSession().getAttribute("cat");
            System.out.println("session cat c>>"+c);
            if (!cardFilter.equalsIgnoreCase("")) {
//                String c;

                if (cardFilter.equalsIgnoreCase("1")) {
                    c = "Personal Credit Card";
                } else if (cardFilter.equalsIgnoreCase("2")) {
                    c = "Personal Debit Card";
                } else if (cardFilter.equalsIgnoreCase("3")) {
                    c = "Corporate Card";
                } else if (cardFilter.equalsIgnoreCase("4")) {
                    c = "Others";
                } else if (cardFilter.equalsIgnoreCase("5")){
                    c = "All";
                }       
                else {
                    c = "Personal Credit Card";
                }

            } else {

            }
            request.getSession().setAttribute("catfilter", c);

            model.addAttribute("countryset", name);
            System.out.println("bankFilter>>>>"+bankFilter.toString());
            model.addAttribute("bankset", bankFilter);
            model.addAttribute("joiningFeeset", joiningFees);

            model.addAttribute("lsBenefitset", lsBenefitsFilter);

            model.addAttribute("prefList", cardSearchService.getPreferenceList());
            
            System.out.println("name>>"+name);
            if (name.equalsIgnoreCase("")) {
                List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(1, 1);

                model.addAttribute("bankList", a);
                model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                model.addAttribute("cardList3", cardSearchService.getAllCardList());
                model.addAttribute("cardList", cardSearchService.getAllCardList());
                model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());

            } else if (name.equalsIgnoreCase("India")) {
            	
                if (c.equalsIgnoreCase("Personal Debit Card")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(2, 1);

                    model.addAttribute("bankList", a);
                    model.addAttribute("category", c);
                    model.addAttribute("cardList", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList3", cardSearchService.getAllCardList());
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());

                } else if (c.equalsIgnoreCase("Corporate Card")) {
                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(3, 1);

                    model.addAttribute("bankList", a);
                    model.addAttribute("category", c);
                    model.addAttribute("cardList", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList3", cardSearchService.getAllCardList());
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());

                } else if (c.equalsIgnoreCase("Others")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(4, 1);

                    model.addAttribute("bankList", a);
                    model.addAttribute("category", c);
                    model.addAttribute("cardList", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList3", cardSearchService.getAllCardList());
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());

                } 
                
                 else if (c.equalsIgnoreCase("All")) {
                     
                    System.out.println("Inside ALl Home ");
                     List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(5, 1);

                    model.addAttribute("bankList", a);
                    model.addAttribute("category", c);
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList3", cardSearchService.getAllCardList());
                    
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());
                     
                 }
                
                else {
                	System.out.println("Inside else Personal Credit Card");
                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(1, 1);
                    model.addAttribute("bankList", a);
                    model.addAttribute("cardList", cardSearchService.getAllCardList());
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList3", cardSearchService.getAllCardList());
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());

                }
            } else if (name.equalsIgnoreCase("Bangladesh")) {

                if (c.equalsIgnoreCase("Personal Debit Card")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(2, 3);

                    model.addAttribute("bankList", a);
                    model.addAttribute("category", c);
                    model.addAttribute("cardList", cardSearchService.getSelectedDebitCardList(3));
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(3));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(3));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(3));
                    model.addAttribute("cardList3", cardSearchService.getSelectedBankAllCardList());
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());
                } else if (c.equalsIgnoreCase("Corporate Card")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(3, 3);
                    model.addAttribute("bankList", a);
                    model.addAttribute("category", c);
                    model.addAttribute("cardList", cardSearchService.getSelectedCorporateCardList(3));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(3));
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(3));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(3));
                    model.addAttribute("cardList3", cardSearchService.getSelectedBankAllCardList());
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());

                } else if (c.equalsIgnoreCase("Others")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(4, 3);
                    model.addAttribute("bankList", a);
                    model.addAttribute("category", c);
                    model.addAttribute("cardList", cardSearchService.getSelectedOtherCardList(3));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(3));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(3));
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(3));
                    model.addAttribute("cardList3", cardSearchService.getSelectedBankAllCardList());
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());

                } 
                else if (c.equalsIgnoreCase("All")) {
                     System.out.println(">>>>>>>>>>>>>>>>>>>>>>1"+c);
                      List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(5, 1);

                    model.addAttribute("bankList", a);
                    model.addAttribute("category", c);
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList3", cardSearchService.getAllCardList());
                    
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());
                     
                 }
                
                else {
                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(1, 3);
                    model.addAttribute("bankList", a);
                    model.addAttribute("cardList", cardSearchService.getSelectedBankAllCardList());
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(3));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(3));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(3));
                    model.addAttribute("cardList3", cardSearchService.getSelectedBankAllCardList());

                }

            }
            model.addAttribute("bankset", bankFilter);
            model.addAttribute("cityNamesList", cardSearchService.getCity());
            model.addAttribute("genderStatus", commonService.getEnumValues("Gender"));
            model.addAttribute("enrollBean", new EnrollBean());
            model.addAttribute("spMailingID", spMailingID);
            model.addAttribute("spUserID", spUserID);
            model.addAttribute("spJobID", spJobID);
            model.addAttribute("spReportId", spReportId);
            model.addAttribute(UTM_SOURCE, utm_source);
            model.addAttribute(UTM_MEDIUM, utm_medium);
            model.addAttribute(UTM_CAMPAIGN, utm_campaign);
            model.addAttribute("recPage", recPage);
            model.addAttribute("bankFilter", bankFilter);
            model.addAttribute("lsBenefitsFilter", lsBenefitsFilter);
            model.addAttribute("bpupgradeurl", commonService.getUpgrdURL());
            model.addAttribute("bpupgrademessage", commonService.getUpgrdMessage());
            model.addAttribute("joiningFees", joiningFees);
            model.addAttribute("utm_source", utm_source);
            model.addAttribute("utm_medium", utm_medium);
            model.addAttribute("utm_campaign", utm_campaign);
            model.addAttribute("pageinfo", "Home");
            CallMeBean callMeBean = new CallMeBean();
            if (null != jpNumber && jpNumber.length()>8) {
                memberDetailsService.getMemberDetailsCallmeFunction(jpNumber, callMeBean);
                model.addAttribute("callMeBean", callMeBean);

                String appstatus = commonService.getAmexAppStatus(jpNumber);

                String newString = appstatus.substring(1, appstatus.length() - 1);
                String randomNumber = commonService.getRandomNumber(jpNumber);
                String randomNumber1 = randomNumber.substring(1, randomNumber.length() - 1);

                model.addAttribute("randomNumber", randomNumber1);
                model.addAttribute("AmexAppStatus", newString);
                memberDetailsService.getMemberDetails(jpNumber, amexBean);
//                 HttpSession s=request.getSession();
//               s.setAttribute("loggedInUserDob",amexBean.getDateOfBirth());
                
                String dobOfSess = (String) request.getSession().getAttribute("loggedInUserDob");
                logger.info("dob of session===>" + getEncValue(dobOfSess, strSoapURL1, soapActEnc));
//            loggedInUserDob=(String) request.getSession().getAttribute("loggedInUserDob");

                cardDetailsBean = new CardDetailsBean();
                cardDetailsBean.setTitle(amexBean.getTitle());
                cardDetailsBean.setFname(amexBean.getFname());
                cardDetailsBean.setMname(amexBean.getMname());
                cardDetailsBean.setLname(amexBean.getLname());
                cardDetailsBean.setMobile(amexBean.getMobile());
                cardDetailsBean.setEmail(amexBean.getEmail());
//                cardDetailsBean.setDob(amexBean.getDateOfBirth());//old
                cardDetailsBean.setDob(loggedInUserDob);//new
                cardDetailsBean.setGender(amexBean.getGender());
                jpNumber = amexBean.getJetpriviligemembershipNumber();
                jpNumber = CommonUtils.getJPNumber(jpNumber);
                loggedInUser = jpNumber;
//                cardDetailsBean.setJpNumber(jpNumber);//old
                cardDetailsBean.setJpNumber(loggedInUser);//new
                cardDetailsBean.setJpTier(amexBean.getJetpriviligemembershipTier());
                model.addAttribute("memberCity", amexBean.getCity());
                model.addAttribute(BEAN_NAME, cardDetailsBean);
            } /* only user is not logged in*/ else {
                NonceStorage nonceStorage = new RequestNonceStorage(request);
                String nonce = nonceGenerator.generateNonce();
                nonceStorage.setState(nonce);
                model.addAttribute("callMeBean", callMeBean);
                model.addAttribute("state", nonce);
                request.setAttribute("state", nonce);
                model.addAttribute("cardDetailsBean", new CardDetailsBean());
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in HomeController homePage() :", e);
        }



        return "homePage";
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/best-co-branded-cards/{city}/{dob}/{flightRange}/{offerId}/{monthlySpend}/{annualIncome}/{gmNo}"})
    public String getRecommendPageAfterLogin(@ModelAttribute(BEAN_NAME) CardDetailsBean cardDetailsBean, HttpServletRequest request, ModelMap model,
            @PathVariable("city") String city, @PathVariable("dob") String dob, @PathVariable("flightRange") Integer flightRange,
            @PathVariable("offerId") String offerId, @PathVariable("monthlySpend") String monthlySpend, @PathVariable("annualIncome") String annualIncome,
            @PathVariable("gmNo") Integer gmNo) {
        System.out.println("*****************GET called******************");
        String lsbpArray[] = offerId.split(",");
        cardDetailsBean.setCityName(city);
        System.out.println("get set dob" + dob);
        cardDetailsBean.setDob(dob);
        cardDetailsBean.setFlightRange(flightRange);
        cardDetailsBean.setLsbpArray(lsbpArray);
        cardDetailsBean.setMonthlySpend(monthlySpend);
        cardDetailsBean.setAnnualIncome(annualIncome);
        cardDetailsBean.setGmNo(gmNo);
        System.out.println("annual in get" + cardDetailsBean.getAnnualIncome());
        List<CardDetailsBean> recommendList = null;
        List<String> groupHeaders = null;
        AmexBean amexBean = new AmexBean();
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        try {

            StringBuilder builder = new StringBuilder();

            for (String s : lsbpArray) {
                builder.append(s);
            }
            String offerIds = builder.toString();

            recommendList = recommendCardService.getRecommndedCards(cardDetailsBean);

            if (!recommendList.isEmpty()) {
                model.addAttribute(APPLICATION_URL, applicationPropertiesUtil.getApplicationURL());
                utm_source = cardDetailsBean.getUtmSource();
                utm_medium = cardDetailsBean.getUtmMedium();
                utm_campaign = cardDetailsBean.getUtmCampaign();

                if (null != utm_source && !utm_source.isEmpty()) {
                    trackCode = "?utm_source=" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                    amexBean.setUtmSource(utm_source);
                    amexBean.setUtmMedium(utm_medium);
                    amexBean.setUtmCampaign(utm_campaign);
                }
                request.getSession().setAttribute(REDIRECT_URL, "home" + trackCode);
                if (null != utm_source && !utm_source.isEmpty()) {
                    trackCode = "utm_" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                }
                request.getSession().setAttribute("homeUrl", "home");
//                request.getSession().setAttribute(REDIRECT_URL, "best-co-branded-cards/" + cardDetailsBean.getCityName() + "/" + cardDetailsBean.getDob() + "/" + cardDetailsBean.getFlightRange() + "/" + offerId + "/" + cardDetailsBean.getMonthlySpend() + "/" + cardDetailsBean.getAnnualIncome() + "/" + cardDetailsBean.getGmNo());
                request.getSession().setAttribute("trackCode", trackCode);
//                if (request.getSession().getAttribute("loggedInUser") == null) {
//                    request.getSession().setAttribute(REDIRECT_URL, "best-co-branded-cards/" + cardDetailsBean.getCityName() + "/" + cardDetailsBean.getDob() + "/" + cardDetailsBean.getFlightRange() + "/" + offerId + "/" + cardDetailsBean.getMonthlySpend() + "/" + cardDetailsBean.getAnnualIncome() + "/" + cardDetailsBean.getGmNo());
//                } else {

//                }
                groupHeaders = commonService.getGroupHeaders(1);
                if (groupHeaders.size() > 3) {
                    groupHeaders.subList(3, groupHeaders.size()).clear();
                }
//                sprint 56
                List<CcPageMaster> pageMasterList = null;

                String callMeFlag = "";
                pageMasterList = commonService.getPageMasterDetail(6);
                for (CcPageMaster flag : pageMasterList) {
                    logger.info("callme rec get" + flag.getCallmeflag() + " promo" + flag.getPromocodeflag());
                    callMeFlag = String.valueOf(flag.getCallmeflag());

                }
                logger.info("callMeFlag get" + callMeFlag);

                model.addAttribute("callMeFlag", callMeFlag);

//            sprint 56 end
                model.addAttribute("groupHeadings", groupHeaders);
                model.addAttribute("banner", commonService.getActiveBanner());
                List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(1, 1);
                model.addAttribute("bpupgradeurl", commonService.getUpgrdURL());
                model.addAttribute("bpupgrademessage", commonService.getUpgrdMessage());
                model.addAttribute("bankList", a);
                model.addAttribute("prefList", cardSearchService.getPreferenceList());
                model.addAttribute("spendValue", cardDetailsBean.getMonthlySpend());
                model.addAttribute("cityNamesList", cardSearchService.getCity());
                model.addAttribute("genderStatus", commonService.getEnumValues("Gender"));
                model.addAttribute("enrollBean", new EnrollBean());
                model.addAttribute("recommendList", recommendList);
                model.addAttribute(UTM_SOURCE, utm_source);
                model.addAttribute(UTM_MEDIUM, utm_medium);
                model.addAttribute(UTM_CAMPAIGN, utm_campaign);
                model.addAttribute("pageinfo", "Recommend");
                String jpNumber = getLoggedInUser(request);

                CallMeBean callMeBean = new CallMeBean();
                if (null != jpNumber && jpNumber.length()>8) {
                    memberDetailsService.getMemberDetails(jpNumber, amexBean);
                    callMeBean = memberDetailsService.getMemberDetailsCallmeFunction(jpNumber, callMeBean);
                    model.addAttribute("callMeBean", callMeBean);
                    CardDetailsBean cardDetailsBean1 = new CardDetailsBean();
                    cardDetailsBean1.setTitle(amexBean.getTitle());
                    cardDetailsBean1.setFname(amexBean.getFname());
                    cardDetailsBean1.setMname(amexBean.getMname());
                    cardDetailsBean1.setLname(amexBean.getLname());
                    cardDetailsBean1.setMobile(amexBean.getMobile());
                    cardDetailsBean1.setEmail(amexBean.getEmail());

                    System.out.println("");
                    
//                    cardDetailsBean1.setDob(amexBean.getDateOfBirth());//old
                    cardDetailsBean1.setDob(loggedInUserDob);//new
                    cardDetailsBean1.setGender(amexBean.getGender());
                    jpNumber = amexBean.getJetpriviligemembershipNumber();
                    jpNumber = CommonUtils.getJPNumber(jpNumber);
                    loggedInUser = jpNumber;
//                    cardDetailsBean1.setJpNumber(jpNumber);//old
                    cardDetailsBean1.setJpNumber(loggedInUser);//new
                    cardDetailsBean1.setJpTier(amexBean.getJetpriviligemembershipTier());
                    model.addAttribute("memberCity", amexBean.getCity());
                    model.addAttribute(BEAN_NAME, cardDetailsBean1);
                } else {
                    model.addAttribute("callMeBean", callMeBean);
                    model.addAttribute(BEAN_NAME, cardDetailsBean);
                    return "recommendPage";
                }
                model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getRcTitle());
                model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getRcDescription());
                model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getRcKeyword());
                return "recommendPage";
            } else {
                return "redirect:/not-eligible";

            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in HomeController homePageSubmit() :", e);
        }

        return "redirect:/not-eligible";

    }

    @RequestMapping(value = "/getcountry", method = RequestMethod.GET)
    public String checkCountryFilter(@ModelAttribute("countryOption") String countryOption, HttpServletRequest request, ModelMap model,
            @ModelAttribute("rcPage") String recPage,
            @ModelAttribute("bank") String bankFilter,
            @ModelAttribute("lsBenefits") String lsBenefitsFilter,
            @ModelAttribute("cardFilter") String cardFilter,
            @ModelAttribute("countryFilter") String countryFilter,
            @ModelAttribute("joiningFees") String joiningFees,
            @ModelAttribute("utm_source") String utmsource,
            @ModelAttribute("utm_medium") String utmmedium,
            @ModelAttribute("utm_campaign") String utmcampaign,
            @ModelAttribute("cardtype") String cardtypetest) {
        request.getSession().setAttribute("cat", cardtypetest);
        name = countryOption;
        countrymm = countryOption;
        bankName = bankFilter;
        lsBenefitsName = lsBenefitsFilter;
        joiningFeesName = joiningFees;

        List<String> groupHeaders = null;
        CardDetailsBean cardDetailsBean = null;
        AmexBean amexBean = new AmexBean();
        String spMailingID = "";
        String spUserID = "";
        String spJobID = "";
        String spReportId = "";
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";

        String jpNumber = getLoggedInUser(request);
        model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getHpTitle());
        model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getHpDescription());
        model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getHpKeyword());
        try {

            spMailingID = CommonUtils.nullSafe(request.getParameter("spMailingID"), "");
            spUserID = CommonUtils.nullSafe(request.getParameter("spUserID"), "");
            spJobID = CommonUtils.nullSafe(request.getParameter("spJobID"), "");
            spReportId = CommonUtils.nullSafe(request.getParameter("spReportId"), "");

            utm_source = CommonUtils.nullSafe(request.getParameter(UTM_SOURCE), "");
            utm_medium = CommonUtils.nullSafe(request.getParameter(UTM_MEDIUM), "");
            utm_campaign = CommonUtils.nullSafe(request.getParameter(UTM_CAMPAIGN), "");
            if (!utmsource.isEmpty()) {
                utm_source = utmsource;
                utm_medium = utmmedium;
                utm_campaign = utmcampaign;
            }
            model.addAttribute(APPLICATION_URL, applicationPropertiesUtil.getApplicationURL());
            if (null != spMailingID && !spMailingID.isEmpty()) {
                trackCode = "?spMailingID=" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
            }
            if (null != utm_source && !utm_source.isEmpty()) {
                trackCode = "?utm_source=" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                amexBean.setUtmSource(utm_source);
                amexBean.setUtmMedium(utm_medium);
                amexBean.setUtmCampaign(utm_campaign);
            }
            request.getSession().setAttribute(REDIRECT_URL, "home" + trackCode);
            if (null != spMailingID && !spMailingID.isEmpty()) {
                trackCode = "lw_" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
            }
            if (null != utm_source && !utm_source.isEmpty()) {
                trackCode = "utm_" + utm_source + "_" + utm_medium + "_" + utm_campaign;
            }
            request.getSession().setAttribute("trackCode", trackCode);
            request.getSession().setAttribute("homeUrl", "home");

            groupHeaders = commonService.getGroupHeaders(1);

            if (groupHeaders.size() > 3) {
                groupHeaders.subList(3, groupHeaders.size()).clear();
            }

            model.addAttribute("groupHeadings", groupHeaders);
            model.addAttribute("banner", commonService.getActiveBanner());
            model.addAttribute("bankList", cardSearchService.getBankList());
            model.addAttribute("prefList", cardSearchService.getPreferenceList());
            model.addAttribute("cityNamesList", cardSearchService.getCity());
            model.addAttribute("genderStatus", commonService.getEnumValues("Gender"));
            model.addAttribute("enrollBean", new EnrollBean());
            model.addAttribute("spMailingID", spMailingID);
            model.addAttribute("spUserID", spUserID);
            model.addAttribute("spJobID", spJobID);
            model.addAttribute("spReportId", spReportId);
            model.addAttribute(UTM_SOURCE, utm_source);
            model.addAttribute(UTM_MEDIUM, utm_medium);
            model.addAttribute(UTM_CAMPAIGN, utm_campaign);
            model.addAttribute("recPage", recPage);
            model.addAttribute("bankFilter", bankFilter);
            model.addAttribute("lsBenefitsFilter", lsBenefitsFilter);
            model.addAttribute("bpupgradeurl", commonService.getUpgrdURL());
            model.addAttribute("bpupgrademessage", commonService.getUpgrdMessage());
            model.addAttribute("utm_source", utm_source);
            model.addAttribute("utm_medium", utm_medium);
            model.addAttribute("utm_campaign", utm_campaign);
            model.addAttribute("pageinfo", "Home");
            /* only user is logged in*/
            if (null != jpNumber && jpNumber.length()>8) {

                String appstatus = commonService.getAmexAppStatus(jpNumber);

                String newString = appstatus.substring(1, appstatus.length() - 1);
                String randomNumber = commonService.getRandomNumber(jpNumber);
                String randomNumber1 = randomNumber.substring(1, randomNumber.length() - 1);

                model.addAttribute("randomNumber", randomNumber1);

                model.addAttribute("AmexAppStatus", newString);
                memberDetailsService.getMemberDetails(jpNumber, amexBean);
                cardDetailsBean = new CardDetailsBean();
                cardDetailsBean.setTitle(amexBean.getTitle());
                cardDetailsBean.setFname(amexBean.getFname());
                cardDetailsBean.setMname(amexBean.getMname());
                cardDetailsBean.setLname(amexBean.getLname());
                cardDetailsBean.setMobile(amexBean.getMobile());
                cardDetailsBean.setEmail(amexBean.getEmail());
//                cardDetailsBean.setDob(amexBean.getDateOfBirth());
                cardDetailsBean.setDob(loggedInUserDob);//new
                cardDetailsBean.setGender(amexBean.getGender());
                jpNumber = amexBean.getJetpriviligemembershipNumber();
                jpNumber = CommonUtils.getJPNumber(jpNumber);
                loggedInUser = jpNumber;
//                cardDetailsBean.setJpNumber(jpNumber);//old
                cardDetailsBean.setJpNumber(loggedInUser);//new
                cardDetailsBean.setJpTier(amexBean.getJetpriviligemembershipTier());
                model.addAttribute("memberCity", amexBean.getCity());
                model.addAttribute(BEAN_NAME, cardDetailsBean);
            } /* only user is not logged in*/ else {
                NonceStorage nonceStorage = new RequestNonceStorage(request);
                String nonce = nonceGenerator.generateNonce();
                nonceStorage.setState(nonce);
                model.addAttribute("state", nonce);
                request.setAttribute("state", nonce);
                model.addAttribute("cardDetailsBean", new CardDetailsBean());
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in HomeController homePage() :", e);
        }

        return "homePage";

    }

    @RequestMapping(value = "/best-co-branded-cards", method = RequestMethod.POST, produces = "text/plain")
    public String getRecommendPage(@ModelAttribute(BEAN_NAME) CardDetailsBean cardDetailsBean, BindingResult result, ModelMap model, HttpServletRequest request) {
        System.out.println("*****************POST called******************");
        List<CardDetailsBean> bankCreditList = null;

        List<CardDetailsBean> recommendList = null;
        List<String> groupHeaders = null;
        AmexBean amexBean = new AmexBean();
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        try {
            System.out.println("-------post controller------->" + cardDetailsBean.getDob());
            System.out.println("-------post controller------->" + cardDetailsBean.getAnnualIncome());
            recommendList = recommendCardService.getRecommndedCards(cardDetailsBean);

            bankCreditList = cardSearchService.getbanklistcreditindia(1, 1);
            model.addAttribute("bankList", bankCreditList);
            if (!recommendList.isEmpty()) {

                String[] arr = cardDetailsBean.getLsbpArray();
                StringBuilder builder = new StringBuilder();

                for (String s : arr) {
                    builder.append(s);
                }
                String offerId = builder.toString();

                model.addAttribute(APPLICATION_URL, applicationPropertiesUtil.getApplicationURL());

                request.getSession().setAttribute(REDIRECT_URL, "best-co-branded-cards/" + cardDetailsBean.getCityName() + "/" + cardDetailsBean.getDob() + "/" + cardDetailsBean.getFlightRange() + "/" + offerId + "/" + cardDetailsBean.getMonthlySpend() + "/" + cardDetailsBean.getAnnualIncome() + "/" + cardDetailsBean.getGmNo());

                utm_source = cardDetailsBean.getUtmSource();
                utm_medium = cardDetailsBean.getUtmMedium();
                utm_campaign = cardDetailsBean.getUtmCampaign();

                model.addAttribute(APPLICATION_URL, applicationPropertiesUtil.getApplicationURL());
                if (null != utm_source && !utm_source.isEmpty()) {
                    trackCode = "?utm_source=" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                    amexBean.setUtmSource(utm_source);
                    amexBean.setUtmMedium(utm_medium);
                    amexBean.setUtmCampaign(utm_campaign);
                }
                request.getSession().setAttribute(REDIRECT_URL, "home" + trackCode);
                if (null != utm_source && !utm_source.isEmpty()) {
                    trackCode = "utm_" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                }
                request.getSession().setAttribute("trackCode", trackCode);
                request.getSession().setAttribute("homeUrl", "home");
//                request.getSession().setAttribute(REDIRECT_URL, "best-co-branded-cards/" + cardDetailsBean.getCityName() + "/" + cardDetailsBean.getDob() + "/" + cardDetailsBean.getFlightRange() + "/" + offerId + "/" + cardDetailsBean.getMonthlySpend() + "/" + cardDetailsBean.getAnnualIncome() + "/" + cardDetailsBean.getGmNo());

                groupHeaders = commonService.getGroupHeaders(1);
                if (groupHeaders.size() > 3) {
                    groupHeaders.subList(3, groupHeaders.size()).clear();
                }
//                sprint 56
                List<CcPageMaster> pageMasterList = null;

                String callMeFlag = "";
                pageMasterList = commonService.getPageMasterDetail(6);
                for (CcPageMaster flag : pageMasterList) {
                    logger.info("callme rec get" + flag.getCallmeflag() + " promo" + flag.getPromocodeflag());
                    callMeFlag = String.valueOf(flag.getCallmeflag());

                }
                logger.info("callMeFlag get" + callMeFlag);

                model.addAttribute("callMeFlag", callMeFlag);

//            sprint 56 end
                model.addAttribute("groupHeadings", groupHeaders);
                model.addAttribute("banner", commonService.getActiveBanner());
                model.addAttribute("prefList", cardSearchService.getPreferenceList());
                model.addAttribute("spendValue", cardDetailsBean.getMonthlySpend());
                model.addAttribute("cityNamesList", cardSearchService.getCity());
                model.addAttribute("genderStatus", commonService.getEnumValues("Gender"));
                model.addAttribute("enrollBean", new EnrollBean());
                model.addAttribute("recommendList", recommendList);
                model.addAttribute(UTM_SOURCE, utm_source);
                model.addAttribute(UTM_MEDIUM, utm_medium);
                model.addAttribute(UTM_CAMPAIGN, utm_campaign);
                model.addAttribute("bpupgradeurl", commonService.getUpgrdURL());
                model.addAttribute("bpupgrademessage", commonService.getUpgrdMessage());
                model.addAttribute("pageinfo", "Recommend");
                String jpNumber = getLoggedInUser(request);
                CallMeBean callMeBean = new CallMeBean();
                if (null != jpNumber && jpNumber.length()>8) {
                    memberDetailsService.getMemberDetails(jpNumber, amexBean);
                    callMeBean = memberDetailsService.getMemberDetailsCallmeFunction(jpNumber, callMeBean);
                    model.addAttribute("callMeBean", callMeBean);
                    CardDetailsBean cardDetailsBean1 = new CardDetailsBean();
                    cardDetailsBean1.setTitle(amexBean.getTitle());
                    cardDetailsBean1.setFname(amexBean.getFname());
                    cardDetailsBean1.setMname(amexBean.getMname());
                    cardDetailsBean1.setLname(amexBean.getLname());
                    cardDetailsBean1.setMobile(amexBean.getMobile());
                    cardDetailsBean1.setEmail(amexBean.getEmail());

//                    cardDetailsBean1.setDob(amexBean.getDateOfBirth());//old
                    cardDetailsBean1.setDob(loggedInUserDob);//new
                    cardDetailsBean1.setGender(amexBean.getGender());
                    jpNumber = amexBean.getJetpriviligemembershipNumber();
                    jpNumber = CommonUtils.getJPNumber(jpNumber);
                    loggedInUser = jpNumber;
//                    cardDetailsBean1.setJpNumber(jpNumber);//old
                    cardDetailsBean1.setJpNumber(loggedInUser);
                    cardDetailsBean1.setJpTier(amexBean.getJetpriviligemembershipTier());
                    model.addAttribute("memberCity", amexBean.getCity());
                    model.addAttribute(BEAN_NAME, cardDetailsBean1);
                } else {
                    model.addAttribute(BEAN_NAME, new CardDetailsBean());
                    model.addAttribute("callMeBean", callMeBean);
                }
                model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getRcTitle());
                model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getRcDescription());
                model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getRcKeyword());
                return "recommendPage";
            } else {
                return "redirect:/not-eligible";
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in HomeController homePageSubmit() :", e);
        }

        return "redirect:/not-eligible";

    }

    @RequestMapping(value = "/cardswidget")
    public String getLWUrls(HttpServletRequest request, RedirectAttributes redirect) {

        String rcPage = "";
        String bank = "";
        String lsBenefits = "";
        String cardFilter = "";
        String countryFilter = "";
        String joiningFees = "", utm_source = "", utm_medium = "", utm_campaign = "";

        try {
            rcPage = CommonUtils.nullSafe(request.getParameter("recommendPage"), "").intern();
            bank = CommonUtils.nullSafe(request.getParameter("bank"), "").intern();
            lsBenefits = CommonUtils.nullSafe(request.getParameter("lsBenefits"), "").intern();
            cardFilter = CommonUtils.nullSafe(request.getParameter("cardFilter"), "").intern();
            countryFilter = CommonUtils.nullSafe(request.getParameter("country"), "").intern();
            joiningFees = CommonUtils.nullSafe(request.getParameter("joiningFees"), "").intern();
            utm_source = CommonUtils.nullSafe(request.getParameter("utm_source"), "").intern();
            utm_medium = CommonUtils.nullSafe(request.getParameter("utm_medium"), "").intern();
            utm_campaign = CommonUtils.nullSafe(request.getParameter("utm_campaign"), "").intern();
            redirect.addFlashAttribute("rcPage", rcPage);
            redirect.addFlashAttribute("bank", bank);
            redirect.addFlashAttribute("lsBenefits", lsBenefits);
            redirect.addFlashAttribute("cardFilter", cardFilter);
            redirect.addFlashAttribute("countryFilter", countryFilter);
            redirect.addFlashAttribute("joiningFees", joiningFees);
            redirect.addFlashAttribute("utm_source", utm_source);
            redirect.addFlashAttribute("utm_medium", utm_medium);
            redirect.addFlashAttribute("utm_campaign", utm_campaign);
        } catch (Exception e) {
            logger.error("@@@@ Exception in HomeController getLWUrls() :", e);
        }
        return "redirect:/";
    }

    public String getLoggedInUser(HttpServletRequest request) {
        System.out.println("-------inside----getLoggedInUser-----"+request.getSession().getAttribute("loggedInUser"));
        
        return (String) request.getSession().getAttribute("loggedInUser");
    }

    @RequestMapping(value = "getCityNames")
    @ResponseBody
    public Map<String, String> getCityNames(HttpServletRequest request) {
        Map<String, String> cityMap = null;
        try {
            String cityName = CommonUtils.nullSafe(request.getParameter("cityName"));
            cityMap = commonService.getCityNames(cityName);
        } catch (Exception e) {
            logger.error("@@@@ Exception in HomeController getCityNames() :", e);
        }

        return cityMap;

    }

    @RequestMapping(method = RequestMethod.GET, value = "/not-eligible")
    public String notEligiblePage(ModelMap model) {

        model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getNeTitle());
        model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getNeDescription());
        model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getNeKeyword());
        return "recommendMessage";
    }

    @RequestMapping(value = "/getcategory", method = RequestMethod.GET)
    public String checkCategoryFilter(HttpServletRequest request, ModelMap model,
            @ModelAttribute("rcPage") String recPage,
            @ModelAttribute("bank") String bankFilter,
            @ModelAttribute("lsBenefits") String lsBenefitsFilter,
            @ModelAttribute("countryOption") String countryOption,
            @ModelAttribute("cardtype") String cardtype,
            @ModelAttribute("joiningFees") String joiningFees,
            @ModelAttribute("utm_source") String utmsource,
            @ModelAttribute("utm_medium") String utmmedium,
            @ModelAttribute("utm_campaign") String utmcampaign,
            @ModelAttribute("categoryOption") String categoryOption) {

        List<String> groupHeaders = null;
        CardDetailsBean cardDetailsBean = null;
        AmexBean amexBean = new AmexBean();
        String spMailingID = "";
        String spUserID = "";
        String spJobID = "";
        String spReportId = "";
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        String country1 = countryOption;
        String cardtype1 = cardtype;

        System.out.println("countryOption>>"+countryOption);
        
        String jpNumber = getLoggedInUser(request);
        model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getHpTitle());
        model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getHpDescription());
        model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getHpKeyword());
        try {

            spMailingID = CommonUtils.nullSafe(request.getParameter("spMailingID"), "");
            spUserID = CommonUtils.nullSafe(request.getParameter("spUserID"), "");
            spJobID = CommonUtils.nullSafe(request.getParameter("spJobID"), "");
            spReportId = CommonUtils.nullSafe(request.getParameter("spReportId"), "");

            utm_source = CommonUtils.nullSafe(request.getParameter(UTM_SOURCE), "");
            utm_medium = CommonUtils.nullSafe(request.getParameter(UTM_MEDIUM), "");
            utm_campaign = CommonUtils.nullSafe(request.getParameter(UTM_CAMPAIGN), "");
            if (!utmsource.isEmpty()) {
                utm_source = utmsource;
                utm_medium = utmmedium;
                utm_campaign = utmcampaign;
            }
            model.addAttribute(APPLICATION_URL, applicationPropertiesUtil.getApplicationURL());
            if (null != spMailingID && !spMailingID.isEmpty()) {
                trackCode = "?spMailingID=" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
            }
            if (null != utm_source && !utm_source.isEmpty()) {
                trackCode = "?utm_source=" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                amexBean.setUtmSource(utm_source);
                amexBean.setUtmMedium(utm_medium);
                amexBean.setUtmCampaign(utm_campaign);
            }
            request.getSession().setAttribute(REDIRECT_URL, "home" + trackCode);
            if (null != spMailingID && !spMailingID.isEmpty()) {
                trackCode = "lw_" + spMailingID + "_" + spUserID + "_" + spJobID + "_" + spReportId;
            }
            if (null != utm_source && !utm_source.isEmpty()) {
                trackCode = "utm_" + utm_source + "_" + utm_medium + "_" + utm_campaign;
            }
            request.getSession().setAttribute("trackCode", trackCode);
            request.getSession().setAttribute("homeUrl", "home");

            groupHeaders = commonService.getGroupHeaders(1);

            if (groupHeaders.size() > 3) {
                groupHeaders.subList(3, groupHeaders.size()).clear();
            }

            model.addAttribute("groupHeadings", groupHeaders);
            model.addAttribute("banner", commonService.getActiveBanner());

            model.addAttribute("cardType", commonService.getCardType());
            model.addAttribute("country", commonService.getCountry());

            model.addAttribute("prefList", cardSearchService.getPreferenceList());
            String c = (String) request.getSession().getAttribute("catfilter");
            if (countryOption.equalsIgnoreCase("1")) {
                countrymm = "India";
                country1 = countrymm;
            } else if (countryOption.equalsIgnoreCase("3")) {
                countrymm = "Bangladesh";
                country1 = countrymm;
            }
           
            System.out.println("country1>>"+country1);
            if (country1.equalsIgnoreCase("India")) {

                if (cardtype1.equalsIgnoreCase("Personal Debit Card")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(2, 1);

                    model.addAttribute("bankList", a);
                    model.addAttribute("countryset", countrymm);
                    model.addAttribute("category", cardtype1);
                    model.addAttribute("cardList", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList3", cardSearchService.getAllCardList());
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());
                    

                } else if (cardtype1.equalsIgnoreCase("Corporate Card")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(3, 1);

                    model.addAttribute("bankList", a);
                    model.addAttribute("countryset", countrymm);
                    model.addAttribute("category", cardtype1);
                    model.addAttribute("cardList", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList3", cardSearchService.getAllCardList());
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());
                    
                } else if (cardtype1.equalsIgnoreCase("Others")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(4, 1);

                    model.addAttribute("bankList", a);
                    model.addAttribute("countryset", countrymm);
                    model.addAttribute("category", cardtype1);
                    model.addAttribute("cardList", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList3", cardSearchService.getAllCardList());
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());
                    
                } 
                
                else if (cardtype1.equalsIgnoreCase("All")) {

                    
                    System.out.println("Inside All if");
                    
                     List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(5, 1);
                   
                    model.addAttribute("countryset", countrymm);
                    model.addAttribute("bankList", a);
                    model.addAttribute("category", cardtype1);
                    model.addAttribute("cardList", cardSearchService.getAllTypeCardList());
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                    model.addAttribute("cardList3", cardSearchService.getAllCardList());
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                    
                    
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());
                    System.out.println("after all");
                    
                }
                
                else {

                    if (countryOption.equalsIgnoreCase("3") || countryOption.equalsIgnoreCase("Bangladesh")) {

                        List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(1, 3);

                        model.addAttribute("bankList", a);
                        model.addAttribute("countryset", countryOption);
                        model.addAttribute("category", cardtype1);
                        model.addAttribute("cardList", cardSearchService.getSelectedBankAllCardList());
                        model.addAttribute("cardList3", cardSearchService.getSelectedBankAllCardList());

                        model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(3));
                        model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(3));
                        model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(3));

                    } else {

                        List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(1, 1);

                        model.addAttribute("bankList", a);
                        model.addAttribute("category", cardtype1);
                        model.addAttribute("cardList", cardSearchService.getAllCardList());
                        model.addAttribute("cardList3", cardSearchService.getAllCardList());
                        model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                        model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                        model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));
                        model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());
                    

                    }
                }
            } else {

                if (cardtype1.equalsIgnoreCase("Personal Debit Card")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(2, 3);
                    model.addAttribute("bankList", a);
                    model.addAttribute("countryset", countrymm);
                    model.addAttribute("category", cardtype1);
                    model.addAttribute("cardList", cardSearchService.getSelectedDebitCardList(3));
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(3));

                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(3));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(3));
                    model.addAttribute("cardList3", cardSearchService.getSelectedBankAllCardList());

                } else if (cardtype1.equalsIgnoreCase("Corporate Card")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(3, 3);
                    model.addAttribute("bankList", a);
                    model.addAttribute("countryset", countrymm);
                    model.addAttribute("category", cardtype1);
                    model.addAttribute("cardList", cardSearchService.getSelectedCorporateCardList(3));
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(3));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(3));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(3));
                    model.addAttribute("cardList3", cardSearchService.getSelectedBankAllCardList());

                } else if (cardtype1.equalsIgnoreCase("Others")) {

                    List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(4, 3);
                    model.addAttribute("bankList", a);
                    model.addAttribute("countryset", countrymm);
                    model.addAttribute("category", cardtype1);
                    model.addAttribute("cardList", cardSearchService.getSelectedOtherCardList(3));
                    model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(3));
                    model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(3));
                    model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(3));
                    model.addAttribute("cardList3", cardSearchService.getSelectedBankAllCardList());

                } 
                
                 else if (cardtype1.equalsIgnoreCase("All")) {

                    System.out.println("Inside All else");
                    
                     List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(5, 1);

                    
                    model.addAttribute("bankList", a);
                    model.addAttribute("countryset", countrymm);
                    model.addAttribute("category", cardtype1);
                    model.addAttribute("cardList", "");
                    model.addAttribute("cardList1", "");
                    model.addAttribute("cardList2", "");
                    model.addAttribute("cardList4", "");                    
                    model.addAttribute("cardList5", cardSearchService.getAllTypeCardList());

                } 
                
                
                else {

                    if (countryOption.equalsIgnoreCase("3") || countryOption.equalsIgnoreCase("Bangladesh")) {

                        List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(1, 3);

                        model.addAttribute("bankList", a);
                        model.addAttribute("countryset", countrymm);
                        model.addAttribute("category", cardtype1);
                        model.addAttribute("cardList", cardSearchService.getSelectedBankAllCardList());
                        model.addAttribute("cardList3", cardSearchService.getSelectedBankAllCardList());

                        model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(3));
                        model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(3));
                        model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(3));

                    } else {
                        List<CardDetailsBean> a = cardSearchService.getbanklistcreditindia(1, 1);

                        model.addAttribute("bankList", a);
                        model.addAttribute("category", cardtype1);
                        model.addAttribute("countryset", countrymm);
                        model.addAttribute("cardList", cardSearchService.getAllCardList());
                        model.addAttribute("cardList3", cardSearchService.getAllCardList());
                        model.addAttribute("cardList1", cardSearchService.getSelectedDebitCardList(1));
                        model.addAttribute("cardList2", cardSearchService.getSelectedCorporateCardList(1));
                        model.addAttribute("cardList4", cardSearchService.getSelectedOtherCardList(1));

                    }
                }

            }
//            model.addAttribute("bankset", bankName);
//            model.addAttribute("joiningFeeset", joiningFeesName);
//            model.addAttribute("lsBenefitset", lsBenefitsName);
System.out.println("After if and else");
            model.addAttribute("bankset", bankFilter);
            model.addAttribute("joiningFeeset", joiningFees);
            model.addAttribute("lsBenefitset", lsBenefitsFilter);
            model.addAttribute("cityNamesList", cardSearchService.getCity());
            model.addAttribute("genderStatus", commonService.getEnumValues("Gender"));
            model.addAttribute("enrollBean", new EnrollBean());
            model.addAttribute("spMailingID", spMailingID);
            model.addAttribute("spUserID", spUserID);
            model.addAttribute("spJobID", spJobID);
            model.addAttribute("spReportId", spReportId);
            model.addAttribute(UTM_SOURCE, utm_source);
            model.addAttribute(UTM_MEDIUM, utm_medium);
            model.addAttribute(UTM_CAMPAIGN, utm_campaign);
            model.addAttribute("recPage", recPage);
            model.addAttribute("bankFilter", bankFilter);
            model.addAttribute("lsBenefitsFilter", lsBenefitsFilter);
            model.addAttribute("bpupgradeurl", commonService.getUpgrdURL());
            model.addAttribute("bpupgrademessage", commonService.getUpgrdMessage());
            model.addAttribute("utm_source", utm_source);
            model.addAttribute("utm_medium", utm_medium);
            model.addAttribute("utm_campaign", utm_campaign);
            model.addAttribute("pageinfo", "Home");

            model.addAttribute("callMeBean", new CallMeBean());
            CallMeBean callMeBean = new CallMeBean();
            if (null != jpNumber && jpNumber.length()>8) {
                memberDetailsService.getMemberDetailsCallmeFunction(jpNumber, callMeBean);
                model.addAttribute("callMeBean", callMeBean);

                String appstatus = commonService.getAmexAppStatus(jpNumber);

                String newString = appstatus.substring(1, appstatus.length() - 1);
                String randomNumber = commonService.getRandomNumber(jpNumber);
                String randomNumber1 = randomNumber.substring(1, randomNumber.length() - 1);

                model.addAttribute("randomNumber", randomNumber1);
                model.addAttribute("AmexAppStatus", newString);
                memberDetailsService.getMemberDetails(jpNumber, amexBean);
                cardDetailsBean = new CardDetailsBean();
                cardDetailsBean.setTitle(amexBean.getTitle());
                cardDetailsBean.setFname(amexBean.getFname());
                cardDetailsBean.setMname(amexBean.getMname());
                cardDetailsBean.setLname(amexBean.getLname());
                cardDetailsBean.setMobile(amexBean.getMobile());
                cardDetailsBean.setEmail(amexBean.getEmail());
//                cardDetailsBean.setDob(amexBean.getDateOfBirth());
                cardDetailsBean.setDob(loggedInUserDob); // new change
                cardDetailsBean.setGender(amexBean.getGender());
                jpNumber = amexBean.getJetpriviligemembershipNumber();
                jpNumber = CommonUtils.getJPNumber(jpNumber);
                loggedInUser = jpNumber;
//                cardDetailsBean.setJpNumber(jpNumber);//old
                cardDetailsBean.setJpNumber(loggedInUser);//new
                cardDetailsBean.setJpTier(amexBean.getJetpriviligemembershipTier());
                model.addAttribute("memberCity", amexBean.getCity());
                model.addAttribute(BEAN_NAME, cardDetailsBean);
            } /* only user is not logged in*/ else {
                NonceStorage nonceStorage = new RequestNonceStorage(request);
                String nonce = nonceGenerator.generateNonce();
                nonceStorage.setState(nonce);
                model.addAttribute("state", nonce);
                request.setAttribute("state", nonce);
                model.addAttribute("cardDetailsBean", new CardDetailsBean());
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in HomeController homePage() :", e);
        }
        System.out.println("before return");
        return "homePage";
    }

    @RequestMapping(value = "/testing", method = RequestMethod.GET)
    @ResponseBody
    public String getCCType(@RequestParam("categoryOption") String categoryOption,
            @ModelAttribute("bank") String bankFilter,
            @ModelAttribute("lsBenefits") String lsBenefitsFilter,
            @ModelAttribute("utm_source") String utmsource,
            @ModelAttribute("utm_medium") String utmmedium,
            @ModelAttribute("utm_campaign") String utmcampaign,
            @ModelAttribute("joiningFees") String joiningFees) {

        category = categoryOption;
        bankName = bankFilter;
        lsBenefitsName = lsBenefitsFilter;
        joiningFeesName = joiningFees;

        return "success";
    }

    //New Header footer implementation start
    @RequestMapping(value = "/reactHeaderFooter", method = RequestMethod.GET)
    public void reactHeaderFooter(HttpServletRequest request, HttpServletResponse response) {

        try {
            String Auth0Json = (String) request.getSession().getAttribute("Auth0Json");
            System.out.println("Auth0Json:::"+Auth0Json);
            System.out.println("GetWindowUDString(loggedInUser, request)>>>"+GetWindowUDString(loggedInUser, request));

            logger.info("Window.UD is : " + getEncValue(GetWindowUDString(loggedInUser, request), Auth0Json, Auth0Json));
            String reactHeaderFooter= GetWindowUDString(loggedInUser, request) + "\n"
                    + "function loadJSDynamic(url, callback) \n"
                    + "{ \n"
                    + "var e = document.createElement('script'); \n"
                    + " e.src = url; \n"
                    + "e.type='text/javascript'; \n"
                    + "e.addEventListener('load', callback); \n"
                    + "document.getElementsByTagName('head')[0].appendChild(e); \n"
                    + "} \n"
                    + "loadJSDynamic ('" + reactHeaderFooterUrl + "', function() { \n"
                    + "console.log(\"JS file loaded successfully\");\n"
                    + "}); \n"
//                    +"var webAuthMS1Partners = window.auth0; \n"
//                    +"webAuthMS1Partners = new webAuthMS1Partners.WebAuth({ \n"
//                    +" domain: '"+authDomain+"', \n"
//                    +"clientID: '"+authClientId+"', \n"
//                    +"responseType: 'code', \n"
//                    +"redirectUri: '"+authCallBackUrl+"', \n"
//                    +"scope: 'openid profile email', \n"
//                    +"connection: AUTH0_CONNECTIONMS1 \n"
//                    +"});"
                    + "";

            response.getWriter().write(reactHeaderFooter);
        } catch (Exception e) {
            System.out.println("Exception at reactHeaderFooter======>" + e);

            logger.error("Exception at reactHeaderFooter ", e);
        }

    }

    public String GetWindowUDString(String jpNumber, HttpServletRequest request) {
        String firstName = (String) request.getSession().getAttribute("firstName");
        logger.info("jpNumber at GetWindowUDString()" + jpNumber);
        if (jpNumber.length() > 7) {
            String windowUD = "window.UD= { }" + " \n"
                    + "window.UD.name = '" + firstName + "' ;"
                    + "window.UD.cityofResidence = digitalData.userInfo.cityofResidence;"
                    + "window.UD.age = digitalData.userInfo.age;"
                    + "window.UD.JPNumber = digitalData.userInfo.JPNumber;"
                    + "window.UD.PointsBalanace = digitalData.userInfo.pointsBalance;"
                    + "window.UD.EmailVerified = digitalData.userInfo.EmailVerified;"
                    + "window.UD.MobileVerified = digitalData.userInfo.MobileVerified;"
                    + "window.UD.PersonId = digitalData.userInfo.PersonId;"
                    + "window.UD.activeStatus = digitalData.userInfo.activeStatus;"
                    + "window.UD.countryofResidence = digitalData.userInfo.countryofResidence;"
                    + "window.UD.firstloginDate = digitalData.userInfo.firstloginDate;"
                    + "window.UD.lastloginDate = digitalData.userInfo.lastloginDate;"
                    + "window.UD.memProgram = digitalData.userInfo.memProgram;"
                    + "window.UD.LoggedInUsing = digitalData.userInfo.usernameType;"
                    + "window.UD.tier = digitalData.userInfo.tier;"
                    + "window.UD.gender = digitalData.userInfo.gender;"
                    + "window.UD.sessionID = digitalData.userInfo.sessionID;"
                    + "window.UD.sessiondatetime = digitalData.userInfo.sessionDateTime;"
                    + "window.UD.cookieaccept = digitalData.userInfo.cookieAccept;"
                    + "window.UD.cookiedatetime = digitalData.userInfo.cookieDateTime;"
                    + "console.log('In reactFingerprintImpl Window.UD 1====>',window.UD); \n"
                    + "";
            return windowUD;
        } else {
            return "";
        }
    }

    @RequestMapping(value = "/reactFingerprintImpl", method = RequestMethod.GET)
    public void reactFingerprintImpl(HttpServletRequest request, HttpServletResponse response) {

        try {
            String Auth0Json = (String) request.getSession().getAttribute("Auth0Json");
            String cardsession = (String) request.getSession().getAttribute("CardsSession");
            logger.info("Auth0Json at reactHeaderFooter(): " + Auth0Json);
            logger.info("Logged In User: " + loggedInUser);
            // String jpNumber = getLoggedInUser(request);
            //String reactHeaderFooter = "" + Auth0Json + " \n"
            String reactHeaderFooter = GetWindowUDString(loggedInUser, request) + "\n"
                    + "var JPNo=\"" + loggedInUser + "\";"
                    + "var sessionID=\"" + cardsession + "\";"
                    + "function loadJSDynamicFingerprint(url, callback) \n"
                    + "{ \n"
                    + "var e = document.createElement('script'); \n"
                    + " e.src = url; \n"
                    + "e.type='text/javascript'; \n"
                    + "e.addEventListener('load', callback); \n"
                    + "document.getElementsByTagName('head')[0].appendChild(e); \n"
                    + "} \n"
                    + "loadJSDynamicFingerprint ('" + reactCommonGlobalUrl + "', function() { \n"
                    + "console.log(\"JS file for fingerprint & GDPR loaded successfully\");\n"
                    + "}); \n"
                    + "";

            response.getWriter().write(reactHeaderFooter);
        } catch (Exception e) {
            System.out.println("Exception at reactFingerprintImpl=====>" + e);
            logger.error("Exception at reactFingerprintImpl ", e);
        }

    }

    
    @RequestMapping(value = "/faq", method = RequestMethod.GET)
    public String faq(HttpServletRequest request,ModelMap model, HttpServletResponse response) {
        System.out.println("-------------->"+applicationPropertiesUtil.getApplicationURL());
        model.addAttribute(APPLICATION_URL, applicationPropertiesUtil.getApplicationURL());
        
        return "faqpage";
    
    }
    
    
    
    @RequestMapping(value = "/initDigitalData", method = RequestMethod.GET)
    public void initDigitalData(HttpServletRequest request, HttpServletResponse response) {

        try {
            JSONParser parser = new JSONParser();
            try {
                 System.out.println("inside not null at initDigitalData==========>"+request.getSession().getAttribute("loggedInUserDob"));
                if (request.getSession().getAttribute("loggedInUserDob") != null && request.getSession().getAttribute("loggedInUserDob") != "") {
                   
                    loggedInUserDob = (String) request.getSession().getAttribute("loggedInUserDob");

                    logger.info("loggedInUserDob at initDigitalData of session-->" + getEncValue(loggedInUserDob, strSoapURL1, soapActEnc));
                    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");

                    Date date = sdf.parse(loggedInUserDob);
                    SimpleDateFormat SimpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    loggedInUserDob = SimpleDateFormat.format(date);
                    logger.info("loggedInUserDob at initDigitalData of formated==>" + getEncValue(loggedInUserDob, strSoapURL1, soapActEnc));
//                     Cookie cookie2 = new Cookie("jpnum", "");
//                    cookie2.setMaxAge(0);
//                    response.addCookie(cookie2);
//                    Cookie cookie3 = new Cookie("user", "");
//                    cookie3.setMaxAge(0);
//                    response.addCookie(cookie3);

                    
                } else {
                    logger.info("else null at initDigitalData========>");
                    loggedInUserDob = "";
                }

                if (request.getSession().getAttribute("loggedInUser") != null) {
                    loggedInUser = (String) request.getSession().getAttribute("loggedInUser");
                    logger.info("loggedInUser at initDigitalData==>" + loggedInUser);
                    if(loggedInUser.length()==11 ){
                    loggedInUser = loggedInUser.substring(2);
                    }
//                    else if(loggedInUser.length()==9)
//                    {
//                    loggedInUser = loggedInUser.substring(2);
//                    }
                    
                } else {
                    loggedInUser = "";
                }
                logger.info("loggedInUser at initDigitalData final==>" + loggedInUser);

            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error("Exception at initdiditaldata====>" + ex);
            }

            String cardsessiontime = (String) request.getSession().getAttribute("CardsSessionTime");
            String cardsession = (String) request.getSession().getAttribute("CardsSession");
            StringBuffer url = request.getRequestURL();
            System.out.println("First login date value outside condition is.............."+request.getSession().getAttribute("firstLoginDate"));
            if (request.getSession().getAttribute("firstLoginDate") == null && request.getSession().getAttribute("lastLoginDate") == null) {
                firstlogindate = "";
                lastlogindate = "";
            } else {
                 System.out.println("last login date inside else part......"+(String) request.getSession().getAttribute("lastLoginDate"));
                firstlogindate = (String) request.getSession().getAttribute("firstLoginDate");
                lastlogindate = (String) request.getSession().getAttribute("lastLoginDate");
            }
            String digitaldata = (String) request.getSession().getAttribute("strDigitalData");
            //logger.info("digitaldata=======>" + digitaldata);
             JSONObject jsonObject = null;
             JSONObject jsonObjectLogs = null;
            if(!digitaldata.isEmpty()){
                    jsonObject  = (JSONObject) parser.parse(digitaldata);
                    jsonObjectLogs  = (JSONObject) parser.parse(digitaldata);
            }else{
            
                System.out.println("digitaldataelse=======>" + digitaldata);
            }
            
            String pointbal = (String) jsonObjectLogs.get("pointsBalance");
            jsonObjectLogs.put("pointsBalance", pointbal);
            logger.info("digitaldata=======>" + getEncValue(jsonObjectLogs.toString(), strSoapURL1, soapActEnc));
            
            String strDigitalData
                    = "var IsButtonClicked=false;"
                    + "var loggedInUserDob =\"" + loggedInUserDob + "\";"
                    + "var loggedInUser=\"" + loggedInUser + "\";"
                    + "var JSESSIONID =\"" + cardsession + "\";"
                    + "var JSESSIONTIME =\"" + cardsessiontime + "\";"
                    + "var a = " + digitaldata + ";"
                    + "var digitalData=" + jsonObject + ";"
                    + "var FIRSTLOGINDATE = \"" + firstlogindate + "\";"
                    + "var LASTLOGINDATE = \"" + lastlogindate + "\";"
                    + "var PID = \"" + (String) request.getSession().getAttribute("PID") + "\";"
                    + "digitalData.userInfo.pid =\"" + (String) request.getSession().getAttribute("PID") + "\";"
                    + "digitalData.userInfo.sessionID =\"" + cardsession + "\";"
                    + "digitalData.userInfo.sessionDateTime =\"" + cardsessiontime + "\";"
                    + "digitalData.userInfo.firstloginDate =\"" + firstlogindate + "\";"
                    + "digitalData.userInfo.lastloginDate =\"" + lastlogindate + "\";"
                    + "console.log('PID in DD is:'+digitalData.userInfo.pid);";
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("JetPrivilege_CCounter_C")) {

                        
                        strDigitalData = strDigitalData + "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':\n"
                                + "new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],\n"
                                + "j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=\n"
                                + "'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);\n"
                                + "})(window,document,'script','dataLayer','GTM-MN7RT6D');\n"
                                + "\n"
                                + "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':\n"
                                + "new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],\n"
                                + "j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=\n"
                                + "'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);\n"
                                + "})(window,document,'script','dataLayer','GTM-TTLP892');";

                        

                    }
                }
            }

            HttpSession s = request.getSession();
            s.setAttribute("strDigitalData", strDigitalData);
            strDigitalData = strDigitalData + GetWindowUDString(loggedInUser, request);

            response.getWriter().write(strDigitalData);

        } catch (Exception ex) {
            ex.printStackTrace();

            logger.error("Exception :" + ex);
        }
    }

    @RequestMapping(value = "/jsFileData", method = RequestMethod.GET)
    @ResponseBody
    public void jsFileload(HttpServletRequest request, HttpServletResponse response) {

        String addsting = null;
        String str = "" + request.getSession().getAttribute("GDPRCheck");
        logger.info("str" + str);
        if (str.equalsIgnoreCase("true")) {
            String url1, ubx, gaubx, cookieres;
            try {
                String appurl = applicationPropertiesUtil.getApplicationURL();

                String url = "https://www.google.com/recaptcha/api.js";
                String GAtoUBXEventMapper = appurl + "static/js/GAtoUBXEventMapper.js";
                String UBX = appurl + "static/js/UBX.js";
                String cookie = "https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js";

                url1 = loadJSFile(url, response);
                gaubx = loadJSFile(GAtoUBXEventMapper, response);
                ubx = loadJSFile(UBX, response);
                cookieres = loadJSFile(cookie, response);

            } catch (Exception ex) {
                System.out.println("error " + ex);
            }
        }

    }

    public String loadJSFile(String api, HttpServletResponse response) {
        String addsting = null;
        try {
            String inputLine = "";

            URL oracle = new URL(api);
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    yc.getInputStream()));

            while ((inputLine = in.readLine()) != null) {
//                System.out.println(inputLine);
                response.getWriter().write(inputLine);
                addsting = inputLine;
            }

            in.close();
        } catch (Exception ex) {
            System.out.println("error " + ex);
        }
        return addsting;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/home")
    public String homePage(HttpServletRequest request, ModelMap model) {
        return "redirect:/";
    }
    
    
    
//    public String  getEncValue(String strval)
//    {
//    	org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
//    obj.put("encVal", strval);
//    StringWriter out = new StringWriter();
//    String jsonText = out.toString();
//    EncryptDetail e = new EncryptDetail();
//    String encryptDataVar = e.encryptData(jsonText, strSoapURL1, soapActEnc);
//    org.json.JSONObject encryptData;
//    String encr_val = null;
//	try {
//		obj.writeJSONString(out);
//		encryptData = new org.json.JSONObject(encryptDataVar);
//	    encr_val = encryptData.getString("encVal");
//	} 
//	catch (IOException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
//	catch (JSONException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
//   
//    return encr_val;
//    }
    
    public String  getEncValue(String strval,String strSoapURL1,String soapActEnc)
    {
   	    ICICIEncryptDetail e = new ICICIEncryptDetail();
   	    String encryptDataVar = e.encryptData(strval, strSoapURL1, soapActEnc);
   	    return encryptDataVar;
    }

}
