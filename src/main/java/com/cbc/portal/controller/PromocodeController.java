/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.controller;

import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.utils.GsonUtil;
import com.cbc.portal.utils.PromocodeSerice;
import static com.mysql.jdbc.StringUtils.isNullOrEmpty;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Aravind E
 */
@Controller
public class PromocodeController {
    @Autowired
    CommonService commonService;
   
   @Value("${application.promoCodeService.url}")
    private String promoServiceUrl;
   
   @Value("${application.promoCodeService.sourceID}")
    private String pcSourceId;
   
   @Value("${application.promoCodeService.transactionID}")
    private String pcTransactionID;
   private Logger logger = Logger.getLogger(PromocodeController.class.getName());
   
    @RequestMapping(value = "/promoCode", method = RequestMethod.POST)
    @ResponseBody
    public String saveCallMeDetails(@RequestParam("voucherNo") String voucherNo) throws JSONException {
        
        
    
//        System.out.println("----------------->");
        PromocodeSerice promocodeSerice=new PromocodeSerice();
        String errorMsg="";
        String jpNumber="";
        String responseStatus="";
        String JsonRequest="";
        String JsonResponse="";
        String errormsg="";
        Map<String, String> map = new HashMap<String, String>();
         try{
        String response=promocodeSerice.getPromoCodeData(jpNumber,voucherNo,promoServiceUrl,pcSourceId,pcTransactionID);
         logger.info("response status "+response);
        JSONObject jSONObject=new JSONObject(response);
         responseStatus=jSONObject.getString("voucherStatus");
             logger.info("jSONObject  "+jSONObject);
         
//             System.out.println("ertrter dfgfdgf"+errormsg);
         
         JsonRequest=jSONObject.getString("request");
             logger.info("======="+jSONObject.getString("error"));
             
             String errorcode=jSONObject.getString("error");
             if(errorcode.length()>3){
              errorcode=errorcode.substring(1, errorcode.length()-1);
             }
             logger.info("chck"+errorcode.contains("-"));
             if(errorcode.contains("-") == true){
         errorcode = errorcode.substring(errorcode.indexOf("-") + 1).trim();
                logger.info("errorcode"+errorcode.trim());
            }
        logger.info("Status  : "+responseStatus);
        logger.info("JsonRequest  : "+JsonRequest);
        logger.info("JsonResponse  : "+JsonResponse);
        if(!responseStatus.equals("Unused") ||responseStatus != "Unused" ){
           errorMsg= commonService.getPromoCodeError(errorcode);
            logger.info("errorMsg"+errorMsg);
        
           if("null".equals(errorMsg) || errorMsg == "null"){
             logger.info("inside if conndtion");
          errorMsg=jSONObject.getString("error");
          errorMsg=errorMsg.substring(1,errorMsg.length()-1);
          
           }
        }
         }catch(Exception ex){
             logger.error("exception e"+ex);
         }
         
         logger.info("errorMsg out side  "+errorMsg);
  
        map.put("responseStatus", responseStatus);
        map.put("errorMsg", errorMsg);
        map.put("JsonRequest", JsonRequest);
        map.put("JsonResponse", JsonResponse);
        
        logger.info("json format  "+GsonUtil.toJson(map));
        
        return GsonUtil.toJson(map);
        
    }
    
    
    
    
}
