/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.auth0.NonceGenerator;
import com.auth0.NonceStorage;
import com.auth0.RequestNonceStorage;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.CityMappingBean;
import com.cbc.portal.beans.EnrollBean;
import com.cbc.portal.beans.IciciBean;
import com.cbc.portal.beans.IciciBean;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.service.CardDetailsService;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.service.IciciCardService;
import com.cbc.portal.service.MemberDetailsService;
import com.cbc.portal.service.MemberEnrollService;
import com.cbc.portal.service.RecommendCardService;
import com.cbc.portal.utils.ApplicationPropertiesUtil;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.GsonUtil;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

/**
 *
 * @author Aravind E
 */
@Controller
public class IciciWebViewController {
    
    
    private Logger logger = Logger.getLogger(IciciCardController.class.getName());
    private final NonceGenerator nonceGenerator = new NonceGenerator();

    @Autowired
    ApplicationPropertiesUtil applicationPropertiesUtil;

    @Autowired
    IciciCardService amexCardService;

    @Autowired
    CommonService commonService;

    @Autowired
    MemberEnrollService memberEnrollService;

    @Autowired
    MemberDetailsService memberDetailsService;

    @Autowired
    CardDetailsService cardDetailsService;

    @Autowired
    RecommendCardService recommendCardService;

    private static final String BEAN_NAME = "iciciBean";
    private static final String REDIRECT_URL = "redirect:/icici-apply-form/";
    private static final String ERROR_URL = "/jsp/portal/error.jsp";
    private static final String FLASH_CARDNAME = "cardName";
    private static final String ENROLL_BEAN = "enrollBean";
    private static final String JP_NUMBER = "jpNumber";
    private static final String RANDOM_NUMBER = "randomNumber";

    @RequestMapping("/icici-apply-webview-----form/{cardName}")
    public String applyICICIPage(@PathVariable("cardName") String cardName, HttpServletRequest request,
            ModelMap model, @ModelAttribute("errorMsg") String errorMsg,
            @ModelAttribute(BEAN_NAME) IciciBean iciciBean, @ModelAttribute(RANDOM_NUMBER) String randomNumber,
            @ModelAttribute(JP_NUMBER) String jpNum) {
           String spMailingID = "";
        String spUserID = "";
        String spJobID = "";
        String spReportId = "";
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        String termsAndCondition = "";
        String user =null;
        String cName = cardName.replaceAll("-", " ");
        String[] strArray = cName.split(" ");
        StringBuilder newCardName = new StringBuilder();
        for (String s : strArray) {
            if ("express".equalsIgnoreCase(s)) {
                s = s + "&reg;";
            }
            newCardName.append(s + " ");
        }
        try {
            spMailingID = CommonUtils.nullSafe(request.getParameter("spMailingID"), "");
            spUserID = CommonUtils.nullSafe(request.getParameter("spUserID"), "");
            spJobID = CommonUtils.nullSafe(request.getParameter("spJobID"), "");
            spReportId = CommonUtils.nullSafe(request.getParameter("spReportId"), "");

            utm_source = CommonUtils.nullSafe(request.getParameter("utm_source"), "");
            utm_medium = CommonUtils.nullSafe(request.getParameter("utm_medium"), "");
            utm_campaign = CommonUtils.nullSafe(request.getParameter("utm_campaign"), "");
            logger.info("IciciCardController ==> utm_source: "+utm_source+" utm_medium: "+utm_medium+" utm_campaign: "+utm_campaign);
            int cpNo = cardDetailsService.getCardIdByName(newCardName.toString().trim());
            if (0 != cpNo) {
                List<String> groupHeaders = null;
                iciciBean.setCpNo(cpNo);
                termsAndCondition = commonService.getTermdAndCondition(cpNo);
                groupHeaders = commonService.getGroupHeaders(1);
                CardDetailsBean cardDetailsBean = recommendCardService.getRecommendedCardById(cpNo);
                //--------new----
                if(!utm_source.isEmpty()){
					trackCode 	= "?utm_source="+utm_source+"_"+utm_medium+"_"+utm_campaign;
					iciciBean.setUtmSource(utm_source);
					iciciBean.setUtmMedium(utm_medium);
					iciciBean.setUtmCampaign(utm_campaign);
				}
                //---end-----------
                if (groupHeaders.size() > 3) {
                    groupHeaders.subList(3, groupHeaders.size()).clear();
                }
                model.addAttribute("groupHeadings", groupHeaders);
                model.addAttribute("applicationURL", applicationPropertiesUtil.getApplicationURL());
                //-----append trackcode----------
              
                request.getSession().setAttribute("redirectUrl", "icici-apply-form/"+cardName+trackCode);
				if(!spMailingID.isEmpty()){
					trackCode 	= "lw_"+spMailingID+"_"+spUserID+"_"+spJobID+"_"+spReportId;
				}
				if(!utm_source.isEmpty()){
					trackCode   =  "utm_"+utm_source+"_"+utm_medium+"_"+utm_campaign;
				}
//				request.getSession().setAttribute("trackCode", trackCode);
				request.getSession().setAttribute("homeUrl", "icici-apply-form/"+cardName);
				
//                if (request.getSession().getAttribute("loggedInUser") == null) {
//                    request.getSession().setAttribute("redirectUrl", "icici-apply-form/" + cardName+trackCode);
//                } else {
//                    request.getSession().setAttribute("homeUrl", "icici-apply-form/" + cardName);
//                }
                //--------add-trackode ---
                 request.getSession().setAttribute("trackCode", trackCode);
                
                String salariedincome = null, selfemployedincome = null;
                model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getAfTitle().replace("{card name}", cardDetailsBean.getImageText()));
                model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getAfDescription().replace("{card name}", cardDetailsBean.getImageText()));
                model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getAfKeyword());
                model.addAttribute("genderStatus", commonService.getEnumValues("Gender"));
                model.addAttribute("cities", commonService.getICICICities());
                model.addAttribute("states", commonService.getICICIStates());
                model.addAttribute("qualification", commonService.getAmexValues("Qualification"));
                model.addAttribute("pincode", commonService.getPincode());
                model.addAttribute("salaryStatus", commonService.getICICIValues("ICICI_EMP_TYPES"));
                model.addAttribute("ICICIBankRelationshipstatus", commonService.getICICIValues("ICICI_REALATION_STATUS"));
                model.addAttribute("ICICIBankRelationshipstatus1", commonService.getICICISelectedValues());
                model.addAttribute("salaryBankWithOtherBank", commonService.getICICIValues("SALARY_ACC_WITH_OTHER_BANK"));
                model.addAttribute("salaryAccountOpened", commonService.getICICIValues("SAL_ACC_OPENED"));
                model.addAttribute("channelType", commonService.getICICIValues("Channel_Type"));
                model.addAttribute("iciciITR", commonService.getICICIValues("ICICI_ITR_STATUS"));
                model.addAttribute(ENROLL_BEAN, new EnrollBean());
                model.addAttribute("termsAndCondition", termsAndCondition);
                                 model.addAttribute("SalariedIncome", commonService.getempstatusSalaried(2, cpNo));
                model.addAttribute("SelfEmpIncome", commonService.getempstatusSelfemp(2, cpNo));

                model.addAttribute("companynames", commonService.getCompanyNames());
                //cr 2
//                System.out.println("salaried age"+commonService.getDataForAgeRangeSalaried(String.valueOf(2), String.valueOf(cpNo)));
//                System.out.println("self age"+commonService.getDataForAgeRangeSelf(String.valueOf(2), String.valueOf(cpNo)));
                model.addAttribute("agebelowrange",commonService.getDataForAgeRangeSalaried(String.valueOf(2), String.valueOf(cpNo)));
                model.addAttribute("ageaboverange",commonService.getDataForAgeRangeSelf(String.valueOf(2), String.valueOf(cpNo)));
                //cr 2 end
                if (null != randomNumber && !randomNumber.isEmpty()) {
                    if (null != user && !user.isEmpty()) {
                        if (iciciBean.getJetpriviligemembershipNumber().isEmpty()) {
                            iciciBean.setJetpriviligemembershipNumber(CommonUtils.getJPNumber(user));
                            cardDetailsBean = amexCardService.getICICIOfferByJPNum(CommonUtils.getJPNumber(user), cardDetailsBean);
                        } else {
                            cardDetailsBean = amexCardService.getICICIOfferByJPNum(iciciBean.getJetpriviligemembershipNumber(), cardDetailsBean);
                        }
                    } else {
                        if (!iciciBean.getJetpriviligemembershipNumber().isEmpty()) {
                            cardDetailsBean = amexCardService.getICICIOfferByJPNum(iciciBean.getJetpriviligemembershipNumber(), cardDetailsBean);
                        } else {
                            cardDetailsBean = amexCardService.getICICIOfferByJPNum(jpNum, cardDetailsBean);
                        }
                    }
                    model.addAttribute(BEAN_NAME, iciciBean);
                } else {

                    if (user == null) {
                        NonceStorage nonceStorage = new RequestNonceStorage(request);
                        String nonce = nonceGenerator.generateNonce();
                        nonceStorage.setState(nonce);
                        model.addAttribute("state", nonce);
                        request.setAttribute("state", nonce);
                        if (!jpNum.isEmpty()) {
                            cardDetailsBean = amexCardService.getICICIOfferByJPNum(jpNum, cardDetailsBean);
                            iciciBean.setJetpriviligemembershipNumber(jpNum);
                            iciciBean.setJetpriviligemembershipTier(memberDetailsService.getTier(jpNum));
                        }
                        model.addAttribute(BEAN_NAME, iciciBean);
                    
                    }
//                    else {
//
//                        String jpNumber = getLoggedInUser(request);
//                        
//                        if (errorMsg.isEmpty()) {
//                            memberDetailsService.getMemberDetailsICICI(jpNumber, iciciBean);
//                        }
//
//                        model.addAttribute(BEAN_NAME, iciciBean);
//                        model.addAttribute("isLoginUser", "Yes");
//                    }
                }
                model.addAttribute("cardBean", cardDetailsBean);
                iciciBean.setOfferId(cardDetailsBean.getOfferId());
            } else {
                return ERROR_URL;
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardController applyiciciCardPage() :", e);
        }
        
        return "applyICICIWebViewForm";
    }
    



}
