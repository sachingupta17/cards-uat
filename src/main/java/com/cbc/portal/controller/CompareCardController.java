package com.cbc.portal.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.NonceGenerator;
import com.auth0.NonceStorage;
import com.auth0.RequestNonceStorage;
import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.EnrollBean;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.entity.CcCardFeatures;
import com.cbc.portal.entity.CcPageMaster;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.service.CompareCardService;
import com.cbc.portal.service.MemberDetailsService;
import com.cbc.portal.utils.ApplicationPropertiesUtil;
import com.cbc.portal.utils.CommonUtils;
import javax.servlet.http.Cookie;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class CompareCardController {
        
	private Logger logger = Logger.getLogger(CompareCardController.class.getName());
	private final NonceGenerator nonceGenerator = new NonceGenerator();
	private static final String ERROR_URL = "/jsp/portal/error.jsp";
	@Autowired
	CompareCardService compareCardService;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	MemberDetailsService memberDetailsService;
	
	@Autowired
	ApplicationPropertiesUtil applicationPropertiesUtil;
	
         String spendValue="";
	/**
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
//        private static final String BEAN_NAME = "cardDetailsBean";
	@RequestMapping(value="/compare-cards")
	public String compareCardsPage(HttpServletRequest request,ModelMap model){
		    String comapreCards = null;
                 String [] compareCardsArr;
		 String spend;
		 List<CardDetailsBean> cardsList=null;
		 Map<String, List <Map<String, List<CcCardFeatures>>>>  featureMap=new LinkedHashMap<>();
		 List<String> groupHeaders=null;
		 List<String> topHeader=new ArrayList<>();
		 CardDetailsBean cardDetailsBean = null;
		 AmexBean amexBean =new AmexBean();
                 CallMeBean callMeBean = new CallMeBean();
		 String trackCode = "";
		 String utm_source = "";
		 String utm_medium = "";
		 String utm_campaign = "";
		 String jpNumber=getLoggedInUser(request);
		 model.addAttribute("applicationURL", applicationPropertiesUtil.getApplicationURL());
                 
		 request.getSession().setAttribute("redirectUrl", "compare-cards");
                request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
            

		 try{
			utm_source   = CommonUtils.nullSafe(request.getParameter("utm_source"), "");
			utm_medium   = CommonUtils.nullSafe(request.getParameter("utm_medium"), "");
			utm_campaign = CommonUtils.nullSafe(request.getParameter("utm_campaign"), "");
			
			if(null!=utm_source && !utm_source.isEmpty()){
				trackCode   = "?utm_source="+utm_source+"_"+utm_medium+"_"+utm_campaign;
				amexBean.setUtmSource(utm_source);
				amexBean.setUtmMedium(utm_medium);
				amexBean.setUtmCampaign(utm_campaign);
			}
//		    request.getSession().setAttribute("redirectUrl", "home"+trackCode);
                        //--------new change---------
                    request.getSession().setAttribute("redirectUrl", "compare-cards/"+trackCode);
		    if(null!=utm_source && !utm_source.isEmpty()){
		    	trackCode   =  "utm_"+utm_source+"_"+utm_medium+"_"+utm_campaign;
		    }
                    request.getSession().setAttribute("homeUrl", "compare-cards/");
		    request.getSession().setAttribute("trackCode", trackCode);
//		    if(request.getSession().getAttribute("loggedInUser")==null){
//                        request.getSession().setAttribute("redirectUrl", "compare-cards");
//                    }else{
//                        request.getSession().setAttribute("homeUrl", "home");
//                    }
                    //-------------------------------------------
                    Cookie[] cookies = request.getCookies();

                        if (cookies != null) {
                         for (Cookie cookie : cookies) {                           
                           if (cookie.getName().equals("cardNo")) {
                               comapreCards = cookie.getValue().replaceAll("\\|", ",");
                            }
                          }
                        }
                        
                        compareCardsArr=new String[]{comapreCards};
                        
                        
			 spend=request.getParameter("spend");
			 String[] innerArray=compareCardsArr[0].split(",");
                        
                         if(spend!=null){
                            request.getSession().setAttribute("spentvalue", spend);
                            spendValue=(String) request.getSession().getAttribute("spentvalue");
                         }else if(spendValue!="" && spend==null){
                          spend=spendValue;
                          spendValue="";
                          }
                          //                sprint 56
                        List<CcPageMaster> pageMasterList = null;

                 String callMeFlag = "";
            pageMasterList = commonService.getPageMasterDetail(2);
            for(CcPageMaster flag:pageMasterList){
                logger.info("callme"+flag.getCallmeflag()+" promo"+flag.getPromocodeflag());
            callMeFlag=String.valueOf(flag.getCallmeflag());
           
            }
            logger.info("callMeFlag"+callMeFlag);
              
            model.addAttribute("callMeFlag",callMeFlag);
         
//            sprint 56 end
			 cardsList=compareCardService.compareCards(compareCardsArr);
			 featureMap=compareCardService.getFeatures(compareCardsArr);
			 groupHeaders=commonService.getGroupHeaders(2);
			 topHeader=groupHeaders.subList(0, 2);
			 model.addAttribute("spendValue", spend);
			 model.addAttribute("cardList", cardsList);
			 model.addAttribute("topHeader",topHeader);
			 model.addAttribute("topHeaderSize",topHeader.size());
			 model.addAttribute("cardsize",innerArray.length);
			 model.addAttribute("featureMap", featureMap);
			 model.addAttribute("enrollBean", new EnrollBean());
			 model.addAttribute("genderStatus",commonService.getEnumValues("Gender"));
			 model.addAttribute("cardDetailsBean", new CardDetailsBean());
			 
			model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getCcTitle());
			model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getCcDescription());
			model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getCcKeyword());
			model.addAttribute("utm_source", utm_source);
			model.addAttribute("utm_medium", utm_medium);
			model.addAttribute("utm_campaign", utm_campaign);
                        model.addAttribute("bpupgradeurl",commonService.getUpgrdURL());
                        model.addAttribute("pageinfo","Compare");
                model.addAttribute("bpupgrademessage",commonService.getUpgrdMessage());
//			CallMeBean callMeBean = new CallMeBean();
			 if(null!=jpNumber){  
                             logger.info("inside if condition");
					memberDetailsService.getMemberDetails(jpNumber, amexBean);
                                        callMeBean= memberDetailsService.getMemberDetailsCallmeFunction(jpNumber, callMeBean);
//                                         model.addAttribute("callMeBean", callMeBean);
		
                                        cardDetailsBean = new CardDetailsBean();
						cardDetailsBean.setTitle(amexBean.getTitle());
						cardDetailsBean.setFname(amexBean.getFname());
						cardDetailsBean.setMname(amexBean.getMname());
						cardDetailsBean.setLname(amexBean.getLname());
						cardDetailsBean.setMobile(amexBean.getMobile());
						cardDetailsBean.setEmail(amexBean.getEmail());
						cardDetailsBean.setDob(amexBean.getDateOfBirth());
						cardDetailsBean.setGender(amexBean.getGender());
                                                //----------new--------
                                        cardDetailsBean.setUtmMedium(utm_medium);
                                        cardDetailsBean.setUtmCampaign(utm_campaign);
                                        cardDetailsBean.setUtmSource(utm_source);
                                        //----------
						jpNumber = amexBean.getJetpriviligemembershipNumber();
						jpNumber = CommonUtils.getJPNumber(jpNumber);

						cardDetailsBean.setJpNumber(jpNumber);
						cardDetailsBean.setJpTier(amexBean.getJetpriviligemembershipTier());
						model.addAttribute("cardDetailsBean", cardDetailsBean);
			 }
			 else{
//                             model.addAttribute("callMeBean", new CallMeBean());
				 	NonceStorage nonceStorage = new RequestNonceStorage(request);
					String nonce = nonceGenerator.generateNonce();
					nonceStorage.setState(nonce);
					model.addAttribute("state", nonce);
					request.setAttribute("state", nonce);
			 }
			 			 model.addAttribute("callMeBean", callMeBean);
		 }catch(Exception e){
                                          model.addAttribute("callMeBean", callMeBean);
			 logger.error("@@@@ Exception in CardSearchController compareCardsPage() :",e);
		 return ERROR_URL;
                 }
		
		 return "comparecards";
	 }
	
	 @RequestMapping(value = "/getTopTableContent",method=RequestMethod.GET)
	 @ResponseBody
	 public  String getTopTableContent(@RequestParam("spend") String spend,@RequestParam("cpNoList") String cpNoList) {
		 Map<String, String> map=null;
		 try{
//                     System.out.println("spend at get"+spend);
			 map=compareCardService.getTopTableContent(spend,cpNoList);
		 }catch(Exception e){
			 logger.error("@@@@ Exception in CardSearchController calculateFreeFlightsByArray() :",e);
		 }
		 return new JSONObject(map).toString();
		  
	 }
	 
		public  String getLoggedInUser(HttpServletRequest request){
			return (String)request.getSession().getAttribute("loggedInUser");
		}
}
