/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.controller;

import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CreditScoreBean;
import com.cbc.portal.beans.EnrollBean;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.service.ExperianAPIService;
import com.cbc.portal.service.MemberDetailsService;
import com.cbc.portal.utils.ApplicationPropertiesUtil;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.EncryptDetail;
import com.cbc.portal.utils.GsonUtil;
import com.google.gson.Gson;
import javax.mail.Session;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
//import static org.springframework.http.HttpHeaders.USER_AGENT;


/**
 *
 * @author parvati
 */
@Controller
public class CheckCreditScoreController {

    @Autowired
    ExperianAPIService experianAPIService;
    @Autowired
    ApplicationPropertiesUtil applicationPropertiesUtil;
    @Autowired
    CommonService commonService;
    @Autowired
    MemberDetailsService memberDetailsService;
    
    @Value("${application.pii.encryption.strSoapURL1}")
   	String strSoapURL1;
   	@Value("${application.pii.encrypt.strSOAPAction1}")
   	String soapActEnc;
    
    private Logger logger = Logger.getLogger(CompareCardController.class.getName());
    private static final String JP_NUMBER = "jpNumber";
    private static final String BEAN_NAME = "creditScoreBean";

    @RequestMapping(value = "/credit-score-application-form ")
    public String experainForm(HttpServletRequest req, HttpServletResponse res, ModelMap model, @ModelAttribute("formNumber") String formNumber, @ModelAttribute(JP_NUMBER) String jpNum, @ModelAttribute(BEAN_NAME) CreditScoreBean creditScoreBean) {
//        CreditScoreBean creditScoreBean = new CreditScoreBean();
        EnrollBean enrollBean = new EnrollBean();

        try {
            
           
            
            
            String user = getLoggedInUser(req);
             logger.info("jpnumber is "+user);
            formNumber = RandomStringUtils.random(20, true, true);
            logger.info("====formNumber" + formNumber);
            logger.info("inside if condition "+user);
            logger.info("inside if condition "+user.length());
             if (null != user && !user.isEmpty() && user.length()>8) {
                 logger.info("inside if condition ");
                String jpNumber = getLoggedInUser(req);
                memberDetailsService.getMemberDetailsCreditScore(user, creditScoreBean);

            } else {
                model.addAttribute("creditScoreBean", new CreditScoreBean());

            }
            creditScoreBean.setFormNumber(formNumber);
            model.addAttribute("formNumber", formNumber);
            model.addAttribute("enrollBean", enrollBean);
            model.addAttribute("genderStatus", commonService.getEnumValues("Gender"));
           
           

            model.addAttribute("creditScoreBean", creditScoreBean);

        } catch (Exception e) {
            logger.error("Exception at experainForm()", e);
        }
        return "checkCreditScore";
    }

    @RequestMapping(value = "/credit-score-application-form ", method = RequestMethod.POST)
    public String experainFormSubmit(HttpServletRequest request, RedirectAttributes redirectAttribute,
            @ModelAttribute("creditScoreBean") CreditScoreBean creditScoreBean, ModelMap model) {
        String creditScore2 = "Unable to get your credit score from Experian. Please try again later.";
        String resp = "",maskEmail="";
        try {
        	
            logger.info("Controller jp number" + creditScoreBean.getJPnumber() + "===fname" + getEncValue(creditScoreBean.getFname().toString()));
            String jp = creditScoreBean.getJPnumber();

//            if (jp != null || jp.length()>7) {
            if (jp != null && jp.length()>7) {
                logger.info("if");
            } else {
                String jpnum = (String) request.getSession().getAttribute("loggedInUser");
                if (jpnum.startsWith("00") == true) {
                    jpnum = jpnum.substring(2);
                }
                logger.info("jpnum" + jpnum);
                creditScoreBean.setJPnumber(jpnum);
               
                resp = experianAPIService.checkExistedCreditReport(creditScoreBean);
            }
            logger.info("Controller jp number 2" + creditScoreBean.getJPnumber());

            
            JSONObject j = new JSONObject(resp);
            JSONObject jlog = new JSONObject(resp);
            jlog.put("creditScore", getEncValue(j.getString("creditScore")));
            jlog.put("emailId", getEncValue(j.getString("emailId")));
//            jlog.put("message", getEncValue(j.getString("message")));
//            jlog.put("date", getEncValue(j.getString("date")));
//            jlog.put("emailIdflag", getEncValue(j.getString("emailIdflag")));
            logger.info("controller response======>" + GsonUtil.toJson(jlog) + "=score=" + getEncValue(j.getString("creditScore")) + "=message=" + j.getString("message"));
             String emailId = j.getString("emailId");
            if (!emailId.isEmpty()) {
                String emailId1 = emailId.substring(0, 2);
                logger.info("1 " + emailId1);
                String email1 = emailId.substring(2, emailId.indexOf("@"));
                String emailId2 = wordReplace(email1);
                logger.info("2 " + emailId2);

                String emailId3 = emailId.substring((emailId.indexOf("@")), (emailId.indexOf("@") + 2));
                logger.info("3 " + emailId3);
                String email2 = emailId.substring(emailId.indexOf("@") + 2);
                String emailId4 = wordReplace(email2);
                logger.info("4 " + emailId4);
                logger.info("final masked id==>" + emailId1 + emailId2 + emailId3 + emailId4);
            maskEmail=emailId1 + emailId2 + emailId3 + emailId4;
            }else{
                maskEmail="";
            }
            HttpSession s = request.getSession();
            s.setAttribute("creditScore", j.getString("creditScore"));
            s.setAttribute("creditScoreError", j.getString("message"));
            s.setAttribute("maskEmail", maskEmail);
            s.setAttribute("date", j.getString("date"));
            s.setAttribute("emailIdflag",j.getString("emailIdflag"));

        } catch (Exception e) {
            logger.error("Exception at experainFormSubmit()", e);
//            model.addAttribute("creditScoreError", creditScore2);

            redirectAttribute.addAttribute("creditScoreError", creditScore2);
            return "redirect:/credit-score-results";

        }
        return "redirect:/credit-score-results";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/credit-score-results")
    public String serviceResponseIciciFail(HttpServletRequest request, ModelMap model) {
        request.getSession().setAttribute("appurl", applicationPropertiesUtil.getApplicationURL());
        model.addAttribute("title", "Co Brand Card - JetPrivilege");
        model.addAttribute("description", "");
        model.addAttribute("keyword", "");
        logger.info("Session creditScore==>" + getEncValue((String)request.getSession().getAttribute("creditScore")) + "==creditScoreError==>" + request.getSession().getAttribute("creditScoreError"));
        model.addAttribute("creditScore", CommonUtils.nullSafe((String) request.getSession().getAttribute("creditScore"), ""));
        model.addAttribute("creditScoreError", CommonUtils.nullSafe((String) request.getSession().getAttribute("creditScoreError"), ""));
        model.addAttribute("emailId", CommonUtils.nullSafe((String) request.getSession().getAttribute("maskEmail"), ""));
        model.addAttribute("emailIdflag", CommonUtils.nullSafe((String) request.getSession().getAttribute("emailIdflag"), ""));
        
        
        return "creditSuccess";

    }

    public String getLoggedInUser(HttpServletRequest request) {
        return (String) request.getSession().getAttribute("loggedInUser");

    }

    public String wordReplace(String orgword) {
        char org[] = orgword.toCharArray();
        StringBuilder replaceWord = new StringBuilder();
        for (int i = 0; i < org.length; i++) {
            String s = Character.toString(org[i]);

            String repLetter = s.replace(s, "x");
            replaceWord.append(repLetter);
        }

        System.out.println("bd" + replaceWord);
        return replaceWord.toString();
    }
    
    
    

    
    public String getEncValue(String strVal)
    {
	    org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
	    obj.put("userjson", strVal);
	    StringWriter out = new StringWriter();
	    String encVal = null;
	    try {
			obj.writeJSONString(out);
		    String jsonText = out.toString();
		    EncryptDetail enc = new EncryptDetail();
		    String encryptDataVar = enc.encryptData(jsonText,strSoapURL1,soapActEnc);
		    org.json.JSONObject encryptData;
		    encryptData = new org.json.JSONObject(encryptDataVar);
		    encVal=""+encryptData.getString("userjson");
		
	    } catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    return encVal;
    }
    
    
}
