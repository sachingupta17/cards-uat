package com.cbc.portal.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.NonceGenerator;
import com.auth0.NonceStorage;
import com.auth0.RequestNonceStorage;
import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.EnrollBean;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.entity.CcPageMaster;
import com.cbc.portal.service.CardDetailsService;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.service.MemberDetailsService;
import com.cbc.portal.utils.ApplicationPropertiesUtil;
import com.cbc.portal.utils.CommonUtils;

@Controller
public class CardDetailsController {

    private Logger logger = Logger.getLogger(CardDetailsController.class.getName());
    private final NonceGenerator nonceGenerator = new NonceGenerator();

    @Autowired
    CardDetailsService cardDetailsService;

    @Autowired
    CommonService commonService;

    @Autowired
    MemberDetailsService memberDetailsService;

    @Autowired
    ApplicationPropertiesUtil applicationPropertiesUtil;

    private static final String CARDNAME = "{card name}";
    private static final String ERROR_URL = "/jsp/portal/error.jsp";	
    /**
     *
     * @param cardName
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/card-details/{cardName}")
    public String cardDetailsbyIdPage(@PathVariable String cardName, ModelMap model, HttpServletRequest request) {

        CardDetailsBean bean = null;
        List<String> groupHeaders = null;
        CardDetailsBean cardDetailsBean = null;
        AmexBean amexBean = new AmexBean();
        String trackCode = "";
        String utm_source = "";
        String utm_medium = "";
        String utm_campaign = "";
        int cpNo=0;	
        String jpNumber = getLoggedInUser(request);
        model.addAttribute("applicationURL", applicationPropertiesUtil.getApplicationURL());
        request.getSession().setAttribute("redirectUrl", "card-details/" + cardName);
        try {
            String cName = cardName.replaceAll("-", " ");
            
            logger.info("cName in cardsdetails Controller>>>>"+cName);
            
            String[] strArray = cName.split(" ");
            StringBuilder newCardName = new StringBuilder();
            for (String s : strArray) {
                if ("express".equalsIgnoreCase(s)) {
                    s = s + "&reg;";
                }
                newCardName.append(s + " ");
            }
            System.out.println("newCardName.toString().trim()>>>>>"+newCardName.toString().trim());
             cpNo = cardDetailsService.getCardIdByName(newCardName.toString().trim());
            utm_source = CommonUtils.nullSafe(request.getParameter("utm_source"), "");
            utm_medium = CommonUtils.nullSafe(request.getParameter("utm_medium"), "");
            utm_campaign = CommonUtils.nullSafe(request.getParameter("utm_campaign"), "");

            model.addAttribute("applicationURL", applicationPropertiesUtil.getApplicationURL());
            if (null != utm_source && !utm_source.isEmpty()) {
                trackCode = "?utm_source=" + utm_source + "_" + utm_medium + "_" + utm_campaign;
                amexBean.setUtmSource(utm_source);
                amexBean.setUtmMedium(utm_medium);
                amexBean.setUtmCampaign(utm_campaign);
            }
            //---------new thing to test-----
            request.getSession().setAttribute("redirectUrl", "card-details/" + cardName + trackCode);

            if (null != utm_source && !utm_source.isEmpty()) {
                trackCode = "utm_" + utm_source + "_" + utm_medium + "_" + utm_campaign;
            }
            request.getSession().setAttribute("homeUrl", "card-details/" + cardName);
//		    if(request.getSession().getAttribute("loggedInUser")==null){

//                        request.getSession().setAttribute("redirectUrl", "card-details/"+cardName+trackCode);
//                    }else{
////                         request.getSession().setAttribute("redirectUrl", "card-details/"+cardName+trackCode);
//                        request.getSession().setAttribute("homeUrl", "home"+trackCode);
//                    }

                     request.getSession().setAttribute("trackCode", trackCode);
		    //----------------
			groupHeaders=commonService.getGroupHeaders(3);
			bean=cardDetailsService.getCardDetailsById(cpNo);
		//                sprint 56
                        List<CcPageMaster> pageMasterList = null;

                 String callMeFlag = "";
            pageMasterList = commonService.getPageMasterDetail(3);
            for(CcPageMaster flag:pageMasterList){
                logger.info("callme"+flag.getCallmeflag()+" promo"+flag.getPromocodeflag());
            callMeFlag=String.valueOf(flag.getCallmeflag());
           
            }
            logger.info("callMeFlag "+callMeFlag);
              
            model.addAttribute("callMeFlag",callMeFlag);
         
//            sprint 56 end

		model.addAttribute("bean", bean);
		model.addAttribute(CcPortalConstants.TITLE, applicationPropertiesUtil.getCdTitle().replace(CARDNAME, bean.getImageText()));
		model.addAttribute(CcPortalConstants.DESCRIPTION, applicationPropertiesUtil.getCdDescription().replace(CARDNAME, bean.getImageText()));
		model.addAttribute(CcPortalConstants.KEYWORD, applicationPropertiesUtil.getCdKeyword().replace(CARDNAME, bean.getImageText()));
		model.addAttribute("groupHeader",groupHeaders);
		model.addAttribute("cardFeature",bean.getCardFeature());
	    model.addAttribute("headersize",groupHeaders.size());
	    model.addAttribute("enrollBean", new EnrollBean());
		model.addAttribute("genderStatus",commonService.getEnumValues("Gender"));
		model.addAttribute("utm_source", utm_source);
		model.addAttribute("utm_medium", utm_medium);
		model.addAttribute("utm_campaign", utm_campaign);
		model.addAttribute("bpupgradeurl",commonService.getUpgrdURL());
                model.addAttribute("bpupgrademessage",commonService.getUpgrdMessage());
                 model.addAttribute("pageinfo","Detail");
		  CallMeBean callMeBean = new CallMeBean();
		 if(null!=jpNumber){
                              
				memberDetailsService.getMemberDetails(jpNumber, amexBean);
				callMeBean= memberDetailsService.getMemberDetailsCallmeFunction(jpNumber, callMeBean);
                                 model.addAttribute("callMeBean", callMeBean);
					cardDetailsBean = new CardDetailsBean();
					cardDetailsBean.setTitle(amexBean.getTitle());
					cardDetailsBean.setFname(amexBean.getFname());
					cardDetailsBean.setMname(amexBean.getMname());
					cardDetailsBean.setLname(amexBean.getLname());
					cardDetailsBean.setMobile(amexBean.getMobile());
					cardDetailsBean.setEmail(amexBean.getEmail());
					cardDetailsBean.setDob(amexBean.getDateOfBirth());
					cardDetailsBean.setGender(amexBean.getGender());
                                        //----------new--------
                                        cardDetailsBean.setUtmMedium(utm_medium);
                                        cardDetailsBean.setUtmCampaign(utm_campaign);
                                        cardDetailsBean.setUtmSource(utm_source);
                                        //----------
					jpNumber = amexBean.getJetpriviligemembershipNumber();
					jpNumber = CommonUtils.getJPNumber(jpNumber);
					cardDetailsBean.setJpNumber(jpNumber);
					cardDetailsBean.setJpTier(amexBean.getJetpriviligemembershipTier());
					model.addAttribute("cardDetailsBean", cardDetailsBean);
		 }
		 else{
			 model.addAttribute("cardDetailsBean", new CardDetailsBean());
                         model.addAttribute("callMeBean", new CallMeBean());
			 	NonceStorage nonceStorage = new RequestNonceStorage(request);
				String nonce = nonceGenerator.generateNonce();
				nonceStorage.setState(nonce);
				model.addAttribute("state", nonce);
				request.setAttribute("state", nonce);
		 }
		}catch(Exception e){
			logger.error("@@@@ Exception in CardDetailsController getCardDetailsPage() :",e);
		}
         if(cpNo==0){
                    logger.info("===inside if loop==== ");
//                    return "redirect:/";
                     return ERROR_URL;
                }else{
		return "cardDetails";
                 }
//		return "cardDetails";
	}
	@RequestMapping(value="/getBenefitContent")
	@ResponseBody
	public String getBenefitContent(ModelMap model, HttpServletRequest request){
		String benefitContent = "";
		try{
			int cpNo = 0 ;
			int gmNo = 0 ;
			logger.info("request params cpNo= "+request.getParameter("cpNo")+" gmNo= "+request.getParameter("gmNo"));
                        System.out.println("request params cpNo= "+request.getParameter("cpNo")+" gmNo= "+request.getParameter("gmNo"));
			String reqCpNo  = request.getParameter("cpNo");
			String reqGmNo  = request.getParameter("gmNo");
			if(null!=reqCpNo && !reqCpNo.isEmpty())
				cpNo = Integer.parseInt(reqCpNo);
			if(null!=reqGmNo && !reqGmNo.isEmpty())
				gmNo = Integer.parseInt(reqGmNo);
			benefitContent = cardDetailsService.getBenefitContent(cpNo,gmNo);
                        System.out.println("benefitContent>>"+benefitContent);
		}catch(Exception e){
			logger.error("error in getBenefitContent :",e);
		}
		return benefitContent;
	}
	
	public  String getLoggedInUser(HttpServletRequest request){
		return (String)request.getSession().getAttribute("loggedInUser");
	}

    

         //new cr mileston 2
        @RequestMapping(value="/getJoiningBenefitContent")
	@ResponseBody
	public String getJoiningBenefitContent(ModelMap model, HttpServletRequest request){
		String benefitContent = "";
		try{
			int cpNo = 0 ;
			int gmNo = 0 ;
			logger.info("request params cpNo= "+request.getParameter("cpNo")+" gmNo= "+request.getParameter("gmNo"));
			String reqCpNo  = request.getParameter("cpNo");
			String reqGmNo  = request.getParameter("gmNo");
			if(null!=reqCpNo && !reqCpNo.isEmpty())
				cpNo = Integer.parseInt(reqCpNo);
			if(null!=reqGmNo && !reqGmNo.isEmpty())
				gmNo = Integer.parseInt(reqGmNo);
//                         System.out.println("gmNo in joining"+gmNo+"");
			benefitContent = cardDetailsService.getJoiningBenefitContent(cpNo);
		}catch(Exception e){
			logger.error("error in getBenefitContent :",e);
		}
		return benefitContent;
	}
        //end cr mileston 2
}
