package com.cbc.portal.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cbc.portal.service.CardSearchService;

@Controller
public class CardSearchController {

	@Autowired
	CardSearchService cardSearchService;
	
	private Logger logger = Logger.getLogger(CardSearchController.class.getName());
	
	 @RequestMapping(value = "/calcfreeflights",method=RequestMethod.GET)
	 @ResponseBody
	 public Map<Integer, String> calculateFreeFlightsByArray(@RequestParam("spend") String spend,@RequestParam("cpNoList") String cpNoList) {
		 Map<Integer, String> map=null;
		 try{
			 map=cardSearchService.calculateFreeFlightsByArray(spend,cpNoList);
		 }catch(Exception e){
			 logger.error("@@@@ Exception in CardSearchController calculateFreeFlightsByArray() :",e);
		 }
		 return map;
	 }
	 	
}
