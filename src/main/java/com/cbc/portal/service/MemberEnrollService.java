package com.cbc.portal.service;

import javax.servlet.http.HttpServletRequest;

import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.EnrollBean;
import java.util.Map;

public interface MemberEnrollService {
	
	public Map memberEnrollment(EnrollBean enrollBean, HttpServletRequest request);
}
