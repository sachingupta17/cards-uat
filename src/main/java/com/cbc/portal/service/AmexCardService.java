package com.cbc.portal.service;

import org.json.JSONObject;

import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.beans.CardDetailsBean;

public interface AmexCardService {

	public String applyAmexCardWebService(AmexBean bean,String userTier);

	public CardDetailsBean getDefaultOfffer(CardDetailsBean cardDetailsBean);
	
	public CardDetailsBean getOfferByJPNum(String jpNumber, CardDetailsBean cardDetailsBean);

	public JSONObject getOfferByJPNumber(String jpnumber, String bpNo);

	public AmexBean getBEAmexFormDetails(String randomNumber, AmexBean amexBean);

	public String saveCallMeDetails(CallMeBean callMeBean);

	public void saveOrUpdateFormData(AmexBean amexBean, int param);

	public void FormInactiveSilverPopApi(AmexBean amexBean);
        
        public String getFormNumber(String formNumber);
}
