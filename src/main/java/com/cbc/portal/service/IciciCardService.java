/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.service;

import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.IciciBean;
import com.cbc.portal.entity.CcBeApplicationFormDetails;

import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author Aravind E
 */
public interface IciciCardService {

//    public String applyIciciCardWebService(IciciBean iciciBean);
    public Map<String, String> applyIciciCardWebService(IciciBean iciciBean, String strSoapURL1, String soapActEnc);
    

    public CardDetailsBean getICICIDefaultOfffer(CardDetailsBean cardDetailsBean);

    public CardDetailsBean getICICIOfferByJPNum(String jpNumber, CardDetailsBean cardDetailsBean);

    public JSONObject getICICIOfferByJPNumber(String jpnumber, String bpNo);
public void saveOrUpdateFormData(IciciBean iciciBean, int param);
public IciciBean getBEIciciFormDetails(String randomNumber, IciciBean iciciBean) ;
    public String getFormNumber(String formNumber);
        public void FormInactiveSilverPopApiIcici(IciciBean iciciBean);
        
        public CcBeApplicationFormDetails fetchPostSignupMemberDtls(String formNo, int param);

}

