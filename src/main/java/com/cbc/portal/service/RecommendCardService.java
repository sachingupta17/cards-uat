package com.cbc.portal.service;

import java.util.List;

import com.cbc.portal.beans.CardDetailsBean;

public interface RecommendCardService {

	List<CardDetailsBean> getRecommndedCards(CardDetailsBean cardDetailsBean);

	CardDetailsBean getRecommendedCardById(int cpNo);
}
