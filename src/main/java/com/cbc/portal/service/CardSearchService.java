package com.cbc.portal.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.cbc.portal.beans.CardDetailsBean;

public interface CardSearchService {
     
    List<CardDetailsBean> getBankList();

    List<CardDetailsBean> getPreferenceList();

    List<CardDetailsBean> getAllCardList();
    
//    List<CardDetailsBean> getAllTypeCardList();
    
    List<CardDetailsBean> getAllTypeCardList();
     List<CardDetailsBean> getSelectedBankAllCardList();

    List<CardDetailsBean> getCardListByFees(BigDecimal fees);

    List<CardDetailsBean> getCardListByLyfPref(Integer lsbpNo);

    Map<Integer, String> calculateFreeFlightsByArray(String spend, String cpNo);

    List<String> getCity();
   
         List<CardDetailsBean> getSelectedDebitCardList(Integer Countryid);
        
        List<CardDetailsBean> getSelectedCorporateCardList(Integer Countryid);
        
        List<CardDetailsBean> getbanklistcreditindia(Integer categoryid, Integer countryid ) ;
        
         List<CardDetailsBean> getbanklistdebit(Integer categoryid) ;
         
          List<CardDetailsBean> getSelectedOtherCardList(Integer Countryid);
}
