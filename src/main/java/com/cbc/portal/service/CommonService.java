package com.cbc.portal.service;

import java.util.List;
import java.util.Map;

import com.cbc.portal.beans.BannerBean;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.CityMappingBean;
import com.cbc.portal.beans.JsonBean;
import com.cbc.portal.entity.CcPageMaster;

public interface CommonService {

    public Map<Byte, String> getEnumValues(String string);

    public Map<String, String> getAmexValues(String string);

    public CityMappingBean getCityMapping(String pin);

    public CityMappingBean getICICICityMapping(String pin);

    public JsonBean getDetailsforPCN(String pcnNumber);

    public List<JsonBean> getDetailsforJpNumber(String jpNumber);

    public List<JsonBean> getICICIDetailsforJpNumber(String jpNumber);

    String getAmexEnumValues(String typeName, String val);

    List<String> getGroupHeaders(int id);

    public List<String> getCities();

    public List<String> getStates();

    public List<String> getICICICities();

    public List<String> getICICIStates();

//    public BannerBean getActiveBanner();
    
    public List<BannerBean> getActiveBanner();

    public String addUserInformation(CardDetailsBean cardDetailsBean);

    public Map<String, String> getCityNames(String cityName);

    public Boolean checkExcludePincode(String pinCode, String cpNo);

    public Boolean checkICICIExcludePincode(String pinCode, String cpNo);

    public Map<String, String> getCitiesByState(String state);

    public String getStateByCity(String city);

    public Map<String, String> getICICICitiesByState(String state);

    public String getICICIStateByCity(String city);

    public int getTimer(String timerName);

    public String getTermdAndCondition(int cpNo);

    public List<String> getPincode();

    public Map<String, String> getICICIValues(String string);

    public String getUpgrdMessage();

    public String getUpgrdURL();

    public String getAmexAppStatus(String jetpriviligemembershipNumber);

    public String getRandomNumber(String jetpriviligemembershipNumber);

    public List<String> getICICISelectedValues();

    public List<String> getCardType();

    public List<String> getCountry();

    public List<String> getCompanyNames();

    public List<String> getempstatusSalaried(Integer bpno, Integer cpno);

    public List<String> getempstatusSelfemp(Integer bpno, Integer cpno);

    public List getagecriteriaAgeabove(String bpNo, String cpNo, String EmpType, String City);

    public List getagecriteriaAgebelow(String bpNo, String cpNo, String EmpType, String City);

    public List<String> getCompanyNamesByLetter(String companyName);

    public Map<String, String> getAllCities();

    //new cr2
    public List<CardDetailsBean> getBankwiseRsponseByStatus(String BpStatus, Integer bpNo);

    public int getBankStatusIdByName(String bsStatus);
    //new cr2 milestone 2

    public int getDataForAgeRangeSalaried(String bpNo, String cpNo);

    public int getDataForAgeRangeSelf(String bpNo, String cpNo);
//cr3

    public void buildSMSForApplication(String pcnNumber, String campaignId, String emailId, String firstName, String lastName, String bankNumber, String smsName, String cardName, String days, String mobileNo, String applicantName, String applicationNo, String jpNumber, String initdate, String bankdetail, String appName);

    public String getOtpForValidate(String mobileNo, String bankNumber, String formNumber, String otpTransactionId);

    public String UpdateApplicationForOtp(String mobileNo, String bankNumber, String formNumber, String otp, String applicationId);

    public String getPromoCodeError(String errorCode);

    public String getEnrollCodeError(String errorCode);

    public List<CcPageMaster> getPageMasterDetail(int id);

    public String checkCardVarientRandomNo(Integer cpNo, String randomNumber);

    public String checkFormStatus(String randomNumber);
}
