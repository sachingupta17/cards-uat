package com.cbc.portal.service;

import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.beans.CreditScoreBean;
import com.cbc.portal.beans.IciciBean;

public interface MemberDetailsService {

    public void getMemberDetails(String jpNumber, AmexBean amexBean);

    public void getMemberDetailsICICI(String jpNumber, IciciBean iciciBean);

    String getTier(String jpNumber);

    public CallMeBean getMemberDetailsCallmeFunction(String jpNumber, CallMeBean callMeBean);

    public void getMemberDetailsCreditScore(String jpNumber, CreditScoreBean creditScoreBean);
    
    String getJPTier(String jpNumber);

}
