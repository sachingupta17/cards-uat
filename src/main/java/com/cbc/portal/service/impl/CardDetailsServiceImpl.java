package com.cbc.portal.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cbc.portal.DAO.CardProductDAO;
import com.cbc.portal.DAO.CardSearchDAO;
import com.cbc.portal.DAO.CommonDAO;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.entity.CcCardFeatures;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcPageGroupLink;
import com.cbc.portal.service.CardDetailsService;
import com.cbc.portal.utils.CommonUtils;

@Component
@Transactional(readOnly=true)
public class CardDetailsServiceImpl implements CardDetailsService {
	
	private Logger logger = Logger.getLogger(CardDetailsServiceImpl.class.getName());
	
	@Autowired
	CardProductDAO cardProductDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	CardSearchDAO cardSearchDAO;
	
	private static final String REG_EXP = "\\r|\\n";

	@Override
	public CardDetailsBean getCardDetailsById(Integer cpNo) {
		CardDetailsBean cardDetailsBean=null;
		CcCardProduct ccBean=null;
		List<CcPageGroupLink> ccPageGroupLink= new ArrayList<>();
		List<String> content=new ArrayList<>();
		List<Integer> arr=new ArrayList<>();
		List<CcCardFeatures> ccFeatureList;
		String cardUrl = "";
		String bankUrl = "";
		try{
			ccBean=cardProductDAO.getCardProductById(cpNo);
			 ccPageGroupLink =commonDAO.getGroupHeaders(3);
			
			if(null!=ccBean){
				cardDetailsBean = new CardDetailsBean();
				cardDetailsBean.setCpNo(ccBean.getCpNo());
				cardDetailsBean.setBpNo(ccBean.getCcBankPartner().getBpNo());
				cardDetailsBean.setCardName(CommonUtils.nullSafe(ccBean.getCardName()));
				cardDetailsBean.setBankName(CommonUtils.nullSafe(ccBean.getCcBankPartner().getBankName()));
				cardDetailsBean.setNetworkName(ccBean.getCcNetworkList().getNetworkName());
				bankUrl=CommonUtils.nullSafe(ccBean.getCcBankPartner().getBpUrl());
				bankUrl=bankUrl.contains("http")?bankUrl.replaceFirst("^(http://www\\.|http://|www\\.|https://www\\.)","//"):"//"+bankUrl;
				cardDetailsBean.setBankUrl(bankUrl);
				cardDetailsBean.setBankUrlName(CommonUtils.nullSafe(ccBean.getCcBankPartner().getBpUrl()));
				cardDetailsBean.setCardImagePath(ccBean.getCpImagePath());
				cardDetailsBean.setImageText(CommonUtils.nullSafe(ccBean.getCpImageText()));
				cardDetailsBean.setCardInfo(CommonUtils.nullSafe(ccBean.getCardInfo()));
				cardUrl=CommonUtils.nullSafe(ccBean.getCardUrl());
				cardDetailsBean.setCardUrl(cardUrl);
				cardDetailsBean.setAdditionalCardInfo(CommonUtils.nullSafe(ccBean.getAdditionalCardInformation()).replaceAll(REG_EXP, "").trim());
				cardDetailsBean.setTermsAndCondition(CommonUtils.nullSafe(ccBean.getTermsAndCondition()).replaceAll(REG_EXP, "").trim());
				cardDetailsBean.setPartnerInfo(CommonUtils.nullSafe(ccBean.getCcBankPartner().getBpInformation()).replaceAll(REG_EXP, "").trim());
				cardDetailsBean.setJpMiles(CommonUtils.checkForDecimal(ccBean.getJpMiles()));
				cardDetailsBean.setPerspendonJp(CommonUtils.checkForDecimal(ccBean.getPerSpendsOnJpMiles()));
				cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccBean.getFees()));
				cardDetailsBean.setLsbpList(cardSearchDAO.getlsbpArray(ccBean.getCpNo()));
				cardDetailsBean.setMilesDisplayMessage(CommonUtils.nullSafe(ccBean.getMilesDisplayMessage()).replaceAll(REG_EXP, "").trim());
                                cardDetailsBean.setApplyflag(ccBean.getApplyFlag());
			
				if(null!=ccPageGroupLink){
				 for(CcPageGroupLink ccpage:ccPageGroupLink){
					 arr.add(ccpage.getCcGroupMaster().getGmNo());
					 
				 }
				} 	Integer [] groupNumber = arr.toArray(new Integer[arr.size()]);
				   for(int i=0;i<groupNumber.length;i++){
						ccFeatureList = new ArrayList<>();
						ccFeatureList = cardProductDAO.getCardFeatureforDetails(groupNumber[i],cpNo);
						if(null!=ccFeatureList){
							int count=0;
							for(CcCardFeatures cfList:ccFeatureList){
								if(count==0){
									content.add("<div class="+"sub-heading "+"margT2> <h2>"+cfList.getCcGroupMaster().getGroupDescription()+"</h2></div>");
								 }
								 content.add(cfList.getCfContent());
								 count++;
								 }
							 }
							 else
								 continue;
				   }
				   cardDetailsBean.setCardFeature(content);
			}
		}catch(Exception e){
			logger.error("@@@@ Exception in CardDetailsServiceImpl getCardDetailsById() :",e);	
		}
		return cardDetailsBean;
	}

	@Override
	public String getBenefitContent(int cpNo, int gmNo) {
		String benefitContent = "";
                String benefitIntermiles = "";
                 List<CcPageGroupLink> ccPageGroupLink = new ArrayList<>();
                 List<Integer> column = new ArrayList<>();
                 int gmN = 0,gmNumber = 0;
                 StringBuffer s1 = new StringBuffer("class='features_mobview'");
                 
		try{
                        ccPageGroupLink = commonDAO.getGroupHeaders(1);
                        
                        if (null != ccPageGroupLink) {

                for (CcPageGroupLink ccpage : ccPageGroupLink) {
                    column.add(ccpage.getCcGroupMaster().getGmNo());
                    if ("Benefits".equalsIgnoreCase(ccpage.getCcGroupMaster().getGroupName())) {
                        gmN = ccpage.getCcGroupMaster().getGmNo();
                        System.out.println("gmN>>>"+gmN);
                        
                        benefitContent = cardProductDAO.getBenefitContent(cpNo, gmN);
                        benefitContent = "<p class='Asterik' style='font-weight: bold;'>Airline Benefits - Eithad Airways*</p>".concat(benefitContent);
                        System.out.println("benefitContent>>>"+benefitContent);
//                        StringBuffer s2 = new StringBuffer(benefitContent);
//                        int startIndex = s2.indexOf(s1.toString());
//                        System.out.println("s2 length>>>"+s2.length());
//                        int endIndex = s2.length();                        
//                        System.out.println("Containse>>>>"+startIndex);
//                        s2.replace(startIndex, endIndex, s1.toString());
//                        s2 = benefitContent;
//                        System.out.println("Final string in s2 ----------> "+s2.toString());
                    }
                    
                    if ("InterMiles Benefit on spends".equalsIgnoreCase(ccpage.getCcGroupMaster().getGroupName())) {
                        gmNumber = ccpage.getCcGroupMaster().getGmNo();
                        System.out.println("gmNumber>>>"+gmNumber);
                        benefitIntermiles = cardProductDAO.getBenefitContent(cpNo, gmNumber);
                        benefitIntermiles = "<p class='Asterik' style='font-weight: bold;'>InterMiles Benefit on spends*</p>".concat(benefitIntermiles);
                        System.out.println("benefitIntermiles>>>>"+benefitIntermiles);
                    }

                }
            }
                        
//			benefitContent = cardProductDAO.getBenefitContent(cpNo, gmNo);
                      benefitContent = benefitIntermiles.concat(benefitContent);
                      System.out.println("benefitContent>>>"+benefitContent);

		}catch(Exception e){
			logger.error("@@@@ Exception in CardDetailsServiceImpl getBenefitContent() :",e);	
		}
		return benefitContent;
	}
@Override
	public String getJoiningBenefitContent(int cpNo) {
		String benefitContent = "";
                 List<CcPageGroupLink> ccPageGroupLink = new ArrayList<>();
                   List<Integer> column = new ArrayList<>();
                    int gmNo = 0;
		try{
                     ccPageGroupLink = commonDAO.getGroupHeaders(1);
                       if (null != ccPageGroupLink) {

                for (CcPageGroupLink ccpage : ccPageGroupLink) {
                    column.add(ccpage.getCcGroupMaster().getGmNo());
                    if ("Joining Benefit for Listing".equalsIgnoreCase(ccpage.getCcGroupMaster().getGroupName())) {
                        gmNo = ccpage.getCcGroupMaster().getGmNo();
                    }

                }
            }
//                       System.out.println("gmNo======="+gmNo);
			benefitContent = cardProductDAO.getBenefitContent(cpNo, gmNo);
		}catch(Exception e){
			logger.error("@@@@ Exception in CardDetailsServiceImpl getBenefitContent() :",e);	
		}
		return benefitContent;
	}

	@Override
	public int getCardIdByName(String cardName) {
		int cpNo = 0;
		try{
			cpNo = cardProductDAO.getCardIdByName(cardName);
		}catch(Exception e){
			logger.error("@@@@ Exception in CardDetailsServiceImpl getCardIdByName() :",e);	
		}
		return cpNo;
	}

}
