package com.cbc.portal.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cbc.portal.DAO.CardProductDAO;
import com.cbc.portal.DAO.CardSearchDAO;
import com.cbc.portal.DAO.CommonDAO;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.entity.CcBankPartner;
import com.cbc.portal.entity.CcCardFeatures;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcLifestyleBenefitPref;
import com.cbc.portal.entity.CcLifestyleBenefitPrefValues;
import com.cbc.portal.entity.CcPageContent;
import com.cbc.portal.entity.CcPageGroupLink;
import com.cbc.portal.service.CardSearchService;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.MilesCalculationUtil;

/**
 *
 * @author abhishek.m
 *
 */
@Component
@Transactional(readOnly = true)
public class CardSearchServiceImpl implements CardSearchService {

    private Logger logger = Logger.getLogger(CardSearchServiceImpl.class.getName());

    @Autowired
    CardSearchDAO cardSearchDAO;

    @Autowired
    CardProductDAO cardProductDAO;

    @Autowired
    CommonDAO commonDAO;

    @Override
    public List<CardDetailsBean> getBankList() {
        List<CardDetailsBean> bankList = new ArrayList<>();
        try {
            List<CcBankPartner> ccBankPartnerList;
            CardDetailsBean cardDetailsBean;
            ccBankPartnerList = cardSearchDAO.getBankList();
            if (!ccBankPartnerList.isEmpty()) {
                for (CcBankPartner ccBankPartner : ccBankPartnerList) {
                    cardDetailsBean = new CardDetailsBean();
                    cardDetailsBean.setBankName(ccBankPartner.getBankName());
                    cardDetailsBean.setBpNo(ccBankPartner.getBpNo());
                    cardDetailsBean.setBankImagePath(ccBankPartner.getBpImagePath());
                    bankList.add(cardDetailsBean);
                }
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getBankList() :", e);
        }

        return bankList;
    }

    @Override
    public List<CardDetailsBean> getPreferenceList() {
        List<CardDetailsBean> prefList = new ArrayList<>();
        try {
            List<CcLifestyleBenefitPref> ccBankPartnerList = cardSearchDAO.getPreferenceList();
            if (!ccBankPartnerList.isEmpty()) {
                for (CcLifestyleBenefitPref ccLifestyleBenefitPref : ccBankPartnerList) {
                    CardDetailsBean cardDetailsBean = new CardDetailsBean();
                    cardDetailsBean.setLsbpNo(ccLifestyleBenefitPref.getLsbpNo());
                    cardDetailsBean.setImageAsIconText(ccLifestyleBenefitPref.getImageAsIconText());
                    cardDetailsBean.setLsbpImageIconPath(ccLifestyleBenefitPref.getImageAsIconPath());
                    cardDetailsBean.setLsbpImageText(ccLifestyleBenefitPref.getImageText());
                    cardDetailsBean.setLsbpImagePath(ccLifestyleBenefitPref.getImagePath());
                    cardDetailsBean.setLsbpImageIconBluePath(ccLifestyleBenefitPref.getImageAsIconBluePath());
                    prefList.add(cardDetailsBean);
                }
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getPreferenceList() :", e);
        }

        return prefList;
    }


//    newcode
    
        @Override
    public List<CardDetailsBean> getAllTypeCardList() {
        List<CardDetailsBean> cardList = new ArrayList<>();
        List<CcCardProduct> ccCardProductList = null;
        StringBuilder content;
        String cardUrl = "";
        List<CcPageGroupLink> ccPageGroupLink = new ArrayList<>();
        List<String> featureList = null;
        List<CcCardFeatures> ccFeature = new ArrayList<>();
        List<Integer> column = new ArrayList<>();
        CcPageContent ccpageContent = null;
        int gmNo = 0;
        try {
            ccCardProductList = cardSearchDAO.getAllTypeCardList();
            System.out.println("ccCardProductList  size>>>>>>>>>>> "+ccCardProductList.size());
            ccPageGroupLink = commonDAO.getGroupHeaders(1);
            ccpageContent = commonDAO.getPageContent();

            if (null != ccPageGroupLink) {

                for (CcPageGroupLink ccpage : ccPageGroupLink) {
                    column.add(ccpage.getCcGroupMaster().getGmNo());
                    if ("Benefits".equalsIgnoreCase(ccpage.getCcGroupMaster().getGroupName())) {
                        gmNo = ccpage.getCcGroupMaster().getGmNo();
                    }

                }
            }

            if (column.size() > 3) {
                column.subList(3, column.size()).clear();
            }

            Integer[] columOrder = column.toArray(new Integer[column.size()]);
            if (!ccCardProductList.isEmpty()) {
                for (CcCardProduct ccCardProduct : ccCardProductList) {
                    featureList = new ArrayList<>();
                    CardDetailsBean cardDetailsBean = new CardDetailsBean();
                    cardDetailsBean.setCpNo(ccCardProduct.getCpNo());
                    cardDetailsBean.setCardName(CommonUtils.nullSafe(ccCardProduct.getCardName()));
                    cardDetailsBean.setImageText(ccCardProduct.getCpImageText());
                    cardDetailsBean.setBpNo(ccCardProduct.getCcBankPartner().getBpNo());
                    cardDetailsBean.setBankName(CommonUtils.nullSafe(ccCardProduct.getCcBankPartner().getBankName()));
                    cardDetailsBean.setNetworkName(ccCardProduct.getCcNetworkList().getNetworkName());
                    
                    //new adobe changes 16th july start
                    cardDetailsBean.setCardInfo(ccCardProduct.getCardInfo());
                    //new adobe changes 16th july end
                    Byte bt = new Byte(ccCardProduct.getCardPosition());
                                cardDetailsBean.setCardDisplayorder(bt.intValue());
//			                         System.out.println("--->"+cardDetailsBean.getCardDisplayorder());
                    if (null != ccpageContent && ccCardProduct.getCcBankPartner().getBpNo() == 1) {
                        cardDetailsBean.setApprovalMessage(CommonUtils.nullSafe(ccpageContent.getApprovalMessage()));
                    } else {
                        cardDetailsBean.setApprovalMessage(" ");
                    }
                    cardUrl = CommonUtils.nullSafe(ccCardProduct.getCardUrl());
                    cardDetailsBean.setCardUrl(cardUrl);
                    cardDetailsBean.setCardImagePath(ccCardProduct.getCpImagePath());
                    cardDetailsBean.setCompareFlag(ccCardProduct.getCompareFlag());
                    cardDetailsBean.setFreeFlightsMessageFlag(ccCardProduct.getFreeFlightsMessageFlag());
                    cardDetailsBean.setFreeFlightsMessage(ccCardProduct.getFreeFlightsMessage());
                    cardDetailsBean.setApplyflag(ccCardProduct.getApplyFlag());// new cr2
                    
//                    System.out.println("=======1========>"+ccCardProduct.getApplyFlag());
//                    System.out.println("======2=======>"+cardDetailsBean.getApplyflag());
                    cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccCardProduct.getFees()));
                    cardDetailsBean.setLsbpList(cardSearchDAO.getlsbpArray(ccCardProduct.getCpNo()));
                    cardDetailsBean.setWelcomeBonus(CommonUtils.checkForDecimal(ccCardProduct.getWelcomeBonus()));
                    cardDetailsBean.setJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getJpMiles()));
                    cardDetailsBean.setPerSpendsOnJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getPerSpendsOnJpMiles()));
                    for (int j = 0; j < columOrder.length; j++) {
                        ccFeature = cardProductDAO.getCardFeatureforDetails(columOrder[j], ccCardProduct.getCpNo());

                        if (null != ccFeature) {
                            content = new StringBuilder();
                            for (CcCardFeatures cflist : ccFeature) {
                                if (cflist.getCfContent().length() != 0) {
                                    content = content.append(cflist.getCfContent().replaceAll("\\r|\\n", "").trim());
                                }

                            }
                            featureList.add(content.toString().trim());

                        }
                        cardDetailsBean.setFeatureList(featureList);
                    }

                    cardList.add(cardDetailsBean);

                    if (null != ccpageContent) {
                        cardList.get(0).setHomePageTitle(ccpageContent.getHomePageTitle());
                        cardList.get(0).setDescription(ccpageContent.getDescription());
                    }
                    if (gmNo != 0) {
                        cardList.get(0).setGmNo(gmNo);
                    }

                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getAllCardList() :", e);

        }
        return cardList;
    }

    

    @Override
    public List<CardDetailsBean> getAllCardList() {
        List<CardDetailsBean> cardList = new ArrayList<>();
        List<CcCardProduct> ccCardProductList = null;
        StringBuilder content;
        String cardUrl = "";
        List<CcPageGroupLink> ccPageGroupLink = new ArrayList<>();
        List<String> featureList = null;
        List<CcCardFeatures> ccFeature = new ArrayList<>();
        List<Integer> column = new ArrayList<>();
        CcPageContent ccpageContent = null;
        int gmNo = 0;
        try {
            ccCardProductList = cardSearchDAO.getAllCardList();
            ccPageGroupLink = commonDAO.getGroupHeaders(1);
            ccpageContent = commonDAO.getPageContent();

            if (null != ccPageGroupLink) {

                for (CcPageGroupLink ccpage : ccPageGroupLink) {
                    column.add(ccpage.getCcGroupMaster().getGmNo());
                    if ("Benefits".equalsIgnoreCase(ccpage.getCcGroupMaster().getGroupName())) {
                        gmNo = ccpage.getCcGroupMaster().getGmNo();
                    }

                }
            }

            if (column.size() > 3) {
                column.subList(3, column.size()).clear();
            }

            Integer[] columOrder = column.toArray(new Integer[column.size()]);
            if (!ccCardProductList.isEmpty()) {
                for (CcCardProduct ccCardProduct : ccCardProductList) {
                    featureList = new ArrayList<>();
                    CardDetailsBean cardDetailsBean = new CardDetailsBean();
                    cardDetailsBean.setCpNo(ccCardProduct.getCpNo());
                    cardDetailsBean.setCardName(CommonUtils.nullSafe(ccCardProduct.getCardName()));
                    cardDetailsBean.setImageText(ccCardProduct.getCpImageText());
                    cardDetailsBean.setBpNo(ccCardProduct.getCcBankPartner().getBpNo());
                    cardDetailsBean.setBankName(CommonUtils.nullSafe(ccCardProduct.getCcBankPartner().getBankName()));
                    cardDetailsBean.setNetworkName(ccCardProduct.getCcNetworkList().getNetworkName());
                    
                    //new adobe changes 16th july start
                    cardDetailsBean.setCardInfo(ccCardProduct.getCardInfo());
                    //new adobe changes 16th july end
                    Byte bt = new Byte(ccCardProduct.getCardPosition());
                                cardDetailsBean.setCardDisplayorder(bt.intValue());
//			                         System.out.println("--->"+cardDetailsBean.getCardDisplayorder());
                    if (null != ccpageContent && ccCardProduct.getCcBankPartner().getBpNo() == 1) {
                        cardDetailsBean.setApprovalMessage(CommonUtils.nullSafe(ccpageContent.getApprovalMessage()));
                    } else {
                        cardDetailsBean.setApprovalMessage(" ");
                    }
                    cardUrl = CommonUtils.nullSafe(ccCardProduct.getCardUrl());
                    cardDetailsBean.setCardUrl(cardUrl);
                    cardDetailsBean.setCardImagePath(ccCardProduct.getCpImagePath());
                    cardDetailsBean.setCompareFlag(ccCardProduct.getCompareFlag());
                    cardDetailsBean.setFreeFlightsMessageFlag(ccCardProduct.getFreeFlightsMessageFlag());
                    cardDetailsBean.setFreeFlightsMessage(ccCardProduct.getFreeFlightsMessage());
                    cardDetailsBean.setApplyflag(ccCardProduct.getApplyFlag());// new cr2
                    
//                    System.out.println("=======1========>"+ccCardProduct.getApplyFlag());
//                    System.out.println("======2=======>"+cardDetailsBean.getApplyflag());
                    cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccCardProduct.getFees()));
                    cardDetailsBean.setLsbpList(cardSearchDAO.getlsbpArray(ccCardProduct.getCpNo()));
                    cardDetailsBean.setWelcomeBonus(CommonUtils.checkForDecimal(ccCardProduct.getWelcomeBonus()));
                    cardDetailsBean.setJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getJpMiles()));
                    cardDetailsBean.setPerSpendsOnJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getPerSpendsOnJpMiles()));
                    for (int j = 0; j < columOrder.length; j++) {
                        ccFeature = cardProductDAO.getCardFeatureforDetails(columOrder[j], ccCardProduct.getCpNo());

                        if (null != ccFeature) {
                            content = new StringBuilder();
                            for (CcCardFeatures cflist : ccFeature) {
                                if (cflist.getCfContent().length() != 0) {
                                    content = content.append(cflist.getCfContent().replaceAll("\\r|\\n", "").trim());
                                }

                            }
                            featureList.add(content.toString().trim());

                        }
                        cardDetailsBean.setFeatureList(featureList);
                    }

                    cardList.add(cardDetailsBean);

                    if (null != ccpageContent) {
                        cardList.get(0).setHomePageTitle(ccpageContent.getHomePageTitle());
                        cardList.get(0).setDescription(ccpageContent.getDescription());
                    }
                    if (gmNo != 0) {
                        cardList.get(0).setGmNo(gmNo);
                    }

                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getAllCardList() :", e);

        }
        return cardList;
    }

    @Override
    public List<CardDetailsBean> getCardListByFees(BigDecimal fees) {
        List<CardDetailsBean> cardList = new ArrayList<>();
        List<CcCardProduct> ccCardProductList = null;
        try {
            ccCardProductList = cardSearchDAO.getCardListByFees(fees);
            if (!ccCardProductList.isEmpty()) {
                for (CcCardProduct ccCardProduct : ccCardProductList) {
                    CardDetailsBean cardDetailsBean = new CardDetailsBean();
                    cardDetailsBean.setCpNo(ccCardProduct.getCpNo());
                    cardDetailsBean.setCardName(ccCardProduct.getCardName());
                    cardDetailsBean.setCardImagePath(ccCardProduct.getCpImagePath());
                    cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccCardProduct.getFees()));
                    cardDetailsBean.setWelcomeBonus(CommonUtils.checkForDecimal(ccCardProduct.getWelcomeBonus()));
                    cardDetailsBean.setJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getJpMiles()));
                    cardDetailsBean.setPerSpendsOnJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getPerSpendsOnJpMiles()));
                    cardList.add(cardDetailsBean);
                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getCardListByFees() :", e);
        }
        return cardList;
    }

    @Override
    public List<CardDetailsBean> getCardListByLyfPref(Integer lsbpNo) {
        List<CardDetailsBean> cardList = new ArrayList<>();
        List<CcLifestyleBenefitPrefValues> ccLyfPrefList = null;
        try {
            ccLyfPrefList = cardSearchDAO.getCardListByLyfPref(lsbpNo);
            if (!ccLyfPrefList.isEmpty()) {
                for (CcLifestyleBenefitPrefValues ccLyfPref : ccLyfPrefList) {
                    CardDetailsBean cardDetailsBean = new CardDetailsBean();
                    cardDetailsBean.setCpNo(ccLyfPref.getCcCardProduct().getCpNo());
                    cardDetailsBean.setCardName(ccLyfPref.getCcCardProduct().getCardName());
                    cardDetailsBean.setCardImagePath(ccLyfPref.getCcCardProduct().getCpImagePath());
                    cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccLyfPref.getCcCardProduct().getFees()));
                    cardDetailsBean.setWelcomeBonus(CommonUtils.checkForDecimal(ccLyfPref.getCcCardProduct().getWelcomeBonus()));
                    cardDetailsBean.setJpMiles(CommonUtils.checkForDecimal(ccLyfPref.getCcCardProduct().getJpMiles()));
                    cardDetailsBean.setPerSpendsOnJpMiles(CommonUtils.checkForDecimal(ccLyfPref.getCcCardProduct().getPerSpendsOnJpMiles()));
                    cardDetailsBean.setLsbpNo(ccLyfPref.getCcLifestyleBenefitPref().getLsbpNo());
                    cardDetailsBean.setLsbpImageText(ccLyfPref.getCcLifestyleBenefitPref().getImageText());
                    cardList.add(cardDetailsBean);
                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getCardListByLyfPref() :", e);
        }
        return cardList;
    }

    @Override
    public List<String> getCity() {
        List<String> cityList = new ArrayList<>();
        try {
            cityList = cardSearchDAO.getCity();
            if (!cityList.isEmpty()) {
                if (cityList.contains("Others")) {
                    cityList.remove("Others");
                }
                cityList.add("Others");
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getCity() :", e);
        }
        return cityList;

    }

    @Override
    public Map<Integer, String> calculateFreeFlightsByArray(String spend, String cpNo) {
        CcCardProduct ccCard = null;
        Map<Integer, String> map = new HashMap<>();
        double perSpend;
        double baseJpMiles;
        double welcomebonus;
        double activationBonus;
        double thresholdBonus;
        double jPChannel;
        double milesEarnedOnSpends;
        double totalMiles;
        double freeTickets;
        double thresholdValue;
        double bonusTickets;
        int intCpId = 0;
        try {
            spend = spend.replace(",", "").trim();
            String[] strArray = cpNo.split(",");
            for (String cpId : strArray) {
                intCpId = Integer.parseInt(cpId);
                ccCard = cardProductDAO.getCardProductById(intCpId);
                double spendVal;
                spendVal = Double.parseDouble(spend) / 12.0;
                if (null != ccCard) {
                    perSpend = ccCard.getPerSpendsOnJpMiles() != null ? ccCard.getPerSpendsOnJpMiles().doubleValue() : 0.0;
                    baseJpMiles = ccCard.getJpMiles() != null ? ccCard.getJpMiles().doubleValue() : 0.0;
                    milesEarnedOnSpends = MilesCalculationUtil.calculateMileEarnedOnSpends(Double.parseDouble(spend), perSpend, baseJpMiles);
                    welcomebonus = ccCard.getWelcomeBonus() != null ? ccCard.getWelcomeBonus().doubleValue() : 0.0;
                    activationBonus = ccCard.getActivationBonusJpMiles() != null ? ccCard.getActivationBonusJpMiles().doubleValue() : 0.0;
                    thresholdBonus = ccCard.getThresholdBonusJpMiles() != null ? ccCard.getThresholdBonusJpMiles().doubleValue() : 0.0;
                    thresholdValue = ccCard.getThresholdValue() != null ? ccCard.getThresholdValue().doubleValue() : 0.0;
                    jPChannel = ccCard.getJpChannel() != null ? ccCard.getJpChannel().doubleValue() : 0.0;
                    bonusTickets = ccCard.getBonusTickets() != null ? ccCard.getBonusTickets().doubleValue() : 0.0;

                    totalMiles = MilesCalculationUtil.calculateTotalMiles(milesEarnedOnSpends, welcomebonus, activationBonus, thresholdBonus, jPChannel, spendVal, thresholdValue);
                    freeTickets = MilesCalculationUtil.calculateFreeTickets(totalMiles);
//                    freeTickets = freeTickets + bonusTickets;
                    map.put(Integer.parseInt(cpId), Integer.toString((int) totalMiles) + "," + Integer.toString((int) freeTickets));

                }
            }
        } catch (Exception e) {
            map.put(intCpId, "0,0");
            logger.error("@@@@ Exception in CardSearchServiceImpl calculateFreeTickets() :", e);
        }
        return map;
    }

    @Override
    public List<CardDetailsBean> getSelectedDebitCardList(Integer Countryid) {
        List<CardDetailsBean> cardList = new ArrayList<>();
        List<CcCardProduct> ccCardProductList = null;
        StringBuilder content;
        String cardUrl = "";
        List<CcPageGroupLink> ccPageGroupLink = new ArrayList<>();
        List<String> featureList = null;
        List<CcCardFeatures> ccFeature = new ArrayList<>();
        List<Integer> column = new ArrayList<>();
        CcPageContent ccpageContent = null;
        int gmNo = 0;
        try {
            System.out.println("Inside getSelectedDebitCardList called in service Impl");
        	System.out.println("Countryid>>>>>"+Countryid);
        	ccCardProductList = cardSearchDAO.getSelectedDebitCardList(Countryid);
        	System.out.println("ccCardProductList>>>>>"+ccCardProductList.size());

            ccPageGroupLink = commonDAO.getGroupHeaders(1);
            ccpageContent = commonDAO.getPageContent();

            if (null != ccPageGroupLink) {

                for (CcPageGroupLink ccpage : ccPageGroupLink) {
                    column.add(ccpage.getCcGroupMaster().getGmNo());
                    if ("Benefits".equalsIgnoreCase(ccpage.getCcGroupMaster().getGroupName())) {
                        gmNo = ccpage.getCcGroupMaster().getGmNo();
                    }

                }
            }

            if (column.size() > 3) {
                column.subList(3, column.size()).clear();
            }

            Integer[] columOrder = column.toArray(new Integer[column.size()]);
            if (!ccCardProductList.isEmpty()) {
                for (CcCardProduct ccCardProduct : ccCardProductList) {
                    featureList = new ArrayList<>();
                    CardDetailsBean cardDetailsBean = new CardDetailsBean();
                    cardDetailsBean.setCpNo(ccCardProduct.getCpNo());
                    cardDetailsBean.setCardName(CommonUtils.nullSafe(ccCardProduct.getCardName()));
                    cardDetailsBean.setImageText(ccCardProduct.getCpImageText());
                    cardDetailsBean.setBpNo(ccCardProduct.getCcBankPartner().getBpNo());
                    cardDetailsBean.setBankName(CommonUtils.nullSafe(ccCardProduct.getCcBankPartner().getBankName()));
                    cardDetailsBean.setNetworkName(ccCardProduct.getCcNetworkList().getNetworkName());
                    
                    //new adobe changes 16th july start
                    cardDetailsBean.setCardInfo(ccCardProduct.getCardInfo());
                    //new adobe changes 16th july end
                     Byte bt = new Byte(ccCardProduct.getCardPosition());
                                cardDetailsBean.setCardDisplayorder(bt.intValue());
//			                         System.out.println("--->"+cardDetailsBean.getCardDisplayorder());
                    if (null != ccpageContent && ccCardProduct.getCcBankPartner().getBpNo() == 1) {
                        cardDetailsBean.setApprovalMessage(CommonUtils.nullSafe(ccpageContent.getApprovalMessage()));
                    } else {
                        cardDetailsBean.setApprovalMessage(" ");
                    }
                    cardUrl = CommonUtils.nullSafe(ccCardProduct.getCardUrl());
                    cardDetailsBean.setCardUrl(cardUrl);
                    cardDetailsBean.setCardImagePath(ccCardProduct.getCpImagePath());

                    cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccCardProduct.getFees()));
                    cardDetailsBean.setLsbpList(cardSearchDAO.getlsbpArray(ccCardProduct.getCpNo()));
                    cardDetailsBean.setWelcomeBonus(CommonUtils.checkForDecimal(ccCardProduct.getWelcomeBonus()));
                    cardDetailsBean.setJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getJpMiles()));
                    cardDetailsBean.setPerSpendsOnJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getPerSpendsOnJpMiles()));
                    for (int j = 0; j < columOrder.length; j++) {
                        ccFeature = cardProductDAO.getCardFeatureforDetails(columOrder[j], ccCardProduct.getCpNo());

                        if (null != ccFeature) {
                            content = new StringBuilder();
                            for (CcCardFeatures cflist : ccFeature) {
                                if (cflist.getCfContent().length() != 0) {
                                    content = content.append(cflist.getCfContent().replaceAll("\\r|\\n", "").trim());
                                }

                            }
                            featureList.add(content.toString().trim());

                        }
                        cardDetailsBean.setFeatureList(featureList);
                    }

                    cardDetailsBean.setFreeFlightsMessageFlag(ccCardProduct.getFreeFlightsMessageFlag());
                    cardDetailsBean.setFreeFlightsMessage(CommonUtils.nullSafe(ccCardProduct.getFreeFlightsMessage()));
                    cardDetailsBean.setCompareFlag(ccCardProduct.getCompareFlag());
                    cardDetailsBean.setApplyflag(ccCardProduct.getApplyFlag());// new cr2
                    cardList.add(cardDetailsBean);

                    if (null != ccpageContent) {
                        cardList.get(0).setHomePageTitle(ccpageContent.getHomePageTitle());
                        cardList.get(0).setDescription(ccpageContent.getDescription());
                    }
                    if (gmNo != 0) {
                        cardList.get(0).setGmNo(gmNo);
                    }

                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getAllCardList() :", e);

        }
        return cardList;
    }

    @Override
    public List<CardDetailsBean> getSelectedCorporateCardList(Integer Countryid) {
        List<CardDetailsBean> cardList = new ArrayList<>();
        List<CcCardProduct> ccCardProductList = null;
        StringBuilder content;
        String cardUrl = "";
        List<CcPageGroupLink> ccPageGroupLink = new ArrayList<>();
        List<String> featureList = null;
        List<CcCardFeatures> ccFeature = new ArrayList<>();
        List<Integer> column = new ArrayList<>();
        CcPageContent ccpageContent = null;
        int gmNo = 0;
        try {
            ccCardProductList = cardSearchDAO.getSelectedCorporateCardList(Countryid);

            ccPageGroupLink = commonDAO.getGroupHeaders(1);
            ccpageContent = commonDAO.getPageContent();

            if (null != ccPageGroupLink) {

                for (CcPageGroupLink ccpage : ccPageGroupLink) {
                    column.add(ccpage.getCcGroupMaster().getGmNo());
                    if ("Benefits".equalsIgnoreCase(ccpage.getCcGroupMaster().getGroupName())) {
                        gmNo = ccpage.getCcGroupMaster().getGmNo();
                    }

                }
            }

            if (column.size() > 3) {
                column.subList(3, column.size()).clear();
            }

            Integer[] columOrder = column.toArray(new Integer[column.size()]);
            if (!ccCardProductList.isEmpty()) {
                for (CcCardProduct ccCardProduct : ccCardProductList) {
                    featureList = new ArrayList<>();
                    CardDetailsBean cardDetailsBean = new CardDetailsBean();
                    cardDetailsBean.setCpNo(ccCardProduct.getCpNo());
                    cardDetailsBean.setCardName(CommonUtils.nullSafe(ccCardProduct.getCardName()));
                    cardDetailsBean.setImageText(ccCardProduct.getCpImageText());
                    cardDetailsBean.setBpNo(ccCardProduct.getCcBankPartner().getBpNo());
                    cardDetailsBean.setBankName(CommonUtils.nullSafe(ccCardProduct.getCcBankPartner().getBankName()));
                    cardDetailsBean.setNetworkName(ccCardProduct.getCcNetworkList().getNetworkName());
                    
                    //new adobe changes 16th july start
                    cardDetailsBean.setCardInfo(ccCardProduct.getCardInfo());
                    //new adobe changes 16th july end
                     Byte bt = new Byte(ccCardProduct.getCardPosition());
                                cardDetailsBean.setCardDisplayorder(bt.intValue());
//			                         System.out.println("--->"+cardDetailsBean.getCardDisplayorder());
                    if (null != ccpageContent && ccCardProduct.getCcBankPartner().getBpNo() == 1) {
                        cardDetailsBean.setApprovalMessage(CommonUtils.nullSafe(ccpageContent.getApprovalMessage()));
                    } else {
                        cardDetailsBean.setApprovalMessage(" ");
                    }
                    cardUrl = CommonUtils.nullSafe(ccCardProduct.getCardUrl());
                    cardDetailsBean.setCardUrl(cardUrl);
                    cardDetailsBean.setCardImagePath(ccCardProduct.getCpImagePath());

                    cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccCardProduct.getFees()));
                    cardDetailsBean.setLsbpList(cardSearchDAO.getlsbpArray(ccCardProduct.getCpNo()));
                    cardDetailsBean.setWelcomeBonus(CommonUtils.checkForDecimal(ccCardProduct.getWelcomeBonus()));
                    cardDetailsBean.setJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getJpMiles()));
                    cardDetailsBean.setPerSpendsOnJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getPerSpendsOnJpMiles()));
                    for (int j = 0; j < columOrder.length; j++) {
                        ccFeature = cardProductDAO.getCardFeatureforDetails(columOrder[j], ccCardProduct.getCpNo());

                        if (null != ccFeature) {
                            content = new StringBuilder();
                            for (CcCardFeatures cflist : ccFeature) {
                                if (cflist.getCfContent().length() != 0) {
                                    content = content.append(cflist.getCfContent().replaceAll("\\r|\\n", "").trim());
                                }

                            }
                            featureList.add(content.toString().trim());

                        }
                        cardDetailsBean.setFeatureList(featureList);
                    }

                    cardDetailsBean.setFreeFlightsMessageFlag(ccCardProduct.getFreeFlightsMessageFlag());
                    cardDetailsBean.setFreeFlightsMessage(CommonUtils.nullSafe(ccCardProduct.getFreeFlightsMessage()));
                    cardDetailsBean.setCompareFlag(ccCardProduct.getCompareFlag());
                    cardDetailsBean.setApplyflag(ccCardProduct.getApplyFlag());// new cr2
                    cardList.add(cardDetailsBean);

                    if (null != ccpageContent) {
                        cardList.get(0).setHomePageTitle(ccpageContent.getHomePageTitle());
                        cardList.get(0).setDescription(ccpageContent.getDescription());
                    }
                    if (gmNo != 0) {
                        cardList.get(0).setGmNo(gmNo);
                    }

                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getAllCardList() :", e);

        }
        return cardList;
    }

    @Override
    public List<CardDetailsBean> getbanklistcreditindia(Integer categoryid, Integer countryid) {

        List<CardDetailsBean> bankList = new ArrayList<CardDetailsBean>();
        try {
            System.out.println("Inside getbanklistcreditindia called");
        	List<CcBankPartner> ccBankPartnerList;
            CardDetailsBean cardDetailsBean = null;
            ccBankPartnerList = cardSearchDAO.getbanklistcreditindia(categoryid, countryid);
            System.out.println("ccBankPartnerList>>>"+ccBankPartnerList.size());
            if (!ccBankPartnerList.isEmpty()) {
                for (CcBankPartner ccBankPartner : ccBankPartnerList) {
                    cardDetailsBean = new CardDetailsBean();
                    System.out.println("Bank_Name>>>"+ccBankPartner.getBankName());
                    cardDetailsBean.setBankName(ccBankPartner.getBankName());
                    cardDetailsBean.setBpNo(ccBankPartner.getBpNo());
                    cardDetailsBean.setBankImagePath(ccBankPartner.getBpImagePath());
                    bankList.add(cardDetailsBean);
                }
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getBankList() :", e);
        }

        return bankList;
    }

    @Override
    public List<CardDetailsBean> getSelectedBankAllCardList() {
        List<CardDetailsBean> cardList = new ArrayList<>();
        List<CcCardProduct> ccCardProductList = null;
        StringBuilder content;
        String cardUrl = "";
        List<CcPageGroupLink> ccPageGroupLink = new ArrayList<>();
        List<String> featureList = null;
        List<CcCardFeatures> ccFeature = new ArrayList<>();
        List<Integer> column = new ArrayList<>();
        CcPageContent ccpageContent = null;
        int gmNo = 0;
        try {
            ccCardProductList = cardSearchDAO.getSelectedBankAllCardList();
            ccPageGroupLink = commonDAO.getGroupHeaders(1);
            ccpageContent = commonDAO.getPageContent();

            if (null != ccPageGroupLink) {

                for (CcPageGroupLink ccpage : ccPageGroupLink) {
                    column.add(ccpage.getCcGroupMaster().getGmNo());
                    if ("Benefits".equalsIgnoreCase(ccpage.getCcGroupMaster().getGroupName())) {
                        gmNo = ccpage.getCcGroupMaster().getGmNo();
                    }

                }
            }

            if (column.size() > 3) {
                column.subList(3, column.size()).clear();
            }

            Integer[] columOrder = column.toArray(new Integer[column.size()]);
            if (!ccCardProductList.isEmpty()) {
                for (CcCardProduct ccCardProduct : ccCardProductList) {
                    featureList = new ArrayList<>();
                    CardDetailsBean cardDetailsBean = new CardDetailsBean();
                    cardDetailsBean.setCpNo(ccCardProduct.getCpNo());
                    cardDetailsBean.setCardName(CommonUtils.nullSafe(ccCardProduct.getCardName()));
                    cardDetailsBean.setImageText(ccCardProduct.getCpImageText());
                    cardDetailsBean.setBpNo(ccCardProduct.getCcBankPartner().getBpNo());
                    cardDetailsBean.setBankName(CommonUtils.nullSafe(ccCardProduct.getCcBankPartner().getBankName()));
                    cardDetailsBean.setNetworkName(ccCardProduct.getCcNetworkList().getNetworkName());
                    
                     //new adobe changes 22nd june start
                    cardDetailsBean.setCardInfo(ccCardProduct.getCardInfo());
                    //new adobe changes 22nd june end
                     Byte bt = new Byte(ccCardProduct.getCardPosition());
                                cardDetailsBean.setCardDisplayorder(bt.intValue());
//			                         System.out.println("--->"+cardDetailsBean.getCardDisplayorder());
                    if (null != ccpageContent && ccCardProduct.getCcBankPartner().getBpNo() == 1) {
                        cardDetailsBean.setApprovalMessage(CommonUtils.nullSafe(ccpageContent.getApprovalMessage()));
                    } else {
                        cardDetailsBean.setApprovalMessage(" ");
                    }
                    cardUrl = CommonUtils.nullSafe(ccCardProduct.getCardUrl());
                    cardDetailsBean.setCardUrl(cardUrl);
                    cardDetailsBean.setCardImagePath(ccCardProduct.getCpImagePath());

                    cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccCardProduct.getFees()));
                    cardDetailsBean.setLsbpList(cardSearchDAO.getlsbpArray(ccCardProduct.getCpNo()));
                    cardDetailsBean.setWelcomeBonus(CommonUtils.checkForDecimal(ccCardProduct.getWelcomeBonus()));
                    cardDetailsBean.setJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getJpMiles()));
                    cardDetailsBean.setPerSpendsOnJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getPerSpendsOnJpMiles()));
                    for (int j = 0; j < columOrder.length; j++) {
                        ccFeature = cardProductDAO.getCardFeatureforDetails(columOrder[j], ccCardProduct.getCpNo());

                        if (null != ccFeature) {
                            content = new StringBuilder();
                            for (CcCardFeatures cflist : ccFeature) {
                                if (cflist.getCfContent().length() != 0) {
                                    content = content.append(cflist.getCfContent().replaceAll("\\r|\\n", "").trim());
                                }

                            }
                            featureList.add(content.toString().trim());

                        }
                        cardDetailsBean.setFeatureList(featureList);
                    }

                    cardDetailsBean.setFreeFlightsMessageFlag(ccCardProduct.getFreeFlightsMessageFlag());
                    cardDetailsBean.setFreeFlightsMessage(CommonUtils.nullSafe(ccCardProduct.getFreeFlightsMessage()));
                    cardDetailsBean.setCompareFlag(ccCardProduct.getCompareFlag());
                    cardDetailsBean.setApplyflag(ccCardProduct.getApplyFlag());// new cr2
                    cardList.add(cardDetailsBean);

                    if (null != ccpageContent) {
                        cardList.get(0).setHomePageTitle(ccpageContent.getHomePageTitle());
                        cardList.get(0).setDescription(ccpageContent.getDescription());
                    }
                    if (gmNo != 0) {
                        cardList.get(0).setGmNo(gmNo);
                    }

                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getAllCardList() :", e);

        }
        return cardList;
    }

    @Override
    public List<CardDetailsBean> getbanklistdebit(Integer categoryid) {
        List<CardDetailsBean> bankList = new ArrayList<CardDetailsBean>();
        try {
            List<CcBankPartner> ccBankPartnerList;
            CardDetailsBean cardDetailsBean = null;
            ccBankPartnerList = cardSearchDAO.getbanklistdebit(categoryid);
            if (!ccBankPartnerList.isEmpty()) {
                for (CcBankPartner ccBankPartner : ccBankPartnerList) {
                    cardDetailsBean = new CardDetailsBean();
                    cardDetailsBean.setBankName(ccBankPartner.getBankName());
                    cardDetailsBean.setBpNo(ccBankPartner.getBpNo());
                    cardDetailsBean.setBankImagePath(ccBankPartner.getBpImagePath());
                    bankList.add(cardDetailsBean);
                }
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getBankList() :", e);
        }

        return bankList;
    }

    @Override
    public List<CardDetailsBean> getSelectedOtherCardList(Integer Countryid) {
        List<CardDetailsBean> cardList = new ArrayList<>();
        List<CcCardProduct> ccCardProductList = null;
        StringBuilder content;
        String cardUrl = "";
        List<CcPageGroupLink> ccPageGroupLink = new ArrayList<>();
        List<String> featureList = null;
        List<CcCardFeatures> ccFeature = new ArrayList<>();
        List<Integer> column = new ArrayList<>();
        CcPageContent ccpageContent = null;
        int gmNo = 0;
        try {
            ccCardProductList = cardSearchDAO.getSelectedOtherCardList(Countryid);

            ccPageGroupLink = commonDAO.getGroupHeaders(1);
            ccpageContent = commonDAO.getPageContent();

            if (null != ccPageGroupLink) {

                for (CcPageGroupLink ccpage : ccPageGroupLink) {
                    column.add(ccpage.getCcGroupMaster().getGmNo());
                    if ("Benefits".equalsIgnoreCase(ccpage.getCcGroupMaster().getGroupName())) {
                        gmNo = ccpage.getCcGroupMaster().getGmNo();
                    }

                }
            }

            if (column.size() > 3) {
                column.subList(3, column.size()).clear();
            }

            Integer[] columOrder = column.toArray(new Integer[column.size()]);
            if (!ccCardProductList.isEmpty()) {
                for (CcCardProduct ccCardProduct : ccCardProductList) {
                    featureList = new ArrayList<>();
                    CardDetailsBean cardDetailsBean = new CardDetailsBean();
                    System.out.println("ccCardProduct.getCpNo()>>>"+ccCardProduct.getCpNo());
                    System.out.println("ccCardProduct.getCardName()>>>"+ccCardProduct.getCardName());
                    System.out.println("ccCardProduct.getCardInfo()>>>"+ccCardProduct.getCardInfo());
                    System.out.println("ccCardProduct.getCardInfo()>>>"+ccCardProduct.getCardInfo());
                    cardDetailsBean.setCpNo(ccCardProduct.getCpNo());
                    cardDetailsBean.setCardName(CommonUtils.nullSafe(ccCardProduct.getCardName()));
                    cardDetailsBean.setImageText(ccCardProduct.getCpImageText());
                    cardDetailsBean.setBpNo(ccCardProduct.getCcBankPartner().getBpNo());
                    cardDetailsBean.setBankName(CommonUtils.nullSafe(ccCardProduct.getCcBankPartner().getBankName()));
                    cardDetailsBean.setNetworkName(ccCardProduct.getCcNetworkList().getNetworkName());
                    
                    //new adobe changes 16th july start
                    cardDetailsBean.setCardInfo(ccCardProduct.getCardInfo());
                    //new adobe changes 16th july end
                     Byte bt = new Byte(ccCardProduct.getCardPosition());
                                cardDetailsBean.setCardDisplayorder(bt.intValue());
			                         System.out.println("cardDetailsBean.getCardDisplayorder()--->"+cardDetailsBean.getCardDisplayorder());
                    if (null != ccpageContent && ccCardProduct.getCcBankPartner().getBpNo() == 1) {
                        cardDetailsBean.setApprovalMessage(CommonUtils.nullSafe(ccpageContent.getApprovalMessage()));
                    } else {
                        cardDetailsBean.setApprovalMessage(" ");
                    }
                    cardUrl = CommonUtils.nullSafe(ccCardProduct.getCardUrl());
                    cardDetailsBean.setCardUrl(cardUrl);
                    cardDetailsBean.setCardImagePath(ccCardProduct.getCpImagePath());

                    cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccCardProduct.getFees()));
                    cardDetailsBean.setLsbpList(cardSearchDAO.getlsbpArray(ccCardProduct.getCpNo()));
                    cardDetailsBean.setWelcomeBonus(CommonUtils.checkForDecimal(ccCardProduct.getWelcomeBonus()));
                    cardDetailsBean.setJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getJpMiles()));
                    cardDetailsBean.setPerSpendsOnJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getPerSpendsOnJpMiles()));
                    System.out.println("columOrder.length>>>"+columOrder.length);
                    for (int j = 0; j < columOrder.length; j++) {
                        ccFeature = cardProductDAO.getCardFeatureforDetails(columOrder[j], ccCardProduct.getCpNo());

                        if (null != ccFeature) {
                            content = new StringBuilder();
                            for (CcCardFeatures cflist : ccFeature) {
                                if (cflist.getCfContent().length() != 0) {
                                    content = content.append(cflist.getCfContent().replaceAll("\\r|\\n", "").trim());
                                }

                            }
                            featureList.add(content.toString().trim());

                        }
                        cardDetailsBean.setFeatureList(featureList);
                    }

                    cardDetailsBean.setFreeFlightsMessageFlag(ccCardProduct.getFreeFlightsMessageFlag());
                    cardDetailsBean.setFreeFlightsMessage(CommonUtils.nullSafe(ccCardProduct.getFreeFlightsMessage()));
                    cardDetailsBean.setCompareFlag(ccCardProduct.getCompareFlag());
                    cardDetailsBean.setApplyflag(ccCardProduct.getApplyFlag());// new cr2
                    cardList.add(cardDetailsBean);

                    if (null != ccpageContent) {
                        cardList.get(0).setHomePageTitle(ccpageContent.getHomePageTitle());
                        cardList.get(0).setDescription(ccpageContent.getDescription());
                    }
                    if (gmNo != 0) {
                        cardList.get(0).setGmNo(gmNo);
                    }

                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CardSearchServiceImpl getAllCardList() :", e);

        }
        return cardList;
    }

}
