package com.cbc.portal.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cbc.portal.beans.BannerBean;
import com.cbc.portal.DAO.CardProductDAO;
import com.cbc.portal.DAO.CommonDAO;
import com.cbc.portal.beans.BannerBean;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.CityMappingBean;
import com.cbc.portal.beans.JsonBean;
import com.cbc.portal.entity.CcAmexApplicationStatus;
import com.cbc.portal.entity.CcBankApplication;
import com.cbc.portal.entity.CcBankPartner;
import com.cbc.portal.entity.CcBankwiseResponseMapping;
import com.cbc.portal.entity.CcBannerManagement;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcCardVarientURL;
import com.cbc.portal.entity.CcEnumValues;
import com.cbc.portal.entity.CcIciciApplicationStatus;
import com.cbc.portal.entity.CcOTP;
import com.cbc.portal.entity.CcPageGroupLink;
import com.cbc.portal.entity.CcPageMaster;
import com.cbc.portal.entity.CcTimer;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.SendGridMail;
import com.cbc.portal.utils.SilverPopUtil;
import com.cbc.portal.utils.SmsService;
import com.cc.SilverPopApiCards;
import com.cc.TransactRequestType;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.springframework.beans.factory.annotation.Value;
import recipientactions.listmgmt.engageservice.ColumnElementType;

@Component
@Transactional(readOnly = true)
public class CommonServiceImpl implements CommonService {

    @Autowired
    CommonDAO commonDAO;

    @Autowired
    private CardProductDAO cardProductDAO;

    @Value("${application.newSms.username}")
    private String smsUserName;

    @Value("${application.newSms.password}")
    private String smsPassword;

    @Value("${application.newSms.sendby}")
    private String smsSendby;

    @Value("${application.newSms.url}")
    private String smsUrl;

    @Value("${application.silverPopApi.url}")
    private String url;

    @Value("${application.silverPopApi.clientID}")
    private String clientId;

    @Value("${application.silverPopApi.clientSecret}")
    private String clientSecret;

    @Value("${application.silverPopApi.refreshToken}")
    private String refreshToken;

    @Value("${application.silverpop.callmeid.smsid}")
//	private long smsid;
    private String smsid;

    @Value("${application.silverpop.userName}")
    private String silverPopuserName;

    @Value("${application.silverpop.password}")
    private String silverPoppassword;

    @Value("${application.silverpop.agentMailId}")
    private String agentMailId;
    
    @Value("${application.newSms.SMSMSGID}")
    private String SMSMSGID;
    
    @Value("${application.sendGridApi.url}")
    private String sendGridUrl;

    @Value("${application.sendGridApi.key}")
    private String sendGridKey;

    @Value("${application.simplica.username}")
    private String simplicaUserName;
    
    @Value("${application.simplica.password}")
    private String simplicaPassword;

    private Logger logger = Logger.getLogger(CommonServiceImpl.class.getName());

    @Override
    public Map<Byte, String> getEnumValues(String typeName) {
        Map<Byte, String> enumValues = new HashMap<>();
        List<CcEnumValues> enumList = new ArrayList<>();
        try {
            enumList = commonDAO.getEnumValues((null != typeName) ? typeName.trim() : "");
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getEnumValues() :", e);
        }
        for (CcEnumValues enumVal : enumList) {
            enumValues.put(enumVal.getIntCode(), enumVal.getValue());
        }

        return enumValues;
    }

    @Override
    public Map<String, String> getAmexValues(String typeName) {
        typeName = (null != typeName) ? typeName.trim() : "";
        List<CcEnumValues> enumList = new ArrayList<>();
        Map<String, String> mapString = new LinkedHashMap<>();
        try {
            enumList = commonDAO.getEnumValues(typeName);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getAmexValues() :", e);
        }
        for (CcEnumValues enumVal : enumList) {
            String[] tokens = enumVal.getValue().split("-");
            mapString.put(tokens[0], tokens[1].replace("-", ""));
        }

        return mapString;
    }

    @Override
    public String getAmexEnumValues(String typeName, String val) {
        typeName = (null != typeName) ? typeName.trim() : "";
        List<CcEnumValues> enumList = new ArrayList<>();
        String enumData = "";
        try {
            enumList = commonDAO.getEnumValues(typeName);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getAmexEnumValues() :", e);
        }
        for (CcEnumValues enumVal : enumList) {
            String[] tokens = enumVal.getValue().split("-");
            if (tokens[0].equalsIgnoreCase(val)) {
                enumData = tokens[1].replace("-", "");
            }
        }

        return enumData;
    }

    @Override
    public CityMappingBean getCityMapping(String pin) {
        CityMappingBean ccMapping = null;
        try {
            ccMapping = commonDAO.getCityMapping(pin);

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getCityMapping() :", e);
        }
        return ccMapping;
    }

    @Override
    public CityMappingBean getICICICityMapping(String pin) {
        CityMappingBean ccMapping = null;
        try {
            ccMapping = commonDAO.getICICICityMapping(pin);

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getICICICityMapping() :", e);
        }
        return ccMapping;
    }

    @Override
    public Map<String, String> getCitiesByState(String state) {
        Map<String, String> citymap = new LinkedHashMap<>();
        List<String> cardList = new ArrayList<>();
        try {
            cardList = commonDAO.getCitiesByState(state);
            for (String city : cardList) {
                citymap.put(city, city);
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getCitiesByState() :", e);
        }
        return citymap;
    }

    public List<String> getCardType() {
        List<String> cardType = new ArrayList<String>();
        try {
            System.out.println("getCardType Called Inside CommonServiceImpl");
        	cardType = commonDAO.getCardType();
        	System.out.println("cardType>>"+cardType.size());

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getCardType() :" + e);
        }
        return cardType;
    }

    public List<String> getCountry() {
        List<String> country = new ArrayList<String>();
        try {
            country = commonDAO.getCoutry();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getCountry() :" + e);
        }
        return country;
    }

    @Override
    public String getStateByCity(String city) {
        String state = "";
        try {
            state = commonDAO.getStateByCity(city);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getStateByCity() :", e);
        }
        return state;
    }

    @Override
    public Map<String, String> getICICICitiesByState(String state) {
        Map<String, String> citymap = new LinkedHashMap<>();
        List<String> cardList = new ArrayList<>();
        try {
            cardList = commonDAO.getICICICitiesByState(state);
            for (String city : cardList) {
                citymap.put(city, city);
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getICICICitiesByState() :", e);
        }
        return citymap;
    }

    @Override
    public String getICICIStateByCity(String city) {
        String state = "";
        try {
            state = commonDAO.getICICIStateByCity(city);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getICICIStateByCity() :", e);
        }
        return state;

    }

    @Override
    public List<String> getCities() {
        List<String> cities = new ArrayList<>();
        try {
            cities = commonDAO.getCities();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getCities() :" + e);
        }
        return cities;
    }

    @Override
    public List<String> getStates() {
        List<String> states = new ArrayList<>();
        try {
            states = commonDAO.getStates();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getStates() :" + e);
        }
        return states;
    }

    @Override
    public List<String> getICICICities() {
        List<String> cities = new ArrayList<>();
        try {
            cities = commonDAO.getICICICities();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getICICICities() :" + e);
        }
        return cities;

    }

    @Override
    public List<String> getICICIStates() {
        List<String> states = new ArrayList<>();
        try {
            states = commonDAO.getICICIStates();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getICICIStates() :" + e);
        }
        return states;
    }

    @Override
    public JsonBean getDetailsforPCN(String pcnNumber) {
        CcAmexApplicationStatus applicationformDetails = null;
        JsonBean webserviceBean = null;

        try {
            applicationformDetails = commonDAO.getApplicationFormByPCN(pcnNumber);

            if (null != applicationformDetails) {
                webserviceBean = new JsonBean();
                webserviceBean.setFirstName(applicationformDetails.getCcApplicationFormDetails().getFname());
                webserviceBean.setMiddleName(applicationformDetails.getCcApplicationFormDetails().getMname());
                webserviceBean.setLastName(applicationformDetails.getCcApplicationFormDetails().getLname());
                webserviceBean.setEmail(applicationformDetails.getCcApplicationFormDetails().getEmail());
                webserviceBean.setMobile(applicationformDetails.getCcApplicationFormDetails().getMobile());
                webserviceBean.setDateOfBirth(CommonUtils
                        .getDate(applicationformDetails.getCcApplicationFormDetails().getDateOfBirth(), "dd-MM-yyyy"));
                webserviceBean.setGender(applicationformDetails.getCcApplicationFormDetails().getGender());
                webserviceBean
                        .setTier(applicationformDetails.getCcApplicationFormDetails().getJetpriviligemembershipTier());
                webserviceBean.setJpnumber(
                        applicationformDetails.getCcApplicationFormDetails().getJetpriviligemembershipNumber());
                webserviceBean.setEduQualification(
                        applicationformDetails.getCcApplicationFormDetails().getEduQualification());
                webserviceBean.setAadharNumber(applicationformDetails.getCcApplicationFormDetails().getAadharNumber());
                webserviceBean.setPancard(applicationformDetails.getCcApplicationFormDetails().getPancard());
                webserviceBean.setMonthlyIncome(
                        applicationformDetails.getCcApplicationFormDetails().getMonthlyIncome().toPlainString());
                webserviceBean.setAddress1(applicationformDetails.getCcApplicationFormDetails().getAddress());
                webserviceBean.setAddress2(applicationformDetails.getCcApplicationFormDetails().getAddress2());
                webserviceBean.setAddress3(applicationformDetails.getCcApplicationFormDetails().getAddress3());
                webserviceBean.setCity(applicationformDetails.getCcApplicationFormDetails().getCity());
                webserviceBean.setState(applicationformDetails.getCcApplicationFormDetails().getState());
                webserviceBean.setPinCode(applicationformDetails.getCcApplicationFormDetails().getPinCode());
                webserviceBean.setPeraddresssameascurr(
                        applicationformDetails.getCcApplicationFormDetails().getPeraddresssameascurr());
                webserviceBean.setPermaddress1(applicationformDetails.getCcApplicationFormDetails().getPermaddress());
                webserviceBean.setPermaddress2(applicationformDetails.getCcApplicationFormDetails().getPermaddress2());
                webserviceBean.setPermaddress3(applicationformDetails.getCcApplicationFormDetails().getPermaddress3());
                webserviceBean.setPermCity(applicationformDetails.getCcApplicationFormDetails().getPCity());
                webserviceBean.setPermState(applicationformDetails.getCcApplicationFormDetails().getPState());
                webserviceBean.setPermPinCode(applicationformDetails.getCcApplicationFormDetails().getPPinCode());
                webserviceBean
                        .setEmploymentType(applicationformDetails.getCcApplicationFormDetails().getEmploymentType());
                webserviceBean.setCompanyName(applicationformDetails.getCcApplicationFormDetails().getCompanyName());
                webserviceBean.setOfficeAddress1(applicationformDetails.getCcApplicationFormDetails().getOAddress());
                webserviceBean.setOfficeAddress2(applicationformDetails.getCcApplicationFormDetails().getOAddress2());
                webserviceBean.setOfficeAddress3(applicationformDetails.getCcApplicationFormDetails().getOAddress3());
                webserviceBean.setOfficeCity(applicationformDetails.getCcApplicationFormDetails().getOCity());
                webserviceBean.setOfficeState(applicationformDetails.getCcApplicationFormDetails().getOState());
                webserviceBean.setOfficePhone(applicationformDetails.getCcApplicationFormDetails().getOPincode());
                webserviceBean.setPhone(applicationformDetails.getCcApplicationFormDetails().getPhone());
                webserviceBean.setStd(applicationformDetails.getCcApplicationFormDetails().getStd());
                webserviceBean.setOfficePhone(applicationformDetails.getCcApplicationFormDetails().getOPhone());
                webserviceBean.setOfficeStd(applicationformDetails.getCcApplicationFormDetails().getOStd());
                webserviceBean.setBankName(
                        applicationformDetails.getCcApplicationFormDetails().getCcBankPartner().getBankName());
                webserviceBean.setAppliedcardName(applicationformDetails.getCardName());
                webserviceBean.setPcnNumber(applicationformDetails.getPcnNumber());
                webserviceBean
                        .setApplicationStatus(applicationformDetails.getCcApplicationFormDetails().getAppStatus());
                webserviceBean.setApplicationDate(
                        CommonUtils.getDate(applicationformDetails.getCcApplicationFormDetails().getCreatedTime(), "dd-MM-yyyy"));
                webserviceBean.setReason(applicationformDetails.getCcApplicationFormDetails().getReason());
                webserviceBean.setDecisionStatus(applicationformDetails.getDecisionStatus());
                webserviceBean.setDecisionDate(CommonUtils.getDate(applicationformDetails.getDecisionDate(), "dd-MM-yyyy"));

            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getDetailsforPCN() :", e);
        }
        return webserviceBean;
    }

    @Override
    public List<JsonBean> getDetailsforJpNumber(String jpNumber) {
        List<JsonBean> webserviceBeanList = new ArrayList<>();
        List<CcAmexApplicationStatus> amexapplicationList = null;

        try {

            amexapplicationList = commonDAO.getApplicationForJPNumber(jpNumber);

            if (null != amexapplicationList) {

                for (CcAmexApplicationStatus applicationformDetails : amexapplicationList) {
                    JsonBean webserviceBean = new JsonBean();
                    webserviceBean.setBankPartnerURL(applicationformDetails.getCcApplicationFormDetails().getCcBankPartner().getBpUrl());
                    webserviceBean.setFirstName(applicationformDetails.getCcApplicationFormDetails().getFname());

                    webserviceBean.setMiddleName(applicationformDetails.getCcApplicationFormDetails().getMname());
                    webserviceBean.setLastName(applicationformDetails.getCcApplicationFormDetails().getLname());
                    webserviceBean.setEmail(applicationformDetails.getCcApplicationFormDetails().getEmail());
                    webserviceBean.setMobile(applicationformDetails.getCcApplicationFormDetails().getMobile());
                    webserviceBean.setDateOfBirth(
                            CommonUtils.getDate(applicationformDetails.getCcApplicationFormDetails().getDateOfBirth(), "dd-MM-yyyy"));
                    webserviceBean.setGender(applicationformDetails.getCcApplicationFormDetails().getGender());
                    webserviceBean.setTier(
                            applicationformDetails.getCcApplicationFormDetails().getJetpriviligemembershipTier());
                    webserviceBean.setJpnumber(
                            applicationformDetails.getCcApplicationFormDetails().getJetpriviligemembershipNumber());
                    webserviceBean.setEduQualification(
                            applicationformDetails.getCcApplicationFormDetails().getEduQualification());
                    webserviceBean
                            .setAadharNumber(applicationformDetails.getCcApplicationFormDetails().getAadharNumber());
                    webserviceBean.setPancard(applicationformDetails.getCcApplicationFormDetails().getPancard());
                    webserviceBean.setMonthlyIncome(
                            applicationformDetails.getCcApplicationFormDetails().getMonthlyIncome().toPlainString());
                    webserviceBean.setAddress1(applicationformDetails.getCcApplicationFormDetails().getAddress());
                    webserviceBean.setAddress2(applicationformDetails.getCcApplicationFormDetails().getAddress2());
                    webserviceBean.setAddress3(applicationformDetails.getCcApplicationFormDetails().getAddress3());
                    webserviceBean.setCity(applicationformDetails.getCcApplicationFormDetails().getCity());
                    webserviceBean.setState(applicationformDetails.getCcApplicationFormDetails().getState());
                    webserviceBean.setPinCode(applicationformDetails.getCcApplicationFormDetails().getPinCode());
                    webserviceBean.setPeraddresssameascurr(applicationformDetails.getCcApplicationFormDetails().getPeraddresssameascurr());
                    webserviceBean
                            .setPermaddress1(applicationformDetails.getCcApplicationFormDetails().getPermaddress());
                    webserviceBean
                            .setPermaddress2(applicationformDetails.getCcApplicationFormDetails().getPermaddress2());
                    webserviceBean
                            .setPermaddress3(applicationformDetails.getCcApplicationFormDetails().getPermaddress3());
                    webserviceBean.setPermCity(applicationformDetails.getCcApplicationFormDetails().getPCity());
                    webserviceBean.setPermState(applicationformDetails.getCcApplicationFormDetails().getPState());
                    webserviceBean.setPermPinCode(applicationformDetails.getCcApplicationFormDetails().getPPinCode());
                    webserviceBean.setEmploymentType(
                            applicationformDetails.getCcApplicationFormDetails().getEmploymentType());
                    webserviceBean
                            .setCompanyName(applicationformDetails.getCcApplicationFormDetails().getCompanyName());
                    webserviceBean
                            .setOfficeAddress1(applicationformDetails.getCcApplicationFormDetails().getOAddress());
                    webserviceBean
                            .setOfficeAddress2(applicationformDetails.getCcApplicationFormDetails().getOAddress2());
                    webserviceBean
                            .setOfficeAddress3(applicationformDetails.getCcApplicationFormDetails().getOAddress3());
                    webserviceBean.setOfficeCity(applicationformDetails.getCcApplicationFormDetails().getOCity());
                    webserviceBean.setOfficeState(applicationformDetails.getCcApplicationFormDetails().getOState());
                    webserviceBean.setOfficePhone(applicationformDetails.getCcApplicationFormDetails().getOPincode());
                    webserviceBean.setPhone(applicationformDetails.getCcApplicationFormDetails().getPhone());
                    webserviceBean.setStd(applicationformDetails.getCcApplicationFormDetails().getStd());
                    webserviceBean.setOfficePhone(applicationformDetails.getCcApplicationFormDetails().getOPhone());
                    webserviceBean.setOfficeStd(applicationformDetails.getCcApplicationFormDetails().getOStd());
                    webserviceBean.setBankName(
                            applicationformDetails.getCcApplicationFormDetails().getCcBankPartner().getBankName());
                    webserviceBean.setAppliedcardName(applicationformDetails.getCardName());
                    webserviceBean.setPcnNumber(applicationformDetails.getPcnNumber());
                    if (applicationformDetails.getCcApplicationFormDetails().getAppStatus()
                            .equalsIgnoreCase("Approved")) {
                        webserviceBean.setApplicationStatus(
                                applicationformDetails.getCcApplicationFormDetails().getAppStatus());
                    } else {
                        webserviceBean.setApplicationStatus("Pending");
                    }
                    webserviceBean.setApplicationDate(
                            CommonUtils.getDate(applicationformDetails.getCcApplicationFormDetails().getCreatedTime(), "dd-MM-yyyy"));
                    webserviceBean.setReason(applicationformDetails.getCcApplicationFormDetails().getReason());
                    if (applicationformDetails.getDecisionStatus().equalsIgnoreCase("Approved")) {
                        webserviceBean.setDecisionStatus(applicationformDetails.getDecisionStatus());
                    } else {
                        webserviceBean.setDecisionStatus("Pending");
                    }
                    if (null != applicationformDetails.getDecisionDate()) {
                        webserviceBean.setDecisionDate(CommonUtils.getDate(applicationformDetails.getDecisionDate(), "dd-MM-yyyy"));

                        webserviceBeanList.add(webserviceBean);
                    }

                }
            }
            List<CcIciciApplicationStatus> ccIciciApplicationStatus = null;
            ccIciciApplicationStatus = commonDAO.getICICIApplicationForJPNumber(jpNumber);
            if (null != ccIciciApplicationStatus) {

                for (CcIciciApplicationStatus applicationformDetails : ccIciciApplicationStatus) {
                    JsonBean webserviceBean1 = new JsonBean();
                    webserviceBean1.setBankPartnerURL(applicationformDetails.getCcApplicationFormDetails().getCcBankPartner().getBpUrl());
                    webserviceBean1.setFirstName(applicationformDetails.getCcApplicationFormDetails().getFname());
                    webserviceBean1.setMiddleName(applicationformDetails.getCcApplicationFormDetails().getMname());
                    webserviceBean1.setLastName(applicationformDetails.getCcApplicationFormDetails().getLname());
                    webserviceBean1.setEmail(applicationformDetails.getCcApplicationFormDetails().getEmail());
                    webserviceBean1.setMobile(applicationformDetails.getCcApplicationFormDetails().getMobile());
                    webserviceBean1.setDateOfBirth(
                            CommonUtils.getDate(applicationformDetails.getCcApplicationFormDetails().getDateOfBirth(), "dd-MM-yyyy"));
                    webserviceBean1.setGender(applicationformDetails.getCcApplicationFormDetails().getGender());
                    webserviceBean1.setTier(
                            applicationformDetails.getCcApplicationFormDetails().getJetpriviligemembershipTier());
                    webserviceBean1.setJpnumber(
                            applicationformDetails.getCcApplicationFormDetails().getJetpriviligemembershipNumber());
                    webserviceBean1.setEduQualification(
                            applicationformDetails.getCcApplicationFormDetails().getEduQualification());
                    webserviceBean1
                            .setAadharNumber(applicationformDetails.getCcApplicationFormDetails().getAadharNumber());
                    webserviceBean1.setPancard(applicationformDetails.getCcApplicationFormDetails().getPancard());
                    webserviceBean1.setMonthlyIncome(
                            applicationformDetails.getCcApplicationFormDetails().getMonthlyIncome().toPlainString());
                    webserviceBean1.setAddress1(applicationformDetails.getCcApplicationFormDetails().getAddress());
                    webserviceBean1.setAddress2(applicationformDetails.getCcApplicationFormDetails().getAddress2());
                    webserviceBean1.setAddress3(applicationformDetails.getCcApplicationFormDetails().getAddress3());
                    webserviceBean1.setCity(applicationformDetails.getCcApplicationFormDetails().getCity());
                    webserviceBean1.setState(applicationformDetails.getCcApplicationFormDetails().getState());
                    webserviceBean1.setPinCode(applicationformDetails.getCcApplicationFormDetails().getPinCode());
                    webserviceBean1.setPeraddresssameascurr(applicationformDetails.getCcApplicationFormDetails().getPeraddresssameascurr());
                    webserviceBean1.setPermaddress1(applicationformDetails.getCcApplicationFormDetails().getPermaddress());
                    webserviceBean1.setPermaddress2(applicationformDetails.getCcApplicationFormDetails().getPermaddress2());
                    webserviceBean1.setPermaddress3(applicationformDetails.getCcApplicationFormDetails().getPermaddress3());
                    webserviceBean1.setPermCity(applicationformDetails.getCcApplicationFormDetails().getPCity());
                    webserviceBean1.setPermState(applicationformDetails.getCcApplicationFormDetails().getPState());
                    webserviceBean1.setPermPinCode(applicationformDetails.getCcApplicationFormDetails().getPPinCode());
                    webserviceBean1.setEmploymentType(applicationformDetails.getCcApplicationFormDetails().getEmploymentType());
                    webserviceBean1.setCompanyName(applicationformDetails.getCcApplicationFormDetails().getCompanyName());
                    webserviceBean1.setOfficeAddress1(applicationformDetails.getCcApplicationFormDetails().getOAddress());
                    webserviceBean1.setOfficeAddress2(applicationformDetails.getCcApplicationFormDetails().getOAddress2());
                    webserviceBean1
                            .setOfficeAddress3(applicationformDetails.getCcApplicationFormDetails().getOAddress3());
                    webserviceBean1.setOfficeCity(applicationformDetails.getCcApplicationFormDetails().getOCity());
                    webserviceBean1.setOfficeState(applicationformDetails.getCcApplicationFormDetails().getOState());
                    webserviceBean1.setOfficePhone(applicationformDetails.getCcApplicationFormDetails().getOPincode());
                    webserviceBean1.setPhone(applicationformDetails.getCcApplicationFormDetails().getPhone());
                    webserviceBean1.setStd(applicationformDetails.getCcApplicationFormDetails().getStd());
                    webserviceBean1.setOfficePhone(applicationformDetails.getCcApplicationFormDetails().getOPhone());
                    webserviceBean1.setOfficeStd(applicationformDetails.getCcApplicationFormDetails().getOStd());
                    webserviceBean1.setBankName(
                            applicationformDetails.getCcApplicationFormDetails().getCcBankPartner().getBankName());
                    webserviceBean1.setAppliedcardName(applicationformDetails.getCardName());
                    webserviceBean1.setPcnNumber(applicationformDetails.getPcnNumber());
                    String appstatus = "";
                    appstatus = applicationformDetails.getCcApplicationFormDetails().getAppStatus();
                    if (null != appstatus) {
                        appstatus = applicationformDetails.getCcApplicationFormDetails().getAppStatus();
                    } else {
                        appstatus = "";
                    }

                    webserviceBean1.setApplicationStatus(appstatus);

                    webserviceBean1.setApplicationDate(CommonUtils.getDate(applicationformDetails.getCcApplicationFormDetails().getCreatedTime(), "dd-MM-yyyy"));
                    webserviceBean1.setReason(applicationformDetails.getCcApplicationFormDetails().getReason());

                    String str = applicationformDetails.getDecisionStatus();
                    if (null != str) {
                        str = applicationformDetails.getDecisionStatus();
                    } else {
                        str = "";
                    }

                    if (str.equalsIgnoreCase("")) {
                        webserviceBean1.setDecisionStatus(appstatus);
                    } else {
                        webserviceBean1.setDecisionStatus(str);
                    }

                    if (null != applicationformDetails.getDecisionDate()) {

                        webserviceBean1.setDecisionDate(CommonUtils.getDate(applicationformDetails.getDecisionDate(), "dd-MM-yyyy"));
                        webserviceBeanList.add(webserviceBean1);
                    }

                }
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getDetailsforJpNumber() :", e);
        }
        return webserviceBeanList;
    }

    @Override
    public List<JsonBean> getICICIDetailsforJpNumber(String jpNumber) {
        List<JsonBean> webserviceBeanList = new ArrayList<>();

        List<CcIciciApplicationStatus> ccIciciApplicationStatus = null;

        try {

            ccIciciApplicationStatus = commonDAO.getICICIApplicationForJPNumber(jpNumber);

            if (null != ccIciciApplicationStatus) {
                JsonBean webserviceBean1 = new JsonBean();
                for (CcIciciApplicationStatus applicationformDetails : ccIciciApplicationStatus) {

                    webserviceBean1.setFirstName(applicationformDetails.getCcApplicationFormDetails().getFname());
                    webserviceBean1.setMiddleName(applicationformDetails.getCcApplicationFormDetails().getMname());
                    webserviceBean1.setLastName(applicationformDetails.getCcApplicationFormDetails().getLname());
                    webserviceBean1.setEmail(applicationformDetails.getCcApplicationFormDetails().getEmail());
                    webserviceBean1.setMobile(applicationformDetails.getCcApplicationFormDetails().getMobile());
                    webserviceBean1.setDateOfBirth(
                            CommonUtils.getDate(applicationformDetails.getCcApplicationFormDetails().getDateOfBirth(), "dd-MM-yyyy"));
                    webserviceBean1.setGender(applicationformDetails.getCcApplicationFormDetails().getGender());
                    webserviceBean1.setTier(
                            applicationformDetails.getCcApplicationFormDetails().getJetpriviligemembershipTier());
                    webserviceBean1.setJpnumber(
                            applicationformDetails.getCcApplicationFormDetails().getJetpriviligemembershipNumber());
                    webserviceBean1.setEduQualification(
                            applicationformDetails.getCcApplicationFormDetails().getEduQualification());
                    webserviceBean1
                            .setAadharNumber(applicationformDetails.getCcApplicationFormDetails().getAadharNumber());
                    webserviceBean1.setPancard(applicationformDetails.getCcApplicationFormDetails().getPancard());
                    webserviceBean1.setMonthlyIncome(
                            applicationformDetails.getCcApplicationFormDetails().getMonthlyIncome().toPlainString());
                    webserviceBean1.setAddress1(applicationformDetails.getCcApplicationFormDetails().getAddress());
                    webserviceBean1.setAddress2(applicationformDetails.getCcApplicationFormDetails().getAddress2());
                    webserviceBean1.setAddress3(applicationformDetails.getCcApplicationFormDetails().getAddress3());
                    webserviceBean1.setCity(applicationformDetails.getCcApplicationFormDetails().getCity());
                    webserviceBean1.setState(applicationformDetails.getCcApplicationFormDetails().getState());
                    webserviceBean1.setPinCode(applicationformDetails.getCcApplicationFormDetails().getPinCode());
                    webserviceBean1.setPeraddresssameascurr(applicationformDetails.getCcApplicationFormDetails().getPeraddresssameascurr());
                    webserviceBean1.setPermaddress1(applicationformDetails.getCcApplicationFormDetails().getPermaddress());
                    webserviceBean1.setPermaddress2(applicationformDetails.getCcApplicationFormDetails().getPermaddress2());
                    webserviceBean1.setPermaddress3(applicationformDetails.getCcApplicationFormDetails().getPermaddress3());
                    webserviceBean1.setPermCity(applicationformDetails.getCcApplicationFormDetails().getPCity());
                    webserviceBean1.setPermState(applicationformDetails.getCcApplicationFormDetails().getPState());
                    webserviceBean1.setPermPinCode(applicationformDetails.getCcApplicationFormDetails().getPPinCode());
                    webserviceBean1.setEmploymentType(applicationformDetails.getCcApplicationFormDetails().getEmploymentType());
                    webserviceBean1.setCompanyName(applicationformDetails.getCcApplicationFormDetails().getCompanyName());
                    webserviceBean1.setOfficeAddress1(applicationformDetails.getCcApplicationFormDetails().getOAddress());
                    webserviceBean1.setOfficeAddress2(applicationformDetails.getCcApplicationFormDetails().getOAddress2());
                    webserviceBean1
                            .setOfficeAddress3(applicationformDetails.getCcApplicationFormDetails().getOAddress3());
                    webserviceBean1.setOfficeCity(applicationformDetails.getCcApplicationFormDetails().getOCity());
                    webserviceBean1.setOfficeState(applicationformDetails.getCcApplicationFormDetails().getOState());
                    webserviceBean1.setOfficePhone(applicationformDetails.getCcApplicationFormDetails().getOPincode());
                    webserviceBean1.setPhone(applicationformDetails.getCcApplicationFormDetails().getPhone());
                    webserviceBean1.setStd(applicationformDetails.getCcApplicationFormDetails().getStd());
                    webserviceBean1.setOfficePhone(applicationformDetails.getCcApplicationFormDetails().getOPhone());
                    webserviceBean1.setOfficeStd(applicationformDetails.getCcApplicationFormDetails().getOStd());
                    webserviceBean1.setBankName(
                            applicationformDetails.getCcApplicationFormDetails().getCcBankPartner().getBankName());
                    webserviceBean1.setAppliedcardName(applicationformDetails.getCardName());
                    webserviceBean1.setPcnNumber(applicationformDetails.getPcnNumber());
                    if (applicationformDetails.getCcApplicationFormDetails().getAppStatus()
                            .equalsIgnoreCase("Approved")) {
                        webserviceBean1.setApplicationStatus(applicationformDetails.getCcApplicationFormDetails().getAppStatus());
                    } else if (applicationformDetails.getCcApplicationFormDetails().getAppStatus()
                            .equalsIgnoreCase("Declined")) {
                        webserviceBean1.setApplicationStatus("Declined");
                    }
                    webserviceBean1.setApplicationDate(CommonUtils.getDate(applicationformDetails.getCcApplicationFormDetails().getCreatedTime(), "dd-MM-yyyy"));
                    webserviceBean1.setReason(applicationformDetails.getCcApplicationFormDetails().getReason());
                    if (applicationformDetails.getDecisionStatus().equalsIgnoreCase("Approved")) {
                        webserviceBean1.setDecisionStatus(applicationformDetails.getDecisionStatus());
                    } else {
                        webserviceBean1.setDecisionStatus("Rejected");
                    }
                    if (null != applicationformDetails.getDecisionDate()) {

                        webserviceBean1.setDecisionDate(CommonUtils.getDate(applicationformDetails.getDecisionDate(), "dd-MM-yyyy"));
                        webserviceBeanList.add(webserviceBean1);
                    }
                }
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getICICIDetailsforJpNumber() :", e);
        }
        return webserviceBeanList;
    }

    @Override
    public List<String> getGroupHeaders(int id) {
        List<String> groupHeadingList = new ArrayList<>();
        List<CcPageGroupLink> pageGroupLink;
        String groupHeader;

        try {
            pageGroupLink = commonDAO.getGroupHeaders(id);

            if (null != pageGroupLink) {
                for (CcPageGroupLink pgLink : pageGroupLink) {
                    if (pgLink.getCcGroupMaster().getGmStatus() == 1) {
                        groupHeader = pgLink.getCcGroupMaster().getGroupDescription();
                        System.out.println("groupHeader>>>>>>"+groupHeader);
                        groupHeadingList.add(groupHeader);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getGroupHeaders() :", e);
        }
        return groupHeadingList;
    }

//    @Override
//    public BannerBean getActiveBanner() {
//        BannerBean banner = new BannerBean();
//        CcBannerManagement ccbanner = null;
//        try {
//            ccbanner = commonDAO.getActiveBanner();
//            banner.setBannerAttachementPath(ccbanner.getBannerImagePath());
//
//        } catch (Exception e) {
//            logger.error("@@@@ Exception in CommonServiceImpl getActiveBanner() :", e);
//        }
//        return banner;
//    }
    
    
    @Override
    public List<BannerBean> getActiveBanner() {
        BannerBean banner = new BannerBean();
        List<CcBannerManagement> bannerMgmtist = new ArrayList<CcBannerManagement>();
        List<BannerBean> bannerList = new ArrayList<BannerBean>();
        //CcBannerManagement ccbanner = null;
        try {
        	bannerMgmtist = commonDAO.getActiveBanner();
            if(!bannerMgmtist.isEmpty()) {
            	for(CcBannerManagement ccBannerMgmt : bannerMgmtist) {
            		BannerBean bb = new BannerBean();
            		bb.setBannerAttachementPath(ccBannerMgmt.getBannerImagePath());
            		if(ccBannerMgmt.getBpNo()!=null)
            		{
            			bb.setBpNo(ccBannerMgmt.getBpNo());
            		}
            		if(ccBannerMgmt.getCpNo()!=null)
            		{
            			bb.setCpNo(ccBannerMgmt.getCpNo());
            		}
            		bannerList.add(bb);
            	}
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getActiveBanner() :", e);
        }
        return bannerList; 
    }
    

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String addUserInformation(CardDetailsBean cardDetailsBean) {
        String status = "";
        String gender = CommonUtils.nullSafe(cardDetailsBean.getGender());
        CcBankApplication ccBank = new CcBankApplication();
        CcCardProduct ccCardProduct = null;
        try {
            ccCardProduct = cardProductDAO.getCardProductById(cardDetailsBean.getCpNo());
            ccBank.setCcBankPartner(cardProductDAO.getBankPartnerById(ccCardProduct.getCcBankPartner().getBpNo()));
            ccBank.setCcCardProduct(ccCardProduct);
            ccBank.setTitle(CommonUtils.nullSafe(cardDetailsBean.getTitle()));
            ccBank.setFirstName(CommonUtils.nullSafe(cardDetailsBean.getFname()));
            ccBank.setMiddleName(CommonUtils.nullSafe(cardDetailsBean.getMname()));
            ccBank.setLastName(CommonUtils.nullSafe(cardDetailsBean.getLname()));
            ccBank.setMobile(CommonUtils.nullSafe(cardDetailsBean.getMobile()));
            ccBank.setEmail(CommonUtils.nullSafe(cardDetailsBean.getEmail()));
            ccBank.setDob(CommonUtils.setDateForAmexAppl(cardDetailsBean.getDob()));
            ccBank.setUtmSource(CommonUtils.nullSafe(cardDetailsBean.getUtmSource(), "Direct"));
            ccBank.setUtmMedium(CommonUtils.nullSafe(cardDetailsBean.getUtmMedium(), "NA"));
            ccBank.setUtmCampaign(CommonUtils.nullSafe(cardDetailsBean.getUtmCampaign(), "NA"));
            if ("0".equalsIgnoreCase(gender)) {
                gender = "Male";
            } else {
                gender = "Female";
            }

            ccBank.setGender(gender);
            ccBank.setJpnumber(CommonUtils.nullSafe(cardDetailsBean.getJpNumber()));
            ccBank.setMemberTier(cardDetailsBean.getJpTier());
            ccBank.setCreatedTime(new Date());
            ccBank.setCreatedBy(1);
            ccBank.setStatus((byte) 1);
            ccBank.setPid(cardDetailsBean.getPID());

            status = commonDAO.addBankApplication(ccBank);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl addUserInformation() :", e);
        }
        return status;
    }

    @Override
    public Map<String, String> getCityNames(String cityName) {
        Map<String, String> cityMap = new HashMap<>();
        List<String> cityList = null;
        try {
            cityList = commonDAO.getCityNames(cityName);
            if (!cityList.isEmpty()) {
                for (String city : cityList) {
                    cityMap.put(city, city);
                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getCityNames() :", e);
        }
        return cityMap;
    }

    @Override
    public Boolean checkExcludePincode(String pinCode, String cpNo) {
        Boolean isExcluded = false;
        try {
            CcCardProduct ccCardProduct = cardProductDAO.getCardProductById(Integer.parseInt(cpNo));
            isExcluded = commonDAO.checkExcludePincode(pinCode, ccCardProduct.getCcBankPartner().getBpNo());
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl checkExcludePincode() :", e);
        }
        return isExcluded;
    }

    @Override
    public Boolean checkICICIExcludePincode(String pinCode, String cpNo) {
        Boolean isExcluded = false;
        try {
            CcCardProduct ccCardProduct = cardProductDAO.getCardProductById(Integer.parseInt(cpNo));
            isExcluded = commonDAO.checkICICIExcludePincode(pinCode, ccCardProduct.getCcBankPartner().getBpNo());
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl checkICICIExcludePincode() :", e);
        }
        return isExcluded;
    }

    @Override
    public int getTimer(String timerName) {
        CcTimer ccTimer = null;
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        int milliSeconds = 0;
        try {
            ccTimer = commonDAO.getTimer();
            if (null != ccTimer) {
                if ("Timer for Application form update".equalsIgnoreCase(timerName)) {
                    days = null != ccTimer.getCtDays1() ? ccTimer.getCtDays1() : 0;
                    hours = null != ccTimer.getCtHours1() ? ccTimer.getCtHours1() : 0;
                    minutes = null != ccTimer.getCtMinutes1() ? ccTimer.getCtMinutes1() : 0;
                    seconds = null != ccTimer.getCtSeconds1() ? ccTimer.getCtSeconds1() : 0;
                } else if ("Timer for Notification".equalsIgnoreCase(timerName)) {
                    days = null != ccTimer.getCtDays2() ? ccTimer.getCtDays2() : 0;
                    hours = null != ccTimer.getCtHours2() ? ccTimer.getCtHours2() : 0;
                    minutes = null != ccTimer.getCtMinutes2() ? ccTimer.getCtMinutes2() : 0;
                    seconds = null != ccTimer.getCtSeconds2() ? ccTimer.getCtSeconds2() : 0;
                } else if ("Timer for Remainder Email".equalsIgnoreCase(timerName)) {
                    days = null != ccTimer.getCtDays3() ? ccTimer.getCtDays3() : 0;
                    hours = null != ccTimer.getCtHours3() ? ccTimer.getCtHours3() : 0;
                    minutes = null != ccTimer.getCtMinutes3() ? ccTimer.getCtMinutes3() : 0;
                    seconds = null != ccTimer.getCtSeconds3() ? ccTimer.getCtSeconds3() : 0;
                } else {
                    days = null != ccTimer.getCtDays4() ? ccTimer.getCtDays4() : 0;
                    hours = null != ccTimer.getCtHours4() ? ccTimer.getCtHours4() : 0;
                    minutes = null != ccTimer.getCtMinutes4() ? ccTimer.getCtMinutes4() : 0;
                    seconds = null != ccTimer.getCtSeconds4() ? ccTimer.getCtSeconds4() : 0;
                }
                milliSeconds = CommonUtils.getMilliSeconds(days, hours, minutes, seconds);
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getTimer() :", e);
        }
        return milliSeconds;
    }

    @Override
    public String getTermdAndCondition(int cpNo) {
        String termsAndCondition = "";
        try {
            termsAndCondition = commonDAO.getTermdAndCondition(cpNo);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getTermdAndCondition() :", e);
        }
        return termsAndCondition;
    }

    @Override
    public Map<String, String> getICICIValues(String typeName) {
        typeName = (null != typeName) ? typeName.trim() : "";

        List<CcEnumValues> enumList = new ArrayList<CcEnumValues>();
        Map<String, String> mapString = new LinkedHashMap<String, String>();
        try {
            enumList = commonDAO.getEnumValues(typeName);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getICICIValues() :", e);
        }
        for (CcEnumValues enumVal : enumList) {
            String tokens = enumVal.getValue();
            mapString.put(tokens, tokens);
        }

        return mapString;
    }

    @Override
    public List<String> getPincode() {
        List<String> pincode = new ArrayList<String>();
        try {
            pincode = commonDAO.getPincode();
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getPincode() :" + e);
        }
        return pincode;
    }

    @Override
    public String getUpgrdMessage() {
        List<String> message = new ArrayList<>();
        String upgrademesage = null;
        try {
            message = commonDAO.getUpgradeMessage();
            upgrademesage = message.get(0);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getUpgrdMessage() :" + e);
        }
        return upgrademesage;
    }

    @Override
    public String getUpgrdURL() {
        List<String> message = new ArrayList<>();
        String upgrademesage = null;
        try {
            message = commonDAO.getUpgradyUrl();
            upgrademesage = message.get(0);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getUpgrdURL() :" + e);
        }
        return upgrademesage;
    }

    @Override
    public String getAmexAppStatus(String jetpriviligemembershipNumber) {

        String appstatus = "";

        if (jetpriviligemembershipNumber.startsWith("00")) {
            jetpriviligemembershipNumber = jetpriviligemembershipNumber.substring(2);
        }
        appstatus = commonDAO.getAmexStatus(jetpriviligemembershipNumber);
        return appstatus;
    }

    @Override
    public String getRandomNumber(String jetpriviligemembershipNumber) {

        String randomNumber = "";

        if (jetpriviligemembershipNumber.startsWith("00")) {
            jetpriviligemembershipNumber = jetpriviligemembershipNumber.substring(2);
        }
        randomNumber = commonDAO.getRandomNumber(jetpriviligemembershipNumber);

        return randomNumber;
    }

    public List<String> getICICISelectedValues() {
        List<String> relationshipType = new ArrayList<String>();
        try {
            relationshipType = commonDAO.getICICISelectedValues();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getICICISelectedValues() :" + e);
        }
        return relationshipType;
    }

    @Override
    public List<String> getCompanyNames() {
        List<String> CompanyNames = new ArrayList<String>();
        try {
            CompanyNames = commonDAO.getCompany();

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getCompanyNames() :" + e);
        }
        return CompanyNames;
    }

    @Override
    public List<String> getempstatusSalaried(Integer bpno, Integer cpno) {
        List empstatussalaried = new ArrayList();

        try {
            empstatussalaried = commonDAO.getemploymentstatusSalaried(bpno, cpno);

        } catch (Exception e) {
            logger.info("Exception in getempstatusSalaried()" + e);
//                 System.out.println("exception"+e);
        }
        return empstatussalaried;
    }

    @Override
    public List<String> getempstatusSelfemp(Integer bpno, Integer cpno) {
        List empstatussalaried = new ArrayList();

        try {
            empstatussalaried = commonDAO.getemploymentstatusSelfemployed(bpno, cpno);

        } catch (Exception e) {
            logger.info("Exception in getempstatusSelfemp()" + e);
//                 System.out.println("exception"+e);
        }
        return empstatussalaried;
    }

    @Override
    public List getagecriteriaAgeabove(String bpNo, String cpNo, String EmpType, String City) {
        List agecriteria = null;

        try {
            agecriteria = commonDAO.getagecriteriaAgeabove(bpNo, cpNo, EmpType, City);

        } catch (Exception e) {
            logger.error("@@@ Exception at CommonServiceImpl getagecriteriaAgeabove(): " + e);
        }
        return agecriteria;
    }

    @Override
    public List getagecriteriaAgebelow(String bpNo, String cpNo, String EmpType, String City) {
        List agecriteria = null;

        try {
            agecriteria = commonDAO.getagecriteriaAgebelow(bpNo, cpNo, EmpType, City);

        } catch (Exception e) {
            logger.error("@@@ Exception at CommonServiceImpl getagecriteriaAgebelow(): " + e);

        }
        return agecriteria;
    }

    @Override
    public List<String> getCompanyNamesByLetter(String companyName) {
        List<String> CompanyNames = new ArrayList<String>();
        try {
            CompanyNames = commonDAO.getCompanyByLetter(companyName);

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getCompanyNamesByLetter() :" + e);
        }
        return CompanyNames;
    }

    @Override
    public Map<String, String> getAllCities() {
        Map<String, String> citymap = new LinkedHashMap<>();
        List<String> cardList = new ArrayList<>();
        try {
            cardList = commonDAO.getAllCities();
            for (String city : cardList) {
                citymap.put(city, city);
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getAllCities() :", e);
        }
        return citymap;
    }

    //new cr2
    @Override
    public List<CardDetailsBean> getBankwiseRsponseByStatus(String BpStatus, Integer bpNo) {

        List<CardDetailsBean> cardDetailsBeanList = new ArrayList<>();

        List<CcBankwiseResponseMapping> ccBankwiseResponseMapping = null;
        try {
//                System.out.println("BpStatus"+BpStatus);
//                System.out.println("bpNo"+bpNo);
            Integer id = commonDAO.getBankStatusIdByName(BpStatus);
//                     System.out.println("id of status"+id);
            ccBankwiseResponseMapping = commonDAO.getResponseByStatus(String.valueOf(id), bpNo);
            CardDetailsBean cardDetailsBean = new CardDetailsBean();
            if (null != ccBankwiseResponseMapping) {

                for (CcBankwiseResponseMapping applicationformDetails : ccBankwiseResponseMapping) {
                    cardDetailsBean.setBpSatatuTitle(applicationformDetails.getTitle());
                    cardDetailsBean.setMessageContent(applicationformDetails.getDescription());
                    cardDetailsBeanList.add(cardDetailsBean);
                }

            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getBankwiseRsponseByStatus() :" + e);
        }
        return cardDetailsBeanList;
    }

    @Override
    public int getBankStatusIdByName(String bsStatus) {
        int cpNo = 0;
        try {
            cpNo = commonDAO.getBankStatusIdByName(bsStatus);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getBankStatusIdByName() :", e);
        }
        return cpNo;
    }

    //cr 2 milestone 2
    @Override
    public int getDataForAgeRangeSalaried(String bpNo, String cpNo) {
        List l1 = null, l2 = null;
        int temp = 0;
        try {
            l1 = commonDAO.getagecriteriaAgebelow(bpNo, cpNo, "Salaried", "");
            l2 = commonDAO.getagecriteriaAgebelow(bpNo, cpNo, "Self Employed-Professional", "");
//                   l2=commonDAO.getagecriteriaAgeabove(bpNo, cpNo, "Salaried", "" );
//                    System.out.println("salaried---below-----"+l1.get(0)+"- self below emploed--"+l2.get(0));

            if ((int) l1.get(0) > (int) l2.get(0)) {
                temp = (int) l1.get(0);
            } else {
                temp = (int) l2.get(0);
            }
//                  System.out.println("temp below"+temp);

        } catch (Exception e) {
            logger.error("error at getDataForAgeRangeSalaried() " + e);
        }
        return temp;
    }

    @Override
    public int getDataForAgeRangeSelf(String bpNo, String cpNo) {
        List l1 = null, l2 = null;
        int temp = 0;
        try {
            l1 = commonDAO.getagecriteriaAgeabove(bpNo, cpNo, "Salaried", "");
            l2 = commonDAO.getagecriteriaAgeabove(bpNo, cpNo, "Self Employed-Professional", "");
//                   System.out.println("salaried-------"+l1.get(0)+"--self-"+l2.get(0));

            if ((int) l1.get(0) < (int) l2.get(0)) {
                temp = (int) l1.get(0);
            } else {
                temp = (int) l2.get(0);
            }
//                  System.out.println("temp above" +temp);
        } catch (Exception e) {
            logger.error("Exception at getDataForAgeRangeSelf()" + e);

        }
        return temp;
    }

    //cr3
    @Override
    public void buildSMSForApplication(String pcnNumber, String campaignId, String emailId, String firstName, String lastName, String bankNumber, String smsName, String cardName, String days, String mobileNo, String applicantName, String applicationNo, String jpNumber, String initdate, String bankdetail, String appName) {
        String msg = "", bankName = "";
        try {
            CcBankPartner ccBankPartner = cardProductDAO.getBankPartnerById(Integer.parseInt(bankNumber));
            if (ccBankPartner != null) {
                bankName = ccBankPartner.getBankName();
            }
            logger.info("smsName *****" + smsName + " ****bank Number *****" + bankNumber);
            String message = commonDAO.getSMSContent(smsName, bankNumber).replaceAll("\\<[^>]*>", "");
//		StringBuilder cardName 	= new StringBuilder(appURL);
            logger.info("message  " + message);

            logger.info("mobile" + mobileNo + " username" + applicantName + " applno" + applicationNo + " jpnumber" + jpNumber + " initdate" + initdate + " bankdetail" + bankdetail);
            logger.info("smsName" + smsName + " appName" + appName);
            if (cardName.contains("<sup>")) {
                cardName = cardName.replaceAll("<sup>", "");
                cardName = cardName.replaceAll("</sup>", "");
            }
            if (cardName.contains("&reg")) {
                cardName = cardName.replaceAll("&reg;", "");
            }

            pcnNumber = pcnNumber.trim();

            if (!pcnNumber.equalsIgnoreCase("") || !pcnNumber.isEmpty()) {
                applicationNo = pcnNumber;

//                        saveColumns.put("Application_Number", pcnNumber);
            }
//                        else{
//                            System.out.println("inside else");
////                        saveColumns.put("Application_Number", applicationNumber);
//                        }

            if (message.contains("&lt;Name&gt;")) {
                message = message.replace("&lt;Name&gt;", applicantName);
            }
            if (message.contains("&lt;Card name&gt;")) {
                message = message.replace("&lt;Card name&gt;", cardName);
            }

            if (message.contains("&lt;Application number&gt;")) {
                message = message.replace("&lt;Application number&gt;", applicationNo);
            }
            if (message.contains("&lt;Bank Number&gt;")) {
                message = message.replace("&lt;Bank Number&gt;", bankdetail);
            }
            if (message.contains("&lt;Days&gt;")) {
                message = message.replace("&lt;Days&gt;", days);
            }

            msg = URLEncoder.encode(message.trim(), "utf-8");

            if (msg != "") {
            	 //String respMesg = SmsService.sendSms(mobileNo, smsUserName, smsPassword, smsSendby, msg, smsUrl, (SMSMSGID + "|" + smsName + "|" + applicationNo).replace(" ", "_"), simplicaUserName, simplicaPassword);
//               if (respMesg.equalsIgnoreCase("OK")) {
//                   silverpopCallForSms(appName, jpNumber, mobileNo, msg, initdate);
//               }
            }
            //silverpopCallForEmail(pcnNumber, campaignId, emailId, smsName, firstName, lastName, cardName, applicationNo, bankName, bankdetail, days);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl at buildSMSForApplication() :", e);
        }
    }

    public void silverpopCallForSms(String appName, String jpNumber, String mobileNo, String smsText, String TransactionDate) {
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            DateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");

            Date date1 = new Date();
            String strDate1 = formatter1.format(date1);
            List<ColumnElementType> columnElementarray = new ArrayList<>();

            ColumnElementType colEleMenType1 = new ColumnElementType();
            colEleMenType1.setNAME("App_Name");
            colEleMenType1.setVALUE(appName);

            ColumnElementType colEleMenType2 = new ColumnElementType();
            colEleMenType2.setNAME("JP_Number");
            colEleMenType2.setVALUE(jpNumber);

            ColumnElementType colEleMenType3 = new ColumnElementType();
            colEleMenType3.setNAME("MobileNumber");
            colEleMenType3.setVALUE(mobileNo);

            ColumnElementType colEleMenType4 = new ColumnElementType();
            colEleMenType4.setNAME("SMS_Text");
            colEleMenType4.setVALUE(smsText);

            ColumnElementType colEleMenType5 = new ColumnElementType();
            colEleMenType5.setNAME("Trans_Date");
//			Date date = new Date();
//			String strDate = formatter.format(date);
            colEleMenType5.setVALUE(strDate1);

            columnElementarray.add(colEleMenType1);
            columnElementarray.add(colEleMenType2);
            columnElementarray.add(colEleMenType3);
            columnElementarray.add(colEleMenType4);
            columnElementarray.add(colEleMenType5);
//			columnElementarray.add(colEleMenType9);
            ColumnElementType[] elementArray = new ColumnElementType[columnElementarray.size()];
            elementArray = columnElementarray.toArray(elementArray);
            logger.info("SMS silver pop calling-->User mobile-->" + mobileNo + " user first name-->" + appName + " user sms--" + smsText + " jp number" + jpNumber + " Trans date" + TransactionDate);

            SilverPopUtil.silverPopAPI(elementArray, silverPopuserName, silverPoppassword, smsid);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl silverpopCallForSms() :", e);
        }
    }

    @Override
    public String getOtpForValidate(String mobileNo, String bankNumber, String formNumber,String otpTransactionId) {
        String otp = "";
        try {
            otp = commonDAO.getOtpForValidate(mobileNo, bankNumber, formNumber, otpTransactionId);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getOtpForValidate() :", e);

        }

        return otp;
    }


    @Override
    public String UpdateApplicationForOtp(String mobileNo, String bankNumber, String formNumber, String otp, String applicationId) {
        String otpValue = "";
        int otpid = 0;
        CcOTP ccOTP = null;
        String status = "";
        List<CcOTP> otpDetail = null;
        try {
            otpDetail = commonDAO.getOtpId(mobileNo, bankNumber, formNumber, otp);
            if (otpDetail != null) {
                for (CcOTP ccOTP1 : otpDetail) {
                    otpid = ccOTP1.getOtpNo();
                }
            }
            if (otpid != 0) {
                ccOTP = commonDAO.getOTPById(otpid);
                if (null != ccOTP) {
                    ccOTP.setApplicationId(Integer.parseInt(applicationId));
                    status = commonDAO.UpdateApplicationForOtp(ccOTP);
                    return status;

                }

            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl getOtpForValidate() :", e);

        }

        return status;
    }

    public void silverpopCallForEmail(String pcnNumber, String campaignid, String userEmail, String statusName, String firstName, String lastName, String cardName, String applicationNumber, String bankName, String bankNumber, String noOfDays) {
        try {

            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
//                    logger.info("formactive silver pop call for "+amexBean.getFname()+" Mobile no:"+amexBean.getMobile());
//            TransactRequestType transReqType = new TransactRequestType();
//          transReqType.setCampaign_id(campaignid);//campaign id
//          transReqType.setTransaction_id(transReqType.getCampaign_id());
//          transReqType.setShow_all_send_details(true);
            HashMap<String, String> saveColumns = new HashMap();
            saveColumns.put("First_Name", firstName);
            saveColumns.put("Last_Name", lastName);
//			if(cardName.contains("<") == true){
//                            System.out.println("in if");
//                        cardName = cardName.replaceAll("<sup>&reg;</sup>", "");
//                        saveColumns.put("Card_Name", cardName);
//                        
//                            System.out.println("cardName"+cardName);
//                        }
//                        else{
//                            System.out.println("in else");
//                        saveColumns.put("Card_Name", cardName);
//                        
//                        }

            saveColumns.put("Card_Name", cardName);

//                        System.out.println("applicationNumber at email"+applicationNumber);
//                        pcnNumber=pcnNumber.trim();
//                        
//                        System.out.println("pcnNumber"+pcnNumber);
//                        
//                        
//                        if(!pcnNumber.equalsIgnoreCase("") || ! pcnNumber.isEmpty())
//                        {
//                            System.out.println("inside if");
//                        saveColumns.put("Application_Number", pcnNumber);
//                        }else{
//                            System.out.println("inside else");
            saveColumns.put("Application_Number", applicationNumber);
//                        }

            if (!statusName.equalsIgnoreCase("Try Again")) {
                saveColumns.put("Bank_Name", bankName);
            }
            if (!statusName.equalsIgnoreCase("Try Again") || !statusName.equalsIgnoreCase("Reject")) {
                saveColumns.put("Bank_Number", bankNumber);
                saveColumns.put("Number_Of_Days", noOfDays);

            }

            saveColumns.put("Email", userEmail);
            Date date = new Date();
            String strDate = formatter.format(date);
//			saveColumns.put("Application_Date", strDate);
//			saveColumns.put("URL", amexBean.getRandomNumber());

//            transReqType.setColumns(saveColumns);
//          //Enter the email id to which the mail is to be send
//          transReqType.setEmail(userEmail);
//          transReqType.setBody_type("HTML");

            SilverPopApiCards.callSilverPopApi(url, clientId, clientSecret, refreshToken);
            SendGridMail sg = new SendGridMail();
            sg.sendMail(sendGridUrl, sendGridKey, saveColumns, campaignid, userEmail, applicationNumber, simplicaUserName, simplicaPassword);
            logger.info("SMS silver pop calling at Email Transaction-->User email-->" + userEmail);
//			SilverPopUtil.silverPopAPI(elementArray, silverPopuserName, silverPoppassword, smsid);
        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl silverpopCallForSms() :", e);
        }
    }

    @Override
    public String getPromoCodeError(String errorCode) {

        String errorMsg = "";
        System.out.println("service impl" + errorCode);
        errorMsg = commonDAO.getPromoCodeError(errorCode);

        return errorMsg;
    }

    @Override
    public String getEnrollCodeError(String errorCode) {

        String errorMsg = "";
        System.out.println("service impl" + errorCode);
        errorMsg = commonDAO.getEnrollCodeError(errorCode);

        return errorMsg;
    }

//sprint 56
    @Override
    public List<CcPageMaster> getPageMasterDetail(int id) {

        List<CcPageMaster> pageMasterList = null;
        try {
            pageMasterList = commonDAO.getPageMasterDetail(id);
        } catch (Exception e) {
            logger.error("Exception at CommonServiceImpl getPageMasterDetail(): ", e);
        }
        return pageMasterList;
    }
    @Override
    public String checkCardVarientRandomNo(Integer cpNo, String randomNumber) {
        List<CcCardVarientURL> cardvarientlist = new ArrayList();
        String jpnumber = "";
        try {
            cardvarientlist = commonDAO.checkCardVarientRandomNo(cpNo, randomNumber);
            if(cardvarientlist != null){
            for(CcCardVarientURL c:cardvarientlist){
           jpnumber=c.getJpNumber();
            }
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl checkCardVarientRandomNo() :", e);

        }
        return jpnumber;

    }
    @Override
    public String checkFormStatus(String randomNumber) {
        List<CcBeApplicationFormDetails> ccBeApplicationFormDetailslist = new ArrayList();
        String formstatus = "";
        try {
            System.out.println("randomNumber==>"+randomNumber);
            ccBeApplicationFormDetailslist = commonDAO.checkFormStatus(randomNumber);
            if(ccBeApplicationFormDetailslist != null){
            for(CcBeApplicationFormDetails c:ccBeApplicationFormDetailslist){
           formstatus=c.getFormStatus();
                System.out.println("formstatus"+formstatus);
            }
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in CommonServiceImpl checkCardVarientRandomNo() :", e);

        }
        return formstatus;

    }
}
