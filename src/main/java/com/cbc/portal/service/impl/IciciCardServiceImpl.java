/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.cbc.portal.DAO.AmexCardDAO;
import com.cbc.portal.DAO.CardProductDAO;
import com.cbc.portal.DAO.IciciCardDAO;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.beans.IciciBean;
import com.cbc.portal.entity.CcApplicationFormDetails;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcIciciApplicationStatus;
import com.cbc.portal.entity.CcMapOffers;
import com.cbc.portal.entity.CcOffers;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.ICICIEncryptDetail;
import com.cbc.portal.utils.IciciWebService;
import com.cbc.portal.utils.SendGridMail;
import com.google.gson.Gson;

/**
 *
 * @author Aravind E
 */
@Component
@Transactional(readOnly = true)
public class IciciCardServiceImpl implements com.cbc.portal.service.IciciCardService {

    private Logger logger = Logger.getLogger(IciciCardServiceImpl.class.getName());
    @Value("${application.trasunion.rest.url}")
    private String transurl;
    @Value("${application.trasunion.username}")
    private String username;
    @Value("${application.trasunion.password}")
    private String password;
    @Value("${application.trasunion.newapplication.url}")
    private String newApplcationUrl;

    @Value("${application.icici.api.url}")
    private String iciciURL;

    @Value("${application.icici.channel_Type}")
    private String channelType;

    @Value("${application.icici.api.username}")
    private String iciciUsername;

    @Value("${application.icici.api.password}")
    private String iciciPassword;

    @Value("${application.icici.api.companyname}")
    private String apiCompanyName;
    @Value("${application.icici.api.productname}")
    private String productName;
    @Value("${application.icici.api.ModeOfLead}")
    private String modeOfLead;

    @Value("${application.icici.api.authorization}")
    private String authorization;

    @Value("${application.icici.api.contenttype}")
    private String contentType;
    @Value("${application.silverPopApi.url}")
    private String url;

    @Value("${application.silverPopApi.clientID}")
    private String clientId;

    @Value("${application.silverPopApi.clientSecret}")
    private String clientSecret;

    @Value("${application.silverPopApi.refreshToken}")
    private String refreshToken;
    @Value("${application.silverpop.inactiveIcici.campaignid}")
    private long inactiveid;

    @Value("${application.silverpop.icici.agentMailId}")
    private String agentMailId;
    
    @Value("${application.sendGridApi.url}")
    private String sendGridUrl;

    @Value("${application.sendGridApi.key}")
    private String sendGridKey;

    @Value("${application.sendGrid.inactiveIcici.campaignid}")
    private String sendGridInactiveid;
    
    @Value("${application.simplica.username}")
    private String simplicaUserName;
    
    @Value("${application.simplica.password}")
    private String simplicaPassword;

//    @Value("${application.trasunion.address3}")
//    private String address3;
    @Autowired
    private WebServiceTemplate webServiceTemplate;

    @Autowired
    private IciciCardDAO iciciCardDAO;

    @Autowired
    private CardProductDAO cardProductDAO;

    @Autowired
    private CommonService commonService;
    @Autowired
    private AmexCardDAO amexCardDAO;

    private String erroMsg = "";
    private String totalexperience = "";
    
//    @Value("${application.pii.encryption.strSoapURL1}")
//   	String strSoapURL1;
//   	@Value("${application.pii.encrypt.strSOAPAction1}")
//   	String soapActEnc;

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public Map<String, String> applyIciciCardWebService(IciciBean bean, String strSoapURL1, String soapActEnc) {
        String res = "", iciciresponse = "", applicatioID = "", decision = "", reason = "", gender = "", dob = "", fullName = "", iciciId = "", result = "",
                eligibility = "", description = "", device = "", iciciresp = "";
        Date date1 = null;
        BigDecimal income;
        String icicinumber = "";
        if (bean.getICICIBankRelationShipNo() != null) {
            icicinumber = bean.getICICIBankRelationShipNo();
        }
        HashMap allResponse = new HashMap();
        bean.setICICIBankRelationShipNo(icicinumber);

        IciciWebService iciciWebService = new IciciWebService();
        fullName = bean.getFname() + bean.getLname();
        gender = bean.getGender();
        if (gender.equals("0")) {
            gender = "Male";
        } else {
            gender = "FeMale";
        }
        int salary = bean.getMonthlyIncome().intValue();
        salary = salary / 12;

        if (bean.getEmploymentType().equalsIgnoreCase("Self Employed-Professional") || bean.getEmploymentType().equalsIgnoreCase("Self Employed-Business")) {

            income = bean.getMonthlyIncome();
        } else {
            income = new BigDecimal(salary);
        }
        dob = bean.getDateOfBirth();
        dob = (null != dob) ? dob.trim() : "";
        // Add Log Code
        Gson gson = new Gson();
        String userJsonData = gson.toJson(bean);
        Map<String, String> allApiResponse = new HashMap<String, String>();
//        logger.info("ICICI FORM DETAILS : " + userJsonData);
//        logger.info("ICICI FORM DETAILS Enc : " + getEncValue(userJsonData, strSoapURL1, soapActEnc));
        
        try {
            System.out.println("bean.getJetpriviligemembershipTier()  " + bean.getJetpriviligemembershipTier());
            System.out.println("******"+!"GOLD".equalsIgnoreCase(bean.getJetpriviligemembershipTier()));
            System.out.println("*$*****"+!"PLATINUM".equalsIgnoreCase(bean.getJetpriviligemembershipTier()));
            
            if (!"GOLD".equalsIgnoreCase(bean.getJetpriviligemembershipTier()) && !"PLATINUM".equalsIgnoreCase(bean.getJetpriviligemembershipTier())) {
                System.out.println("inside if condition ");
                res = iciciWebService.getTransUnionDetails(username, password, transurl, newApplcationUrl, bean, channelType, strSoapURL1, soapActEnc);//address3
//           res="{\"$id\":\"1\",\"ApplicationId\":null,\"Decision\":null,\"ErrorMessage\":\"Sorry! we are unable to process your application. Please contact administrator.\",\"Reason\":null}";
                ObjectMapper obmentityuser = new ObjectMapper();
                logger.info("Transunion Response@@@@@@@" + res);
                LinkedHashMap<String, String> tabledata_HM = new LinkedHashMap<String, String>();

                tabledata_HM = (LinkedHashMap<String, String>) obmentityuser.readValue(res, Map.class);

                for (Map.Entry<String, String> entry : tabledata_HM.entrySet()) {

                    if (entry.getKey().equals("ApplicationId")) {

                        applicatioID = entry.getValue();
                        if (applicatioID == null) {
                            applicatioID = " ";
                        }
                    }
                    if (entry.getKey().equals("Decision")) {
                        decision = entry.getValue();

                        if (decision == null) {
                            decision = " ";
                        }
                    }
                    if (entry.getKey().equals("Reason")) {
                        reason = entry.getValue();
                    }
                    if (entry.getKey().equals("ErrorMessage")) {
                        erroMsg = entry.getValue();
                    }

                }
            } else {
                System.out.println("at decision pending else=====================>");
                decision = "Pending";
            }

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(IciciCardServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        dob = bean.getDateOfBirth();
        dob = (null != dob) ? dob.trim() : "";
        CcApplicationFormDetails ccApplicationForm = new CcApplicationFormDetails();
        ccApplicationForm.setFname(bean.getFname());
        ccApplicationForm.setMname(bean.getMname());
        ccApplicationForm.setLname(bean.getLname());
        ccApplicationForm.setEmail(bean.getEmail());
        ccApplicationForm.setMobile(bean.getMobile());
        ccApplicationForm.setDateOfBirth(CommonUtils.ICICIDate(dob));
        ccApplicationForm.setGender(gender);
        ccApplicationForm.setEduQualification(commonService.getAmexEnumValues("Qualification", bean.getEduQualification()));
        ccApplicationForm.setAadharNumber(bean.getAadharNumber());
        ccApplicationForm.setPancard(bean.getPancard());
        ccApplicationForm.setMonthlyIncome(bean.getMonthlyIncome());
        ccApplicationForm.setAddress(bean.getAddress());
        ccApplicationForm.setAddress2(bean.getAddress2());
        ccApplicationForm.setAddress3(bean.getAddress3());
        ccApplicationForm.setCity(bean.getCity());
        ccApplicationForm.setState(bean.getState1());
        ccApplicationForm.setPinCode(bean.getPinCode());
        ccApplicationForm.setPeraddresssameascurr(bean.getPeraddresssameascurr());
        ccApplicationForm.setPermaddress(bean.getPermaddress());
        ccApplicationForm.setPermaddress2(bean.getPermaddress2());
        ccApplicationForm.setPermaddress3(bean.getPermaddress3());
        ccApplicationForm.setPCity(bean.getPCity());
        ccApplicationForm.setPState(bean.getPState());
        ccApplicationForm.setPPinCode(bean.getPpinCode());
        ccApplicationForm.setOStd(bean.getOStd());
        ccApplicationForm.setOPhone(bean.getOPhone());
        ccApplicationForm.setJetpriviligemember(bean.getJetpriviligemember());
        ccApplicationForm.setJetpriviligemembershipNumber(bean.getJetpriviligemembershipNumber());
        ccApplicationForm.setJetpriviligemembershipTier(bean.getJetpriviligemembershipTier());
        ccApplicationForm.setEmploymentType(bean.getEmploymentType());
        ccApplicationForm.setCompanyName(bean.getCompanyName());
        ccApplicationForm.setOAddress(bean.getOAddress());
        ccApplicationForm.setOAddress2(bean.getOAddress2());
        ccApplicationForm.setOAddress3(bean.getOAddress3());
        ccApplicationForm.setOCity(bean.getOCity());
        ccApplicationForm.setOState(bean.getOState());
        ccApplicationForm.setOPincode(bean.getOPincode());
        ccApplicationForm.setPhone(bean.getPhone());
        ccApplicationForm.setStd(bean.getStd());
        logger.info("decision======1>" + decision);

        ccApplicationForm.setAppStatus(decision);
        ccApplicationForm.setITRStatus(bean.getITRStaus());
        if (0 != bean.getJetpriviligemembershipNumber().length()) {

            ccApplicationForm.setJetpriviligemember("Y");
        }
//        String exp=bean.getTotalExperience();
//        if(exp.equalsIgnoreCase("")){
//
//            exp="0.0";
//        }

        String totalexp = bean.getTotalExperience();

        String mo = null, years = null;
        if ("".equals(totalexp)) {
            mo = "0";
            years = "0";
            totalexperience = years + "." + mo;
        } else if (totalexp.contains("years") || totalexp.contains("year")) {

            totalexp = totalexp.substring(0, totalexp.indexOf("m")).trim();
            if (!totalexp.contains("s")) {
                totalexp = totalexp.replace(" ", "");
//             String months=totalexp.trim().substring(0, totalexp.indexOf("m"));
                String months = totalexp;

                mo = months.substring(months.indexOf("r") + 1).trim();
                years = months.substring(0, months.indexOf("year"));
                totalexperience = years + "." + mo;

            }
            if (totalexp.contains("s")) {

                totalexp = totalexp.replace(" ", "");
//             String months=totalexp.trim().substring(0, totalexp.indexOf("m"));
                String months = totalexp;
                mo = months.substring(months.indexOf("s") + 1).trim();
                years = months.substring(0, months.indexOf("years"));
                totalexperience = years + "." + mo;

            }

        }

        if ((Integer.parseInt(mo)) >= 7) {
            int y = (Integer.parseInt(years));
            int totyear = y + 1;
            totalexperience = String.valueOf(totyear);
        } else {
            totalexperience = years;
        }

        ccApplicationForm.setTotalexp(totalexperience);
        ccApplicationForm.setIciciRelationShipNo(bean.getICICIBankRelationShipNo());
        ccApplicationForm.setIciciBakRealtion(bean.getIciciBankRelationship());
        ccApplicationForm.setSalaryBankWithOtherBank(bean.getSalaryBankWithOtherBank());
        ccApplicationForm.setSalaryAccountOpened(bean.getSalaryAccountOpened());
        //-------new set--------
        ccApplicationForm.setUtmSource(CommonUtils.nullSafe(bean.getUtmSource(), "Direct"));
        ccApplicationForm.setUtmMedium(CommonUtils.nullSafe(bean.getUtmMedium(), "NA"));
        ccApplicationForm.setUtmCampaign(CommonUtils.nullSafe(bean.getUtmCampaign(), "NA"));
        //-----end-------------
        ccApplicationForm.setChannelType(channelType);
        ccApplicationForm.setCreatedTime(new Date());
        ccApplicationForm.setCreatedBy(1);
        ccApplicationForm.setStatus((byte) 1);
        ccApplicationForm.setPid(CommonUtils.nullSafe(bean.getPid()));//new pid
        ccApplicationForm.setCcBankPartner(cardProductDAO.getCardProductById(bean.getCpNo()).getCcBankPartner());
        ccApplicationForm.setCcCardProduct(cardProductDAO.getCardProductById(bean.getCpNo()));
        ccApplicationForm.setPromocode(CommonUtils.nullSafe(bean.getPromocode(), ""));
        ccApplicationForm.setApplicationInitiatedBy(CommonUtils.nullSafe(bean.getInitiatedBy(), "Self"));
        ccApplicationForm.setAgentId(CommonUtils.nullSafe(bean.getAgentId(), "NA"));
        ccApplicationForm.setFormStatus("Completed");

        Integer id = null;
        id = iciciCardDAO.submitICICIApplicationForm(ccApplicationForm);
        logger.info("id:" + id);
        bean.setAppFormNo(id);//cr3
        logger.info("=====>" + bean.getOtpVal());
        if (0 != id) {
             String statusOfOTP=commonService.UpdateApplicationForOtp(String.valueOf(bean.getMobile()), "2", bean.getFormNumber(), bean.getOtpTransactionId(),String.valueOf(id));
            logger.info("decision----------->" + decision);
            bean.setAppStatus(decision);

            saveOrUpdateFormData(bean, id);

            Integer statusID = null;
            CcIciciApplicationStatus ccIciciApplicationStatus = new CcIciciApplicationStatus();
            ccApplicationForm = null;
            ccApplicationForm = iciciCardDAO.getApplicationFormById(id);
            if (null != ccApplicationForm) {
                ccIciciApplicationStatus.setCcApplicationFormDetails(ccApplicationForm);
                ccIciciApplicationStatus.setCardName(ccApplicationForm.getCcCardProduct().getCardName());
                ccIciciApplicationStatus.setJpNumber(ccApplicationForm.getJetpriviligemembershipNumber());
                ccIciciApplicationStatus.setPcnNumber(applicatioID);
                ccIciciApplicationStatus.setWsRequest(iciciWebService.getContent());
                ccIciciApplicationStatus.setWsResponse(res);
                ccIciciApplicationStatus.setTransUnionReason(reason);
                ccIciciApplicationStatus.setPid(CommonUtils.nullSafe(ccApplicationForm.getPid()));//new pid
                ccIciciApplicationStatus.setTransUnionErrorMsg(erroMsg);

                if ("Approved".equalsIgnoreCase(decision)
                        || "Declined".equalsIgnoreCase(decision) || "Referred".equalsIgnoreCase(decision)) {
                    ccIciciApplicationStatus.setTransDecisionStatus(decision);
                } else {
                    ccIciciApplicationStatus.setDecisionStatus("Rejected");
                }
                ccIciciApplicationStatus.setDecisionDate(new Date());
                ccIciciApplicationStatus.setCreatedTime(new Date());
                ccIciciApplicationStatus.setCreatedBy(1);
                ccIciciApplicationStatus.setStatus((byte) 1);

//                if (decision.equalsIgnoreCase("Approved") || decision.equalsIgnoreCase("Referred")) {
                    CcCardProduct cardName = cardProductDAO.getCardProductById(bean.getCpNo());
//        System.out.println("cardName"+cardName);
                    String cardname = cardName.getCardName();
//        System.out.println("card name"+cardname);
                    String substr = "Jetprivilege-";
                    cardname = substr.concat(cardname).replaceAll(" ", "-");
//        System.out.println("cardname"+cardname);
System.out.println("))))))"+bean.getJetpriviligemembershipTier().equalsIgnoreCase("GOLD"));
System.out.println(")))PLATINUM)))"+bean.getJetpriviligemembershipTier().equalsIgnoreCase("PLATINUM"));
                    if (bean.getJetpriviligemembershipTier().equalsIgnoreCase("GOLD") || bean.getJetpriviligemembershipTier().equalsIgnoreCase("PLATINUM")) {

                        iciciresponse = iciciWebService.ICICIService(iciciURL, bean, id, iciciUsername, iciciPassword, apiCompanyName, productName, modeOfLead, authorization, contentType, cardname, strSoapURL1, soapActEnc);

                    } else {

                        if (decision.equalsIgnoreCase("Approved") || decision.equalsIgnoreCase("Referred")) {

                            iciciresponse = iciciWebService.ICICIService(iciciURL, bean, id, iciciUsername, iciciPassword, apiCompanyName, productName, modeOfLead, authorization, contentType, cardname, strSoapURL1, soapActEnc);

                        }
                    }
//                   iciciresponse= iciciWebService.ICICIService(iciciURL, bean, id,iciciUsername,iciciPassword,apiCompanyName,productName,modeOfLead,authorization,contentType,cardname);
                    String key = "", value = "";
                    ObjectMapper mapper = new ObjectMapper();
                    Map<String, String> map1;
                    //try {
//                        map1 = mapper.readValue(iciciresponse.replace("\\", ""), Map.class);
//                        for (Map.Entry<String, String> entry : map1.entrySet()) {
//                            key = entry.getKey();
//                            if (key.equals("Result")) {
//                                result = entry.getValue();
//                            } else if (key.equals("Id")) {
//                                iciciId = entry.getValue();
//                            } else if (key.equals("Eligibility")) {
//                                eligibility = entry.getValue();
//                            } else if (key.equals("Description")) {
//                                description = entry.getValue();
//                            } else {
//                                device = entry.getValue();
//                            }
//
//                        }
                    
                    if(!iciciresponse.isEmpty())
                    {
                    iciciId = iciciresponse.substring(iciciresponse.indexOf("#") + 1, iciciresponse.indexOf("|"));
            		
            		boolean succFlg = iciciresponse.contains("Data Saved Successfully");
            		boolean dupFlg = iciciresponse.contains("Duplicate Data");
            		boolean blockedFlg = iciciresponse.contains("Data Blocked for Telecalling");
            		
            		if(succFlg)
            		{
            			result = "Success";
            			description = "Data Saved Successfully";
            			eligibility = "Eligible";
            		}
            		else if(dupFlg)
            		{
            			result = "Fail";
            			description = "Duplicate Data";
            			eligibility = "Not Eligible";
            		}
            		else if(blockedFlg)
            		{
            			result = "Fail";
            			description = "Data Blocked for Telecalling";
            			eligibility = "Not Eligible";
            		}
            		
                    }
                        bean.setIciciId(iciciId);
                        bean.setReferenceId(String.valueOf(id));
                        ccIciciApplicationStatus.setIciciApiRequest(iciciWebService.getRequest());
                        ccIciciApplicationStatus.setIciciApiResponse(iciciWebService.getResponse());
                        ccIciciApplicationStatus.setIciciResult(result);
                        ccIciciApplicationStatus.setIciciID(iciciId);
                        ccIciciApplicationStatus.setIciciEligibility(eligibility);
                        ccIciciApplicationStatus.setIciciDescription(description);
                        ccIciciApplicationStatus.setIciciDevice(device);

//                    } catch (IOException ex) {
//                        java.util.logging.Logger.getLogger(IciciCardServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//                    }

//                }

                iciciCardDAO.addICICIStatus(ccIciciApplicationStatus);

            }

        }

        allApiResponse.put("TuResponse", res);
        if (iciciresponse != "" || !iciciresponse.equalsIgnoreCase("")) {
//            System.out.println("=========if condition "+iciciresponse);
            allApiResponse.put("ICICIResponse", iciciresponse);
        } else {
//             System.out.println("=========else  condition "+iciciresponse);
            allApiResponse.put("ICICIResponse", null);

        }

        return allApiResponse;
    }

    @Override
    public CardDetailsBean getICICIDefaultOfffer(CardDetailsBean cardDetailsBean) {

        CcOffers ccOffers = null;
        try {
            ccOffers = iciciCardDAO.getDefaultOfffer(cardDetailsBean.getBpNo());
            if (null != ccOffers) {
                cardDetailsBean.setOfferTitle(ccOffers.getOfferTitle());
                cardDetailsBean.setOfferContent(ccOffers.getOfferDesc());
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardServiceImpl getDefaultOfffer() :", e);
        }
        return cardDetailsBean;
    }

    @Override
    public CardDetailsBean getICICIOfferByJPNum(String jpNumber, CardDetailsBean cardDetailsBean) {
        CcMapOffers ccMapOffers = null;
        try {
            ccMapOffers = iciciCardDAO.getOfferByJPNum(jpNumber, cardDetailsBean.getBpNo());
            if (null != ccMapOffers) {
                cardDetailsBean.setOfferTitle(ccMapOffers.getCcOffers().getOfferTitle());
                cardDetailsBean.setOfferContent(ccMapOffers.getCcOffers().getOfferDesc());
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICICardServiceImpl getOfferByJPNum() :", e);
        }
        return cardDetailsBean;
    }

    @Override
    public JSONObject getICICIOfferByJPNumber(String jpnumber, String cpNo) {
        CcMapOffers ccMapOffers = null;
        CcOffers ccOffers = null;
        JSONObject json = new JSONObject();
        try {
            CcCardProduct ccCardProduct = cardProductDAO.getCardProductById(Integer.parseInt(cpNo));
            ccMapOffers = iciciCardDAO.getOfferByJPNum(jpnumber, ccCardProduct.getCcBankPartner().getBpNo());
            if (null != ccMapOffers) {
                json.put("offerDesc", ccMapOffers.getCcOffers().getOfferDesc());
                json.put("offerId", ccMapOffers.getCcOffers().getOfferId());
            } else {
                ccOffers = iciciCardDAO.getDefaultOfffer(ccCardProduct.getCcBankPartner().getBpNo());
                if (null != ccOffers) {
                    json.put("offerDesc", ccOffers.getOfferDesc());
                    json.put("offerId", ccOffers.getOfferId());
                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICIServiceImpl getOfferByJPNumber() :", e);
        }
        return json;
    }

    public String getErroMsg() {
        return erroMsg;
    }

    public void setErroMsg(String erroMsg) {
        this.erroMsg = erroMsg;
    }

    @Override
    public String getFormNumber(String formNumber) {
        String FormNumber = "";
        List form = new ArrayList();
        try {
        	if(!formNumber.isEmpty())
        	{
        		form = amexCardDAO.getFormNumber(formNumber);
        		if(!form.isEmpty())
            	{
        			FormNumber = (String) form.get(0);
            	}
        	}
        } catch (Exception e) {
//                 System.out.println("@@@@ Exception in AmexCardServiceImpl getFormNumber() :"+e);
            logger.error("@@@@ Exception in AmexCardServiceImpl getFormNumber() :", e);
        }

        return FormNumber;
    }

    @Override
    public IciciBean getBEIciciFormDetails(String randomNumber, IciciBean iciciBean) {
        CcBeApplicationFormDetails ccBeApplicationFormDetails = null;
        String fName;
        String mName;
        String lName;
        String fullName;
        String address1;
        String address2;
        String address3;
        String pincode;
        String state;
        String city;
        String std;
        String phone;
        String ostd;
        String ophone;
        try {
            logger.info("randomNumber in getBEAmexFormDetails func===>" + randomNumber);

            ccBeApplicationFormDetails = iciciCardDAO.getBEIciciFormDetails(randomNumber, iciciBean.getCpNo());
//            logger.info("getBEAmexFormDetails func ccBeApplicationFormDetails=======>" + ccBeApplicationFormDetails);
            if (null != ccBeApplicationFormDetails) {

                fName = CommonUtils.nullSafe(ccBeApplicationFormDetails.getFname());
                mName = CommonUtils.nullSafe(ccBeApplicationFormDetails.getMname());
                lName = CommonUtils.nullSafe(ccBeApplicationFormDetails.getLname());
                fullName = fName.concat(" ").concat(mName).concat(" ").concat(lName);
                iciciBean.setFname(fName);
                iciciBean.setMname(mName);
                iciciBean.setLname(lName);
                iciciBean.setFullName(fullName);

                iciciBean.setEmail(CommonUtils.nullSafe(ccBeApplicationFormDetails.getEmail()));
                iciciBean.setMobile(CommonUtils.nullSafe(ccBeApplicationFormDetails.getMobile()));

                if (null != ccBeApplicationFormDetails.getDateOfBirth()) {
                    iciciBean.setDateOfBirth(CommonUtils.getDate(ccBeApplicationFormDetails.getDateOfBirth(), "MM-dd-yyyy"));
                }
                if (null != ccBeApplicationFormDetails.getGender() && !ccBeApplicationFormDetails.getGender().isEmpty()) {
                    iciciBean.setGender("Male".equals(CommonUtils.nullSafe(ccBeApplicationFormDetails.getGender())) ? "0" : "1");
                }

                String enumData = "";
                if (!ccBeApplicationFormDetails.getEduQualification().equalsIgnoreCase("")) {
                    Map<Byte, String> enumMap = commonService.getEnumValues("Qualification");
                    for (Map.Entry<Byte, String> entry : enumMap.entrySet()) {
                        if (entry.getValue().contains(CommonUtils.nullSafe(ccBeApplicationFormDetails.getEduQualification()))) {
                            enumData = entry.getValue().split("-")[0];
                        }
                    }
                } else {

                    enumData = "";
                }

                iciciBean.setEduQualification(enumData);
//                iciciBean.setAadharNumber(CommonUtils.nullSafe(ccBeApplicationFormDetails.getAadharNumber()));
                iciciBean.setPancard(CommonUtils.nullSafe(ccBeApplicationFormDetails.getPancard()));

                if (null != ccBeApplicationFormDetails.getMonthlyIncome()) {
                    BigDecimal bd = new BigDecimal(0.00);
                    System.out.println("bd" + bd);
                    int c = bd.intValueExact();
                    BigDecimal b = ccBeApplicationFormDetails.getMonthlyIncome();

                    int a = b.toBigInteger().intValueExact();
                    logger.info("a====>" + String.valueOf(a));

                    logger.info("b" + b.toString());
                    if (a == c) {

                    } else {
                        BigDecimal income = CommonUtils.bigdecnullSafe(ccBeApplicationFormDetails.getMonthlyIncome(), new BigDecimal(0));

                        iciciBean.setMonthlyIncome(income.setScale(0));
                    }
//                    iciciBean.setMonthlyIncome(CommonUtils.checkForDecimal(new BigDecimal(ccBeApplicationFormDetails.getMonthlyIncome())));
                }
                address1 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getAddress());
                address2 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getAddress2());
                address3 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getAddress3());
                city = CommonUtils.nullSafe(ccBeApplicationFormDetails.getCity());
                pincode = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPinCode());
                state = CommonUtils.nullSafe(ccBeApplicationFormDetails.getState());

                iciciBean.setAddress(address1);
                iciciBean.setAddress2(address2);
                iciciBean.setAddress3(address3);
                iciciBean.setCity(city);
                iciciBean.setState1(state);
                iciciBean.setPinCode(pincode);

                if (!(address1.isEmpty())) {
                    iciciBean.setResedentialAddress(
                            address1 + " " + address2 + " " + address3 + " " + state + " " + city + " " + pincode);
                }
                iciciBean.setPeraddresssameascurr(
                        CommonUtils.nullSafe(ccBeApplicationFormDetails.getPeraddresssameascurr()));

                address1 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPermaddress());
                address2 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPermaddress2());
                address3 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPermaddress3());
                city = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPCity());
                pincode = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPPinCode());
                state = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPState());

                iciciBean.setPermaddress(address1);
                iciciBean.setPermaddress2(address2);
                iciciBean.setPermaddress3(address3);
                iciciBean.setPCity(city);
                iciciBean.setPState(state);
                iciciBean.setPpinCode(pincode);

                if (!(address1.isEmpty())) {
                    iciciBean.setPermanentAddress(address1 + " " + address2 + " " + address3 + " " + state + " " + city + " " + pincode);
                }

                iciciBean.setJetpriviligemembershipTier(CommonUtils.nullSafe(ccBeApplicationFormDetails.getJetpriviligemembershipTier()));
                iciciBean.setJetpriviligemembershipNumber(CommonUtils.nullSafe(ccBeApplicationFormDetails.getJetpriviligemembershipNumber()));
//                if (!ccBeApplicationFormDetails.getEmploymentType().isEmpty()) {
//                    iciciBean.setEmploymentType(CommonUtils.nullSafe(ccBeApplicationFormDetails.getEmploymentType()));
//                }
                iciciBean.setCompanyName(CommonUtils.nullSafe(ccBeApplicationFormDetails.getCompanyName()));

                address1 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOAddress());
                address2 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOAddress2());
                address3 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOAddress3());
                city = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOCity());
                pincode = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOPincode());
                state = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOState());

                iciciBean.setOAddress(address1);
                iciciBean.setOAddress2(address2);
                iciciBean.setOAddress3(address3);
                iciciBean.setOCity(city);
                iciciBean.setOState(state);
                iciciBean.setOPincode(pincode);

                if (!(address1.isEmpty())) {
                    iciciBean.setCompanyAddress(address1 + " " + address2 + " " + address3 + " " + state + " " + city + " " + pincode);
                }

                std = CommonUtils.nullSafe(ccBeApplicationFormDetails.getStd(), "");
                phone = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPhone(), "");
                if (std != "" && phone != "") {
                    iciciBean.setPhone(phone);
                    iciciBean.setStd(std);
                    iciciBean.setHomeNumber(std + " " + phone);
                }
                ostd = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOStd());
                ophone = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOPhone());
                if (ostd != "" && ophone != "") {
                    iciciBean.setOPhone(ophone);
                    iciciBean.setOStd(ostd);
                }
                if (!ostd.isEmpty()) {
                    iciciBean.setOfficeNumber(ostd + " " + ophone);
                }
                logger.info("form number if not null==>" + ccBeApplicationFormDetails.getFormNumber() + "randomNumber==>" + randomNumber);
                iciciBean.setInitiatedBy(CommonUtils.nullSafe(ccBeApplicationFormDetails.getApplicationInitiatedBy()));
                iciciBean.setSourceOfCall(CommonUtils.nullSafe(ccBeApplicationFormDetails.getSourceOfCall()));
                iciciBean.setFormNumber(CommonUtils.nullSafe(ccBeApplicationFormDetails.getFormNumber()));
                iciciBean.setRandomNumber(randomNumber);
                iciciBean.setInitiationDate(CommonUtils.getDate(ccBeApplicationFormDetails.getInitiationDate(), "dd-MM-yyyy HH:mm:ss"));
                iciciBean.setAgentId(CommonUtils.nullSafe(ccBeApplicationFormDetails.getAgentId()));
                if (ccBeApplicationFormDetails.getEmploymentType() != null || ccBeApplicationFormDetails.getEmploymentType() != "" || ccBeApplicationFormDetails.getEmploymentType() != "NA") {
                    iciciBean.setEmploymentType(ccBeApplicationFormDetails.getEmploymentType());
                }
                if (ccBeApplicationFormDetails.getITRstatus() != null || ccBeApplicationFormDetails.getITRstatus() != "" || ccBeApplicationFormDetails.getITRstatus() != "NA") {
                    iciciBean.setITRStaus(ccBeApplicationFormDetails.getITRstatus());
                }
                if (ccBeApplicationFormDetails.getIciciBankRelationship() != null || ccBeApplicationFormDetails.getIciciBankRelationship() != "" || ccBeApplicationFormDetails.getIciciBankRelationship() != "NA") {
                    iciciBean.setIciciBankRelationship(ccBeApplicationFormDetails.getIciciBankRelationship());
                }
                if (ccBeApplicationFormDetails.getIciciRelationshipNo() != null || ccBeApplicationFormDetails.getIciciRelationshipNo() != "" || ccBeApplicationFormDetails.getIciciRelationshipNo() != "NA") {
                    iciciBean.setICICIBankRelationShipNo(ccBeApplicationFormDetails.getIciciRelationshipNo());
                }
                if (ccBeApplicationFormDetails.getSalAccwithotherbank() != null || ccBeApplicationFormDetails.getSalAccwithotherbank() != "" || ccBeApplicationFormDetails.getSalAccwithotherbank() != "NA") {
                    iciciBean.setSalaryBankWithOtherBank(ccBeApplicationFormDetails.getSalAccwithotherbank());
                }
                if (ccBeApplicationFormDetails.getSalAccountopend() != null || ccBeApplicationFormDetails.getSalAccountopend() != "" || ccBeApplicationFormDetails.getSalAccountopend() != "NA") {
                    iciciBean.setSalaryAccountOpened(ccBeApplicationFormDetails.getSalAccountopend());
                }
                if (ccBeApplicationFormDetails.getTotalExp() != null || ccBeApplicationFormDetails.getTotalExp() != "" || ccBeApplicationFormDetails.getTotalExp() != "NA") {
                    String totalexp = ccBeApplicationFormDetails.getTotalExp();
                    System.out.println("totalexp==>" + totalexp);
                    int index = totalexp.indexOf(".");
                    System.out.println("index" + index);
                    String preindex = totalexp.substring(0, index);
                    String postindex = totalexp.substring(index + 1);

                    System.out.println("preindex" + preindex + "postindex" + postindex);
                    iciciBean.setYears(preindex);
                    iciciBean.setMonths(postindex);
//                               iciciBean.setTotalExperience(ccBeApplicationFormDetails.getTotalExp());
                }
            } else {
//            iciciBean=new IciciBean();
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl getBEAmexFormDetails() :", e);
        }
        return iciciBean;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void saveOrUpdateFormData(IciciBean iciciBean, int param) {
        try {
            CcBeApplicationFormDetails ccBeApplicationFormDetails = null;

            ccBeApplicationFormDetails = iciciCardDAO.checkICICIFormNumber(iciciBean.getFormNumber());
            String randomNumber = "";

            if (iciciBean.getRandomNumber().isEmpty() || null == iciciBean.getRandomNumber() || "NA".equalsIgnoreCase(iciciBean.getRandomNumber())) {
                randomNumber = RandomStringUtils.random(20, true, true);
            } else {
                randomNumber = iciciBean.getRandomNumber();
            }
            if (randomNumber.length() < 18) {
                randomNumber = RandomStringUtils.random(20, true, true);
            }
            iciciBean.setRandomNumber(randomNumber);

            if (null != ccBeApplicationFormDetails) {
                ccBeApplicationFormDetails = getICICIBEApplicationDetails(ccBeApplicationFormDetails, iciciBean);
                if (0 != param) {
                    logger.info("at 0 param");
                    ccBeApplicationFormDetails.setFormStatus("Completed");
                } else {
                    logger.info("at modified else block");
                    ccBeApplicationFormDetails.setCreatedTime(new Date());
                    ccBeApplicationFormDetails.setModifiedTime(new Date());
                    ccBeApplicationFormDetails.setModifiedBy(1);
                }
                iciciCardDAO.saveOrUpdateFormData(ccBeApplicationFormDetails);
            } else {
                ccBeApplicationFormDetails = new CcBeApplicationFormDetails();

                ccBeApplicationFormDetails = getICICIBEApplicationDetails(ccBeApplicationFormDetails, iciciBean);
                ccBeApplicationFormDetails.setFormStatus("Initiated");
                ccBeApplicationFormDetails.setBpNo(2);
                ccBeApplicationFormDetails.setCpNo(iciciBean.getCpNo());
                ccBeApplicationFormDetails.setInitiationDate(new Date());
                ccBeApplicationFormDetails.setCreatedTime(new Date());
                ccBeApplicationFormDetails.setCreatedBy(1);
                ccBeApplicationFormDetails.setStatus((byte) 1);
                iciciCardDAO.saveOrUpdateFormData(ccBeApplicationFormDetails);

//                amexCardDAO.save(ccBeApplicationFormDetails);
            }

            logger.info("Amex Application Save/Update form no: " + ccBeApplicationFormDetails.getFormNumber() + "  BEAPP form no" + ccBeApplicationFormDetails.getBeAppFormNo());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("e" + e);
            logger.error("@@@@ Exception in AmexCardServiceImpl saveFormData() :", e);
        }
    }

    private CcBeApplicationFormDetails getICICIBEApplicationDetails(CcBeApplicationFormDetails ccBeApplicationFormDetails, IciciBean iciciBean) {
        String address = "";
        String address2 = "";
        String address3 = "";
        String city = "";
        String state = "";
        try {
            String firstName;
            String middleName;
            String lastName;
            String dob = "", gender = "";
            dob = iciciBean.getDateOfBirth();
            System.out.println("==========>" + iciciBean.getDateOfBirth());
            dob = (null != dob) ? dob.trim() : "";
            System.out.println("==dob 2" + dob);
            gender = iciciBean.getGender();
            if (gender.equals("0")) {
                gender = "Male";
            } else if (gender.equals("1")) {
                gender = "Female";
            }

            String totalexp = iciciBean.getTotalExperience();

            String mo = null, years = null;
            if ("".equals(totalexp)) {
                mo = "0";
                years = "0";
                totalexperience = years + "." + mo;
            } else if (totalexp.contains("years") || totalexp.contains("year")) {

                totalexp = totalexp.substring(0, totalexp.indexOf("m")).trim();
                if (!totalexp.contains("s")) {
                    totalexp = totalexp.replace(" ", "");
//             String months=totalexp.trim().substring(0, totalexp.indexOf("m"));
                    String months = totalexp;

                    mo = months.substring(months.indexOf("r") + 1).trim();
                    years = months.substring(0, months.indexOf("year"));
                    totalexperience = years + "." + mo;

                }
                if (totalexp.contains("s")) {

                    totalexp = totalexp.replace(" ", "");
//             String months=totalexp.trim().substring(0, totalexp.indexOf("m"));
                    String months = totalexp;
                    mo = months.substring(months.indexOf("s") + 1).trim();
                    years = months.substring(0, months.indexOf("years"));
                    totalexperience = years + "." + mo;

                }
            }

            firstName = CommonUtils.nullSafe(iciciBean.getFname(), "");
            middleName = CommonUtils.nullSafe(iciciBean.getMname(), "");
            lastName = CommonUtils.nullSafe(iciciBean.getLname(), "");
            System.out.println("serive : " + iciciBean.getFname());
            ccBeApplicationFormDetails.setFname(firstName);
            ccBeApplicationFormDetails.setMname(middleName);
            ccBeApplicationFormDetails.setLname(lastName);
            if (null != iciciBean.getDateOfBirth() && !iciciBean.getDateOfBirth().isEmpty()) {
                ccBeApplicationFormDetails.setDateOfBirth(CommonUtils.ICICIDate(dob));
            }
//            if (null != iciciBean.getGender() && !iciciBean.getGender().isEmpty()) {
//                ccBeApplicationFormDetails.setGender("0".equalsIgnoreCase(CommonUtils.nullSafe(iciciBean.getGender())) ? "Male" : "Female");
//            }
            ccBeApplicationFormDetails.setGender(CommonUtils.nullSafe(gender, "NA"));

            System.out.println("serive : after");
            String Email = CommonUtils.nullSafe(iciciBean.getEmail(), "");
            ccBeApplicationFormDetails.setEmail(Email);
            String mobile = CommonUtils.nullSafe(iciciBean.getMobile(), "");
            ccBeApplicationFormDetails.setMobile(mobile);
            ccBeApplicationFormDetails.setEduQualification(commonService.getAmexEnumValues("Qualification", iciciBean.getEduQualification()));
            ccBeApplicationFormDetails.setAadharNumber("" + iciciBean.getAadharNumber());
            ccBeApplicationFormDetails.setPancard("" + iciciBean.getPancard());

            BigDecimal income = CommonUtils.bigdecnullSafe(iciciBean.getMonthlyIncome(), new BigDecimal(0));
            System.out.println("----------income---->" + income);

            ccBeApplicationFormDetails.setMonthlyIncome(income);

            ccBeApplicationFormDetails.setAddress(CommonUtils.nullSafe(iciciBean.getAddress()));
            ccBeApplicationFormDetails.setAddress2(CommonUtils.nullSafe(iciciBean.getAddress2()));
            ccBeApplicationFormDetails.setAddress3(CommonUtils.nullSafe(iciciBean.getAddress3()));
            System.out.println("========city=============>" + iciciBean.getHcity());
            ccBeApplicationFormDetails.setCity(CommonUtils.nullSafe(iciciBean.getHcity()));
            ccBeApplicationFormDetails.setState(CommonUtils.nullSafe(iciciBean.getHstate()));
            System.out.println("=========state============>" + iciciBean.getHstate());

            ccBeApplicationFormDetails.setPinCode(CommonUtils.nullSafe(iciciBean.getPinCode()));

            String perAdd = CommonUtils.nullSafe(iciciBean.getPeraddresssameascurr());
            ccBeApplicationFormDetails.setPeraddresssameascurr(perAdd);
            if ("Yes".equalsIgnoreCase(perAdd)) {
                ccBeApplicationFormDetails.setPermaddress(CommonUtils.nullSafe(iciciBean.getAddress()));
                ccBeApplicationFormDetails.setPermaddress2(CommonUtils.nullSafe(iciciBean.getAddress2()));
                ccBeApplicationFormDetails.setPermaddress3(CommonUtils.nullSafe(iciciBean.getAddress3()));
                ccBeApplicationFormDetails.setPCity(CommonUtils.nullSafe(iciciBean.getHcity()));
                ccBeApplicationFormDetails.setPState(CommonUtils.nullSafe(iciciBean.getHstate()));
                ccBeApplicationFormDetails.setPPinCode(CommonUtils.nullSafe(iciciBean.getPpinCode()));

            } else {
                ccBeApplicationFormDetails.setPermaddress(CommonUtils.nullSafe(iciciBean.getPermaddress()));
                ccBeApplicationFormDetails.setPermaddress2(CommonUtils.nullSafe(iciciBean.getPermaddress2()));
                ccBeApplicationFormDetails.setPermaddress3(CommonUtils.nullSafe(iciciBean.getPermaddress3()));
                ccBeApplicationFormDetails.setPCity(CommonUtils.nullSafe(iciciBean.getPCity()));
                ccBeApplicationFormDetails.setPState(CommonUtils.nullSafe(iciciBean.getPState()));
                ccBeApplicationFormDetails.setPPinCode(CommonUtils.nullSafe(iciciBean.getPpinCode()));
            }
            ccBeApplicationFormDetails.setJetpriviligemember(CommonUtils.nullSafe(iciciBean.getJetpriviligemember()));
            ccBeApplicationFormDetails.setJetpriviligemembershipNumber(CommonUtils.nullSafe(iciciBean.getJetpriviligemembershipNumber()));
            ccBeApplicationFormDetails.setJetpriviligemembershipTier(CommonUtils.nullSafe(iciciBean.getJetpriviligemembershipTier()));

            ccBeApplicationFormDetails.setEmploymentType(CommonUtils.nullSafe(iciciBean.getEmploymentType()));
            ccBeApplicationFormDetails.setCompanyName(CommonUtils.nullSafe(iciciBean.getCompanyName()));

            ccBeApplicationFormDetails.setOAddress(CommonUtils.nullSafe(iciciBean.getOAddress()));
            ccBeApplicationFormDetails.setOAddress2(CommonUtils.nullSafe(iciciBean.getOAddress2()));
            ccBeApplicationFormDetails.setOAddress3(CommonUtils.nullSafe(iciciBean.getOAddress3()));
            ccBeApplicationFormDetails.setOCity(CommonUtils.nullSafe(iciciBean.getHcity2()));
            ccBeApplicationFormDetails.setOState(CommonUtils.nullSafe(iciciBean.getHstate2()));
            ccBeApplicationFormDetails.setOPincode(CommonUtils.nullSafe(iciciBean.getOPincode()));
            ccBeApplicationFormDetails.setStd(CommonUtils.nullSafe(iciciBean.getStd()));
            ccBeApplicationFormDetails.setPhone(CommonUtils.nullSafe(iciciBean.getPhone()));
            ccBeApplicationFormDetails.setUtmSource(CommonUtils.nullSafe(iciciBean.getUtmSource(), "Direct"));
            ccBeApplicationFormDetails.setUtmMedium(CommonUtils.nullSafe(iciciBean.getUtmMedium(), "NA"));
            ccBeApplicationFormDetails.setUtmCampaign(CommonUtils.nullSafe(iciciBean.getUtmCampaign(), "NA"));
            ccBeApplicationFormDetails.setOStd(CommonUtils.nullSafe(iciciBean.getOStd()));
            ccBeApplicationFormDetails.setOPhone(CommonUtils.nullSafe(iciciBean.getOPhone()));
            logger.info("at be app form===>" + iciciBean.getAppStatus());

            ccBeApplicationFormDetails.setAppStatus(CommonUtils.nullSafe(iciciBean.getAppStatus(), "NA"));
            ccBeApplicationFormDetails.setApplicationInitiatedBy(CommonUtils.nullSafe(iciciBean.getInitiatedBy(), "Self"));
//            ccBeApplicationFormDetails.setFormStatus("Initiated");
            ccBeApplicationFormDetails.setAgentId(CommonUtils.nullSafe(iciciBean.getAgentId(), "NA"));

            ccBeApplicationFormDetails.setSourceOfCall(CommonUtils.nullSafe(iciciBean.getSourceOfCall(), "NA"));
            ccBeApplicationFormDetails.setRandomNumber(CommonUtils.nullSafe(iciciBean.getRandomNumber(), "NA"));
            ccBeApplicationFormDetails.setFormNumber(CommonUtils.nullSafe(iciciBean.getFormNumber()));
            ccBeApplicationFormDetails.setJetpriviligemember("Y");
            ccBeApplicationFormDetails.setUniqueFormNumber(CommonUtils.nullSafe(iciciBean.getFormNumber()));
            ccBeApplicationFormDetails.setITRstatus(CommonUtils.nullSafe(iciciBean.getITRStaus()));
            ccBeApplicationFormDetails.setIciciBankRelationship(CommonUtils.nullSafe(iciciBean.getIciciBankRelationship()));
            ccBeApplicationFormDetails.setIciciRelationshipNo(CommonUtils.nullSafe(iciciBean.getICICIBankRelationShipNo()));
            ccBeApplicationFormDetails.setSalAccountopend(CommonUtils.nullSafe(iciciBean.getSalaryAccountOpened()));
            ccBeApplicationFormDetails.setSalAccwithotherbank(CommonUtils.nullSafe(iciciBean.getSalaryBankWithOtherBank()));
            ccBeApplicationFormDetails.setTotalExp(CommonUtils.nullSafe(totalexperience));
            ccBeApplicationFormDetails.setPromocode(CommonUtils.nullSafe(iciciBean.getPromocode()));
            String inactive = iciciBean.getEmailInactiveFlag();
            inactive = ("".equals(inactive)) ? "N" : inactive;
//            System.out.println("inactive"+inactive);
            ccBeApplicationFormDetails.setEmailFlag(CommonUtils.nullSafe(inactive, "N"));

        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICI getBEApplicationDetails() :", e);
            System.out.println("-=================>" + e);
            e.printStackTrace();
        }
        return ccBeApplicationFormDetails;
    }

    @Override
    public void FormInactiveSilverPopApiIcici(IciciBean iciciBean) {
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            logger.info("formactive silver pop call for " + iciciBean.getFname() + " Mobile no:" + iciciBean.getMobile());
//            TransactRequestType transReqType = new TransactRequestType();
//            transReqType.setCampaign_id(inactiveid);//campaign id
//            transReqType.setTransaction_id(transReqType.getCampaign_id());
//            transReqType.setShow_all_send_details(true);
            HashMap<String, String> saveColumns = new HashMap();
            CcCardProduct cardName = cardProductDAO.getCardProductById(iciciBean.getCpNo());
            saveColumns.put("First_Name", iciciBean.getFname());
            saveColumns.put("Last_Name", iciciBean.getLname());
            saveColumns.put("Form_Number", iciciBean.getFormNumber());
            saveColumns.put("JP_Number", iciciBean.getJetpriviligemembershipNumber());
            saveColumns.put("Mobile_Number", iciciBean.getMobile());
            saveColumns.put("Email", agentMailId);
            saveColumns.put("Member_EmailID", iciciBean.getEmail());
            saveColumns.put("Card_Name", cardName.getCardName());

            Date date = new Date();

            String strDate = formatter.format(date);
            saveColumns.put("Application_Date", strDate);
//            saveColumns.put("URL", amexBean.getRandomNumber());

//            transReqType.setColumns(saveColumns);
            //Enter the email id to which the mail is to be send
//            transReqType.setEmail(agentMailId);
//            transReqType.setBody_type("HTML");

//            SilverPopApiCards.callSilverPopApi(url, clientId, clientSecret, refreshToken);
            
            SendGridMail sg = new SendGridMail();
            sg.sendMail(sendGridUrl, sendGridKey, saveColumns, sendGridInactiveid, agentMailId, iciciBean.getFormNumber(), simplicaUserName, simplicaPassword);
            
//			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
//			List<ColumnElementType> columnElementarray = new ArrayList<>();
//			
//			ColumnElementType colEleMenType1 = new ColumnElementType();
//			colEleMenType1.setNAME("First_Name");
//			colEleMenType1.setVALUE(amexBean.getFname());
//			
//			ColumnElementType colEleMenType2 = new ColumnElementType();
//			colEleMenType2.setNAME("Last_Name");
//			colEleMenType2.setVALUE(amexBean.getLname());
//			
//			ColumnElementType colEleMenType3 = new ColumnElementType();
//			colEleMenType3.setNAME("Form_Number");
//			colEleMenType3.setVALUE(amexBean.getFormNumber());
//			
//			ColumnElementType colEleMenType4 = new ColumnElementType();
//			colEleMenType4.setNAME("JP_Number");
//			colEleMenType4.setVALUE(amexBean.getJetpriviligemembershipNumber());
//			
//			ColumnElementType colEleMenType5 = new ColumnElementType();
//			colEleMenType5.setNAME("Mobile_Number");
//			colEleMenType5.setVALUE(amexBean.getMobile());
//			
//			ColumnElementType colEleMenType6 = new ColumnElementType();
//			colEleMenType6.setNAME("Email");
//			colEleMenType6.setVALUE("parvati.gholap@vernost.in");
////                      colEleMenType6.setVALUE("poonam.velaskar@vernost.in"); 
//			
//			ColumnElementType colEleMenType7 = new ColumnElementType();
//			colEleMenType7.setNAME("Member_EmailID");
//			colEleMenType7.setVALUE(amexBean.getEmail());
//			
//			ColumnElementType colEleMenType8 = new ColumnElementType();
//			colEleMenType8.setNAME("Application_Date");
//			Date date = new Date();
//			String strDate = formatter.format(date);
//			colEleMenType8.setVALUE(strDate);
//			
//			ColumnElementType colEleMenType9 = new ColumnElementType();
//			colEleMenType9.setNAME("URL");
//			colEleMenType9.setVALUE(amexBean.getRandomNumber());
//			
//			columnElementarray.add(colEleMenType1);
//			columnElementarray.add(colEleMenType2);
//			columnElementarray.add(colEleMenType3);
//			columnElementarray.add(colEleMenType4);
//			columnElementarray.add(colEleMenType5);
//			columnElementarray.add(colEleMenType6);
//			columnElementarray.add(colEleMenType7);
//			columnElementarray.add(colEleMenType8);
//			columnElementarray.add(colEleMenType9);
//			ColumnElementType[] elementArray = new ColumnElementType[columnElementarray.size()];
//			elementArray = columnElementarray.toArray(elementArray);
//                        logger.info("Amex inactive silver pop calling-->User mobile-->"+amexBean.getMobile()+" user first name-->"+amexBean.getFname()+" user email--"+amexBean.getEmail());
//			SilverPopUtil.silverPopAPI(elementArray, silverPopuserName, silverPoppassword, inactiveListId);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl FormInactiveSilverPopApi() :", e);
        }
    }
    
    
    
    public String  getEncValue(String strval, String strSoapURL1, String soapActEnc)
    {
	    ICICIEncryptDetail e = new ICICIEncryptDetail();
	    String encryptDataVar = e.encryptData(strval, strSoapURL1, soapActEnc);
	    return encryptDataVar;
    }
    
    
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public CcBeApplicationFormDetails fetchPostSignupMemberDtls(String formNo, int param) {
    	CcBeApplicationFormDetails ccBeApplicationFormDetails = null;
    	try {
    		logger.info("form number in service - "+formNo);
    		if(formNo!= null && !formNo.isEmpty())
    		{
    			ccBeApplicationFormDetails = iciciCardDAO.checkICICIFormNumber(formNo);
    		}	

            if (null != ccBeApplicationFormDetails) {
                return ccBeApplicationFormDetails;
            }

            //logger.info("Amex Application fetch signup data() form no: " + ccBeApplicationFormDetails.getFormNumber() + "  BEAPP form no" + ccBeApplicationFormDetails.getBeAppFormNo());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("@@@@ Exception in AmexCardServiceImpl fetch signup data() :", e);
        }
        return ccBeApplicationFormDetails;
    }    

}
