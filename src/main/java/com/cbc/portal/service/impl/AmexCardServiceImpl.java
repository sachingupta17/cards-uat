package com.cbc.portal.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.tools.ant.types.CommandlineJava.SysProperties;
import org.datacontract.schemas._2004._07.ObjectFactory;
import org.datacontract.schemas._2004._07.UserLeadDetailsJetAirways;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.xml.transform.StringResult;
import org.tempuri.SubmitGNAWithoutPrequalApplication;
import org.tempuri.SubmitGNAWithoutPrequalApplicationResponse;

import com.cbc.portal.DAO.AmexCardDAO;
import com.cbc.portal.DAO.CardProductDAO;
import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.constants.CcPortalConstants;
import com.cbc.portal.entity.CcAmexApplicationStatus;
import com.cbc.portal.entity.CcApplicationFormDetails;
import com.cbc.portal.entity.CcBeApplicationFormDetails;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcMapOffers;
import com.cbc.portal.entity.CcMemberDetails;
import com.cbc.portal.entity.CcOffers;
import com.cbc.portal.service.AmexCardService;
import com.cbc.portal.service.CommonService;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.SSLContextUtil;
import com.cbc.portal.utils.SendGridMail;
import com.cbc.portal.utils.SilverPopUtil;
import com.cbc.portal.utils.SoapServicesUtil;
import static com.cbc.portal.utils.VerifyRecaptcha.url;
import com.cc.SilverPopApiCards;
import com.cc.TransactRequestType;
import java.util.HashMap;
import org.apache.commons.lang.RandomStringUtils;
import recipientactions.listmgmt.engageservice.ColumnElementType;

@Component
@Transactional(readOnly = true)
public class AmexCardServiceImpl implements AmexCardService {

    private Logger logger = Logger.getLogger(AmexCardServiceImpl.class.getName());

    private static final String PENDING = "pending";

    @Value("${application.amex.partner}")
    private String partner;

    @Value("${application.amex.username}")
    private String username;

    @Value("${application.amex.password}")
    private String password;

    @Value("${application.silverpop.userName}")
    private String silverPopuserName;

    @Value("${application.silverpop.password}")
    private String silverPoppassword;

    @Value("${application.silverpop.inactiveListId}")
    private String inactiveListId;

    @Value("${application.silverpop.callmeListId}")
    private String callmeListId;

    @Value("${application.silverPopApi.url}")
    private String url;

    @Value("${application.silverPopApi.clientID}")
    private String clientId;

    @Value("${application.silverPopApi.clientSecret}")
    private String clientSecret;

    @Value("${application.silverPopApi.refreshToken}")
    private String refreshToken;

    @Value("${application.silverpop.inactive.campaignid}")
    private long inactiveid;

    @Value("${application.silverpop.callmeid.new.campaignid}")
    private long callmeid;

    @Value("${application.silverpop.agentMailId}")
    private String agentMailId;

    @Value("${application.callme.agentMailId}")
    private String callmeAgentId;
    
    @Value("${application.sendGridApi.url}")
    private String sendGridUrl;

    @Value("${application.sendGridApi.key}")
    private String sendGridKey;

    @Value("${application.sendGrid.inactive.campaignid}")
    private String sendGridInactiveid;

    @Value("${application.sendGrid.callmeid.new.campaignid}")
    private String sendGridCallmeid;

    @Value("${application.simplica.username}")
    private String simplicaUserName;
    
    @Value("${application.simplica.password}")
    private String simplicaPassword;
    
    @Autowired
    private WebServiceTemplate webServiceTemplate;

    @Autowired
    private AmexCardDAO amexCardDAO;

    @Autowired
    private CardProductDAO cardProductDAO;

    @Autowired
    private CommonService commonService;

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String applyAmexCardWebService(AmexBean bean, String userTier) {

        String offer_ID = "";

        SubmitGNAWithoutPrequalApplicationResponse response = new SubmitGNAWithoutPrequalApplicationResponse();
        CcApplicationFormDetails ccApplicationForm = new CcApplicationFormDetails();

        ObjectFactory objFactory = new ObjectFactory();
        UserLeadDetailsJetAirways leadDetails = new UserLeadDetailsJetAirways();
        org.tempuri.ObjectFactory obj2 = new org.tempuri.ObjectFactory();

        CcMapOffers ccMapOffers = null;
        CcOffers ccOffers = null;

        try {
            logger.info("userTier of session" + userTier);
            CcCardProduct ccCardProduct = cardProductDAO.getCardProductById(bean.getCpNo());
            ccMapOffers = amexCardDAO.getOfferByJPNum(bean.getJetpriviligemembershipNumber(), ccCardProduct.getCcBankPartner().getBpNo());

            if (null != ccMapOffers) {
                offer_ID = ccMapOffers.getCcOffers().getOfferId();
                logger.info("in ccMapOffers CP_NO is--->" + bean.getCpNo() + "   JPNUMBER---->" + bean.getJetpriviligemembershipNumber() + "   OFFER ID---->" + offer_ID);
            } else {
                ccOffers = amexCardDAO.getDefaultOfffer(ccCardProduct.getCcBankPartner().getBpNo());
                if (null != ccOffers) {
                    offer_ID = ccOffers.getOfferId();
                    logger.info("ccOffers in CP_NO is--->" + bean.getCpNo() + "   JPNUMBER---->" + bean.getJetpriviligemembershipNumber() + "  OFFER ID---->" + offer_ID);
                }

            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in getOfferByJPNumber() IN form submit :", e);
        }

        String dob = "";
        String email = "";
        String fname = "";
        String mname = "";
        String lname = "";
        String gender = "";
        String tier = "";
        String mobile = "";
        String addrs1 = "";
        String addrs2 = "";
        String addrs3 = "";
        String phone = "";
        String std = "";
        String city = "";
        String state = "";
        String pincode = "";
        String pancard = "";
        String aadhar = "";
        String jpnumber = "";
        String statement = "";
        String qualification = "";
        String emptype = "";
        String income = "";
        String company = "";
        String permascurr;
        String oaddrs1 = "";
        String oaddrs2 = "";
        String oaddrs3 = "";
        String ophone = "";
        String ostd = "";
        String ocity = "";
        String ostate = "";
        String opincode = "";
        String paddr = "";
        String paddr2 = "";
        String paddr3 = "";
        String pcity = "";
        String pstate = "";
        String ppincode = "";

        fname = CommonUtils.nullSafe(bean.getFname());
        leadDetails.setFNAME(objFactory.createUserLeadDetailsJetAirwaysFNAME(fname));
        ccApplicationForm.setFname(fname);

        mname = CommonUtils.nullSafe(bean.getMname());
        leadDetails.setMNAME(objFactory.createUserLeadDetailsJetAirwaysMNAME(mname));
        ccApplicationForm.setMname(mname);

        lname = CommonUtils.nullSafe(bean.getLname());
        leadDetails.setLNAME(objFactory.createUserLeadDetailsJetAirwaysLNAME(lname));
        ccApplicationForm.setLname(lname);

        gender = CommonUtils.nullSafe(bean.getGender());
        gender = "0".equalsIgnoreCase(gender) ? "Male" : "Female";
        leadDetails.setGENDER(objFactory.createUserLeadDetailsJetAirwaysGENDER(gender));
        ccApplicationForm.setGender(gender);

        dob = CommonUtils.nullSafe(bean.getDateOfBirth());
        leadDetails.setDOB(objFactory.createUserLeadDetailsJetAirwaysDOB(dob));
        ccApplicationForm.setDateOfBirth(CommonUtils.setDateForAmexAppl(dob));

        email = CommonUtils.nullSafe(bean.getEmail());
        leadDetails.setEMAIL(objFactory.createUserLeadDetailsJetAirwaysEMAIL(email));
        ccApplicationForm.setEmail(email);

        tier = CommonUtils.nullSafe(bean.getJetpriviligemembershipTier());
        logger.info("tier" + tier);
        String APITier = "";
        tier = tier.toUpperCase();
        if (userTier.equals("PLATINUM")) {
            APITier = "PL";

        } else if (userTier.equals("GOLD")) {

            APITier = "GL";
        } else if (userTier.equals("SILVER")) {
            APITier = "SL";
        } else if (userTier.equals("RED")) {
            APITier = "BP";
        } else if (userTier.equals("BASE")) {
            APITier = "BL";
        }

        APITier = APITier.toUpperCase();
        logger.info("APITier uppercase" + APITier);
        logger.info("APITier: " + APITier + " value which save in database: " + userTier + " Jp Number: " + bean.getJetpriviligemembershipNumber());

        leadDetails.setJetPriviligeMembershipTier(
                objFactory.createUserLeadDetailsJetAirwaysJetPriviligeMembershipTier(APITier));
        ccApplicationForm.setJetpriviligemembershipTier(userTier);

        mobile = CommonUtils.nullSafe(bean.getMobile());
        leadDetails.setMOBILE(objFactory.createUserLeadDetailsJetAirwaysMOBILE(mobile));
        ccApplicationForm.setMobile(mobile);

        addrs1 = CommonUtils.nullSafe(bean.getAddress());
//		if(null!=addrs1)addrs1 = CommonUtils.replaceHtml(addrs1);
        leadDetails.setAddress(objFactory.createUserLeadDetailsJetAirwaysAddress(addrs1));
        ccApplicationForm.setAddress(addrs1);

        addrs2 = CommonUtils.nullSafe(bean.getAddress2());
//		if(null!=addrs2)addrs2 = CommonUtils.replaceHtml(addrs2);
        leadDetails.setAddress2(objFactory.createUserLeadDetailsJetAirwaysAddress2(addrs2));
        ccApplicationForm.setAddress2(addrs2);

        addrs3 = CommonUtils.nullSafe(bean.getAddress3());
//		if(null!=addrs3)addrs3 = CommonUtils.replaceHtml(addrs3);
        leadDetails.setAddress3(objFactory.createUserLeadDetailsJetAirwaysAddress3(addrs3));
        ccApplicationForm.setAddress3(addrs3);

        city = null != bean.getHcity() && !bean.getHcity().isEmpty() ? CommonUtils.nullSafe(bean.getHcity()) : CommonUtils.nullSafe(bean.getCity());
        leadDetails.setCity(objFactory.createUserLeadDetailsJetAirwaysCity(city));
        ccApplicationForm.setCity(city);
        bean.setCity(city);

        state = null != bean.getHstate() && !bean.getHstate().isEmpty() ? CommonUtils.nullSafe(bean.getHstate()) : CommonUtils.nullSafe(bean.getState1());
        leadDetails.setState(objFactory.createUserLeadDetailsJetAirwaysState(state));
        ccApplicationForm.setState(state);
        bean.setState1(state);

        pincode = CommonUtils.nullSafe(bean.getPinCode());
        leadDetails.setPincode(objFactory.createUserLeadDetailsJetAirwaysPincode(pincode));
        ccApplicationForm.setPinCode(pincode);

        phone = CommonUtils.nullSafe(bean.getPhone());
        leadDetails.setPHONE(objFactory.createUserLeadDetailsJetAirwaysPHONE(phone));
        ccApplicationForm.setPhone(phone);

        std = CommonUtils.nullSafe(bean.getStd());
        leadDetails.setSTD(objFactory.createUserLeadDetailsJetAirwaysSTD(std));
        ccApplicationForm.setStd(std);

        permascurr = CommonUtils.nullSafe(bean.getPeraddresssameascurr());
        leadDetails.setPeraddresssameascurr(objFactory.createUserLeadDetailsJetAirwaysPeraddresssameascurr(permascurr));
        ccApplicationForm.setPeraddresssameascurr(permascurr);

        paddr = CommonUtils.nullSafe(bean.getPermaddress());
//		if(null!=paddr)paddr = CommonUtils.replaceHtml(paddr);

        ccApplicationForm.setPermaddress(paddr);

        paddr2 = CommonUtils.nullSafe(bean.getPermaddress2());
//		if(null!=paddr2)paddr2 = CommonUtils.replaceHtml(paddr2);

        ccApplicationForm.setPermaddress2(paddr2);

        paddr3 = CommonUtils.nullSafe(bean.getPermaddress3());
//		if(null!=paddr3)paddr3 = CommonUtils.replaceHtml(paddr3);

        ccApplicationForm.setPermaddress3(paddr3);

        pcity = null != bean.getHcity1() && !bean.getHcity1().isEmpty() ? CommonUtils.nullSafe(bean.getHcity1()) : CommonUtils.nullSafe(bean.getPCity());

        ccApplicationForm.setPCity(pcity);
        bean.setPCity(pcity);

        pstate = null != bean.getHstate1() && !bean.getHstate1().isEmpty() ? CommonUtils.nullSafe(bean.getHstate1()) : CommonUtils.nullSafe(bean.getPState());

        ccApplicationForm.setPState(pstate);
        bean.setPState(pstate);

        ppincode = CommonUtils.nullSafe(bean.getPpinCode());

        ccApplicationForm.setPPinCode(ppincode);
        if (!bean.getPeraddresssameascurr().equals("Yes")) {
            leadDetails.setPermaddress(objFactory.createUserLeadDetailsJetAirwaysPermaddress(paddr));
            leadDetails.setPermaddress2(objFactory.createUserLeadDetailsJetAirwaysPermaddress2(paddr2));
            leadDetails.setPermaddress3(objFactory.createUserLeadDetailsJetAirwaysPermaddress3(paddr3));
            leadDetails.setPermcity(objFactory.createUserLeadDetailsJetAirwaysPermcity(pcity));
            leadDetails.setPermstate(objFactory.createUserLeadDetailsJetAirwaysPermstate(pstate));
            leadDetails.setPermpincode(objFactory.createUserLeadDetailsJetAirwaysPermpincode(ppincode));
        } else {
            leadDetails.setPermaddress(objFactory.createUserLeadDetailsJetAirwaysPermaddress(""));
            leadDetails.setPermaddress2(objFactory.createUserLeadDetailsJetAirwaysPermaddress2(""));
            leadDetails.setPermaddress3(objFactory.createUserLeadDetailsJetAirwaysPermaddress3(""));
            leadDetails.setPermcity(objFactory.createUserLeadDetailsJetAirwaysPermcity(""));
            leadDetails.setPermstate(objFactory.createUserLeadDetailsJetAirwaysPermstate(""));
            leadDetails.setPermpincode(objFactory.createUserLeadDetailsJetAirwaysPermpincode(""));

        }
        pancard = CommonUtils.nullSafe(bean.getPancard());
        leadDetails.setPANCARD(objFactory.createUserLeadDetailsJetAirwaysPANCARD(pancard));
        ccApplicationForm.setPancard(pancard);

        aadhar = CommonUtils.nullSafe(bean.getAadharNumber());
        leadDetails.setAadharnumber(objFactory.createUserLeadDetailsJetAirwaysAadharnumber(aadhar));
        ccApplicationForm.setAadharNumber(aadhar);

        if (!bean.getJetpriviligemembershipNumber().isEmpty()) {
            leadDetails.setJetPriviligeMember(objFactory.createUserLeadDetailsJetAirwaysJetPriviligeMember("Y"));
            ccApplicationForm.setJetpriviligemember("Y");
        }

        jpnumber = CommonUtils.nullSafe(bean.getJetpriviligemembershipNumber());
        leadDetails.setJetPriviligeMembershipNumber(
                objFactory.createUserLeadDetailsJetAirwaysJetPriviligeMembershipNumber(jpnumber));
        ccApplicationForm.setJetpriviligemembershipNumber(jpnumber);

        statement = CommonUtils.nullSafe(bean.getStmtTosent());
        leadDetails.setJetStatementTobeSentTO(objFactory.createUserLeadDetailsJetAirwaysJetStatementTobeSentTO(statement));

        qualification = CommonUtils.nullSafe(bean.getEduQualification());
        leadDetails.setEducationalQualification(objFactory.createUserLeadDetailsJetAirwaysEducationalQualification(qualification));
        ccApplicationForm.setEduQualification(commonService.getAmexEnumValues("Qualification", qualification));

        emptype = CommonUtils.nullSafe(bean.getEmploymentType());
        leadDetails.setEmploymentType(objFactory.createUserLeadDetailsJetAirwaysEmploymentType(emptype));
        ccApplicationForm.setEmploymentType(commonService.getAmexEnumValues("EMP_STATUS", emptype));

        income = CommonUtils.nullSafe(bean.getMonthlyIncome());
        leadDetails.setMonthlyInCome(objFactory.createUserLeadDetailsJetAirwaysMonthlyInCome(income));
        if (null != bean.getMonthlyIncome()) {
            ccApplicationForm.setMonthlyIncome(new BigDecimal(bean.getMonthlyIncome()));
        }
        company = CommonUtils.nullSafe(bean.getCompanyName());
        if (null != company) {
            company = CommonUtils.replaceHtml(company);
        }
        leadDetails.setCompanyName(objFactory.createUserLeadDetailsJetAirwaysCompanyName(company));
        ccApplicationForm.setCompanyName(company);

        oaddrs1 = CommonUtils.nullSafe(bean.getOAddress());
//		if(null!=oaddrs1)oaddrs1 = CommonUtils.replaceHtml(oaddrs1);
        leadDetails.setOADDRESS(objFactory.createUserLeadDetailsJetAirwaysOADDRESS(oaddrs1));
        ccApplicationForm.setOAddress(oaddrs1);

        oaddrs2 = CommonUtils.nullSafe(bean.getOAddress2());
//		if(null!=oaddrs2)oaddrs2 = CommonUtils.replaceHtml(oaddrs2);
        leadDetails.setOADDRESS2(objFactory.createUserLeadDetailsJetAirwaysOADDRESS2(oaddrs2));
        ccApplicationForm.setOAddress2(oaddrs2);

        oaddrs3 = CommonUtils.nullSafe(bean.getOAddress3());
//		if(null!=oaddrs3)oaddrs3 = CommonUtils.replaceHtml(oaddrs3);
        leadDetails.setOADDRESS3(objFactory.createUserLeadDetailsJetAirwaysOADDRESS3(oaddrs3));
        ccApplicationForm.setOAddress3(oaddrs3);

        ocity = null != bean.getHcity2() && !bean.getHcity2().isEmpty() ? CommonUtils.nullSafe(bean.getHcity2()) : CommonUtils.nullSafe(bean.getOCity());
        leadDetails.setOCity(objFactory.createUserLeadDetailsJetAirwaysOCity(ocity));
        ccApplicationForm.setOCity(ocity);
        bean.setOCity(ocity);

        ostate = null != bean.getHstate2() && !bean.getHstate2().isEmpty() ? CommonUtils.nullSafe(bean.getHstate2()) : CommonUtils.nullSafe(bean.getOState());
        leadDetails.setOState(objFactory.createUserLeadDetailsJetAirwaysOState(ostate));
        ccApplicationForm.setOState(ostate);
        bean.setOState(ostate);

        opincode = CommonUtils.nullSafe(bean.getOPincode());
        leadDetails.setOPincode(objFactory.createUserLeadDetailsJetAirwaysOPincode(opincode));
        ccApplicationForm.setOPincode(opincode);

        ophone = CommonUtils.nullSafe(bean.getOPhone());
        leadDetails.setOPHONE(objFactory.createUserLeadDetailsJetAirwaysOPHONE(ophone));
        ccApplicationForm.setOPhone(ophone);

        ostd = CommonUtils.nullSafe(bean.getOStd());
        leadDetails.setOSTD(objFactory.createUserLeadDetailsJetAirwaysOSTD(ostd));
        ccApplicationForm.setOStd(ostd);

        leadDetails.setAccesstype(objFactory.createUserLeadDetailsJetAirwaysAccesstype(""));

        leadDetails.setChosenCard(objFactory.createUserLeadDetailsJetAirwaysChosenCard("jetairways"));

        leadDetails.setConsentForAdditionalPreviliges(objFactory.createUserLeadDetailsJetAirwaysConsentForAdditionalPreviliges("Y"));
        leadDetails.setConsentForEmailCommunication(objFactory.createUserLeadDetailsJetAirwaysConsentForEmailCommunication("YES"));
        leadDetails.setConsentForInsurancePlanCommunication(objFactory.createUserLeadDetailsJetAirwaysConsentForInsurancePlanCommunication("Y"));
        leadDetails.setConsentForMarketingCommunicationOverPhone(objFactory.createUserLeadDetailsJetAirwaysConsentForMarketingCommunicationOverPhone("Y"));
        leadDetails.setConsentForOnlineStatements(objFactory.createUserLeadDetailsJetAirwaysConsentForOnlineStatements("YES"));
        leadDetails.setConsentForPaybackPointsEnrolment(objFactory.createUserLeadDetailsJetAirwaysConsentForPaybackPointsEnrolment("YES"));
        leadDetails.setCreditCardNumber(objFactory.createUserLeadDetailsJetAirwaysCreditCardNumber(""));
        leadDetails.setCreditCardType(objFactory.createUserLeadDetailsJetAirwaysCreditCardType("NA"));
        leadDetails.setPlatinumCardBillingPreference(objFactory.createUserLeadDetailsJetAirwaysPlatinumCardBillingPreference(""));
        leadDetails.setDisclaimer(objFactory.createUserLeadDetailsJetAirwaysDisclaimer("i agree"));

        partner = (null != partner) ? partner.trim() : "";
        leadDetails.setPartner(objFactory.createUserLeadDetailsJetAirwaysPartner(partner));

        password = (null != password) ? password.trim() : "";
        leadDetails.setPassword(objFactory.createUserLeadDetailsJetAirwaysPassword(password));

        username = (null != username) ? username.trim() : "";
        leadDetails.setUsername(objFactory.createUserLeadDetailsJetAirwaysUsername(username));

        if (offer_ID.equalsIgnoreCase("A1")) {

            leadDetails.setLeadCampaign(objFactory.createUserLeadDetailsJetAirwaysLeadCampaign("JETOIP"));
        } else {
            leadDetails.setLeadCampaign(objFactory.createUserLeadDetailsJetAirwaysLeadCampaign("JETDM"));
        }

        leadDetails.setCampaignCategorization(objFactory.createUserLeadDetailsJetAirwaysCampaignCategorization(offer_ID));
        leadDetails.setTeleCallerFlag(objFactory.createUserLeadDetailsJetAirwaysTeleCallerFlag("n"));
        leadDetails.setSourceOfLead(objFactory.createUserLeadDetailsJetAirwaysSourceOfLead(""));

        ccApplicationForm.setOfferId(offer_ID);
        ccApplicationForm.setPromocode(CommonUtils.nullSafe(bean.getPromocode(),""));

        ccApplicationForm.setPid(CommonUtils.nullSafe(bean.getPid()));//new pid
        SubmitGNAWithoutPrequalApplication submitGNAWithoutPrequalApplication = obj2.createSubmitGNAWithoutPrequalApplication();
        submitGNAWithoutPrequalApplication.setComposite(obj2.createSubmitGNAWithoutPrequalApplicationComposite(leadDetails));

        try {
            SSLContextUtil.sslManager();
            response = (SubmitGNAWithoutPrequalApplicationResponse) webServiceTemplate.marshalSendAndReceive(submitGNAWithoutPrequalApplication, SoapServicesUtil.addActionToSoap());

            StringResult messageRequest = new StringResult();
            StringResult messageResponse = new StringResult();

            Marshaller marshaller = webServiceTemplate.getMarshaller();
            marshaller.marshal(submitGNAWithoutPrequalApplication, messageRequest);
            marshaller.marshal(response, messageResponse);
            logger.info(messageResponse + "messageResponse");
            if (response.getSubmitGNAWithoutPrequalApplicationResult().getValue().isSuccess()) {
                ccApplicationForm.setAppStatus(response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getGnastatus().getValue());
            } else {
                ccApplicationForm.setAppStatus("Failed");
            }
            if (!response.getSubmitGNAWithoutPrequalApplicationResult().getValue().isDataValidationError()) {
                Integer id;
                ccApplicationForm.setReason(response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getDeclineReason().getValue());
                ccApplicationForm.setPcnNumber(response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getPCNNumber().getValue());
                ccApplicationForm.setApplicationInitiatedBy(CommonUtils.nullSafe(bean.getInitiatedBy(), "Self"));
                ccApplicationForm.setSourceOfCall(CommonUtils.nullSafe(bean.getSourceOfCall(), "NA"));
                ccApplicationForm.setFormStatus("Completed");
                if (!bean.getInitiationDate().isEmpty()) {
                    ccApplicationForm.setInitiationDate(CommonUtils.getDateFromString(bean.getInitiationDate()));
                } else {
                    ccApplicationForm.setInitiationDate(new Date());
                }
                ccApplicationForm.setAgentId(CommonUtils.nullSafe(bean.getAgentId(), "NA"));
                ccApplicationForm.setCreatedTime(new Date());
                ccApplicationForm.setCreatedBy(1);
                ccApplicationForm.setStatus((byte) 1);
                ccApplicationForm.setCcBankPartner(cardProductDAO.getCardProductById(bean.getCpNo()).getCcBankPartner());
                ccApplicationForm.setCcCardProduct(cardProductDAO.getCardProductById(bean.getCpNo()));
                ccApplicationForm.setUtmSource(CommonUtils.nullSafe(bean.getUtmSource(), "Direct"));
                ccApplicationForm.setUtmMedium(CommonUtils.nullSafe(bean.getUtmMedium(), "NA"));
                ccApplicationForm.setUtmCampaign(CommonUtils.nullSafe(bean.getUtmCampaign(), "NA"));
                id = amexCardDAO.submitAmexApplicationForm(ccApplicationForm);
                bean.setAppFormNo(id);//cr3
                System.out.println("otpval at amex" + bean.getOtpVal());
                if (0 != id) {
                    String statusOfOTP = commonService.UpdateApplicationForOtp(String.valueOf(bean.getMobile()), "1", bean.getFormNumber(), bean.getOtpTransactionId(), String.valueOf(id));

                    saveOrUpdateFormData(bean, id);
                    updateAmexStatus(id, messageRequest, messageResponse, response);

                }
                if ("Approved".equalsIgnoreCase(response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getGnastatus().getValue()) || PENDING.equalsIgnoreCase(
                        response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getGnastatus().getValue())) {
                    logger.info("AmexCardApplication Success ===>"
                            + response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getGnastatus().getValue());

                    return response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getGnastatus().getValue() + ","
                            + response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getPCNNumber().getValue();
                } else if ("gnadecline".equalsIgnoreCase(response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getGnastatus().getValue())) {
                    return PENDING + "," + response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getPCNNumber().getValue();
                } else {
                    return "amexdecline" + "," + " ";
                }
            } else if (response.getSubmitGNAWithoutPrequalApplicationResult().getValue().isDataValidationError()) {

                logger.info("AmexCardApplication Data Validation Error" + response
                        .getSubmitGNAWithoutPrequalApplicationResult().getValue().getValidationErrorDescription().getValue());
                return response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getValidationErrorDescription()
                        .getValue() + "," + "DVE";
            } else {
                return "amexdecline" + "," + " ";
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl applyAmexCardWebService() :", e);
        }
        logger.info("AmexCardApplication Success"
                + response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getGnastatus().getValue());
        return "";
    }

    @Override
    public CardDetailsBean getDefaultOfffer(CardDetailsBean cardDetailsBean) {
        CcOffers ccOffers = null;
        try {
            ccOffers = amexCardDAO.getDefaultOfffer(cardDetailsBean.getBpNo());
            if (null != ccOffers) {
                cardDetailsBean.setOfferId(ccOffers.getOfferId());
                cardDetailsBean.setOfferDesc(ccOffers.getOfferDesc());
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl getDefaultOfffer() :", e);
        }
        return cardDetailsBean;
    }

    public void updateAmexStatus(Integer id, StringResult messageRequest, StringResult messageResponse,
            SubmitGNAWithoutPrequalApplicationResponse response) {

        CcApplicationFormDetails ccApplicationForm = amexCardDAO.getApplicationFormById(id);
        CcAmexApplicationStatus ccAmexStatus = new CcAmexApplicationStatus();

        ccAmexStatus.setCcApplicationFormDetails(ccApplicationForm);
        ccAmexStatus.setCardName(ccApplicationForm.getCcCardProduct().getCardName());
        ccAmexStatus.setJpNumber(ccApplicationForm.getJetpriviligemembershipNumber());
        ccAmexStatus.setPcnNumber(CommonUtils.nullSafe(
                response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getPCNNumber().getValue()));
        ccAmexStatus.setWsRequest(messageRequest.toString());
        ccAmexStatus.setWsResponse(messageResponse.toString());
        if ("Approved".equalsIgnoreCase(
                response.getSubmitGNAWithoutPrequalApplicationResult().getValue().getGnastatus().getValue())
                || PENDING.equalsIgnoreCase(response.getSubmitGNAWithoutPrequalApplicationResult().getValue()
                        .getGnastatus().getValue())) {
            ccAmexStatus.setDecisionStatus(response.getSubmitGNAWithoutPrequalApplicationResult().getValue()
                    .getGnastatus().getValue());
        } else {
            ccAmexStatus.setDecisionStatus("Rejected");
        }
        ccAmexStatus.setDecisionDate(new Date());
        ccAmexStatus.setCreatedTime(new Date());
        ccAmexStatus.setCreatedBy(1);
        ccAmexStatus.setStatus((byte) 1);
        ccAmexStatus.setPid(CommonUtils.nullSafe(ccApplicationForm.getPid()));// for pid
        amexCardDAO.addAmexStatus(ccAmexStatus);
    }

    @Override
    public CardDetailsBean getOfferByJPNum(String jpNumber, CardDetailsBean cardDetailsBean) {
        CcMapOffers ccMapOffers = null;
        CcOffers ccOffers = null;
        try {
            ccMapOffers = amexCardDAO.getOfferByJPNum(jpNumber, cardDetailsBean.getBpNo());
                     System.out.println("ccMapOffers=============>"+ccMapOffers);

            if (null != ccMapOffers) {
                // System.out.println("========inside if condition=====>"+ccMapOffers);
                cardDetailsBean.setOfferId(ccMapOffers.getCcOffers().getOfferId());
                cardDetailsBean.setOfferDesc(ccMapOffers.getCcOffers().getOfferDesc());
            } else {
                System.out.println("========ccMapOffers else  if condition=====>"+ccMapOffers);
                ccOffers = amexCardDAO.getDefaultOfffer(cardDetailsBean.getBpNo());
                if (null != ccOffers) {
                    cardDetailsBean.setOfferId(ccOffers.getOfferId());
                    cardDetailsBean.setOfferDesc(ccOffers.getOfferDesc());
                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl getOfferByJPNum() :", e);
        }
        return cardDetailsBean;
    }

    @Override
    public JSONObject getOfferByJPNumber(String jpnumber, String cpNo) {
        CcMapOffers ccMapOffers = null;
        CcOffers ccOffers = null;
        JSONObject json = new JSONObject();
        try {
            CcCardProduct ccCardProduct = cardProductDAO.getCardProductById(Integer.parseInt(cpNo));
            ccMapOffers = amexCardDAO.getOfferByJPNum(jpnumber, ccCardProduct.getCcBankPartner().getBpNo());

            if (null != ccMapOffers) {
                // System.out.println("inside if"+ccMapOffers.getCcOffers().getOfferDesc());
                json.put("offerDesc", ccMapOffers.getCcOffers().getOfferDesc());
                json.put("offerId", ccMapOffers.getCcOffers().getOfferId());
            } else {
//                             System.out.println("inside else");
                ccOffers = amexCardDAO.getDefaultOfffer(ccCardProduct.getCcBankPartner().getBpNo());
                if (null != ccOffers) {
                    // System.out.println("inside if"+ccOffers.getOfferDesc());
                    json.put("offerDesc", ccOffers.getOfferDesc());
                    json.put("offerId", ccOffers.getOfferId());
                }
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl getOfferByJPNumber() :", e);
        }
        return json;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void saveOrUpdateFormData(AmexBean amexBean, int param) {

        try {
            CcBeApplicationFormDetails ccBeApplicationFormDetails = null;

            logger.info("form number saveOrUpdateFormData fuc===>" + amexBean.getFormNumber());
            System.out.println("form number saveOrUpdateFormData fuc===>" + amexBean.getFormNumber());
//                     System.out.println("===Form Number=="+amexBean.getFormNumber());
            ccBeApplicationFormDetails = amexCardDAO.checkFormNumber(amexBean.getFormNumber());
//                    System.out.println("====ccBeApplicationFormDetails object=====>"+ccBeApplicationFormDetails);
            String randomNumber = "";

            if (amexBean.getRandomNumber().isEmpty() || null == amexBean.getRandomNumber() || "NA".equalsIgnoreCase(amexBean.getRandomNumber())) {
                randomNumber = RandomStringUtils.random(20, true, true);
            } else {
                randomNumber = amexBean.getRandomNumber();
            }
            if (randomNumber.length() < 18) {
                randomNumber = RandomStringUtils.random(20, true, true);
            }
            amexBean.setRandomNumber(randomNumber);

            if (null != ccBeApplicationFormDetails) {
                logger.info("Amex form exist for update : " + amexBean.getFormNumber() + " BE_APP_FORM_No " + ccBeApplicationFormDetails.getBeAppFormNo());

                logger.info("inside if ccBeApplicationFormDetails not null");
                ccBeApplicationFormDetails = getBEApplicationDetails(ccBeApplicationFormDetails, amexBean);
                logger.info("Amex form details exist, mapping bean details: " + amexBean.getFormNumber() + "<-->" + ccBeApplicationFormDetails.getFormNumber() + " BE_APP_FORM_No " + ccBeApplicationFormDetails.getBeAppFormNo());
                logger.info("param" + param);
                if (0 != param) {
                    logger.info("at 0 param");
                    ccBeApplicationFormDetails.setFormStatus("Completed");
                } else {
                    logger.info("at modified else block");
                    ccBeApplicationFormDetails.setCreatedTime(new Date());
                    ccBeApplicationFormDetails.setModifiedTime(new Date());
                    ccBeApplicationFormDetails.setModifiedBy(1);
                }
                amexCardDAO.saveOrUpdateFormData(ccBeApplicationFormDetails);
            } else {
//                            System.out.println("New Amex form no for insert : "+amexBean.getFormNumber());
                logger.info("New Amex form no for insert : " + amexBean.getFormNumber());
//                                 System.out.println("=======form details is not existed=====>"+amexBean.getFormNumber());
                ccBeApplicationFormDetails = new CcBeApplicationFormDetails();

                ccBeApplicationFormDetails = getBEApplicationDetails(ccBeApplicationFormDetails, amexBean);
//				logger.info("New Amex form details, mapping bean details: "+amexBean.getFormNumber()+"<-->"+ccBeApplicationFormDetails.getFormNumber()+" BE_APP_FORM_No "+ccBeApplicationFormDetails.getBeAppFormNo());

                ccBeApplicationFormDetails.setFormStatus("Initiated");
                ccBeApplicationFormDetails.setInitiationDate(new Date());
                ccBeApplicationFormDetails.setCreatedTime(new Date());
                ccBeApplicationFormDetails.setCreatedBy(1);
                ccBeApplicationFormDetails.setStatus((byte) 1);
                amexCardDAO.save(ccBeApplicationFormDetails);
            }

            logger.info("Amex Application Save/Update form no: " + ccBeApplicationFormDetails.getFormNumber() + "  BEAPP form no" + ccBeApplicationFormDetails.getBeAppFormNo());
            System.out.println("Amex Application Save/Update form no: " + ccBeApplicationFormDetails.getFormNumber() + "  BEAPP form no" + ccBeApplicationFormDetails.getBeAppFormNo());
        } catch (Exception e) {
//                    e.printStackTrace();
//                    System.out.println("e"+e);
            logger.error("@@@@ Exception in AmexCardServiceImpl saveFormData() :", e);
        }
    }

    private CcBeApplicationFormDetails getBEApplicationDetails(CcBeApplicationFormDetails ccBeApplicationFormDetails, AmexBean amexBean) {
        String address = "";
        String address2 = "";
        String address3 = "";
        String city = "";
        String state = "";
        try {
            String firstName;
            String middleName;
            String lastName;

            firstName = CommonUtils.nullSafe(amexBean.getFname(), "");
            middleName = CommonUtils.nullSafe(amexBean.getMname(), "");
            lastName = CommonUtils.nullSafe(amexBean.getLname(), "");

            firstName = CommonUtils.checkStringLength(firstName);
            middleName = CommonUtils.checkStringLength(middleName);
            lastName = CommonUtils.checkStringLength(lastName);

            ccBeApplicationFormDetails.setFname(firstName);
            ccBeApplicationFormDetails.setMname(middleName);
            ccBeApplicationFormDetails.setLname(lastName);
            if (null != amexBean.getDateOfBirth() && !amexBean.getDateOfBirth().isEmpty()) {
                ccBeApplicationFormDetails.setDateOfBirth(CommonUtils.setDateForAmexAppl(amexBean.getDateOfBirth()));
            }
            if (null != amexBean.getGender() && !amexBean.getGender().isEmpty()) {
                ccBeApplicationFormDetails.setGender("0".equalsIgnoreCase(CommonUtils.nullSafe(amexBean.getGender())) ? "Male" : "Female");
            }
            String Email = CommonUtils.nullSafe(amexBean.getEmail(), "");
            ccBeApplicationFormDetails.setEmail(Email);
            String mobile = CommonUtils.nullSafe(amexBean.getMobile(), "");
            ccBeApplicationFormDetails.setMobile(mobile);

            String qualification = CommonUtils.nullSafe(amexBean.getEduQualification());
            ccBeApplicationFormDetails.setEduQualification(commonService.getAmexEnumValues("Qualification", qualification));
            if (null == amexBean.getMonthlyIncome()) {
                amexBean.setMonthlyIncome(CcPortalConstants.EMPTY);
            }
            if (!amexBean.getMonthlyIncome().equals(CcPortalConstants.EMPTY)) {
                ccBeApplicationFormDetails.setMonthlyIncome(new BigDecimal(amexBean.getMonthlyIncome()));
            }
            ccBeApplicationFormDetails.setPancard(CommonUtils.nullSafe(amexBean.getPancard()));
            ccBeApplicationFormDetails.setAadharNumber(CommonUtils.nullSafe(amexBean.getAadharNumber()));

            address = CommonUtils.nullSafe(amexBean.getAddress());
            address2 = CommonUtils.nullSafe(amexBean.getAddress2());
            address3 = CommonUtils.nullSafe(amexBean.getAddress3());

            address = !address.isEmpty() && address.length() > 24 ? address.substring(0, 23) : address;
            address2 = !address2.isEmpty() && address2.length() > 24 ? address2.substring(0, 23) : address2;
            address3 = !address3.isEmpty() && address3.length() > 24 ? address3.substring(0, 23) : address3;

            ccBeApplicationFormDetails.setAddress(address);
            ccBeApplicationFormDetails.setAddress2(address2);
            ccBeApplicationFormDetails.setAddress3(address3);

            ccBeApplicationFormDetails.setPinCode(CommonUtils.nullSafe(amexBean.getPinCode()));
            city = null != amexBean.getHcity() && !amexBean.getHcity().isEmpty() ? CommonUtils.nullSafe(amexBean.getHcity()) : CommonUtils.nullSafe(amexBean.getCity());
            state = null != amexBean.getHstate() && !amexBean.getHstate().isEmpty() ? CommonUtils.nullSafe(amexBean.getHstate()) : CommonUtils.nullSafe(amexBean.getState1());
            ccBeApplicationFormDetails.setCity(city);
            ccBeApplicationFormDetails.setState(state);
            String perAdd = CommonUtils.nullSafe(amexBean.getPeraddresssameascurr());
            ccBeApplicationFormDetails.setPeraddresssameascurr(perAdd);
            if ("Yes".equalsIgnoreCase(perAdd)) {
                ccBeApplicationFormDetails.setPermaddress(address);
                ccBeApplicationFormDetails.setPermaddress2(address2);
                ccBeApplicationFormDetails.setPermaddress3(address3);
                ccBeApplicationFormDetails.setPPinCode(CommonUtils.nullSafe(amexBean.getPinCode()));
                ccBeApplicationFormDetails.setPState(state);
                ccBeApplicationFormDetails.setPCity(city);
            } else {
                ccBeApplicationFormDetails.setPermaddress(CommonUtils.nullSafe(amexBean.getPermaddress()));
                ccBeApplicationFormDetails.setPermaddress2(CommonUtils.nullSafe(amexBean.getPermaddress2()));
                ccBeApplicationFormDetails.setPermaddress3(CommonUtils.nullSafe(amexBean.getPermaddress3()));
                ccBeApplicationFormDetails.setPPinCode(CommonUtils.nullSafe(amexBean.getPpinCode()));
                city = null != amexBean.getHcity1() && !amexBean.getHcity1().isEmpty() ? CommonUtils.nullSafe(amexBean.getHcity1()) : CommonUtils.nullSafe(amexBean.getPCity());
                state = null != amexBean.getHstate1() && !amexBean.getHstate1().isEmpty() ? CommonUtils.nullSafe(amexBean.getHstate1()) : CommonUtils.nullSafe(amexBean.getPState());
                ccBeApplicationFormDetails.setPState(state);
                ccBeApplicationFormDetails.setPCity(city);
            }
            ccBeApplicationFormDetails.setOAddress(CommonUtils.nullSafe(amexBean.getOAddress()));
            ccBeApplicationFormDetails.setOAddress2(CommonUtils.nullSafe(amexBean.getOAddress2()));
            ccBeApplicationFormDetails.setOAddress3(CommonUtils.nullSafe(amexBean.getOAddress3()));
            ccBeApplicationFormDetails.setOPincode(CommonUtils.nullSafe(amexBean.getOPincode()));
            city = null != amexBean.getHcity2() && !amexBean.getHcity2().isEmpty() ? CommonUtils.nullSafe(amexBean.getHcity2()) : CommonUtils.nullSafe(amexBean.getOCity());
            state = null != amexBean.getHstate2() && !amexBean.getHstate2().isEmpty() ? CommonUtils.nullSafe(amexBean.getHstate2()) : CommonUtils.nullSafe(amexBean.getOState());
            ccBeApplicationFormDetails.setOState(state);
            ccBeApplicationFormDetails.setOCity(city);

            ccBeApplicationFormDetails.setEmploymentType(commonService.getAmexEnumValues("EMP_STATUS", CommonUtils.nullSafe(amexBean.getEmploymentType())));
            ccBeApplicationFormDetails.setCompanyName(CommonUtils.nullSafe(amexBean.getCompanyName()));

            ccBeApplicationFormDetails.setJetpriviligemembershipNumber(amexBean.getJetpriviligemembershipNumber());
            ccBeApplicationFormDetails.setJetpriviligemember("Y");
            ccBeApplicationFormDetails.setJetpriviligemembershipTier(CommonUtils.nullSafe(amexBean.getJetpriviligemembershipTier()));

            ccBeApplicationFormDetails.setStd(CommonUtils.nullSafe(amexBean.getStd()));
            ccBeApplicationFormDetails.setPhone(CommonUtils.nullSafe(amexBean.getPhone()));

            ccBeApplicationFormDetails.setOStd(CommonUtils.nullSafe(amexBean.getOStd()));
            ccBeApplicationFormDetails.setOPhone(CommonUtils.nullSafe(amexBean.getOPhone()));

            ccBeApplicationFormDetails.setAppStatus("NA");

            ccBeApplicationFormDetails.setRandomNumber(CommonUtils.nullSafe(amexBean.getRandomNumber(), "NA"));
            logger.info("form number at getBEApplicationDetails()" + amexBean.getFormNumber());
            ccBeApplicationFormDetails.setFormNumber(CommonUtils.nullSafe(amexBean.getFormNumber()));
            ccBeApplicationFormDetails.setUniqueFormNumber(CommonUtils.nullSafe(amexBean.getFormNumber()));
//			ccBeApplicationFormDetails.setUniqueFormNumber("qVkwdWLqG5fd6PfTBJ0p");

            ccBeApplicationFormDetails.setApplicationInitiatedBy(CommonUtils.nullSafe(amexBean.getInitiatedBy(), "Self"));
            ccBeApplicationFormDetails.setSourceOfCall(CommonUtils.nullSafe(amexBean.getSourceOfCall(), "NA"));
            ccBeApplicationFormDetails.setUtmSource(CommonUtils.nullSafe(amexBean.getUtmSource(), "Direct"));
            ccBeApplicationFormDetails.setUtmMedium(CommonUtils.nullSafe(amexBean.getUtmMedium(), "NA"));
            ccBeApplicationFormDetails.setUtmCampaign(CommonUtils.nullSafe(amexBean.getUtmCampaign(), "NA"));
            // sprint 52
            String inactive=amexBean.getEmailInactiveFlag();
           inactive= ("".equals(inactive) )?"N":inactive;
//            System.out.println("inactive"+inactive);
            ccBeApplicationFormDetails.setEmailFlag(CommonUtils.nullSafe(inactive, "N"));
            
            ccBeApplicationFormDetails.setPromocode(CommonUtils.nullSafe(amexBean.getPromocode(),""));
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl getBEApplicationDetails() :", e);
        }
        return ccBeApplicationFormDetails;
    }

    @Override
    public AmexBean getBEAmexFormDetails(String randomNumber, AmexBean amexBean) {
        CcBeApplicationFormDetails ccBeApplicationFormDetails = null;
        String fName;
        String mName;
        String lName;
        String fullName;
        String address1;
        String address2;
        String address3;
        String pincode;
        String state;
        String city;
        String std;
        String phone;
        try {
            logger.info("randomNumber in getBEAmexFormDetails func===>" + randomNumber);
            System.out.println("randomNumber in getBEAmexFormDetails func===>" + randomNumber);

            ccBeApplicationFormDetails = amexCardDAO.getBEAmexFormDetails(randomNumber);
            logger.info("getBEAmexFormDetails func ccBeApplicationFormDetails=======>" + ccBeApplicationFormDetails);
            System.out.println("getBEAmexFormDetails func ccBeApplicationFormDetails=======>" + ccBeApplicationFormDetails);
            if (null != ccBeApplicationFormDetails) {

                fName = CommonUtils.nullSafe(ccBeApplicationFormDetails.getFname());
                mName = CommonUtils.nullSafe(ccBeApplicationFormDetails.getMname());
                lName = CommonUtils.nullSafe(ccBeApplicationFormDetails.getLname());
                fullName = fName.concat(" ").concat(mName).concat(" ").concat(lName);
                amexBean.setFname(fName);
                amexBean.setMname(mName);
                amexBean.setLname(lName);
                amexBean.setFullName(fullName);

                amexBean.setEmail(CommonUtils.nullSafe(ccBeApplicationFormDetails.getEmail()));
                amexBean.setMobile(CommonUtils.nullSafe(ccBeApplicationFormDetails.getMobile()));

                if (null != ccBeApplicationFormDetails.getDateOfBirth()) {
                    amexBean.setDateOfBirth(CommonUtils.getDate(ccBeApplicationFormDetails.getDateOfBirth(), "MM-dd-yyyy"));
                }
                if (null != ccBeApplicationFormDetails.getGender() && !ccBeApplicationFormDetails.getGender().isEmpty()) {
                    amexBean.setGender("Male".equals(CommonUtils.nullSafe(ccBeApplicationFormDetails.getGender())) ? "0" : "1");
                }

                String enumData = "";
                if (!ccBeApplicationFormDetails.getEduQualification().equalsIgnoreCase("")) {
                    Map<Byte, String> enumMap = commonService.getEnumValues("Qualification");
                    for (Entry<Byte, String> entry : enumMap.entrySet()) {
                        if (entry.getValue().contains(CommonUtils.nullSafe(ccBeApplicationFormDetails.getEduQualification()))) {
                            enumData = entry.getValue().split("-")[0];
                        }
                    }
                } else {

                    enumData = "";
                }

                amexBean.setEduQualification(enumData);
                amexBean.setAadharNumber(CommonUtils.nullSafe(ccBeApplicationFormDetails.getAadharNumber()));
                amexBean.setPancard(CommonUtils.nullSafe(ccBeApplicationFormDetails.getPancard()));

                if (null != ccBeApplicationFormDetails.getMonthlyIncome()) {
                    amexBean.setMonthlyIncome(CommonUtils.checkForDecimal(ccBeApplicationFormDetails.getMonthlyIncome()));
                }
                address1 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getAddress());
                address2 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getAddress2());
                address3 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getAddress3());
                city = CommonUtils.nullSafe(ccBeApplicationFormDetails.getCity());
                pincode = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPinCode());
                state = CommonUtils.nullSafe(ccBeApplicationFormDetails.getState());

                amexBean.setAddress(address1);
                amexBean.setAddress2(address2);
                amexBean.setAddress3(address3);
                amexBean.setCity(city);
                amexBean.setState1(state);
                amexBean.setPinCode(pincode);

                if (!(address1.isEmpty())) {
                    amexBean.setResedentialAddress(
                            address1 + " " + address2 + " " + address3 + " " + state + " " + city + " " + pincode);
                }
                amexBean.setPeraddresssameascurr(
                        CommonUtils.nullSafe(ccBeApplicationFormDetails.getPeraddresssameascurr()));

                address1 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPermaddress());
                address2 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPermaddress2());
                address3 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPermaddress3());
                city = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPCity());
                pincode = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPPinCode());
                state = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPState());

                amexBean.setPermaddress(address1);
                amexBean.setPermaddress2(address2);
                amexBean.setPermaddress3(address3);
                amexBean.setPCity(city);
                amexBean.setPState(state);
                amexBean.setPpinCode(pincode);

                if (!(address1.isEmpty())) {
                    amexBean.setPermanentAddress(address1 + " " + address2 + " " + address3 + " " + state + " " + city + " " + pincode);
                }

                amexBean.setJetpriviligemembershipTier(CommonUtils.nullSafe(ccBeApplicationFormDetails.getJetpriviligemembershipTier()));
                amexBean.setJetpriviligemembershipNumber(CommonUtils.nullSafe(ccBeApplicationFormDetails.getJetpriviligemembershipNumber()));
                if (!ccBeApplicationFormDetails.getEmploymentType().isEmpty()) {
                    amexBean.setEmploymentType("Salaried".equals(CommonUtils.nullSafe(ccBeApplicationFormDetails.getEmploymentType())) ? "E" : "SE");
                }
                amexBean.setCompanyName(CommonUtils.nullSafe(ccBeApplicationFormDetails.getCompanyName()));

                address1 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOAddress());
                address2 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOAddress2());
                address3 = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOAddress3());
                city = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOCity());
                pincode = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOPincode());
                state = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOState());

                amexBean.setOAddress(address1);
                amexBean.setOAddress2(address2);
                amexBean.setOAddress3(address3);
                amexBean.setOCity(city);
                amexBean.setOState(state);
                amexBean.setOPincode(pincode);

                if (!(address1.isEmpty())) {
                    amexBean.setCompanyAddress(address1 + " " + address2 + " " + address3 + " " + state + " " + city + " " + pincode);
                }

                std = CommonUtils.nullSafe(ccBeApplicationFormDetails.getStd());
                phone = CommonUtils.nullSafe(ccBeApplicationFormDetails.getPhone());

                amexBean.setPhone(phone);
                amexBean.setStd(std);
                amexBean.setHomeNumber(std + " " + phone);

                std = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOStd());
                phone = CommonUtils.nullSafe(ccBeApplicationFormDetails.getOPhone());

                amexBean.setOPhone(phone);
                amexBean.setOStd(std);
                if (!std.isEmpty()) {
                    amexBean.setOfficeNumber(std + " " + phone);
                }
                logger.info("form number if not null==>" + ccBeApplicationFormDetails.getFormNumber() + "randomNumber==>" + randomNumber);
                System.out.println("form number if not null==>" + ccBeApplicationFormDetails.getFormNumber() + "randomNumber==>" + randomNumber);
                amexBean.setInitiatedBy(CommonUtils.nullSafe(ccBeApplicationFormDetails.getApplicationInitiatedBy()));
                amexBean.setSourceOfCall(CommonUtils.nullSafe(ccBeApplicationFormDetails.getSourceOfCall()));
                amexBean.setFormNumber(CommonUtils.nullSafe(ccBeApplicationFormDetails.getFormNumber()));
                amexBean.setRandomNumber(randomNumber);
                amexBean.setInitiationDate(CommonUtils.getDate(ccBeApplicationFormDetails.getInitiationDate(), "dd-MM-yyyy HH:mm:ss"));
                amexBean.setAgentId(CommonUtils.nullSafe(ccBeApplicationFormDetails.getAgentId()));
            }

        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl getBEAmexFormDetails() :", e);
        }
        return amexBean;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String saveCallMeDetails(CallMeBean callMeBean) {
        String status = "";
        CcMemberDetails ccMemberDetails = new CcMemberDetails();
        Map<String, String> emailMap = new LinkedHashMap<>();
        String name;
        String mobile;
        String email;
        String jpNumber;
        String formNumber;
        try {
            name = CommonUtils.nullSafe(callMeBean.getSocialName());
            ccMemberDetails.setName(name);
            emailMap.put("Name - ", name);
            mobile = CommonUtils.nullSafe(callMeBean.getSocialPhone());
            ccMemberDetails.setMobile(mobile);
            emailMap.put("Mobile Number - ", mobile);
            email = CommonUtils.nullSafe(callMeBean.getSocialEmail());
            ccMemberDetails.setEmail(email);
            emailMap.put("Email - ", email);
            jpNumber = CommonUtils.nullSafe(callMeBean.getSocialjpNumber());
            ccMemberDetails.setJetpriviligemembershipNumber(jpNumber);
            emailMap.put("JPNumber - ", jpNumber);
            formNumber = CommonUtils.nullSafe(callMeBean.getFormNumber());
            ccMemberDetails.setFormNumber(formNumber);
            ccMemberDetails.setPageName(CommonUtils.nullSafe(callMeBean.getPageName()));
            ccMemberDetails.setCardName(CommonUtils.nullSafe(callMeBean.getCardName()));
            
            emailMap.put("Form Number - ", formNumber);
            ccMemberDetails.setCreatedTime(new Date());
            ccMemberDetails.setCreatedBy(1);
            ccMemberDetails.setStatus((byte) 1);

            status = amexCardDAO.saveCallMeDetails(ccMemberDetails);
            if (CcPortalConstants.SUCCESS.equalsIgnoreCase(status)) {
                CallMeSilverPopApi(callMeBean);
            }
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl saveCallMeDetails() :", e);
        }
        return status;
    }

    @Override
    public void FormInactiveSilverPopApi(AmexBean amexBean) {
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            logger.info("formactive silver pop call for " + amexBean.getFname() + " Mobile no:" + amexBean.getMobile());
//            TransactRequestType transReqType = new TransactRequestType();
//            transReqType.setCampaign_id(inactiveid);//campaign id
//            transReqType.setTransaction_id(transReqType.getCampaign_id());
//            transReqType.setShow_all_send_details(true);
            HashMap<String, String> saveColumns = new HashMap();
            saveColumns.put("First_Name", amexBean.getFname());
            saveColumns.put("Last_Name", amexBean.getLname());
            saveColumns.put("Form_Number", amexBean.getFormNumber());
            saveColumns.put("JP_Number", amexBean.getJetpriviligemembershipNumber());
            saveColumns.put("Mobile_Number", amexBean.getMobile());
            saveColumns.put("Email", agentMailId);
            saveColumns.put("Member_EmailID", amexBean.getEmail());
            Date date = new Date();
            String strDate = formatter.format(date);
            saveColumns.put("Application_Date", strDate);
            saveColumns.put("URL", amexBean.getRandomNumber());

//            transReqType.setColumns(saveColumns);
            //Enter the email id to which the mail is to be send
//            transReqType.setEmail(agentMailId);
//            transReqType.setBody_type("HTML");

//            SilverPopApiCards.callSilverPopApi(url, clientId, clientSecret, refreshToken);
            
            SendGridMail sg = new SendGridMail();
            sg.sendMail(sendGridUrl, sendGridKey, saveColumns, sendGridInactiveid, agentMailId, amexBean.getFormNumber(), simplicaUserName, simplicaPassword);
            
//			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
//			List<ColumnElementType> columnElementarray = new ArrayList<>();
//			
//			ColumnElementType colEleMenType1 = new ColumnElementType();
//			colEleMenType1.setNAME("First_Name");
//			colEleMenType1.setVALUE(amexBean.getFname());
//			
//			ColumnElementType colEleMenType2 = new ColumnElementType();
//			colEleMenType2.setNAME("Last_Name");
//			colEleMenType2.setVALUE(amexBean.getLname());
//			
//			ColumnElementType colEleMenType3 = new ColumnElementType();
//			colEleMenType3.setNAME("Form_Number");
//			colEleMenType3.setVALUE(amexBean.getFormNumber());
//			
//			ColumnElementType colEleMenType4 = new ColumnElementType();
//			colEleMenType4.setNAME("JP_Number");
//			colEleMenType4.setVALUE(amexBean.getJetpriviligemembershipNumber());
//			
//			ColumnElementType colEleMenType5 = new ColumnElementType();
//			colEleMenType5.setNAME("Mobile_Number");
//			colEleMenType5.setVALUE(amexBean.getMobile());
//			
//			ColumnElementType colEleMenType6 = new ColumnElementType();
//			colEleMenType6.setNAME("Email");
//			colEleMenType6.setVALUE("parvati.gholap@vernost.in");
////                      colEleMenType6.setVALUE("poonam.velaskar@vernost.in"); 
//			
//			ColumnElementType colEleMenType7 = new ColumnElementType();
//			colEleMenType7.setNAME("Member_EmailID");
//			colEleMenType7.setVALUE(amexBean.getEmail());
//			
//			ColumnElementType colEleMenType8 = new ColumnElementType();
//			colEleMenType8.setNAME("Application_Date");
//			Date date = new Date();
//			String strDate = formatter.format(date);
//			colEleMenType8.setVALUE(strDate);
//			
//			ColumnElementType colEleMenType9 = new ColumnElementType();
//			colEleMenType9.setNAME("URL");
//			colEleMenType9.setVALUE(amexBean.getRandomNumber());
//			
//			columnElementarray.add(colEleMenType1);
//			columnElementarray.add(colEleMenType2);
//			columnElementarray.add(colEleMenType3);
//			columnElementarray.add(colEleMenType4);
//			columnElementarray.add(colEleMenType5);
//			columnElementarray.add(colEleMenType6);
//			columnElementarray.add(colEleMenType7);
//			columnElementarray.add(colEleMenType8);
//			columnElementarray.add(colEleMenType9);
//			ColumnElementType[] elementArray = new ColumnElementType[columnElementarray.size()];
//			elementArray = columnElementarray.toArray(elementArray);
//                        logger.info("Amex inactive silver pop calling-->User mobile-->"+amexBean.getMobile()+" user first name-->"+amexBean.getFname()+" user email--"+amexBean.getEmail());
//			SilverPopUtil.silverPopAPI(elementArray, silverPopuserName, silverPoppassword, inactiveListId);
        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl FormInactiveSilverPopApi() :", e);
        }
    }

    private void CallMeSilverPopApi(CallMeBean callMeBean) {
//        try {
         try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
//                    System.out.println("callmeSilverpop");
//            TransactRequestType transReqType = new TransactRequestType();
//            transReqType.setCampaign_id(callmeid);//campaign id
//            transReqType.setTransaction_id(transReqType.getCampaign_id());
//            transReqType.setShow_all_send_details(true);
            HashMap<String, String> saveColumns = new HashMap();
            saveColumns.put("First_Name", CommonUtils.nullSafe(callMeBean.getSocialName()));
            saveColumns.put("Last_Name", "");
            saveColumns.put("Form_Number", CommonUtils.nullSafe(callMeBean.getFormNumber()));
            saveColumns.put("Member_EmailID", CommonUtils.nullSafe(callMeBean.getSocialEmail()));
            saveColumns.put("Mobile_Number", CommonUtils.nullSafe(callMeBean.getSocialPhone()));
            saveColumns.put("JP_Number", CommonUtils.nullSafe(callMeBean.getSocialjpNumber()));
            saveColumns.put("URL", CommonUtils.nullSafe(callMeBean.getPageName()));
             
            String cardName=callMeBean.getCardName();
            if (cardName.contains("<sup>")) {
               
                cardName = cardName.replaceAll("<sup>", "");
                cardName = cardName.replaceAll("</sup>", "");
            }
            if (cardName.contains("&reg")) {
                cardName = cardName.replaceAll("&reg;", "");
               
            }
            
            saveColumns.put("Card_name", CommonUtils.nullSafe(cardName));
            saveColumns.put("Email", callmeAgentId);
//            transReqType.setColumns(saveColumns);
//            transReqType.setEmail(callmeAgentId); //for prod
//            transReqType.setBody_type("HTML");

//            SilverPopApiCards.callSilverPopApi(url, clientId, clientSecret, refreshToken);
            
            SendGridMail sg = new SendGridMail();
            sg.sendMail(sendGridUrl, sendGridKey, saveColumns, sendGridCallmeid, callmeAgentId, callMeBean.getFormNumber(), simplicaUserName, simplicaPassword);

        } catch (Exception e) {
            logger.error("@@@@ Exception in AmexCardServiceImpl CallMeSilverpopApi() :", e);
        }
    }

    @Override
    public String getFormNumber(String formNumber) {
        String FormNumber = "";
        List form = new ArrayList();
        try {

            form = amexCardDAO.getFormNumber(formNumber);

            FormNumber = (String) form.get(0);
        } catch (Exception e) {
//                 System.out.println("@@@@ Exception in AmexCardServiceImpl getFormNumber() :"+e);
            logger.error("@@@@ Exception in AmexCardServiceImpl getFormNumber() :", e);
        }

        return FormNumber;
    }
    


}
