package com.cbc.portal.service.impl;

import com.cbc.portal.DAO.CommonDAO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.xml.transform.StringResult;

import com.cbc.portal.DAO.MemberEnrollDAO;
import com.cbc.portal.beans.EnrollBean;
import com.cbc.portal.entity.CcMemberEnrollment;
import com.cbc.portal.service.MemberEnrollService;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.EncryptDetail;
import com.skywards.mercator_cris.Address;
import com.skywards.mercator_cris.ArrayOfAddress;
import com.skywards.mercator_cris.ArrayOfContact;
import com.skywards.mercator_cris.Contact;
import com.skywards.mercator_cris.Member;
import com.skywards.mercator_cris.MemberEnrollment;
import com.skywards.mercator_cris.MemberEnrollmentResponse;
import com.skywards.mercator_cris.SkywardsProfile;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javassist.bytecode.stackmap.BasicBlock;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import org.apache.tools.ant.util.Base64Converter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Component
@Transactional(readOnly = true)
public class MemberEnrollServiceImpl implements MemberEnrollService {

    private Logger logger = Logger.getLogger(MemberEnrollServiceImpl.class.getName());

    @Autowired
    private WebServiceTemplate webServiceTemplateCris;

    @Autowired
    private MemberEnrollDAO memberEnrollDAO;

    @Value("${application.trust_verification.url}")
    String verificationUrl;
    @Value("${application.mac.soap.url}")
    String Mac3Url;

    @Autowired
    private CommonDAO commonDAO;
    
    @Value("${application.pii.encryption.strSoapURL1}")
	String strSoapURL1;
	@Value("${application.pii.encrypt.strSOAPAction1}")
	String soapActEnc;

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public Map memberEnrollment(EnrollBean enrollBean, HttpServletRequest request) {
        String title = "";
        String firstName = "";
        String lastName = "";
        String dateOfBirth = "";
        String gender = "";
        String isdcode = "";
        String mobileNumber = "";
        String emailId = "";
        String password = "";
        String message = "";
        String jpnumber = "";
        String city = "";
        String mediaCode = "";
        String cookieName = "";
        String cookieValue = "";
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        String ResponseDescription = "";

        Map< String, String> hashMap
                = new HashMap< String, String>();
        ipAddress = (null != ipAddress) ? ipAddress.trim() : "";
        int resjpnum = 0;
        StringResult messageRequest = new StringResult();
        StringResult messageResponse = new StringResult();

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("JSESSIONID")) {
                    cookieName = cookie.getName();
                    cookieValue = cookie.getValue();
                    //do something
                    //value can be retrieved using #cookie.getValue()
                }
            }
        }
        CcMemberEnrollment ccMemberErollment = new CcMemberEnrollment();

        DateFormat sourceDf = new SimpleDateFormat("dd-MM-yyyy");

        MemberEnrollment enrollment = new MemberEnrollment();
        Member member = new Member();
        SkywardsProfile profile = new SkywardsProfile();
        Address address = new Address();
        ArrayOfAddress arrayOfAddress = new ArrayOfAddress();
        ArrayOfContact arrayOfContact = new ArrayOfContact();
        Contact contact = new Contact();
        MemberEnrollmentResponse response = null;
        XMLGregorianCalendar xmlGregorianCalendar = null;
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(new Date());

        try {

            title = CommonUtils.nullSafe(enrollBean.getEnrollTitle(), "");
            firstName = CommonUtils.nullSafe(enrollBean.getEnrollFname(), "");
            lastName = CommonUtils.nullSafe(enrollBean.getEnrollLname(), "");

            city = CommonUtils.nullSafe(enrollBean.getEnrollCity(), "");
            dateOfBirth = CommonUtils.nullSafe(enrollBean.getEnrollDob(), "");
            if (dateOfBirth.length() > 0) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date date = null;
                date = sdf.parse(dateOfBirth);
                gregorianCalendar.setTime(date);
                xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
            }//if dateOfBirth

            gender = CommonUtils.nullSafe(enrollBean.getEnrollGender(), "");
            if ("0".equalsIgnoreCase(gender)) {
                gender = "M";
            } else {
                gender = "F";
            }
            if ("Mr".equalsIgnoreCase(title)) {
                gender = "M";
            } else if ("Ms".equalsIgnoreCase(title) || "Mrs".equalsIgnoreCase(title)) {
                gender = "F";
            }

            mobileNumber = CommonUtils.nullSafe(enrollBean.getEnrollPhone(), "");;
            emailId = CommonUtils.nullSafe(enrollBean.getEnrollemail(), "");
            password = CommonUtils.nullSafe(enrollBean.getHashPassword(), "");
            int enrollbp = enrollBean.getEnrollbpNo();
            mediaCode = memberEnrollDAO.getEnrollMediaCode(enrollbp);

            member.setTitle(title);
            member.setFirstName(firstName);
            member.setLastName(lastName);
            member.setBirthDate(xmlGregorianCalendar);
            member.setEmailAddress(emailId);
            member.setPassword(password);
            member.setEcmCode(mediaCode);
            member.setGender(gender);

            address.setCountry("IN");
            address.setIsPreferred(true);
            address.setUsages("HOME");
            address.setTown(city);
            arrayOfAddress.getAddress().add(address);

            contact.setContactType("MOBIL");
            contact.setIsdCode(isdcode);
            contact.setContactNumber(mobileNumber);
            contact.setPreferred("true");
            arrayOfContact.getContact().add(contact);

            profile.setMember(member);
            profile.setAddresses(arrayOfAddress);
            profile.setContacts(arrayOfContact);

            enrollment.setSkywardsProfile(profile);

            hashMap = request(enrollBean, cookieName, cookieValue, ipAddress);

            for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                if (entry.getKey().equalsIgnoreCase("JPNumber")) {
                    jpnumber = entry.getValue();
                } else if (entry.getKey().equalsIgnoreCase("ResponseDescription")) {
                    ResponseDescription = entry.getValue();
                }

            }

            logger.info("final JPNUMBER" + jpnumber);

        } catch (Exception e) {
            enrollBean.setMessage("Some error occurred, please try after sometime.");
            logger.error("@@@@ Exception in MemberEnrollmentServiceImpl at memberEnrollment() :", e);
        }

        if (jpnumber.length() == 11 && jpnumber != null) {
            jpnumber = jpnumber.substring(2);
        }
        hashMap.put("JPNumber", jpnumber);
        hashMap.put("ResponseDescription", ResponseDescription);

        logger.info("****final JPNUMBER****" + jpnumber);
        return hashMap;
    }

    public Map request(EnrollBean enrollBean, String cookieName, String cookieValue, String ipaddress) throws ParseException {
        CcMemberEnrollment ccMemberErollment = new CcMemberEnrollment();
        String JP_number = "";
        String gender = "";
        Boolean TandCcheck;
        Boolean RecieveMArketingcheck;
        List<String> list = new ArrayList<>();
        HashMap< String, String> hm
                = new HashMap< String, String>();

        if ("Mr".equalsIgnoreCase(enrollBean.getEnrollTitle())) {

            gender = "M";
        } else if ("Ms".equalsIgnoreCase(enrollBean.getEnrollTitle()) || "Mrs".equalsIgnoreCase(enrollBean.getEnrollTitle())) {
            gender = "F";
        }
        String TandC = enrollBean.getTermsAndConditionsEnroll();
        String RecieveMarketing = enrollBean.getRecieveMarketingTeam();
        if (TandC == null || "false".equals(TandC)) {
//            TandC="N";
            TandCcheck = false;
        } else {
//         TandC="Y";
            TandCcheck = true;
        }
        if (RecieveMarketing == null || "false".equals(RecieveMarketing)) {
//            RecieveMarketing="N";
            RecieveMArketingcheck = false;
        } else {
//         RecieveMarketing="Y";
            RecieveMArketingcheck = true;
        }
        int enrollbp = enrollBean.getEnrollbpNo();
        String mediaCode = memberEnrollDAO.getEnrollMediaCode(enrollbp);
        String citycode = commonDAO.getIATACode(enrollBean.getEnrollCity());
//        ams change for passing citycode as "" or citycode
        if(citycode == null){
        citycode="";
        }
        String date = enrollBean.getEnrollDob();
        Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(date);
        String parsedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date1);
        String mobilenumber = "91-" + enrollBean.getEnrollPhone();

        Date currentdate = new Date();
        String parsedcurrentdate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(currentdate);
        String result = "";

        String ApiId = "9a14447e-c9c9-4bef-91dc-719aeb8aa9fa";
        String ApiKey = "H3fbmUOABcN3LYro9XBWFXeSndCeXMrO2vG8Yu5pWJM=";
//        ams changes for passing password as ""
        String json = "{\n"
                + "                \"Title\": \"" + enrollBean.getEnrollTitle() + "\",\n"
                + "                \"FirstName\": \"" + enrollBean.getEnrollFname() + "\",\n"
                + "                \"MiddleName\": \"" + enrollBean.getEnrollMname() + "\",\n"
                + "                \"LastName\": \"" + enrollBean.getEnrollLname() + "\",\n"
                + "                \"EmailID\": \"" + enrollBean.getEnrollemail() + "\",\n"
                + "                \"Gender\": \"" + gender + "\",\n"
                + "                \"DOB\": \"" + parsedDate + "\",\n"
                + "                \"MobileNumber\": \"" + mobilenumber + "\",\n"
                + "                \"PreferredContactNumber\": \"MOBIL\",\n"
                + "                \"UserPassword\": \"\",\n"
                + "                \"UserIP\": \"::1\",\n"
                + "                \"COR\": \"IN\",\n"
                + "                \"MediaCode\": \"" + mediaCode + "\",\n"
                + "                \"ReferenceCode\": null,\n"
                + "                \"Addresses\": [{\n"
                + "                                \"AddressID\": 0,\n"
                + "                                \"OrgID\": 0,\n"
                + "                                \"LocationID\": 0,\n"
                + "                                \"ContactType\": null,\n"
                + "                                \"FormatedContactNumber\": null,\n"
                + "                                \"Address1\": \"Sect-1\",\n"
                + "                                \"Address2\": \"Sect-1\",\n"
                + "                                \"Address3\": \"Sect-1\",\n"
                + "                                \"PhoneNumber\": null,\n"
                + "                                \"MobileNumber\": null,\n"
                + "                                \"BusinessPhoneNumber\": null,\n"
                + "                                \"CountryISDCode\": null,\n"
                + "                                \"AreaCode\": null,\n"
                + "                                \"ContactNumber\": null,\n"
                + "                                \"CountryName\": \"IN\",\n"
                + "                                \"CountryCode\": null,\n"
                + "                                \"CountryID\": 0,\n"
                + "                                \"CityName\": \"" + enrollBean.getEnrollCity() + "\",\n"
                + "                                \"Cityismapped\": null,\n"
                + "                                \"CityID\": 0,\n"
                + "                                \"CityCode\": \"" + citycode + "\",\n"
                + "                                \"State\": \"\",\n"
                + "                                \"POBoxNumber\": null,\n"
                + "                                \"PostalCode\": \"\",\n"
                + "                                \"Company\": \"\",\n"
                + "                                \"IndustryType\": \"\",\n"
                + "                                \"JobTitle\": \"\",\n"
                + "                                \"AddressType\": \"HOME\",\n"
                + "                                \"Designation\": \"\",\n"
                + "                                \"PreferredContactNumber\": null,\n"
                + "                                \"IsPreferred\": false,\n"
                + "                                \"City\": null\n"
                + "                }],\n"
                + "                \"ReceiveMarketingCommunication\": " + RecieveMArketingcheck + ",\n"
                + "                \"UserName\": null,\n"
                + "                \"WorkPreferredAddress\": \"\",\n"
                + "                \"QuickEnrolMediaCode\": null,\n"
                + "                \"Guardian\": {\n"
                + "                                \"ActiveStatus\": \"\",\n"
                + "                                \"PersonId\": 0,\n"
                + "                                \"Relationship\": \"\",\n"
                + "                                \"CardNumber\": \"\",\n"
                + "                                \"SendCareOf\": \"\"\n"
                + "                },\n"
                + "                \"CitizenOf\": \"IN\",\n"
                + "                \"PreassignedJPNumber\": null,\n"
                + "                \"StartDate\": \"0001-01-01T00:00:00\",\n"
                + "                \"EndDate\": \"0001-01-01T00:00:00\",\n"
                + "                \"IsQuickEnrol\": false,\n"
                + "                \"IsJetKidEnrol\": false,\n"
                + "                \"FP1\": \"ea2c2f7d06abd5d5437ccf5089adfcd1965c8ec5b3c89c5074b370d60d65c9c638b0b8d0be1a199416af04b5558cd7891280031b40c570e6c2a8b9a6d2fa5dbf\",\n"
                + "                \"ModuleName\": \"enrol\",\n"
                + "                \"SessionId\": \"" + cookieValue + "\",\n"
                + "                \"SessionDateTime\": \"" + parsedcurrentdate + "\",\n"
                + "                \"TnC\": true\n"
                + "}";
        System.out.println("json request :"+json);
        logger.info("*****JSON Request********" + json);
        URL url;
        String requestContentBase64String = "";
        HttpURLConnection urlConnection = null;
        try {
            logger.info("***********Mac3Url***************" + Mac3Url);
            url = new URL(Mac3Url);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setConnectTimeout(65000);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            Date epochStart = new Date(0);
            String requestTimeStamp = "" + ((new Date().getTime() - epochStart.getTime()) / 1000);
            String nonce = java.util.UUID.randomUUID().toString().replace("-", "");

            if (json != null) {
                byte[] content = json.getBytes();
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] requestContentHash = md.digest(content);
                requestContentBase64String = Base64.getEncoder().encodeToString(requestContentHash);
            }
            String signatureRawData = String.format("%s%s%s%s%s%s", ApiId, urlConnection.getRequestMethod(),
                    url.toString().toLowerCase(), requestTimeStamp, nonce, requestContentBase64String);

            byte[] secretKeyByteArray = ApiKey.getBytes();
            byte[] signature = signatureRawData.getBytes();
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secretKeyByteArray, "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] signatureBytes = sha256_HMAC.doFinal(signature);
            String requestSignatureBase64String = Base64.getEncoder().encodeToString(signatureBytes);
            String header = String.format("amx %s:%s:%s:%s", ApiId, requestSignatureBase64String, nonce,
                    requestTimeStamp);
            logger.info("Header is: " + header);

            urlConnection.setRequestProperty("Authorization", header);
            logger.info("Header is header.trim(): " + header.trim());
            try (OutputStream os = urlConnection.getOutputStream()) {
                logger.info("inside output stream" + os);
                os.write(json.getBytes("UTF-8"));
                logger.info("indide output stream" + json.getBytes("UTF-8"));

            } catch (Exception ex) {
                logger.info("Error in Output Stream: " + ex);
            }

            logger.info("urlConnection.getInputStream()-------->" + urlConnection.getInputStream().toString());
            InputStream in = (InputStream) urlConnection.getInputStream();
            InputStreamReader reader = new InputStreamReader(in);
            logger.info("reader=======>" + reader);
            int data = reader.read();
            while (data != -1) {
                char current = (char) data;
                result += current;
                data = reader.read();
            }
            logger.info("data  :" + data);
            logger.info("Enroll Service Response  " + result);
            JSONObject jSONObject = new JSONObject(result);
            String respStatus = "" + jSONObject.getJSONArray("Response").get(0);
            String jpnumResp = "" + jSONObject.getJSONArray("Response").get(1);
            JSONObject jSONOrespStatus = new JSONObject(respStatus);
            JSONObject response = new JSONObject(jpnumResp);
            String jpNumberstatus = "" + response.getJSONObject("ResponseData").get("ResponseDataEnrol");
            JSONArray jsonarray = new JSONArray(jpNumberstatus);
            for (int i = 0; i < jsonarray.length(); i++) {

                JSONObject jsonProductObject = jsonarray.getJSONObject(i);
                JP_number = jsonProductObject.getString("JetPrivilegeNumber");

            }
            String status = jSONOrespStatus.getJSONObject("ResponseStatus").getString("ResponseDescription");
            String responsecode = String.valueOf(jSONOrespStatus.getJSONObject("ResponseStatus").getInt("ResponseCode"));
            logger.info("Enroll JpNumber :" + JP_number);
            logger.info("Enroll JpNumber :" + JP_number);
            hm.put("JPNumber", JP_number);
            hm.put("ResponseDescription", status);
            if (status.equalsIgnoreCase(" Same Mobile no already exists for another member. Please try with another Mobile Number")) {

                enrollBean.setMessage(status);
            } else if (status.equalsIgnoreCase(" Same email id already exists for another member. Please try with another email id")) {

                enrollBean.setMessage(status);

            } else if (JP_number == null || JP_number == "" || JP_number.isEmpty()) {
                enrollBean.setMessage("Some error occurred, please try after sometime.");
            }
            enrollBean.setResponseCode(responsecode);

            logger.info("JP_NUMBER   :" + JP_number + " : Status  :" + status + "  : responsecode  : " + responsecode);
            if (JP_number.length() == 11 && JP_number != null) {
                enrollBean.setJpNumber(JP_number);
                ccMemberErollment.setJpnumber(JP_number);
                ccMemberErollment.setTitle(enrollBean.getEnrollTitle());
                ccMemberErollment.setFirstName(enrollBean.getEnrollFname());
                ccMemberErollment.setMiddleName(CommonUtils.nullSafe(enrollBean.getEnrollMname(), ""));
                ccMemberErollment.setLastName(enrollBean.getEnrollLname());
                ccMemberErollment.setGender(gender);
                ccMemberErollment.setCity(enrollBean.getEnrollCity());
                ccMemberErollment.setMediaCode(mediaCode);
                ccMemberErollment.setDateOfBirth(date1);
                ccMemberErollment.setMobileNumber(enrollBean.getEnrollPhone());
                ccMemberErollment.setEmailId(enrollBean.getEnrollemail());
                ccMemberErollment.setCreatedTime(new Date());
                ccMemberErollment.setStatus((byte) 1);
                ccMemberErollment.setIpaddress(ipaddress);
                memberEnrollDAO.memberEnrollment(ccMemberErollment);
            }

        } catch (MalformedURLException e) {
            logger.error(e);

            enrollBean.setMessage("Some error occurred, please try after sometime.");
            e.printStackTrace();
        } catch (IOException e) {
            enrollBean.setMessage("Some error occurred, please try after sometime.");

            logger.error(e);
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            enrollBean.setMessage("Some error occurred, please try after sometime.");
            //   System.out.println(e);
            logger.error(e);
            e.printStackTrace();

        } catch (InvalidKeyException e) {
            enrollBean.setMessage("Some error occurred, please try after sometime.");
            //   System.out.println(e);
            logger.error(e);
            e.printStackTrace();

        } catch (Exception e) {
            enrollBean.setMessage("Some error occurred, please try after sometime.");
//   System.out.println(e);
            logger.error(e);
            e.printStackTrace();
        }

        return hm;
    }
    
//    public String  getEncValue(String strval)
//    {
//    	org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
//    obj.put("encVal", strval);
//    StringWriter out = new StringWriter();
//    String jsonText = out.toString();
//    EncryptDetail e = new EncryptDetail();
//    String encryptDataVar = e.encryptData(jsonText, strSoapURL1, soapActEnc);
//    org.json.JSONObject encryptData;
//    String encr_val = null;
//   	try {
//   		obj.writeJSONString(out);
//   		encryptData = new org.json.JSONObject(encryptDataVar);
//   	    encr_val = encryptData.getString("encVal");
//   	} 
//   	catch (IOException e1) {
//   		// TODO Auto-generated catch block
//   		e1.printStackTrace();
//   	}
//   	catch (JSONException e1) {
//   		// TODO Auto-generated catch block
//   		e1.printStackTrace();
//   	}

//    return encr_val;
//    }
    
    
}
