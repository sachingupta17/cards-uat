package com.cbc.portal.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cbc.portal.DAO.CardProductDAO;
import com.cbc.portal.DAO.CommonDAO;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.entity.CcCardFeatures;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcLifestyleBenefitPrefValues;
import com.cbc.portal.entity.CcPageGroupLink;
import com.cbc.portal.service.CardSearchService;
import com.cbc.portal.service.CompareCardService;
import com.cbc.portal.utils.CommonUtils;

@Component
@Transactional(readOnly=true)
public class CompareCardServiceImpl implements CompareCardService {

	private Logger logger = Logger.getLogger(CompareCardServiceImpl.class.getName());
	
	@Autowired
	CardProductDAO cardProductDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	CardSearchService cardSearchService;
	
	
	@Override
	public List<CardDetailsBean> compareCards(String[] compareCardsArr) {
		List<CardDetailsBean> cardsList = new ArrayList<>();
		CardDetailsBean cardDetailsBean = null;
		CcCardProduct ccCard = null;
		
		String[] innerArray = compareCardsArr[0].split(",");
		
		try{
		 for(String st:innerArray){
			                  
			 ccCard=cardProductDAO.getCardProductById(Integer.parseInt(st));
			
			 if(null!=ccCard){
				 cardDetailsBean = new CardDetailsBean();
				 cardDetailsBean.setCpNo(ccCard.getCpNo());
				 cardDetailsBean.setCardName(ccCard.getCardName());
				 cardDetailsBean.setCardImagePath(ccCard.getCpImagePath());
				 cardDetailsBean.setImageText(ccCard.getCpImageText());
				 cardDetailsBean.setBpNo(ccCard.getCcBankPartner().getBpNo());
				 cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccCard.getFees()));
				 cardDetailsBean.setBankName(ccCard.getCcBankPartner().getBankName());
				 cardDetailsBean.setNetworkName(ccCard.getCcNetworkList().getNetworkName());
				cardDetailsBean.setApplyflag(ccCard.getApplyFlag());	
				 cardsList.add(cardDetailsBean);
			 }
			 
			
		 }
		
		}catch(Exception e){
			logger.error("@@@@ Exception in CompareCardServiceImpl compareCards() :",e);	
		}
		return cardsList;
	}


	@Override
	public Map<String, List <Map<String, List<CcCardFeatures>>>>  getFeatures(String[] compareCardsArr) {
		List<CcPageGroupLink> ccPageGroup= null;
		List<String> column=new ArrayList<>();
		CcCardFeatures modifiedccFeature=null;
		List<String> ccFeature=null;
		List<CcCardFeatures> cclist=null;
		Map<String, List<CcCardFeatures>> map=null;
		List <Map<String, List<CcCardFeatures>>> listMap=null;
		Map<String, List <Map<String, List<CcCardFeatures>>>> newMap=new LinkedHashMap<>();
		String[] cardsArray=compareCardsArr[0].split(",");
			try{
				ccPageGroup =commonDAO.getGroupHeaders(2);
				
				 if(!ccPageGroup.isEmpty()){
					 for(CcPageGroupLink ccpage:ccPageGroup){
						 column.add(ccpage.getCcGroupMaster().getGroupDescription());
					 }
				 }
				 
				 List<String> newColumn=column.subList(2, column.size());
				 List<String> cardsList = Arrays.asList(cardsArray); 
				 List<Integer> intlIST = new ArrayList<>();
				 for(String s : cardsList) intlIST.add(Integer.valueOf(s));
				 
				 String [] groupNumber = newColumn.toArray(new String[newColumn.size()]);
				    Integer [] cardNumber = intlIST.toArray(new Integer[intlIST.size()]);
				   
				    for(int i=0;i<groupNumber.length;i++){
				    	listMap=new ArrayList<>();
				    	ccFeature=commonDAO.getAllCardFeatures(groupNumber[i]);
				    	 String []featureNumber = ccFeature.toArray(new String[ccFeature.size()]);
					
				    	 for(int j=0;j<featureNumber.length;j++){
				    		 cclist=new ArrayList<>();
							 for(int k=0;k<cardNumber.length;k++){
								 modifiedccFeature=cardProductDAO.getCardFeatureByrow(groupNumber[i],featureNumber[j],cardNumber[k]);
								 if(null!=modifiedccFeature){
									 cclist.add(modifiedccFeature);
								 		}	
								 else{
									 cclist.add(new CcCardFeatures());
								 }
							 
						 	}
							 map=new LinkedHashMap<>();
							 map.put(featureNumber[j],cclist);
							 listMap.add(map);
					 	}
					 newMap.put(groupNumber[i],listMap);
					 } 
		}catch(Exception e){
			logger.error("@@@@ Exception in CompareCardServiceImpl getFeatures() :",e);	
		}
			
		return newMap;
	}

	@Override
	public Map<String, String> getTopTableContent(String spend, String cpNoList) {
		String[] cardsArray=cpNoList.split(",");
		List<CcPageGroupLink> ccPageGroupLink= new ArrayList<>();
		List<Integer> column=new ArrayList<>();
		CcCardFeatures modifiedccFeature=null;
		String content;
		String lsbp= "";
		Map<String, String> map=new LinkedHashMap<>();
		Map<Integer, String> spendmap=null;
		List<CcLifestyleBenefitPrefValues>lsbpValues=null;
		try{
		 ccPageGroupLink =commonDAO.getGroupHeaders(2);
		 spendmap=cardSearchService.calculateFreeFlightsByArray(spend,cpNoList);

		 if(!ccPageGroupLink.isEmpty()){
			 int count=0;
			 for(CcPageGroupLink ccpage:ccPageGroupLink){
				 if(count<2){
				
					 column.add(ccpage.getCcGroupMaster().getGmNo());
					 count++;
					 
				 }
				 else continue;
			 }
		 }
		 List<String> cardsList=Arrays.asList(cardsArray); 
		 List<Integer> intlIST=new ArrayList<>();
		 for(String s : cardsList) intlIST.add(Integer.valueOf(s));
		  
		 	Integer [] groupNumber = column.toArray(new Integer[column.size()]);
		    Integer [] cardNumber  =intlIST.toArray(new Integer[intlIST.size()]);
		    for(int i=0;i<groupNumber.length;i++){
					 for(int k=0;k<cardNumber.length;k++){
						 modifiedccFeature=cardProductDAO.getCardFeatureBycpNogmNo(groupNumber[i],cardNumber[k]);
						 content = "";
						 if(null!=modifiedccFeature){
							 content=modifiedccFeature.getCfContent().replaceAll("\\r|\\n", "").trim();
							 for (Map.Entry<Integer, String> entry : spendmap.entrySet())
							 {
								 if(entry.getKey().equals(cardNumber[k])){
									 content=content.replace("$$AirTicket$$", "<strong>"+entry.getValue().split(",")[1]+"</strong>");
									 lsbpValues=cardProductDAO.getLSBPValuesByCPID(entry.getKey());
									 if(null!=lsbpValues){
										 lsbp="";
										 for(CcLifestyleBenefitPrefValues lsbpitem:lsbpValues){
											 lsbp = lsbp+lsbpitem.getCcLifestyleBenefitPref().getLsbpName()+", ";
										 }
										 
									 }
									 content=content.replace("$$Lifestyle$$", "<strong>"+lsbp+"</strong>");
								 }
							 }
							 if(modifiedccFeature.getCfTermsAndConditions().trim().length()!=0){
								 content = content+ "-->"+modifiedccFeature.getCfTermsAndConditions().trim();
							 }
							 content = content.replace("\"", "\\\"");
						 map.put(i+""+(k+1),content);
					 	}
					 else
					 continue;
				 	}
			 } 
		}catch(Exception e){
			logger.error("@@@@ Exception in CompareCardServiceImpl getTopTableContent() :",e);	
		}
		return map;
	}

}
