package com.cbc.portal.service.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Security;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cbc.portal.DAO.CommonDAO;
import com.cbc.portal.beans.AmexBean;
import com.cbc.portal.beans.CallMeBean;
import com.cbc.portal.beans.CityMappingBean;
import com.cbc.portal.beans.CreditScoreBean;
import com.cbc.portal.beans.IciciBean;
import com.cbc.portal.service.MemberDetailsService;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.CrisServicesUtils;
import com.skywards.mercator_cris.Address;
import com.skywards.mercator_cris.ArrayOfAddress;
import com.skywards.mercator_cris.ArrayOfContact;
import com.skywards.mercator_cris.Contact;
import com.skywards.mercator_cris.GetSkywardsProfileResponse;

@Component
@Transactional(readOnly = true)
public class MemberDetailsServiceImpl implements MemberDetailsService {

    private static Logger logger = Logger.getLogger(MemberDetailsServiceImpl.class.getName());

    @Autowired
    CrisServicesUtils crisServicesUtils;

    @Autowired
    CommonDAO commonDAO;

    private static final String USER_AGENT = "Mozilla/5.0";
    
    @Override
    public void getMemberDetails(String jpNumber, AmexBean amexBean) {
//        System.out.println("jpNumber getmemeber"+jpNumber);
        String userName = "";
        String gender = "";
        String tier = "";
        String title = "";
        String fullName = "";
        String firstName = "";
        String lastName = "";
        String middleName = "";
        String email = "";
        String contactNumber = "";
        String address1 = "";
        String address2 = "";
        String address3 = "";
        String mdPincode = "";
        String mdState = "";
        String mdCity = "";

        GetSkywardsProfileResponse getSkywardsProfileResponse = null;
        XMLGregorianCalendar xmlGregorianCalendar = null;
        Date mdDateOfBirth = null;
        ArrayOfAddress arrayOfAddress = new ArrayOfAddress();
        CityMappingBean cityMappingBean = new CityMappingBean();

        try {
        	if(!jpNumber.isEmpty() && jpNumber.length() > 9)
        	{
            getSkywardsProfileResponse = crisServicesUtils.getMemberDetails(jpNumber);
            if (null != getSkywardsProfileResponse && null != getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember()) {
                userName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getUsername();
                userName = (null != userName) ? userName.trim() : "";
                if (userName.length() > 9 && userName.startsWith("00")) {
                    userName = userName.substring(2);

                }
                amexBean.setJetpriviligemembershipNumber(userName);

                title = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getTitle();
                title = (null != title) ? title.trim() : "";

                firstName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getFirstName();
                firstName = (null != firstName) ? firstName.trim() : "";

                lastName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getLastName();
                lastName = (null != lastName) ? lastName.trim() : "";

                middleName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getMiddleName();
                middleName = (null != middleName) ? middleName.trim() : " ";

                fullName = firstName.concat(" " + middleName).concat(" " + lastName);
                amexBean.setTitle(title);
                amexBean.setFname(firstName);
                amexBean.setLname(lastName);
                amexBean.setMname(middleName);
                amexBean.setFullName(fullName);

                gender = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getGender();
                gender = (null != gender) ? gender.trim() : "";
                if ("m".equalsIgnoreCase(gender)) {
                    amexBean.setGender("0");
                } else if ("f".equalsIgnoreCase(gender)) {
                    amexBean.setGender("1");
                }
                xmlGregorianCalendar = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getBirthDate();
                mdDateOfBirth = xmlGregorianCalendar.toGregorianCalendar().getTime();
                String dateStr = mdDateOfBirth.toString();
                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                Date date = formatter.parse(dateStr);
                
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                String formatedDate = (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE) + "-" + cal.get(Calendar.YEAR);

                amexBean.setDateOfBirth(formatedDate);
                
//                System.out.println("formatedDate"+formatedDate);
                tier = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getTier();
                tier = (null != tier) ? tier.trim() : "";
                amexBean.setJetpriviligemembershipTier(tier);

                email = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getEmailAddress();
                email = (null != email) ? email.trim() : "";
                amexBean.setEmail(email);

                arrayOfAddress = getSkywardsProfileResponse.getGetSkywardsProfileResult().getAddresses();

                for (Address address : arrayOfAddress.getAddress()) {

                    if (address.isIsPreferred()) {
                        address1 = address.getAddress1();
                        address2 = address.getAddress2();
                        address3 = address.getAddress3();
                        mdPincode = address.getPostCode();
                    }
                    continue;

                }
                address1 = CommonUtils.nullSafe(address1);
                address2 = CommonUtils.nullSafe(address2);
                address3 = CommonUtils.nullSafe(address3);
                mdPincode = CommonUtils.nullSafe(mdPincode);

                cityMappingBean = commonDAO.getCityMapping(mdPincode);
                if (null != cityMappingBean) {
                    mdState = CommonUtils.nullSafe(cityMappingBean.getState(), "");
                    mdCity = CommonUtils.nullSafe(cityMappingBean.getCity(), "");
                }

                amexBean.setAddress(address1);
                amexBean.setAddress2(address2);
                //add new
                amexBean.setAddress3(address3);
                amexBean.setPinCode(mdPincode);
                amexBean.setState1(mdState);
                amexBean.setCity(mdCity);

                if (!("".equalsIgnoreCase(address1) && "".equalsIgnoreCase(mdPincode))) {
                    amexBean.setResedentialAddress(address1 + " " + address2 + " " + address3 + " " + mdPincode + " " + mdState + " " + mdCity);
                }

                if (null != getSkywardsProfileResponse.getGetSkywardsProfileResult().getContacts()) {
                    ArrayOfContact arrayOfContact = getSkywardsProfileResponse.getGetSkywardsProfileResult().getContacts();
                    List<Contact> listOfContacts = arrayOfContact.getContact();
                    for (Contact contact : listOfContacts) {
                        if ("MOBIL".equalsIgnoreCase(contact.getContactType())) {
                            contactNumber = contact.getContactNumber();
                            amexBean.setMobile(contactNumber);
                        } else {
                            continue;
                        }
                    }//for each
                }//if contacts
            }//if member details are there
        	}
        } catch (Exception e) {
            logger.error("@@@@ Exception in MemberDetailsServiceImpl getMemberDetails() :", e);
        }

    }
     
    /*Code Start  By Vernost Team*/
    @Override
    public void getMemberDetailsICICI(String jpNumber, IciciBean iciciBean) {
        
        GetSkywardsProfileResponse getSkywardsProfileResponse = null;
        XMLGregorianCalendar xmlGregorianCalendar = null;
        Date mdDateOfBirth = null;
        ArrayOfAddress arrayOfAddress = new ArrayOfAddress();
        CityMappingBean cityMappingBean = new CityMappingBean();
        
        String userName = "";
        String gender = "";
        String tier = "";
        String title = "";
        String fullName = "";
        String firstName = "";
        String lastName = "";
        String middleName = "";
        String email = "";
        String contactNumber = "";
        String address1 = "";
        String address2 = "";
        String address3 = "";
        String mdPincode = "";
        String mdState = "";
        String mdCity = "";
        //JSONObject jsonRes;
        

        try {
        	if(!jpNumber.isEmpty() && jpNumber.length() > 9)
        	{
        	//	jsonRes = fetchProfile(jpNumber);
        	//	logger.info("---------->>> "+jsonRes.toString());
            getSkywardsProfileResponse = crisServicesUtils.getMemberDetails(jpNumber); //readjson response //crisServicesUtils.getMemberDetails(jpNumber);
            if (null != getSkywardsProfileResponse && null != getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember()) {
                userName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getUsername();
                userName = (null != userName) ? userName.trim() : "";
                if (userName.length() > 9 && userName.startsWith("00")) {
                    userName = userName.substring(2);

                }
               
                iciciBean.setJetpriviligemembershipNumber(userName);

                title = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getTitle();
                title = (null != title) ? title.trim() : "";

                firstName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getFirstName();
                firstName = (null != firstName) ? firstName.trim() : "";

                lastName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getLastName();
                lastName = (null != lastName) ? lastName.trim() : "";

                middleName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getMiddleName();
                middleName = (null != middleName) ? middleName.trim() : " ";

                fullName = firstName.concat(" " + middleName).concat(" " + lastName);
                iciciBean.setTitle(title);
                iciciBean.setFname(firstName);
                iciciBean.setLname(lastName);
                iciciBean.setMname(middleName);
                iciciBean.setFullName(fullName);

                gender = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getGender();
                gender = (null != gender) ? gender.trim() : "";
                if ("m".equalsIgnoreCase(gender)) {
                    iciciBean.setGender("0");
                } else if ("f".equalsIgnoreCase(gender)) {
                    iciciBean.setGender("1");
                }
                
                xmlGregorianCalendar = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getBirthDate();
                mdDateOfBirth = xmlGregorianCalendar.toGregorianCalendar().getTime();
                String dateStr = mdDateOfBirth.toString();
                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                Date date = formatter.parse(dateStr);

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
//                String formatedDate = cal.get(Calendar.DATE) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
               
                String formatedDate = cal.get(Calendar.DATE) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
                formatedDate = formatedDate.replaceAll("-", "/");
                Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(formatedDate);
                String parsedDate = new SimpleDateFormat("MM-dd-yyyy").format(date1);
                 
                logger.info("--------------parsedDate---------------"+parsedDate);
                iciciBean.setDateOfBirth(parsedDate);
                tier = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getTier();
                tier = (null != tier) ? tier.trim() : "";
                iciciBean.setJetpriviligemembershipTier(tier);

                email = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getEmailAddress();
                email = (null != email) ? email.trim() : "";
                iciciBean.setEmail(email);

                arrayOfAddress = getSkywardsProfileResponse.getGetSkywardsProfileResult().getAddresses();

                for (Address address : arrayOfAddress.getAddress()) {

                    if (address.isIsPreferred()) {
                        address1 = address.getAddress1();
                        address2 = address.getAddress2();
                        address3 = address.getAddress3();
                        mdPincode = address.getPostCode();
                    }
                    continue;

                }
                address1 = CommonUtils.nullSafe(address1);
                address2 = CommonUtils.nullSafe(address2);
                address3 = CommonUtils.nullSafe(address3);
                mdPincode = CommonUtils.nullSafe(mdPincode);

                cityMappingBean = commonDAO.getCityMapping(mdPincode);
                if (null != cityMappingBean) {
                    mdState = CommonUtils.nullSafe(cityMappingBean.getState(), "");
                    mdCity = CommonUtils.nullSafe(cityMappingBean.getCity(), "");
                }

                iciciBean.setAddress(address1);
                iciciBean.setAddress2(address2);
                //add new
                iciciBean.setAddress3(address3);
                iciciBean.setPinCode(mdPincode);
                iciciBean.setState1(mdState);
                iciciBean.setCity(mdCity);

                if (!("".equalsIgnoreCase(address1) && "".equalsIgnoreCase(mdPincode))) {
                    iciciBean.setResedentialAddress(address1 + " " + address2 + " " + address3 + " " + mdPincode + " " + mdState + " " + mdCity);
                }

                if (null != getSkywardsProfileResponse.getGetSkywardsProfileResult().getContacts()) {
                    ArrayOfContact arrayOfContact = getSkywardsProfileResponse.getGetSkywardsProfileResult().getContacts();
                    List<Contact> listOfContacts = arrayOfContact.getContact();
                    for (Contact contact : listOfContacts) {
                        if ("MOBIL".equalsIgnoreCase(contact.getContactType())) {
                            contactNumber = contact.getContactNumber();
                            iciciBean.setMobile(contactNumber);
                        } else {
                            continue;
                        }
                    }//for each
                }//if contacts
            }//if member details are there
        	}
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICI MemberDetailsServiceImpl getMemberDetails() :", e);
        }

    }
    
      @Override
    public CallMeBean getMemberDetailsCallmeFunction(String jpNumber, CallMeBean callMeBean) {
    
     GetSkywardsProfileResponse getSkywardsProfileResponse = null;
        XMLGregorianCalendar xmlGregorianCalendar = null;
        Date mdDateOfBirth = null;
        ArrayOfAddress arrayOfAddress = new ArrayOfAddress();
        CityMappingBean cityMappingBean = new CityMappingBean();
        
        String userName = "";
        String gender = "";
        String tier = "";
        String title = "";
        String fullName = "";
        String firstName = "";
        String lastName = "";
        String middleName = "";
        String email = "";
        String contactNumber = "";
        String address1 = "";
        String address2 = "";
        String address3 = "";
        String mdPincode = "";
        String mdState = "";
        String mdCity = "";
     try{
    	 if(!jpNumber.isEmpty())
     	{
       getSkywardsProfileResponse = crisServicesUtils.getMemberDetails(jpNumber);
       
              firstName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getFirstName();
                firstName = (null != firstName) ? firstName.trim() : "";
//                callMeBean.setFirstName(firstName);
                lastName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getLastName();
                lastName = (null != lastName) ? lastName.trim() : "";
//                callMeBean.setLastName(lastName);
                middleName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getMiddleName();
                middleName = (null != middleName) ? middleName.trim() : " ";
//                callMeBean.setLastName(middleName);
                  fullName = firstName.concat(" " + middleName).concat(" " + lastName);
                callMeBean.setSocialName(fullName);
                email = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getEmailAddress();
                email = (null != email) ? email.trim() : "";
                callMeBean.setSocialEmail(email);
                
               if (null != getSkywardsProfileResponse.getGetSkywardsProfileResult().getContacts()) {
                    ArrayOfContact arrayOfContact = getSkywardsProfileResponse.getGetSkywardsProfileResult().getContacts();
                    List<Contact> listOfContacts = arrayOfContact.getContact();
                    for (Contact contact : listOfContacts) {
                        if ("MOBIL".equalsIgnoreCase(contact.getContactType())) {
                            contactNumber = contact.getContactNumber();
                            callMeBean.setSocialPhone(contactNumber);
                        } else {
                            continue;
                        }
                    }//for each
                }
     	}
       
     }catch(Exception ex){
         logger.error("GetMemberDetails for Call Me functionality"+ex);
     
     }
     return callMeBean;
     }
    
    

    /*Code End  By Vernost Team*/
    @Override
    public String getTier(String jpNumber) {
        String tier = "";
        GetSkywardsProfileResponse getSkywardsProfileResponse = null;
        try {
        	if(!jpNumber.isEmpty())
        	{
            getSkywardsProfileResponse = crisServicesUtils.getMemberDetails(jpNumber);
            if (null != getSkywardsProfileResponse && null != getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember()) {
                tier = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getTier();
                tier = (null != tier) ? tier.trim() : "";
            }//if
        	}
        } catch (Exception e) {
            logger.error("@@@@ Exception in MemberDetailsServiceImpl getTier() :", e);
        }
        return tier;
    }
    @Override
    public void getMemberDetailsCreditScore(String jpNumber, CreditScoreBean creditScoreBean) {

        GetSkywardsProfileResponse getSkywardsProfileResponse = null;
        XMLGregorianCalendar xmlGregorianCalendar = null;
        Date mdDateOfBirth = null;
        ArrayOfAddress arrayOfAddress = new ArrayOfAddress();
        CityMappingBean cityMappingBean = new CityMappingBean();

        String userName = "";
        String gender = "";
        String tier = "";
        String title = "";
        String fullName = "";
        String firstName = "";
        String lastName = "";
        String middleName = "";
        String email = "";
        String contactNumber = "";
        String address1 = "";
        String address2 = "";
        String address3 = "";
        String mdPincode = "";
        String mdState = "";
        String mdCity = "";
        String mergedJp = "";
        String mobileVerified = "";
        String emailVerified = "";
        try {
        	if(!jpNumber.isEmpty())
        	{
            getSkywardsProfileResponse = crisServicesUtils.getMemberDetails(jpNumber);
            if (null != getSkywardsProfileResponse && null != getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember()) {
                userName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getUsername();
                userName = (null != userName) ? userName.trim() : "";
                if (userName.length() > 9 && userName.startsWith("00")) {
                    userName = userName.substring(2);

                }

                creditScoreBean.setJPnumber(userName);

                mergedJp = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getMergedCardNo();
                creditScoreBean.setMergeJP(mergedJp);
                mobileVerified=getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getIsMobileVerified();
                creditScoreBean.setMobileVerified(mobileVerified);
                emailVerified=getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getIsEmailVerified();
                creditScoreBean.setEmailVerified(emailVerified);
                title = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getTitle();
                title = (null != title) ? title.trim() : "";

                firstName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getFirstName();
                firstName = (null != firstName) ? firstName.trim() : "";

                lastName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getLastName();
                lastName = (null != lastName) ? lastName.trim() : "";

                middleName = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getMiddleName();
                middleName = (null != middleName) ? middleName.trim() : " ";

                fullName = firstName.concat(" " + lastName);
//                iciciBean.setTitle(title);
                creditScoreBean.setFname(firstName);
                creditScoreBean.setLname(lastName);
//                iciciBean.setMname(middleName);
                creditScoreBean.setFullName(fullName);

                gender = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getGender();
                gender = (null != gender) ? gender.trim() : "";
                creditScoreBean.setGender(gender);
//                if ("m".equalsIgnoreCase(gender)) {
//                    iciciBean.setGender("0");
//                } else if ("f".equalsIgnoreCase(gender)) {
//                    iciciBean.setGender("1");
//                }
                xmlGregorianCalendar = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getBirthDate();
                mdDateOfBirth = xmlGregorianCalendar.toGregorianCalendar().getTime();
                String dateStr = mdDateOfBirth.toString();
                DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                Date date = formatter.parse(dateStr);

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
//                String formatedDate = cal.get(Calendar.DATE) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);

                String formatedDate = cal.get(Calendar.DATE) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
                formatedDate = formatedDate.replaceAll("-", "/");
                Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(formatedDate);
                String parsedDate = new SimpleDateFormat("MM-dd-yyyy").format(date1);

                logger.info("--------------parsedDate---------------" + parsedDate);
                creditScoreBean.setDob(parsedDate);
                tier = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getTier();
                tier = (null != tier) ? tier.trim() : "";
                creditScoreBean.setTier(tier);

                email = getSkywardsProfileResponse.getGetSkywardsProfileResult().getMember().getEmailAddress();
                email = (null != email) ? email.trim() : "";
                creditScoreBean.setEmailId(email);
                creditScoreBean.setEmailJPID(email);
                arrayOfAddress = getSkywardsProfileResponse.getGetSkywardsProfileResult().getAddresses();

                for (Address address : arrayOfAddress.getAddress()) {

                    if (address.isIsPreferred()) {
                        address1 = address.getAddress1();
                        address2 = address.getAddress2();
                        address3 = address.getAddress3();
                        mdPincode = address.getPostCode();
                    }
                    continue;

                }
                address1 = CommonUtils.nullSafe(address1);
                address2 = CommonUtils.nullSafe(address2);
                address3 = CommonUtils.nullSafe(address3);
                mdPincode = CommonUtils.nullSafe(mdPincode);

                cityMappingBean = commonDAO.getCityMapping(mdPincode);
                if (null != cityMappingBean) {
                    mdState = CommonUtils.nullSafe(cityMappingBean.getState(), "");
                    mdCity = CommonUtils.nullSafe(cityMappingBean.getCity(), "");
                }

//                iciciBean.setAddress(address1);
//                iciciBean.setAddress2(address2);
                //add new
//                iciciBean.setAddress3(address3);
//                iciciBean.setPinCode(mdPincode);
//                iciciBean.setState1(mdState);
                creditScoreBean.setCity(mdCity);

                if (!("".equalsIgnoreCase(address1) && "".equalsIgnoreCase(mdPincode))) {
//                    iciciBean.setResedentialAddress(address1 + " " + address2 + " " + address3 + " " + mdPincode + " " + mdState + " " + mdCity);
                }

                if (null != getSkywardsProfileResponse.getGetSkywardsProfileResult().getContacts()) {
                    ArrayOfContact arrayOfContact = getSkywardsProfileResponse.getGetSkywardsProfileResult().getContacts();
                    List<Contact> listOfContacts = arrayOfContact.getContact();
                    for (Contact contact : listOfContacts) {
                        if ("MOBIL".equalsIgnoreCase(contact.getContactType())) {
                            contactNumber = contact.getContactNumber();
                            creditScoreBean.setMobileNo(contactNumber);
                            creditScoreBean.setMobileNumberJP(contactNumber);
                        } else {
                            continue;
                        }
                    }//for each
                }//if contacts
            }//if member details are there
        	}
        } catch (Exception e) {
            logger.error("@@@@ Exception in ICICI MemberDetailsServiceImpl getMemberDetails() :", e);
        }

    }
    
    
    @Override
    public String getJPTier(String jpNumber) {
        String tier = "";
        try {
        	doTrustToCertificates();
//        URL obj = new URL("https://apiuat.jetprivilege.com:8082/api/experience/member-profile/v2/fetch-profile?JPNumber="+jpNumber);
        URL obj = new URL("https://apiuat.jetprivilege.com/api/experience/member-profile/v2/fetch-profile?JPNumber="+jpNumber);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.addRequestProperty("partnerID", "HDFC");
		con.addRequestProperty("Authorization", "Basic MTU5aUNYaUUxWVc2Wml2ZFcyZFl3cnN0cEpza2JsS0Q6UmIxR1Z3OERORk5UZ3h1Q2U2Z2JTeUdZWVlBbnE3Y0w=");
		
//		con.addRequestProperty("partnerID", "HDFC"); 
		
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = con.getResponseCode();
		logger.info("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			logger.info(response.toString());
			
			JSONObject jsonObject=new JSONObject(""+response.toString());
			logger.info("-------------<"+jsonObject.toString());
			tier=jsonObject.getString("Tier");
			logger.info("-------------<"+jsonObject.getString("Tier"));
		
			
		} else {
			// Process the response
			String response = null;
			 InputStream inputStream;
		      BufferedReader reader;
		      inputStream = con.getErrorStream();
		      String line = null;
		      reader = new BufferedReader( new InputStreamReader( inputStream ) );
		      while( ( line = reader.readLine() ) != null )
		      {
		        //System.out.println( line );
		        response=response+line;
		      }
			logger.info("GET request not worked"+response);
		}
        }catch (Exception e) {
			
        	e.printStackTrace();
		}

        return tier;
    }
    
    
    public void doTrustToCertificates() throws Exception {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
          TrustManager[] trustAllCerts = new TrustManager[]{
      new X509TrustManager() {
          public java.security.cert.X509Certificate[] getAcceptedIssuers() {
              return null;
          }
          public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
              //No need to implement. 
          }
          public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
              //No need to implement. 
          }
      }
  };
  // Install the all-trusting trust manager
  try {
      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new java.security.SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
  } catch (Exception e) {
      System.out.println(e);
  }
      }

    
    
    public JSONObject fetchProfile(String jpNumber) {
        String tier = "";
        JSONObject jsonObject = null;
        try {
        	doTrustToCertificates();
//        URL obj = new URL("https://apiuat.jetprivilege.com:8082/api/experience/member-profile/v2/fetch-profile?JPNumber="+jpNumber);
        URL obj = new URL("https://apiuat.jetprivilege.com/api/experience/member-profile/v2/fetch-profile?JPNumber="+jpNumber);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.addRequestProperty("partnerID", "HDFC");
		con.addRequestProperty("Authorization", "Basic MTU5aUNYaUUxWVc2Wml2ZFcyZFl3cnN0cEpza2JsS0Q6UmIxR1Z3OERORk5UZ3h1Q2U2Z2JTeUdZWVlBbnE3Y0w=");
		
//		con.addRequestProperty("partnerID", "HDFC"); 
		
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = con.getResponseCode();
		logger.info("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			logger.info(response.toString());
			
			jsonObject=new JSONObject(""+response.toString());
			logger.info("-------------<"+jsonObject.toString());
			tier=jsonObject.getString("Tier");
			logger.info("-------------<"+jsonObject.getString("Tier"));
		
			
		} else {
			// Process the response
			String response = null;
			 InputStream inputStream;
		      BufferedReader reader;
		      inputStream = con.getErrorStream();
		      String line = null;
		      reader = new BufferedReader( new InputStreamReader( inputStream ) );
		      while( ( line = reader.readLine() ) != null )
		      {
		        //System.out.println( line );
		        response=response+line;
		      }
			logger.info("GET request not worked"+response);
		}
        }catch (Exception e) {
			
        	e.printStackTrace();
		}

        return jsonObject;
    }
    
    

}
