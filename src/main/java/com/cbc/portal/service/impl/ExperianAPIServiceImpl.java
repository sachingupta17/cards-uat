/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.service.impl;

import com.cbc.portal.DAO.ExperianAPIDao;
import com.cbc.portal.beans.CreditScoreBean;
import com.cbc.portal.controller.CompareCardController;
import com.cbc.portal.entity.CcCreditErrorMessage;
import com.cbc.portal.entity.CcCreditReport;
import com.cbc.portal.service.ExperianAPIService;
import com.cbc.portal.utils.CommonUtils;
import com.cbc.portal.utils.EncryptDetail;
import com.cbc.portal.utils.GsonUtil;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Aravind E
 */
@Component
@Transactional
public class ExperianAPIServiceImpl implements ExperianAPIService {

    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ExperianAPIServiceImpl.class.getName());

    @Autowired
    ExperianAPIDao experianAPIDao;
    
    @Value("${application.experianUrl}")
    private String experianUrl;
    @Value("${application.cbc.upload.files.folder}")
    private String fileFolderLocation;
    
    @Value("${application.pii.encryption.strSoapURL1}")
   	String strSoapURL1;
   	@Value("${application.pii.encrypt.strSOAPAction1}")
   	String soapActEnc;

   	//    for UAT
   		  private String fileURL="https://app-res-uploads-uat.s3.amazonaws.com/cards/files/";
    
   	//    for Prod
    //	  private String fileURL="https://app-res-uploads.s3.ap-south-1.amazonaws.com/cards/images/ccfiles/";
    
    @Override
    public Map<String, String> callExperianApi(CreditScoreBean bean, int id) {
        Map<String, String> creditResponse = new HashMap<>();
        try {
            creditResponse = singleRequest(bean, id);
        } catch (Exception ex) {
            logger.error("Exception at callExperianApi()", ex);
        }

        return creditResponse;
    }

    /**
     * Call to single action url with json string
     *
     * @param bean
     * @param id
     * @param params
     * @return
     * @throws Exception
     */
    public Map<String, String> singleRequest(CreditScoreBean bean, int id) throws Exception {
        String creditScore = "", creditScoreLevel = "", reportNumber = "", reportDate = "", reportTime = "";
        Map<String, String> map = new HashMap();
        Map<String, String> creditResponse = new HashMap();
        String message = "", errorMessage = "", respStatus = "";
        String range = "";
        JSONObject jsono = null;
        int tempflag=0;
        try {
        	// For UAT Old  
        	// String params = "clientName=JET_PRIVILEGE_EM&allowInput=1&allowEdit=1&allowCaptcha=1&allowConsent=1&allowEmailVerify=1&allowVoucher=1&voucherCode=JetPrivilegeb8U0f&firstName=" + bean.getFname() + "&surName=" + bean.getLname() + "&dateOfBirth=&gender=1&mobileNo=" + bean.getMobileNo() + "&email=" + bean.getEmailId() + "&flatno=&city=&state=&pincode=&pan=&reason=&middleName=&telephoneNo=&telephoneType=0&passport=&voterid=&aadhaar=&driverlicense=&noValidationByPass=0&emailConditionalByPass=1";

        	// For UAT New
        	String params = "clientName=JET_PRIVILEGE_EM&allowInput=1&allowEdit=1&allowCaptcha=1&allowConsent=1&allowEmailVerify=1&allowVoucher=1&voucherCode=JetPrivilegep9DAV&firstName=" + bean.getFname() + "&surName=" + bean.getLname() + "&dateOfBirth=&gender=1&mobileNo=" + bean.getMobileNo() + "&email=" + bean.getEmailId() + "&flatno=&city=&state=&pincode=&pan=&reason=&middleName=&telephoneNo=&telephoneType=0&passport=&voterid=&aadhaar=&driverlicense=&noValidationByPass=0&emailConditionalByPass=1";
        	
        	//  For Prod
            // String params = "clientName=JET_PRIVILEGE_EM&allowInput=1&allowEdit=1&allowCaptcha=1&allowConsent=1&allowEmailVerify=1&allowVoucher=1&voucherCode=Jet PrivilegevneWP&firstName=" + bean.getFname() + "&surName=" + bean.getLname() + "&dateOfBirth=&gender=1&mobileNo=" + bean.getMobileNo() + "&email=" + bean.getEmailId() + "&flatno=&city=&state=&pincode=&pan=&reason=&middleName=&telephoneNo=&telephoneType=0&passport=&voterid=&aadhaar=&driverlicense=&noValidationByPass=0&emailConditionalByPass=1";
        	//String paramsLog = "clientName=JET_PRIVILEGE_EM&allowInput=1&allowEdit=1&allowCaptcha=1&allowConsent=1&allowEmailVerify=1&allowVoucher=1&voucherCode=JetPrivilegep9DAV&firstName=" + getEncValue(bean.getFname()) + "&surName=" + getEncValue(bean.getLname()) + "&dateOfBirth=&gender=1&mobileNo=" + getEncValue(bean.getMobileNo()) + "&email=" + getEncValue(bean.getEmailId()) + "&flatno=&city=&state=&pincode=&pan=&reason=&middleName=&telephoneNo=&telephoneType=0&passport=&voterid=&aadhaar=&driverlicense=&noValidationByPass=0&emailConditionalByPass=1";
            logger.info("Request of experian=====>" + params + "bean=====>" + bean.getJPnumber());
            String url = experianUrl;
            logger.info("Call url : " + url);
          
            URL u = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(3000);
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0");

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(params);
            writer.flush();
            writer.close();
            os.close();

            int status = conn.getResponseCode();
            logger.info("Experian Response status=====>" + status + "==HttpURLConnection===>" + HttpURLConnection.HTTP_OK);
            if (status == HttpURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();


                
                
                logger.info("Response of Experian" + response.toString());
                if (response.toString().equalsIgnoreCase("")) {
                    errorMessage = experianAPIDao.getErrorMessage("", "No response received from API");
                    logger.info("errorMessage = Response not received================>" + errorMessage);
                    creditResponse.put("creditScore", creditScore);
                    creditResponse.put("message", errorMessage);
                    creditResponse.put("emailId", "");
                    creditResponse.put("emailIdflag", "false");
                    creditResponse.put("date", "");

                } else {
                    jsono = new JSONObject(response.toString());
//                    String test=jsono.toJSONString
                    
//                System.out.println("SUCESS REPORT" + jsono.getString("showHtmlReportForCreditReport"));
                    if (!jsono.getString("errorString").equalsIgnoreCase("null")) {

                        errorMessage = experianAPIDao.getErrorMessage(jsono.getString("errorString"), "");
                        logger.info("errorMessage = Api error response================>" + errorMessage);

                        creditResponse.put("creditScore", creditScore);
                        creditResponse.put("message", errorMessage);
                        creditResponse.put("emailId", "");
                        creditResponse.put("emailIdflag", "false");
                        creditResponse.put("date", "");

                        respStatus = "Success";
                    } else {
                        if (!jsono.getString("showHtmlReportForCreditReport").equalsIgnoreCase("null")) {
                            logger.info("============inside showHtmlReportForCreditReport=========");
                            String resposne = jsono.getString("showHtmlReportForCreditReport");

                            String str = escapeXml(resposne);
                            logger.info("Response  ::   " + str);

                            Document doc = convertStringToDocument(str);

                            String str2 = convertDocumentToString(doc);
                            logger.info("converted xml   :: " + str2);

                            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                            Document doct = db.parse(new InputSource(new StringReader(str2)));

                            NodeList list1 = doct.getElementsByTagName("CreditProfileHeader");

                            for (int temp = 0; temp < list1.getLength(); temp++) {

                                Node nNode = list1.item(temp);

                                logger.info("\nCurrent Element :" + nNode.getNodeName());

                                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                                    Element eElement = (Element) nNode;
                                    logger.info("ReportDate : " + eElement.getElementsByTagName("ReportDate").item(0).getTextContent());
                                    logger.info("ReportTime : " + eElement.getElementsByTagName("ReportTime").item(0).getTextContent());
                                    logger.info("ReportNumber : " + eElement.getElementsByTagName("ReportNumber").item(0).getTextContent());

                                    map.put("ReportDate", eElement.getElementsByTagName("ReportDate").item(0).getTextContent());
                                    map.put("ReportTime", eElement.getElementsByTagName("ReportTime").item(0).getTextContent());
                                    map.put("ReportNumber", eElement.getElementsByTagName("ReportNumber").item(0).getTextContent());

                                }
                            }
                            NodeList list2 = doct.getElementsByTagName("SCORE");
                            logger.info("list2   ::  " + list2+" List Length "+list2.getLength());
                          
                            for (int temp = 0; temp < list2.getLength(); temp++) {

                                Node nNode = list2.item(temp);

                                logger.info("\nCurrent Element :" + nNode.getNodeName());

                                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                                    Element eElement = (Element) nNode;

                                    logger.info("bureauscore : " + getEncValue(eElement.getElementsByTagName("BureauScore").item(0).getTextContent()));
                                    logger.info("bureauscoreconfidlevel : " + getEncValue(eElement.getElementsByTagName("BureauScoreConfidLevel").item(0).getTextContent()));
                                    map.put("bureauscore", eElement.getElementsByTagName("BureauScore").item(0).getTextContent());
                                    map.put("bureauscoreconfidlevel", eElement.getElementsByTagName("BureauScoreConfidLevel").item(0).getTextContent());
                                    map.put("respStatus", "Success");
                                }

                            }
                            //logger.info("map===>" + map);
                            JSONObject j = new JSONObject(map);
                            creditScore = j.getString("bureauscore");
                            creditScoreLevel = j.getString("bureauscoreconfidlevel");
                            reportNumber = j.getString("ReportNumber");
                            reportDate = j.getString("ReportDate");
                            reportTime = j.getString("ReportTime");
                            respStatus = j.getString("respStatus");
                            creditResponse.put("creditScore", creditScore);
                            creditResponse.put("message", message);
                            creditResponse.put("emailId", bean.getEmailId());
                            creditResponse.put("emailIdflag", "true");
                            creditResponse.put("date", "");

                            if (!creditScore.equalsIgnoreCase("null")) {

                                int a = Integer.parseInt(creditScore);
                                if (a >= 0 && a <= 560) {
                                    range = "0-560";
                                } else if (a >= 561 && a <= 720) {
                                    range = "561-720";
                                } else if (a >= 721 && a <= 880) {
                                    range = "721-880";
                                } else if (a >= 881 && a <= 960) {
                                    range = "881-960";
                                } else if (a >= 961 && a <= 999) {
                                    range = "961-999";
                                } else {
                                    range = "";
                                }
                            }

                        } else {
                            logger.info("==========No xml generated======");

                            creditResponse.put("creditScore", creditScore);
                            creditResponse.put("message", message);
                            creditResponse.put("emailId", "");
                            creditResponse.put("emailIdflag", "false");
                            creditResponse.put("date", "");

                        }
                        
                        
                    }
                    
                   logger.info("creditScore===>" + getEncValue(creditScore));

                }

            } 
            else {
 
                respStatus = "Failure";
                tempflag=1;
                errorMessage = experianAPIDao.getErrorMessage("", "API Failure");
                logger.info("errorMessage= Api fail ===============>" + errorMessage);
                

            }
            if (id > 0) {
                logger.info("updating user info---------->" + bean.getJPnumber());
                CcCreditReport creditReport;

                creditReport = experianAPIDao.getReportById(id);
                if (creditReport != null) {
                    creditReport.setfName(bean.getFname());
//                    creditReport.setmName(bean.getMname());
                    creditReport.setlName(bean.getLname());
                    creditReport.setMobileNumber(bean.getMobileNo());
                    creditReport.setEmailID(bean.getEmailId());
                    creditReport.setJpNumber(bean.getJPnumber());
//                    creditReport.setCreditScore(creditScore);
                    creditReport.setApplicationDate(new Date());
                    if(tempflag !=1){
                    
                    creditReport.setStageoneID(NullUtils(jsono.getString("stageOneId_")));
                    creditReport.setStagtwoID(NullUtils(jsono.getString("stageTwoId_")));
                    String errormsg = (NullUtils(jsono.getString("errorString")));
                    creditReport.setNewuserID(NullUtils(jsono.getString("newUserId")));
                    creditReport.setErrorMsg(errormsg);
                    }
                    
                    creditReport.setModifiedDate(new Date());
                    creditReport.setCity(bean.getCity());
                    creditReport.setDob(bean.getDob());
                    creditReport.setCreditScoreLevel(creditScoreLevel);
                    creditReport.setCreditScore(creditScore);
                    creditReport.setEmailJPID(bean.getEmailJPID());
                    creditReport.setGender(bean.getGender());
                    creditReport.setMergeJP(bean.getMergeJP());
                    creditReport.setMobileNumberJP(bean.getMobileNumberJP());
                    creditReport.setPromocode(bean.getPromocode());
                    creditReport.setReportDate(reportDate);
                    creditReport.setReportNumber(reportNumber);
                    creditReport.setReportTime(reportTime);
                    creditReport.setStatus(respStatus);
                    creditReport.setTier(bean.getTier());
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:MM:SS");
                    String requestdate = formatter.format(new Date());
                    requestdate = requestdate.replace("/", "");
                    requestdate = requestdate.replace(":", "").trim();
                     requestdate = requestdate.replace(" ", "_").trim();
                    System.out.println("requestdate" + requestdate);
                    String requestfile = createRequestfile(requestdate, params);
                    String responsefile = createResponsefile(requestdate, jsono.toString());
                    creditReport.setRequestFilePath(requestfile);
                    creditReport.setResponseFilePath(responsefile);
                      String APIReq=fileURL + "REQUEST_FILE_"+requestdate+".txt";
                String APIres=fileURL + "RESPONSE_FILE_"+requestdate+".txt";
                
                creditReport.setApiRequest(APIReq);
                creditReport.setResponse(APIres);

                    experianAPIDao.updateAPIDetails(creditReport);

                }

            } else {
                logger.info("Save new user info---------->" + bean.getJPnumber());

                CcCreditReport ccr = new CcCreditReport();
                ccr.setfName(bean.getFname());
//                ccr.setmName(bean.getMname());
                ccr.setlName(bean.getLname());
                ccr.setMobileNumber(bean.getMobileNo());
                ccr.setEmailID(bean.getEmailId());
                ccr.setJpNumber(bean.getJPnumber());
//                ccr.setCreditScore(creditScore);
//                Date dob=dateFormat(bean.getDOB());
//                ccr.setDateOfBirth(dob);
                ccr.setApplicationDate(new Date());
                if(tempflag !=1){
                
                ccr.setStageoneID(NullUtils(jsono.getString("stageOneId_")));
                ccr.setStagtwoID(NullUtils(jsono.getString("stageTwoId_")));
                String errormsg = (NullUtils(jsono.getString("errorString")));
                ccr.setErrorMsg(errormsg);
                ccr.setNewuserID(NullUtils(jsono.getString("newUserId")));
                
                }
                ccr.setCreatedDate(new Date());
                ccr.setCity(bean.getCity());
                ccr.setDob(bean.getDob());
                ccr.setCreditScoreLevel(creditScoreLevel);
                ccr.setCreditScore(creditScore);
                ccr.setEmailJPID(bean.getEmailJPID());
                ccr.setGender(bean.getGender());
                ccr.setMergeJP(bean.getMergeJP());
                ccr.setMobileNumberJP(bean.getMobileNumberJP());
                ccr.setPromocode(bean.getPromocode());
                ccr.setReportDate(reportDate);
                ccr.setReportNumber(reportNumber);
                ccr.setReportTime(reportTime);
                ccr.setStatus(respStatus);
                ccr.setTier(bean.getTier());
//                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//                String requestdate = formatter.format(new Date());
//                requestdate = requestdate.replace("/", "");
//                System.out.println("requestdate" + requestdate);
                
                 SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:MM:SS");
                String requestdate = formatter.format(new Date());
                requestdate = requestdate.replace("/", "");
                requestdate = requestdate.replace(":", "").trim();
                 requestdate = requestdate.replace(" ", "_").trim();
                
                logger.info("requestdate" + requestdate);
                
                
                   String requestfile = createRequestfile(requestdate, params);
                    String responsefile = createResponsefile(requestdate, jsono.toString());
                
                
                
                ccr.setRequestFilePath(requestfile);
                ccr.setResponseFilePath(responsefile);
                String APIReq=fileURL + "REQUEST_FILE_" + requestdate + ".txt";
                String APIres=fileURL + "RESPONSE_FILE_" + requestdate + ".txt";
                
                ccr.setApiRequest(APIReq);
                ccr.setResponse(APIres);

                experianAPIDao.saveAPIDetails(ccr);

            }

        } catch (Exception e) {
            logger.error("Exception at singleRequest()", e);

            creditResponse.put("creditScore", "");
            creditResponse.put("message", "Unable to get your credit score from Experian. Please try again later.");
            creditResponse.put("emailId", "");
            creditResponse.put("emailIdflag", "false");
            creditResponse.put("date", "");
            

        }
        return creditResponse;
    }

    /**
     * Get JSON string from single action response
     *
     * @param conn
     * @return
     * @throws Exception
     */
    private String print(HttpURLConnection conn) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        logger.info("response :: " + response.toString());

        return response.toString();
    }

    /**
     * Parse JSON string from response and put each key-value pair in map
     *
     * @param json
     * @return
     * @throws Exception
     */
    private Map parseJson(String json) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Map.class);
    }

    public String escapeXml(String s) {

        //logger.info("escape special Char " + s);
        return s.replaceAll("&amp;", "&").replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&quot;", "\"");
    }

    private static String convertDocumentToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            // below code to remove XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Date dateFormat(String date) {
        String strDate = "";
        Date date2 = null;
        try {
            Date date1 = new SimpleDateFormat("dd-MMM-yyyy").parse(date);
            logger.info(date + "\t" + date1);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            strDate = formatter.format(date1);
            logger.info("Date Format with dd-M-yyyy hh:mm:ss : " + strDate);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date2 = sdf.parse(strDate);

        } catch (Exception ex) {
            logger.error("Exception at dateFormat()===>", ex);
        }
        return date2;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String checkExistedCreditReport(CreditScoreBean creditScoreBean) {

        List<CcCreditReport> ccList = new ArrayList<>();
        String dBjpNum = "", dbMobile = "", emailId = "", dbScore = "", errorString = "", errorMessage = "", jpNum = creditScoreBean.getJPnumber(), mobile = creditScoreBean.getMobileNo();
        Date applicationdate;
        int id = 0;
        Map<String, String> finalCreditResponse = new HashMap<>();
        Map<String, String> creditResponse = new HashMap<>();
        try {
            ccList = experianAPIDao.getExistedCreditReport(jpNum, mobile);
            logger.info("List  + "+ccList.size());
            if (!ccList.isEmpty()) {
                for (CcCreditReport creport : ccList) {
                    dBjpNum = creport.getJpNumber();
                    dbMobile = creport.getMobileNumber();
                    dbScore = creport.getCreditScore();
                    emailId = creport.getEmailID();
                    Date date = creport.getCreatedDate();
                    String pattern = "yyyy-MM-dd";
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                    String date1 = simpleDateFormat.format(date);
                    System.out.println(date1);
                    if (!creport.getResponse().equalsIgnoreCase("")) {
//                        dbScore = getCreditScore(creport.getResponse());

                    }
                    errorString = creport.getErrorMsg();
                    
                    logger.info("Response readed creditScore===========>" + getEncValue(dbScore));
                    logger.info("errorString of creditScore===========>" + errorString);
                    logger.info("Application Date of creditScore===========>" + creport.getApplicationDate());
                    id = creport.getCrid();
                    applicationdate = creport.getApplicationDate();
                    
                    if (!dBjpNum.isEmpty() && !dbMobile.isEmpty()) {
                        if (dBjpNum.equalsIgnoreCase(jpNum) && !dbMobile.equalsIgnoreCase(mobile)) {
                            logger.info("Old/Same JP Number & New Mobile Number ");
                    logger.info(dBjpNum+" old JPNumber  "+jpNum+"  && "+getEncValue(dbMobile)+" New  mobile"+getEncValue(mobile) );
                            
                            if (!dbScore.equalsIgnoreCase("")) {
                                
                                int days=DatesCal(applicationdate);
                                creditResponse.put("creditScore", "");
//                                creditResponse.put("message", "Credit score is already shared for this IM number. Kindly use different IM number, or enroll for new IM number, to check the credit score associated with it.");
                                creditResponse.put("message", "You have already fetched free credit score using this InterMiles Number, please try again after "+days+" days.");
                                creditResponse.put("emailId", "");
                                creditResponse.put("emailIdflag", "false");
                                creditResponse.put("date", "");
                            } else {
                                creditResponse = callExperianApi(creditScoreBean, id);
                            }

                        } else if (!dBjpNum.equalsIgnoreCase(jpNum) && dbMobile.equalsIgnoreCase(mobile)) {
                            
                            logger.info("New JP Number & Old Mobile Number ");
                            
                    logger.info(dBjpNum+" New JPNumber  "+jpNum+"  && "+getEncValue(dbMobile)+" old  mobile"+getEncValue(mobile) );
//                            System.out.println("New JP Number & Old Mobile Number ");
                            if (!errorString.equalsIgnoreCase("") && errorString.length()> 5) {
                                logger.info("inside  error message condition");
                                if (errorString.equalsIgnoreCase("consumer record not found")) {
                                    errorMessage = experianAPIDao.getErrorMessage(errorString, "");
                                    creditResponse.put("creditScore", "");
                                    creditResponse.put("message", errorMessage);
                                    creditResponse.put("emailId", "");
                                    creditResponse.put("emailIdflag", "false");
                                    creditResponse.put("date", "");

                                } else {
                                    logger.info("Call Experin  API ");
                                    creditResponse = callExperianApi(creditScoreBean, id);
                                }
                            } else {
                                 logger.info("alerday fetch the score  ");
                                int days=DatesCal(applicationdate);
                                creditResponse.put("creditScore", "");
                                creditResponse.put("message", "You have already fetched free credit score using this Mobile Number. Please try again after "+days+" days.");
                                creditResponse.put("emailId", "");
                                creditResponse.put("emailIdflag", "false");
                                creditResponse.put("date", "");

                            }
                        } else if (dBjpNum.equalsIgnoreCase(jpNum) && dbMobile.equalsIgnoreCase(mobile)) {
                            logger.info("Old/Same JP Number & Old Mobile Number (same combination)");
                            SimpleDateFormat dd = new SimpleDateFormat("dd-MM-yyyy");
                            logger.info("applicationdate=======>" + applicationdate);
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(applicationdate);
                            cal.add(Calendar.YEAR, 1); // to get previous year add -1
                            logger.info("-----cal.getTime()--->"+cal.getTime());
                            Date nextYear = cal.getTime();
                                         
                            cal.setTime(nextYear);
                            logger.info("nextYear=======>" + nextYear);
                            logger.info("nextYear=======>" + nextYear);
                            Calendar cal2 = Calendar.getInstance();
                            Date today = cal2.getTime();
                            cal2.setTime(today);
                            logger.info(">>>>>>>>>"+cal);
                            logger.info(">>>>>>>>>"+today);
                            logger.info("current==>after future" + cal2.after(cal));
                            if (cal2.after(cal) == true) {
                                logger.info("Appcation Date out of span====call experian api here");
                                creditResponse = callExperianApi(creditScoreBean, id);

                            } else {
                                logger.info("Application Date within 1 year sapn");
                                
                                if (!errorString.equalsIgnoreCase("null")) {
                                logger.info("errorString" + errorString);
                                    if (errorString.equalsIgnoreCase("consumer record not found")) {
                                        errorMessage = experianAPIDao.getErrorMessage(errorString, "");

                                        creditResponse.put("creditScore", "");
                                        creditResponse.put("message", errorMessage);
                                        creditResponse.put("emailId", "");
                                        creditResponse.put("emailIdflag", "false");
                                        creditResponse.put("date", "");

                                    } else {
                                        creditResponse = callExperianApi(creditScoreBean, id);

                                    }
                                } else {
                                    creditResponse.put("creditScore", dbScore);
                                    creditResponse.put("message", "");
                                    creditResponse.put("emailId", emailId);
                                    creditResponse.put("date", ""+date1);
                                    creditResponse.put("emailIdflag", "false");

                                }

                            }
//                            System.out.println("future==>after current"+cal.after(cal2));
                        }

                    } else {
                        logger.info("rare case  ");
                        creditResponse.put("creditScore", "");
                        creditResponse.put("message", "");
                        creditResponse.put("emailId", "");
                        creditResponse.put("date", "");
                        creditResponse.put("emailIdflag", "false");

                    }
                }
            } else {
                logger.info("New User---------calling Experian Api");
                creditResponse = callExperianApi(creditScoreBean, id);

            }

        } catch (Exception e) {
            logger.error("Exception at checkExistedCreditReport()===>", e);
        }
        return GsonUtil.toJson(creditResponse);
    }

    public String getCreditScore(String response) {
        String creditScore = "";
        Map<String, String> map = new HashMap();
        try {
            JSONObject jsono = new JSONObject(response);
            if (!jsono.getString("showHtmlReportForCreditReport").equalsIgnoreCase("null")) {
                logger.info("xml report in getCreditScore()");
                String resposne = jsono.getString("showHtmlReportForCreditReport");

                String str = escapeXml(resposne);
                logger.info("Response Xml   " + str);

                Document doc = convertStringToDocument(str);

                String str2 = convertDocumentToString(doc);
                logger.info("xml conversion at getCreditScore()=====>" + str2);

                DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doct = db.parse(new InputSource(new StringReader(str2)));

                NodeList list2 = doct.getElementsByTagName("SCORE");
                for (int temp = 0; temp < list2.getLength(); temp++) {

                    Node nNode = list2.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;
                        logger.info("bureauscore at getCreditScore() : " + getEncValue(eElement.getElementsByTagName("BureauScore").item(0).getTextContent()));
                        logger.info("bureauscoreconfidlevel at getCreditScore() : " + getEncValue(eElement.getElementsByTagName("BureauScoreConfidLevel").item(0).getTextContent()));
                        map.put("bureauscore", eElement.getElementsByTagName("BureauScore").item(0).getTextContent());
                        map.put("bureauscoreconfidlevel", eElement.getElementsByTagName("BureauScoreConfidLevel").item(0).getTextContent());
//                        map.put("respStatus", "success");
                    }

                }
            } else {
                map.put("bureauscore", "");
            }
            JSONObject j = new JSONObject(map);
            creditScore = j.getString("bureauscore");
            logger.info("Credit Score  at getCreditScore()====>" + creditScore);
        } catch (Exception e) {
            logger.error("Exception in ExperianAPIServiceImpl at getCreditScore() : ", e);
        }
        return creditScore;
    }

    public String createRequestfile(String date, String data) throws IOException {
        File fw = null;
        String newTextFile = "";
        try {
            
            logger.info("File Location "+fileFolderLocation);
            newTextFile = fileFolderLocation + "REQUEST_FILE_" + date + ".txt";
            logger.info("======>" + fileFolderLocation + "REQUEST_FILE_" + date + ".txt");

            fw = new File(newTextFile);

            if (!fw.exists()) {
                fw.createNewFile();

            }
//Write Content
            FileWriter writer = new FileWriter(fw);

            writer.write(data);
            writer.close();
        } catch (Exception e) {
            logger.error("Exception at createRequestfile()", e);
        }
        logger.info("file name------->" + fw.getName());
        return fw.getName();
    }

    public String createResponsefile(String date, String data) throws IOException {
        File fw = null;
        String newTextFile = "";
        try {
            logger.info("File Location "+fileFolderLocation);
            newTextFile = fileFolderLocation + "RESPONSE_FILE_" + date + ".txt";
            fw = new File(newTextFile);

            if (!fw.exists()) {
                fw.createNewFile();

            }

//Write Content
            FileWriter writer = new FileWriter(fw);
            writer.write(data);
            writer.close();
        } catch (Exception e) {
            logger.error("Exception at createResponsefile()", e);
        }
        logger.info("file name------>" + fw.getCanonicalPath());
        return fw.getName();
    }
    
    
    
    public  int DatesCal(Date date1){
    
        
        int days = 0;
    try{    
        
        SimpleDateFormat dd = new SimpleDateFormat("dd-MM-yyyy");
                            logger.info("applicationdate=======>" + date1);
                            Calendar cal1 = Calendar.getInstance();
                            cal1.setTime(date1);
                            cal1.add(Calendar.YEAR, 1); // to get previous year add -1
                            logger.info("-----cal.getTime()--->"+cal1.getTime());
                            Date nextYear = cal1.getTime();
                                         
                            cal1.setTime(nextYear);
                            logger.info("nextYear=======>" + nextYear);
                            Calendar cal2 = Calendar.getInstance();
                            Date today = cal2.getTime();
                            cal2.setTime(today);
                            String next_date=""+cal1.getTime();
        
        
         DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        Date date = (Date) formatter.parse(next_date);
        logger.info(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String st = "" + cal.get(Calendar.DATE);

        if (st.length() == 1 && st != null) {
            st = "0" + st;
        } 
        
        int i=cal.get(Calendar.MONTH)+1;
        String month1=""+i;
        
         if (month1.length() == 1 && st != null) {
            month1 = "0" + month1;
        }
        
        String formatedDate = cal.get(Calendar.YEAR) + "-" +  month1+ "-" + st;
        logger.info("formatedDate : " + formatedDate);
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = f.format(new Date());

        logger.info("Current Date = " + strDate);
        LocalDate dateBefore = LocalDate.parse(strDate);
        LocalDate dateAfter = LocalDate.parse(formatedDate);
        long noOfDaysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);
        days=(int) noOfDaysBetween;
        logger.info("Number of Days "+noOfDaysBetween);
    }catch(Exception ex){
        logger.info("Exception  ::  "+ex);
    }
        
        
    return days;
    }
    
    
    public  String NullUtils(String value){
  
    
   String returnString = null;
		try{
			returnString = null!=value && !value.isEmpty()?value.trim():"";
			return returnString;
		}catch(Exception e){
			logger.error("Exception in nullsafe", e);
		}
	
        
    return returnString;
    }
    

    
    public String getEncValue(String strVal)
    {
	    org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
	    obj.put("userjson", strVal);
	    StringWriter out = new StringWriter();
	    String encVal = null;
	    try {
			obj.writeJSONString(out);
		    String jsonText = out.toString();
		    EncryptDetail enc = new EncryptDetail();
		    String encryptDataVar = enc.encryptData(jsonText,strSoapURL1,soapActEnc);
		    org.json.JSONObject encryptData;
		    encryptData = new org.json.JSONObject(encryptDataVar);
		    encVal=""+encryptData.getString("userjson");
		
	    } catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    return encVal;
    }
    
    
}
