package com.cbc.portal.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cbc.portal.DAO.CardProductDAO;
import com.cbc.portal.DAO.CardSearchDAO;
import com.cbc.portal.DAO.CommonDAO;
import com.cbc.portal.DAO.RecommendCardDAO;
import com.cbc.portal.beans.CardDetailsBean;
import com.cbc.portal.entity.CcCardFeatures;
import com.cbc.portal.entity.CcCardProduct;
import com.cbc.portal.entity.CcPageContent;
import com.cbc.portal.entity.CcPageGroupLink;
import com.cbc.portal.service.CardDetailsService;
import com.cbc.portal.service.CardSearchService;
import com.cbc.portal.service.RecommendCardService;
import com.cbc.portal.utils.CommonUtils;

/**
 * 
 * @author abhishek.m
 *
 */
@Component
@Transactional(readOnly=true)
public class RecommendCardServiceImpl implements RecommendCardService{

	private Logger logger = Logger.getLogger(RecommendCardServiceImpl.class.getName());
	
	@Autowired
	RecommendCardDAO recommendCardDAO;
	
	@Autowired
	CardDetailsService cardDetailsService;
	
	@Autowired
	CardSearchService cardSearchService;
	
	@Autowired
	CardProductDAO cardProductDAO;
	
	@Autowired
	CardSearchDAO cardSearchDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public List<CardDetailsBean> getRecommndedCards(CardDetailsBean cardDetailsBean) {
		
		int flightRange;
		int age = 0;
		String freeAirTicket = "";
		String monthlySpend;
		String message 		= null;
		String strLyfData 	= null;
		String benefitA 	= "";
		String benefitB 	= "";
		List<Integer> bpNoList;
		List<Integer> cpNoList;
		List<String> cardNameList = null;
		List<String> lyfPrefNameList ;
		Map<Integer, String> freeTicketMap;
		List<CardDetailsBean> cardDetailsBeanList = new ArrayList<>();
		String annualEndPrice = null;
		String annualPrice = "";
		try{
			if(cardDetailsBean.getAnnualIncome().contains("gt")){
//				annualPrice = cardDetailsBean.getAnnualIncome().replace(">", "").trim();
                                annualPrice = cardDetailsBean.getAnnualIncome().replace("gt", "").trim();
			}
			else{
				String [] splitAnnualIncome = cardDetailsBean.getAnnualIncome().split("-");
				annualEndPrice = splitAnnualIncome[1].trim();
//                                  System.out.println("annualPrice"+annualPrice);
			}   
                      
			                 try{
                                         if(cardDetailsBean.getDob().contains(",")){
//                                             System.out.println(""+cardDetailsBean.getDob());
//                                             System.out.println("insde if");
                                      String dob=cardDetailsBean.getDob();
//                                             System.out.println("index"+dob.indexOf(","));
                                      dob=dob.substring(0, dob.indexOf(","));
//                                             System.out.println("dob="+dob);
                                      cardDetailsBean.setDob(dob);
//                                             System.out.println("in if get dob"+cardDetailsBean.getDob());
                                         }else{
//                                          System.out.println("elsed "+cardDetailsBean.getDob());
                                         }
                                         
                                      
                                            
			age = CommonUtils.getAge(cardDetailsBean.getDob());
                        
                                        }catch(Exception Ex){
                                            System.out.println("exception "+Ex);
                                        }
//                                   age=0    ;
//			                 System.out.println("age"+age);
			flightRange  = cardDetailsBean.getFlightRange();
			monthlySpend = cardDetailsBean.getMonthlySpend();
			BigDecimal bd = new BigDecimal(monthlySpend);
			bd=bd.multiply(new BigDecimal("12"));
			String []lyfprf = cardDetailsBean.getLsbpArray();
			List<String> lyfList = new ArrayList<>(Arrays.asList(lyfprf));
			lyfList.removeAll(Arrays.asList("", null));
			strLyfData = lyfList.get(0);
			String [] str = strLyfData.split(",");
			List<Integer> intLyfList = new ArrayList<>();
			for(String strlyfList : str) {
				intLyfList.add(Integer.parseInt(strlyfList)); 
			}
			bpNoList = recommendCardDAO.getBankListByCity(cardDetailsBean.getCityName());
                       
			if(!bpNoList.isEmpty()){
			cardNameList = recommendCardDAO.getCardListByPref(intLyfList);
                       
			}
			if(cardNameList!=null && !cardNameList.isEmpty()){
			cardNameList 	= recommendCardDAO.getEligibilityCardNames(bpNoList,age,annualEndPrice,cardNameList,annualPrice);
                           
			}
			if(null!=cardNameList && !cardNameList.isEmpty()){
				cpNoList 		= recommendCardDAO.getCardNoByName(bpNoList,cardNameList);
				for(Integer cpNo: cpNoList){
//                                    System.out.println("cpno="+cpNo);
					lyfPrefNameList = recommendCardDAO.getLyfPrefNamesList(cpNo,intLyfList);
                       		if(!lyfPrefNameList.isEmpty()){
						 benefitA = lyfPrefNameList.get(0);
						 benefitA = benefitA!=null && benefitA.length() > 0?benefitA:"";
						 if(lyfPrefNameList.size()>1){
							 benefitB = lyfPrefNameList.get(1);
							 benefitB = benefitB!=null && benefitB.length() > 0?" and "+benefitB:"";
						}
						 else{
							 benefitB="";
						 }
					}
					
					CardDetailsBean bean = getRecommendedCardById(cpNo);
                                        
                                        
					freeTicketMap = cardSearchService.calculateFreeFlightsByArray(bd.toString(),cpNo.toString());
                                       
					for (Entry<Integer, String> entry : freeTicketMap.entrySet()) {
						freeAirTicket = entry.getValue();
//                                                System.out.println("freeAirTicket"+freeAirTicket);
//                                                System.out.println("");
				    }
					String miles=freeAirTicket.split(",")[0];
					String free=freeAirTicket.split(",")[1];
					int freeTic=Integer.parseInt(free);
					bean.setJpMiles(miles);
					bean.setFreeTickets(free);
					bean.setLyfList(lyfPrefNameList.size());
					if(freeTic<flightRange){
						message = freeTic+" out of your "+flightRange+" air travel in a year for FREE";
					}
					if(freeTic==flightRange){
						message = "All your "+freeTic+" air travel in a year for FREE";
					}
					if(freeTic>flightRange){
						message = "More Free Flights than your estimated annual travel";
					}
					bean.setTicketMessage(message);
					bean.setLyfPrefMessage(benefitA+benefitB);
					if(null!=bean){
							cardDetailsBeanList.add(bean);
                                                        for(CardDetailsBean c:cardDetailsBeanList){
//                                                            System.out.println("temp"+c.getCpNo());
//                                                            System.out.println("free tickets-->"+c.getFreeTickets());
//                                                            System.out.println("cardname-->"+c.getCardName());
                                                        }
					}
				}
			}
		}
		catch(Exception e){
			logger.error("@@@@ Exception in RecommendCardServiceImpl getRecommndedCards() :",e);
		}
		return cardDetailsBeanList;
	}

	@Override
	public CardDetailsBean getRecommendedCardById(int cpNo) {
		CardDetailsBean cardDetailsBean = null;
		CcCardProduct ccCardProduct=null;
		StringBuilder content;
		List<CcPageGroupLink> ccPageGroupLink= new ArrayList<>();
		List<String> featureList= null;
		List<CcCardFeatures> ccFeature=new ArrayList<>();
		List<Integer> column=new ArrayList<>();
		CcPageContent ccpageContent=null;
		int gmNo = 0;
	try{
		ccCardProduct=cardProductDAO.getCardProductById(cpNo);
		ccPageGroupLink =commonDAO.getGroupHeaders(1);
		ccpageContent=commonDAO.getPageContent();
		 if(null!=ccPageGroupLink){
			 
			 for(CcPageGroupLink ccpage:ccPageGroupLink){
				 column.add(ccpage.getCcGroupMaster().getGmNo());
				 if("Benefits".equalsIgnoreCase(ccpage.getCcGroupMaster().getGroupName()))
					 gmNo = ccpage.getCcGroupMaster().getGmNo();
			 }
		 }
		 
			if(column.size()>3)
				column.subList(3, column.size()).clear();
			
		 Integer [] columOrder=column.toArray(new Integer[column.size()]);
		if(null!=ccCardProduct){
                   
				featureList= new ArrayList<>();
				cardDetailsBean = new CardDetailsBean();
				cardDetailsBean.setCpNo(ccCardProduct.getCpNo());
				cardDetailsBean.setCardName(ccCardProduct.getCardName());
				cardDetailsBean.setBpNo(ccCardProduct.getCcBankPartner().getBpNo());
				cardDetailsBean.setBankName(CommonUtils.nullSafe(ccCardProduct.getCcBankPartner().getBankName()));
				cardDetailsBean.setNetworkName(ccCardProduct.getCcNetworkList().getNetworkName());
				cardDetailsBean.setCardImagePath(ccCardProduct.getCpImagePath());
				cardDetailsBean.setImageText(ccCardProduct.getCpImageText());
				cardDetailsBean.setFees(CommonUtils.checkForDecimal(ccCardProduct.getFees()));
				cardDetailsBean.setLsbpList(cardSearchDAO.getlsbpArray(ccCardProduct.getCpNo()));
				cardDetailsBean.setWelcomeBonus(CommonUtils.checkForDecimal(ccCardProduct.getWelcomeBonus()));
				cardDetailsBean.setJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getJpMiles()));
				cardDetailsBean.setPerSpendsOnJpMiles(CommonUtils.checkForDecimal(ccCardProduct.getPerSpendsOnJpMiles()));
                                cardDetailsBean.setApplyflag(ccCardProduct.getApplyFlag());
                                //new adobe changes 16th july start
                                 cardDetailsBean.setCardInfo(ccCardProduct.getCardInfo());
                                 //new adobe changes 16th july end
                                cardDetailsBean.setDefaultPromocode(CommonUtils.nullSafe(ccCardProduct.getDefaultPromocode(), ""));
				if(null!=ccpageContent)
                                    cardDetailsBean.setApprovalMessage(ccpageContent.getApprovalMessage());
				else
					cardDetailsBean.setApprovalMessage(" ");
						 for(int j=0;j<columOrder.length;j++){
							 ccFeature=cardProductDAO.getCardFeatureforDetails(columOrder[j],ccCardProduct.getCpNo());
							                                            for(CcCardFeatures c:ccFeature){
//                                                                                                      System.out.println("ccFeature"+c.getCfDisplayOrder());
//                                                                                                        System.out.println("cfname"+c.getCfName());
//                                                                                                        System.out.println("cfno"+c.getCfNo());
                                                                                                    }
							if(null!=ccFeature){
								 content = new StringBuilder();
								 for(CcCardFeatures cflist:ccFeature){
									 if(cflist.getCfContent().length()!=0){
										 content=content.append(cflist.getCfContent()+"<br/>");
									 }
									 	
									 }
								 featureList.add(content.toString());
								
								 }
							cardDetailsBean.setFeatureList(featureList);
						 }
						 if(gmNo!=0)
								cardDetailsBean.setGmNo(gmNo);	
		}
	}
	catch(Exception e){
		logger.error("@@@@ Exception in RecommendCardServiceImpl getRecommendedCardById() :",e);
		
	}
	return cardDetailsBean;
	}
}
