package com.cbc.portal.service;

import com.cbc.portal.beans.CardDetailsBean;

public interface CardDetailsService {
	
	CardDetailsBean getCardDetailsById(Integer cpNo);

	String getBenefitContent(int cpNo, int gmNo);

	int getCardIdByName(String cardName);

        //new cr2 milestone2
	        String getJoiningBenefitContent(int cpNo);
}
