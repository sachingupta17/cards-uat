/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.portal.service;

import com.cbc.portal.beans.CreditScoreBean;
import java.util.Map;

/**
 *
 * @author Aravind E
 */
public interface ExperianAPIService {

    public Map<String, String> callExperianApi(CreditScoreBean bean,int id);

    public String checkExistedCreditReport(CreditScoreBean creditScoreBean);

}
