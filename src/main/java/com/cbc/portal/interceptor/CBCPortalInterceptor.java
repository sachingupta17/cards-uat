package com.cbc.portal.interceptor;

import com.cbc.portal.service.MemberDetailsService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cbc.portal.utils.ApplicationPropertiesUtil;
import com.cbc.portal.utils.CrisServicesUtils;
import com.cbc.portal.utils.EncryptDetail;
import com.cbc.portal.utils.ICICIEncryptDetail;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
//import org.apache.axis2.databinding.types.soapencoding.DateTime;
import javax.servlet.http.Cookie;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;

public class CBCPortalInterceptor extends HandlerInterceptorAdapter {

    private Logger logger = Logger.getLogger(CBCPortalInterceptor.class.getName());
    @Autowired
    ApplicationPropertiesUtil applicationPropertiesUtil;

    //for Adobe import
    @Autowired
    CrisServicesUtils crisServicesUtils;
    @Autowired
    MemberDetailsService memberDetailsService;

    @Value("${application.pii.encryption.strSoapURL1}")
    private String strSoapURL1;
    
//	@Value("${application.pii.encrypt.strSOAPAction1}")
//   	String soapActEnc;

    @Value("${application.pii.decrypt.strSOAPAction1}")
    private String strSOAPActiondecrypt;

    @Value("${application.pii.encrypt.strSOAPAction1}")
    private String strSOAPActionencrypt;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String requestUri = request.getRequestURI();
        requestUri = (null != requestUri) ? requestUri.trim() : "";

        //logger.info("req URL --- "+requestUri);
        String appUrl = applicationPropertiesUtil.getApplicationURL();

        if (appUrl.equals("")) {
            request.getSession().setAttribute("appurl", appUrl);
        }

        // Code to delete cookies
        Cookie[] cookie = request.getCookies();
        String number;
        String name;
        String GTMJPNumber;

        if (cookie != null) {
            for (int i = 0; i < cookie.length; i++) {
                if (cookie[i].getName().trim().equals("JetPrivilege_CCounter_C")) {

                    request.getSession().setAttribute("GDPRCheck", true);
                    break;
                } else {
                    request.getSession().setAttribute("GDPRCheck", false);
//                              System.out.println("In ELSE :"+cookie[i].getName());
                }

            }
        }

        Cookie[] cookie1 = request.getCookies();

        if (cookie1 != null) {
            for (int i = 0; i < cookie1.length; i++) {
                
                
                 if (cookie1[i].getName().trim().equals("forceFlag")) {
                     
                    String flag = cookie1[i].getValue();
                     System.out.println("Interceptor "+flag); 
                    request.getSession().setAttribute("forceFlag", flag);
                     
                  }
                

                if (cookie1[i].getName().trim().equals("jpnum")) {
                     number = cookie1[i].getValue();
                    request.getSession().setAttribute("loggedInUser", number);
                    
                    if(number.length()==11){
                        System.out.println("Interceptor Number  "+number);
                       GTMJPNumber=number.substring(2);
                       System.out.println("Interceptor Number  "+GTMJPNumber);
                     request.getSession().setAttribute("gtmloggedInUser", GTMJPNumber);
                    }
                    
                    }

               else if (cookie1[i].getName().trim().equals("user")) {
                     name = cookie1[i].getValue();
                    // System.out.println("----------> Interceptor loggedINUser:----->  "+cookie[i].getValue());
                     //logger.info("----------> Interceptor loggedINUser:----->  "+cookie[i].getValue());
                     //request.getSession().setAttribute("loggedInUserName", name);
                     
                     String name1=cookie1[i].getValue();
                     JSONObject obj = new JSONObject();
                     obj.put("user", name1);
                     StringWriter out = new StringWriter();
                     obj.writeJSONString(out);

                     String jsonText = out.toString();
                     EncryptDetail enc = new EncryptDetail();
                     //logger.info("jsonText=======>" + jsonText);

                     String encryptDataVar = enc.encryptData(jsonText, strSoapURL1, strSOAPActionencrypt);
                     logger.info("encryptDataVar=======>" + encryptDataVar);
                     org.json.JSONObject encryptData = new org.json.JSONObject(encryptDataVar);
                     String encr_name = encryptData.getString("user");
                     logger.info("----------> Interceptor loggedINUser:-----> "+encr_name);
                     request.getSession().setAttribute("loggedInUserName", name1);
                     
                  }
                
               else if (cookie1[i].getName().trim().equals("firstLoginDate")) {
                     
                     request.getSession().setAttribute("firstLoginDate", cookie1[i].getValue());
                  }
               else if (cookie1[i].getName().trim().equals("lastLoginDate")) {
                     name = cookie1[i].getValue();
                     request.getSession().setAttribute("lastLoginDate", cookie1[i].getValue());
                  }
               else if (cookie1[i].getName().trim().equals("firstName")) {
                     name = cookie1[i].getValue();
                     request.getSession().setAttribute("firstName", cookie1[i].getValue());
                  }
                
                
                
                
            }
        }

        String strDigitalData = "";
        String jpNumber = "" + request.getSession().getAttribute("loggedInUser");

        jpNumber = (null != jpNumber) ? jpNumber.trim() : "";
        logger.info("loggedInUser in Interceptor : " + jpNumber);
        request.getSession().setAttribute("loggedInUser", jpNumber);
        if (!jpNumber.equalsIgnoreCase("")) {

            jpNumber = jpNumber.substring(2);
        }
        if (request.getSession().getAttribute("loggedInUser") != null && jpNumber.length() > 8) {
            String loggedInUserDob = crisServicesUtils.getUserDob(jpNumber);
            request.getSession().setAttribute("loggedInUserDob", loggedInUserDob);
//              HttpSession s=request.getSession();
//              s.setAttribute("loggedInUserDob", loggedInUserDob);
            
            logger.info("loggedInUserDob at interceptor======>" + getEncValue(loggedInUserDob, strSoapURL1, strSOAPActionencrypt));
        } else {
            request.getSession().setAttribute("loggedInUserDob", "");
        }

        if (request.getSession().getAttribute("DigitalData") != null) {
            strDigitalData = "" + request.getSession().getAttribute("DigitalData");

        } else {

            if (jpNumber.length() > 8 && request.getSession().getAttribute("DigitalData") == null) {
                strDigitalData = crisServicesUtils.GetDigitalDataJSON(jpNumber, strSoapURL1, strSOAPActionencrypt);

                request.getSession().setAttribute("DigitalData", strDigitalData);
            } else if (request.getSession().getAttribute("EmptyDigitalData") != null) {
                strDigitalData = "" + request.getSession().getAttribute("EmptyDigitalData"); //crisServicesUtils.GetDigitalDataJSON(jpNumber);
            } else {
//             if(jpNumber.length()>8){
                strDigitalData = crisServicesUtils.GetDigitalDataJSON(jpNumber, strSoapURL1, strSOAPActionencrypt);
//             }   

                request.getSession().setAttribute("EmptyDigitalData", strDigitalData);
            }

        }
//    }

        request.getSession().setAttribute("strDigitalData", strDigitalData);
//                System.out.println("First Session:"+request.getSession().getAttribute("CardsSession"));
        if (request.getSession().getAttribute("CardsSession") == null || request.getSession().getAttribute("CardsSession") != request.getSession().getId()) {
            request.getSession().setAttribute("CardsSession", request.getSession().getId());
            request.getSession().setAttribute("CardsSessionTime", new SimpleDateFormat("E MMM dd yyyy HH:mm:ss 'GMT'z").format(new Date()));
        }
        
        
        
        
        

        //Adobe code ends here
        
        
        
        
        
        if (requestUri.endsWith("/") || requestUri.endsWith("home") || requestUri.endsWith("loginDetails")
                || requestUri.endsWith("logout") ||requestUri.endsWith("getmethod") 
                || requestUri.endsWith("initDigitalData") ||requestUri.endsWith("reactHeaderFooter") 
                || requestUri.endsWith("reactFingerprintImpl") ||requestUri.endsWith("currentSession") 
                || requestUri.endsWith("loginSessionSet") ||requestUri.endsWith("jsFileData") 
                || requestUri.endsWith("faq") ||requestUri.endsWith("validateOTP") 
                || requestUri.endsWith("regenerateOTP") ||requestUri.endsWith("generateOTP") 
                || requestUri.endsWith("InterMilesT&amp;C.pdf") ||requestUri.endsWith("InterMilesT&C.pdf") 
                || requestUri.endsWith(".pdf")
                || requestUri.endsWith("cardswidget")
                || requestUri.endsWith("credit-score-application-form")
                || requestUri.endsWith("credit-score-results")
                || requestUri.endsWith("detailsforpcn") || requestUri.endsWith("detailsforjpnumber")
                || requestUri.endsWith("getTier") || requestUri.endsWith("getCity") || requestUri.endsWith("calcfreeflights")
                || requestUri.endsWith("compare-cards")
                || requestUri.endsWith("getTopTableContent") || requestUri.endsWith("enrollme")
                
                || requestUri.endsWith("getBenefitContent") || requestUri.endsWith("getCityNames") || requestUri.endsWith("not-eligible")
                || requestUri.endsWith("request-approved") || requestUri.endsWith("request-disapproved")
                || requestUri.endsWith("request-approval-pending") || requestUri.endsWith("request-amex-approval-pending")
                || requestUri.endsWith("google6b6e28de20453482.html") || requestUri.indexOf("banners") != -1
                || requestUri.indexOf("banks") != -1 || requestUri.endsWith("sitemap.xml")
                || requestUri.indexOf("cards") != -1 || requestUri.indexOf("lifestyles") != -1) {

            return true;
        }
//        else{
//        
//           response.sendRedirect(appUrl);
//        }
        
        return true;
    }
    
    public String  getEncValue(String strval,String strSoapURL1,String soapActEnc)
    {
   	    ICICIEncryptDetail e = new ICICIEncryptDetail();
   	    String encryptDataVar = e.encryptData(strval, strSoapURL1, soapActEnc);
   	    return encryptDataVar;
    }
	
}
