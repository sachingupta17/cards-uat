/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.auth0.controller;

import com.auth0.Auth0User;
//import com.auth0.Authorize;
import com.auth0.NonceUtils;
import com.auth0.SessionUtils;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Aravind E
 */
public class LoginCallback extends BaseController {
    
    private static final long serialVersionUID = 1L;
    HttpSession session;
    private Logger logger = Logger.getLogger(LoginCallback.class.getName());

    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
    }
        public void doGet(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
      
 logger.info("CheckController page");

        final Auth0User user = SessionUtils.getAuth0User(req);
        final boolean loggedIn = user != null;

        req.getSession().setAttribute("loggedIn", loggedIn);
            logger.info("--------logincallback------->"+loggedIn);
        if (null != user) {
            req.getSession().setAttribute("loggedInUser", user.getNickname());
            logger.info("--------logincallbackloop------->"+loggedIn);

            req.getSession().setAttribute("loggedInUserName", user.getName().contains(" ") ? user.getName().split(" ")[0] : user.getName());
        }

        NonceUtils.addNonceToStorage(req);
        final String state = SessionUtils.getState(req);
        String callback = this.logoutReturnUrl+"loginCallback";
        logger.info("callback URL" + callback);
         
        final String authorizationCode = req.getParameter("code");
        String url=callback+"?code="+authorizationCode+"&state="+state;
            System.out.println("-----url  logincallback---------->"+url);
        
//        ServletOutputStream sout = res.getOutputStream();
//        String content = ReadTextUtil.getContents();

//        sout.print(url);

     
      
        res.sendRedirect(url);

    }
}
