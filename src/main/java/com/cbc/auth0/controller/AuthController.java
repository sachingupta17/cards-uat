/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.auth0.controller;

/**
 *
 * @author Aravind E
 */
import com.auth0.Authorize;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth0.Auth0User;

import com.auth0.NonceUtils;
import com.auth0.SessionUtils;
import org.apache.log4j.Logger;

public class AuthController extends BaseController {

    private static final long serialVersionUID = 1L;

    private Logger logger = Logger.getLogger(AuthController.class.getName());

    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
    }

    protected void doGet(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
        logger.info("AuthController page");
        final Auth0User user = SessionUtils.getAuth0User(req);
        final boolean loggedIn = user != null;
        if (!loggedIn) {

//            NonceUtils.addNonceToStorage(req);
            SessionUtils.getState(req);

            String state = SessionUtils.getState(req);

            logger.info("**********state**************" + state);
//                   state=state.replace("&", " ");
//                   final String state1=state;  

            String callback = this.logoutReturnUrl + "loginCallback";

            final String url = Authorize.buildUrl(this.domain, this.clientId, callback, this.scope, state, true);
            res.sendRedirect(url);
        } else {
            setupRequest(req);
            req.getRequestDispatcher("/WEB-INF/jsp/user.jsp").forward(req, res);
        }
    }

}
