package com.cbc.auth0.controller;

import com.auth0.Auth0User;
import com.auth0.SessionUtils;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.log4j.Logger;

public class LoginController extends BaseController {

    private Logger logger = Logger.getLogger(LoginController.class.getName());

    protected void doGet(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
        final Auth0User user = SessionUtils.getAuth0User(req);
        final boolean loggedIn = user != null;

//        if (loggedIn) {
//            res.sendRedirect("/");
//        } 
//        else {
//            if (req.getSession() != null) {
//                req.getSession().invalidate();
//            }
        final String loginUrl = this.portalLoginUrl + "?externalReturnUrl=" + this.externalReturnUrl;
        logger.info(loginUrl);
        res.sendRedirect(loginUrl);
    }
}
//}
