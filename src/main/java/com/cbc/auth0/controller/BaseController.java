/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbc.auth0.controller;

/**
 *
 * @author Aravind E
 */
import com.auth0.Auth0User;
//import com.auth0.CustomAuth0ServletCallback;
import com.auth0.SessionUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

public class BaseController extends HttpServlet {

    protected String domain;
    protected String issuer;
    protected String clientId;
    protected String clientSecret;
    protected String audience;
    protected String connection;
    protected String scope;
    protected String portalLoginUrl;
//    protected String partnerLoginEndpoint;
    protected String externalReturnUrl;
    protected String callbackUrl;
    protected String logoutReturnUrl;
    private static Properties properties = new Properties();

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
			
//			  String ssoPath = System.getProperty("ssofile"); InputStream input = new
//			  FileInputStream(new File(ssoPath.substring(7)));
//			  BaseController.properties.load(input);
			
            BaseController.properties.load(getServletContext().getResourceAsStream("/WEB-INF/ApplicationConfig-DEFAULT.properties"));
        } catch (IOException ex) {
            Logger.getLogger(BaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
//        String test=properties.getProperty("application.cbc.application_URL");
        this.domain = properties.getProperty("application.cbc.auth0.domain");
        this.issuer = properties.getProperty("application.auth0.issuer");
        this.clientId = properties.getProperty("application.cbc.auth0.clientID");
        this.clientSecret = properties.getProperty("application.cbc.auth0.client_secret");
        this.audience = properties.getProperty("auth0.audience");
        this.connection = properties.getProperty("auth0.connection");
        this.scope = properties.getProperty("auth0.scope");
        this.portalLoginUrl = properties.getProperty("application.cbc.auth0.external_auth.url");
//        this.partnerLoginEndpoint = config.getServletContext().getInitParameter("partner_login_endpoint");
//        this.externalReturnUrl = config.getServletContext().getInitParameter("external_return_url");
//        this.callbackUrl = config.getServletContext().getInitParameter("callback_url");
        this.logoutReturnUrl = properties.getProperty("application.cbc.application_URL");
    }

    /**
     * required attributes needed in request for view presentation
     */
    protected void setupRequest(final HttpServletRequest req) {

        final Auth0User user = SessionUtils.getAuth0User(req);
        req.setAttribute("loggedIn", user != null);
        req.setAttribute("user", user);
        req.setAttribute("domain", domain);
        req.setAttribute("issuer", issuer);
        req.setAttribute("clientId", clientId);
        req.setAttribute("audience", audience);
        req.setAttribute("scope", scope);
        req.setAttribute("portalLoginUrl", portalLoginUrl);
//        req.setAttribute("partnerLoginEndpoint", partnerLoginEndpoint);
        req.setAttribute("externalReturnUrl", externalReturnUrl);

        req.setAttribute("callbackUrl", callbackUrl);
        req.setAttribute("logoutReturnUrl", logoutReturnUrl);
    }

}
