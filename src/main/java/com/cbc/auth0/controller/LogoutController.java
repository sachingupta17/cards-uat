package com.cbc.auth0.controller;

import com.auth0.NonceUtils;
import com.cbc.portal.utils.ApplicationPropertiesUtil;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class LogoutController extends BaseController {


 private Logger logger = Logger.getLogger(LoginController.class.getName());
   
    protected void doGet(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {
//        String redirectionURL=req.getHeader("referer");
        String redirectionURL=(String) req.getSession().getAttribute("cuurenturl");
        if(!redirectionURL.contains("home")){
        redirectionURL=this.logoutReturnUrl+redirectionURL;
        }else{
        redirectionURL=this.logoutReturnUrl;
        redirectionURL=redirectionURL.substring(0,redirectionURL.length()-1);
        
                
        }
        
      if (req.getSession() != null) {
          req.getSession().invalidate();
      }
      
//      NonceUtils.removeNonceFromStorage(req);
      final String url = "https://" + this.domain + "/v2/logout?client_id=" +this.clientId + "&returnTo=" +redirectionURL;
     logger.info("***logout URL***"+url);
      res.sendRedirect(url);
    }

}
