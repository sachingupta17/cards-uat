/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auth0;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author Parvti G
 */
import com.auth0.client.auth.AuthAPI;
import com.auth0.exception.APIException;
import com.auth0.exception.Auth0Exception;
import com.auth0.json.auth.TokenHolder;
import com.auth0.json.auth.UserInfo;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.net.AuthRequest;
import com.auth0.net.Request;
import com.cbc.portal.utils.EncryptDetail;
import com.cbc.portal.utils.ICICIEncryptDetail;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;

import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;

public class Auth0CallbackHandler extends HttpServlet {

//    protected Properties properties = new Properties();
    protected String redirectOnSuccess;
    protected String redirectOnFail;
    protected AuthAPI auth0Client;
    protected String audience;
    protected String scope;
    protected String redircetionURL;
    private static Properties properties = new Properties();
    

    private Logger logger = Logger.getLogger(Auth0CallbackHandler.class.getName());
    
    @Value("${application.cbc.application_URL}")
	private String applicationUrl;
    
    //@Value("${application.pii.encryption.strSoapURL1}")
	private static String strSoapURL1 = "http://services.jetprivilege.com/GlobalEncryptDecryptService.asmx";
	//@Value("${application.pii.encrypt.strSOAPAction1}")
	private static String soapActEnc ="http://tempuri.org/Encrypt";

    /**
     * Initialize this servlet with required configuration
     */
    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
        redirectOnSuccess = readParameter("auth0.redirect_on_success", config);
        redirectOnFail = readParameter("auth0.redirect_on_error", config);

        try {
//                    String ssoPath = System.getProperty("ssofile");
//                    InputStream input = new FileInputStream(new File(ssoPath.substring(7)));
//                    Auth0CallbackHandler.properties.load(input);
                    
           Auth0CallbackHandler.properties.load(getServletContext().getResourceAsStream("/WEB-INF/ApplicationConfig-DEFAULT.properties"));
        } catch (IOException ex) {

        }
        final String clientId = (String) properties.get("auth0.client_id");
        final String clientSecret = (String) properties.get("auth0.client_secret");
        final String domain = (String) properties.get("auth0.exchange_domain");
        this.redircetionURL = (String) properties.get("application.cbc.application_URL");
        this.audience = (String) properties.get("auth0.audience");
        this.scope = (String) properties.get("auth0.scope");
        this.auth0Client = new AuthAPI(domain, clientId, clientSecret);
    }

    /**
     * Entrypoint for http request
     *
     * 1). Responsible for validating the request and ensuring the nonce value
     * in session storage matches the nonce value passed to this endpoint. 2).
     * Exchanging the authorization code received with this http request for
     * tokens 3). Getting user profile information using id token 4). Storing
     * both tokens and user profile information into session storage 5).
     * Clearing the stored nonce value out of state storage 6). Handling success
     * and any failure outcomes
     *
   
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    @Override
    public void doGet(final HttpServletRequest req, final HttpServletResponse res)
            throws IOException, ServletException {

        String Jpnumber1 = "" + req.getSession().getAttribute("loggedInUser");
        if (Jpnumber1 == null) {
            Jpnumber1 = "";
        }
        String appurl=redircetionURL;
         appurl=appurl.substring(0, appurl.length()-1);
         
         
        
       
        logger.info("Auth0CallbackHandler class request : " + req.getRequestURL());
        res.setHeader("Access-Control-Allow-Origin", "*");
//        res.setHeader("X-Forwarded-Proto", "https");
       
        
        try {
            if (isValidRequest(req)) {
                
                String str = "";
                JSONObject json = null;
                System.out.println("Valid   ");
                logger.info("Valid request : " + req);
                
                final TokenHolder tokenHolder = fetchTokens(req);
                logger.info("tokenHolder : " + tokenHolder);
                
               final String idToken = tokenHolder.getIdToken();                
                logger.info("idToken : " + idToken);
                ObjectMapper mapper = new ObjectMapper();
                
                String url = "";
                
                //validating idToken starts here
                try {                	
                	PublicKey publicKey = getPublicKeyFromString();
//                	Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) publicKey, null);
//                	JWTVerifier verifier = JWT.require(algorithm).withIssuer("https://sso-dev.intermiles.com/").withAudience("7o0WjAx6DmSMa9Qo9aBC4g97QfbQSyrS").build();
//                	DecodedJWT jwt = verifier.verify(idToken);
//                	DecodedJWT jwt = JWT.decode(idToken);
//            		Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) publicKey, null);
//            		algorithm.verify(jwt);
//            		System.out.println("get ==========> "+jwt.getHeader());
//            		if (jwt.getExpiresAt().before(Calendar.getInstance().getTime())) {
//            			  throw new RuntimeException("Exired token!");
//            			}
//            		logger.info("id ===========> "+jwt.getId());
                	
                	
                	Jws parseClaimsJws = Jwts.parser().setSigningKey(publicKey)
             				.parseClaimsJws(idToken);
                    System.out.println("get =====sdsd=====> "+parseClaimsJws.getHeader());
                    String jsonString = parseClaimsJws.getBody().toString();
                    System.out.println("------------>"+jsonString);
                    
//                    Map<String, String> attributes = new HashMap<String, String>();  
//                    	attributes =	mapper.readValue(jsonString, Map.class);
                    
                    json = decodeJWT(idToken);
                    
//                    logger.info("OIDC============> "+json.get("https://www.intermiles.com/OIDC"));
                    
                    String example = nullSafe(""+json.get("https://www.intermiles.com/OIDC"),"");
                    
//                    if(json.get("https://www.intermiles.com/OIDC").toString().toLowerCase().equals("on")) {
                    if(example.toString().toLowerCase().equals("on")) {
                    	System.out.println("inside oidc on");
                    	logger.info("inside oidc on");
                    	url = "https://www.intermiles.com/";
                    }
                    
                	
                }catch(Exception e) {
                	logger.info("============================>  ",e);
                }
                
                
                //validating idToken ends here
                

                
//                logger.info("provider>>"+provider.toString());
//                logger.info("jwk>>"+jwk);
                
                
//                final String accessToken = tokenHolder.getAccessToken();
//                logger.info("accessToken : " + accessToken);
//                
//                final Tokens tokens = new Tokens(idToken, accessToken, null, null);
//                logger.info("tokens : " + tokens);
//                
//                final UserInfo userInfo = getUserInfo(accessToken);
//                logger.info("userInfo : " + userInfo);
//                
//                final Map<String, Object> attributes = userInfo.getValues();
                
//                logger.info("=====attributes====="+attributes);
                   // by renuka for adobe
//                String firstLoginDate = (String) attributes.get("created_at");
//                req.getSession().setAttribute("firstLoginDate", firstLoginDate);
                
                String firstLoginDate = (String) json.get(url + "created_at");
                req.getSession().setAttribute("firstLoginDate", firstLoginDate);
                
                
//                String lastLoginDate = (String) attributes.get("cris_Member_LastLoginDate");
//                req.getSession().setAttribute("lastLoginDate", lastLoginDate);
                
                
//                String firstName = (String) attributes.get("cris_Member_FirstName");
//                req.getSession().setAttribute("firstName", firstName);
                
//                String jpNumber = getJpnumber(attributes);

                String lastLoginDate = (String) json.get(url + "cris_Member_LastLoginDate");
                req.getSession().setAttribute("lastLoginDate", lastLoginDate);
                
                String firstName = (String) json.get(url + "cris_Member_FirstName");
                req.getSession().setAttribute("firstName", firstName);
               
                String jpNumber = (String) json.get(url + "user_id");
                jpNumber = jpNumber.replace("auth0|", "").trim();
                
                 //For New header&footer Implementation start
//                String Auth0Json = Auth0SettingJson(attributes);
//                req.getSession().setAttribute("Auth0Json", Auth0Json);
                //For New header&footer Implementation end

//               logger.info("attributes : " + attributes);
//               final Auth0User auth0User = new Auth0User(attributes);
//               req.getSession().setAttribute("loggedInUser", auth0User.getNickname());
//               req.getSession().setAttribute("loggedInUserName", auth0User.getName().contains(" ") ? auth0User.getName().split(" ")[0] : auth0User.getName());
//               logger.info("auth0User : " + auth0User);
//               Cookie cookie=new Cookie("jpnum", ""+req.getSession().getAttribute("loggedInUser"));
//               res.addCookie(cookie);
//               Cookie cookie1=new Cookie("user", ""+req.getSession().getAttribute("loggedInUserName"));
//               res.addCookie(cookie1);
//                
//               Cookie cookie3=new Cookie("firstLoginDate", ""+req.getSession().getAttribute("firstLoginDate"));//Added by Arshad create_at to firstLoginDate
//               res.addCookie(cookie3);
//               Cookie cookie4=new Cookie("lastLoginDate", ""+req.getSession().getAttribute("lastLoginDate"));//Modified from cris_Member_LastLoginDate to lastlogindate
//               res.addCookie(cookie4);
//               Cookie cookie5=new Cookie("firstName", ""+req.getSession().getAttribute("firstName"));
//               res.addCookie(cookie5);
                
                
                req.getSession().setAttribute("loggedInUser", json.get("nickname"));
                req.getSession().setAttribute("loggedInUserName", ((String) json.get(url + "name")).contains(" ") ? ((String) json.get(url + "name")).split(" ")[0] : ((String) json.get(url + "name")));
//                logger.info("auth0User : " + auth0User);
                
                logger.info("loggedInUser ======> "+req.getSession().getAttribute("loggedInUser"));
                
                logger.info("loggedInUserName ====> "+getEncValue((String) req.getSession().getAttribute("loggedInUserName"),strSoapURL1, soapActEnc));
                
                //logger.info("loggedInUserName ====> "+req.getSession().getAttribute("loggedInUserName"));
                
                Cookie cookie=new Cookie("jpnum", ""+req.getSession().getAttribute("loggedInUser"));
                res.addCookie(cookie);
                Cookie cookie1=new Cookie("user", ""+req.getSession().getAttribute("loggedInUserName"));
                res.addCookie(cookie1);
                 
                Cookie cookie3=new Cookie("firstLoginDate", ""+req.getSession().getAttribute("firstLoginDate"));//Added by Arshad create_at to firstLoginDate
                res.addCookie(cookie3);
                Cookie cookie4=new Cookie("lastLoginDate", ""+req.getSession().getAttribute("lastLoginDate"));//Modified from cris_Member_LastLoginDate to lastlogindate
                res.addCookie(cookie4);
                Cookie cookie5=new Cookie("firstName", ""+req.getSession().getAttribute("firstName"));
                res.addCookie(cookie5);
                
                
                
//               store(tokens, auth0User, req);
//                NonceUtils.removeNonceFromStorage(req);
               if (!Jpnumber1.equalsIgnoreCase(jpNumber)) {
                    onSuccess(req, res);
               } else {
                    
                    logger.info("----------redircetionURL1------------>"+redircetionURL+ redirectOnSuccess);
                    res.sendRedirect(appurl+ redirectOnSuccess);
//                    res.sendRedirect(url);
//                    res.sendRedirect("http://localhost:8080/cbcportal"+ redirectOnSuccess);
               }

            } else {
                   System.out.println("Session invalid");
               if (req.getSession() != null) {
                    logger.info("inside else ------------session");
                    req.getSession().invalidate();
               }
               System.out.println("Failuer "+req.getRequestURL());
               System.out.println("Resonse "+res.getOutputStream().toString());
               onFailure(req, res, new Exception());

               logger.info("new Exception() : " + new Exception());

            }
        }
        catch (JWTVerificationException exception) {
			// TODO: handle exception
			System.out.println("exception :- "+exception.getLocalizedMessage());
		}
        catch (Exception E) {
            logger.error("****Exception****" + E);
        }

    }

    /**
     * Actions / navigation to take when a request is deemed successful by this
     * callback handler
     */
    protected void onSuccess(final HttpServletRequest req, final HttpServletResponse res)
            throws ServletException, IOException {
        ServletOutputStream sout = res.getOutputStream();
        String script = "";
        String appurl=redircetionURL;
         appurl=appurl.substring(0, appurl.length()-1);
        try {
            
            

            script = req.getParameter("script");
            
             String str =(String) req.getSession().getAttribute("cuurenturl");
             logger.info("cuurenturl =============> "+(String) req.getSession().getAttribute("cuurenturl"));
            if (null != script && script.equals("1")) {
                
                if(str==null){
                str="";
                }
               

                logger.info("-------4-------->"+str);
                System.out.println("-------4-------->"+str);
                
                sout.print(str);
                System.out.println("After printinh sout===========>");
                logger.info("After printinh sout===========>");
                
            } else {
                logger.info("=====redircetionURL+ redirectOnSuccess========>"+appurl+ redirectOnSuccess);
                res.sendRedirect(appurl+ redirectOnSuccess);

            }
            
        } catch (Exception E) {

        }

    }

    /**
     * Actions / navigation to take when a request is deemed unsuccessful by
     * this callback handler
     */
    protected void onFailure(final HttpServletRequest req, final HttpServletResponse res,
            Exception ex) throws ServletException, IOException {
           
        System.out.println("fsdfdsfsdf "+req.getParameter("script"));
        System.out.println("error_description "+req.getParameter("error_description"));
        System.out.println("=================INSIDE FAILURE=====================");
     
        logger.info("=================INSIDE FAILURE=====================");
        
        
        String appurl=redircetionURL;
          String temp="Log user out due to Auth Time expiry";
               temp=temp.trim();
         if((req.getParameter("error_description").trim()).equalsIgnoreCase(temp)){
             
             
             System.out.println("inside condition auth call back handler");
             logger.info("inside condition auth call back handler");
            Cookie forceFlag1=new Cookie("forceFlag", "forcelogout");
                res.addCookie(forceFlag1);
                req.getSession().setAttribute("forceFlag", "forcelogout");
//           res.setHeader("Refresh", "0; URL=http://localhost:8080/cbcportal");
         
         }else{
          Cookie forceFlag1=new Cookie("forceFlag", "");
                res.addCookie(forceFlag1);
                req.getSession().setAttribute("forceFlag", "");
         }
        
        
         appurl=appurl.substring(0, appurl.length()-1);
         Cookie cookie=new Cookie("jpnum", "");
                res.addCookie(cookie);
                Cookie cookie1=new Cookie("user", "");
                res.addCookie(cookie1);
                
                Cookie cookie3=new Cookie("firstLoginDate", "");
                res.addCookie(cookie3);
                Cookie cookie4=new Cookie("lastLoginDate", "");
                res.addCookie(cookie4);
                Cookie cookie5=new Cookie("firstName", "");
                res.addCookie(cookie5);
         
         
         
        final String redirectOnFailLocation = appurl+ redirectOnFail;
        logger.info("failure "+redircetionURL + redirectOnFail);
       
        logger.info("=====failure====>" + redirectOnFailLocation);
		System.out.println("redirecing failure ====================> "+redirectOnFailLocation);
        
        

        
        res.sendRedirect(redirectOnFailLocation);
    }

    /**
     * Store tokens and auth0User
     *
     * @param tokens the tokens
     * @param user the user profile
     * @param req the http servlet request
     */
    protected void store(final Tokens tokens, final Auth0User user, final HttpServletRequest req) {
        SessionUtils.setTokens(req, tokens);
        SessionUtils.setAuth0User(req, user);
    }

    /**
     * Get tokens for this request
     *
     * @param req the http servlet request
     * @return the tokens associated with the authentication request
     * @throws IOException
     */
    protected TokenHolder fetchTokens(final HttpServletRequest req) throws IOException {
        final String authorizationCode = req.getParameter("code");
//        final String redirectUri = req.getRequestURL().toString();
        logger.info("redirectUri TokenHolder" + redircetionURL);

        return this.exchangeCode(authorizationCode, redircetionURL);
    }

    /**
     * Indicates whether the request is deemed valid
     *
     * @param req the http servlet request
     * @return boolean whether this request is deemed valid
     * @throws IOException
     */
    protected boolean isValidRequest(final HttpServletRequest req) throws IOException {

        return !hasError(req) && isValidState(req);
//        return true;

    }

    /**
     * Checks for the presence of an error in the http servlet request params
     *
     * @param req the http servlet request
     * @return boolean whether this http servlet request indicates an error was
     * present
     */
    protected boolean hasError(final HttpServletRequest req) {
        logger.info("hasError  : " + req.getParameter("error") != null);
        return req.getParameter("error") != null;
    }

    /**
     * Indicates whether the nonce value in storage matches the nonce value
     * passed with the http servlet request
     *
     * @param req the http servlet request
     * @return boolean whether nonce value in storage matches the nonce value in
     * the http request
     */
    protected boolean isValidState(final HttpServletRequest req) {
//        boolean status = false;/
//
//        String stateFromRequest = req.getParameter("state");
//        System.out.println("----stateFromRequest----->"+stateFromRequest);
////        stateFromRequest = stateFromRequest.replace(" ", "&");
//        System.out.println("----stateFromReques aftert----->"+stateFromRequest);
////        final String stateFromRequest1=stateFromRequest;
//          System.out.println("----stateFromReques aftert----->"+NonceUtils.matchesNonceInStorage(req, stateFromRequest));
//        logger.info("status    :"+NonceUtils.matchesNonceInStorage(req, stateFromRequest));
//        return NonceUtils.matchesNonceInStorage(req, stateFromRequest);
        return true;

       
    }

    /**
     * Attempts to get the parameter (property) from (servlet) context
     *
     * @param parameter the parameter name to lookup
     * @param config the servlet config to search
     * @return the paramter value
     */
    protected String readParameter(final String parameter, final ServletConfig config) {
        final String initParam = config.getInitParameter(parameter);
        if (StringUtils.isNotEmpty(initParam)) {
            return initParam;
        }
        final String servletContextInitParam = config.getServletContext().getInitParameter(parameter);
        if (StringUtils.isNotEmpty(servletContextInitParam)) {
            return servletContextInitParam;
        }
        throw new IllegalArgumentException(parameter + " needs to be defined");
    }

    protected TokenHolder exchangeCode(final String code, final String callback1) {

        String callback = redircetionURL + "loginCallback";
        logger.info("callback in callback1 " + callback);
        AuthRequest request = auth0Client.exchangeCode(code, callback).setAudience(this.audience).setScope(this.scope);
        logger.info("AuthRequest :" + request);
        
        System.out.println("logger.info(\"AuthRequest :\" + request);  "+request);
        
        try {

            final TokenHolder holder = request.execute();
            return holder;
        } catch (Auth0Exception ex) {
            logger.error("auth Exception" + ex);

            throw new RuntimeException(ex);
        }
    }

    protected UserInfo getUserInfo(String accessToken) {
        final Request<UserInfo> request = auth0Client.userInfo(accessToken);
        
        try {
            UserInfo info = request.execute();
            return info;
        } catch (APIException ex) {
            logger.error("APIException  " + ex);
            throw new RuntimeException(ex);
        } catch (Auth0Exception ex) {
            logger.error("Auth0Exception  " + ex);
            throw new RuntimeException(ex);
        }
    }

    public String getJpnumber(Map hashMap) {
        String str = "";
        Iterator<Map.Entry<String, Object>> itr = hashMap.entrySet().iterator();

        while (itr.hasNext()) {
            Map.Entry<String, Object> entry = itr.next();
            if (entry.getKey().equalsIgnoreCase("sub")) {
                str = "" + entry.getValue();
            } else {

            }

        }

        str = str.replace("auth0|", "").trim();

        return str;
    }
    
    public String Auth0SettingJson(Map hashMap) throws IOException, JSONException
    {
        String strJPNum="";
        String strTier="";
        String strPointBalance="";
        String strName="";
        String strJPNo="";
        String strAge="";
        String strGender="";
        String strCity="";
        String strCountry="";
        String strFirstLoginDate="";
        String strLastLoginDate="";
        String strEmailVerified="";
        String strMobileVerified="";
        String strPersonId="";
        String strActiveStatus="";
        String strMemProgram="";
        String strLoggedInUsing="";
        
//        try {
//            
////            Auth0CallbackHandler.properties.load(getServletContext().getResourceAsStream("/WEB-INF/ApplicationConfig-DEFAULT.properties"));
//        } catch (IOException ex) {
//
//        }
        
         String strSoapURL1 = (String) properties.get("application.pii.encryption.strSoapURL1");
         String strSOAPActionencrypt = (String) properties.get("application.pii.encrypt.strSOAPAction1");
        
        
        // Logic to put value in above variales
        Iterator<Map.Entry<String, Object>> itr = hashMap.entrySet().iterator();
        while (itr.hasNext()) {
       
            Map.Entry<String, Object> entry = itr.next();
            System.out.println("entry  key==>"+entry.getKey()+"entry value"+entry.getValue());
                   if(entry.getKey().equalsIgnoreCase("cris_Member_PointsBalanace")){
                   strPointBalance=(String) entry.getValue();
                   }
                   if(entry.getKey().equalsIgnoreCase("cris_Member_Tier")){
                   strTier=(String) entry.getValue();
                  
                   }
                   if(entry.getKey().equalsIgnoreCase("nickname")){
                   strJPNum=(String) entry.getValue();
                   strJPNo = strJPNum.substring(2);
                   }
                   //for setting logged in using
                   if(strJPNum.contains("@")){
                       strLoggedInUsing="EMAIL";
                   }else{
                     if(strJPNum.equals("00") || strJPNum.length()<10 ){
                         strLoggedInUsing="JPNO";
                     }else{
                         strLoggedInUsing="MOB";
                     }
                   }
                   if(entry.getKey().equalsIgnoreCase("cris_Member_FirstName")){
                   strName=(String) entry.getValue();
                   }
                   if(entry.getKey().equalsIgnoreCase("cris_Member_IsEmailVerified")){
                   strEmailVerified = (String) entry.getValue();
                   }
                   if(entry.getKey().equalsIgnoreCase("cris_Member_IsMobileVerified")){
                   strMobileVerified = (String) entry.getValue();
                   }
                   if(entry.getKey().equalsIgnoreCase("cris_Member_PersonID")){
                   strPersonId = (String) entry.getValue();
                   }
                   if(entry.getKey().equalsIgnoreCase("cris_Member_ActiveStatus")){
                   strActiveStatus = (String) entry.getValue();
                   }
                   if(entry.getKey().equalsIgnoreCase("cris_Member_CountryOfResidence")){
                   strCountry= (String) entry.getValue();
                   }
                   if(entry.getKey().equalsIgnoreCase("cris_Member_Gender")){
                   strGender=(String) entry.getValue();
                   }
                   if(entry.getKey().equalsIgnoreCase("created_at")){
                   strFirstLoginDate=(String) entry.getValue();
                   }
                   if(entry.getKey().equalsIgnoreCase("cris_Member_LastLoginDate")){
                   strLastLoginDate = (String) entry.getValue();
                   }
                   if(entry.getKey().equalsIgnoreCase("cris_Member_MemProgram")){
                   strMemProgram = (String) entry.getValue();
                   }
                   
        }   
                   //Encrypt cris data for Adobe
            JSONObject obj = new JSONObject();
//            obj.put("Age", Age);
            obj.put("Gender", strGender);
            obj.put("CountryCountryResidence", strCountry);
//            obj.put("CityOfResidence", CityOfResidence);

            StringWriter out = new StringWriter();
            obj.writeJSONString(out);

            String jsonText = out.toString();
            EncryptDetail e = new EncryptDetail();
            String encryptDataVar = e.encryptData(jsonText,strSoapURL1,strSOAPActionencrypt);
            org.json.JSONObject encryptData = new org.json.JSONObject(encryptDataVar);
//            String encr_Age = encryptData.getString("Age");
            String encr_Gender = encryptData.getString("Gender");
            String encr_CountryResidence = encryptData.getString("CountryCountryResidence");
//            String encr_CityOfResidence = encryptData.getString("CityOfResidence");
                  
        
        
             // final string
                    String strJson="window.UD = \n" +
            "        { \n" +
            "        JPNumber: \""+strJPNo+"\", \n" +
            "        name: \""+strName+"\", \n" +
            "        pointsBalance: \""+strPointBalance+"\", \n" +
            "        EmailVerified: \""+strEmailVerified+"\", \n" +
            "        MobileVerified: \""+strMobileVerified+"\", \n" +
            "        PersonId: \""+strPersonId+"\", \n" +
            "        activeStatus: \""+strActiveStatus+"\", \n" +
            "        countryofResidence: \""+encr_CountryResidence+"\", \n" +
            "        gender: \""+encr_Gender+"\", \n" +
            "        firstloginDate: \""+strFirstLoginDate+"\", \n" +
            "        lastloginDate: \""+strLastLoginDate+"\", \n" +  
            "        memProgram: \""+strMemProgram+"\", \n" + 
            "        LoggedInUsing: \""+strLoggedInUsing+"\", \n" +   
            "        tier: \""+strTier+"\",  }";
        
        return strJson;
    }
    
public  PublicKey getPublicKeyFromString()  {
		
//		String publicKeyPEM = key;
    	logger.info("publicKeyStarted=====>");
    	System.out.println("publicKeyStarted=====>");
		PublicKey pubKey = null;
		
//	---- UAT ---- 
		String pem = "-----BEGIN CERTIFICATE-----\r\n" + 
				"MIIDCzCCAfOgAwIBAgIJClhSWglbkj4cMA0GCSqGSIb3DQEBCwUAMCMxITAfBgNV\r\n" + 
				"BAMTGHNzby1kZXYuamV0cHJpdmlsZWdlLmNvbTAeFw0xOTA4MjIwMTUwMzZaFw0z\r\n" + 
				"MzA0MzAwMTUwMzZaMCMxITAfBgNVBAMTGHNzby1kZXYuamV0cHJpdmlsZWdlLmNv\r\n" + 
				"bTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMh/QraiL4uRrOULRTiU\r\n" + 
				"X1jhy86S1eFCvIIT8zdYA9FKJQoJDlqCv13w5eaEw7DTBfzFjLT+DE6knGvzM/vP\r\n" + 
				"GaG47jC3bSaLXUrtIBq5FkdVNSVvfqa7M0ivbpdcd3lV6vI90amVp0xB5OfHGeEm\r\n" + 
				"SY8JlaOvXFUVleJ+V9Kj7Z57U0g1UpW30fn0puozniAHyh7BAimlpB/uiZYCQzxn\r\n" + 
				"GQgIBZWpI80efMGP5tMnfT8obmpG4oGivjoOaoN3c3oigTV5i+mA+xt7yLv71VSg\r\n" + 
				"uDtNZs1EKqTDF0GqoIAB5HHi6Ynni2sxsUuJxe66F6ZOxdW1ws3e8XByY+pxKwGL\r\n" + 
				"b8sCAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUxxLHUMhkRHB5\r\n" + 
				"TU+Ay8MOeI4OmYQwDgYDVR0PAQH/BAQDAgKEMA0GCSqGSIb3DQEBCwUAA4IBAQDF\r\n" + 
				"3gI9kGw7A/MOCHHWk8lvoeeLtRcXhWpy6kFPrAXtOY3pgVrG9E64EGmyBfyPI8XL\r\n" + 
				"kBtx9Ok8mtHoLwvulyGYVcCNsFVwvn1XK9bqJwqEe7OslHPQLMu0dASZ7qMnzH6Z\r\n" + 
				"ksk91bz/k0xCX2JkMwH0v+7pKpSfWc0M03fNORbvH/3ZEHcEEI7upLn8xPdPxSlD\r\n" + 
				"wQ/bpO7e6h9gbCP5rX7GDXgBMyHZkgGK5ZG3fG8B08PC7h0/XYqSM07fEIRUPP5b\r\n" + 
				"5DJ8HuDkvmYqTebKyKkHXe9yXVJ/0rY/sCDeYRuAksSU9lRfqQzutw2v9kw7zavu\r\n" + 
				"1pHe/J/PfeVeiApnmHWr\r\n" + 
				"-----END CERTIFICATE-----";

//		---- PROD ----
//		String pem = "-----BEGIN PUBLIC KEY-----\r\n"+
//				"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1trb8Uevr7m1ShbrRxL/\r\n"+
//				"0JIagCV5vyn3IThpV1NEkgd7QKTDyRwV+KzV1wGlzNHI0KsGvMHhtNzjnWFQBDFp\r\n"+
//				"LZ+jjVANgT8TNPmjjK1hyxQKPTpZDBbqdfN8IQtpSJYor1r+QggMIrjnxBMS77ux\r\n"+
//				"ci29MX7WK/yKizupXijVHHIl2TdqJzlT75T95Wh66Lf7HRiw8bX/BR+hBkcHNdl7\r\n"+
//				"pbvvl0/xxY8oqpDO/v/4vMgbeOPYRSpxWLfsVjcqwzxubDKiaWSzxr9jedlcxvhe\r\n"+
//				"po8xvmM24GtZ1hJoxp6foQTrBVXReM/3drJ4PEpePjXU8Ydaf+Mr7EvGxngggzRd\r\n"+
//				"cQIDAQAB\r\n"+
//				"-----END PUBLIC KEY-----";
		


		
		try {
			
			//local
//			String path = "http://localhost:8080/cbcportal/static/images/sso.pem";
			
			//uat
//			String path = "https://shopuat.intermiles.com/static/images/sso.pem";
			
//			logger.info("file path ===> "+path);
//			System.out.println("file path ===> "+path);
//			FileInputStream fin = new FileInputStream(path);
			
//			 UAT
			InputStream fin = new ByteArrayInputStream(pem.getBytes());
			System.out.println("fin=================> "+fin.toString());
			CertificateFactory f = CertificateFactory.getInstance("X.509");
			X509Certificate certificate = (X509Certificate)f.generateCertificate(fin);
			pubKey = certificate.getPublicKey();
			


//			 PROD current 2021
//			 String publicKeyPEM ="-----BEGIN PUBLIC KEY-----\n" + 
//				 		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1trb8Uevr7m1ShbrRxL/\n" + 
//				 		"0JIagCV5vyn3IThpV1NEkgd7QKTDyRwV+KzV1wGlzNHI0KsGvMHhtNzjnWFQBDFp\n" + 
//				 		"LZ+jjVANgT8TNPmjjK1hyxQKPTpZDBbqdfN8IQtpSJYor1r+QggMIrjnxBMS77ux\n" + 
//				 		"ci29MX7WK/yKizupXijVHHIl2TdqJzlT75T95Wh66Lf7HRiw8bX/BR+hBkcHNdl7\n" + 
//				 		"pbvvl0/xxY8oqpDO/v/4vMgbeOPYRSpxWLfsVjcqwzxubDKiaWSzxr9jedlcxvhe\n" + 
//				 		"po8xvmM24GtZ1hJoxp6foQTrBVXReM/3drJ4PEpePjXU8Ydaf+Mr7EvGxngggzRd\n" + 
//				 		"cQIDAQAB\n" + 
//				 		"-----END PUBLIC KEY-----";
						 
//						 
//				            "-----BEGIN PUBLIC KEY-----\n" +
//				                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAixn0CGu8/M4txn4pdp8K\n" +
//				                "m8RQfVa+cHX25/a5sPmzP49u7YlQsRvtOexzgdwDcfUJm3hHMZcbZBtrHKsS8q4Q\n" +
//				                "QtGQioyVml8EaLuFNFYisaIEldVyRbXFG54FNp03vSU9ImS/cOiM9swo+1w5JgWO\n" +
//				                "F9efy7JO40LA9E7lv64COUYjFhrn+HRZuKoblL19+Sj49FyXexAUS29UM9PfIdY6\n" +
//				                "ar1FA8cxzPqW7EkXZ0Mua3IzNnYcjMvUL9TJwoLAAz9S1Tv4Is5jupy9UXkuJ4r8\n" +
//				                "Jx9DqI3Q3ur0VekYSd5tnTI4K+no9ABCFVv7+6Q45Ec2eB0xMwlqI+phcGhGMVCX\n" +
//				                "1QIDAQAB\n" +
//				                "-----END PUBLIC KEY-----";

				        // decode to its constituent bytes
//				        publicKeyPEM = publicKeyPEM.replace("-----BEGIN PUBLIC KEY-----\n", "");
//				        publicKeyPEM = publicKeyPEM.replace("-----END PUBLIC KEY-----", "");
//				        BASE64Decoder base64Decoder = new BASE64Decoder();
//				        byte[] publicKeyBytes = base64Decoder.decodeBuffer(publicKeyPEM);

				        // create a key object from the bytes
//				        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
//				        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//				        pubKey = keyFactory.generatePublic(keySpec);

				 
		     
			System.out.println("publicKeyPEM>>>"+pubKey);
			logger.info("publicKeyPEM>>>"+pubKey);
			System.out.println("publicKeyPEM>>>"+pubKey);
			
		
		}catch(Exception e) {
			logger.info("Exception getPublicKeyFromString e>"+e);
			System.out.println("Exception getPublicKeyFromString e>"+e);
			e.printStackTrace();
		}
		
		return pubKey;
	}
    
    
 public JSONObject decodeJWT(String jwtToken){
    	
    	JSONObject json = null;
    	JSONObject jsonLogs = null;
    	
    try {
//    	 jwtToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlFUQXhRa1l5UWpNNVF6RTVRMEU0TkVFM09EaEZRakUxT0RVNVFrRkVOMFF5TWpJeFJqUkRRUSJ9.eyJjcmlzX01lbWJlcl8yRkEiOiJ0cnVlIiwiaHR0cHM6Ly93d3cuaW50ZXJtaWxlcy5jb20vT0lEQyI6Ik9GRiIsIm5pY2tuYW1lIjoiMDAyMTAxOTgyNjIiLCJnaXZlbl9uYW1lIjoiWSIsImZhbWlseV9uYW1lIjoiQyIsIm5hbWUiOiJZIEMiLCJlbWFpbCI6InlvZ2VzaC5jaGF2YW5AdmVybm9zdC5pbiIsInVzZXJfbWV0YWRhdGEiOnt9LCJjcmlzX01lbWJlcl9MYXN0TG9naW5EYXRlIjoiMjAyMC0wNi0yM1QxOTo1NDozOSIsImNyaXNfTWVtYmVyX0FjdGl2ZUNhcmRObyI6IjAwMjEwMTk4MjYyIiwiY3Jpc19NZW1iZXJfRmlyc3ROYW1lIjoiWSIsImNyaXNfTWVtYmVyX0xhc3ROYW1lIjoiQyIsImNyaXNfTWVtYmVyX0VtYWlsQWRkcmVzcyI6InlvZ2VzaC5jaGF2YW5AdmVybm9zdC5pbiIsImNyaXNfTWVtYmVyX01vYmlsZU51bWJlciI6IjkxOTk2Nzk0MTA1NSIsImNyaXNfTWVtYmVyX0lzTW9iaWxlVmVyaWZpZWQiOiJZIiwiY3Jpc19NZW1iZXJfSXNFbWFpbFZlcmlmaWVkIjoiWSIsImNyaXNfTWVtYmVyX1BlcnNvbklEIjoiODY5MzAwMSIsImNyaXNfTWVtYmVyX01lbVByb2dyYW0iOiJPTSIsImNyaXNfTWVtYmVyX1RpdGxlIjoiTXIiLCJjcmlzX01lbWJlcl9HZW5kZXIiOiJNIiwiY3Jpc19NZW1iZXJfQ291bnRyeU9mUmVzaWRlbmNlIjoiSU4iLCJjcmlzX01lbWJlcl9Qb2ludHNCYWxhbmFjZSI6IjIyMDY4OCIsImNyaXNfTWVtYmVyX1RpZXIiOiJCQVNFIiwiY3Jpc19NZW1iZXJfRmxhZ1NpdGVSZWdpc3RlcmVkIjoiWSIsImNyaXNfTWVtYmVyX0FjdGl2ZVN0YXR1cyI6IkFDVCIsImNyaXNfTWVtYmVyX01lcmdlZENhcmRObyI6IiIsIl9jcmlzX3Byb2ZpbGVfbG9hZGVkX2F0IjoxNTkyOTIyMjgyNDUxLCJwaWN0dXJlIjoiaHR0cHM6Ly9zLmdyYXZhdGFyLmNvbS9hdmF0YXIvYmZlYWExODNhNGFiMmUwMDIwMzg5ZDhkYTc3YmQwMDU_cz00ODAmcj1wZyZkPWh0dHBzJTNBJTJGJTJGY2RuLmF1dGgwLmNvbSUyRmF2YXRhcnMlMkZ5Yy5wbmciLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiY2xpZW50SUQiOiI3bzBXakF4NkRtU01hOVFvOWFCQzRnOTdRZmJRU3lyUyIsInVwZGF0ZWRfYXQiOiIyMDIwLTA2LTIzVDE0OjI0OjQyLjQ2MVoiLCJ1c2VyX2lkIjoiYXV0aDB8MDAyMTAxOTgyNjIiLCJpZGVudGl0aWVzIjpbeyJ1c2VyX2lkIjoiMDAyMTAxOTgyNjIiLCJwcm92aWRlciI6ImF1dGgwIiwiY29ubmVjdGlvbiI6IkNSSVMiLCJpc1NvY2lhbCI6ZmFsc2V9XSwiY3JlYXRlZF9hdCI6IjIwMTktMDktMjRUMTE6NTI6MzkuMTc4WiIsImh0dHBzOi8vd3d3O2ludGVybWlsZXM7Y29tL09JREMiOiJPRkYiLCJpc3MiOiJodHRwczovL3Nzby1kZXYuaW50ZXJtaWxlcy5jb20vIiwic3ViIjoiYXV0aDB8MDAyMTAxOTgyNjIiLCJhdWQiOiI3bzBXakF4NkRtU01hOVFvOWFCQzRnOTdRZmJRU3lyUyIsImlhdCI6MTU5MjkyMjI4OCwiZXhwIjoxNTkyOTU4Mjg4LCJhdF9oYXNoIjoiaWtKeFJuajhpUm1aMVlTbHNRRnlyUSIsIm5vbmNlIjoiTk9OQ0UifQ.IKHoeFurOZMFKufUGhoe_GSjf--4-S8yXECMMPAFTN4pWXMF9bY-GXowUgajq3BQvMRjgHs_QhWfZdKoGCsp4-ovhFz1CCpeWT3OumalDALN8sS0lRKNvMl3_ECJxnH2BkBgid3GHfRUqI3RA7QnIWB_OgQfQ7flC3Mau4X_At9HAqpN6HjzVyIPTnN_3o7jMpXz7ii_u2ZQQYVRts8Gs1pN0jmMiITso0xc-Mg6OLCWNhP1vkEJ_UCmwFKNfJ1Gd1qEKjPX7Hv1_mPP3c__nMtC46zv4seZ4wWTLCW2PZ_qSSEw_Io8AZJ0I4wGNGd1mxnQBX33umRf0t493xkCxQ";
    	String oidcValue = "";
         System.out.println("------------ Decode JWT ------------");
         logger.info("------------  JWT jwtToken------------"+jwtToken);
         String[] split_string = jwtToken.split("\\.");
         logger.info("------------ split_string ------------"+split_string);
         String base64EncodedHeader = split_string[0];
         String base64EncodedBody = split_string[1];
         String base64EncodedSignature = split_string[2];

         System.out.println("~~~~~~~~~ JWT Header ~~~~~~~");
         logger.info("~~~~~~~~~ JWT Header ~~~~~~~");
         Base64 base64Url = new Base64(true);
         String header = new String(base64Url.decode(base64EncodedHeader));
         System.out.println("JWT Header : " + header);


         System.out.println("~~~~~~~~~ JWT Body ~~~~~~~");
         logger.info("~~~~~~~~~ JWT Body ~~~~~~~");
         String body = new String(base64Url.decode(base64EncodedBody));
         System.out.println("JWT Body : "+body);
         
         
         //logger.info("JWT Body : "+body);
         
         JSONParser parser = new JSONParser();
         json = (JSONObject) parser.parse(body);
         jsonLogs = (JSONObject) parser.parse(body);
         
//         Map<String, Object> newMap = new HashMap<String, Object>();
//         newMap = json.get("");
         
         String url = "https://www.intermiles.com/";
         String lastLoginDate = (String) jsonLogs.get(url + "cris_Member_LastLoginDate");
         String firstName = (String) jsonLogs.get(url + "cris_Member_FirstName");
         String email = (String) jsonLogs.get(url + "cris_Member_EmailAddress");
         String lastName = (String) jsonLogs.get(url + "cris_Member_LastName");
         String mobNo = (String) jsonLogs.get(url + "cris_Member_MobileNumber");
         String pointsBal = (String) jsonLogs.get(url + "cris_Member_PointsBalance");
         String tier = (String) jsonLogs.get(url + "cris_Member_Tier");
         String userId = (String) jsonLogs.get(url + "user_id");
         String name = (String) jsonLogs.get(url + "name");
         String gender = (String) jsonLogs.get(url + "cris_Member_Gender");
         
         jsonLogs.put(url + "cris_Member_LastLoginDate", getEncValue(lastLoginDate,strSoapURL1, soapActEnc));
         jsonLogs.put(url + "cris_Member_FirstName", getEncValue(firstName,strSoapURL1, soapActEnc));
         jsonLogs.put(url + "cris_Member_EmailAddress", getEncValue(email,strSoapURL1, soapActEnc));
         jsonLogs.put(url + "cris_Member_LastName", getEncValue(lastName,strSoapURL1, soapActEnc));
         jsonLogs.put(url + "cris_Member_MobileNumber", getEncValue(mobNo,strSoapURL1, soapActEnc));
         jsonLogs.put(url + "cris_Member_PointsBalance", getEncValue(pointsBal,strSoapURL1, soapActEnc));
         jsonLogs.put(url + "cris_Member_Tier", getEncValue(tier,strSoapURL1, soapActEnc));
         jsonLogs.put(url + "user_id", getEncValue(userId,strSoapURL1, soapActEnc));
         jsonLogs.put(url + "name", getEncValue(name,strSoapURL1, soapActEnc));
         jsonLogs.put(url + "cris_Member_Gender", getEncValue(gender,strSoapURL1, soapActEnc));
         
         Map<String, Object> attributes = new HashMap<String, Object>();
         Set<Entry<String, JsonElement>> entrySet = jsonLogs.entrySet();
         
         
         logger.info("OIDC>>>>"+entrySet);
         
//         for(Map.Entry<String,JsonElement> entry : entrySet){        
//        	 logger.info("entry.getKey()>>>>"+entry.getKey()+"Vaule>>>>"+entry.getValue());
//         }
         
         logger.info("OIDC Passing>>>>>>"+json.get("https://www.intermiles.com/OIDC"));
         return json;
    	
    }catch(Exception e) {    	
    	logger.info("e>>"+e);
    }
	return json;
	
    	
    }
 
 public String nullSafe(String givenString, String defaultString){
		String returnString;
		try{
			returnString = null!=givenString && !givenString.isEmpty()?givenString.trim():defaultString;
			return returnString;
		}catch(Exception e){
			logger.error("Exception in nullsafe", e);
		}
		return "";
	}

 
 public String  getEncValue(String strval,String strSoapURL1,String soapActEnc)
 {
	    ICICIEncryptDetail e = new ICICIEncryptDetail();
	    String encryptDataVar = e.encryptData(strval, strSoapURL1, soapActEnc);
	    return encryptDataVar;
 }
 
}