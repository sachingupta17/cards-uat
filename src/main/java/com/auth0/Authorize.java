/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.auth0;

import java.util.logging.Logger;

/**
 *
 * @author Aravind E
 */
public class Authorize {
    
    private static  Logger logger = Logger.getLogger(Authorize.class.getName());
    public static String buildUrl(final String domain, final String clientId, final String callbackUrl,
      final String scope, final String state, final boolean silentAuth) {
      final StringBuilder builder = new StringBuilder("https://").append(domain).append("/authorize?response_type=code&client_id=")
        .append(clientId).append("&redirect_uri=").append(callbackUrl).append("&state=").append(state).append("&scope=").append(scope);
      if (silentAuth) {
        builder.append("&prompt=none");
      }
      final String url = builder.toString();
      logger.info("build URL "+url);
//        System.out.println("---------------->"+url);
     return url;
  }
}
