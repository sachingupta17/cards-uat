package com.cc;

import com.cc.BuildXMLRequest;
import com.cc.GenerateToken;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;


public class SilverPopApiCards {
	
	
	
	private static Logger logger = Logger.getLogger(SilverPopApiCards.class.getName());
	private static final int BUFFER_SIZE = 50000;
	//public void silverPopAPICall(Long templateId, ArrayList<String> column_names){
	
	//comment the main method line while integrating and uncomment the above line
//		public static void main(String args[]){
		public static void callSilverPopApi(String url1, String clientId,String clientSecret, String refreshToken){	
		
			Properties prop = new Properties();
			InputStream input = null;
			
			
			                 System.out.println("inside amex silver pop class");
			
			TransactRequestType transReqType=new TransactRequestType();
			
			//pass parameters here
			
			String request = BuildXMLRequest.BuildXMLString();
			System.out.println(request);
			
			String sendEncoding = "utf-8";
			HttpURLConnection urlConn = null;
			OutputStream out = null; 
			InputStream in = null;
			
			try {

				String oAuth = "Bearer " + GenerateToken.getAccessToken(url1,clientId,clientSecret,refreshToken);

				URL url = new URL("http://transact4.silverpop.com/XTMail");
				
				urlConn = (HttpURLConnection)url.openConnection(); 
				urlConn.setRequestMethod("POST");
				urlConn.setDoOutput(true);
				urlConn.setRequestProperty("Content-Type","text/xml;charset=" + sendEncoding);
				urlConn.setRequestProperty ("Authorization", oAuth);
				urlConn.connect();
				System.out.println("connected");
				out = urlConn.getOutputStream();
				out.write(request.getBytes(sendEncoding));
				out.flush();
				in = urlConn.getInputStream();
				InputStreamReader inReader = new InputStreamReader(in, sendEncoding); 
				StringBuffer responseBuffer = new StringBuffer();
				
				char[] buffer = new char[BUFFER_SIZE];
				int bytes;
				
				System.out.println("conn");
				while ((bytes = inReader.read(buffer)) != -1) {
				responseBuffer.append(buffer, 0, bytes);
				}
				
				String response = responseBuffer.toString();
				logger.info("\n******************** Response ********************\n"+response);
				System.out.println(response);
			
		} 
			catch( MalformedURLException e){
				logger.error("Exception in CallTranscatApi: "+e.getMessage());
				e.printStackTrace();
			}
			catch(UnsupportedEncodingException e){
				logger.error("Exception in CallTranscatApi: "+e.getMessage());
				e.printStackTrace();
			}
	        catch(ProtocolException e){
	        	logger.error("Exception in CallTranscatApi: "+e.getMessage());
				e.printStackTrace();
			}
			catch(IOException e){
				logger.error("Exception in CallTranscatApi: "+e.getMessage());
				e.printStackTrace();
			}
			finally {
				if (out != null) {
					try {out.close();} catch (Exception e) {}
				}
				if (in != null) {
					try {in.close();} catch (Exception e) {}
				}
				if (urlConn != null) { urlConn.disconnect();
				}

			}
		
	}

}

