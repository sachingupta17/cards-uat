package com.cc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TransactRequestType {

	
	private static long campaign_id;
	private static long transaction_id;
	private static boolean show_all_send_details;
	private static HashMap<String,String> Columns;
	private static String email;
	private static String body_type;
	private static ArrayList columnsList;
	
	
	public ArrayList<String> getColumnsasList(ArrayList columns) {
		return columnsList;
	}
	public void setColumnsasList(ArrayList columns) {
		this.columnsList = columns;
	}
	public long getCampaign_id() {
		return campaign_id;
	}
	public void setCampaign_id(long campaign_id) {
		this.campaign_id = campaign_id;
	}
	public long getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(long transaction_id) {
		this.transaction_id = transaction_id;
	}
	public boolean getShow_all_send_details() {
		return show_all_send_details;
	}
	public void setShow_all_send_details(boolean show_all_send_details) {
		this.show_all_send_details = show_all_send_details;
	}
	public HashMap<String, String> getColumns() {
		return Columns;
	}
	public void setColumns(HashMap<String, String> columns) {
		Columns = columns;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBody_type() {
		return body_type;
	}
	public void setBody_type(String body_type) {
		this.body_type = body_type;
	}

   	
	
	
	
	

}
