
package com.cc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

public class BuildXMLRequest {
	
	final static Logger logger = Logger.getLogger(BuildXMLRequest.class);
	
	
	public static String BuildXMLString() {
				
		
		/*
		 * Sample XML:
		 * 
		 * <XTMAILING>
				<CAMPAIGN_ID>55592405</CAMPAIGN_ID>
				<TRANSACTION_ID>55592405</TRANSACTION_ID>
				<SHOW_ALL_SEND_DETAIL>true</SHOW_ALL_SEND_DETAIL>
				<SAVE_COLUMNS>
					<COLUMN_NAME>Title</COLUMN_NAME>
					<COLUMN_NAME>Lastname</COLUMN_NAME>
					<COLUMN_NAME>JP_Number</COLUMN_NAME>
				</SAVE_COLUMNS>
				<RECIPIENT>
					 <EMAIL>sura.pradeepkumar@customercentria.com</EMAIL>
					 <BODY_TYPE>HTML</BODY_TYPE>
					 <PERSONALIZATION>
						<TAG_NAME>Title</TAG_NAME>
						<VALUE>PradeepTestTitle</VALUE>
					 </PERSONALIZATION>
					 <PERSONALIZATION>
						<TAG_NAME>Lastname</TAG_NAME>
						<VALUE>PradeepTestLastName</VALUE>
					 </PERSONALIZATION>	
					 <PERSONALIZATION>
						<TAG_NAME>JP_Number</TAG_NAME>
						<VALUE>12345</VALUE>
					 </PERSONALIZATION>
					 <ATTACHMENT> 
						 <NAME>sample.csv</NAME>
						 <MIME_TYPE>text/plain</MIME_TYPE>
						 <DATA>WW91ciB0ZXh0IGF0dGFjaG1lbnQ=</DATA>
					 </ATTACHMENT>
				</RECIPIENT>
			</XTMAILING>
		 * 
		 */
		TransactRequestType transReqType=new TransactRequestType();
		StringBuilder sb = new StringBuilder();
		String strResult="<XTMAILING><CAMPAIGN_ID>"+transReqType.getCampaign_id()+"</CAMPAIGN_ID>"
				+ "<TRANSACTION_ID>"+transReqType.getTransaction_id()+"</TRANSACTION_ID>"
				+"<SHOW_ALL_SEND_DETAIL>"+transReqType.getShow_all_send_details()+"</SHOW_ALL_SEND_DETAIL>"
				+"<SAVE_COLUMNS>";
		sb.append(strResult);
		Set<String> column_set=( transReqType.getColumns().keySet());
		for (String entry : column_set) {
			String columnName = "\"" + entry + "\"";
			sb.append("<COLUMN_NAME>" + entry + "</COLUMN_NAME>");
		}
		
		sb.append("</SAVE_COLUMNS><RECIPIENT><EMAIL>"+transReqType.getEmail()+"</EMAIL><BODY_TYPE>"
				+transReqType.getBody_type()+"</BODY_TYPE>");
		 HashMap<String,String> column_tag_value=transReqType.getColumns();
		for(Map.Entry<String, String> entry : column_tag_value.entrySet()){
			sb.append("<PERSONALIZATION><TAG_NAME>"+entry.getKey()+"</TAG_NAME><VALUE>"+entry.getValue()+"</VALUE></PERSONALIZATION>");
		}
		sb.append("</RECIPIENT></XTMAILING>");
		String request= sb.toString();	
		
				
		return request;
	    
	}
}