package com.cc;


import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class GenerateToken {
	
	final static Logger logger = Logger.getLogger(GenerateToken.class);
	
	public static final String PARAM_CLIENT_ID = "client_id";
	public static final String PARAM_CLIENT_SECRET = "client_secret";
	public static final String PARAM_REFRESH_TOKEN = "refresh_token";
	public static final String PARAM_GRANT_TYPE = "grant_type";
	public static final String GRANT_TYPE = "refresh_token";
	private String url;
	private HttpClient httpClient;
	private String responseText;
	
	public GenerateToken(String url) {
		this(url, new HttpClient());
	}
	
	GenerateToken(String url, HttpClient httpClient) {
		this.url = url;
		this.httpClient = httpClient;
	}
	
	public String retrieveToken(String clientId, String clientSecret, String refereshToken) {
		PostMethod post = createPost(clientId, clientSecret, refereshToken);
		try {
		httpClient.executeMethod(post);
		responseText = getResponseText(post);
		return getTokenFromResponse();
		} catch (Exception e) {
		throw new RuntimeException(e);
		}
	}
	
	private PostMethod createPost(String clientId, String clientSecret, String refereshToken) {
		PostMethod post = new PostMethod(url);
		post.setParameter(PARAM_CLIENT_ID, clientId);
		post.setParameter(PARAM_CLIENT_SECRET, clientSecret);
		post.setParameter(PARAM_REFRESH_TOKEN, refereshToken);
		post.setParameter(PARAM_GRANT_TYPE, GRANT_TYPE);
		return post;
	}
	
	private String getResponseText(PostMethod post) throws IOException {
		InputStream is = post.getResponseBodyAsStream();
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(is).useDelimiter("$@#$@#");
		if(scanner.hasNext()){
			return (scanner.next());
		}
		else{
			return "";
		}
	}
	
	private String getTokenFromResponse() throws IOException {
		System.out.println(responseText);
		JSONObject json = (JSONObject) JSONSerializer.toJSON(responseText);
		return json.getString("access_token");
	}
	
	public static String getAccessToken(String url, String clientId, String clientSecret, String refreshToken){
		Properties prop = new Properties();
		InputStream input = null;
		
		
				
//		String url = prop.getProperty("authUrl");
//		String clientId = prop.getProperty("clientId");
//		String clientSecret = prop.getProperty("clientSecret");
//		String refreshToken = prop.getProperty("refreshToken");
                
//                String url ="http://api4.ibmmarketingcloud.com/oauth/token";
//		String clientId = "e19e5d25-c286-4f62-b3ab-0a8ed4c4f15d";
//		String clientSecret = "638e3589-037d-41e9-ae5a-eba7fec585f3";
//		String refreshToken = "rnpSe0HN4cj048V9APr8Zr6ArN6vCTQwuJja7KporW7MS1";
                
		
		GenerateToken tokenRetriever = new GenerateToken(url);
		String accessToken = tokenRetriever.retrieveToken(clientId, clientSecret, refreshToken);

		return accessToken;
		
	}

	
	}