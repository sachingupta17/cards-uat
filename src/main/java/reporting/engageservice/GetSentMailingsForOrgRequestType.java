/**
 * GetSentMailingsForOrgRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package reporting.engageservice;


/**
 *  GetSentMailingsForOrgRequestType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class GetSentMailingsForOrgRequestType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = GetSentMailingsForOrgRequestType
       Namespace URI = SilverpopApi:EngageService.Reporting
       Namespace Prefix = ns6
     */

    /**
     * field for DATE_START
     */
    protected reporting.engageservice.DateTime3 localDATE_START;

    /**
     * field for DATE_END
     */
    protected reporting.engageservice.DateTime3 localDATE_END;

    /**
     * field for PRIVATE
     */
    protected java.lang.String localPRIVATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPRIVATETracker = false;

    /**
     * field for SHARED
     */
    protected java.lang.String localSHARED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSHAREDTracker = false;

    /**
     * field for SCHEDULED
     */
    protected java.lang.String localSCHEDULED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSCHEDULEDTracker = false;

    /**
     * field for SENT
     */
    protected java.lang.String localSENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSENTTracker = false;

    /**
     * field for SENDING
     */
    protected java.lang.String localSENDING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSENDINGTracker = false;

    /**
     * field for OPTIN_CONFIRMATION
     */
    protected java.lang.String localOPTIN_CONFIRMATION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOPTIN_CONFIRMATIONTracker = false;

    /**
     * field for PROFILE_CONFIRMATION
     */
    protected java.lang.String localPROFILE_CONFIRMATION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPROFILE_CONFIRMATIONTracker = false;

    /**
     * field for AUTOMATED
     */
    protected java.lang.String localAUTOMATED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAUTOMATEDTracker = false;

    /**
     * field for CAMPAIGN_ACTIVE
     */
    protected java.lang.String localCAMPAIGN_ACTIVE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAMPAIGN_ACTIVETracker = false;

    /**
     * field for CAMPAIGN_COMPLETED
     */
    protected java.lang.String localCAMPAIGN_COMPLETED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAMPAIGN_COMPLETEDTracker = false;

    /**
     * field for CAMPAIGN_CANCELLED
     */
    protected java.lang.String localCAMPAIGN_CANCELLED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAMPAIGN_CANCELLEDTracker = false;

    /**
     * field for CAMPAIGN_SCRAPE_TEMPLATE
     */
    protected java.lang.String localCAMPAIGN_SCRAPE_TEMPLATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAMPAIGN_SCRAPE_TEMPLATETracker = false;

    /**
     * field for INCLUDE_TAGS
     */
    protected java.lang.String localINCLUDE_TAGS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_TAGSTracker = false;

    /**
     * field for EXCLUDE_ZERO_SENT
     */
    protected java.lang.String localEXCLUDE_ZERO_SENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXCLUDE_ZERO_SENTTracker = false;

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime3
     */
    public reporting.engageservice.DateTime3 getDATE_START() {
        return localDATE_START;
    }

    /**
     * Auto generated setter method
     * @param param DATE_START
     */
    public void setDATE_START(reporting.engageservice.DateTime3 param) {
        this.localDATE_START = param;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime3
     */
    public reporting.engageservice.DateTime3 getDATE_END() {
        return localDATE_END;
    }

    /**
     * Auto generated setter method
     * @param param DATE_END
     */
    public void setDATE_END(reporting.engageservice.DateTime3 param) {
        this.localDATE_END = param;
    }

    public boolean isPRIVATESpecified() {
        return localPRIVATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPRIVATE() {
        return localPRIVATE;
    }

    /**
     * Auto generated setter method
     * @param param PRIVATE
     */
    public void setPRIVATE(java.lang.String param) {
        localPRIVATETracker = param != null;

        this.localPRIVATE = param;
    }

    public boolean isSHAREDSpecified() {
        return localSHAREDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSHARED() {
        return localSHARED;
    }

    /**
     * Auto generated setter method
     * @param param SHARED
     */
    public void setSHARED(java.lang.String param) {
        localSHAREDTracker = param != null;

        this.localSHARED = param;
    }

    public boolean isSCHEDULEDSpecified() {
        return localSCHEDULEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCHEDULED() {
        return localSCHEDULED;
    }

    /**
     * Auto generated setter method
     * @param param SCHEDULED
     */
    public void setSCHEDULED(java.lang.String param) {
        localSCHEDULEDTracker = param != null;

        this.localSCHEDULED = param;
    }

    public boolean isSENTSpecified() {
        return localSENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSENT() {
        return localSENT;
    }

    /**
     * Auto generated setter method
     * @param param SENT
     */
    public void setSENT(java.lang.String param) {
        localSENTTracker = param != null;

        this.localSENT = param;
    }

    public boolean isSENDINGSpecified() {
        return localSENDINGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSENDING() {
        return localSENDING;
    }

    /**
     * Auto generated setter method
     * @param param SENDING
     */
    public void setSENDING(java.lang.String param) {
        localSENDINGTracker = param != null;

        this.localSENDING = param;
    }

    public boolean isOPTIN_CONFIRMATIONSpecified() {
        return localOPTIN_CONFIRMATIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOPTIN_CONFIRMATION() {
        return localOPTIN_CONFIRMATION;
    }

    /**
     * Auto generated setter method
     * @param param OPTIN_CONFIRMATION
     */
    public void setOPTIN_CONFIRMATION(java.lang.String param) {
        localOPTIN_CONFIRMATIONTracker = param != null;

        this.localOPTIN_CONFIRMATION = param;
    }

    public boolean isPROFILE_CONFIRMATIONSpecified() {
        return localPROFILE_CONFIRMATIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPROFILE_CONFIRMATION() {
        return localPROFILE_CONFIRMATION;
    }

    /**
     * Auto generated setter method
     * @param param PROFILE_CONFIRMATION
     */
    public void setPROFILE_CONFIRMATION(java.lang.String param) {
        localPROFILE_CONFIRMATIONTracker = param != null;

        this.localPROFILE_CONFIRMATION = param;
    }

    public boolean isAUTOMATEDSpecified() {
        return localAUTOMATEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAUTOMATED() {
        return localAUTOMATED;
    }

    /**
     * Auto generated setter method
     * @param param AUTOMATED
     */
    public void setAUTOMATED(java.lang.String param) {
        localAUTOMATEDTracker = param != null;

        this.localAUTOMATED = param;
    }

    public boolean isCAMPAIGN_ACTIVESpecified() {
        return localCAMPAIGN_ACTIVETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAMPAIGN_ACTIVE() {
        return localCAMPAIGN_ACTIVE;
    }

    /**
     * Auto generated setter method
     * @param param CAMPAIGN_ACTIVE
     */
    public void setCAMPAIGN_ACTIVE(java.lang.String param) {
        localCAMPAIGN_ACTIVETracker = param != null;

        this.localCAMPAIGN_ACTIVE = param;
    }

    public boolean isCAMPAIGN_COMPLETEDSpecified() {
        return localCAMPAIGN_COMPLETEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAMPAIGN_COMPLETED() {
        return localCAMPAIGN_COMPLETED;
    }

    /**
     * Auto generated setter method
     * @param param CAMPAIGN_COMPLETED
     */
    public void setCAMPAIGN_COMPLETED(java.lang.String param) {
        localCAMPAIGN_COMPLETEDTracker = param != null;

        this.localCAMPAIGN_COMPLETED = param;
    }

    public boolean isCAMPAIGN_CANCELLEDSpecified() {
        return localCAMPAIGN_CANCELLEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAMPAIGN_CANCELLED() {
        return localCAMPAIGN_CANCELLED;
    }

    /**
     * Auto generated setter method
     * @param param CAMPAIGN_CANCELLED
     */
    public void setCAMPAIGN_CANCELLED(java.lang.String param) {
        localCAMPAIGN_CANCELLEDTracker = param != null;

        this.localCAMPAIGN_CANCELLED = param;
    }

    public boolean isCAMPAIGN_SCRAPE_TEMPLATESpecified() {
        return localCAMPAIGN_SCRAPE_TEMPLATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAMPAIGN_SCRAPE_TEMPLATE() {
        return localCAMPAIGN_SCRAPE_TEMPLATE;
    }

    /**
     * Auto generated setter method
     * @param param CAMPAIGN_SCRAPE_TEMPLATE
     */
    public void setCAMPAIGN_SCRAPE_TEMPLATE(java.lang.String param) {
        localCAMPAIGN_SCRAPE_TEMPLATETracker = param != null;

        this.localCAMPAIGN_SCRAPE_TEMPLATE = param;
    }

    public boolean isINCLUDE_TAGSSpecified() {
        return localINCLUDE_TAGSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_TAGS() {
        return localINCLUDE_TAGS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_TAGS
     */
    public void setINCLUDE_TAGS(java.lang.String param) {
        localINCLUDE_TAGSTracker = param != null;

        this.localINCLUDE_TAGS = param;
    }

    public boolean isEXCLUDE_ZERO_SENTSpecified() {
        return localEXCLUDE_ZERO_SENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXCLUDE_ZERO_SENT() {
        return localEXCLUDE_ZERO_SENT;
    }

    /**
     * Auto generated setter method
     * @param param EXCLUDE_ZERO_SENT
     */
    public void setEXCLUDE_ZERO_SENT(java.lang.String param) {
        localEXCLUDE_ZERO_SENTTracker = param != null;

        this.localEXCLUDE_ZERO_SENT = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.Reporting");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":GetSentMailingsForOrgRequestType",
                    xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "GetSentMailingsForOrgRequestType", xmlWriter);
            }
        }

        if (localDATE_START == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "DATE_START cannot be null!!");
        }

        localDATE_START.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.Reporting", "DATE_START"), xmlWriter);

        if (localDATE_END == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "DATE_END cannot be null!!");
        }

        localDATE_END.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.Reporting", "DATE_END"), xmlWriter);

        if (localPRIVATETracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "PRIVATE", xmlWriter);

            if (localPRIVATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PRIVATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPRIVATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSHAREDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SHARED", xmlWriter);

            if (localSHARED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SHARED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSHARED);
            }

            xmlWriter.writeEndElement();
        }

        if (localSCHEDULEDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SCHEDULED", xmlWriter);

            if (localSCHEDULED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SCHEDULED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSCHEDULED);
            }

            xmlWriter.writeEndElement();
        }

        if (localSENTTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SENT", xmlWriter);

            if (localSENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSENDINGTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SENDING", xmlWriter);

            if (localSENDING == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SENDING cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSENDING);
            }

            xmlWriter.writeEndElement();
        }

        if (localOPTIN_CONFIRMATIONTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "OPTIN_CONFIRMATION", xmlWriter);

            if (localOPTIN_CONFIRMATION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OPTIN_CONFIRMATION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOPTIN_CONFIRMATION);
            }

            xmlWriter.writeEndElement();
        }

        if (localPROFILE_CONFIRMATIONTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "PROFILE_CONFIRMATION", xmlWriter);

            if (localPROFILE_CONFIRMATION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PROFILE_CONFIRMATION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPROFILE_CONFIRMATION);
            }

            xmlWriter.writeEndElement();
        }

        if (localAUTOMATEDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "AUTOMATED", xmlWriter);

            if (localAUTOMATED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AUTOMATED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAUTOMATED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAMPAIGN_ACTIVETracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CAMPAIGN_ACTIVE", xmlWriter);

            if (localCAMPAIGN_ACTIVE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAMPAIGN_ACTIVE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAMPAIGN_ACTIVE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAMPAIGN_COMPLETEDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CAMPAIGN_COMPLETED", xmlWriter);

            if (localCAMPAIGN_COMPLETED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAMPAIGN_COMPLETED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAMPAIGN_COMPLETED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAMPAIGN_CANCELLEDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CAMPAIGN_CANCELLED", xmlWriter);

            if (localCAMPAIGN_CANCELLED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAMPAIGN_CANCELLED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAMPAIGN_CANCELLED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAMPAIGN_SCRAPE_TEMPLATETracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CAMPAIGN_SCRAPE_TEMPLATE",
                xmlWriter);

            if (localCAMPAIGN_SCRAPE_TEMPLATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAMPAIGN_SCRAPE_TEMPLATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAMPAIGN_SCRAPE_TEMPLATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_TAGSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_TAGS", xmlWriter);

            if (localINCLUDE_TAGS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_TAGS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_TAGS);
            }

            xmlWriter.writeEndElement();
        }

        if (localEXCLUDE_ZERO_SENTTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "EXCLUDE_ZERO_SENT", xmlWriter);

            if (localEXCLUDE_ZERO_SENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXCLUDE_ZERO_SENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXCLUDE_ZERO_SENT);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.Reporting")) {
            return "ns6";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static GetSentMailingsForOrgRequestType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            GetSentMailingsForOrgRequestType object = new GetSentMailingsForOrgRequestType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"GetSentMailingsForOrgRequestType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (GetSentMailingsForOrgRequestType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "DATE_START").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_START").equals(
                            reader.getName())) {
                    object.setDATE_START(reporting.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "DATE_END").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_END").equals(
                            reader.getName())) {
                    object.setDATE_END(reporting.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "PRIVATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PRIVATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PRIVATE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPRIVATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SHARED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SHARED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SHARED" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSHARED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SCHEDULED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCHEDULED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCHEDULED" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCHEDULED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SENT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SENT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SENT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SENDING").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SENDING").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SENDING" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSENDING(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "OPTIN_CONFIRMATION").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "OPTIN_CONFIRMATION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OPTIN_CONFIRMATION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOPTIN_CONFIRMATION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "PROFILE_CONFIRMATION").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "PROFILE_CONFIRMATION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PROFILE_CONFIRMATION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPROFILE_CONFIRMATION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "AUTOMATED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AUTOMATED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AUTOMATED" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAUTOMATED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CAMPAIGN_ACTIVE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAMPAIGN_ACTIVE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAMPAIGN_ACTIVE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAMPAIGN_ACTIVE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CAMPAIGN_COMPLETED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAMPAIGN_COMPLETED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAMPAIGN_COMPLETED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAMPAIGN_COMPLETED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CAMPAIGN_CANCELLED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAMPAIGN_CANCELLED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAMPAIGN_CANCELLED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAMPAIGN_CANCELLED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CAMPAIGN_SCRAPE_TEMPLATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAMPAIGN_SCRAPE_TEMPLATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAMPAIGN_SCRAPE_TEMPLATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAMPAIGN_SCRAPE_TEMPLATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_TAGS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCLUDE_TAGS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_TAGS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_TAGS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "EXCLUDE_ZERO_SENT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXCLUDE_ZERO_SENT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXCLUDE_ZERO_SENT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXCLUDE_ZERO_SENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
