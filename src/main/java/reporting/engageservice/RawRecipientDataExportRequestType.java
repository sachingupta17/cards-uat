/**
 * RawRecipientDataExportRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package reporting.engageservice;


/**
 *  RawRecipientDataExportRequestType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class RawRecipientDataExportRequestType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = RawRecipientDataExportRequestType
       Namespace URI = SilverpopApi:EngageService.Reporting
       Namespace Prefix = ns6
     */

    /**
     * field for MAILING_ID
     */
    protected long localMAILING_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMAILING_IDTracker = false;

    /**
     * field for REPORT_ID
     */
    protected long localREPORT_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORT_IDTracker = false;

    /**
     * field for MAILING
     * This was an Array!
     */
    protected reporting.engageservice.MultiMailingsElementType[] localMAILING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMAILINGTracker = false;

    /**
     * field for CAMPAIGN_ID
     */
    protected long localCAMPAIGN_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAMPAIGN_IDTracker = false;

    /**
     * field for LIST_ID
     */
    protected long localLIST_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLIST_IDTracker = false;

    /**
     * field for INCLUDE_QUERIES
     */
    protected java.lang.String localINCLUDE_QUERIES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_QUERIESTracker = false;

    /**
     * field for ALL_NON_EXPORTED
     */
    protected java.lang.String localALL_NON_EXPORTED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALL_NON_EXPORTEDTracker = false;

    /**
     * field for EVENT_DATE_START
     */
    protected reporting.engageservice.DateTime3 localEVENT_DATE_START;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEVENT_DATE_STARTTracker = false;

    /**
     * field for EVENT_DATE_END
     */
    protected reporting.engageservice.DateTime3 localEVENT_DATE_END;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEVENT_DATE_ENDTracker = false;

    /**
     * field for SEND_DATE_START
     */
    protected reporting.engageservice.DateTime3 localSEND_DATE_START;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSEND_DATE_STARTTracker = false;

    /**
     * field for SEND_DATE_END
     */
    protected reporting.engageservice.DateTime3 localSEND_DATE_END;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSEND_DATE_ENDTracker = false;

    /**
     * field for EXPORT_FORMAT
     */
    protected reporting.engageservice.EventExportFormat localEXPORT_FORMAT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXPORT_FORMATTracker = false;

    /**
     * field for EXPORT_FILE_NAME
     */
    protected java.lang.String localEXPORT_FILE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXPORT_FILE_NAMETracker = false;

    /**
     * field for FILE_ENCODING
     */
    protected reporting.engageservice.FileEncoding localFILE_ENCODING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFILE_ENCODINGTracker = false;

    /**
     * field for EMAIL
     */
    protected java.lang.String localEMAIL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILTracker = false;

    /**
     * field for MOVE_TO_FTP
     */
    protected java.lang.String localMOVE_TO_FTP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMOVE_TO_FTPTracker = false;

    /**
     * field for PRIVATE
     */
    protected java.lang.String localPRIVATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPRIVATETracker = false;

    /**
     * field for SHARED
     */
    protected java.lang.String localSHARED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSHAREDTracker = false;

    /**
     * field for SENT_MAILINGS
     */
    protected java.lang.String localSENT_MAILINGS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSENT_MAILINGSTracker = false;

    /**
     * field for SENDING
     */
    protected java.lang.String localSENDING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSENDINGTracker = false;

    /**
     * field for OPTIN_CONFIRMATION
     */
    protected java.lang.String localOPTIN_CONFIRMATION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOPTIN_CONFIRMATIONTracker = false;

    /**
     * field for PROFILE_CONFIRMATION
     */
    protected java.lang.String localPROFILE_CONFIRMATION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPROFILE_CONFIRMATIONTracker = false;

    /**
     * field for AUTOMATED
     */
    protected java.lang.String localAUTOMATED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAUTOMATEDTracker = false;

    /**
     * field for CAMPAIGN_ACTIVE
     */
    protected java.lang.String localCAMPAIGN_ACTIVE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAMPAIGN_ACTIVETracker = false;

    /**
     * field for CAMPAIGN_COMPLETED
     */
    protected java.lang.String localCAMPAIGN_COMPLETED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAMPAIGN_COMPLETEDTracker = false;

    /**
     * field for CAMPAIGN_CANCELLED
     */
    protected java.lang.String localCAMPAIGN_CANCELLED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAMPAIGN_CANCELLEDTracker = false;

    /**
     * field for CAMPAIGN_SCRAPE_TEMPLATE
     */
    protected java.lang.String localCAMPAIGN_SCRAPE_TEMPLATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCAMPAIGN_SCRAPE_TEMPLATETracker = false;

    /**
     * field for INCLUDE_TEST_MAILINGS
     */
    protected java.lang.String localINCLUDE_TEST_MAILINGS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_TEST_MAILINGSTracker = false;

    /**
     * field for ALL_EVENT_TYPES
     */
    protected java.lang.String localALL_EVENT_TYPES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALL_EVENT_TYPESTracker = false;

    /**
     * field for SENT
     */
    protected java.lang.String localSENT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSENTTracker = false;

    /**
     * field for SUPPRESSED
     */
    protected java.lang.String localSUPPRESSED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUPPRESSEDTracker = false;

    /**
     * field for OPENS
     */
    protected java.lang.String localOPENS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOPENSTracker = false;

    /**
     * field for CLICKS
     */
    protected java.lang.String localCLICKS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCLICKSTracker = false;

    /**
     * field for OPTINS
     */
    protected java.lang.String localOPTINS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOPTINSTracker = false;

    /**
     * field for OPTOUTS
     */
    protected java.lang.String localOPTOUTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOPTOUTSTracker = false;

    /**
     * field for FORWARDS
     */
    protected java.lang.String localFORWARDS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFORWARDSTracker = false;

    /**
     * field for ATTACHMENTS
     */
    protected java.lang.String localATTACHMENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localATTACHMENTSTracker = false;

    /**
     * field for CONVERSIONS
     */
    protected java.lang.String localCONVERSIONS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCONVERSIONSTracker = false;

    /**
     * field for CLICKSTREAMS
     */
    protected java.lang.String localCLICKSTREAMS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCLICKSTREAMSTracker = false;

    /**
     * field for HARD_BOUNCES
     */
    protected java.lang.String localHARD_BOUNCES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHARD_BOUNCESTracker = false;

    /**
     * field for SOFT_BOUNCES
     */
    protected java.lang.String localSOFT_BOUNCES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSOFT_BOUNCESTracker = false;

    /**
     * field for REPLY_ABUSE
     */
    protected java.lang.String localREPLY_ABUSE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPLY_ABUSETracker = false;

    /**
     * field for REPLY_COA
     */
    protected java.lang.String localREPLY_COA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPLY_COATracker = false;

    /**
     * field for REPLY_OTHER
     */
    protected java.lang.String localREPLY_OTHER;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPLY_OTHERTracker = false;

    /**
     * field for MAIL_BLOCKS
     */
    protected java.lang.String localMAIL_BLOCKS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMAIL_BLOCKSTracker = false;

    /**
     * field for MAILING_RESTRICTIONS
     */
    protected java.lang.String localMAILING_RESTRICTIONS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMAILING_RESTRICTIONSTracker = false;

    /**
     * field for SMS_ERROR
     */
    protected java.lang.String localSMS_ERROR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSMS_ERRORTracker = false;

    /**
     * field for SMS_REJECT
     */
    protected java.lang.String localSMS_REJECT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSMS_REJECTTracker = false;

    /**
     * field for SMS_OPTOUT
     */
    protected java.lang.String localSMS_OPTOUT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSMS_OPTOUTTracker = false;

    /**
     * field for INCLUDE_SEEDS
     */
    protected java.lang.String localINCLUDE_SEEDS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_SEEDSTracker = false;

    /**
     * field for INCLUDE_FORWARDS
     */
    protected java.lang.String localINCLUDE_FORWARDS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_FORWARDSTracker = false;

    /**
     * field for INCLUDE_INBOX_MONITORING
     */
    protected java.lang.String localINCLUDE_INBOX_MONITORING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_INBOX_MONITORINGTracker = false;

    /**
     * field for CODED_TYPE_FIELDS
     */
    protected java.lang.String localCODED_TYPE_FIELDS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCODED_TYPE_FIELDSTracker = false;

    /**
     * field for EXCLUDE_DELETED
     */
    protected java.lang.String localEXCLUDE_DELETED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXCLUDE_DELETEDTracker = false;

    /**
     * field for COLUMNS
     */
    protected reporting.engageservice.ColumnsElementType localCOLUMNS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOLUMNSTracker = false;

    public boolean isMAILING_IDSpecified() {
        return localMAILING_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getMAILING_ID() {
        return localMAILING_ID;
    }

    /**
     * Auto generated setter method
     * @param param MAILING_ID
     */
    public void setMAILING_ID(long param) {
        // setting primitive attribute tracker to true
        localMAILING_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localMAILING_ID = param;
    }

    public boolean isREPORT_IDSpecified() {
        return localREPORT_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getREPORT_ID() {
        return localREPORT_ID;
    }

    /**
     * Auto generated setter method
     * @param param REPORT_ID
     */
    public void setREPORT_ID(long param) {
        // setting primitive attribute tracker to true
        localREPORT_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localREPORT_ID = param;
    }

    public boolean isMAILINGSpecified() {
        return localMAILINGTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.MultiMailingsElementType[]
     */
    public reporting.engageservice.MultiMailingsElementType[] getMAILING() {
        return localMAILING;
    }

    /**
     * validate the array for MAILING
     */
    protected void validateMAILING(
        reporting.engageservice.MultiMailingsElementType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param MAILING
     */
    public void setMAILING(
        reporting.engageservice.MultiMailingsElementType[] param) {
        validateMAILING(param);

        localMAILINGTracker = param != null;

        this.localMAILING = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param reporting.engageservice.MultiMailingsElementType
     */
    public void addMAILING(
        reporting.engageservice.MultiMailingsElementType param) {
        if (localMAILING == null) {
            localMAILING = new reporting.engageservice.MultiMailingsElementType[] {
                    
                };
        }

        //update the setting tracker
        localMAILINGTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localMAILING);
        list.add(param);
        this.localMAILING = (reporting.engageservice.MultiMailingsElementType[]) list.toArray(new reporting.engageservice.MultiMailingsElementType[list.size()]);
    }

    public boolean isCAMPAIGN_IDSpecified() {
        return localCAMPAIGN_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getCAMPAIGN_ID() {
        return localCAMPAIGN_ID;
    }

    /**
     * Auto generated setter method
     * @param param CAMPAIGN_ID
     */
    public void setCAMPAIGN_ID(long param) {
        // setting primitive attribute tracker to true
        localCAMPAIGN_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localCAMPAIGN_ID = param;
    }

    public boolean isLIST_IDSpecified() {
        return localLIST_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getLIST_ID() {
        return localLIST_ID;
    }

    /**
     * Auto generated setter method
     * @param param LIST_ID
     */
    public void setLIST_ID(long param) {
        // setting primitive attribute tracker to true
        localLIST_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localLIST_ID = param;
    }

    public boolean isINCLUDE_QUERIESSpecified() {
        return localINCLUDE_QUERIESTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_QUERIES() {
        return localINCLUDE_QUERIES;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_QUERIES
     */
    public void setINCLUDE_QUERIES(java.lang.String param) {
        localINCLUDE_QUERIESTracker = param != null;

        this.localINCLUDE_QUERIES = param;
    }

    public boolean isALL_NON_EXPORTEDSpecified() {
        return localALL_NON_EXPORTEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALL_NON_EXPORTED() {
        return localALL_NON_EXPORTED;
    }

    /**
     * Auto generated setter method
     * @param param ALL_NON_EXPORTED
     */
    public void setALL_NON_EXPORTED(java.lang.String param) {
        localALL_NON_EXPORTEDTracker = param != null;

        this.localALL_NON_EXPORTED = param;
    }

    public boolean isEVENT_DATE_STARTSpecified() {
        return localEVENT_DATE_STARTTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime3
     */
    public reporting.engageservice.DateTime3 getEVENT_DATE_START() {
        return localEVENT_DATE_START;
    }

    /**
     * Auto generated setter method
     * @param param EVENT_DATE_START
     */
    public void setEVENT_DATE_START(reporting.engageservice.DateTime3 param) {
        localEVENT_DATE_STARTTracker = param != null;

        this.localEVENT_DATE_START = param;
    }

    public boolean isEVENT_DATE_ENDSpecified() {
        return localEVENT_DATE_ENDTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime3
     */
    public reporting.engageservice.DateTime3 getEVENT_DATE_END() {
        return localEVENT_DATE_END;
    }

    /**
     * Auto generated setter method
     * @param param EVENT_DATE_END
     */
    public void setEVENT_DATE_END(reporting.engageservice.DateTime3 param) {
        localEVENT_DATE_ENDTracker = param != null;

        this.localEVENT_DATE_END = param;
    }

    public boolean isSEND_DATE_STARTSpecified() {
        return localSEND_DATE_STARTTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime3
     */
    public reporting.engageservice.DateTime3 getSEND_DATE_START() {
        return localSEND_DATE_START;
    }

    /**
     * Auto generated setter method
     * @param param SEND_DATE_START
     */
    public void setSEND_DATE_START(reporting.engageservice.DateTime3 param) {
        localSEND_DATE_STARTTracker = param != null;

        this.localSEND_DATE_START = param;
    }

    public boolean isSEND_DATE_ENDSpecified() {
        return localSEND_DATE_ENDTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime3
     */
    public reporting.engageservice.DateTime3 getSEND_DATE_END() {
        return localSEND_DATE_END;
    }

    /**
     * Auto generated setter method
     * @param param SEND_DATE_END
     */
    public void setSEND_DATE_END(reporting.engageservice.DateTime3 param) {
        localSEND_DATE_ENDTracker = param != null;

        this.localSEND_DATE_END = param;
    }

    public boolean isEXPORT_FORMATSpecified() {
        return localEXPORT_FORMATTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.EventExportFormat
     */
    public reporting.engageservice.EventExportFormat getEXPORT_FORMAT() {
        return localEXPORT_FORMAT;
    }

    /**
     * Auto generated setter method
     * @param param EXPORT_FORMAT
     */
    public void setEXPORT_FORMAT(
        reporting.engageservice.EventExportFormat param) {
        localEXPORT_FORMATTracker = param != null;

        this.localEXPORT_FORMAT = param;
    }

    public boolean isEXPORT_FILE_NAMESpecified() {
        return localEXPORT_FILE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXPORT_FILE_NAME() {
        return localEXPORT_FILE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param EXPORT_FILE_NAME
     */
    public void setEXPORT_FILE_NAME(java.lang.String param) {
        localEXPORT_FILE_NAMETracker = param != null;

        this.localEXPORT_FILE_NAME = param;
    }

    public boolean isFILE_ENCODINGSpecified() {
        return localFILE_ENCODINGTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.FileEncoding
     */
    public reporting.engageservice.FileEncoding getFILE_ENCODING() {
        return localFILE_ENCODING;
    }

    /**
     * Auto generated setter method
     * @param param FILE_ENCODING
     */
    public void setFILE_ENCODING(reporting.engageservice.FileEncoding param) {
        localFILE_ENCODINGTracker = param != null;

        this.localFILE_ENCODING = param;
    }

    public boolean isEMAILSpecified() {
        return localEMAILTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL() {
        return localEMAIL;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL
     */
    public void setEMAIL(java.lang.String param) {
        localEMAILTracker = param != null;

        this.localEMAIL = param;
    }

    public boolean isMOVE_TO_FTPSpecified() {
        return localMOVE_TO_FTPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMOVE_TO_FTP() {
        return localMOVE_TO_FTP;
    }

    /**
     * Auto generated setter method
     * @param param MOVE_TO_FTP
     */
    public void setMOVE_TO_FTP(java.lang.String param) {
        localMOVE_TO_FTPTracker = param != null;

        this.localMOVE_TO_FTP = param;
    }

    public boolean isPRIVATESpecified() {
        return localPRIVATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPRIVATE() {
        return localPRIVATE;
    }

    /**
     * Auto generated setter method
     * @param param PRIVATE
     */
    public void setPRIVATE(java.lang.String param) {
        localPRIVATETracker = param != null;

        this.localPRIVATE = param;
    }

    public boolean isSHAREDSpecified() {
        return localSHAREDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSHARED() {
        return localSHARED;
    }

    /**
     * Auto generated setter method
     * @param param SHARED
     */
    public void setSHARED(java.lang.String param) {
        localSHAREDTracker = param != null;

        this.localSHARED = param;
    }

    public boolean isSENT_MAILINGSSpecified() {
        return localSENT_MAILINGSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSENT_MAILINGS() {
        return localSENT_MAILINGS;
    }

    /**
     * Auto generated setter method
     * @param param SENT_MAILINGS
     */
    public void setSENT_MAILINGS(java.lang.String param) {
        localSENT_MAILINGSTracker = param != null;

        this.localSENT_MAILINGS = param;
    }

    public boolean isSENDINGSpecified() {
        return localSENDINGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSENDING() {
        return localSENDING;
    }

    /**
     * Auto generated setter method
     * @param param SENDING
     */
    public void setSENDING(java.lang.String param) {
        localSENDINGTracker = param != null;

        this.localSENDING = param;
    }

    public boolean isOPTIN_CONFIRMATIONSpecified() {
        return localOPTIN_CONFIRMATIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOPTIN_CONFIRMATION() {
        return localOPTIN_CONFIRMATION;
    }

    /**
     * Auto generated setter method
     * @param param OPTIN_CONFIRMATION
     */
    public void setOPTIN_CONFIRMATION(java.lang.String param) {
        localOPTIN_CONFIRMATIONTracker = param != null;

        this.localOPTIN_CONFIRMATION = param;
    }

    public boolean isPROFILE_CONFIRMATIONSpecified() {
        return localPROFILE_CONFIRMATIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPROFILE_CONFIRMATION() {
        return localPROFILE_CONFIRMATION;
    }

    /**
     * Auto generated setter method
     * @param param PROFILE_CONFIRMATION
     */
    public void setPROFILE_CONFIRMATION(java.lang.String param) {
        localPROFILE_CONFIRMATIONTracker = param != null;

        this.localPROFILE_CONFIRMATION = param;
    }

    public boolean isAUTOMATEDSpecified() {
        return localAUTOMATEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAUTOMATED() {
        return localAUTOMATED;
    }

    /**
     * Auto generated setter method
     * @param param AUTOMATED
     */
    public void setAUTOMATED(java.lang.String param) {
        localAUTOMATEDTracker = param != null;

        this.localAUTOMATED = param;
    }

    public boolean isCAMPAIGN_ACTIVESpecified() {
        return localCAMPAIGN_ACTIVETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAMPAIGN_ACTIVE() {
        return localCAMPAIGN_ACTIVE;
    }

    /**
     * Auto generated setter method
     * @param param CAMPAIGN_ACTIVE
     */
    public void setCAMPAIGN_ACTIVE(java.lang.String param) {
        localCAMPAIGN_ACTIVETracker = param != null;

        this.localCAMPAIGN_ACTIVE = param;
    }

    public boolean isCAMPAIGN_COMPLETEDSpecified() {
        return localCAMPAIGN_COMPLETEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAMPAIGN_COMPLETED() {
        return localCAMPAIGN_COMPLETED;
    }

    /**
     * Auto generated setter method
     * @param param CAMPAIGN_COMPLETED
     */
    public void setCAMPAIGN_COMPLETED(java.lang.String param) {
        localCAMPAIGN_COMPLETEDTracker = param != null;

        this.localCAMPAIGN_COMPLETED = param;
    }

    public boolean isCAMPAIGN_CANCELLEDSpecified() {
        return localCAMPAIGN_CANCELLEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAMPAIGN_CANCELLED() {
        return localCAMPAIGN_CANCELLED;
    }

    /**
     * Auto generated setter method
     * @param param CAMPAIGN_CANCELLED
     */
    public void setCAMPAIGN_CANCELLED(java.lang.String param) {
        localCAMPAIGN_CANCELLEDTracker = param != null;

        this.localCAMPAIGN_CANCELLED = param;
    }

    public boolean isCAMPAIGN_SCRAPE_TEMPLATESpecified() {
        return localCAMPAIGN_SCRAPE_TEMPLATETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCAMPAIGN_SCRAPE_TEMPLATE() {
        return localCAMPAIGN_SCRAPE_TEMPLATE;
    }

    /**
     * Auto generated setter method
     * @param param CAMPAIGN_SCRAPE_TEMPLATE
     */
    public void setCAMPAIGN_SCRAPE_TEMPLATE(java.lang.String param) {
        localCAMPAIGN_SCRAPE_TEMPLATETracker = param != null;

        this.localCAMPAIGN_SCRAPE_TEMPLATE = param;
    }

    public boolean isINCLUDE_TEST_MAILINGSSpecified() {
        return localINCLUDE_TEST_MAILINGSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_TEST_MAILINGS() {
        return localINCLUDE_TEST_MAILINGS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_TEST_MAILINGS
     */
    public void setINCLUDE_TEST_MAILINGS(java.lang.String param) {
        localINCLUDE_TEST_MAILINGSTracker = param != null;

        this.localINCLUDE_TEST_MAILINGS = param;
    }

    public boolean isALL_EVENT_TYPESSpecified() {
        return localALL_EVENT_TYPESTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALL_EVENT_TYPES() {
        return localALL_EVENT_TYPES;
    }

    /**
     * Auto generated setter method
     * @param param ALL_EVENT_TYPES
     */
    public void setALL_EVENT_TYPES(java.lang.String param) {
        localALL_EVENT_TYPESTracker = param != null;

        this.localALL_EVENT_TYPES = param;
    }

    public boolean isSENTSpecified() {
        return localSENTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSENT() {
        return localSENT;
    }

    /**
     * Auto generated setter method
     * @param param SENT
     */
    public void setSENT(java.lang.String param) {
        localSENTTracker = param != null;

        this.localSENT = param;
    }

    public boolean isSUPPRESSEDSpecified() {
        return localSUPPRESSEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUPPRESSED() {
        return localSUPPRESSED;
    }

    /**
     * Auto generated setter method
     * @param param SUPPRESSED
     */
    public void setSUPPRESSED(java.lang.String param) {
        localSUPPRESSEDTracker = param != null;

        this.localSUPPRESSED = param;
    }

    public boolean isOPENSSpecified() {
        return localOPENSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOPENS() {
        return localOPENS;
    }

    /**
     * Auto generated setter method
     * @param param OPENS
     */
    public void setOPENS(java.lang.String param) {
        localOPENSTracker = param != null;

        this.localOPENS = param;
    }

    public boolean isCLICKSSpecified() {
        return localCLICKSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCLICKS() {
        return localCLICKS;
    }

    /**
     * Auto generated setter method
     * @param param CLICKS
     */
    public void setCLICKS(java.lang.String param) {
        localCLICKSTracker = param != null;

        this.localCLICKS = param;
    }

    public boolean isOPTINSSpecified() {
        return localOPTINSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOPTINS() {
        return localOPTINS;
    }

    /**
     * Auto generated setter method
     * @param param OPTINS
     */
    public void setOPTINS(java.lang.String param) {
        localOPTINSTracker = param != null;

        this.localOPTINS = param;
    }

    public boolean isOPTOUTSSpecified() {
        return localOPTOUTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getOPTOUTS() {
        return localOPTOUTS;
    }

    /**
     * Auto generated setter method
     * @param param OPTOUTS
     */
    public void setOPTOUTS(java.lang.String param) {
        localOPTOUTSTracker = param != null;

        this.localOPTOUTS = param;
    }

    public boolean isFORWARDSSpecified() {
        return localFORWARDSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFORWARDS() {
        return localFORWARDS;
    }

    /**
     * Auto generated setter method
     * @param param FORWARDS
     */
    public void setFORWARDS(java.lang.String param) {
        localFORWARDSTracker = param != null;

        this.localFORWARDS = param;
    }

    public boolean isATTACHMENTSSpecified() {
        return localATTACHMENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getATTACHMENTS() {
        return localATTACHMENTS;
    }

    /**
     * Auto generated setter method
     * @param param ATTACHMENTS
     */
    public void setATTACHMENTS(java.lang.String param) {
        localATTACHMENTSTracker = param != null;

        this.localATTACHMENTS = param;
    }

    public boolean isCONVERSIONSSpecified() {
        return localCONVERSIONSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCONVERSIONS() {
        return localCONVERSIONS;
    }

    /**
     * Auto generated setter method
     * @param param CONVERSIONS
     */
    public void setCONVERSIONS(java.lang.String param) {
        localCONVERSIONSTracker = param != null;

        this.localCONVERSIONS = param;
    }

    public boolean isCLICKSTREAMSSpecified() {
        return localCLICKSTREAMSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCLICKSTREAMS() {
        return localCLICKSTREAMS;
    }

    /**
     * Auto generated setter method
     * @param param CLICKSTREAMS
     */
    public void setCLICKSTREAMS(java.lang.String param) {
        localCLICKSTREAMSTracker = param != null;

        this.localCLICKSTREAMS = param;
    }

    public boolean isHARD_BOUNCESSpecified() {
        return localHARD_BOUNCESTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getHARD_BOUNCES() {
        return localHARD_BOUNCES;
    }

    /**
     * Auto generated setter method
     * @param param HARD_BOUNCES
     */
    public void setHARD_BOUNCES(java.lang.String param) {
        localHARD_BOUNCESTracker = param != null;

        this.localHARD_BOUNCES = param;
    }

    public boolean isSOFT_BOUNCESSpecified() {
        return localSOFT_BOUNCESTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSOFT_BOUNCES() {
        return localSOFT_BOUNCES;
    }

    /**
     * Auto generated setter method
     * @param param SOFT_BOUNCES
     */
    public void setSOFT_BOUNCES(java.lang.String param) {
        localSOFT_BOUNCESTracker = param != null;

        this.localSOFT_BOUNCES = param;
    }

    public boolean isREPLY_ABUSESpecified() {
        return localREPLY_ABUSETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPLY_ABUSE() {
        return localREPLY_ABUSE;
    }

    /**
     * Auto generated setter method
     * @param param REPLY_ABUSE
     */
    public void setREPLY_ABUSE(java.lang.String param) {
        localREPLY_ABUSETracker = param != null;

        this.localREPLY_ABUSE = param;
    }

    public boolean isREPLY_COASpecified() {
        return localREPLY_COATracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPLY_COA() {
        return localREPLY_COA;
    }

    /**
     * Auto generated setter method
     * @param param REPLY_COA
     */
    public void setREPLY_COA(java.lang.String param) {
        localREPLY_COATracker = param != null;

        this.localREPLY_COA = param;
    }

    public boolean isREPLY_OTHERSpecified() {
        return localREPLY_OTHERTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPLY_OTHER() {
        return localREPLY_OTHER;
    }

    /**
     * Auto generated setter method
     * @param param REPLY_OTHER
     */
    public void setREPLY_OTHER(java.lang.String param) {
        localREPLY_OTHERTracker = param != null;

        this.localREPLY_OTHER = param;
    }

    public boolean isMAIL_BLOCKSSpecified() {
        return localMAIL_BLOCKSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMAIL_BLOCKS() {
        return localMAIL_BLOCKS;
    }

    /**
     * Auto generated setter method
     * @param param MAIL_BLOCKS
     */
    public void setMAIL_BLOCKS(java.lang.String param) {
        localMAIL_BLOCKSTracker = param != null;

        this.localMAIL_BLOCKS = param;
    }

    public boolean isMAILING_RESTRICTIONSSpecified() {
        return localMAILING_RESTRICTIONSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMAILING_RESTRICTIONS() {
        return localMAILING_RESTRICTIONS;
    }

    /**
     * Auto generated setter method
     * @param param MAILING_RESTRICTIONS
     */
    public void setMAILING_RESTRICTIONS(java.lang.String param) {
        localMAILING_RESTRICTIONSTracker = param != null;

        this.localMAILING_RESTRICTIONS = param;
    }

    public boolean isSMS_ERRORSpecified() {
        return localSMS_ERRORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSMS_ERROR() {
        return localSMS_ERROR;
    }

    /**
     * Auto generated setter method
     * @param param SMS_ERROR
     */
    public void setSMS_ERROR(java.lang.String param) {
        localSMS_ERRORTracker = param != null;

        this.localSMS_ERROR = param;
    }

    public boolean isSMS_REJECTSpecified() {
        return localSMS_REJECTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSMS_REJECT() {
        return localSMS_REJECT;
    }

    /**
     * Auto generated setter method
     * @param param SMS_REJECT
     */
    public void setSMS_REJECT(java.lang.String param) {
        localSMS_REJECTTracker = param != null;

        this.localSMS_REJECT = param;
    }

    public boolean isSMS_OPTOUTSpecified() {
        return localSMS_OPTOUTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSMS_OPTOUT() {
        return localSMS_OPTOUT;
    }

    /**
     * Auto generated setter method
     * @param param SMS_OPTOUT
     */
    public void setSMS_OPTOUT(java.lang.String param) {
        localSMS_OPTOUTTracker = param != null;

        this.localSMS_OPTOUT = param;
    }

    public boolean isINCLUDE_SEEDSSpecified() {
        return localINCLUDE_SEEDSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_SEEDS() {
        return localINCLUDE_SEEDS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_SEEDS
     */
    public void setINCLUDE_SEEDS(java.lang.String param) {
        localINCLUDE_SEEDSTracker = param != null;

        this.localINCLUDE_SEEDS = param;
    }

    public boolean isINCLUDE_FORWARDSSpecified() {
        return localINCLUDE_FORWARDSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_FORWARDS() {
        return localINCLUDE_FORWARDS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_FORWARDS
     */
    public void setINCLUDE_FORWARDS(java.lang.String param) {
        localINCLUDE_FORWARDSTracker = param != null;

        this.localINCLUDE_FORWARDS = param;
    }

    public boolean isINCLUDE_INBOX_MONITORINGSpecified() {
        return localINCLUDE_INBOX_MONITORINGTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_INBOX_MONITORING() {
        return localINCLUDE_INBOX_MONITORING;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_INBOX_MONITORING
     */
    public void setINCLUDE_INBOX_MONITORING(java.lang.String param) {
        localINCLUDE_INBOX_MONITORINGTracker = param != null;

        this.localINCLUDE_INBOX_MONITORING = param;
    }

    public boolean isCODED_TYPE_FIELDSSpecified() {
        return localCODED_TYPE_FIELDSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCODED_TYPE_FIELDS() {
        return localCODED_TYPE_FIELDS;
    }

    /**
     * Auto generated setter method
     * @param param CODED_TYPE_FIELDS
     */
    public void setCODED_TYPE_FIELDS(java.lang.String param) {
        localCODED_TYPE_FIELDSTracker = param != null;

        this.localCODED_TYPE_FIELDS = param;
    }

    public boolean isEXCLUDE_DELETEDSpecified() {
        return localEXCLUDE_DELETEDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXCLUDE_DELETED() {
        return localEXCLUDE_DELETED;
    }

    /**
     * Auto generated setter method
     * @param param EXCLUDE_DELETED
     */
    public void setEXCLUDE_DELETED(java.lang.String param) {
        localEXCLUDE_DELETEDTracker = param != null;

        this.localEXCLUDE_DELETED = param;
    }

    public boolean isCOLUMNSSpecified() {
        return localCOLUMNSTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.ColumnsElementType
     */
    public reporting.engageservice.ColumnsElementType getCOLUMNS() {
        return localCOLUMNS;
    }

    /**
     * Auto generated setter method
     * @param param COLUMNS
     */
    public void setCOLUMNS(reporting.engageservice.ColumnsElementType param) {
        localCOLUMNSTracker = param != null;

        this.localCOLUMNS = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.Reporting");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":RawRecipientDataExportRequestType",
                    xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "RawRecipientDataExportRequestType", xmlWriter);
            }
        }

        if (localMAILING_IDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "MAILING_ID", xmlWriter);

            if (localMAILING_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "MAILING_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localMAILING_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORT_IDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "REPORT_ID", xmlWriter);

            if (localREPORT_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORT_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localREPORT_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localMAILINGTracker) {
            if (localMAILING != null) {
                for (int i = 0; i < localMAILING.length; i++) {
                    if (localMAILING[i] != null) {
                        localMAILING[i].serialize(new javax.xml.namespace.QName(
                                "SilverpopApi:EngageService.Reporting",
                                "MAILING"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "MAILING cannot be null!!");
            }
        }

        if (localCAMPAIGN_IDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CAMPAIGN_ID", xmlWriter);

            if (localCAMPAIGN_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "CAMPAIGN_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localCAMPAIGN_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localLIST_IDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "LIST_ID", xmlWriter);

            if (localLIST_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "LIST_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localLIST_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_QUERIESTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_QUERIES", xmlWriter);

            if (localINCLUDE_QUERIES == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_QUERIES cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_QUERIES);
            }

            xmlWriter.writeEndElement();
        }

        if (localALL_NON_EXPORTEDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "ALL_NON_EXPORTED", xmlWriter);

            if (localALL_NON_EXPORTED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALL_NON_EXPORTED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALL_NON_EXPORTED);
            }

            xmlWriter.writeEndElement();
        }

        if (localEVENT_DATE_STARTTracker) {
            if (localEVENT_DATE_START == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "EVENT_DATE_START cannot be null!!");
            }

            localEVENT_DATE_START.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "EVENT_DATE_START"),
                xmlWriter);
        }

        if (localEVENT_DATE_ENDTracker) {
            if (localEVENT_DATE_END == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "EVENT_DATE_END cannot be null!!");
            }

            localEVENT_DATE_END.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "EVENT_DATE_END"),
                xmlWriter);
        }

        if (localSEND_DATE_STARTTracker) {
            if (localSEND_DATE_START == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SEND_DATE_START cannot be null!!");
            }

            localSEND_DATE_START.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "SEND_DATE_START"),
                xmlWriter);
        }

        if (localSEND_DATE_ENDTracker) {
            if (localSEND_DATE_END == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SEND_DATE_END cannot be null!!");
            }

            localSEND_DATE_END.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "SEND_DATE_END"),
                xmlWriter);
        }

        if (localEXPORT_FORMATTracker) {
            if (localEXPORT_FORMAT == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "EXPORT_FORMAT cannot be null!!");
            }

            localEXPORT_FORMAT.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "EXPORT_FORMAT"),
                xmlWriter);
        }

        if (localEXPORT_FILE_NAMETracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "EXPORT_FILE_NAME", xmlWriter);

            if (localEXPORT_FILE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXPORT_FILE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXPORT_FILE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localFILE_ENCODINGTracker) {
            if (localFILE_ENCODING == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "FILE_ENCODING cannot be null!!");
            }

            localFILE_ENCODING.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "FILE_ENCODING"),
                xmlWriter);
        }

        if (localEMAILTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "EMAIL", xmlWriter);

            if (localEMAIL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL);
            }

            xmlWriter.writeEndElement();
        }

        if (localMOVE_TO_FTPTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "MOVE_TO_FTP", xmlWriter);

            if (localMOVE_TO_FTP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MOVE_TO_FTP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMOVE_TO_FTP);
            }

            xmlWriter.writeEndElement();
        }

        if (localPRIVATETracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "PRIVATE", xmlWriter);

            if (localPRIVATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PRIVATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPRIVATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSHAREDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SHARED", xmlWriter);

            if (localSHARED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SHARED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSHARED);
            }

            xmlWriter.writeEndElement();
        }

        if (localSENT_MAILINGSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SENT_MAILINGS", xmlWriter);

            if (localSENT_MAILINGS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SENT_MAILINGS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSENT_MAILINGS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSENDINGTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SENDING", xmlWriter);

            if (localSENDING == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SENDING cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSENDING);
            }

            xmlWriter.writeEndElement();
        }

        if (localOPTIN_CONFIRMATIONTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "OPTIN_CONFIRMATION", xmlWriter);

            if (localOPTIN_CONFIRMATION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OPTIN_CONFIRMATION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOPTIN_CONFIRMATION);
            }

            xmlWriter.writeEndElement();
        }

        if (localPROFILE_CONFIRMATIONTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "PROFILE_CONFIRMATION", xmlWriter);

            if (localPROFILE_CONFIRMATION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PROFILE_CONFIRMATION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPROFILE_CONFIRMATION);
            }

            xmlWriter.writeEndElement();
        }

        if (localAUTOMATEDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "AUTOMATED", xmlWriter);

            if (localAUTOMATED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AUTOMATED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAUTOMATED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAMPAIGN_ACTIVETracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CAMPAIGN_ACTIVE", xmlWriter);

            if (localCAMPAIGN_ACTIVE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAMPAIGN_ACTIVE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAMPAIGN_ACTIVE);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAMPAIGN_COMPLETEDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CAMPAIGN_COMPLETED", xmlWriter);

            if (localCAMPAIGN_COMPLETED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAMPAIGN_COMPLETED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAMPAIGN_COMPLETED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAMPAIGN_CANCELLEDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CAMPAIGN_CANCELLED", xmlWriter);

            if (localCAMPAIGN_CANCELLED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAMPAIGN_CANCELLED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAMPAIGN_CANCELLED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCAMPAIGN_SCRAPE_TEMPLATETracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CAMPAIGN_SCRAPE_TEMPLATE",
                xmlWriter);

            if (localCAMPAIGN_SCRAPE_TEMPLATE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CAMPAIGN_SCRAPE_TEMPLATE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCAMPAIGN_SCRAPE_TEMPLATE);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_TEST_MAILINGSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_TEST_MAILINGS",
                xmlWriter);

            if (localINCLUDE_TEST_MAILINGS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_TEST_MAILINGS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_TEST_MAILINGS);
            }

            xmlWriter.writeEndElement();
        }

        if (localALL_EVENT_TYPESTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "ALL_EVENT_TYPES", xmlWriter);

            if (localALL_EVENT_TYPES == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALL_EVENT_TYPES cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALL_EVENT_TYPES);
            }

            xmlWriter.writeEndElement();
        }

        if (localSENTTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SENT", xmlWriter);

            if (localSENT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SENT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSENT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUPPRESSEDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SUPPRESSED", xmlWriter);

            if (localSUPPRESSED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUPPRESSED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUPPRESSED);
            }

            xmlWriter.writeEndElement();
        }

        if (localOPENSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "OPENS", xmlWriter);

            if (localOPENS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OPENS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOPENS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCLICKSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CLICKS", xmlWriter);

            if (localCLICKS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CLICKS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCLICKS);
            }

            xmlWriter.writeEndElement();
        }

        if (localOPTINSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "OPTINS", xmlWriter);

            if (localOPTINS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OPTINS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOPTINS);
            }

            xmlWriter.writeEndElement();
        }

        if (localOPTOUTSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "OPTOUTS", xmlWriter);

            if (localOPTOUTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "OPTOUTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localOPTOUTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localFORWARDSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "FORWARDS", xmlWriter);

            if (localFORWARDS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FORWARDS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFORWARDS);
            }

            xmlWriter.writeEndElement();
        }

        if (localATTACHMENTSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "ATTACHMENTS", xmlWriter);

            if (localATTACHMENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ATTACHMENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localATTACHMENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCONVERSIONSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CONVERSIONS", xmlWriter);

            if (localCONVERSIONS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CONVERSIONS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCONVERSIONS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCLICKSTREAMSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CLICKSTREAMS", xmlWriter);

            if (localCLICKSTREAMS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CLICKSTREAMS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCLICKSTREAMS);
            }

            xmlWriter.writeEndElement();
        }

        if (localHARD_BOUNCESTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "HARD_BOUNCES", xmlWriter);

            if (localHARD_BOUNCES == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "HARD_BOUNCES cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localHARD_BOUNCES);
            }

            xmlWriter.writeEndElement();
        }

        if (localSOFT_BOUNCESTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SOFT_BOUNCES", xmlWriter);

            if (localSOFT_BOUNCES == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SOFT_BOUNCES cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSOFT_BOUNCES);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPLY_ABUSETracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "REPLY_ABUSE", xmlWriter);

            if (localREPLY_ABUSE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPLY_ABUSE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPLY_ABUSE);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPLY_COATracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "REPLY_COA", xmlWriter);

            if (localREPLY_COA == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPLY_COA cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPLY_COA);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPLY_OTHERTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "REPLY_OTHER", xmlWriter);

            if (localREPLY_OTHER == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPLY_OTHER cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPLY_OTHER);
            }

            xmlWriter.writeEndElement();
        }

        if (localMAIL_BLOCKSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "MAIL_BLOCKS", xmlWriter);

            if (localMAIL_BLOCKS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MAIL_BLOCKS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMAIL_BLOCKS);
            }

            xmlWriter.writeEndElement();
        }

        if (localMAILING_RESTRICTIONSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "MAILING_RESTRICTIONS", xmlWriter);

            if (localMAILING_RESTRICTIONS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MAILING_RESTRICTIONS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMAILING_RESTRICTIONS);
            }

            xmlWriter.writeEndElement();
        }

        if (localSMS_ERRORTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SMS_ERROR", xmlWriter);

            if (localSMS_ERROR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SMS_ERROR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSMS_ERROR);
            }

            xmlWriter.writeEndElement();
        }

        if (localSMS_REJECTTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SMS_REJECT", xmlWriter);

            if (localSMS_REJECT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SMS_REJECT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSMS_REJECT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSMS_OPTOUTTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "SMS_OPTOUT", xmlWriter);

            if (localSMS_OPTOUT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SMS_OPTOUT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSMS_OPTOUT);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_SEEDSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_SEEDS", xmlWriter);

            if (localINCLUDE_SEEDS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_SEEDS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_SEEDS);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_FORWARDSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_FORWARDS", xmlWriter);

            if (localINCLUDE_FORWARDS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_FORWARDS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_FORWARDS);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_INBOX_MONITORINGTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_INBOX_MONITORING",
                xmlWriter);

            if (localINCLUDE_INBOX_MONITORING == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_INBOX_MONITORING cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_INBOX_MONITORING);
            }

            xmlWriter.writeEndElement();
        }

        if (localCODED_TYPE_FIELDSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "CODED_TYPE_FIELDS", xmlWriter);

            if (localCODED_TYPE_FIELDS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "CODED_TYPE_FIELDS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localCODED_TYPE_FIELDS);
            }

            xmlWriter.writeEndElement();
        }

        if (localEXCLUDE_DELETEDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "EXCLUDE_DELETED", xmlWriter);

            if (localEXCLUDE_DELETED == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXCLUDE_DELETED cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXCLUDE_DELETED);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOLUMNSTracker) {
            if (localCOLUMNS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "COLUMNS cannot be null!!");
            }

            localCOLUMNS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "COLUMNS"),
                xmlWriter);
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.Reporting")) {
            return "ns6";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static RawRecipientDataExportRequestType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            RawRecipientDataExportRequestType object = new RawRecipientDataExportRequestType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"RawRecipientDataExportRequestType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (RawRecipientDataExportRequestType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                java.util.ArrayList list3 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "MAILING_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAILING_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MAILING_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMAILING_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setMAILING_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "REPORT_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORT_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORT_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORT_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setREPORT_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "MAILING").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAILING").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list3.add(reporting.engageservice.MultiMailingsElementType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone3 = false;

                    while (!loopDone3) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone3 = true;
                        } else {
                            if (new javax.xml.namespace.QName(
                                        "SilverpopApi:EngageService.Reporting",
                                        "MAILING").equals(reader.getName())) {
                                list3.add(reporting.engageservice.MultiMailingsElementType.Factory.parse(
                                        reader));
                            } else {
                                loopDone3 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setMAILING((reporting.engageservice.MultiMailingsElementType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            reporting.engageservice.MultiMailingsElementType.class,
                            list3));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CAMPAIGN_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAMPAIGN_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAMPAIGN_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAMPAIGN_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setCAMPAIGN_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "LIST_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "LIST_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LIST_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLIST_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setLIST_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_QUERIES").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCLUDE_QUERIES").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_QUERIES" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_QUERIES(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "ALL_NON_EXPORTED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ALL_NON_EXPORTED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALL_NON_EXPORTED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALL_NON_EXPORTED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "EVENT_DATE_START").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EVENT_DATE_START").equals(
                            reader.getName())) {
                    object.setEVENT_DATE_START(reporting.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "EVENT_DATE_END").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EVENT_DATE_END").equals(
                            reader.getName())) {
                    object.setEVENT_DATE_END(reporting.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "SEND_DATE_START").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SEND_DATE_START").equals(
                            reader.getName())) {
                    object.setSEND_DATE_START(reporting.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "SEND_DATE_END").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SEND_DATE_END").equals(
                            reader.getName())) {
                    object.setSEND_DATE_END(reporting.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "EXPORT_FORMAT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXPORT_FORMAT").equals(
                            reader.getName())) {
                    object.setEXPORT_FORMAT(reporting.engageservice.EventExportFormat.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "EXPORT_FILE_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXPORT_FILE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXPORT_FILE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXPORT_FILE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "FILE_ENCODING").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FILE_ENCODING").equals(
                            reader.getName())) {
                    object.setFILE_ENCODING(reporting.engageservice.FileEncoding.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "EMAIL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "MOVE_TO_FTP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MOVE_TO_FTP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MOVE_TO_FTP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMOVE_TO_FTP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "PRIVATE").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PRIVATE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PRIVATE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPRIVATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SHARED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SHARED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SHARED" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSHARED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "SENT_MAILINGS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SENT_MAILINGS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SENT_MAILINGS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSENT_MAILINGS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SENDING").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SENDING").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SENDING" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSENDING(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "OPTIN_CONFIRMATION").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "OPTIN_CONFIRMATION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OPTIN_CONFIRMATION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOPTIN_CONFIRMATION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "PROFILE_CONFIRMATION").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "PROFILE_CONFIRMATION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PROFILE_CONFIRMATION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPROFILE_CONFIRMATION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "AUTOMATED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "AUTOMATED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AUTOMATED" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAUTOMATED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CAMPAIGN_ACTIVE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAMPAIGN_ACTIVE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAMPAIGN_ACTIVE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAMPAIGN_ACTIVE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CAMPAIGN_COMPLETED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAMPAIGN_COMPLETED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAMPAIGN_COMPLETED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAMPAIGN_COMPLETED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CAMPAIGN_CANCELLED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CAMPAIGN_CANCELLED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAMPAIGN_CANCELLED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAMPAIGN_CANCELLED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CAMPAIGN_SCRAPE_TEMPLATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "CAMPAIGN_SCRAPE_TEMPLATE").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CAMPAIGN_SCRAPE_TEMPLATE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCAMPAIGN_SCRAPE_TEMPLATE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_TEST_MAILINGS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INCLUDE_TEST_MAILINGS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_TEST_MAILINGS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_TEST_MAILINGS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "ALL_EVENT_TYPES").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ALL_EVENT_TYPES").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALL_EVENT_TYPES" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALL_EVENT_TYPES(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SENT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SENT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SENT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSENT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SUPPRESSED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUPPRESSED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUPPRESSED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUPPRESSED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "OPENS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OPENS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OPENS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOPENS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "CLICKS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "CLICKS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CLICKS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCLICKS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "OPTINS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OPTINS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OPTINS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOPTINS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "OPTOUTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "OPTOUTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "OPTOUTS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setOPTOUTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "FORWARDS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FORWARDS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FORWARDS" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFORWARDS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "ATTACHMENTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ATTACHMENTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ATTACHMENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setATTACHMENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CONVERSIONS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CONVERSIONS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CONVERSIONS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCONVERSIONS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CLICKSTREAMS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CLICKSTREAMS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CLICKSTREAMS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCLICKSTREAMS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "HARD_BOUNCES").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "HARD_BOUNCES").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "HARD_BOUNCES" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setHARD_BOUNCES(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "SOFT_BOUNCES").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SOFT_BOUNCES").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SOFT_BOUNCES" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSOFT_BOUNCES(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "REPLY_ABUSE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPLY_ABUSE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPLY_ABUSE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPLY_ABUSE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "REPLY_COA").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPLY_COA").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPLY_COA" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPLY_COA(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "REPLY_OTHER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPLY_OTHER").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPLY_OTHER" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPLY_OTHER(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "MAIL_BLOCKS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAIL_BLOCKS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MAIL_BLOCKS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMAIL_BLOCKS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "MAILING_RESTRICTIONS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAILING_RESTRICTIONS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MAILING_RESTRICTIONS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMAILING_RESTRICTIONS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SMS_ERROR").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SMS_ERROR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SMS_ERROR" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSMS_ERROR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SMS_REJECT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SMS_REJECT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SMS_REJECT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSMS_REJECT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SMS_OPTOUT").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SMS_OPTOUT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SMS_OPTOUT" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSMS_OPTOUT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_SEEDS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCLUDE_SEEDS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_SEEDS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_SEEDS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_FORWARDS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCLUDE_FORWARDS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_FORWARDS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_FORWARDS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_INBOX_MONITORING").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INCLUDE_INBOX_MONITORING").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_INBOX_MONITORING" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_INBOX_MONITORING(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "CODED_TYPE_FIELDS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CODED_TYPE_FIELDS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CODED_TYPE_FIELDS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCODED_TYPE_FIELDS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "EXCLUDE_DELETED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXCLUDE_DELETED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXCLUDE_DELETED" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXCLUDE_DELETED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "COLUMNS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "COLUMNS").equals(
                            reader.getName())) {
                    object.setCOLUMNS(reporting.engageservice.ColumnsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
