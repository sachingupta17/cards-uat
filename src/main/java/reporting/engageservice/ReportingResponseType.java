/**
 * ReportingResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package reporting.engageservice;


/**
 *  ReportingResponseType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ReportingResponseType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ReportingResponseType
       Namespace URI = SilverpopApi:EngageService.Reporting
       Namespace Prefix = ns6
     */

    /**
     * field for SUCCESS
     */
    protected reporting.engageservice.ReportingSuccess localSUCCESS;

    /**
     * field for Fault
     */
    protected reporting.engageservice.FaultType localFault;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFaultTracker = false;

    /**
     * field for MAILING
     */
    protected reporting.engageservice.TrackingMetricMailingElementType localMAILING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMAILINGTracker = false;

    /**
     * field for MailingE
     * This was an Array!
     */
    protected reporting.engageservice.MailingElementType[] localMailingE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMailingETracker = false;

    /**
     * field for JOB_ID
     */
    protected long localJOB_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localJOB_IDTracker = false;

    /**
     * field for FILE_PATH
     */
    protected java.lang.String localFILE_PATH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFILE_PATHTracker = false;

    /**
     * field for JOB_STATUS
     */
    protected reporting.engageservice.JobStatus localJOB_STATUS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localJOB_STATUSTracker = false;

    /**
     * field for JOB_DESCRIPTION
     */
    protected java.lang.String localJOB_DESCRIPTION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localJOB_DESCRIPTIONTracker = false;

    /**
     * field for PARAMETERS
     */
    protected reporting.engageservice.ParametersElementType localPARAMETERS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPARAMETERSTracker = false;

    /**
     * field for TopDomains
     */
    protected reporting.engageservice.TopDomainsElementType localTopDomains;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTopDomainsTracker = false;

    /**
     * field for InboxMonitored
     */
    protected reporting.engageservice.InboxMonitoredElementType localInboxMonitored;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localInboxMonitoredTracker = false;

    /**
     * field for Clicks
     */
    protected reporting.engageservice.ClicksElementType localClicks;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localClicksTracker = false;

    /**
     * Auto generated getter method
     * @return reporting.engageservice.ReportingSuccess
     */
    public reporting.engageservice.ReportingSuccess getSUCCESS() {
        return localSUCCESS;
    }

    /**
     * Auto generated setter method
     * @param param SUCCESS
     */
    public void setSUCCESS(reporting.engageservice.ReportingSuccess param) {
        this.localSUCCESS = param;
    }

    public boolean isFaultSpecified() {
        return localFaultTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.FaultType
     */
    public reporting.engageservice.FaultType getFault() {
        return localFault;
    }

    /**
     * Auto generated setter method
     * @param param Fault
     */
    public void setFault(reporting.engageservice.FaultType param) {
        localFaultTracker = param != null;

        this.localFault = param;
    }

    public boolean isMAILINGSpecified() {
        return localMAILINGTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.TrackingMetricMailingElementType
     */
    public reporting.engageservice.TrackingMetricMailingElementType getMAILING() {
        return localMAILING;
    }

    /**
     * Auto generated setter method
     * @param param MAILING
     */
    public void setMAILING(
        reporting.engageservice.TrackingMetricMailingElementType param) {
        localMAILINGTracker = param != null;

        this.localMAILING = param;
    }

    public boolean isMailingESpecified() {
        return localMailingETracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.MailingElementType[]
     */
    public reporting.engageservice.MailingElementType[] getMailingE() {
        return localMailingE;
    }

    /**
     * validate the array for MailingE
     */
    protected void validateMailingE(
        reporting.engageservice.MailingElementType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param MailingE
     */
    public void setMailingE(reporting.engageservice.MailingElementType[] param) {
        validateMailingE(param);

        localMailingETracker = param != null;

        this.localMailingE = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param reporting.engageservice.MailingElementType
     */
    public void addMailingE(reporting.engageservice.MailingElementType param) {
        if (localMailingE == null) {
            localMailingE = new reporting.engageservice.MailingElementType[] {  };
        }

        //update the setting tracker
        localMailingETracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localMailingE);
        list.add(param);
        this.localMailingE = (reporting.engageservice.MailingElementType[]) list.toArray(new reporting.engageservice.MailingElementType[list.size()]);
    }

    public boolean isJOB_IDSpecified() {
        return localJOB_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getJOB_ID() {
        return localJOB_ID;
    }

    /**
     * Auto generated setter method
     * @param param JOB_ID
     */
    public void setJOB_ID(long param) {
        // setting primitive attribute tracker to true
        localJOB_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localJOB_ID = param;
    }

    public boolean isFILE_PATHSpecified() {
        return localFILE_PATHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFILE_PATH() {
        return localFILE_PATH;
    }

    /**
     * Auto generated setter method
     * @param param FILE_PATH
     */
    public void setFILE_PATH(java.lang.String param) {
        localFILE_PATHTracker = param != null;

        this.localFILE_PATH = param;
    }

    public boolean isJOB_STATUSSpecified() {
        return localJOB_STATUSTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.JobStatus
     */
    public reporting.engageservice.JobStatus getJOB_STATUS() {
        return localJOB_STATUS;
    }

    /**
     * Auto generated setter method
     * @param param JOB_STATUS
     */
    public void setJOB_STATUS(reporting.engageservice.JobStatus param) {
        localJOB_STATUSTracker = param != null;

        this.localJOB_STATUS = param;
    }

    public boolean isJOB_DESCRIPTIONSpecified() {
        return localJOB_DESCRIPTIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getJOB_DESCRIPTION() {
        return localJOB_DESCRIPTION;
    }

    /**
     * Auto generated setter method
     * @param param JOB_DESCRIPTION
     */
    public void setJOB_DESCRIPTION(java.lang.String param) {
        localJOB_DESCRIPTIONTracker = param != null;

        this.localJOB_DESCRIPTION = param;
    }

    public boolean isPARAMETERSSpecified() {
        return localPARAMETERSTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.ParametersElementType
     */
    public reporting.engageservice.ParametersElementType getPARAMETERS() {
        return localPARAMETERS;
    }

    /**
     * Auto generated setter method
     * @param param PARAMETERS
     */
    public void setPARAMETERS(
        reporting.engageservice.ParametersElementType param) {
        localPARAMETERSTracker = param != null;

        this.localPARAMETERS = param;
    }

    public boolean isTopDomainsSpecified() {
        return localTopDomainsTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.TopDomainsElementType
     */
    public reporting.engageservice.TopDomainsElementType getTopDomains() {
        return localTopDomains;
    }

    /**
     * Auto generated setter method
     * @param param TopDomains
     */
    public void setTopDomains(
        reporting.engageservice.TopDomainsElementType param) {
        localTopDomainsTracker = param != null;

        this.localTopDomains = param;
    }

    public boolean isInboxMonitoredSpecified() {
        return localInboxMonitoredTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.InboxMonitoredElementType
     */
    public reporting.engageservice.InboxMonitoredElementType getInboxMonitored() {
        return localInboxMonitored;
    }

    /**
     * Auto generated setter method
     * @param param InboxMonitored
     */
    public void setInboxMonitored(
        reporting.engageservice.InboxMonitoredElementType param) {
        localInboxMonitoredTracker = param != null;

        this.localInboxMonitored = param;
    }

    public boolean isClicksSpecified() {
        return localClicksTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.ClicksElementType
     */
    public reporting.engageservice.ClicksElementType getClicks() {
        return localClicks;
    }

    /**
     * Auto generated setter method
     * @param param Clicks
     */
    public void setClicks(reporting.engageservice.ClicksElementType param) {
        localClicksTracker = param != null;

        this.localClicks = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.Reporting");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ReportingResponseType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ReportingResponseType", xmlWriter);
            }
        }

        if (localSUCCESS == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "SUCCESS cannot be null!!");
        }

        localSUCCESS.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.Reporting", "SUCCESS"), xmlWriter);

        if (localFaultTracker) {
            if (localFault == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "Fault cannot be null!!");
            }

            localFault.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "Fault"), xmlWriter);
        }

        if (localMAILINGTracker) {
            if (localMAILING == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "MAILING cannot be null!!");
            }

            localMAILING.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "MAILING"),
                xmlWriter);
        }

        if (localMailingETracker) {
            if (localMailingE != null) {
                for (int i = 0; i < localMailingE.length; i++) {
                    if (localMailingE[i] != null) {
                        localMailingE[i].serialize(new javax.xml.namespace.QName(
                                "SilverpopApi:EngageService.Reporting",
                                "Mailing"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "Mailing cannot be null!!");
            }
        }

        if (localJOB_IDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "JOB_ID", xmlWriter);

            if (localJOB_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "JOB_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localJOB_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localFILE_PATHTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "FILE_PATH", xmlWriter);

            if (localFILE_PATH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FILE_PATH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFILE_PATH);
            }

            xmlWriter.writeEndElement();
        }

        if (localJOB_STATUSTracker) {
            if (localJOB_STATUS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "JOB_STATUS cannot be null!!");
            }

            localJOB_STATUS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "JOB_STATUS"),
                xmlWriter);
        }

        if (localJOB_DESCRIPTIONTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "JOB_DESCRIPTION", xmlWriter);

            if (localJOB_DESCRIPTION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "JOB_DESCRIPTION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localJOB_DESCRIPTION);
            }

            xmlWriter.writeEndElement();
        }

        if (localPARAMETERSTracker) {
            if (localPARAMETERS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "PARAMETERS cannot be null!!");
            }

            localPARAMETERS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "PARAMETERS"),
                xmlWriter);
        }

        if (localTopDomainsTracker) {
            if (localTopDomains == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "TopDomains cannot be null!!");
            }

            localTopDomains.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "TopDomains"),
                xmlWriter);
        }

        if (localInboxMonitoredTracker) {
            if (localInboxMonitored == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "InboxMonitored cannot be null!!");
            }

            localInboxMonitored.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "InboxMonitored"),
                xmlWriter);
        }

        if (localClicksTracker) {
            if (localClicks == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "Clicks cannot be null!!");
            }

            localClicks.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "Clicks"), xmlWriter);
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.Reporting")) {
            return "ns6";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ReportingResponseType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ReportingResponseType object = new ReportingResponseType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ReportingResponseType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ReportingResponseType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                java.util.ArrayList list4 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SUCCESS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUCCESS").equals(
                            reader.getName())) {
                    object.setSUCCESS(reporting.engageservice.ReportingSuccess.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "Fault").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Fault").equals(
                            reader.getName())) {
                    object.setFault(reporting.engageservice.FaultType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "MAILING").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAILING").equals(
                            reader.getName())) {
                    object.setMAILING(reporting.engageservice.TrackingMetricMailingElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "Mailing").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Mailing").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list4.add(reporting.engageservice.MailingElementType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone4 = false;

                    while (!loopDone4) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone4 = true;
                        } else {
                            if (new javax.xml.namespace.QName(
                                        "SilverpopApi:EngageService.Reporting",
                                        "Mailing").equals(reader.getName())) {
                                list4.add(reporting.engageservice.MailingElementType.Factory.parse(
                                        reader));
                            } else {
                                loopDone4 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setMailingE((reporting.engageservice.MailingElementType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            reporting.engageservice.MailingElementType.class,
                            list4));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "JOB_ID").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "JOB_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "JOB_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setJOB_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setJOB_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "FILE_PATH").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "FILE_PATH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FILE_PATH" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFILE_PATH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "JOB_STATUS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "JOB_STATUS").equals(
                            reader.getName())) {
                    object.setJOB_STATUS(reporting.engageservice.JobStatus.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "JOB_DESCRIPTION").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "JOB_DESCRIPTION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "JOB_DESCRIPTION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setJOB_DESCRIPTION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "PARAMETERS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "PARAMETERS").equals(
                            reader.getName())) {
                    object.setPARAMETERS(reporting.engageservice.ParametersElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "TopDomains").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "TopDomains").equals(
                            reader.getName())) {
                    object.setTopDomains(reporting.engageservice.TopDomainsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "InboxMonitored").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "InboxMonitored").equals(
                            reader.getName())) {
                    object.setInboxMonitored(reporting.engageservice.InboxMonitoredElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "Clicks").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Clicks").equals(
                            reader.getName())) {
                    object.setClicks(reporting.engageservice.ClicksElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
