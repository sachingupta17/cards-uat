/**
 * WebTrackingDataExportRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package reporting.engageservice;


/**
 *  WebTrackingDataExportRequestType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class WebTrackingDataExportRequestType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = WebTrackingDataExportRequestType
       Namespace URI = SilverpopApi:EngageService.Reporting
       Namespace Prefix = ns6
     */

    /**
     * field for EVENT_DATE_START
     */
    protected reporting.engageservice.DateTime3 localEVENT_DATE_START;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEVENT_DATE_STARTTracker = false;

    /**
     * field for EVENT_DATE_END
     */
    protected reporting.engageservice.DateTime3 localEVENT_DATE_END;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEVENT_DATE_ENDTracker = false;

    /**
     * field for DOMAINS
     */
    protected reporting.engageservice.DomainsElementType localDOMAINS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDOMAINSTracker = false;

    /**
     * field for SITES
     */
    protected reporting.engageservice.SitesElementType localSITES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSITESTracker = false;

    /**
     * field for DATABASE_ID
     */
    protected long localDATABASE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATABASE_IDTracker = false;

    /**
     * field for EXPORT_FORMAT
     */
    protected reporting.engageservice.EventExportFormat localEXPORT_FORMAT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXPORT_FORMATTracker = false;

    /**
     * field for EXPORT_FILE_NAME
     */
    protected java.lang.String localEXPORT_FILE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEXPORT_FILE_NAMETracker = false;

    /**
     * field for FILE_ENCODING
     */
    protected reporting.engageservice.FileEncoding localFILE_ENCODING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFILE_ENCODINGTracker = false;

    /**
     * field for EMAIL
     */
    protected java.lang.String localEMAIL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILTracker = false;

    /**
     * field for MOVE_TO_FTP
     */
    protected java.lang.String localMOVE_TO_FTP;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMOVE_TO_FTPTracker = false;

    /**
     * field for ALL_EVENT_TYPES
     */
    protected java.lang.String localALL_EVENT_TYPES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALL_EVENT_TYPESTracker = false;

    /**
     * field for INCLUDE_SITE_VISIT_EVENTS
     */
    protected java.lang.String localINCLUDE_SITE_VISIT_EVENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_SITE_VISIT_EVENTSTracker = false;

    /**
     * field for INCLUDE_PAGE_VIEW_EVENTS
     */
    protected java.lang.String localINCLUDE_PAGE_VIEW_EVENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_PAGE_VIEW_EVENTSTracker = false;

    /**
     * field for INCLUDE_CLICK_EVENTS
     */
    protected java.lang.String localINCLUDE_CLICK_EVENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_CLICK_EVENTSTracker = false;

    /**
     * field for INCLUDE_FORM_SUBMIT_EVENTS
     */
    protected java.lang.String localINCLUDE_FORM_SUBMIT_EVENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_FORM_SUBMIT_EVENTSTracker = false;

    /**
     * field for INCLUDE_DOWNLOAD_EVENTS
     */
    protected java.lang.String localINCLUDE_DOWNLOAD_EVENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_DOWNLOAD_EVENTSTracker = false;

    /**
     * field for INCLUDE_MEDIA_EVENTS
     */
    protected java.lang.String localINCLUDE_MEDIA_EVENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_MEDIA_EVENTSTracker = false;

    /**
     * field for INCLUDE_SHARE_TO_SOCIAL_EVENTS
     */
    protected java.lang.String localINCLUDE_SHARE_TO_SOCIAL_EVENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_SHARE_TO_SOCIAL_EVENTSTracker = false;

    /**
     * field for INCLUDE_CUSTOM_EVENTS
     */
    protected java.lang.String localINCLUDE_CUSTOM_EVENTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINCLUDE_CUSTOM_EVENTSTracker = false;

    /**
     * field for COLUMNS
     */
    protected reporting.engageservice.ColumnsElementType localCOLUMNS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOLUMNSTracker = false;

    public boolean isEVENT_DATE_STARTSpecified() {
        return localEVENT_DATE_STARTTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime3
     */
    public reporting.engageservice.DateTime3 getEVENT_DATE_START() {
        return localEVENT_DATE_START;
    }

    /**
     * Auto generated setter method
     * @param param EVENT_DATE_START
     */
    public void setEVENT_DATE_START(reporting.engageservice.DateTime3 param) {
        localEVENT_DATE_STARTTracker = param != null;

        this.localEVENT_DATE_START = param;
    }

    public boolean isEVENT_DATE_ENDSpecified() {
        return localEVENT_DATE_ENDTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime3
     */
    public reporting.engageservice.DateTime3 getEVENT_DATE_END() {
        return localEVENT_DATE_END;
    }

    /**
     * Auto generated setter method
     * @param param EVENT_DATE_END
     */
    public void setEVENT_DATE_END(reporting.engageservice.DateTime3 param) {
        localEVENT_DATE_ENDTracker = param != null;

        this.localEVENT_DATE_END = param;
    }

    public boolean isDOMAINSSpecified() {
        return localDOMAINSTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DomainsElementType
     */
    public reporting.engageservice.DomainsElementType getDOMAINS() {
        return localDOMAINS;
    }

    /**
     * Auto generated setter method
     * @param param DOMAINS
     */
    public void setDOMAINS(reporting.engageservice.DomainsElementType param) {
        localDOMAINSTracker = param != null;

        this.localDOMAINS = param;
    }

    public boolean isSITESSpecified() {
        return localSITESTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.SitesElementType
     */
    public reporting.engageservice.SitesElementType getSITES() {
        return localSITES;
    }

    /**
     * Auto generated setter method
     * @param param SITES
     */
    public void setSITES(reporting.engageservice.SitesElementType param) {
        localSITESTracker = param != null;

        this.localSITES = param;
    }

    public boolean isDATABASE_IDSpecified() {
        return localDATABASE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getDATABASE_ID() {
        return localDATABASE_ID;
    }

    /**
     * Auto generated setter method
     * @param param DATABASE_ID
     */
    public void setDATABASE_ID(long param) {
        // setting primitive attribute tracker to true
        localDATABASE_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localDATABASE_ID = param;
    }

    public boolean isEXPORT_FORMATSpecified() {
        return localEXPORT_FORMATTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.EventExportFormat
     */
    public reporting.engageservice.EventExportFormat getEXPORT_FORMAT() {
        return localEXPORT_FORMAT;
    }

    /**
     * Auto generated setter method
     * @param param EXPORT_FORMAT
     */
    public void setEXPORT_FORMAT(
        reporting.engageservice.EventExportFormat param) {
        localEXPORT_FORMATTracker = param != null;

        this.localEXPORT_FORMAT = param;
    }

    public boolean isEXPORT_FILE_NAMESpecified() {
        return localEXPORT_FILE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEXPORT_FILE_NAME() {
        return localEXPORT_FILE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param EXPORT_FILE_NAME
     */
    public void setEXPORT_FILE_NAME(java.lang.String param) {
        localEXPORT_FILE_NAMETracker = param != null;

        this.localEXPORT_FILE_NAME = param;
    }

    public boolean isFILE_ENCODINGSpecified() {
        return localFILE_ENCODINGTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.FileEncoding
     */
    public reporting.engageservice.FileEncoding getFILE_ENCODING() {
        return localFILE_ENCODING;
    }

    /**
     * Auto generated setter method
     * @param param FILE_ENCODING
     */
    public void setFILE_ENCODING(reporting.engageservice.FileEncoding param) {
        localFILE_ENCODINGTracker = param != null;

        this.localFILE_ENCODING = param;
    }

    public boolean isEMAILSpecified() {
        return localEMAILTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL() {
        return localEMAIL;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL
     */
    public void setEMAIL(java.lang.String param) {
        localEMAILTracker = param != null;

        this.localEMAIL = param;
    }

    public boolean isMOVE_TO_FTPSpecified() {
        return localMOVE_TO_FTPTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMOVE_TO_FTP() {
        return localMOVE_TO_FTP;
    }

    /**
     * Auto generated setter method
     * @param param MOVE_TO_FTP
     */
    public void setMOVE_TO_FTP(java.lang.String param) {
        localMOVE_TO_FTPTracker = param != null;

        this.localMOVE_TO_FTP = param;
    }

    public boolean isALL_EVENT_TYPESSpecified() {
        return localALL_EVENT_TYPESTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALL_EVENT_TYPES() {
        return localALL_EVENT_TYPES;
    }

    /**
     * Auto generated setter method
     * @param param ALL_EVENT_TYPES
     */
    public void setALL_EVENT_TYPES(java.lang.String param) {
        localALL_EVENT_TYPESTracker = param != null;

        this.localALL_EVENT_TYPES = param;
    }

    public boolean isINCLUDE_SITE_VISIT_EVENTSSpecified() {
        return localINCLUDE_SITE_VISIT_EVENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_SITE_VISIT_EVENTS() {
        return localINCLUDE_SITE_VISIT_EVENTS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_SITE_VISIT_EVENTS
     */
    public void setINCLUDE_SITE_VISIT_EVENTS(java.lang.String param) {
        localINCLUDE_SITE_VISIT_EVENTSTracker = param != null;

        this.localINCLUDE_SITE_VISIT_EVENTS = param;
    }

    public boolean isINCLUDE_PAGE_VIEW_EVENTSSpecified() {
        return localINCLUDE_PAGE_VIEW_EVENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_PAGE_VIEW_EVENTS() {
        return localINCLUDE_PAGE_VIEW_EVENTS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_PAGE_VIEW_EVENTS
     */
    public void setINCLUDE_PAGE_VIEW_EVENTS(java.lang.String param) {
        localINCLUDE_PAGE_VIEW_EVENTSTracker = param != null;

        this.localINCLUDE_PAGE_VIEW_EVENTS = param;
    }

    public boolean isINCLUDE_CLICK_EVENTSSpecified() {
        return localINCLUDE_CLICK_EVENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_CLICK_EVENTS() {
        return localINCLUDE_CLICK_EVENTS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_CLICK_EVENTS
     */
    public void setINCLUDE_CLICK_EVENTS(java.lang.String param) {
        localINCLUDE_CLICK_EVENTSTracker = param != null;

        this.localINCLUDE_CLICK_EVENTS = param;
    }

    public boolean isINCLUDE_FORM_SUBMIT_EVENTSSpecified() {
        return localINCLUDE_FORM_SUBMIT_EVENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_FORM_SUBMIT_EVENTS() {
        return localINCLUDE_FORM_SUBMIT_EVENTS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_FORM_SUBMIT_EVENTS
     */
    public void setINCLUDE_FORM_SUBMIT_EVENTS(java.lang.String param) {
        localINCLUDE_FORM_SUBMIT_EVENTSTracker = param != null;

        this.localINCLUDE_FORM_SUBMIT_EVENTS = param;
    }

    public boolean isINCLUDE_DOWNLOAD_EVENTSSpecified() {
        return localINCLUDE_DOWNLOAD_EVENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_DOWNLOAD_EVENTS() {
        return localINCLUDE_DOWNLOAD_EVENTS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_DOWNLOAD_EVENTS
     */
    public void setINCLUDE_DOWNLOAD_EVENTS(java.lang.String param) {
        localINCLUDE_DOWNLOAD_EVENTSTracker = param != null;

        this.localINCLUDE_DOWNLOAD_EVENTS = param;
    }

    public boolean isINCLUDE_MEDIA_EVENTSSpecified() {
        return localINCLUDE_MEDIA_EVENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_MEDIA_EVENTS() {
        return localINCLUDE_MEDIA_EVENTS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_MEDIA_EVENTS
     */
    public void setINCLUDE_MEDIA_EVENTS(java.lang.String param) {
        localINCLUDE_MEDIA_EVENTSTracker = param != null;

        this.localINCLUDE_MEDIA_EVENTS = param;
    }

    public boolean isINCLUDE_SHARE_TO_SOCIAL_EVENTSSpecified() {
        return localINCLUDE_SHARE_TO_SOCIAL_EVENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_SHARE_TO_SOCIAL_EVENTS() {
        return localINCLUDE_SHARE_TO_SOCIAL_EVENTS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_SHARE_TO_SOCIAL_EVENTS
     */
    public void setINCLUDE_SHARE_TO_SOCIAL_EVENTS(java.lang.String param) {
        localINCLUDE_SHARE_TO_SOCIAL_EVENTSTracker = param != null;

        this.localINCLUDE_SHARE_TO_SOCIAL_EVENTS = param;
    }

    public boolean isINCLUDE_CUSTOM_EVENTSSpecified() {
        return localINCLUDE_CUSTOM_EVENTSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINCLUDE_CUSTOM_EVENTS() {
        return localINCLUDE_CUSTOM_EVENTS;
    }

    /**
     * Auto generated setter method
     * @param param INCLUDE_CUSTOM_EVENTS
     */
    public void setINCLUDE_CUSTOM_EVENTS(java.lang.String param) {
        localINCLUDE_CUSTOM_EVENTSTracker = param != null;

        this.localINCLUDE_CUSTOM_EVENTS = param;
    }

    public boolean isCOLUMNSSpecified() {
        return localCOLUMNSTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.ColumnsElementType
     */
    public reporting.engageservice.ColumnsElementType getCOLUMNS() {
        return localCOLUMNS;
    }

    /**
     * Auto generated setter method
     * @param param COLUMNS
     */
    public void setCOLUMNS(reporting.engageservice.ColumnsElementType param) {
        localCOLUMNSTracker = param != null;

        this.localCOLUMNS = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.Reporting");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":WebTrackingDataExportRequestType",
                    xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "WebTrackingDataExportRequestType", xmlWriter);
            }
        }

        if (localEVENT_DATE_STARTTracker) {
            if (localEVENT_DATE_START == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "EVENT_DATE_START cannot be null!!");
            }

            localEVENT_DATE_START.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "EVENT_DATE_START"),
                xmlWriter);
        }

        if (localEVENT_DATE_ENDTracker) {
            if (localEVENT_DATE_END == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "EVENT_DATE_END cannot be null!!");
            }

            localEVENT_DATE_END.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "EVENT_DATE_END"),
                xmlWriter);
        }

        if (localDOMAINSTracker) {
            if (localDOMAINS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "DOMAINS cannot be null!!");
            }

            localDOMAINS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "DOMAINS"),
                xmlWriter);
        }

        if (localSITESTracker) {
            if (localSITES == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SITES cannot be null!!");
            }

            localSITES.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "SITES"), xmlWriter);
        }

        if (localDATABASE_IDTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "DATABASE_ID", xmlWriter);

            if (localDATABASE_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "DATABASE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localDATABASE_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localEXPORT_FORMATTracker) {
            if (localEXPORT_FORMAT == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "EXPORT_FORMAT cannot be null!!");
            }

            localEXPORT_FORMAT.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "EXPORT_FORMAT"),
                xmlWriter);
        }

        if (localEXPORT_FILE_NAMETracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "EXPORT_FILE_NAME", xmlWriter);

            if (localEXPORT_FILE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EXPORT_FILE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEXPORT_FILE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localFILE_ENCODINGTracker) {
            if (localFILE_ENCODING == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "FILE_ENCODING cannot be null!!");
            }

            localFILE_ENCODING.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "FILE_ENCODING"),
                xmlWriter);
        }

        if (localEMAILTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "EMAIL", xmlWriter);

            if (localEMAIL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL);
            }

            xmlWriter.writeEndElement();
        }

        if (localMOVE_TO_FTPTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "MOVE_TO_FTP", xmlWriter);

            if (localMOVE_TO_FTP == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MOVE_TO_FTP cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMOVE_TO_FTP);
            }

            xmlWriter.writeEndElement();
        }

        if (localALL_EVENT_TYPESTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "ALL_EVENT_TYPES", xmlWriter);

            if (localALL_EVENT_TYPES == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALL_EVENT_TYPES cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALL_EVENT_TYPES);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_SITE_VISIT_EVENTSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_SITE_VISIT_EVENTS",
                xmlWriter);

            if (localINCLUDE_SITE_VISIT_EVENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_SITE_VISIT_EVENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_SITE_VISIT_EVENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_PAGE_VIEW_EVENTSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_PAGE_VIEW_EVENTS",
                xmlWriter);

            if (localINCLUDE_PAGE_VIEW_EVENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_PAGE_VIEW_EVENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_PAGE_VIEW_EVENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_CLICK_EVENTSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_CLICK_EVENTS", xmlWriter);

            if (localINCLUDE_CLICK_EVENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_CLICK_EVENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_CLICK_EVENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_FORM_SUBMIT_EVENTSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_FORM_SUBMIT_EVENTS",
                xmlWriter);

            if (localINCLUDE_FORM_SUBMIT_EVENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_FORM_SUBMIT_EVENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_FORM_SUBMIT_EVENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_DOWNLOAD_EVENTSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_DOWNLOAD_EVENTS",
                xmlWriter);

            if (localINCLUDE_DOWNLOAD_EVENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_DOWNLOAD_EVENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_DOWNLOAD_EVENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_MEDIA_EVENTSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_MEDIA_EVENTS", xmlWriter);

            if (localINCLUDE_MEDIA_EVENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_MEDIA_EVENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_MEDIA_EVENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_SHARE_TO_SOCIAL_EVENTSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace,
                "INCLUDE_SHARE_TO_SOCIAL_EVENTS", xmlWriter);

            if (localINCLUDE_SHARE_TO_SOCIAL_EVENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_SHARE_TO_SOCIAL_EVENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_SHARE_TO_SOCIAL_EVENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localINCLUDE_CUSTOM_EVENTSTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "INCLUDE_CUSTOM_EVENTS",
                xmlWriter);

            if (localINCLUDE_CUSTOM_EVENTS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INCLUDE_CUSTOM_EVENTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINCLUDE_CUSTOM_EVENTS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOLUMNSTracker) {
            if (localCOLUMNS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "COLUMNS cannot be null!!");
            }

            localCOLUMNS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "COLUMNS"),
                xmlWriter);
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.Reporting")) {
            return "ns6";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static WebTrackingDataExportRequestType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            WebTrackingDataExportRequestType object = new WebTrackingDataExportRequestType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"WebTrackingDataExportRequestType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (WebTrackingDataExportRequestType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "EVENT_DATE_START").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EVENT_DATE_START").equals(
                            reader.getName())) {
                    object.setEVENT_DATE_START(reporting.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "EVENT_DATE_END").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EVENT_DATE_END").equals(
                            reader.getName())) {
                    object.setEVENT_DATE_END(reporting.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "DOMAINS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "DOMAINS").equals(
                            reader.getName())) {
                    object.setDOMAINS(reporting.engageservice.DomainsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SITES").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SITES").equals(
                            reader.getName())) {
                    object.setSITES(reporting.engageservice.SitesElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "DATABASE_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATABASE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "DATABASE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setDATABASE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setDATABASE_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "EXPORT_FORMAT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXPORT_FORMAT").equals(
                            reader.getName())) {
                    object.setEXPORT_FORMAT(reporting.engageservice.EventExportFormat.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "EXPORT_FILE_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXPORT_FILE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EXPORT_FILE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEXPORT_FILE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "FILE_ENCODING").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FILE_ENCODING").equals(
                            reader.getName())) {
                    object.setFILE_ENCODING(reporting.engageservice.FileEncoding.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "EMAIL").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "MOVE_TO_FTP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MOVE_TO_FTP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MOVE_TO_FTP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMOVE_TO_FTP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "ALL_EVENT_TYPES").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ALL_EVENT_TYPES").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALL_EVENT_TYPES" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALL_EVENT_TYPES(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_SITE_VISIT_EVENTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INCLUDE_SITE_VISIT_EVENTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_SITE_VISIT_EVENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_SITE_VISIT_EVENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_PAGE_VIEW_EVENTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INCLUDE_PAGE_VIEW_EVENTS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_PAGE_VIEW_EVENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_PAGE_VIEW_EVENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_CLICK_EVENTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCLUDE_CLICK_EVENTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_CLICK_EVENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_CLICK_EVENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_FORM_SUBMIT_EVENTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INCLUDE_FORM_SUBMIT_EVENTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_FORM_SUBMIT_EVENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_FORM_SUBMIT_EVENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_DOWNLOAD_EVENTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INCLUDE_DOWNLOAD_EVENTS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_DOWNLOAD_EVENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_DOWNLOAD_EVENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_MEDIA_EVENTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "INCLUDE_MEDIA_EVENTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_MEDIA_EVENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_MEDIA_EVENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_SHARE_TO_SOCIAL_EVENTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INCLUDE_SHARE_TO_SOCIAL_EVENTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_SHARE_TO_SOCIAL_EVENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_SHARE_TO_SOCIAL_EVENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "INCLUDE_CUSTOM_EVENTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "INCLUDE_CUSTOM_EVENTS").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INCLUDE_CUSTOM_EVENTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINCLUDE_CUSTOM_EVENTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "COLUMNS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "COLUMNS").equals(
                            reader.getName())) {
                    object.setCOLUMNS(reporting.engageservice.ColumnsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
