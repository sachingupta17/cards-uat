/**
 * MailingElementType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package reporting.engageservice;


/**
 *  MailingElementType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class MailingElementType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = MailingElementType
       Namespace URI = SilverpopApi:EngageService.Reporting
       Namespace Prefix = ns6
     */

    /**
     * field for MailingId
     */
    protected long localMailingId;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMailingIdTracker = false;

    /**
     * field for ReportId
     */
    protected long localReportId;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localReportIdTracker = false;

    /**
     * field for ScheduledTS
     */
    protected reporting.engageservice.DateTime4 localScheduledTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localScheduledTSTracker = false;

    /**
     * field for MailingName
     */
    protected java.lang.String localMailingName;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMailingNameTracker = false;

    /**
     * field for ListName
     */
    protected java.lang.String localListName;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localListNameTracker = false;

    /**
     * field for ListId
     */
    protected long localListId;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localListIdTracker = false;

    /**
     * field for ParentListId
     */
    protected long localParentListId;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localParentListIdTracker = false;

    /**
     * field for UserName
     */
    protected java.lang.String localUserName;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localUserNameTracker = false;

    /**
     * field for SentTS
     */
    protected reporting.engageservice.DateTime4 localSentTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSentTSTracker = false;

    /**
     * field for SentDateTime
     */
    protected reporting.engageservice.DateTime4 localSentDateTime;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSentDateTimeTracker = false;

    /**
     * field for NumSent
     */
    protected int localNumSent;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumSentTracker = false;

    /**
     * field for Subject
     */
    protected java.lang.String localSubject;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSubjectTracker = false;

    /**
     * field for Visibility
     */
    protected reporting.engageservice.PrivateShared localVisibility;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVisibilityTracker = false;

    /**
     * field for QueryId
     */
    protected long localQueryId;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localQueryIdTracker = false;

    /**
     * field for QueryName
     */
    protected java.lang.String localQueryName;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localQueryNameTracker = false;

    /**
     * field for ContactListId
     */
    protected long localContactListId;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localContactListIdTracker = false;

    /**
     * field for ContactListName
     */
    protected java.lang.String localContactListName;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localContactListNameTracker = false;

    /**
     * field for Tags
     */
    protected reporting.engageservice.TagsElementType localTags;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTagsTracker = false;

    /**
     * field for NumSeeds
     */
    protected int localNumSeeds;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumSeedsTracker = false;

    /**
     * field for NumSuppressed
     */
    protected int localNumSuppressed;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumSuppressedTracker = false;

    /**
     * field for NumInboxMonitored
     */
    protected int localNumInboxMonitored;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumInboxMonitoredTracker = false;

    /**
     * field for NumBounceHard
     */
    protected int localNumBounceHard;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumBounceHardTracker = false;

    /**
     * field for NumBounceSoft
     */
    protected int localNumBounceSoft;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumBounceSoftTracker = false;

    /**
     * field for NumUniqueOpen
     */
    protected int localNumUniqueOpen;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueOpenTracker = false;

    /**
     * field for NumGrossOpen
     */
    protected int localNumGrossOpen;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossOpenTracker = false;

    /**
     * field for NumUniqueClick
     */
    protected int localNumUniqueClick;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueClickTracker = false;

    /**
     * field for NumGrossClick
     */
    protected int localNumGrossClick;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossClickTracker = false;

    /**
     * field for NumUniqueAttach
     */
    protected int localNumUniqueAttach;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueAttachTracker = false;

    /**
     * field for NumGrossAttach
     */
    protected int localNumGrossAttach;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossAttachTracker = false;

    /**
     * field for NumUniqueClickStreams
     */
    protected int localNumUniqueClickStreams;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueClickStreamsTracker = false;

    /**
     * field for NumGrossClickStreams
     */
    protected int localNumGrossClickStreams;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossClickStreamsTracker = false;

    /**
     * field for NumUniqueMedia
     */
    protected int localNumUniqueMedia;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueMediaTracker = false;

    /**
     * field for NumGrossMedia
     */
    protected int localNumGrossMedia;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossMediaTracker = false;

    /**
     * field for NumGrossAbuse
     */
    protected int localNumGrossAbuse;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossAbuseTracker = false;

    /**
     * field for NumGrossChangeAddress
     */
    protected int localNumGrossChangeAddress;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossChangeAddressTracker = false;

    /**
     * field for NumGrossMailBlock
     */
    protected int localNumGrossMailBlock;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossMailBlockTracker = false;

    /**
     * field for NumGrossMailRestriction
     */
    protected int localNumGrossMailRestriction;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossMailRestrictionTracker = false;

    /**
     * field for NumGrossOther
     */
    protected int localNumGrossOther;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossOtherTracker = false;

    /**
     * field for NumConversions
     */
    protected int localNumConversions;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumConversionsTracker = false;

    /**
     * field for NumConversionAmount
     */
    protected int localNumConversionAmount;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumConversionAmountTracker = false;

    /**
     * field for NumBounceHardFwd
     */
    protected int localNumBounceHardFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumBounceHardFwdTracker = false;

    /**
     * field for NumBounceSoftFwd
     */
    protected int localNumBounceSoftFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumBounceSoftFwdTracker = false;

    /**
     * field for NumConversionAmountFwd
     */
    protected int localNumConversionAmountFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumConversionAmountFwdTracker = false;

    /**
     * field for NumAttachOpenFwd
     */
    protected int localNumAttachOpenFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumAttachOpenFwdTracker = false;

    /**
     * field for NumClickFwd
     */
    protected int localNumClickFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumClickFwdTracker = false;

    /**
     * field for NumUniqueForwardFwd
     */
    protected int localNumUniqueForwardFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueForwardFwdTracker = false;

    /**
     * field for NumGrossForwardFwd
     */
    protected int localNumGrossForwardFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossForwardFwdTracker = false;

    /**
     * field for NumUniqueConversionsFwd
     */
    protected int localNumUniqueConversionsFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueConversionsFwdTracker = false;

    /**
     * field for NumGrossConversionsFwd
     */
    protected int localNumGrossConversionsFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossConversionsFwdTracker = false;

    /**
     * field for NumUniqueClickstreamFwd
     */
    protected int localNumUniqueClickstreamFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueClickstreamFwdTracker = false;

    /**
     * field for NumGrossClickstreamFwd
     */
    protected int localNumGrossClickstreamFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossClickstreamFwdTracker = false;

    /**
     * field for NumUniqueClickFwd
     */
    protected int localNumUniqueClickFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueClickFwdTracker = false;

    /**
     * field for NumGrossClickFwd
     */
    protected int localNumGrossClickFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossClickFwdTracker = false;

    /**
     * field for NumUniqueAttachOpenFwd
     */
    protected int localNumUniqueAttachOpenFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueAttachOpenFwdTracker = false;

    /**
     * field for NumGrossAttachOpenFwd
     */
    protected int localNumGrossAttachOpenFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossAttachOpenFwdTracker = false;

    /**
     * field for NumUniqueMediaFwd
     */
    protected int localNumUniqueMediaFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueMediaFwdTracker = false;

    /**
     * field for NumGrossMediaFwd
     */
    protected int localNumGrossMediaFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossMediaFwdTracker = false;

    /**
     * field for NumUniqueOpenFwd
     */
    protected int localNumUniqueOpenFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumUniqueOpenFwdTracker = false;

    /**
     * field for NumGrossOpenFwd
     */
    protected int localNumGrossOpenFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumGrossOpenFwdTracker = false;

    /**
     * field for NumAbuseFwd
     */
    protected int localNumAbuseFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumAbuseFwdTracker = false;

    /**
     * field for NumChangeAddressFwd
     */
    protected int localNumChangeAddressFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumChangeAddressFwdTracker = false;

    /**
     * field for NumMailRestrictionFwd
     */
    protected int localNumMailRestrictionFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumMailRestrictionFwdTracker = false;

    /**
     * field for NumMailBlockFwd
     */
    protected int localNumMailBlockFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumMailBlockFwdTracker = false;

    /**
     * field for NumOtherFwd
     */
    protected int localNumOtherFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumOtherFwdTracker = false;

    /**
     * field for NumSuppressedFwd
     */
    protected int localNumSuppressedFwd;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNumSuppressedFwdTracker = false;

    public boolean isMailingIdSpecified() {
        return localMailingIdTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getMailingId() {
        return localMailingId;
    }

    /**
     * Auto generated setter method
     * @param param MailingId
     */
    public void setMailingId(long param) {
        // setting primitive attribute tracker to true
        localMailingIdTracker = param != java.lang.Long.MIN_VALUE;

        this.localMailingId = param;
    }

    public boolean isReportIdSpecified() {
        return localReportIdTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getReportId() {
        return localReportId;
    }

    /**
     * Auto generated setter method
     * @param param ReportId
     */
    public void setReportId(long param) {
        // setting primitive attribute tracker to true
        localReportIdTracker = param != java.lang.Long.MIN_VALUE;

        this.localReportId = param;
    }

    public boolean isScheduledTSSpecified() {
        return localScheduledTSTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime4
     */
    public reporting.engageservice.DateTime4 getScheduledTS() {
        return localScheduledTS;
    }

    /**
     * Auto generated setter method
     * @param param ScheduledTS
     */
    public void setScheduledTS(reporting.engageservice.DateTime4 param) {
        localScheduledTSTracker = param != null;

        this.localScheduledTS = param;
    }

    public boolean isMailingNameSpecified() {
        return localMailingNameTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMailingName() {
        return localMailingName;
    }

    /**
     * Auto generated setter method
     * @param param MailingName
     */
    public void setMailingName(java.lang.String param) {
        localMailingNameTracker = param != null;

        this.localMailingName = param;
    }

    public boolean isListNameSpecified() {
        return localListNameTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getListName() {
        return localListName;
    }

    /**
     * Auto generated setter method
     * @param param ListName
     */
    public void setListName(java.lang.String param) {
        localListNameTracker = param != null;

        this.localListName = param;
    }

    public boolean isListIdSpecified() {
        return localListIdTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getListId() {
        return localListId;
    }

    /**
     * Auto generated setter method
     * @param param ListId
     */
    public void setListId(long param) {
        // setting primitive attribute tracker to true
        localListIdTracker = param != java.lang.Long.MIN_VALUE;

        this.localListId = param;
    }

    public boolean isParentListIdSpecified() {
        return localParentListIdTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getParentListId() {
        return localParentListId;
    }

    /**
     * Auto generated setter method
     * @param param ParentListId
     */
    public void setParentListId(long param) {
        // setting primitive attribute tracker to true
        localParentListIdTracker = param != java.lang.Long.MIN_VALUE;

        this.localParentListId = param;
    }

    public boolean isUserNameSpecified() {
        return localUserNameTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUserName() {
        return localUserName;
    }

    /**
     * Auto generated setter method
     * @param param UserName
     */
    public void setUserName(java.lang.String param) {
        localUserNameTracker = param != null;

        this.localUserName = param;
    }

    public boolean isSentTSSpecified() {
        return localSentTSTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime4
     */
    public reporting.engageservice.DateTime4 getSentTS() {
        return localSentTS;
    }

    /**
     * Auto generated setter method
     * @param param SentTS
     */
    public void setSentTS(reporting.engageservice.DateTime4 param) {
        localSentTSTracker = param != null;

        this.localSentTS = param;
    }

    public boolean isSentDateTimeSpecified() {
        return localSentDateTimeTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.DateTime4
     */
    public reporting.engageservice.DateTime4 getSentDateTime() {
        return localSentDateTime;
    }

    /**
     * Auto generated setter method
     * @param param SentDateTime
     */
    public void setSentDateTime(reporting.engageservice.DateTime4 param) {
        localSentDateTimeTracker = param != null;

        this.localSentDateTime = param;
    }

    public boolean isNumSentSpecified() {
        return localNumSentTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumSent() {
        return localNumSent;
    }

    /**
     * Auto generated setter method
     * @param param NumSent
     */
    public void setNumSent(int param) {
        // setting primitive attribute tracker to true
        localNumSentTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumSent = param;
    }

    public boolean isSubjectSpecified() {
        return localSubjectTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSubject() {
        return localSubject;
    }

    /**
     * Auto generated setter method
     * @param param Subject
     */
    public void setSubject(java.lang.String param) {
        localSubjectTracker = param != null;

        this.localSubject = param;
    }

    public boolean isVisibilitySpecified() {
        return localVisibilityTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.PrivateShared
     */
    public reporting.engageservice.PrivateShared getVisibility() {
        return localVisibility;
    }

    /**
     * Auto generated setter method
     * @param param Visibility
     */
    public void setVisibility(reporting.engageservice.PrivateShared param) {
        localVisibilityTracker = param != null;

        this.localVisibility = param;
    }

    public boolean isQueryIdSpecified() {
        return localQueryIdTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getQueryId() {
        return localQueryId;
    }

    /**
     * Auto generated setter method
     * @param param QueryId
     */
    public void setQueryId(long param) {
        // setting primitive attribute tracker to true
        localQueryIdTracker = param != java.lang.Long.MIN_VALUE;

        this.localQueryId = param;
    }

    public boolean isQueryNameSpecified() {
        return localQueryNameTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getQueryName() {
        return localQueryName;
    }

    /**
     * Auto generated setter method
     * @param param QueryName
     */
    public void setQueryName(java.lang.String param) {
        localQueryNameTracker = param != null;

        this.localQueryName = param;
    }

    public boolean isContactListIdSpecified() {
        return localContactListIdTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getContactListId() {
        return localContactListId;
    }

    /**
     * Auto generated setter method
     * @param param ContactListId
     */
    public void setContactListId(long param) {
        // setting primitive attribute tracker to true
        localContactListIdTracker = param != java.lang.Long.MIN_VALUE;

        this.localContactListId = param;
    }

    public boolean isContactListNameSpecified() {
        return localContactListNameTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getContactListName() {
        return localContactListName;
    }

    /**
     * Auto generated setter method
     * @param param ContactListName
     */
    public void setContactListName(java.lang.String param) {
        localContactListNameTracker = param != null;

        this.localContactListName = param;
    }

    public boolean isTagsSpecified() {
        return localTagsTracker;
    }

    /**
     * Auto generated getter method
     * @return reporting.engageservice.TagsElementType
     */
    public reporting.engageservice.TagsElementType getTags() {
        return localTags;
    }

    /**
     * Auto generated setter method
     * @param param Tags
     */
    public void setTags(reporting.engageservice.TagsElementType param) {
        localTagsTracker = param != null;

        this.localTags = param;
    }

    public boolean isNumSeedsSpecified() {
        return localNumSeedsTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumSeeds() {
        return localNumSeeds;
    }

    /**
     * Auto generated setter method
     * @param param NumSeeds
     */
    public void setNumSeeds(int param) {
        // setting primitive attribute tracker to true
        localNumSeedsTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumSeeds = param;
    }

    public boolean isNumSuppressedSpecified() {
        return localNumSuppressedTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumSuppressed() {
        return localNumSuppressed;
    }

    /**
     * Auto generated setter method
     * @param param NumSuppressed
     */
    public void setNumSuppressed(int param) {
        // setting primitive attribute tracker to true
        localNumSuppressedTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumSuppressed = param;
    }

    public boolean isNumInboxMonitoredSpecified() {
        return localNumInboxMonitoredTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumInboxMonitored() {
        return localNumInboxMonitored;
    }

    /**
     * Auto generated setter method
     * @param param NumInboxMonitored
     */
    public void setNumInboxMonitored(int param) {
        // setting primitive attribute tracker to true
        localNumInboxMonitoredTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumInboxMonitored = param;
    }

    public boolean isNumBounceHardSpecified() {
        return localNumBounceHardTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumBounceHard() {
        return localNumBounceHard;
    }

    /**
     * Auto generated setter method
     * @param param NumBounceHard
     */
    public void setNumBounceHard(int param) {
        // setting primitive attribute tracker to true
        localNumBounceHardTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumBounceHard = param;
    }

    public boolean isNumBounceSoftSpecified() {
        return localNumBounceSoftTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumBounceSoft() {
        return localNumBounceSoft;
    }

    /**
     * Auto generated setter method
     * @param param NumBounceSoft
     */
    public void setNumBounceSoft(int param) {
        // setting primitive attribute tracker to true
        localNumBounceSoftTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumBounceSoft = param;
    }

    public boolean isNumUniqueOpenSpecified() {
        return localNumUniqueOpenTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueOpen() {
        return localNumUniqueOpen;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueOpen
     */
    public void setNumUniqueOpen(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueOpenTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueOpen = param;
    }

    public boolean isNumGrossOpenSpecified() {
        return localNumGrossOpenTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossOpen() {
        return localNumGrossOpen;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossOpen
     */
    public void setNumGrossOpen(int param) {
        // setting primitive attribute tracker to true
        localNumGrossOpenTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossOpen = param;
    }

    public boolean isNumUniqueClickSpecified() {
        return localNumUniqueClickTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueClick() {
        return localNumUniqueClick;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueClick
     */
    public void setNumUniqueClick(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueClickTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueClick = param;
    }

    public boolean isNumGrossClickSpecified() {
        return localNumGrossClickTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossClick() {
        return localNumGrossClick;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossClick
     */
    public void setNumGrossClick(int param) {
        // setting primitive attribute tracker to true
        localNumGrossClickTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossClick = param;
    }

    public boolean isNumUniqueAttachSpecified() {
        return localNumUniqueAttachTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueAttach() {
        return localNumUniqueAttach;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueAttach
     */
    public void setNumUniqueAttach(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueAttachTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueAttach = param;
    }

    public boolean isNumGrossAttachSpecified() {
        return localNumGrossAttachTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossAttach() {
        return localNumGrossAttach;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossAttach
     */
    public void setNumGrossAttach(int param) {
        // setting primitive attribute tracker to true
        localNumGrossAttachTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossAttach = param;
    }

    public boolean isNumUniqueClickStreamsSpecified() {
        return localNumUniqueClickStreamsTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueClickStreams() {
        return localNumUniqueClickStreams;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueClickStreams
     */
    public void setNumUniqueClickStreams(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueClickStreamsTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueClickStreams = param;
    }

    public boolean isNumGrossClickStreamsSpecified() {
        return localNumGrossClickStreamsTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossClickStreams() {
        return localNumGrossClickStreams;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossClickStreams
     */
    public void setNumGrossClickStreams(int param) {
        // setting primitive attribute tracker to true
        localNumGrossClickStreamsTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossClickStreams = param;
    }

    public boolean isNumUniqueMediaSpecified() {
        return localNumUniqueMediaTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueMedia() {
        return localNumUniqueMedia;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueMedia
     */
    public void setNumUniqueMedia(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueMediaTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueMedia = param;
    }

    public boolean isNumGrossMediaSpecified() {
        return localNumGrossMediaTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossMedia() {
        return localNumGrossMedia;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossMedia
     */
    public void setNumGrossMedia(int param) {
        // setting primitive attribute tracker to true
        localNumGrossMediaTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossMedia = param;
    }

    public boolean isNumGrossAbuseSpecified() {
        return localNumGrossAbuseTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossAbuse() {
        return localNumGrossAbuse;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossAbuse
     */
    public void setNumGrossAbuse(int param) {
        // setting primitive attribute tracker to true
        localNumGrossAbuseTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossAbuse = param;
    }

    public boolean isNumGrossChangeAddressSpecified() {
        return localNumGrossChangeAddressTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossChangeAddress() {
        return localNumGrossChangeAddress;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossChangeAddress
     */
    public void setNumGrossChangeAddress(int param) {
        // setting primitive attribute tracker to true
        localNumGrossChangeAddressTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossChangeAddress = param;
    }

    public boolean isNumGrossMailBlockSpecified() {
        return localNumGrossMailBlockTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossMailBlock() {
        return localNumGrossMailBlock;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossMailBlock
     */
    public void setNumGrossMailBlock(int param) {
        // setting primitive attribute tracker to true
        localNumGrossMailBlockTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossMailBlock = param;
    }

    public boolean isNumGrossMailRestrictionSpecified() {
        return localNumGrossMailRestrictionTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossMailRestriction() {
        return localNumGrossMailRestriction;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossMailRestriction
     */
    public void setNumGrossMailRestriction(int param) {
        // setting primitive attribute tracker to true
        localNumGrossMailRestrictionTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossMailRestriction = param;
    }

    public boolean isNumGrossOtherSpecified() {
        return localNumGrossOtherTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossOther() {
        return localNumGrossOther;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossOther
     */
    public void setNumGrossOther(int param) {
        // setting primitive attribute tracker to true
        localNumGrossOtherTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossOther = param;
    }

    public boolean isNumConversionsSpecified() {
        return localNumConversionsTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumConversions() {
        return localNumConversions;
    }

    /**
     * Auto generated setter method
     * @param param NumConversions
     */
    public void setNumConversions(int param) {
        // setting primitive attribute tracker to true
        localNumConversionsTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumConversions = param;
    }

    public boolean isNumConversionAmountSpecified() {
        return localNumConversionAmountTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumConversionAmount() {
        return localNumConversionAmount;
    }

    /**
     * Auto generated setter method
     * @param param NumConversionAmount
     */
    public void setNumConversionAmount(int param) {
        // setting primitive attribute tracker to true
        localNumConversionAmountTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumConversionAmount = param;
    }

    public boolean isNumBounceHardFwdSpecified() {
        return localNumBounceHardFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumBounceHardFwd() {
        return localNumBounceHardFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumBounceHardFwd
     */
    public void setNumBounceHardFwd(int param) {
        // setting primitive attribute tracker to true
        localNumBounceHardFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumBounceHardFwd = param;
    }

    public boolean isNumBounceSoftFwdSpecified() {
        return localNumBounceSoftFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumBounceSoftFwd() {
        return localNumBounceSoftFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumBounceSoftFwd
     */
    public void setNumBounceSoftFwd(int param) {
        // setting primitive attribute tracker to true
        localNumBounceSoftFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumBounceSoftFwd = param;
    }

    public boolean isNumConversionAmountFwdSpecified() {
        return localNumConversionAmountFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumConversionAmountFwd() {
        return localNumConversionAmountFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumConversionAmountFwd
     */
    public void setNumConversionAmountFwd(int param) {
        // setting primitive attribute tracker to true
        localNumConversionAmountFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumConversionAmountFwd = param;
    }

    public boolean isNumAttachOpenFwdSpecified() {
        return localNumAttachOpenFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumAttachOpenFwd() {
        return localNumAttachOpenFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumAttachOpenFwd
     */
    public void setNumAttachOpenFwd(int param) {
        // setting primitive attribute tracker to true
        localNumAttachOpenFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumAttachOpenFwd = param;
    }

    public boolean isNumClickFwdSpecified() {
        return localNumClickFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumClickFwd() {
        return localNumClickFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumClickFwd
     */
    public void setNumClickFwd(int param) {
        // setting primitive attribute tracker to true
        localNumClickFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumClickFwd = param;
    }

    public boolean isNumUniqueForwardFwdSpecified() {
        return localNumUniqueForwardFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueForwardFwd() {
        return localNumUniqueForwardFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueForwardFwd
     */
    public void setNumUniqueForwardFwd(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueForwardFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueForwardFwd = param;
    }

    public boolean isNumGrossForwardFwdSpecified() {
        return localNumGrossForwardFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossForwardFwd() {
        return localNumGrossForwardFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossForwardFwd
     */
    public void setNumGrossForwardFwd(int param) {
        // setting primitive attribute tracker to true
        localNumGrossForwardFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossForwardFwd = param;
    }

    public boolean isNumUniqueConversionsFwdSpecified() {
        return localNumUniqueConversionsFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueConversionsFwd() {
        return localNumUniqueConversionsFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueConversionsFwd
     */
    public void setNumUniqueConversionsFwd(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueConversionsFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueConversionsFwd = param;
    }

    public boolean isNumGrossConversionsFwdSpecified() {
        return localNumGrossConversionsFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossConversionsFwd() {
        return localNumGrossConversionsFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossConversionsFwd
     */
    public void setNumGrossConversionsFwd(int param) {
        // setting primitive attribute tracker to true
        localNumGrossConversionsFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossConversionsFwd = param;
    }

    public boolean isNumUniqueClickstreamFwdSpecified() {
        return localNumUniqueClickstreamFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueClickstreamFwd() {
        return localNumUniqueClickstreamFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueClickstreamFwd
     */
    public void setNumUniqueClickstreamFwd(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueClickstreamFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueClickstreamFwd = param;
    }

    public boolean isNumGrossClickstreamFwdSpecified() {
        return localNumGrossClickstreamFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossClickstreamFwd() {
        return localNumGrossClickstreamFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossClickstreamFwd
     */
    public void setNumGrossClickstreamFwd(int param) {
        // setting primitive attribute tracker to true
        localNumGrossClickstreamFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossClickstreamFwd = param;
    }

    public boolean isNumUniqueClickFwdSpecified() {
        return localNumUniqueClickFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueClickFwd() {
        return localNumUniqueClickFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueClickFwd
     */
    public void setNumUniqueClickFwd(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueClickFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueClickFwd = param;
    }

    public boolean isNumGrossClickFwdSpecified() {
        return localNumGrossClickFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossClickFwd() {
        return localNumGrossClickFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossClickFwd
     */
    public void setNumGrossClickFwd(int param) {
        // setting primitive attribute tracker to true
        localNumGrossClickFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossClickFwd = param;
    }

    public boolean isNumUniqueAttachOpenFwdSpecified() {
        return localNumUniqueAttachOpenFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueAttachOpenFwd() {
        return localNumUniqueAttachOpenFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueAttachOpenFwd
     */
    public void setNumUniqueAttachOpenFwd(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueAttachOpenFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueAttachOpenFwd = param;
    }

    public boolean isNumGrossAttachOpenFwdSpecified() {
        return localNumGrossAttachOpenFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossAttachOpenFwd() {
        return localNumGrossAttachOpenFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossAttachOpenFwd
     */
    public void setNumGrossAttachOpenFwd(int param) {
        // setting primitive attribute tracker to true
        localNumGrossAttachOpenFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossAttachOpenFwd = param;
    }

    public boolean isNumUniqueMediaFwdSpecified() {
        return localNumUniqueMediaFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueMediaFwd() {
        return localNumUniqueMediaFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueMediaFwd
     */
    public void setNumUniqueMediaFwd(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueMediaFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueMediaFwd = param;
    }

    public boolean isNumGrossMediaFwdSpecified() {
        return localNumGrossMediaFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossMediaFwd() {
        return localNumGrossMediaFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossMediaFwd
     */
    public void setNumGrossMediaFwd(int param) {
        // setting primitive attribute tracker to true
        localNumGrossMediaFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossMediaFwd = param;
    }

    public boolean isNumUniqueOpenFwdSpecified() {
        return localNumUniqueOpenFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumUniqueOpenFwd() {
        return localNumUniqueOpenFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumUniqueOpenFwd
     */
    public void setNumUniqueOpenFwd(int param) {
        // setting primitive attribute tracker to true
        localNumUniqueOpenFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumUniqueOpenFwd = param;
    }

    public boolean isNumGrossOpenFwdSpecified() {
        return localNumGrossOpenFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumGrossOpenFwd() {
        return localNumGrossOpenFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumGrossOpenFwd
     */
    public void setNumGrossOpenFwd(int param) {
        // setting primitive attribute tracker to true
        localNumGrossOpenFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumGrossOpenFwd = param;
    }

    public boolean isNumAbuseFwdSpecified() {
        return localNumAbuseFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumAbuseFwd() {
        return localNumAbuseFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumAbuseFwd
     */
    public void setNumAbuseFwd(int param) {
        // setting primitive attribute tracker to true
        localNumAbuseFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumAbuseFwd = param;
    }

    public boolean isNumChangeAddressFwdSpecified() {
        return localNumChangeAddressFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumChangeAddressFwd() {
        return localNumChangeAddressFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumChangeAddressFwd
     */
    public void setNumChangeAddressFwd(int param) {
        // setting primitive attribute tracker to true
        localNumChangeAddressFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumChangeAddressFwd = param;
    }

    public boolean isNumMailRestrictionFwdSpecified() {
        return localNumMailRestrictionFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumMailRestrictionFwd() {
        return localNumMailRestrictionFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumMailRestrictionFwd
     */
    public void setNumMailRestrictionFwd(int param) {
        // setting primitive attribute tracker to true
        localNumMailRestrictionFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumMailRestrictionFwd = param;
    }

    public boolean isNumMailBlockFwdSpecified() {
        return localNumMailBlockFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumMailBlockFwd() {
        return localNumMailBlockFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumMailBlockFwd
     */
    public void setNumMailBlockFwd(int param) {
        // setting primitive attribute tracker to true
        localNumMailBlockFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumMailBlockFwd = param;
    }

    public boolean isNumOtherFwdSpecified() {
        return localNumOtherFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumOtherFwd() {
        return localNumOtherFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumOtherFwd
     */
    public void setNumOtherFwd(int param) {
        // setting primitive attribute tracker to true
        localNumOtherFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumOtherFwd = param;
    }

    public boolean isNumSuppressedFwdSpecified() {
        return localNumSuppressedFwdTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNumSuppressedFwd() {
        return localNumSuppressedFwd;
    }

    /**
     * Auto generated setter method
     * @param param NumSuppressedFwd
     */
    public void setNumSuppressedFwd(int param) {
        // setting primitive attribute tracker to true
        localNumSuppressedFwdTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNumSuppressedFwd = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.Reporting");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":MailingElementType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "MailingElementType", xmlWriter);
            }
        }

        if (localMailingIdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "MailingId", xmlWriter);

            if (localMailingId == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "MailingId cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localMailingId));
            }

            xmlWriter.writeEndElement();
        }

        if (localReportIdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "ReportId", xmlWriter);

            if (localReportId == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ReportId cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localReportId));
            }

            xmlWriter.writeEndElement();
        }

        if (localScheduledTSTracker) {
            if (localScheduledTS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ScheduledTS cannot be null!!");
            }

            localScheduledTS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "ScheduledTS"),
                xmlWriter);
        }

        if (localMailingNameTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "MailingName", xmlWriter);

            if (localMailingName == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "MailingName cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localMailingName);
            }

            xmlWriter.writeEndElement();
        }

        if (localListNameTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "ListName", xmlWriter);

            if (localListName == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ListName cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localListName);
            }

            xmlWriter.writeEndElement();
        }

        if (localListIdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "ListId", xmlWriter);

            if (localListId == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ListId cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localListId));
            }

            xmlWriter.writeEndElement();
        }

        if (localParentListIdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "ParentListId", xmlWriter);

            if (localParentListId == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ParentListId cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localParentListId));
            }

            xmlWriter.writeEndElement();
        }

        if (localUserNameTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "UserName", xmlWriter);

            if (localUserName == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "UserName cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localUserName);
            }

            xmlWriter.writeEndElement();
        }

        if (localSentTSTracker) {
            if (localSentTS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SentTS cannot be null!!");
            }

            localSentTS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "SentTS"), xmlWriter);
        }

        if (localSentDateTimeTracker) {
            if (localSentDateTime == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SentDateTime cannot be null!!");
            }

            localSentDateTime.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "SentDateTime"),
                xmlWriter);
        }

        if (localNumSentTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumSent", xmlWriter);

            if (localNumSent == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumSent cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumSent));
            }

            xmlWriter.writeEndElement();
        }

        if (localSubjectTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "Subject", xmlWriter);

            if (localSubject == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Subject cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSubject);
            }

            xmlWriter.writeEndElement();
        }

        if (localVisibilityTracker) {
            if (localVisibility == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "Visibility cannot be null!!");
            }

            localVisibility.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "Visibility"),
                xmlWriter);
        }

        if (localQueryIdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "QueryId", xmlWriter);

            if (localQueryId == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "QueryId cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localQueryId));
            }

            xmlWriter.writeEndElement();
        }

        if (localQueryNameTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "QueryName", xmlWriter);

            if (localQueryName == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "QueryName cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localQueryName);
            }

            xmlWriter.writeEndElement();
        }

        if (localContactListIdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "ContactListId", xmlWriter);

            if (localContactListId == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ContactListId cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localContactListId));
            }

            xmlWriter.writeEndElement();
        }

        if (localContactListNameTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "ContactListName", xmlWriter);

            if (localContactListName == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ContactListName cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localContactListName);
            }

            xmlWriter.writeEndElement();
        }

        if (localTagsTracker) {
            if (localTags == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "Tags cannot be null!!");
            }

            localTags.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.Reporting", "Tags"), xmlWriter);
        }

        if (localNumSeedsTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumSeeds", xmlWriter);

            if (localNumSeeds == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumSeeds cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumSeeds));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumSuppressedTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumSuppressed", xmlWriter);

            if (localNumSuppressed == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumSuppressed cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumSuppressed));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumInboxMonitoredTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumInboxMonitored", xmlWriter);

            if (localNumInboxMonitored == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumInboxMonitored cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumInboxMonitored));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumBounceHardTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumBounceHard", xmlWriter);

            if (localNumBounceHard == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumBounceHard cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumBounceHard));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumBounceSoftTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumBounceSoft", xmlWriter);

            if (localNumBounceSoft == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumBounceSoft cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumBounceSoft));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueOpenTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueOpen", xmlWriter);

            if (localNumUniqueOpen == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueOpen cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueOpen));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossOpenTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossOpen", xmlWriter);

            if (localNumGrossOpen == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossOpen cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossOpen));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueClickTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueClick", xmlWriter);

            if (localNumUniqueClick == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueClick cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueClick));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossClickTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossClick", xmlWriter);

            if (localNumGrossClick == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossClick cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossClick));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueAttachTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueAttach", xmlWriter);

            if (localNumUniqueAttach == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueAttach cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueAttach));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossAttachTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossAttach", xmlWriter);

            if (localNumGrossAttach == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossAttach cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossAttach));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueClickStreamsTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueClickStreams",
                xmlWriter);

            if (localNumUniqueClickStreams == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueClickStreams cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueClickStreams));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossClickStreamsTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossClickStreams", xmlWriter);

            if (localNumGrossClickStreams == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossClickStreams cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossClickStreams));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueMediaTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueMedia", xmlWriter);

            if (localNumUniqueMedia == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueMedia cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueMedia));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossMediaTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossMedia", xmlWriter);

            if (localNumGrossMedia == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossMedia cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossMedia));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossAbuseTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossAbuse", xmlWriter);

            if (localNumGrossAbuse == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossAbuse cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossAbuse));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossChangeAddressTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossChangeAddress",
                xmlWriter);

            if (localNumGrossChangeAddress == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossChangeAddress cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossChangeAddress));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossMailBlockTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossMailBlock", xmlWriter);

            if (localNumGrossMailBlock == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossMailBlock cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossMailBlock));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossMailRestrictionTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossMailRestriction",
                xmlWriter);

            if (localNumGrossMailRestriction == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossMailRestriction cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossMailRestriction));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossOtherTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossOther", xmlWriter);

            if (localNumGrossOther == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossOther cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossOther));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumConversionsTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumConversions", xmlWriter);

            if (localNumConversions == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumConversions cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumConversions));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumConversionAmountTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumConversionAmount", xmlWriter);

            if (localNumConversionAmount == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumConversionAmount cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumConversionAmount));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumBounceHardFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumBounceHardFwd", xmlWriter);

            if (localNumBounceHardFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumBounceHardFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumBounceHardFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumBounceSoftFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumBounceSoftFwd", xmlWriter);

            if (localNumBounceSoftFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumBounceSoftFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumBounceSoftFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumConversionAmountFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumConversionAmountFwd",
                xmlWriter);

            if (localNumConversionAmountFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumConversionAmountFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumConversionAmountFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumAttachOpenFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumAttachOpenFwd", xmlWriter);

            if (localNumAttachOpenFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumAttachOpenFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumAttachOpenFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumClickFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumClickFwd", xmlWriter);

            if (localNumClickFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumClickFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumClickFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueForwardFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueForwardFwd", xmlWriter);

            if (localNumUniqueForwardFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueForwardFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueForwardFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossForwardFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossForwardFwd", xmlWriter);

            if (localNumGrossForwardFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossForwardFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossForwardFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueConversionsFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueConversionsFwd",
                xmlWriter);

            if (localNumUniqueConversionsFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueConversionsFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueConversionsFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossConversionsFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossConversionsFwd",
                xmlWriter);

            if (localNumGrossConversionsFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossConversionsFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossConversionsFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueClickstreamFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueClickstreamFwd",
                xmlWriter);

            if (localNumUniqueClickstreamFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueClickstreamFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueClickstreamFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossClickstreamFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossClickstreamFwd",
                xmlWriter);

            if (localNumGrossClickstreamFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossClickstreamFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossClickstreamFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueClickFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueClickFwd", xmlWriter);

            if (localNumUniqueClickFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueClickFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueClickFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossClickFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossClickFwd", xmlWriter);

            if (localNumGrossClickFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossClickFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossClickFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueAttachOpenFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueAttachOpenFwd",
                xmlWriter);

            if (localNumUniqueAttachOpenFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueAttachOpenFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueAttachOpenFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossAttachOpenFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossAttachOpenFwd",
                xmlWriter);

            if (localNumGrossAttachOpenFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossAttachOpenFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossAttachOpenFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueMediaFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueMediaFwd", xmlWriter);

            if (localNumUniqueMediaFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueMediaFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueMediaFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossMediaFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossMediaFwd", xmlWriter);

            if (localNumGrossMediaFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossMediaFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossMediaFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumUniqueOpenFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumUniqueOpenFwd", xmlWriter);

            if (localNumUniqueOpenFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumUniqueOpenFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumUniqueOpenFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumGrossOpenFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumGrossOpenFwd", xmlWriter);

            if (localNumGrossOpenFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumGrossOpenFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumGrossOpenFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumAbuseFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumAbuseFwd", xmlWriter);

            if (localNumAbuseFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumAbuseFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumAbuseFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumChangeAddressFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumChangeAddressFwd", xmlWriter);

            if (localNumChangeAddressFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumChangeAddressFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumChangeAddressFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumMailRestrictionFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumMailRestrictionFwd",
                xmlWriter);

            if (localNumMailRestrictionFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumMailRestrictionFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumMailRestrictionFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumMailBlockFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumMailBlockFwd", xmlWriter);

            if (localNumMailBlockFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumMailBlockFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumMailBlockFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumOtherFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumOtherFwd", xmlWriter);

            if (localNumOtherFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumOtherFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumOtherFwd));
            }

            xmlWriter.writeEndElement();
        }

        if (localNumSuppressedFwdTracker) {
            namespace = "SilverpopApi:EngageService.Reporting";
            writeStartElement(null, namespace, "NumSuppressedFwd", xmlWriter);

            if (localNumSuppressedFwd == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NumSuppressedFwd cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNumSuppressedFwd));
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.Reporting")) {
            return "ns6";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static MailingElementType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            MailingElementType object = new MailingElementType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"MailingElementType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (MailingElementType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "MailingId").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "MailingId").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MailingId" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMailingId(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setMailingId(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "ReportId").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ReportId").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ReportId" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setReportId(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setReportId(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "ScheduledTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ScheduledTS").equals(
                            reader.getName())) {
                    object.setScheduledTS(reporting.engageservice.DateTime4.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "MailingName").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MailingName").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MailingName" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMailingName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "ListName").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ListName").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ListName" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setListName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "ListId").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "ListId").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ListId" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setListId(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setListId(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "ParentListId").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ParentListId").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ParentListId" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setParentListId(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setParentListId(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "UserName").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "UserName").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "UserName" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUserName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "SentTS").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "SentTS").equals(
                            reader.getName())) {
                    object.setSentTS(reporting.engageservice.DateTime4.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "SentDateTime").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SentDateTime").equals(
                            reader.getName())) {
                    object.setSentDateTime(reporting.engageservice.DateTime4.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "NumSent").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumSent").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumSent" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumSent(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumSent(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "Subject").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Subject").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Subject" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSubject(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "Visibility").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Visibility").equals(
                            reader.getName())) {
                    object.setVisibility(reporting.engageservice.PrivateShared.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "QueryId").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "QueryId").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "QueryId" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setQueryId(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setQueryId(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "QueryName").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "QueryName").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "QueryName" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setQueryName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "ContactListId").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ContactListId").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ContactListId" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setContactListId(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setContactListId(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "ContactListName").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ContactListName").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ContactListName" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setContactListName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "Tags").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "Tags").equals(
                            reader.getName())) {
                    object.setTags(reporting.engageservice.TagsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting", "NumSeeds").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumSeeds").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumSeeds" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumSeeds(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumSeeds(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumSuppressed").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumSuppressed").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumSuppressed" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumSuppressed(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumSuppressed(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumInboxMonitored").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumInboxMonitored").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumInboxMonitored" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumInboxMonitored(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumInboxMonitored(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumBounceHard").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumBounceHard").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumBounceHard" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumBounceHard(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumBounceHard(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumBounceSoft").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumBounceSoft").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumBounceSoft" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumBounceSoft(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumBounceSoft(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueOpen").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumUniqueOpen").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueOpen" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueOpen(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueOpen(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossOpen").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossOpen").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossOpen" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossOpen(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossOpen(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueClick").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumUniqueClick").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueClick" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueClick(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueClick(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossClick").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossClick").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossClick" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossClick(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossClick(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueAttach").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumUniqueAttach").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueAttach" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueAttach(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueAttach(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossAttach").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossAttach").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossAttach" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossAttach(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossAttach(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueClickStreams").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumUniqueClickStreams").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueClickStreams" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueClickStreams(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueClickStreams(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossClickStreams").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossClickStreams").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossClickStreams" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossClickStreams(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossClickStreams(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueMedia").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumUniqueMedia").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueMedia" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueMedia(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueMedia(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossMedia").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossMedia").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossMedia" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossMedia(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossMedia(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossAbuse").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossAbuse").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossAbuse" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossAbuse(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossAbuse(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossChangeAddress").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumGrossChangeAddress").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossChangeAddress" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossChangeAddress(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossChangeAddress(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossMailBlock").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossMailBlock").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossMailBlock" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossMailBlock(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossMailBlock(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossMailRestriction").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumGrossMailRestriction").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossMailRestriction" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossMailRestriction(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossMailRestriction(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossOther").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossOther").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossOther" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossOther(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossOther(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumConversions").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumConversions").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumConversions" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumConversions(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumConversions(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumConversionAmount").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumConversionAmount").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumConversionAmount" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumConversionAmount(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumConversionAmount(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumBounceHardFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumBounceHardFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumBounceHardFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumBounceHardFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumBounceHardFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumBounceSoftFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumBounceSoftFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumBounceSoftFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumBounceSoftFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumBounceSoftFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumConversionAmountFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumConversionAmountFwd").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumConversionAmountFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumConversionAmountFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumConversionAmountFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumAttachOpenFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumAttachOpenFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumAttachOpenFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumAttachOpenFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumAttachOpenFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumClickFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumClickFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumClickFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumClickFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumClickFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueForwardFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumUniqueForwardFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueForwardFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueForwardFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueForwardFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossForwardFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossForwardFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossForwardFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossForwardFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossForwardFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueConversionsFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumUniqueConversionsFwd").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueConversionsFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueConversionsFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueConversionsFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossConversionsFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumGrossConversionsFwd").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossConversionsFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossConversionsFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossConversionsFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueClickstreamFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumUniqueClickstreamFwd").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueClickstreamFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueClickstreamFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueClickstreamFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossClickstreamFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumGrossClickstreamFwd").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossClickstreamFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossClickstreamFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossClickstreamFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueClickFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumUniqueClickFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueClickFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueClickFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueClickFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossClickFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossClickFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossClickFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossClickFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossClickFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueAttachOpenFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumUniqueAttachOpenFwd").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueAttachOpenFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueAttachOpenFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueAttachOpenFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossAttachOpenFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumGrossAttachOpenFwd").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossAttachOpenFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossAttachOpenFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossAttachOpenFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueMediaFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumUniqueMediaFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueMediaFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueMediaFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueMediaFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossMediaFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossMediaFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossMediaFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossMediaFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossMediaFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumUniqueOpenFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumUniqueOpenFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumUniqueOpenFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumUniqueOpenFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumUniqueOpenFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumGrossOpenFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumGrossOpenFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumGrossOpenFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumGrossOpenFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumGrossOpenFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumAbuseFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumAbuseFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumAbuseFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumAbuseFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumAbuseFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumChangeAddressFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumChangeAddressFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumChangeAddressFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumChangeAddressFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumChangeAddressFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumMailRestrictionFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "NumMailRestrictionFwd").equals(reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumMailRestrictionFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumMailRestrictionFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumMailRestrictionFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumMailBlockFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumMailBlockFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumMailBlockFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumMailBlockFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumMailBlockFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumOtherFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumOtherFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumOtherFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumOtherFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumOtherFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.Reporting",
                            "NumSuppressedFwd").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NumSuppressedFwd").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NumSuppressedFwd" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNumSuppressedFwd(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNumSuppressedFwd(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
