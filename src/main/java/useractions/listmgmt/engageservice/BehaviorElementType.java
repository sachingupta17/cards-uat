/**
 * BehaviorElementType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.listmgmt.engageservice;


/**
 *  BehaviorElementType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class BehaviorElementType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = BehaviorElementType
       Namespace URI = SilverpopApi:EngageService.ListMgmt.UserActions
       Namespace Prefix = ns5
     */

    /**
     * field for OPTION_OPERATOR
     */
    protected useractions.listmgmt.engageservice.OptionOperator localOPTION_OPERATOR;

    /**
     * field for TYPE_OPERATOR
     */
    protected useractions.listmgmt.engageservice.TypeOperator localTYPE_OPERATOR;

    /**
     * field for MAILING_ID
     */
    protected long localMAILING_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMAILING_IDTracker = false;

    /**
     * field for REPORT_ID
     */
    protected long localREPORT_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPORT_IDTracker = false;

    /**
     * field for LINK_NAME
     */
    protected java.lang.String localLINK_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLINK_NAMETracker = false;

    /**
     * field for WHERE_OPERATOR
     */
    protected useractions.listmgmt.engageservice.WhereOperator localWHERE_OPERATOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWHERE_OPERATORTracker = false;

    /**
     * field for CRITERIA_OPERATOR
     */
    protected useractions.listmgmt.engageservice.CriteriaOperator localCRITERIA_OPERATOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCRITERIA_OPERATORTracker = false;

    /**
     * field for VALUES
     */
    protected java.lang.String localVALUES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVALUESTracker = false;

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.OptionOperator
     */
    public useractions.listmgmt.engageservice.OptionOperator getOPTION_OPERATOR() {
        return localOPTION_OPERATOR;
    }

    /**
     * Auto generated setter method
     * @param param OPTION_OPERATOR
     */
    public void setOPTION_OPERATOR(
        useractions.listmgmt.engageservice.OptionOperator param) {
        this.localOPTION_OPERATOR = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.TypeOperator
     */
    public useractions.listmgmt.engageservice.TypeOperator getTYPE_OPERATOR() {
        return localTYPE_OPERATOR;
    }

    /**
     * Auto generated setter method
     * @param param TYPE_OPERATOR
     */
    public void setTYPE_OPERATOR(
        useractions.listmgmt.engageservice.TypeOperator param) {
        this.localTYPE_OPERATOR = param;
    }

    public boolean isMAILING_IDSpecified() {
        return localMAILING_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getMAILING_ID() {
        return localMAILING_ID;
    }

    /**
     * Auto generated setter method
     * @param param MAILING_ID
     */
    public void setMAILING_ID(long param) {
        // setting primitive attribute tracker to true
        localMAILING_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localMAILING_ID = param;
    }

    public boolean isREPORT_IDSpecified() {
        return localREPORT_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getREPORT_ID() {
        return localREPORT_ID;
    }

    /**
     * Auto generated setter method
     * @param param REPORT_ID
     */
    public void setREPORT_ID(long param) {
        // setting primitive attribute tracker to true
        localREPORT_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localREPORT_ID = param;
    }

    public boolean isLINK_NAMESpecified() {
        return localLINK_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getLINK_NAME() {
        return localLINK_NAME;
    }

    /**
     * Auto generated setter method
     * @param param LINK_NAME
     */
    public void setLINK_NAME(java.lang.String param) {
        localLINK_NAMETracker = param != null;

        this.localLINK_NAME = param;
    }

    public boolean isWHERE_OPERATORSpecified() {
        return localWHERE_OPERATORTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.WhereOperator
     */
    public useractions.listmgmt.engageservice.WhereOperator getWHERE_OPERATOR() {
        return localWHERE_OPERATOR;
    }

    /**
     * Auto generated setter method
     * @param param WHERE_OPERATOR
     */
    public void setWHERE_OPERATOR(
        useractions.listmgmt.engageservice.WhereOperator param) {
        localWHERE_OPERATORTracker = param != null;

        this.localWHERE_OPERATOR = param;
    }

    public boolean isCRITERIA_OPERATORSpecified() {
        return localCRITERIA_OPERATORTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.CriteriaOperator
     */
    public useractions.listmgmt.engageservice.CriteriaOperator getCRITERIA_OPERATOR() {
        return localCRITERIA_OPERATOR;
    }

    /**
     * Auto generated setter method
     * @param param CRITERIA_OPERATOR
     */
    public void setCRITERIA_OPERATOR(
        useractions.listmgmt.engageservice.CriteriaOperator param) {
        localCRITERIA_OPERATORTracker = param != null;

        this.localCRITERIA_OPERATOR = param;
    }

    public boolean isVALUESSpecified() {
        return localVALUESTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVALUES() {
        return localVALUES;
    }

    /**
     * Auto generated setter method
     * @param param VALUES
     */
    public void setVALUES(java.lang.String param) {
        localVALUESTracker = param != null;

        this.localVALUES = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.ListMgmt.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":BehaviorElementType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "BehaviorElementType", xmlWriter);
            }
        }

        if (localOPTION_OPERATOR == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "OPTION_OPERATOR cannot be null!!");
        }

        localOPTION_OPERATOR.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions",
                "OPTION_OPERATOR"), xmlWriter);

        if (localTYPE_OPERATOR == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "TYPE_OPERATOR cannot be null!!");
        }

        localTYPE_OPERATOR.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions",
                "TYPE_OPERATOR"), xmlWriter);

        if (localMAILING_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "MAILING_ID", xmlWriter);

            if (localMAILING_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "MAILING_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localMAILING_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localREPORT_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "REPORT_ID", xmlWriter);

            if (localREPORT_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "REPORT_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localREPORT_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localLINK_NAMETracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "LINK_NAME", xmlWriter);

            if (localLINK_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "LINK_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localLINK_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localWHERE_OPERATORTracker) {
            if (localWHERE_OPERATOR == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "WHERE_OPERATOR cannot be null!!");
            }

            localWHERE_OPERATOR.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "WHERE_OPERATOR"), xmlWriter);
        }

        if (localCRITERIA_OPERATORTracker) {
            if (localCRITERIA_OPERATOR == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "CRITERIA_OPERATOR cannot be null!!");
            }

            localCRITERIA_OPERATOR.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "CRITERIA_OPERATOR"), xmlWriter);
        }

        if (localVALUESTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "VALUES", xmlWriter);

            if (localVALUES == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VALUES cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVALUES);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.ListMgmt.UserActions")) {
            return "ns5";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static BehaviorElementType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            BehaviorElementType object = new BehaviorElementType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"BehaviorElementType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (BehaviorElementType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "OPTION_OPERATOR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "OPTION_OPERATOR").equals(
                            reader.getName())) {
                    object.setOPTION_OPERATOR(useractions.listmgmt.engageservice.OptionOperator.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "TYPE_OPERATOR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "TYPE_OPERATOR").equals(
                            reader.getName())) {
                    object.setTYPE_OPERATOR(useractions.listmgmt.engageservice.TypeOperator.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "MAILING_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAILING_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MAILING_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMAILING_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setMAILING_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "REPORT_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPORT_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPORT_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPORT_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setREPORT_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "LINK_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LINK_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LINK_NAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLINK_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "WHERE_OPERATOR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "WHERE_OPERATOR").equals(
                            reader.getName())) {
                    object.setWHERE_OPERATOR(useractions.listmgmt.engageservice.WhereOperator.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "CRITERIA_OPERATOR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CRITERIA_OPERATOR").equals(
                            reader.getName())) {
                    object.setCRITERIA_OPERATOR(useractions.listmgmt.engageservice.CriteriaOperator.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "VALUES").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "VALUES").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VALUES" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVALUES(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
