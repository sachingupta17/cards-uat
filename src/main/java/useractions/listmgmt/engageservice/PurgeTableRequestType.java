/**
 * PurgeTableRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.listmgmt.engageservice;


/**
 *  PurgeTableRequestType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class PurgeTableRequestType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = PurgeTableRequestType
       Namespace URI = SilverpopApi:EngageService.ListMgmt.UserActions
       Namespace Prefix = ns5
     */

    /**
     * field for TABLE_NAME
     */
    protected java.lang.String localTABLE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTABLE_NAMETracker = false;

    /**
     * field for TABLE_ID
     */
    protected long localTABLE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTABLE_IDTracker = false;

    /**
     * field for TABLE_VISIBILITY
     */
    protected useractions.listmgmt.engageservice.Visibility localTABLE_VISIBILITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTABLE_VISIBILITYTracker = false;

    /**
     * field for DELETE_BEFORE
     */
    protected useractions.listmgmt.engageservice.DateTime3 localDELETE_BEFORE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDELETE_BEFORETracker = false;

    /**
     * field for EMAIL
     */
    protected java.lang.String localEMAIL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILTracker = false;

    public boolean isTABLE_NAMESpecified() {
        return localTABLE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTABLE_NAME() {
        return localTABLE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param TABLE_NAME
     */
    public void setTABLE_NAME(java.lang.String param) {
        localTABLE_NAMETracker = param != null;

        this.localTABLE_NAME = param;
    }

    public boolean isTABLE_IDSpecified() {
        return localTABLE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getTABLE_ID() {
        return localTABLE_ID;
    }

    /**
     * Auto generated setter method
     * @param param TABLE_ID
     */
    public void setTABLE_ID(long param) {
        // setting primitive attribute tracker to true
        localTABLE_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localTABLE_ID = param;
    }

    public boolean isTABLE_VISIBILITYSpecified() {
        return localTABLE_VISIBILITYTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.Visibility
     */
    public useractions.listmgmt.engageservice.Visibility getTABLE_VISIBILITY() {
        return localTABLE_VISIBILITY;
    }

    /**
     * Auto generated setter method
     * @param param TABLE_VISIBILITY
     */
    public void setTABLE_VISIBILITY(
        useractions.listmgmt.engageservice.Visibility param) {
        localTABLE_VISIBILITYTracker = param != null;

        this.localTABLE_VISIBILITY = param;
    }

    public boolean isDELETE_BEFORESpecified() {
        return localDELETE_BEFORETracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.DateTime3
     */
    public useractions.listmgmt.engageservice.DateTime3 getDELETE_BEFORE() {
        return localDELETE_BEFORE;
    }

    /**
     * Auto generated setter method
     * @param param DELETE_BEFORE
     */
    public void setDELETE_BEFORE(
        useractions.listmgmt.engageservice.DateTime3 param) {
        localDELETE_BEFORETracker = param != null;

        this.localDELETE_BEFORE = param;
    }

    public boolean isEMAILSpecified() {
        return localEMAILTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL() {
        return localEMAIL;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL
     */
    public void setEMAIL(java.lang.String param) {
        localEMAILTracker = param != null;

        this.localEMAIL = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.ListMgmt.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":PurgeTableRequestType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "PurgeTableRequestType", xmlWriter);
            }
        }

        if (localTABLE_NAMETracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "TABLE_NAME", xmlWriter);

            if (localTABLE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TABLE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTABLE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localTABLE_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "TABLE_ID", xmlWriter);

            if (localTABLE_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "TABLE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localTABLE_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localTABLE_VISIBILITYTracker) {
            if (localTABLE_VISIBILITY == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "TABLE_VISIBILITY cannot be null!!");
            }

            localTABLE_VISIBILITY.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "TABLE_VISIBILITY"), xmlWriter);
        }

        if (localDELETE_BEFORETracker) {
            if (localDELETE_BEFORE == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "DELETE_BEFORE cannot be null!!");
            }

            localDELETE_BEFORE.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "DELETE_BEFORE"), xmlWriter);
        }

        if (localEMAILTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "EMAIL", xmlWriter);

            if (localEMAIL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.ListMgmt.UserActions")) {
            return "ns5";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static PurgeTableRequestType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            PurgeTableRequestType object = new PurgeTableRequestType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"PurgeTableRequestType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (PurgeTableRequestType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "TABLE_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "TABLE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TABLE_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTABLE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "TABLE_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "TABLE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TABLE_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTABLE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setTABLE_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "TABLE_VISIBILITY").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "TABLE_VISIBILITY").equals(
                            reader.getName())) {
                    object.setTABLE_VISIBILITY(useractions.listmgmt.engageservice.Visibility.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "DELETE_BEFORE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "DELETE_BEFORE").equals(
                            reader.getName())) {
                    object.setDELETE_BEFORE(useractions.listmgmt.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "EMAIL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
