/**
 * FailureElementType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.listmgmt.engageservice;


/**
 *  FailureElementType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class FailureElementType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = FailureElementType
       Namespace URI = SilverpopApi:EngageService.ListMgmt.UserActions
       Namespace Prefix = ns5
     */

    /**
     * field for COLUMN
     * This was an Array!
     */
    protected useractions.listmgmt.engageservice.ColumnNameElementType[] localCOLUMN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOLUMNTracker = false;

    /**
     * field for KEY_COLUMN
     * This was an Array!
     */
    protected useractions.listmgmt.engageservice.ColumnNameElementType[] localKEY_COLUMN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localKEY_COLUMNTracker = false;

    /**
     * field for Failure_type
     * This was an Attribute!
     */
    protected java.lang.String localFailure_type;

    /**
     * field for Description
     * This was an Attribute!
     */
    protected java.lang.String localDescription;

    public boolean isCOLUMNSpecified() {
        return localCOLUMNTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.ColumnNameElementType[]
     */
    public useractions.listmgmt.engageservice.ColumnNameElementType[] getCOLUMN() {
        return localCOLUMN;
    }

    /**
     * validate the array for COLUMN
     */
    protected void validateCOLUMN(
        useractions.listmgmt.engageservice.ColumnNameElementType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param COLUMN
     */
    public void setCOLUMN(
        useractions.listmgmt.engageservice.ColumnNameElementType[] param) {
        validateCOLUMN(param);

        localCOLUMNTracker = param != null;

        this.localCOLUMN = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param useractions.listmgmt.engageservice.ColumnNameElementType
     */
    public void addCOLUMN(
        useractions.listmgmt.engageservice.ColumnNameElementType param) {
        if (localCOLUMN == null) {
            localCOLUMN = new useractions.listmgmt.engageservice.ColumnNameElementType[] {
                    
                };
        }

        //update the setting tracker
        localCOLUMNTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localCOLUMN);
        list.add(param);
        this.localCOLUMN = (useractions.listmgmt.engageservice.ColumnNameElementType[]) list.toArray(new useractions.listmgmt.engageservice.ColumnNameElementType[list.size()]);
    }

    public boolean isKEY_COLUMNSpecified() {
        return localKEY_COLUMNTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.ColumnNameElementType[]
     */
    public useractions.listmgmt.engageservice.ColumnNameElementType[] getKEY_COLUMN() {
        return localKEY_COLUMN;
    }

    /**
     * validate the array for KEY_COLUMN
     */
    protected void validateKEY_COLUMN(
        useractions.listmgmt.engageservice.ColumnNameElementType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param KEY_COLUMN
     */
    public void setKEY_COLUMN(
        useractions.listmgmt.engageservice.ColumnNameElementType[] param) {
        validateKEY_COLUMN(param);

        localKEY_COLUMNTracker = param != null;

        this.localKEY_COLUMN = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param useractions.listmgmt.engageservice.ColumnNameElementType
     */
    public void addKEY_COLUMN(
        useractions.listmgmt.engageservice.ColumnNameElementType param) {
        if (localKEY_COLUMN == null) {
            localKEY_COLUMN = new useractions.listmgmt.engageservice.ColumnNameElementType[] {
                    
                };
        }

        //update the setting tracker
        localKEY_COLUMNTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localKEY_COLUMN);
        list.add(param);
        this.localKEY_COLUMN = (useractions.listmgmt.engageservice.ColumnNameElementType[]) list.toArray(new useractions.listmgmt.engageservice.ColumnNameElementType[list.size()]);
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFailure_type() {
        return localFailure_type;
    }

    /**
     * Auto generated setter method
     * @param param Failure_type
     */
    public void setFailure_type(java.lang.String param) {
        this.localFailure_type = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getDescription() {
        return localDescription;
    }

    /**
     * Auto generated setter method
     * @param param Description
     */
    public void setDescription(java.lang.String param) {
        this.localDescription = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.ListMgmt.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":FailureElementType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "FailureElementType", xmlWriter);
            }
        }

        if (localFailure_type != null) {
            writeAttribute("", "failure_type",
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localFailure_type), xmlWriter);
        }
        else {
            throw new org.apache.axis2.databinding.ADBException(
                "required attribute localFailure_type is null");
        }

        if (localDescription != null) {
            writeAttribute("", "description",
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localDescription), xmlWriter);
        }
        else {
            throw new org.apache.axis2.databinding.ADBException(
                "required attribute localDescription is null");
        }

        if (localCOLUMNTracker) {
            if (localCOLUMN != null) {
                for (int i = 0; i < localCOLUMN.length; i++) {
                    if (localCOLUMN[i] != null) {
                        localCOLUMN[i].serialize(new javax.xml.namespace.QName(
                                "SilverpopApi:EngageService.ListMgmt.UserActions",
                                "COLUMN"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "COLUMN cannot be null!!");
            }
        }

        if (localKEY_COLUMNTracker) {
            if (localKEY_COLUMN != null) {
                for (int i = 0; i < localKEY_COLUMN.length; i++) {
                    if (localKEY_COLUMN[i] != null) {
                        localKEY_COLUMN[i].serialize(new javax.xml.namespace.QName(
                                "SilverpopApi:EngageService.ListMgmt.UserActions",
                                "KEY_COLUMN"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "KEY_COLUMN cannot be null!!");
            }
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.ListMgmt.UserActions")) {
            return "ns5";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static FailureElementType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            FailureElementType object = new FailureElementType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"FailureElementType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (FailureElementType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                // handle attribute "failure_type"
                java.lang.String tempAttribFailure_type = reader.getAttributeValue(null,
                        "failure_type");

                if (tempAttribFailure_type != null) {
                    java.lang.String content = tempAttribFailure_type;

                    object.setFailure_type(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            tempAttribFailure_type));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "Required attribute failure_type is missing");
                }

                handledAttributes.add("failure_type");

                // handle attribute "description"
                java.lang.String tempAttribDescription = reader.getAttributeValue(null,
                        "description");

                if (tempAttribDescription != null) {
                    java.lang.String content = tempAttribDescription;

                    object.setDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            tempAttribDescription));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "Required attribute description is missing");
                }

                handledAttributes.add("description");

                reader.next();

                java.util.ArrayList list1 = new java.util.ArrayList();

                java.util.ArrayList list2 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "COLUMN").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "COLUMN").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list1.add(useractions.listmgmt.engageservice.ColumnNameElementType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone1 = false;

                    while (!loopDone1) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone1 = true;
                        } else {
                            if (new javax.xml.namespace.QName(
                                        "SilverpopApi:EngageService.ListMgmt.UserActions",
                                        "COLUMN").equals(reader.getName())) {
                                list1.add(useractions.listmgmt.engageservice.ColumnNameElementType.Factory.parse(
                                        reader));
                            } else {
                                loopDone1 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setCOLUMN((useractions.listmgmt.engageservice.ColumnNameElementType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            useractions.listmgmt.engageservice.ColumnNameElementType.class,
                            list1));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "KEY_COLUMN").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "KEY_COLUMN").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list2.add(useractions.listmgmt.engageservice.ColumnNameElementType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone2 = false;

                    while (!loopDone2) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone2 = true;
                        } else {
                            if (new javax.xml.namespace.QName(
                                        "SilverpopApi:EngageService.ListMgmt.UserActions",
                                        "KEY_COLUMN").equals(reader.getName())) {
                                list2.add(useractions.listmgmt.engageservice.ColumnNameElementType.Factory.parse(
                                        reader));
                            } else {
                                loopDone2 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setKEY_COLUMN((useractions.listmgmt.engageservice.ColumnNameElementType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            useractions.listmgmt.engageservice.ColumnNameElementType.class,
                            list2));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
