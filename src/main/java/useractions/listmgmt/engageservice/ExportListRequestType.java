/**
 * ExportListRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.listmgmt.engageservice;


/**
 *  ExportListRequestType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ExportListRequestType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ExportListRequestType
       Namespace URI = SilverpopApi:EngageService.ListMgmt.UserActions
       Namespace Prefix = ns5
     */

    /**
     * field for LIST_ID
     */
    protected long localLIST_ID;

    /**
     * field for EMAIL
     */
    protected java.lang.String localEMAIL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILTracker = false;

    /**
     * field for EXPORT_TYPE
     */
    protected useractions.listmgmt.engageservice.ExportType localEXPORT_TYPE;

    /**
     * field for EXPORT_FORMAT
     */
    protected useractions.listmgmt.engageservice.ListTableExportFormat localEXPORT_FORMAT;

    /**
     * field for FILE_ENCODING
     */
    protected useractions.listmgmt.engageservice.FileEncoding localFILE_ENCODING;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFILE_ENCODINGTracker = false;

    /**
     * field for ADD_TO_STORED_FILES
     */
    protected java.lang.String localADD_TO_STORED_FILES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localADD_TO_STORED_FILESTracker = false;

    /**
     * field for DATE_START
     */
    protected useractions.listmgmt.engageservice.DateTime3 localDATE_START;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_STARTTracker = false;

    /**
     * field for DATE_END
     */
    protected useractions.listmgmt.engageservice.DateTime3 localDATE_END;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localDATE_ENDTracker = false;

    /**
     * Auto generated getter method
     * @return long
     */
    public long getLIST_ID() {
        return localLIST_ID;
    }

    /**
     * Auto generated setter method
     * @param param LIST_ID
     */
    public void setLIST_ID(long param) {
        this.localLIST_ID = param;
    }

    public boolean isEMAILSpecified() {
        return localEMAILTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL() {
        return localEMAIL;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL
     */
    public void setEMAIL(java.lang.String param) {
        localEMAILTracker = param != null;

        this.localEMAIL = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.ExportType
     */
    public useractions.listmgmt.engageservice.ExportType getEXPORT_TYPE() {
        return localEXPORT_TYPE;
    }

    /**
     * Auto generated setter method
     * @param param EXPORT_TYPE
     */
    public void setEXPORT_TYPE(
        useractions.listmgmt.engageservice.ExportType param) {
        this.localEXPORT_TYPE = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.ListTableExportFormat
     */
    public useractions.listmgmt.engageservice.ListTableExportFormat getEXPORT_FORMAT() {
        return localEXPORT_FORMAT;
    }

    /**
     * Auto generated setter method
     * @param param EXPORT_FORMAT
     */
    public void setEXPORT_FORMAT(
        useractions.listmgmt.engageservice.ListTableExportFormat param) {
        this.localEXPORT_FORMAT = param;
    }

    public boolean isFILE_ENCODINGSpecified() {
        return localFILE_ENCODINGTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.FileEncoding
     */
    public useractions.listmgmt.engageservice.FileEncoding getFILE_ENCODING() {
        return localFILE_ENCODING;
    }

    /**
     * Auto generated setter method
     * @param param FILE_ENCODING
     */
    public void setFILE_ENCODING(
        useractions.listmgmt.engageservice.FileEncoding param) {
        localFILE_ENCODINGTracker = param != null;

        this.localFILE_ENCODING = param;
    }

    public boolean isADD_TO_STORED_FILESSpecified() {
        return localADD_TO_STORED_FILESTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getADD_TO_STORED_FILES() {
        return localADD_TO_STORED_FILES;
    }

    /**
     * Auto generated setter method
     * @param param ADD_TO_STORED_FILES
     */
    public void setADD_TO_STORED_FILES(java.lang.String param) {
        localADD_TO_STORED_FILESTracker = param != null;

        this.localADD_TO_STORED_FILES = param;
    }

    public boolean isDATE_STARTSpecified() {
        return localDATE_STARTTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.DateTime3
     */
    public useractions.listmgmt.engageservice.DateTime3 getDATE_START() {
        return localDATE_START;
    }

    /**
     * Auto generated setter method
     * @param param DATE_START
     */
    public void setDATE_START(
        useractions.listmgmt.engageservice.DateTime3 param) {
        localDATE_STARTTracker = param != null;

        this.localDATE_START = param;
    }

    public boolean isDATE_ENDSpecified() {
        return localDATE_ENDTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.DateTime3
     */
    public useractions.listmgmt.engageservice.DateTime3 getDATE_END() {
        return localDATE_END;
    }

    /**
     * Auto generated setter method
     * @param param DATE_END
     */
    public void setDATE_END(useractions.listmgmt.engageservice.DateTime3 param) {
        localDATE_ENDTracker = param != null;

        this.localDATE_END = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.ListMgmt.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ExportListRequestType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ExportListRequestType", xmlWriter);
            }
        }

        namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
        writeStartElement(null, namespace, "LIST_ID", xmlWriter);

        if (localLIST_ID == java.lang.Long.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "LIST_ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localLIST_ID));
        }

        xmlWriter.writeEndElement();

        if (localEMAILTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "EMAIL", xmlWriter);

            if (localEMAIL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL);
            }

            xmlWriter.writeEndElement();
        }

        if (localEXPORT_TYPE == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "EXPORT_TYPE cannot be null!!");
        }

        localEXPORT_TYPE.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions", "EXPORT_TYPE"),
            xmlWriter);

        if (localEXPORT_FORMAT == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "EXPORT_FORMAT cannot be null!!");
        }

        localEXPORT_FORMAT.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions",
                "EXPORT_FORMAT"), xmlWriter);

        if (localFILE_ENCODINGTracker) {
            if (localFILE_ENCODING == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "FILE_ENCODING cannot be null!!");
            }

            localFILE_ENCODING.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "FILE_ENCODING"), xmlWriter);
        }

        if (localADD_TO_STORED_FILESTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "ADD_TO_STORED_FILES", xmlWriter);

            if (localADD_TO_STORED_FILES == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ADD_TO_STORED_FILES cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localADD_TO_STORED_FILES);
            }

            xmlWriter.writeEndElement();
        }

        if (localDATE_STARTTracker) {
            if (localDATE_START == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_START cannot be null!!");
            }

            localDATE_START.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "DATE_START"), xmlWriter);
        }

        if (localDATE_ENDTracker) {
            if (localDATE_END == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "DATE_END cannot be null!!");
            }

            localDATE_END.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "DATE_END"), xmlWriter);
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.ListMgmt.UserActions")) {
            return "ns5";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ExportListRequestType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ExportListRequestType object = new ExportListRequestType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ExportListRequestType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ExportListRequestType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "LIST_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LIST_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LIST_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLIST_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "EMAIL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "EXPORT_TYPE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXPORT_TYPE").equals(
                            reader.getName())) {
                    object.setEXPORT_TYPE(useractions.listmgmt.engageservice.ExportType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "EXPORT_FORMAT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EXPORT_FORMAT").equals(
                            reader.getName())) {
                    object.setEXPORT_FORMAT(useractions.listmgmt.engageservice.ListTableExportFormat.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "FILE_ENCODING").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FILE_ENCODING").equals(
                            reader.getName())) {
                    object.setFILE_ENCODING(useractions.listmgmt.engageservice.FileEncoding.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "ADD_TO_STORED_FILES").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ADD_TO_STORED_FILES").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ADD_TO_STORED_FILES" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setADD_TO_STORED_FILES(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "DATE_START").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_START").equals(
                            reader.getName())) {
                    object.setDATE_START(useractions.listmgmt.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "DATE_END").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "DATE_END").equals(
                            reader.getName())) {
                    object.setDATE_END(useractions.listmgmt.engageservice.DateTime3.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
