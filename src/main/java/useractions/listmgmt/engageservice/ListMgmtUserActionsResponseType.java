/**
 * ListMgmtUserActionsResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.listmgmt.engageservice;


/**
 *  ListMgmtUserActionsResponseType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ListMgmtUserActionsResponseType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ListMgmtUserActionsResponseType
       Namespace URI = SilverpopApi:EngageService.ListMgmt.UserActions
       Namespace Prefix = ns5
     */

    /**
     * field for SUCCESS
     */
    protected useractions.listmgmt.engageservice.ListMgmtUserSuccess localSUCCESS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUCCESSTracker = false;

    /**
     * field for Fault
     */
    protected useractions.listmgmt.engageservice.FaultType localFault;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFaultTracker = false;

    /**
     * field for JOB_ID
     */
    protected long localJOB_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localJOB_IDTracker = false;

    /**
     * field for FILE_PATH
     */
    protected java.lang.String localFILE_PATH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFILE_PATHTracker = false;

    /**
     * field for ID
     */
    protected long localID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localIDTracker = false;

    /**
     * field for NAME
     */
    protected java.lang.String localNAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNAMETracker = false;

    /**
     * field for TYPE
     */
    protected useractions.listmgmt.engageservice.ListTypeId localTYPE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTYPETracker = false;

    /**
     * field for SIZE
     */
    protected int localSIZE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSIZETracker = false;

    /**
     * field for NUM_OPT_OUTS
     */
    protected int localNUM_OPT_OUTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNUM_OPT_OUTSTracker = false;

    /**
     * field for NUM_UNDELIVERABLE
     */
    protected int localNUM_UNDELIVERABLE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localNUM_UNDELIVERABLETracker = false;

    /**
     * field for LAST_MODIFIED
     */
    protected useractions.listmgmt.engageservice.DateTime2 localLAST_MODIFIED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLAST_MODIFIEDTracker = false;

    /**
     * field for LAST_CONFIGURED
     */
    protected useractions.listmgmt.engageservice.DateTime2 localLAST_CONFIGURED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLAST_CONFIGUREDTracker = false;

    /**
     * field for CREATED
     */
    protected useractions.listmgmt.engageservice.DateTime2 localCREATED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCREATEDTracker = false;

    /**
     * field for VISIBILITY
     */
    protected useractions.listmgmt.engageservice.Visibility localVISIBILITY;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVISIBILITYTracker = false;

    /**
     * field for USER_ID
     */
    protected java.lang.String localUSER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localUSER_IDTracker = false;

    /**
     * field for ORGANIZATION_ID
     */
    protected java.lang.String localORGANIZATION_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localORGANIZATION_IDTracker = false;

    /**
     * field for OPT_IN_FORM_DEFINED
     */
    protected useractions.listmgmt.engageservice.True localOPT_IN_FORM_DEFINED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOPT_IN_FORM_DEFINEDTracker = false;

    /**
     * field for OPT_OUT_FORM_DEFINED
     */
    protected useractions.listmgmt.engageservice.True localOPT_OUT_FORM_DEFINED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOPT_OUT_FORM_DEFINEDTracker = false;

    /**
     * field for PROFILE_FORM_DEFINED
     */
    protected useractions.listmgmt.engageservice.True localPROFILE_FORM_DEFINED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPROFILE_FORM_DEFINEDTracker = false;

    /**
     * field for OPT_IN_AUTOREPLY_DEFINED
     */
    protected useractions.listmgmt.engageservice.True localOPT_IN_AUTOREPLY_DEFINED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOPT_IN_AUTOREPLY_DEFINEDTracker = false;

    /**
     * field for PROFILE_AUTOREPLY_DEFINED
     */
    protected useractions.listmgmt.engageservice.True localPROFILE_AUTOREPLY_DEFINED;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPROFILE_AUTOREPLY_DEFINEDTracker = false;

    /**
     * field for SMS_KEYWORD
     */
    protected java.lang.String localSMS_KEYWORD;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSMS_KEYWORDTracker = false;

    /**
     * field for COLUMNS
     */
    protected useractions.listmgmt.engageservice.ColumnsElementType localCOLUMNS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOLUMNSTracker = false;

    /**
     * field for SELECTION_VALUES
     */
    protected useractions.listmgmt.engageservice.SelectionValuesElementType localSELECTION_VALUES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSELECTION_VALUESTracker = false;

    /**
     * field for KEY_COLUMNS
     */
    protected useractions.listmgmt.engageservice.ColumnsElementType localKEY_COLUMNS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localKEY_COLUMNSTracker = false;

    /**
     * field for Mailing
     * This was an Array!
     */
    protected useractions.listmgmt.engageservice.MailingElementType[] localMailing;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMailingTracker = false;

    /**
     * field for LIST
     * This was an Array!
     */
    protected useractions.listmgmt.engageservice.ListElementType[] localLIST;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLISTTracker = false;

    /**
     * field for TABLE_ID
     */
    protected long localTABLE_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTABLE_IDTracker = false;

    /**
     * field for FAILURES
     */
    protected useractions.listmgmt.engageservice.FailuresElementType localFAILURES;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFAILURESTracker = false;

    /**
     * field for CONTACT_LIST_ID
     */
    protected long localCONTACT_LIST_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCONTACT_LIST_IDTracker = false;

    public boolean isSUCCESSSpecified() {
        return localSUCCESSTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.ListMgmtUserSuccess
     */
    public useractions.listmgmt.engageservice.ListMgmtUserSuccess getSUCCESS() {
        return localSUCCESS;
    }

    /**
     * Auto generated setter method
     * @param param SUCCESS
     */
    public void setSUCCESS(
        useractions.listmgmt.engageservice.ListMgmtUserSuccess param) {
        localSUCCESSTracker = param != null;

        this.localSUCCESS = param;
    }

    public boolean isFaultSpecified() {
        return localFaultTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.FaultType
     */
    public useractions.listmgmt.engageservice.FaultType getFault() {
        return localFault;
    }

    /**
     * Auto generated setter method
     * @param param Fault
     */
    public void setFault(useractions.listmgmt.engageservice.FaultType param) {
        localFaultTracker = param != null;

        this.localFault = param;
    }

    public boolean isJOB_IDSpecified() {
        return localJOB_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getJOB_ID() {
        return localJOB_ID;
    }

    /**
     * Auto generated setter method
     * @param param JOB_ID
     */
    public void setJOB_ID(long param) {
        // setting primitive attribute tracker to true
        localJOB_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localJOB_ID = param;
    }

    public boolean isFILE_PATHSpecified() {
        return localFILE_PATHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFILE_PATH() {
        return localFILE_PATH;
    }

    /**
     * Auto generated setter method
     * @param param FILE_PATH
     */
    public void setFILE_PATH(java.lang.String param) {
        localFILE_PATHTracker = param != null;

        this.localFILE_PATH = param;
    }

    public boolean isIDSpecified() {
        return localIDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getID() {
        return localID;
    }

    /**
     * Auto generated setter method
     * @param param ID
     */
    public void setID(long param) {
        // setting primitive attribute tracker to true
        localIDTracker = param != java.lang.Long.MIN_VALUE;

        this.localID = param;
    }

    public boolean isNAMESpecified() {
        return localNAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNAME() {
        return localNAME;
    }

    /**
     * Auto generated setter method
     * @param param NAME
     */
    public void setNAME(java.lang.String param) {
        localNAMETracker = param != null;

        this.localNAME = param;
    }

    public boolean isTYPESpecified() {
        return localTYPETracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.ListTypeId
     */
    public useractions.listmgmt.engageservice.ListTypeId getTYPE() {
        return localTYPE;
    }

    /**
     * Auto generated setter method
     * @param param TYPE
     */
    public void setTYPE(useractions.listmgmt.engageservice.ListTypeId param) {
        localTYPETracker = param != null;

        this.localTYPE = param;
    }

    public boolean isSIZESpecified() {
        return localSIZETracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getSIZE() {
        return localSIZE;
    }

    /**
     * Auto generated setter method
     * @param param SIZE
     */
    public void setSIZE(int param) {
        // setting primitive attribute tracker to true
        localSIZETracker = param != java.lang.Integer.MIN_VALUE;

        this.localSIZE = param;
    }

    public boolean isNUM_OPT_OUTSSpecified() {
        return localNUM_OPT_OUTSTracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNUM_OPT_OUTS() {
        return localNUM_OPT_OUTS;
    }

    /**
     * Auto generated setter method
     * @param param NUM_OPT_OUTS
     */
    public void setNUM_OPT_OUTS(int param) {
        // setting primitive attribute tracker to true
        localNUM_OPT_OUTSTracker = param != java.lang.Integer.MIN_VALUE;

        this.localNUM_OPT_OUTS = param;
    }

    public boolean isNUM_UNDELIVERABLESpecified() {
        return localNUM_UNDELIVERABLETracker;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNUM_UNDELIVERABLE() {
        return localNUM_UNDELIVERABLE;
    }

    /**
     * Auto generated setter method
     * @param param NUM_UNDELIVERABLE
     */
    public void setNUM_UNDELIVERABLE(int param) {
        // setting primitive attribute tracker to true
        localNUM_UNDELIVERABLETracker = param != java.lang.Integer.MIN_VALUE;

        this.localNUM_UNDELIVERABLE = param;
    }

    public boolean isLAST_MODIFIEDSpecified() {
        return localLAST_MODIFIEDTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.DateTime2
     */
    public useractions.listmgmt.engageservice.DateTime2 getLAST_MODIFIED() {
        return localLAST_MODIFIED;
    }

    /**
     * Auto generated setter method
     * @param param LAST_MODIFIED
     */
    public void setLAST_MODIFIED(
        useractions.listmgmt.engageservice.DateTime2 param) {
        localLAST_MODIFIEDTracker = param != null;

        this.localLAST_MODIFIED = param;
    }

    public boolean isLAST_CONFIGUREDSpecified() {
        return localLAST_CONFIGUREDTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.DateTime2
     */
    public useractions.listmgmt.engageservice.DateTime2 getLAST_CONFIGURED() {
        return localLAST_CONFIGURED;
    }

    /**
     * Auto generated setter method
     * @param param LAST_CONFIGURED
     */
    public void setLAST_CONFIGURED(
        useractions.listmgmt.engageservice.DateTime2 param) {
        localLAST_CONFIGUREDTracker = param != null;

        this.localLAST_CONFIGURED = param;
    }

    public boolean isCREATEDSpecified() {
        return localCREATEDTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.DateTime2
     */
    public useractions.listmgmt.engageservice.DateTime2 getCREATED() {
        return localCREATED;
    }

    /**
     * Auto generated setter method
     * @param param CREATED
     */
    public void setCREATED(useractions.listmgmt.engageservice.DateTime2 param) {
        localCREATEDTracker = param != null;

        this.localCREATED = param;
    }

    public boolean isVISIBILITYSpecified() {
        return localVISIBILITYTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.Visibility
     */
    public useractions.listmgmt.engageservice.Visibility getVISIBILITY() {
        return localVISIBILITY;
    }

    /**
     * Auto generated setter method
     * @param param VISIBILITY
     */
    public void setVISIBILITY(
        useractions.listmgmt.engageservice.Visibility param) {
        localVISIBILITYTracker = param != null;

        this.localVISIBILITY = param;
    }

    public boolean isUSER_IDSpecified() {
        return localUSER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUSER_ID() {
        return localUSER_ID;
    }

    /**
     * Auto generated setter method
     * @param param USER_ID
     */
    public void setUSER_ID(java.lang.String param) {
        localUSER_IDTracker = param != null;

        this.localUSER_ID = param;
    }

    public boolean isORGANIZATION_IDSpecified() {
        return localORGANIZATION_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getORGANIZATION_ID() {
        return localORGANIZATION_ID;
    }

    /**
     * Auto generated setter method
     * @param param ORGANIZATION_ID
     */
    public void setORGANIZATION_ID(java.lang.String param) {
        localORGANIZATION_IDTracker = param != null;

        this.localORGANIZATION_ID = param;
    }

    public boolean isOPT_IN_FORM_DEFINEDSpecified() {
        return localOPT_IN_FORM_DEFINEDTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.True
     */
    public useractions.listmgmt.engageservice.True getOPT_IN_FORM_DEFINED() {
        return localOPT_IN_FORM_DEFINED;
    }

    /**
     * Auto generated setter method
     * @param param OPT_IN_FORM_DEFINED
     */
    public void setOPT_IN_FORM_DEFINED(
        useractions.listmgmt.engageservice.True param) {
        localOPT_IN_FORM_DEFINEDTracker = param != null;

        this.localOPT_IN_FORM_DEFINED = param;
    }

    public boolean isOPT_OUT_FORM_DEFINEDSpecified() {
        return localOPT_OUT_FORM_DEFINEDTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.True
     */
    public useractions.listmgmt.engageservice.True getOPT_OUT_FORM_DEFINED() {
        return localOPT_OUT_FORM_DEFINED;
    }

    /**
     * Auto generated setter method
     * @param param OPT_OUT_FORM_DEFINED
     */
    public void setOPT_OUT_FORM_DEFINED(
        useractions.listmgmt.engageservice.True param) {
        localOPT_OUT_FORM_DEFINEDTracker = param != null;

        this.localOPT_OUT_FORM_DEFINED = param;
    }

    public boolean isPROFILE_FORM_DEFINEDSpecified() {
        return localPROFILE_FORM_DEFINEDTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.True
     */
    public useractions.listmgmt.engageservice.True getPROFILE_FORM_DEFINED() {
        return localPROFILE_FORM_DEFINED;
    }

    /**
     * Auto generated setter method
     * @param param PROFILE_FORM_DEFINED
     */
    public void setPROFILE_FORM_DEFINED(
        useractions.listmgmt.engageservice.True param) {
        localPROFILE_FORM_DEFINEDTracker = param != null;

        this.localPROFILE_FORM_DEFINED = param;
    }

    public boolean isOPT_IN_AUTOREPLY_DEFINEDSpecified() {
        return localOPT_IN_AUTOREPLY_DEFINEDTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.True
     */
    public useractions.listmgmt.engageservice.True getOPT_IN_AUTOREPLY_DEFINED() {
        return localOPT_IN_AUTOREPLY_DEFINED;
    }

    /**
     * Auto generated setter method
     * @param param OPT_IN_AUTOREPLY_DEFINED
     */
    public void setOPT_IN_AUTOREPLY_DEFINED(
        useractions.listmgmt.engageservice.True param) {
        localOPT_IN_AUTOREPLY_DEFINEDTracker = param != null;

        this.localOPT_IN_AUTOREPLY_DEFINED = param;
    }

    public boolean isPROFILE_AUTOREPLY_DEFINEDSpecified() {
        return localPROFILE_AUTOREPLY_DEFINEDTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.True
     */
    public useractions.listmgmt.engageservice.True getPROFILE_AUTOREPLY_DEFINED() {
        return localPROFILE_AUTOREPLY_DEFINED;
    }

    /**
     * Auto generated setter method
     * @param param PROFILE_AUTOREPLY_DEFINED
     */
    public void setPROFILE_AUTOREPLY_DEFINED(
        useractions.listmgmt.engageservice.True param) {
        localPROFILE_AUTOREPLY_DEFINEDTracker = param != null;

        this.localPROFILE_AUTOREPLY_DEFINED = param;
    }

    public boolean isSMS_KEYWORDSpecified() {
        return localSMS_KEYWORDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSMS_KEYWORD() {
        return localSMS_KEYWORD;
    }

    /**
     * Auto generated setter method
     * @param param SMS_KEYWORD
     */
    public void setSMS_KEYWORD(java.lang.String param) {
        localSMS_KEYWORDTracker = param != null;

        this.localSMS_KEYWORD = param;
    }

    public boolean isCOLUMNSSpecified() {
        return localCOLUMNSTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.ColumnsElementType
     */
    public useractions.listmgmt.engageservice.ColumnsElementType getCOLUMNS() {
        return localCOLUMNS;
    }

    /**
     * Auto generated setter method
     * @param param COLUMNS
     */
    public void setCOLUMNS(
        useractions.listmgmt.engageservice.ColumnsElementType param) {
        localCOLUMNSTracker = param != null;

        this.localCOLUMNS = param;
    }

    public boolean isSELECTION_VALUESSpecified() {
        return localSELECTION_VALUESTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.SelectionValuesElementType
     */
    public useractions.listmgmt.engageservice.SelectionValuesElementType getSELECTION_VALUES() {
        return localSELECTION_VALUES;
    }

    /**
     * Auto generated setter method
     * @param param SELECTION_VALUES
     */
    public void setSELECTION_VALUES(
        useractions.listmgmt.engageservice.SelectionValuesElementType param) {
        localSELECTION_VALUESTracker = param != null;

        this.localSELECTION_VALUES = param;
    }

    public boolean isKEY_COLUMNSSpecified() {
        return localKEY_COLUMNSTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.ColumnsElementType
     */
    public useractions.listmgmt.engageservice.ColumnsElementType getKEY_COLUMNS() {
        return localKEY_COLUMNS;
    }

    /**
     * Auto generated setter method
     * @param param KEY_COLUMNS
     */
    public void setKEY_COLUMNS(
        useractions.listmgmt.engageservice.ColumnsElementType param) {
        localKEY_COLUMNSTracker = param != null;

        this.localKEY_COLUMNS = param;
    }

    public boolean isMailingSpecified() {
        return localMailingTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.MailingElementType[]
     */
    public useractions.listmgmt.engageservice.MailingElementType[] getMailing() {
        return localMailing;
    }

    /**
     * validate the array for Mailing
     */
    protected void validateMailing(
        useractions.listmgmt.engageservice.MailingElementType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param Mailing
     */
    public void setMailing(
        useractions.listmgmt.engageservice.MailingElementType[] param) {
        validateMailing(param);

        localMailingTracker = param != null;

        this.localMailing = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param useractions.listmgmt.engageservice.MailingElementType
     */
    public void addMailing(
        useractions.listmgmt.engageservice.MailingElementType param) {
        if (localMailing == null) {
            localMailing = new useractions.listmgmt.engageservice.MailingElementType[] {
                    
                };
        }

        //update the setting tracker
        localMailingTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localMailing);
        list.add(param);
        this.localMailing = (useractions.listmgmt.engageservice.MailingElementType[]) list.toArray(new useractions.listmgmt.engageservice.MailingElementType[list.size()]);
    }

    public boolean isLISTSpecified() {
        return localLISTTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.ListElementType[]
     */
    public useractions.listmgmt.engageservice.ListElementType[] getLIST() {
        return localLIST;
    }

    /**
     * validate the array for LIST
     */
    protected void validateLIST(
        useractions.listmgmt.engageservice.ListElementType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param LIST
     */
    public void setLIST(
        useractions.listmgmt.engageservice.ListElementType[] param) {
        validateLIST(param);

        localLISTTracker = param != null;

        this.localLIST = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param useractions.listmgmt.engageservice.ListElementType
     */
    public void addLIST(
        useractions.listmgmt.engageservice.ListElementType param) {
        if (localLIST == null) {
            localLIST = new useractions.listmgmt.engageservice.ListElementType[] {
                    
                };
        }

        //update the setting tracker
        localLISTTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localLIST);
        list.add(param);
        this.localLIST = (useractions.listmgmt.engageservice.ListElementType[]) list.toArray(new useractions.listmgmt.engageservice.ListElementType[list.size()]);
    }

    public boolean isTABLE_IDSpecified() {
        return localTABLE_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getTABLE_ID() {
        return localTABLE_ID;
    }

    /**
     * Auto generated setter method
     * @param param TABLE_ID
     */
    public void setTABLE_ID(long param) {
        // setting primitive attribute tracker to true
        localTABLE_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localTABLE_ID = param;
    }

    public boolean isFAILURESSpecified() {
        return localFAILURESTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.FailuresElementType
     */
    public useractions.listmgmt.engageservice.FailuresElementType getFAILURES() {
        return localFAILURES;
    }

    /**
     * Auto generated setter method
     * @param param FAILURES
     */
    public void setFAILURES(
        useractions.listmgmt.engageservice.FailuresElementType param) {
        localFAILURESTracker = param != null;

        this.localFAILURES = param;
    }

    public boolean isCONTACT_LIST_IDSpecified() {
        return localCONTACT_LIST_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getCONTACT_LIST_ID() {
        return localCONTACT_LIST_ID;
    }

    /**
     * Auto generated setter method
     * @param param CONTACT_LIST_ID
     */
    public void setCONTACT_LIST_ID(long param) {
        // setting primitive attribute tracker to true
        localCONTACT_LIST_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localCONTACT_LIST_ID = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.ListMgmt.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ListMgmtUserActionsResponseType",
                    xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ListMgmtUserActionsResponseType", xmlWriter);
            }
        }

        if (localSUCCESSTracker) {
            if (localSUCCESS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SUCCESS cannot be null!!");
            }

            localSUCCESS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions", "SUCCESS"),
                xmlWriter);
        }

        if (localFaultTracker) {
            if (localFault == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "Fault cannot be null!!");
            }

            localFault.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions", "Fault"),
                xmlWriter);
        }

        if (localJOB_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "JOB_ID", xmlWriter);

            if (localJOB_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "JOB_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localJOB_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localFILE_PATHTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "FILE_PATH", xmlWriter);

            if (localFILE_PATH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FILE_PATH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFILE_PATH);
            }

            xmlWriter.writeEndElement();
        }

        if (localIDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "ID", xmlWriter);

            if (localID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localID));
            }

            xmlWriter.writeEndElement();
        }

        if (localNAMETracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "NAME", xmlWriter);

            if (localNAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localNAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localTYPETracker) {
            if (localTYPE == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "TYPE cannot be null!!");
            }

            localTYPE.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions", "TYPE"),
                xmlWriter);
        }

        if (localSIZETracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "SIZE", xmlWriter);

            if (localSIZE == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SIZE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localSIZE));
            }

            xmlWriter.writeEndElement();
        }

        if (localNUM_OPT_OUTSTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "NUM_OPT_OUTS", xmlWriter);

            if (localNUM_OPT_OUTS == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NUM_OPT_OUTS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNUM_OPT_OUTS));
            }

            xmlWriter.writeEndElement();
        }

        if (localNUM_UNDELIVERABLETracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "NUM_UNDELIVERABLE", xmlWriter);

            if (localNUM_UNDELIVERABLE == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "NUM_UNDELIVERABLE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localNUM_UNDELIVERABLE));
            }

            xmlWriter.writeEndElement();
        }

        if (localLAST_MODIFIEDTracker) {
            if (localLAST_MODIFIED == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "LAST_MODIFIED cannot be null!!");
            }

            localLAST_MODIFIED.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "LAST_MODIFIED"), xmlWriter);
        }

        if (localLAST_CONFIGUREDTracker) {
            if (localLAST_CONFIGURED == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "LAST_CONFIGURED cannot be null!!");
            }

            localLAST_CONFIGURED.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "LAST_CONFIGURED"), xmlWriter);
        }

        if (localCREATEDTracker) {
            if (localCREATED == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "CREATED cannot be null!!");
            }

            localCREATED.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions", "CREATED"),
                xmlWriter);
        }

        if (localVISIBILITYTracker) {
            if (localVISIBILITY == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "VISIBILITY cannot be null!!");
            }

            localVISIBILITY.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "VISIBILITY"), xmlWriter);
        }

        if (localUSER_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "USER_ID", xmlWriter);

            if (localUSER_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "USER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localUSER_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localORGANIZATION_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "ORGANIZATION_ID", xmlWriter);

            if (localORGANIZATION_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ORGANIZATION_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localORGANIZATION_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localOPT_IN_FORM_DEFINEDTracker) {
            if (localOPT_IN_FORM_DEFINED == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "OPT_IN_FORM_DEFINED cannot be null!!");
            }

            localOPT_IN_FORM_DEFINED.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "OPT_IN_FORM_DEFINED"), xmlWriter);
        }

        if (localOPT_OUT_FORM_DEFINEDTracker) {
            if (localOPT_OUT_FORM_DEFINED == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "OPT_OUT_FORM_DEFINED cannot be null!!");
            }

            localOPT_OUT_FORM_DEFINED.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "OPT_OUT_FORM_DEFINED"), xmlWriter);
        }

        if (localPROFILE_FORM_DEFINEDTracker) {
            if (localPROFILE_FORM_DEFINED == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "PROFILE_FORM_DEFINED cannot be null!!");
            }

            localPROFILE_FORM_DEFINED.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "PROFILE_FORM_DEFINED"), xmlWriter);
        }

        if (localOPT_IN_AUTOREPLY_DEFINEDTracker) {
            if (localOPT_IN_AUTOREPLY_DEFINED == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "OPT_IN_AUTOREPLY_DEFINED cannot be null!!");
            }

            localOPT_IN_AUTOREPLY_DEFINED.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "OPT_IN_AUTOREPLY_DEFINED"), xmlWriter);
        }

        if (localPROFILE_AUTOREPLY_DEFINEDTracker) {
            if (localPROFILE_AUTOREPLY_DEFINED == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "PROFILE_AUTOREPLY_DEFINED cannot be null!!");
            }

            localPROFILE_AUTOREPLY_DEFINED.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "PROFILE_AUTOREPLY_DEFINED"), xmlWriter);
        }

        if (localSMS_KEYWORDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "SMS_KEYWORD", xmlWriter);

            if (localSMS_KEYWORD == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SMS_KEYWORD cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSMS_KEYWORD);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOLUMNSTracker) {
            if (localCOLUMNS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "COLUMNS cannot be null!!");
            }

            localCOLUMNS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions", "COLUMNS"),
                xmlWriter);
        }

        if (localSELECTION_VALUESTracker) {
            if (localSELECTION_VALUES == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SELECTION_VALUES cannot be null!!");
            }

            localSELECTION_VALUES.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "SELECTION_VALUES"), xmlWriter);
        }

        if (localKEY_COLUMNSTracker) {
            if (localKEY_COLUMNS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "KEY_COLUMNS cannot be null!!");
            }

            localKEY_COLUMNS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "KEY_COLUMNS"), xmlWriter);
        }

        if (localMailingTracker) {
            if (localMailing != null) {
                for (int i = 0; i < localMailing.length; i++) {
                    if (localMailing[i] != null) {
                        localMailing[i].serialize(new javax.xml.namespace.QName(
                                "SilverpopApi:EngageService.ListMgmt.UserActions",
                                "Mailing"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "Mailing cannot be null!!");
            }
        }

        if (localLISTTracker) {
            if (localLIST != null) {
                for (int i = 0; i < localLIST.length; i++) {
                    if (localLIST[i] != null) {
                        localLIST[i].serialize(new javax.xml.namespace.QName(
                                "SilverpopApi:EngageService.ListMgmt.UserActions",
                                "LIST"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "LIST cannot be null!!");
            }
        }

        if (localTABLE_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "TABLE_ID", xmlWriter);

            if (localTABLE_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "TABLE_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localTABLE_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localFAILURESTracker) {
            if (localFAILURES == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "FAILURES cannot be null!!");
            }

            localFAILURES.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "FAILURES"), xmlWriter);
        }

        if (localCONTACT_LIST_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "CONTACT_LIST_ID", xmlWriter);

            if (localCONTACT_LIST_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "CONTACT_LIST_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localCONTACT_LIST_ID));
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.ListMgmt.UserActions")) {
            return "ns5";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ListMgmtUserActionsResponseType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ListMgmtUserActionsResponseType object = new ListMgmtUserActionsResponseType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ListMgmtUserActionsResponseType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ListMgmtUserActionsResponseType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                java.util.ArrayList list26 = new java.util.ArrayList();

                java.util.ArrayList list27 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "SUCCESS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUCCESS").equals(
                            reader.getName())) {
                    object.setSUCCESS(useractions.listmgmt.engageservice.ListMgmtUserSuccess.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "Fault").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "Fault").equals(
                            reader.getName())) {
                    object.setFault(useractions.listmgmt.engageservice.FaultType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "JOB_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "JOB_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "JOB_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setJOB_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setJOB_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "FILE_PATH").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FILE_PATH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FILE_PATH" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFILE_PATH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "TYPE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "TYPE").equals(
                            reader.getName())) {
                    object.setTYPE(useractions.listmgmt.engageservice.ListTypeId.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "SIZE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SIZE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SIZE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSIZE(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setSIZE(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "NUM_OPT_OUTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NUM_OPT_OUTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NUM_OPT_OUTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNUM_OPT_OUTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNUM_OPT_OUTS(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "NUM_UNDELIVERABLE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NUM_UNDELIVERABLE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NUM_UNDELIVERABLE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNUM_UNDELIVERABLE(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setNUM_UNDELIVERABLE(java.lang.Integer.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "LAST_MODIFIED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LAST_MODIFIED").equals(
                            reader.getName())) {
                    object.setLAST_MODIFIED(useractions.listmgmt.engageservice.DateTime2.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "LAST_CONFIGURED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LAST_CONFIGURED").equals(
                            reader.getName())) {
                    object.setLAST_CONFIGURED(useractions.listmgmt.engageservice.DateTime2.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "CREATED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CREATED").equals(
                            reader.getName())) {
                    object.setCREATED(useractions.listmgmt.engageservice.DateTime2.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "VISIBILITY").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "VISIBILITY").equals(
                            reader.getName())) {
                    object.setVISIBILITY(useractions.listmgmt.engageservice.Visibility.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "USER_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "USER_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "USER_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUSER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "ORGANIZATION_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ORGANIZATION_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ORGANIZATION_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setORGANIZATION_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "OPT_IN_FORM_DEFINED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "OPT_IN_FORM_DEFINED").equals(
                            reader.getName())) {
                    object.setOPT_IN_FORM_DEFINED(useractions.listmgmt.engageservice.True.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "OPT_OUT_FORM_DEFINED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "OPT_OUT_FORM_DEFINED").equals(
                            reader.getName())) {
                    object.setOPT_OUT_FORM_DEFINED(useractions.listmgmt.engageservice.True.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "PROFILE_FORM_DEFINED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "PROFILE_FORM_DEFINED").equals(
                            reader.getName())) {
                    object.setPROFILE_FORM_DEFINED(useractions.listmgmt.engageservice.True.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "OPT_IN_AUTOREPLY_DEFINED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "OPT_IN_AUTOREPLY_DEFINED").equals(reader.getName())) {
                    object.setOPT_IN_AUTOREPLY_DEFINED(useractions.listmgmt.engageservice.True.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "PROFILE_AUTOREPLY_DEFINED").equals(
                            reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "PROFILE_AUTOREPLY_DEFINED").equals(
                            reader.getName())) {
                    object.setPROFILE_AUTOREPLY_DEFINED(useractions.listmgmt.engageservice.True.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "SMS_KEYWORD").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SMS_KEYWORD").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SMS_KEYWORD" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSMS_KEYWORD(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "COLUMNS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "COLUMNS").equals(
                            reader.getName())) {
                    object.setCOLUMNS(useractions.listmgmt.engageservice.ColumnsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "SELECTION_VALUES").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SELECTION_VALUES").equals(
                            reader.getName())) {
                    object.setSELECTION_VALUES(useractions.listmgmt.engageservice.SelectionValuesElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "KEY_COLUMNS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "KEY_COLUMNS").equals(
                            reader.getName())) {
                    object.setKEY_COLUMNS(useractions.listmgmt.engageservice.ColumnsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "Mailing").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "Mailing").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list26.add(useractions.listmgmt.engageservice.MailingElementType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone26 = false;

                    while (!loopDone26) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone26 = true;
                        } else {
                            if (new javax.xml.namespace.QName(
                                        "SilverpopApi:EngageService.ListMgmt.UserActions",
                                        "Mailing").equals(reader.getName())) {
                                list26.add(useractions.listmgmt.engageservice.MailingElementType.Factory.parse(
                                        reader));
                            } else {
                                loopDone26 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setMailing((useractions.listmgmt.engageservice.MailingElementType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            useractions.listmgmt.engageservice.MailingElementType.class,
                            list26));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "LIST").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LIST").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list27.add(useractions.listmgmt.engageservice.ListElementType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone27 = false;

                    while (!loopDone27) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone27 = true;
                        } else {
                            if (new javax.xml.namespace.QName(
                                        "SilverpopApi:EngageService.ListMgmt.UserActions",
                                        "LIST").equals(reader.getName())) {
                                list27.add(useractions.listmgmt.engageservice.ListElementType.Factory.parse(
                                        reader));
                            } else {
                                loopDone27 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setLIST((useractions.listmgmt.engageservice.ListElementType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            useractions.listmgmt.engageservice.ListElementType.class,
                            list27));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "TABLE_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "TABLE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TABLE_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTABLE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setTABLE_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "FAILURES").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FAILURES").equals(
                            reader.getName())) {
                    object.setFAILURES(useractions.listmgmt.engageservice.FailuresElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "CONTACT_LIST_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CONTACT_LIST_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "CONTACT_LIST_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCONTACT_LIST_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setCONTACT_LIST_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
