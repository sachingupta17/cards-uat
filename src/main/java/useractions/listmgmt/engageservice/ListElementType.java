/**
 * ListElementType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.listmgmt.engageservice;


/**
 *  ListElementType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ListElementType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ListElementType
       Namespace URI = SilverpopApi:EngageService.ListMgmt.UserActions
       Namespace Prefix = ns5
     */

    /**
     * field for ID
     */
    protected long localID;

    /**
     * field for NAME
     */
    protected java.lang.String localNAME;

    /**
     * field for TYPE
     */
    protected useractions.listmgmt.engageservice.ListTypeId localTYPE;

    /**
     * field for SIZE
     */
    protected int localSIZE;

    /**
     * field for NUM_OPT_OUTS
     */
    protected int localNUM_OPT_OUTS;

    /**
     * field for NUM_UNDELIVERABLE
     */
    protected int localNUM_UNDELIVERABLE;

    /**
     * field for LAST_MODIFIED
     */
    protected useractions.listmgmt.engageservice.DateTime2 localLAST_MODIFIED;

    /**
     * field for VISIBILITY
     */
    protected useractions.listmgmt.engageservice.Visibility localVISIBILITY;

    /**
     * field for PARENT_NAME
     */
    protected java.lang.String localPARENT_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPARENT_NAMETracker = false;

    /**
     * field for USER_ID
     */
    protected java.lang.String localUSER_ID;

    /**
     * field for PARENT_FOLDER_ID
     */
    protected long localPARENT_FOLDER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPARENT_FOLDER_IDTracker = false;

    /**
     * field for IS_FOLDER
     */
    protected useractions.listmgmt.engageservice.True localIS_FOLDER;

    /**
     * field for FLAGGED_FOR_BACKUP
     */
    protected useractions.listmgmt.engageservice.True localFLAGGED_FOR_BACKUP;

    /**
     * field for SUPPRESSION_LIST_ID
     * This was an Array!
     */
    protected long[] localSUPPRESSION_LIST_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUPPRESSION_LIST_IDTracker = false;

    /**
     * Auto generated getter method
     * @return long
     */
    public long getID() {
        return localID;
    }

    /**
     * Auto generated setter method
     * @param param ID
     */
    public void setID(long param) {
        this.localID = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getNAME() {
        return localNAME;
    }

    /**
     * Auto generated setter method
     * @param param NAME
     */
    public void setNAME(java.lang.String param) {
        this.localNAME = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.ListTypeId
     */
    public useractions.listmgmt.engageservice.ListTypeId getTYPE() {
        return localTYPE;
    }

    /**
     * Auto generated setter method
     * @param param TYPE
     */
    public void setTYPE(useractions.listmgmt.engageservice.ListTypeId param) {
        this.localTYPE = param;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getSIZE() {
        return localSIZE;
    }

    /**
     * Auto generated setter method
     * @param param SIZE
     */
    public void setSIZE(int param) {
        this.localSIZE = param;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNUM_OPT_OUTS() {
        return localNUM_OPT_OUTS;
    }

    /**
     * Auto generated setter method
     * @param param NUM_OPT_OUTS
     */
    public void setNUM_OPT_OUTS(int param) {
        this.localNUM_OPT_OUTS = param;
    }

    /**
     * Auto generated getter method
     * @return int
     */
    public int getNUM_UNDELIVERABLE() {
        return localNUM_UNDELIVERABLE;
    }

    /**
     * Auto generated setter method
     * @param param NUM_UNDELIVERABLE
     */
    public void setNUM_UNDELIVERABLE(int param) {
        this.localNUM_UNDELIVERABLE = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.DateTime2
     */
    public useractions.listmgmt.engageservice.DateTime2 getLAST_MODIFIED() {
        return localLAST_MODIFIED;
    }

    /**
     * Auto generated setter method
     * @param param LAST_MODIFIED
     */
    public void setLAST_MODIFIED(
        useractions.listmgmt.engageservice.DateTime2 param) {
        this.localLAST_MODIFIED = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.Visibility
     */
    public useractions.listmgmt.engageservice.Visibility getVISIBILITY() {
        return localVISIBILITY;
    }

    /**
     * Auto generated setter method
     * @param param VISIBILITY
     */
    public void setVISIBILITY(
        useractions.listmgmt.engageservice.Visibility param) {
        this.localVISIBILITY = param;
    }

    public boolean isPARENT_NAMESpecified() {
        return localPARENT_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getPARENT_NAME() {
        return localPARENT_NAME;
    }

    /**
     * Auto generated setter method
     * @param param PARENT_NAME
     */
    public void setPARENT_NAME(java.lang.String param) {
        localPARENT_NAMETracker = param != null;

        this.localPARENT_NAME = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUSER_ID() {
        return localUSER_ID;
    }

    /**
     * Auto generated setter method
     * @param param USER_ID
     */
    public void setUSER_ID(java.lang.String param) {
        this.localUSER_ID = param;
    }

    public boolean isPARENT_FOLDER_IDSpecified() {
        return localPARENT_FOLDER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getPARENT_FOLDER_ID() {
        return localPARENT_FOLDER_ID;
    }

    /**
     * Auto generated setter method
     * @param param PARENT_FOLDER_ID
     */
    public void setPARENT_FOLDER_ID(long param) {
        // setting primitive attribute tracker to true
        localPARENT_FOLDER_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localPARENT_FOLDER_ID = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.True
     */
    public useractions.listmgmt.engageservice.True getIS_FOLDER() {
        return localIS_FOLDER;
    }

    /**
     * Auto generated setter method
     * @param param IS_FOLDER
     */
    public void setIS_FOLDER(useractions.listmgmt.engageservice.True param) {
        this.localIS_FOLDER = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.True
     */
    public useractions.listmgmt.engageservice.True getFLAGGED_FOR_BACKUP() {
        return localFLAGGED_FOR_BACKUP;
    }

    /**
     * Auto generated setter method
     * @param param FLAGGED_FOR_BACKUP
     */
    public void setFLAGGED_FOR_BACKUP(
        useractions.listmgmt.engageservice.True param) {
        this.localFLAGGED_FOR_BACKUP = param;
    }

    public boolean isSUPPRESSION_LIST_IDSpecified() {
        return localSUPPRESSION_LIST_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long[]
     */
    public long[] getSUPPRESSION_LIST_ID() {
        return localSUPPRESSION_LIST_ID;
    }

    /**
     * validate the array for SUPPRESSION_LIST_ID
     */
    protected void validateSUPPRESSION_LIST_ID(long[] param) {
    }

    /**
     * Auto generated setter method
     * @param param SUPPRESSION_LIST_ID
     */
    public void setSUPPRESSION_LIST_ID(long[] param) {
        validateSUPPRESSION_LIST_ID(param);

        localSUPPRESSION_LIST_IDTracker = param != null;

        this.localSUPPRESSION_LIST_ID = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.ListMgmt.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ListElementType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ListElementType", xmlWriter);
            }
        }

        namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
        writeStartElement(null, namespace, "ID", xmlWriter);

        if (localID == java.lang.Long.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localID));
        }

        xmlWriter.writeEndElement();

        namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
        writeStartElement(null, namespace, "NAME", xmlWriter);

        if (localNAME == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "NAME cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localNAME);
        }

        xmlWriter.writeEndElement();

        if (localTYPE == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "TYPE cannot be null!!");
        }

        localTYPE.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions", "TYPE"),
            xmlWriter);

        namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
        writeStartElement(null, namespace, "SIZE", xmlWriter);

        if (localSIZE == java.lang.Integer.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "SIZE cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localSIZE));
        }

        xmlWriter.writeEndElement();

        namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
        writeStartElement(null, namespace, "NUM_OPT_OUTS", xmlWriter);

        if (localNUM_OPT_OUTS == java.lang.Integer.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "NUM_OPT_OUTS cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localNUM_OPT_OUTS));
        }

        xmlWriter.writeEndElement();

        namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
        writeStartElement(null, namespace, "NUM_UNDELIVERABLE", xmlWriter);

        if (localNUM_UNDELIVERABLE == java.lang.Integer.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "NUM_UNDELIVERABLE cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localNUM_UNDELIVERABLE));
        }

        xmlWriter.writeEndElement();

        if (localLAST_MODIFIED == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "LAST_MODIFIED cannot be null!!");
        }

        localLAST_MODIFIED.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions",
                "LAST_MODIFIED"), xmlWriter);

        if (localVISIBILITY == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "VISIBILITY cannot be null!!");
        }

        localVISIBILITY.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions", "VISIBILITY"),
            xmlWriter);

        if (localPARENT_NAMETracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "PARENT_NAME", xmlWriter);

            if (localPARENT_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "PARENT_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localPARENT_NAME);
            }

            xmlWriter.writeEndElement();
        }

        namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
        writeStartElement(null, namespace, "USER_ID", xmlWriter);

        if (localUSER_ID == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "USER_ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localUSER_ID);
        }

        xmlWriter.writeEndElement();

        if (localPARENT_FOLDER_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "PARENT_FOLDER_ID", xmlWriter);

            if (localPARENT_FOLDER_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "PARENT_FOLDER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPARENT_FOLDER_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localIS_FOLDER == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "IS_FOLDER cannot be null!!");
        }

        localIS_FOLDER.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions", "IS_FOLDER"),
            xmlWriter);

        if (localFLAGGED_FOR_BACKUP == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "FLAGGED_FOR_BACKUP cannot be null!!");
        }

        localFLAGGED_FOR_BACKUP.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions",
                "FLAGGED_FOR_BACKUP"), xmlWriter);

        if (localSUPPRESSION_LIST_IDTracker) {
            if (localSUPPRESSION_LIST_ID != null) {
                namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";

                for (int i = 0; i < localSUPPRESSION_LIST_ID.length; i++) {
                    if (localSUPPRESSION_LIST_ID[i] != java.lang.Long.MIN_VALUE) {
                        writeStartElement(null, namespace,
                            "SUPPRESSION_LIST_ID", xmlWriter);

                        xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                localSUPPRESSION_LIST_ID[i]));
                        xmlWriter.writeEndElement();
                    } else {
                        // we have to do nothing since minOccurs is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "SUPPRESSION_LIST_ID cannot be null!!");
            }
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.ListMgmt.UserActions")) {
            return "ns5";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ListElementType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ListElementType object = new ListElementType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ListElementType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ListElementType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                java.util.ArrayList list14 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "TYPE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "TYPE").equals(
                            reader.getName())) {
                    object.setTYPE(useractions.listmgmt.engageservice.ListTypeId.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "SIZE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SIZE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SIZE" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSIZE(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "NUM_OPT_OUTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NUM_OPT_OUTS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NUM_OPT_OUTS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNUM_OPT_OUTS(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "NUM_UNDELIVERABLE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "NUM_UNDELIVERABLE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "NUM_UNDELIVERABLE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setNUM_UNDELIVERABLE(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "LAST_MODIFIED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LAST_MODIFIED").equals(
                            reader.getName())) {
                    object.setLAST_MODIFIED(useractions.listmgmt.engageservice.DateTime2.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "VISIBILITY").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "VISIBILITY").equals(
                            reader.getName())) {
                    object.setVISIBILITY(useractions.listmgmt.engageservice.Visibility.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "PARENT_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "PARENT_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PARENT_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPARENT_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "USER_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "USER_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "USER_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUSER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "PARENT_FOLDER_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "PARENT_FOLDER_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PARENT_FOLDER_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPARENT_FOLDER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setPARENT_FOLDER_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "IS_FOLDER").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "IS_FOLDER").equals(
                            reader.getName())) {
                    object.setIS_FOLDER(useractions.listmgmt.engageservice.True.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "FLAGGED_FOR_BACKUP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FLAGGED_FOR_BACKUP").equals(
                            reader.getName())) {
                    object.setFLAGGED_FOR_BACKUP(useractions.listmgmt.engageservice.True.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "SUPPRESSION_LIST_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUPPRESSION_LIST_ID").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list14.add(reader.getElementText());

                    //loop until we find a start element that is not part of this array
                    boolean loopDone14 = false;

                    while (!loopDone14) {
                        // Ensure we are at the EndElement
                        while (!reader.isEndElement()) {
                            reader.next();
                        }

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone14 = true;
                        } else {
                            if (new javax.xml.namespace.QName(
                                        "SilverpopApi:EngageService.ListMgmt.UserActions",
                                        "SUPPRESSION_LIST_ID").equals(
                                        reader.getName())) {
                                list14.add(reader.getElementText());
                            } else {
                                loopDone14 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setSUPPRESSION_LIST_ID((long[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            long.class, list14));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
