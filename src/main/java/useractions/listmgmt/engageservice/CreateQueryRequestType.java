/**
 * CreateQueryRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.listmgmt.engageservice;


/**
 *  CreateQueryRequestType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class CreateQueryRequestType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = CreateQueryRequestType
       Namespace URI = SilverpopApi:EngageService.ListMgmt.UserActions
       Namespace Prefix = ns5
     */

    /**
     * field for QUERY_NAME
     */
    protected java.lang.String localQUERY_NAME;

    /**
     * field for PARENT_LIST_ID
     */
    protected long localPARENT_LIST_ID;

    /**
     * field for VISIBILITY
     */
    protected useractions.listmgmt.engageservice.Visibility localVISIBILITY;

    /**
     * field for PARENT_FOLDER_ID
     */
    protected long localPARENT_FOLDER_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localPARENT_FOLDER_IDTracker = false;

    /**
     * field for ALLOW_FIELD_CHANGE
     */
    protected useractions.listmgmt.engageservice.AllowFieldChange localALLOW_FIELD_CHANGE;

    /**
     * field for SELECT_COLUMNS
     */
    protected java.lang.String localSELECT_COLUMNS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSELECT_COLUMNSTracker = false;

    /**
     * field for CRITERIA
     */
    protected useractions.listmgmt.engageservice.CriteriaElementType localCRITERIA;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCRITERIATracker = false;

    /**
     * field for BEHAVIOR
     */
    protected useractions.listmgmt.engageservice.BehaviorElementType localBEHAVIOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localBEHAVIORTracker = false;

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getQUERY_NAME() {
        return localQUERY_NAME;
    }

    /**
     * Auto generated setter method
     * @param param QUERY_NAME
     */
    public void setQUERY_NAME(java.lang.String param) {
        this.localQUERY_NAME = param;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getPARENT_LIST_ID() {
        return localPARENT_LIST_ID;
    }

    /**
     * Auto generated setter method
     * @param param PARENT_LIST_ID
     */
    public void setPARENT_LIST_ID(long param) {
        this.localPARENT_LIST_ID = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.Visibility
     */
    public useractions.listmgmt.engageservice.Visibility getVISIBILITY() {
        return localVISIBILITY;
    }

    /**
     * Auto generated setter method
     * @param param VISIBILITY
     */
    public void setVISIBILITY(
        useractions.listmgmt.engageservice.Visibility param) {
        this.localVISIBILITY = param;
    }

    public boolean isPARENT_FOLDER_IDSpecified() {
        return localPARENT_FOLDER_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getPARENT_FOLDER_ID() {
        return localPARENT_FOLDER_ID;
    }

    /**
     * Auto generated setter method
     * @param param PARENT_FOLDER_ID
     */
    public void setPARENT_FOLDER_ID(long param) {
        // setting primitive attribute tracker to true
        localPARENT_FOLDER_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localPARENT_FOLDER_ID = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.AllowFieldChange
     */
    public useractions.listmgmt.engageservice.AllowFieldChange getALLOW_FIELD_CHANGE() {
        return localALLOW_FIELD_CHANGE;
    }

    /**
     * Auto generated setter method
     * @param param ALLOW_FIELD_CHANGE
     */
    public void setALLOW_FIELD_CHANGE(
        useractions.listmgmt.engageservice.AllowFieldChange param) {
        this.localALLOW_FIELD_CHANGE = param;
    }

    public boolean isSELECT_COLUMNSSpecified() {
        return localSELECT_COLUMNSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSELECT_COLUMNS() {
        return localSELECT_COLUMNS;
    }

    /**
     * Auto generated setter method
     * @param param SELECT_COLUMNS
     */
    public void setSELECT_COLUMNS(java.lang.String param) {
        localSELECT_COLUMNSTracker = param != null;

        this.localSELECT_COLUMNS = param;
    }

    public boolean isCRITERIASpecified() {
        return localCRITERIATracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.CriteriaElementType
     */
    public useractions.listmgmt.engageservice.CriteriaElementType getCRITERIA() {
        return localCRITERIA;
    }

    /**
     * Auto generated setter method
     * @param param CRITERIA
     */
    public void setCRITERIA(
        useractions.listmgmt.engageservice.CriteriaElementType param) {
        localCRITERIATracker = param != null;

        this.localCRITERIA = param;
    }

    public boolean isBEHAVIORSpecified() {
        return localBEHAVIORTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.listmgmt.engageservice.BehaviorElementType
     */
    public useractions.listmgmt.engageservice.BehaviorElementType getBEHAVIOR() {
        return localBEHAVIOR;
    }

    /**
     * Auto generated setter method
     * @param param BEHAVIOR
     */
    public void setBEHAVIOR(
        useractions.listmgmt.engageservice.BehaviorElementType param) {
        localBEHAVIORTracker = param != null;

        this.localBEHAVIOR = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.ListMgmt.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":CreateQueryRequestType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "CreateQueryRequestType", xmlWriter);
            }
        }

        namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
        writeStartElement(null, namespace, "QUERY_NAME", xmlWriter);

        if (localQUERY_NAME == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "QUERY_NAME cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localQUERY_NAME);
        }

        xmlWriter.writeEndElement();

        namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
        writeStartElement(null, namespace, "PARENT_LIST_ID", xmlWriter);

        if (localPARENT_LIST_ID == java.lang.Long.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "PARENT_LIST_ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localPARENT_LIST_ID));
        }

        xmlWriter.writeEndElement();

        if (localVISIBILITY == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "VISIBILITY cannot be null!!");
        }

        localVISIBILITY.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions", "VISIBILITY"),
            xmlWriter);

        if (localPARENT_FOLDER_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "PARENT_FOLDER_ID", xmlWriter);

            if (localPARENT_FOLDER_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "PARENT_FOLDER_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPARENT_FOLDER_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localALLOW_FIELD_CHANGE == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "ALLOW_FIELD_CHANGE cannot be null!!");
        }

        localALLOW_FIELD_CHANGE.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.ListMgmt.UserActions",
                "ALLOW_FIELD_CHANGE"), xmlWriter);

        if (localSELECT_COLUMNSTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.UserActions";
            writeStartElement(null, namespace, "SELECT_COLUMNS", xmlWriter);

            if (localSELECT_COLUMNS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SELECT_COLUMNS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSELECT_COLUMNS);
            }

            xmlWriter.writeEndElement();
        }

        if (localCRITERIATracker) {
            if (localCRITERIA == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "CRITERIA cannot be null!!");
            }

            localCRITERIA.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "CRITERIA"), xmlWriter);
        }

        if (localBEHAVIORTracker) {
            if (localBEHAVIOR == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "BEHAVIOR cannot be null!!");
            }

            localBEHAVIOR.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.UserActions",
                    "BEHAVIOR"), xmlWriter);
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals("SilverpopApi:EngageService.ListMgmt.UserActions")) {
            return "ns5";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static CreateQueryRequestType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            CreateQueryRequestType object = new CreateQueryRequestType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"CreateQueryRequestType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (CreateQueryRequestType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "QUERY_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "QUERY_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "QUERY_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setQUERY_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "PARENT_LIST_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "PARENT_LIST_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PARENT_LIST_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPARENT_LIST_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "VISIBILITY").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "VISIBILITY").equals(
                            reader.getName())) {
                    object.setVISIBILITY(useractions.listmgmt.engageservice.Visibility.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "PARENT_FOLDER_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "PARENT_FOLDER_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "PARENT_FOLDER_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setPARENT_FOLDER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setPARENT_FOLDER_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "ALLOW_FIELD_CHANGE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ALLOW_FIELD_CHANGE").equals(
                            reader.getName())) {
                    object.setALLOW_FIELD_CHANGE(useractions.listmgmt.engageservice.AllowFieldChange.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "SELECT_COLUMNS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SELECT_COLUMNS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SELECT_COLUMNS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSELECT_COLUMNS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "CRITERIA").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CRITERIA").equals(
                            reader.getName())) {
                    object.setCRITERIA(useractions.listmgmt.engageservice.CriteriaElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.UserActions",
                            "BEHAVIOR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "BEHAVIOR").equals(
                            reader.getName())) {
                    object.setBEHAVIOR(useractions.listmgmt.engageservice.BehaviorElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
