/**
 * ScheduleMailingRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.mailmanagement.engageservice;


/**
 *  ScheduleMailingRequestType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ScheduleMailingRequestType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ScheduleMailingRequestType
       Namespace URI = SilverpopApi:EngageService.MailManagement.UserActions
       Namespace Prefix = ns7
     */

    /**
     * field for TEMPLATE_ID
     */
    protected long localTEMPLATE_ID;

    /**
     * field for LIST_ID
     */
    protected long localLIST_ID;

    /**
     * field for MAILING_NAME
     */
    protected java.lang.String localMAILING_NAME;

    /**
     * field for SEND_HTML
     */
    protected java.lang.String localSEND_HTML;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSEND_HTMLTracker = false;

    /**
     * field for SEND_AOL
     */
    protected java.lang.String localSEND_AOL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSEND_AOLTracker = false;

    /**
     * field for SEND_TEXT
     */
    protected java.lang.String localSEND_TEXT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSEND_TEXTTracker = false;

    /**
     * field for SUBJECT
     */
    protected java.lang.String localSUBJECT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUBJECTTracker = false;

    /**
     * field for FROM_NAME
     */
    protected java.lang.String localFROM_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFROM_NAMETracker = false;

    /**
     * field for FROM_ADDRESS
     */
    protected java.lang.String localFROM_ADDRESS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFROM_ADDRESSTracker = false;

    /**
     * field for REPLY_TO
     */
    protected java.lang.String localREPLY_TO;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localREPLY_TOTracker = false;

    /**
     * field for VISIBILITY
     */
    protected useractions.mailmanagement.engageservice.Visibility localVISIBILITY;

    /**
     * field for SCHEDULED
     */
    protected java.lang.String localSCHEDULED;

    /**
     * field for INBOX_MONITOR
     */
    protected java.lang.String localINBOX_MONITOR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localINBOX_MONITORTracker = false;

    /**
     * field for SEND_TIME_OPTIMIZATION
     */
    protected useractions.mailmanagement.engageservice.SendTimeOptimization localSEND_TIME_OPTIMIZATION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSEND_TIME_OPTIMIZATIONTracker = false;

    /**
     * field for WA_MAILINGLEVEL_CODE
     */
    protected java.lang.String localWA_MAILINGLEVEL_CODE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localWA_MAILINGLEVEL_CODETracker = false;

    /**
     * field for SUPPRESSION_LISTS
     */
    protected useractions.mailmanagement.engageservice.SuppressionListsElementType localSUPPRESSION_LISTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUPPRESSION_LISTSTracker = false;

    /**
     * field for SUBSTITUTIONS
     */
    protected useractions.mailmanagement.engageservice.SubstitutionsElementType localSUBSTITUTIONS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUBSTITUTIONSTracker = false;

    /**
     * Auto generated getter method
     * @return long
     */
    public long getTEMPLATE_ID() {
        return localTEMPLATE_ID;
    }

    /**
     * Auto generated setter method
     * @param param TEMPLATE_ID
     */
    public void setTEMPLATE_ID(long param) {
        this.localTEMPLATE_ID = param;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getLIST_ID() {
        return localLIST_ID;
    }

    /**
     * Auto generated setter method
     * @param param LIST_ID
     */
    public void setLIST_ID(long param) {
        this.localLIST_ID = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMAILING_NAME() {
        return localMAILING_NAME;
    }

    /**
     * Auto generated setter method
     * @param param MAILING_NAME
     */
    public void setMAILING_NAME(java.lang.String param) {
        this.localMAILING_NAME = param;
    }

    public boolean isSEND_HTMLSpecified() {
        return localSEND_HTMLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSEND_HTML() {
        return localSEND_HTML;
    }

    /**
     * Auto generated setter method
     * @param param SEND_HTML
     */
    public void setSEND_HTML(java.lang.String param) {
        localSEND_HTMLTracker = param != null;

        this.localSEND_HTML = param;
    }

    public boolean isSEND_AOLSpecified() {
        return localSEND_AOLTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSEND_AOL() {
        return localSEND_AOL;
    }

    /**
     * Auto generated setter method
     * @param param SEND_AOL
     */
    public void setSEND_AOL(java.lang.String param) {
        localSEND_AOLTracker = param != null;

        this.localSEND_AOL = param;
    }

    public boolean isSEND_TEXTSpecified() {
        return localSEND_TEXTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSEND_TEXT() {
        return localSEND_TEXT;
    }

    /**
     * Auto generated setter method
     * @param param SEND_TEXT
     */
    public void setSEND_TEXT(java.lang.String param) {
        localSEND_TEXTTracker = param != null;

        this.localSEND_TEXT = param;
    }

    public boolean isSUBJECTSpecified() {
        return localSUBJECTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUBJECT() {
        return localSUBJECT;
    }

    /**
     * Auto generated setter method
     * @param param SUBJECT
     */
    public void setSUBJECT(java.lang.String param) {
        localSUBJECTTracker = param != null;

        this.localSUBJECT = param;
    }

    public boolean isFROM_NAMESpecified() {
        return localFROM_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFROM_NAME() {
        return localFROM_NAME;
    }

    /**
     * Auto generated setter method
     * @param param FROM_NAME
     */
    public void setFROM_NAME(java.lang.String param) {
        localFROM_NAMETracker = param != null;

        this.localFROM_NAME = param;
    }

    public boolean isFROM_ADDRESSSpecified() {
        return localFROM_ADDRESSTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFROM_ADDRESS() {
        return localFROM_ADDRESS;
    }

    /**
     * Auto generated setter method
     * @param param FROM_ADDRESS
     */
    public void setFROM_ADDRESS(java.lang.String param) {
        localFROM_ADDRESSTracker = param != null;

        this.localFROM_ADDRESS = param;
    }

    public boolean isREPLY_TOSpecified() {
        return localREPLY_TOTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getREPLY_TO() {
        return localREPLY_TO;
    }

    /**
     * Auto generated setter method
     * @param param REPLY_TO
     */
    public void setREPLY_TO(java.lang.String param) {
        localREPLY_TOTracker = param != null;

        this.localREPLY_TO = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.Visibility
     */
    public useractions.mailmanagement.engageservice.Visibility getVISIBILITY() {
        return localVISIBILITY;
    }

    /**
     * Auto generated setter method
     * @param param VISIBILITY
     */
    public void setVISIBILITY(
        useractions.mailmanagement.engageservice.Visibility param) {
        this.localVISIBILITY = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSCHEDULED() {
        return localSCHEDULED;
    }

    /**
     * Auto generated setter method
     * @param param SCHEDULED
     */
    public void setSCHEDULED(java.lang.String param) {
        this.localSCHEDULED = param;
    }

    public boolean isINBOX_MONITORSpecified() {
        return localINBOX_MONITORTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getINBOX_MONITOR() {
        return localINBOX_MONITOR;
    }

    /**
     * Auto generated setter method
     * @param param INBOX_MONITOR
     */
    public void setINBOX_MONITOR(java.lang.String param) {
        localINBOX_MONITORTracker = param != null;

        this.localINBOX_MONITOR = param;
    }

    public boolean isSEND_TIME_OPTIMIZATIONSpecified() {
        return localSEND_TIME_OPTIMIZATIONTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.SendTimeOptimization
     */
    public useractions.mailmanagement.engageservice.SendTimeOptimization getSEND_TIME_OPTIMIZATION() {
        return localSEND_TIME_OPTIMIZATION;
    }

    /**
     * Auto generated setter method
     * @param param SEND_TIME_OPTIMIZATION
     */
    public void setSEND_TIME_OPTIMIZATION(
        useractions.mailmanagement.engageservice.SendTimeOptimization param) {
        localSEND_TIME_OPTIMIZATIONTracker = param != null;

        this.localSEND_TIME_OPTIMIZATION = param;
    }

    public boolean isWA_MAILINGLEVEL_CODESpecified() {
        return localWA_MAILINGLEVEL_CODETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getWA_MAILINGLEVEL_CODE() {
        return localWA_MAILINGLEVEL_CODE;
    }

    /**
     * Auto generated setter method
     * @param param WA_MAILINGLEVEL_CODE
     */
    public void setWA_MAILINGLEVEL_CODE(java.lang.String param) {
        localWA_MAILINGLEVEL_CODETracker = param != null;

        this.localWA_MAILINGLEVEL_CODE = param;
    }

    public boolean isSUPPRESSION_LISTSSpecified() {
        return localSUPPRESSION_LISTSTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.SuppressionListsElementType
     */
    public useractions.mailmanagement.engageservice.SuppressionListsElementType getSUPPRESSION_LISTS() {
        return localSUPPRESSION_LISTS;
    }

    /**
     * Auto generated setter method
     * @param param SUPPRESSION_LISTS
     */
    public void setSUPPRESSION_LISTS(
        useractions.mailmanagement.engageservice.SuppressionListsElementType param) {
        localSUPPRESSION_LISTSTracker = param != null;

        this.localSUPPRESSION_LISTS = param;
    }

    public boolean isSUBSTITUTIONSSpecified() {
        return localSUBSTITUTIONSTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.SubstitutionsElementType
     */
    public useractions.mailmanagement.engageservice.SubstitutionsElementType getSUBSTITUTIONS() {
        return localSUBSTITUTIONS;
    }

    /**
     * Auto generated setter method
     * @param param SUBSTITUTIONS
     */
    public void setSUBSTITUTIONS(
        useractions.mailmanagement.engageservice.SubstitutionsElementType param) {
        localSUBSTITUTIONSTracker = param != null;

        this.localSUBSTITUTIONS = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.MailManagement.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ScheduleMailingRequestType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ScheduleMailingRequestType", xmlWriter);
            }
        }

        namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
        writeStartElement(null, namespace, "TEMPLATE_ID", xmlWriter);

        if (localTEMPLATE_ID == java.lang.Long.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "TEMPLATE_ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localTEMPLATE_ID));
        }

        xmlWriter.writeEndElement();

        namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
        writeStartElement(null, namespace, "LIST_ID", xmlWriter);

        if (localLIST_ID == java.lang.Long.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "LIST_ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localLIST_ID));
        }

        xmlWriter.writeEndElement();

        namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
        writeStartElement(null, namespace, "MAILING_NAME", xmlWriter);

        if (localMAILING_NAME == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "MAILING_NAME cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localMAILING_NAME);
        }

        xmlWriter.writeEndElement();

        if (localSEND_HTMLTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "SEND_HTML", xmlWriter);

            if (localSEND_HTML == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SEND_HTML cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSEND_HTML);
            }

            xmlWriter.writeEndElement();
        }

        if (localSEND_AOLTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "SEND_AOL", xmlWriter);

            if (localSEND_AOL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SEND_AOL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSEND_AOL);
            }

            xmlWriter.writeEndElement();
        }

        if (localSEND_TEXTTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "SEND_TEXT", xmlWriter);

            if (localSEND_TEXT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SEND_TEXT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSEND_TEXT);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUBJECTTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "SUBJECT", xmlWriter);

            if (localSUBJECT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUBJECT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUBJECT);
            }

            xmlWriter.writeEndElement();
        }

        if (localFROM_NAMETracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "FROM_NAME", xmlWriter);

            if (localFROM_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FROM_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFROM_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localFROM_ADDRESSTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "FROM_ADDRESS", xmlWriter);

            if (localFROM_ADDRESS == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FROM_ADDRESS cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFROM_ADDRESS);
            }

            xmlWriter.writeEndElement();
        }

        if (localREPLY_TOTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "REPLY_TO", xmlWriter);

            if (localREPLY_TO == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "REPLY_TO cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localREPLY_TO);
            }

            xmlWriter.writeEndElement();
        }

        if (localVISIBILITY == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "VISIBILITY cannot be null!!");
        }

        localVISIBILITY.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.MailManagement.UserActions",
                "VISIBILITY"), xmlWriter);

        namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
        writeStartElement(null, namespace, "SCHEDULED", xmlWriter);

        if (localSCHEDULED == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "SCHEDULED cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localSCHEDULED);
        }

        xmlWriter.writeEndElement();

        if (localINBOX_MONITORTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "INBOX_MONITOR", xmlWriter);

            if (localINBOX_MONITOR == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "INBOX_MONITOR cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localINBOX_MONITOR);
            }

            xmlWriter.writeEndElement();
        }

        if (localSEND_TIME_OPTIMIZATIONTracker) {
            if (localSEND_TIME_OPTIMIZATION == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SEND_TIME_OPTIMIZATION cannot be null!!");
            }

            localSEND_TIME_OPTIMIZATION.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.MailManagement.UserActions",
                    "SEND_TIME_OPTIMIZATION"), xmlWriter);
        }

        if (localWA_MAILINGLEVEL_CODETracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "WA_MAILINGLEVEL_CODE", xmlWriter);

            if (localWA_MAILINGLEVEL_CODE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "WA_MAILINGLEVEL_CODE cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localWA_MAILINGLEVEL_CODE);
            }

            xmlWriter.writeEndElement();
        }

        if (localSUPPRESSION_LISTSTracker) {
            if (localSUPPRESSION_LISTS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SUPPRESSION_LISTS cannot be null!!");
            }

            localSUPPRESSION_LISTS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.MailManagement.UserActions",
                    "SUPPRESSION_LISTS"), xmlWriter);
        }

        if (localSUBSTITUTIONSTracker) {
            if (localSUBSTITUTIONS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SUBSTITUTIONS cannot be null!!");
            }

            localSUBSTITUTIONS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.MailManagement.UserActions",
                    "SUBSTITUTIONS"), xmlWriter);
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals(
                    "SilverpopApi:EngageService.MailManagement.UserActions")) {
            return "ns7";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ScheduleMailingRequestType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ScheduleMailingRequestType object = new ScheduleMailingRequestType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ScheduleMailingRequestType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ScheduleMailingRequestType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "TEMPLATE_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "TEMPLATE_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TEMPLATE_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTEMPLATE_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "LIST_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LIST_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LIST_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLIST_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "MAILING_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAILING_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MAILING_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMAILING_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SEND_HTML").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SEND_HTML").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SEND_HTML" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSEND_HTML(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SEND_AOL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SEND_AOL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SEND_AOL" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSEND_AOL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SEND_TEXT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SEND_TEXT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SEND_TEXT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSEND_TEXT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SUBJECT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUBJECT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUBJECT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUBJECT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "FROM_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FROM_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FROM_NAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFROM_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "FROM_ADDRESS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FROM_ADDRESS").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FROM_ADDRESS" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFROM_ADDRESS(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "REPLY_TO").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "REPLY_TO").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "REPLY_TO" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setREPLY_TO(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "VISIBILITY").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "VISIBILITY").equals(
                            reader.getName())) {
                    object.setVISIBILITY(useractions.mailmanagement.engageservice.Visibility.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SCHEDULED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SCHEDULED").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SCHEDULED" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSCHEDULED(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "INBOX_MONITOR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "INBOX_MONITOR").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "INBOX_MONITOR" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setINBOX_MONITOR(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SEND_TIME_OPTIMIZATION").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("",
                            "SEND_TIME_OPTIMIZATION").equals(reader.getName())) {
                    object.setSEND_TIME_OPTIMIZATION(useractions.mailmanagement.engageservice.SendTimeOptimization.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "WA_MAILINGLEVEL_CODE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "WA_MAILINGLEVEL_CODE").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "WA_MAILINGLEVEL_CODE" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setWA_MAILINGLEVEL_CODE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SUPPRESSION_LISTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUPPRESSION_LISTS").equals(
                            reader.getName())) {
                    object.setSUPPRESSION_LISTS(useractions.mailmanagement.engageservice.SuppressionListsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SUBSTITUTIONS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUBSTITUTIONS").equals(
                            reader.getName())) {
                    object.setSUBSTITUTIONS(useractions.mailmanagement.engageservice.SubstitutionsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
