/**
 * ExpressionElementType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.mailmanagement.engageservice;


/**
 *  ExpressionElementType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ExpressionElementType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ExpressionElementType
       Namespace URI = SilverpopApi:EngageService.MailManagement.UserActions
       Namespace Prefix = ns7
     */

    /**
     * field for AND_OR
     */
    protected useractions.mailmanagement.engageservice.AndOr localAND_OR;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAND_ORTracker = false;

    /**
     * field for LEFT_PARENS
     */
    protected useractions.mailmanagement.engageservice.LeftParens localLEFT_PARENS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLEFT_PARENSTracker = false;

    /**
     * field for TYPE
     */
    protected useractions.mailmanagement.engageservice.ExpressionType localTYPE;

    /**
     * field for COLUMN_NAME
     */
    protected java.lang.String localCOLUMN_NAME;

    /**
     * field for OPERATORS
     */
    protected useractions.mailmanagement.engageservice.Operators localOPERATORS;

    /**
     * field for VALUES
     */
    protected java.lang.String localVALUES;

    /**
     * field for RIGHT_PARENS
     */
    protected useractions.mailmanagement.engageservice.RightParens localRIGHT_PARENS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRIGHT_PARENSTracker = false;

    public boolean isAND_ORSpecified() {
        return localAND_ORTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.AndOr
     */
    public useractions.mailmanagement.engageservice.AndOr getAND_OR() {
        return localAND_OR;
    }

    /**
     * Auto generated setter method
     * @param param AND_OR
     */
    public void setAND_OR(useractions.mailmanagement.engageservice.AndOr param) {
        localAND_ORTracker = param != null;

        this.localAND_OR = param;
    }

    public boolean isLEFT_PARENSSpecified() {
        return localLEFT_PARENSTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.LeftParens
     */
    public useractions.mailmanagement.engageservice.LeftParens getLEFT_PARENS() {
        return localLEFT_PARENS;
    }

    /**
     * Auto generated setter method
     * @param param LEFT_PARENS
     */
    public void setLEFT_PARENS(
        useractions.mailmanagement.engageservice.LeftParens param) {
        localLEFT_PARENSTracker = param != null;

        this.localLEFT_PARENS = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.ExpressionType
     */
    public useractions.mailmanagement.engageservice.ExpressionType getTYPE() {
        return localTYPE;
    }

    /**
     * Auto generated setter method
     * @param param TYPE
     */
    public void setTYPE(
        useractions.mailmanagement.engageservice.ExpressionType param) {
        this.localTYPE = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getCOLUMN_NAME() {
        return localCOLUMN_NAME;
    }

    /**
     * Auto generated setter method
     * @param param COLUMN_NAME
     */
    public void setCOLUMN_NAME(java.lang.String param) {
        this.localCOLUMN_NAME = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.Operators
     */
    public useractions.mailmanagement.engageservice.Operators getOPERATORS() {
        return localOPERATORS;
    }

    /**
     * Auto generated setter method
     * @param param OPERATORS
     */
    public void setOPERATORS(
        useractions.mailmanagement.engageservice.Operators param) {
        this.localOPERATORS = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVALUES() {
        return localVALUES;
    }

    /**
     * Auto generated setter method
     * @param param VALUES
     */
    public void setVALUES(java.lang.String param) {
        this.localVALUES = param;
    }

    public boolean isRIGHT_PARENSSpecified() {
        return localRIGHT_PARENSTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.RightParens
     */
    public useractions.mailmanagement.engageservice.RightParens getRIGHT_PARENS() {
        return localRIGHT_PARENS;
    }

    /**
     * Auto generated setter method
     * @param param RIGHT_PARENS
     */
    public void setRIGHT_PARENS(
        useractions.mailmanagement.engageservice.RightParens param) {
        localRIGHT_PARENSTracker = param != null;

        this.localRIGHT_PARENS = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.MailManagement.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ExpressionElementType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ExpressionElementType", xmlWriter);
            }
        }

        if (localAND_ORTracker) {
            if (localAND_OR == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "AND_OR cannot be null!!");
            }

            localAND_OR.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.MailManagement.UserActions",
                    "AND_OR"), xmlWriter);
        }

        if (localLEFT_PARENSTracker) {
            if (localLEFT_PARENS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "LEFT_PARENS cannot be null!!");
            }

            localLEFT_PARENS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.MailManagement.UserActions",
                    "LEFT_PARENS"), xmlWriter);
        }

        if (localTYPE == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "TYPE cannot be null!!");
        }

        localTYPE.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.MailManagement.UserActions", "TYPE"),
            xmlWriter);

        namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
        writeStartElement(null, namespace, "COLUMN_NAME", xmlWriter);

        if (localCOLUMN_NAME == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "COLUMN_NAME cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localCOLUMN_NAME);
        }

        xmlWriter.writeEndElement();

        if (localOPERATORS == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "OPERATORS cannot be null!!");
        }

        localOPERATORS.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.MailManagement.UserActions",
                "OPERATORS"), xmlWriter);

        namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
        writeStartElement(null, namespace, "VALUES", xmlWriter);

        if (localVALUES == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "VALUES cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localVALUES);
        }

        xmlWriter.writeEndElement();

        if (localRIGHT_PARENSTracker) {
            if (localRIGHT_PARENS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "RIGHT_PARENS cannot be null!!");
            }

            localRIGHT_PARENS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.MailManagement.UserActions",
                    "RIGHT_PARENS"), xmlWriter);
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals(
                    "SilverpopApi:EngageService.MailManagement.UserActions")) {
            return "ns7";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ExpressionElementType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ExpressionElementType object = new ExpressionElementType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ExpressionElementType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ExpressionElementType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "AND_OR").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "AND_OR").equals(
                            reader.getName())) {
                    object.setAND_OR(useractions.mailmanagement.engageservice.AndOr.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "LEFT_PARENS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LEFT_PARENS").equals(
                            reader.getName())) {
                    object.setLEFT_PARENS(useractions.mailmanagement.engageservice.LeftParens.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "TYPE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "TYPE").equals(
                            reader.getName())) {
                    object.setTYPE(useractions.mailmanagement.engageservice.ExpressionType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "COLUMN_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "COLUMN_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "COLUMN_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setCOLUMN_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "OPERATORS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "OPERATORS").equals(
                            reader.getName())) {
                    object.setOPERATORS(useractions.mailmanagement.engageservice.Operators.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "VALUES").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "VALUES").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VALUES" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVALUES(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "RIGHT_PARENS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "RIGHT_PARENS").equals(
                            reader.getName())) {
                    object.setRIGHT_PARENS(useractions.mailmanagement.engageservice.RightParens.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
