/**
 * MailingTemplateElementType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.mailmanagement.engageservice;


/**
 *  MailingTemplateElementType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class MailingTemplateElementType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = MailingTemplateElementType
       Namespace URI = SilverpopApi:EngageService.MailManagement.UserActions
       Namespace Prefix = ns7
     */

    /**
     * field for MAILING_ID
     */
    protected long localMAILING_ID;

    /**
     * field for MAILING_NAME
     */
    protected java.lang.String localMAILING_NAME;

    /**
     * field for SUBJECT
     */
    protected java.lang.String localSUBJECT;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUBJECTTracker = false;

    /**
     * field for LAST_MODIFIED
     */
    protected useractions.mailmanagement.engageservice.DateTime2 localLAST_MODIFIED;

    /**
     * field for VISIBILITY
     */
    protected useractions.mailmanagement.engageservice.TemplateVisibility localVISIBILITY;

    /**
     * field for USER_ID
     */
    protected java.lang.String localUSER_ID;

    /**
     * field for FLAGGED_FOR_BACKUP
     */
    protected java.lang.String localFLAGGED_FOR_BACKUP;

    /**
     * field for ALLOW_CRM_BLOCK
     */
    protected java.lang.String localALLOW_CRM_BLOCK;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localALLOW_CRM_BLOCKTracker = false;

    /**
     * Auto generated getter method
     * @return long
     */
    public long getMAILING_ID() {
        return localMAILING_ID;
    }

    /**
     * Auto generated setter method
     * @param param MAILING_ID
     */
    public void setMAILING_ID(long param) {
        this.localMAILING_ID = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getMAILING_NAME() {
        return localMAILING_NAME;
    }

    /**
     * Auto generated setter method
     * @param param MAILING_NAME
     */
    public void setMAILING_NAME(java.lang.String param) {
        this.localMAILING_NAME = param;
    }

    public boolean isSUBJECTSpecified() {
        return localSUBJECTTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSUBJECT() {
        return localSUBJECT;
    }

    /**
     * Auto generated setter method
     * @param param SUBJECT
     */
    public void setSUBJECT(java.lang.String param) {
        localSUBJECTTracker = param != null;

        this.localSUBJECT = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.DateTime2
     */
    public useractions.mailmanagement.engageservice.DateTime2 getLAST_MODIFIED() {
        return localLAST_MODIFIED;
    }

    /**
     * Auto generated setter method
     * @param param LAST_MODIFIED
     */
    public void setLAST_MODIFIED(
        useractions.mailmanagement.engageservice.DateTime2 param) {
        this.localLAST_MODIFIED = param;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.TemplateVisibility
     */
    public useractions.mailmanagement.engageservice.TemplateVisibility getVISIBILITY() {
        return localVISIBILITY;
    }

    /**
     * Auto generated setter method
     * @param param VISIBILITY
     */
    public void setVISIBILITY(
        useractions.mailmanagement.engageservice.TemplateVisibility param) {
        this.localVISIBILITY = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getUSER_ID() {
        return localUSER_ID;
    }

    /**
     * Auto generated setter method
     * @param param USER_ID
     */
    public void setUSER_ID(java.lang.String param) {
        this.localUSER_ID = param;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFLAGGED_FOR_BACKUP() {
        return localFLAGGED_FOR_BACKUP;
    }

    /**
     * Auto generated setter method
     * @param param FLAGGED_FOR_BACKUP
     */
    public void setFLAGGED_FOR_BACKUP(java.lang.String param) {
        this.localFLAGGED_FOR_BACKUP = param;
    }

    public boolean isALLOW_CRM_BLOCKSpecified() {
        return localALLOW_CRM_BLOCKTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getALLOW_CRM_BLOCK() {
        return localALLOW_CRM_BLOCK;
    }

    /**
     * Auto generated setter method
     * @param param ALLOW_CRM_BLOCK
     */
    public void setALLOW_CRM_BLOCK(java.lang.String param) {
        localALLOW_CRM_BLOCKTracker = param != null;

        this.localALLOW_CRM_BLOCK = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.MailManagement.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":MailingTemplateElementType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "MailingTemplateElementType", xmlWriter);
            }
        }

        namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
        writeStartElement(null, namespace, "MAILING_ID", xmlWriter);

        if (localMAILING_ID == java.lang.Long.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "MAILING_ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localMAILING_ID));
        }

        xmlWriter.writeEndElement();

        namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
        writeStartElement(null, namespace, "MAILING_NAME", xmlWriter);

        if (localMAILING_NAME == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "MAILING_NAME cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localMAILING_NAME);
        }

        xmlWriter.writeEndElement();

        if (localSUBJECTTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "SUBJECT", xmlWriter);

            if (localSUBJECT == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SUBJECT cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSUBJECT);
            }

            xmlWriter.writeEndElement();
        }

        if (localLAST_MODIFIED == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "LAST_MODIFIED cannot be null!!");
        }

        localLAST_MODIFIED.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.MailManagement.UserActions",
                "LAST_MODIFIED"), xmlWriter);

        if (localVISIBILITY == null) {
            throw new org.apache.axis2.databinding.ADBException(
                "VISIBILITY cannot be null!!");
        }

        localVISIBILITY.serialize(new javax.xml.namespace.QName(
                "SilverpopApi:EngageService.MailManagement.UserActions",
                "VISIBILITY"), xmlWriter);

        namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
        writeStartElement(null, namespace, "USER_ID", xmlWriter);

        if (localUSER_ID == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "USER_ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localUSER_ID);
        }

        xmlWriter.writeEndElement();

        namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
        writeStartElement(null, namespace, "FLAGGED_FOR_BACKUP", xmlWriter);

        if (localFLAGGED_FOR_BACKUP == null) {
            // write the nil attribute
            throw new org.apache.axis2.databinding.ADBException(
                "FLAGGED_FOR_BACKUP cannot be null!!");
        } else {
            xmlWriter.writeCharacters(localFLAGGED_FOR_BACKUP);
        }

        xmlWriter.writeEndElement();

        if (localALLOW_CRM_BLOCKTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "ALLOW_CRM_BLOCK", xmlWriter);

            if (localALLOW_CRM_BLOCK == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ALLOW_CRM_BLOCK cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localALLOW_CRM_BLOCK);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals(
                    "SilverpopApi:EngageService.MailManagement.UserActions")) {
            return "ns7";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static MailingTemplateElementType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            MailingTemplateElementType object = new MailingTemplateElementType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"MailingTemplateElementType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (MailingTemplateElementType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "MAILING_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAILING_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MAILING_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMAILING_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "MAILING_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAILING_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MAILING_NAME" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMAILING_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SUBJECT").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUBJECT").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SUBJECT" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSUBJECT(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "LAST_MODIFIED").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LAST_MODIFIED").equals(
                            reader.getName())) {
                    object.setLAST_MODIFIED(useractions.mailmanagement.engageservice.DateTime2.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "VISIBILITY").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "VISIBILITY").equals(
                            reader.getName())) {
                    object.setVISIBILITY(useractions.mailmanagement.engageservice.TemplateVisibility.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "USER_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "USER_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "USER_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setUSER_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "FLAGGED_FOR_BACKUP").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FLAGGED_FOR_BACKUP").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FLAGGED_FOR_BACKUP" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFLAGGED_FOR_BACKUP(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "ALLOW_CRM_BLOCK").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ALLOW_CRM_BLOCK").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ALLOW_CRM_BLOCK" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setALLOW_CRM_BLOCK(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
