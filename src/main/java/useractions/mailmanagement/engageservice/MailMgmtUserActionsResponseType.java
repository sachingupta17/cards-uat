/**
 * MailMgmtUserActionsResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package useractions.mailmanagement.engageservice;


/**
 *  MailMgmtUserActionsResponseType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class MailMgmtUserActionsResponseType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = MailMgmtUserActionsResponseType
       Namespace URI = SilverpopApi:EngageService.MailManagement.UserActions
       Namespace Prefix = ns7
     */

    /**
     * field for SUCCESS
     */
    protected useractions.mailmanagement.engageservice.MailMgmtUserSuccess localSUCCESS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUCCESSTracker = false;

    /**
     * field for Fault
     */
    protected useractions.mailmanagement.engageservice.FaultType localFault;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFaultTracker = false;

    /**
     * field for MAILING_ID
     */
    protected long localMAILING_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMAILING_IDTracker = false;

    /**
     * field for RULESET_ID
     */
    protected long localRULESET_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRULESET_IDTracker = false;

    /**
     * field for JOB_ID
     */
    protected long localJOB_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localJOB_IDTracker = false;

    /**
     * field for FILE_NAME
     */
    protected java.lang.String localFILE_NAME;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFILE_NAMETracker = false;

    /**
     * field for RULESET
     * This was an Array!
     */
    protected useractions.mailmanagement.engageservice.RulesetElementType[] localRULESET;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRULESETTracker = false;

    /**
     * field for FILE_PATH
     */
    protected java.lang.String localFILE_PATH;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFILE_PATHTracker = false;

    /**
     * field for MAILING_TEMPLATE
     * This was an Array!
     */
    protected useractions.mailmanagement.engageservice.MailingTemplateElementType[] localMAILING_TEMPLATE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localMAILING_TEMPLATETracker = false;

    /**
     * field for HTMLBody
     */
    protected java.lang.String localHTMLBody;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localHTMLBodyTracker = false;

    /**
     * field for AOLBody
     */
    protected java.lang.String localAOLBody;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localAOLBodyTracker = false;

    /**
     * field for TextBody
     */
    protected java.lang.String localTextBody;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localTextBodyTracker = false;

    /**
     * field for SpamScore
     */
    protected java.lang.String localSpamScore;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSpamScoreTracker = false;

    public boolean isSUCCESSSpecified() {
        return localSUCCESSTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.MailMgmtUserSuccess
     */
    public useractions.mailmanagement.engageservice.MailMgmtUserSuccess getSUCCESS() {
        return localSUCCESS;
    }

    /**
     * Auto generated setter method
     * @param param SUCCESS
     */
    public void setSUCCESS(
        useractions.mailmanagement.engageservice.MailMgmtUserSuccess param) {
        localSUCCESSTracker = param != null;

        this.localSUCCESS = param;
    }

    public boolean isFaultSpecified() {
        return localFaultTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.FaultType
     */
    public useractions.mailmanagement.engageservice.FaultType getFault() {
        return localFault;
    }

    /**
     * Auto generated setter method
     * @param param Fault
     */
    public void setFault(
        useractions.mailmanagement.engageservice.FaultType param) {
        localFaultTracker = param != null;

        this.localFault = param;
    }

    public boolean isMAILING_IDSpecified() {
        return localMAILING_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getMAILING_ID() {
        return localMAILING_ID;
    }

    /**
     * Auto generated setter method
     * @param param MAILING_ID
     */
    public void setMAILING_ID(long param) {
        // setting primitive attribute tracker to true
        localMAILING_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localMAILING_ID = param;
    }

    public boolean isRULESET_IDSpecified() {
        return localRULESET_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getRULESET_ID() {
        return localRULESET_ID;
    }

    /**
     * Auto generated setter method
     * @param param RULESET_ID
     */
    public void setRULESET_ID(long param) {
        // setting primitive attribute tracker to true
        localRULESET_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localRULESET_ID = param;
    }

    public boolean isJOB_IDSpecified() {
        return localJOB_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return long
     */
    public long getJOB_ID() {
        return localJOB_ID;
    }

    /**
     * Auto generated setter method
     * @param param JOB_ID
     */
    public void setJOB_ID(long param) {
        // setting primitive attribute tracker to true
        localJOB_IDTracker = param != java.lang.Long.MIN_VALUE;

        this.localJOB_ID = param;
    }

    public boolean isFILE_NAMESpecified() {
        return localFILE_NAMETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFILE_NAME() {
        return localFILE_NAME;
    }

    /**
     * Auto generated setter method
     * @param param FILE_NAME
     */
    public void setFILE_NAME(java.lang.String param) {
        localFILE_NAMETracker = param != null;

        this.localFILE_NAME = param;
    }

    public boolean isRULESETSpecified() {
        return localRULESETTracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.RulesetElementType[]
     */
    public useractions.mailmanagement.engageservice.RulesetElementType[] getRULESET() {
        return localRULESET;
    }

    /**
     * validate the array for RULESET
     */
    protected void validateRULESET(
        useractions.mailmanagement.engageservice.RulesetElementType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param RULESET
     */
    public void setRULESET(
        useractions.mailmanagement.engageservice.RulesetElementType[] param) {
        validateRULESET(param);

        localRULESETTracker = param != null;

        this.localRULESET = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param useractions.mailmanagement.engageservice.RulesetElementType
     */
    public void addRULESET(
        useractions.mailmanagement.engageservice.RulesetElementType param) {
        if (localRULESET == null) {
            localRULESET = new useractions.mailmanagement.engageservice.RulesetElementType[] {
                    
                };
        }

        //update the setting tracker
        localRULESETTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localRULESET);
        list.add(param);
        this.localRULESET = (useractions.mailmanagement.engageservice.RulesetElementType[]) list.toArray(new useractions.mailmanagement.engageservice.RulesetElementType[list.size()]);
    }

    public boolean isFILE_PATHSpecified() {
        return localFILE_PATHTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getFILE_PATH() {
        return localFILE_PATH;
    }

    /**
     * Auto generated setter method
     * @param param FILE_PATH
     */
    public void setFILE_PATH(java.lang.String param) {
        localFILE_PATHTracker = param != null;

        this.localFILE_PATH = param;
    }

    public boolean isMAILING_TEMPLATESpecified() {
        return localMAILING_TEMPLATETracker;
    }

    /**
     * Auto generated getter method
     * @return useractions.mailmanagement.engageservice.MailingTemplateElementType[]
     */
    public useractions.mailmanagement.engageservice.MailingTemplateElementType[] getMAILING_TEMPLATE() {
        return localMAILING_TEMPLATE;
    }

    /**
     * validate the array for MAILING_TEMPLATE
     */
    protected void validateMAILING_TEMPLATE(
        useractions.mailmanagement.engageservice.MailingTemplateElementType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param MAILING_TEMPLATE
     */
    public void setMAILING_TEMPLATE(
        useractions.mailmanagement.engageservice.MailingTemplateElementType[] param) {
        validateMAILING_TEMPLATE(param);

        localMAILING_TEMPLATETracker = param != null;

        this.localMAILING_TEMPLATE = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param useractions.mailmanagement.engageservice.MailingTemplateElementType
     */
    public void addMAILING_TEMPLATE(
        useractions.mailmanagement.engageservice.MailingTemplateElementType param) {
        if (localMAILING_TEMPLATE == null) {
            localMAILING_TEMPLATE = new useractions.mailmanagement.engageservice.MailingTemplateElementType[] {
                    
                };
        }

        //update the setting tracker
        localMAILING_TEMPLATETracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localMAILING_TEMPLATE);
        list.add(param);
        this.localMAILING_TEMPLATE = (useractions.mailmanagement.engageservice.MailingTemplateElementType[]) list.toArray(new useractions.mailmanagement.engageservice.MailingTemplateElementType[list.size()]);
    }

    public boolean isHTMLBodySpecified() {
        return localHTMLBodyTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getHTMLBody() {
        return localHTMLBody;
    }

    /**
     * Auto generated setter method
     * @param param HTMLBody
     */
    public void setHTMLBody(java.lang.String param) {
        localHTMLBodyTracker = param != null;

        this.localHTMLBody = param;
    }

    public boolean isAOLBodySpecified() {
        return localAOLBodyTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getAOLBody() {
        return localAOLBody;
    }

    /**
     * Auto generated setter method
     * @param param AOLBody
     */
    public void setAOLBody(java.lang.String param) {
        localAOLBodyTracker = param != null;

        this.localAOLBody = param;
    }

    public boolean isTextBodySpecified() {
        return localTextBodyTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getTextBody() {
        return localTextBody;
    }

    /**
     * Auto generated setter method
     * @param param TextBody
     */
    public void setTextBody(java.lang.String param) {
        localTextBodyTracker = param != null;

        this.localTextBody = param;
    }

    public boolean isSpamScoreSpecified() {
        return localSpamScoreTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getSpamScore() {
        return localSpamScore;
    }

    /**
     * Auto generated setter method
     * @param param SpamScore
     */
    public void setSpamScore(java.lang.String param) {
        localSpamScoreTracker = param != null;

        this.localSpamScore = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.MailManagement.UserActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":MailMgmtUserActionsResponseType",
                    xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "MailMgmtUserActionsResponseType", xmlWriter);
            }
        }

        if (localSUCCESSTracker) {
            if (localSUCCESS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SUCCESS cannot be null!!");
            }

            localSUCCESS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.MailManagement.UserActions",
                    "SUCCESS"), xmlWriter);
        }

        if (localFaultTracker) {
            if (localFault == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "Fault cannot be null!!");
            }

            localFault.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.MailManagement.UserActions",
                    "Fault"), xmlWriter);
        }

        if (localMAILING_IDTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "MAILING_ID", xmlWriter);

            if (localMAILING_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "MAILING_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localMAILING_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localRULESET_IDTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "RULESET_ID", xmlWriter);

            if (localRULESET_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "RULESET_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localRULESET_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localJOB_IDTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "JOB_ID", xmlWriter);

            if (localJOB_ID == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "JOB_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localJOB_ID));
            }

            xmlWriter.writeEndElement();
        }

        if (localFILE_NAMETracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "FILE_NAME", xmlWriter);

            if (localFILE_NAME == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FILE_NAME cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFILE_NAME);
            }

            xmlWriter.writeEndElement();
        }

        if (localRULESETTracker) {
            if (localRULESET != null) {
                for (int i = 0; i < localRULESET.length; i++) {
                    if (localRULESET[i] != null) {
                        localRULESET[i].serialize(new javax.xml.namespace.QName(
                                "SilverpopApi:EngageService.MailManagement.UserActions",
                                "RULESET"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "RULESET cannot be null!!");
            }
        }

        if (localFILE_PATHTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "FILE_PATH", xmlWriter);

            if (localFILE_PATH == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "FILE_PATH cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localFILE_PATH);
            }

            xmlWriter.writeEndElement();
        }

        if (localMAILING_TEMPLATETracker) {
            if (localMAILING_TEMPLATE != null) {
                for (int i = 0; i < localMAILING_TEMPLATE.length; i++) {
                    if (localMAILING_TEMPLATE[i] != null) {
                        localMAILING_TEMPLATE[i].serialize(new javax.xml.namespace.QName(
                                "SilverpopApi:EngageService.MailManagement.UserActions",
                                "MAILING_TEMPLATE"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "MAILING_TEMPLATE cannot be null!!");
            }
        }

        if (localHTMLBodyTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "HTMLBody", xmlWriter);

            if (localHTMLBody == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "HTMLBody cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localHTMLBody);
            }

            xmlWriter.writeEndElement();
        }

        if (localAOLBodyTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "AOLBody", xmlWriter);

            if (localAOLBody == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "AOLBody cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localAOLBody);
            }

            xmlWriter.writeEndElement();
        }

        if (localTextBodyTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "TextBody", xmlWriter);

            if (localTextBody == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "TextBody cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localTextBody);
            }

            xmlWriter.writeEndElement();
        }

        if (localSpamScoreTracker) {
            namespace = "SilverpopApi:EngageService.MailManagement.UserActions";
            writeStartElement(null, namespace, "SpamScore", xmlWriter);

            if (localSpamScore == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "SpamScore cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localSpamScore);
            }

            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals(
                    "SilverpopApi:EngageService.MailManagement.UserActions")) {
            return "ns7";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static MailMgmtUserActionsResponseType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            MailMgmtUserActionsResponseType object = new MailMgmtUserActionsResponseType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"MailMgmtUserActionsResponseType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (MailMgmtUserActionsResponseType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                java.util.ArrayList list7 = new java.util.ArrayList();

                java.util.ArrayList list9 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SUCCESS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUCCESS").equals(
                            reader.getName())) {
                    object.setSUCCESS(useractions.mailmanagement.engageservice.MailMgmtUserSuccess.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "Fault").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "Fault").equals(
                            reader.getName())) {
                    object.setFault(useractions.mailmanagement.engageservice.FaultType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "MAILING_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAILING_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "MAILING_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setMAILING_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setMAILING_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "RULESET_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "RULESET_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RULESET_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRULESET_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setRULESET_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "JOB_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "JOB_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "JOB_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setJOB_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    object.setJOB_ID(java.lang.Long.MIN_VALUE);
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "FILE_NAME").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FILE_NAME").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FILE_NAME" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFILE_NAME(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "RULESET").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "RULESET").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list7.add(useractions.mailmanagement.engageservice.RulesetElementType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone7 = false;

                    while (!loopDone7) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone7 = true;
                        } else {
                            if (new javax.xml.namespace.QName(
                                        "SilverpopApi:EngageService.MailManagement.UserActions",
                                        "RULESET").equals(reader.getName())) {
                                list7.add(useractions.mailmanagement.engageservice.RulesetElementType.Factory.parse(
                                        reader));
                            } else {
                                loopDone7 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setRULESET((useractions.mailmanagement.engageservice.RulesetElementType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            useractions.mailmanagement.engageservice.RulesetElementType.class,
                            list7));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "FILE_PATH").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "FILE_PATH").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "FILE_PATH" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setFILE_PATH(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "MAILING_TEMPLATE").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "MAILING_TEMPLATE").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list9.add(useractions.mailmanagement.engageservice.MailingTemplateElementType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone9 = false;

                    while (!loopDone9) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone9 = true;
                        } else {
                            if (new javax.xml.namespace.QName(
                                        "SilverpopApi:EngageService.MailManagement.UserActions",
                                        "MAILING_TEMPLATE").equals(
                                        reader.getName())) {
                                list9.add(useractions.mailmanagement.engageservice.MailingTemplateElementType.Factory.parse(
                                        reader));
                            } else {
                                loopDone9 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setMAILING_TEMPLATE((useractions.mailmanagement.engageservice.MailingTemplateElementType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            useractions.mailmanagement.engageservice.MailingTemplateElementType.class,
                            list9));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "HTMLBody").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "HTMLBody").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "HTMLBody" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setHTMLBody(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "AOLBody").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "AOLBody").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "AOLBody" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setAOLBody(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "TextBody").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "TextBody").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "TextBody" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setTextBody(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.MailManagement.UserActions",
                            "SpamScore").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SpamScore").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "SpamScore" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setSpamScore(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
