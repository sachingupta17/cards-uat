/**
 * SelectRecipientRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package recipientactions.listmgmt.engageservice;


/**
 *  SelectRecipientRequestType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class SelectRecipientRequestType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = SelectRecipientRequestType
       Namespace URI = SilverpopApi:EngageService.ListMgmt.RecipientActions
       Namespace Prefix = ns4
     */

    /**
     * field for LIST_ID
     */
    protected long localLIST_ID;

    /**
     * field for EMAIL
     */
    protected java.lang.String localEMAIL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILTracker = false;

    /**
     * field for ENCODED_RECIPIENT_ID
     */
    protected java.lang.String localENCODED_RECIPIENT_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localENCODED_RECIPIENT_IDTracker = false;

    /**
     * field for RETURN_CONTACT_LISTS
     */
    protected recipientactions.listmgmt.engageservice.ListMgmtSuccess localRETURN_CONTACT_LISTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRETURN_CONTACT_LISTSTracker = false;

    /**
     * field for COLUMN
     * This was an Array!
     */
    protected recipientactions.listmgmt.engageservice.ColumnElementType[] localCOLUMN;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOLUMNTracker = false;

    /**
     * Auto generated getter method
     * @return long
     */
    public long getLIST_ID() {
        return localLIST_ID;
    }

    /**
     * Auto generated setter method
     * @param param LIST_ID
     */
    public void setLIST_ID(long param) {
        this.localLIST_ID = param;
    }

    public boolean isEMAILSpecified() {
        return localEMAILTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL() {
        return localEMAIL;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL
     */
    public void setEMAIL(java.lang.String param) {
        localEMAILTracker = param != null;

        this.localEMAIL = param;
    }

    public boolean isENCODED_RECIPIENT_IDSpecified() {
        return localENCODED_RECIPIENT_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getENCODED_RECIPIENT_ID() {
        return localENCODED_RECIPIENT_ID;
    }

    /**
     * Auto generated setter method
     * @param param ENCODED_RECIPIENT_ID
     */
    public void setENCODED_RECIPIENT_ID(java.lang.String param) {
        localENCODED_RECIPIENT_IDTracker = param != null;

        this.localENCODED_RECIPIENT_ID = param;
    }

    public boolean isRETURN_CONTACT_LISTSSpecified() {
        return localRETURN_CONTACT_LISTSTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.ListMgmtSuccess
     */
    public recipientactions.listmgmt.engageservice.ListMgmtSuccess getRETURN_CONTACT_LISTS() {
        return localRETURN_CONTACT_LISTS;
    }

    /**
     * Auto generated setter method
     * @param param RETURN_CONTACT_LISTS
     */
    public void setRETURN_CONTACT_LISTS(
        recipientactions.listmgmt.engageservice.ListMgmtSuccess param) {
        localRETURN_CONTACT_LISTSTracker = param != null;

        this.localRETURN_CONTACT_LISTS = param;
    }

    public boolean isCOLUMNSpecified() {
        return localCOLUMNTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.ColumnElementType[]
     */
    public recipientactions.listmgmt.engageservice.ColumnElementType[] getCOLUMN() {
        return localCOLUMN;
    }

    /**
     * validate the array for COLUMN
     */
    protected void validateCOLUMN(
        recipientactions.listmgmt.engageservice.ColumnElementType[] param) {
    }

    /**
     * Auto generated setter method
     * @param param COLUMN
     */
    public void setCOLUMN(
        recipientactions.listmgmt.engageservice.ColumnElementType[] param) {
        validateCOLUMN(param);

        localCOLUMNTracker = param != null;

        this.localCOLUMN = param;
    }

    /**
     * Auto generated add method for the array for convenience
     * @param param recipientactions.listmgmt.engageservice.ColumnElementType
     */
    public void addCOLUMN(
        recipientactions.listmgmt.engageservice.ColumnElementType param) {
        if (localCOLUMN == null) {
            localCOLUMN = new recipientactions.listmgmt.engageservice.ColumnElementType[] {
                    
                };
        }

        //update the setting tracker
        localCOLUMNTracker = true;

        java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localCOLUMN);
        list.add(param);
        this.localCOLUMN = (recipientactions.listmgmt.engageservice.ColumnElementType[]) list.toArray(new recipientactions.listmgmt.engageservice.ColumnElementType[list.size()]);
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":SelectRecipientRequestType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "SelectRecipientRequestType", xmlWriter);
            }
        }

        namespace = "SilverpopApi:EngageService.ListMgmt.RecipientActions";
        writeStartElement(null, namespace, "LIST_ID", xmlWriter);

        if (localLIST_ID == java.lang.Long.MIN_VALUE) {
            throw new org.apache.axis2.databinding.ADBException(
                "LIST_ID cannot be null!!");
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localLIST_ID));
        }

        xmlWriter.writeEndElement();

        if (localEMAILTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.RecipientActions";
            writeStartElement(null, namespace, "EMAIL", xmlWriter);

            if (localEMAIL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL);
            }

            xmlWriter.writeEndElement();
        }

        if (localENCODED_RECIPIENT_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.RecipientActions";
            writeStartElement(null, namespace, "ENCODED_RECIPIENT_ID", xmlWriter);

            if (localENCODED_RECIPIENT_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ENCODED_RECIPIENT_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localENCODED_RECIPIENT_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localRETURN_CONTACT_LISTSTracker) {
            if (localRETURN_CONTACT_LISTS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "RETURN_CONTACT_LISTS cannot be null!!");
            }

            localRETURN_CONTACT_LISTS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "RETURN_CONTACT_LISTS"), xmlWriter);
        }

        if (localCOLUMNTracker) {
            if (localCOLUMN != null) {
                for (int i = 0; i < localCOLUMN.length; i++) {
                    if (localCOLUMN[i] != null) {
                        localCOLUMN[i].serialize(new javax.xml.namespace.QName(
                                "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                                "COLUMN"), xmlWriter);
                    } else {
                        // we don't have to do any thing since minOccures is zero
                    }
                }
            } else {
                throw new org.apache.axis2.databinding.ADBException(
                    "COLUMN cannot be null!!");
            }
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions")) {
            return "ns4";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static SelectRecipientRequestType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            SelectRecipientRequestType object = new SelectRecipientRequestType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"SelectRecipientRequestType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (SelectRecipientRequestType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                java.util.ArrayList list5 = new java.util.ArrayList();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "LIST_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LIST_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "LIST_ID" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setLIST_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                    // 1 - A start element we are not expecting indicates an invalid parameter was passed
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "EMAIL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "ENCODED_RECIPIENT_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ENCODED_RECIPIENT_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ENCODED_RECIPIENT_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setENCODED_RECIPIENT_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "RETURN_CONTACT_LISTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "RETURN_CONTACT_LISTS").equals(
                            reader.getName())) {
                    object.setRETURN_CONTACT_LISTS(recipientactions.listmgmt.engageservice.ListMgmtSuccess.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "COLUMN").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "COLUMN").equals(
                            reader.getName())) {
                    // Process the array and step past its final element's end.
                    list5.add(recipientactions.listmgmt.engageservice.ColumnElementType.Factory.parse(
                            reader));

                    //loop until we find a start element that is not part of this array
                    boolean loopDone5 = false;

                    while (!loopDone5) {
                        // We should be at the end element, but make sure
                        while (!reader.isEndElement())
                            reader.next();

                        // Step out of this element
                        reader.next();

                        // Step to next element event.
                        while (!reader.isStartElement() &&
                                !reader.isEndElement())
                            reader.next();

                        if (reader.isEndElement()) {
                            //two continuous end elements means we are exiting the xml structure
                            loopDone5 = true;
                        } else {
                            if (new javax.xml.namespace.QName(
                                        "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                                        "COLUMN").equals(reader.getName())) {
                                list5.add(recipientactions.listmgmt.engageservice.ColumnElementType.Factory.parse(
                                        reader));
                            } else {
                                loopDone5 = true;
                            }
                        }
                    }

                    // call the converter utility  to convert and set the array
                    object.setCOLUMN((recipientactions.listmgmt.engageservice.ColumnElementType[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                            recipientactions.listmgmt.engageservice.ColumnElementType.class,
                            list5));
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
