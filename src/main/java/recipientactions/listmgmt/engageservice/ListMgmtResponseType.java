/**
 * ListMgmtResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:48:01 BST)
 */
package recipientactions.listmgmt.engageservice;


/**
 *  ListMgmtResponseType bean class
 */
@SuppressWarnings({"unchecked",
    "unused"
})
public class ListMgmtResponseType implements org.apache.axis2.databinding.ADBBean {
    /* This type was generated from the piece of schema that had
       name = ListMgmtResponseType
       Namespace URI = SilverpopApi:EngageService.ListMgmt.RecipientActions
       Namespace Prefix = ns4
     */

    /**
     * field for SUCCESS
     */
    protected recipientactions.listmgmt.engageservice.ListMgmtSuccess localSUCCESS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localSUCCESSTracker = false;

    /**
     * field for Fault
     */
    protected recipientactions.listmgmt.engageservice.FaultType localFault;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localFaultTracker = false;

    /**
     * field for EMAIL
     */
    protected java.lang.String localEMAIL;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEMAILTracker = false;

    /**
     * field for EmailE
     */
    protected java.lang.String localEmailE;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEmailETracker = false;

    /**
     * field for RecipientId
     */
    protected java.lang.String localRecipientId;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localRecipientIdTracker = false;

    /**
     * field for EmailType
     */
    protected recipientactions.listmgmt.engageservice.EmailType localEmailType;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localEmailTypeTracker = false;

    /**
     * field for LastModified
     */
    protected recipientactions.listmgmt.engageservice.DateTime1 localLastModified;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localLastModifiedTracker = false;

    /**
     * field for CreatedFrom
     */
    protected recipientactions.listmgmt.engageservice.CreatedFrom localCreatedFrom;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCreatedFromTracker = false;

    /**
     * field for OptedIn
     */
    protected recipientactions.listmgmt.engageservice.DateTime1 localOptedIn;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOptedInTracker = false;

    /**
     * field for OptedOut
     */
    protected recipientactions.listmgmt.engageservice.DateTime1 localOptedOut;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localOptedOutTracker = false;

    /**
     * field for ResumeSendDate
     */
    protected recipientactions.listmgmt.engageservice.Date1 localResumeSendDate;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localResumeSendDateTracker = false;

    /**
     * field for ORGANIZATION_ID
     */
    protected java.lang.String localORGANIZATION_ID;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localORGANIZATION_IDTracker = false;

    /**
     * field for VISITOR_ASSOCIATION
     */
    protected java.lang.String localVISITOR_ASSOCIATION;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localVISITOR_ASSOCIATIONTracker = false;

    /**
     * field for COLUMNS
     */
    protected recipientactions.listmgmt.engageservice.ColumnsElementType localCOLUMNS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCOLUMNSTracker = false;

    /**
     * field for CONTACT_LISTS
     */
    protected recipientactions.listmgmt.engageservice.ContactListsElementType localCONTACT_LISTS;

    /*  This tracker boolean wil be used to detect whether the user called the set method
     *   for this attribute. It will be used to determine whether to include this field
     *   in the serialized XML
     */
    protected boolean localCONTACT_LISTSTracker = false;

    public boolean isSUCCESSSpecified() {
        return localSUCCESSTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.ListMgmtSuccess
     */
    public recipientactions.listmgmt.engageservice.ListMgmtSuccess getSUCCESS() {
        return localSUCCESS;
    }

    /**
     * Auto generated setter method
     * @param param SUCCESS
     */
    public void setSUCCESS(
        recipientactions.listmgmt.engageservice.ListMgmtSuccess param) {
        localSUCCESSTracker = param != null;

        this.localSUCCESS = param;
    }

    public boolean isFaultSpecified() {
        return localFaultTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.FaultType
     */
    public recipientactions.listmgmt.engageservice.FaultType getFault() {
        return localFault;
    }

    /**
     * Auto generated setter method
     * @param param Fault
     */
    public void setFault(
        recipientactions.listmgmt.engageservice.FaultType param) {
        localFaultTracker = param != null;

        this.localFault = param;
    }

    public boolean isEMAILSpecified() {
        return localEMAILTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEMAIL() {
        return localEMAIL;
    }

    /**
     * Auto generated setter method
     * @param param EMAIL
     */
    public void setEMAIL(java.lang.String param) {
        localEMAILTracker = param != null;

        this.localEMAIL = param;
    }

    public boolean isEmailESpecified() {
        return localEmailETracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getEmailE() {
        return localEmailE;
    }

    /**
     * Auto generated setter method
     * @param param EmailE
     */
    public void setEmailE(java.lang.String param) {
        localEmailETracker = param != null;

        this.localEmailE = param;
    }

    public boolean isRecipientIdSpecified() {
        return localRecipientIdTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getRecipientId() {
        return localRecipientId;
    }

    /**
     * Auto generated setter method
     * @param param RecipientId
     */
    public void setRecipientId(java.lang.String param) {
        localRecipientIdTracker = param != null;

        this.localRecipientId = param;
    }

    public boolean isEmailTypeSpecified() {
        return localEmailTypeTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.EmailType
     */
    public recipientactions.listmgmt.engageservice.EmailType getEmailType() {
        return localEmailType;
    }

    /**
     * Auto generated setter method
     * @param param EmailType
     */
    public void setEmailType(
        recipientactions.listmgmt.engageservice.EmailType param) {
        localEmailTypeTracker = param != null;

        this.localEmailType = param;
    }

    public boolean isLastModifiedSpecified() {
        return localLastModifiedTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.DateTime1
     */
    public recipientactions.listmgmt.engageservice.DateTime1 getLastModified() {
        return localLastModified;
    }

    /**
     * Auto generated setter method
     * @param param LastModified
     */
    public void setLastModified(
        recipientactions.listmgmt.engageservice.DateTime1 param) {
        localLastModifiedTracker = param != null;

        this.localLastModified = param;
    }

    public boolean isCreatedFromSpecified() {
        return localCreatedFromTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.CreatedFrom
     */
    public recipientactions.listmgmt.engageservice.CreatedFrom getCreatedFrom() {
        return localCreatedFrom;
    }

    /**
     * Auto generated setter method
     * @param param CreatedFrom
     */
    public void setCreatedFrom(
        recipientactions.listmgmt.engageservice.CreatedFrom param) {
        localCreatedFromTracker = param != null;

        this.localCreatedFrom = param;
    }

    public boolean isOptedInSpecified() {
        return localOptedInTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.DateTime1
     */
    public recipientactions.listmgmt.engageservice.DateTime1 getOptedIn() {
        return localOptedIn;
    }

    /**
     * Auto generated setter method
     * @param param OptedIn
     */
    public void setOptedIn(
        recipientactions.listmgmt.engageservice.DateTime1 param) {
        localOptedInTracker = param != null;

        this.localOptedIn = param;
    }

    public boolean isOptedOutSpecified() {
        return localOptedOutTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.DateTime1
     */
    public recipientactions.listmgmt.engageservice.DateTime1 getOptedOut() {
        return localOptedOut;
    }

    /**
     * Auto generated setter method
     * @param param OptedOut
     */
    public void setOptedOut(
        recipientactions.listmgmt.engageservice.DateTime1 param) {
        localOptedOutTracker = param != null;

        this.localOptedOut = param;
    }

    public boolean isResumeSendDateSpecified() {
        return localResumeSendDateTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.Date1
     */
    public recipientactions.listmgmt.engageservice.Date1 getResumeSendDate() {
        return localResumeSendDate;
    }

    /**
     * Auto generated setter method
     * @param param ResumeSendDate
     */
    public void setResumeSendDate(
        recipientactions.listmgmt.engageservice.Date1 param) {
        localResumeSendDateTracker = param != null;

        this.localResumeSendDate = param;
    }

    public boolean isORGANIZATION_IDSpecified() {
        return localORGANIZATION_IDTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getORGANIZATION_ID() {
        return localORGANIZATION_ID;
    }

    /**
     * Auto generated setter method
     * @param param ORGANIZATION_ID
     */
    public void setORGANIZATION_ID(java.lang.String param) {
        localORGANIZATION_IDTracker = param != null;

        this.localORGANIZATION_ID = param;
    }

    public boolean isVISITOR_ASSOCIATIONSpecified() {
        return localVISITOR_ASSOCIATIONTracker;
    }

    /**
     * Auto generated getter method
     * @return java.lang.String
     */
    public java.lang.String getVISITOR_ASSOCIATION() {
        return localVISITOR_ASSOCIATION;
    }

    /**
     * Auto generated setter method
     * @param param VISITOR_ASSOCIATION
     */
    public void setVISITOR_ASSOCIATION(java.lang.String param) {
        localVISITOR_ASSOCIATIONTracker = param != null;

        this.localVISITOR_ASSOCIATION = param;
    }

    public boolean isCOLUMNSSpecified() {
        return localCOLUMNSTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.ColumnsElementType
     */
    public recipientactions.listmgmt.engageservice.ColumnsElementType getCOLUMNS() {
        return localCOLUMNS;
    }

    /**
     * Auto generated setter method
     * @param param COLUMNS
     */
    public void setCOLUMNS(
        recipientactions.listmgmt.engageservice.ColumnsElementType param) {
        localCOLUMNSTracker = param != null;

        this.localCOLUMNS = param;
    }

    public boolean isCONTACT_LISTSSpecified() {
        return localCONTACT_LISTSTracker;
    }

    /**
     * Auto generated getter method
     * @return recipientactions.listmgmt.engageservice.ContactListsElementType
     */
    public recipientactions.listmgmt.engageservice.ContactListsElementType getCONTACT_LISTS() {
        return localCONTACT_LISTS;
    }

    /**
     * Auto generated setter method
     * @param param CONTACT_LISTS
     */
    public void setCONTACT_LISTS(
        recipientactions.listmgmt.engageservice.ContactListsElementType param) {
        localCONTACT_LISTSTracker = param != null;

        this.localCONTACT_LISTS = param;
    }

    /**
     *
     * @param parentQName
     * @param factory
     * @return org.apache.axiom.om.OMElement
     */
    public org.apache.axiom.om.OMElement getOMElement(
        final javax.xml.namespace.QName parentQName,
        final org.apache.axiom.om.OMFactory factory)
        throws org.apache.axis2.databinding.ADBException {
        return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(
                this, parentQName));
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        serialize(parentQName, xmlWriter, false);
    }

    public void serialize(final javax.xml.namespace.QName parentQName,
        javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
        throws javax.xml.stream.XMLStreamException,
            org.apache.axis2.databinding.ADBException {
        java.lang.String prefix = null;
        java.lang.String namespace = null;

        prefix = parentQName.getPrefix();
        namespace = parentQName.getNamespaceURI();
        writeStartElement(prefix, namespace, parentQName.getLocalPart(),
            xmlWriter);

        if (serializeType) {
            java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions");

            if ((namespacePrefix != null) &&
                    (namespacePrefix.trim().length() > 0)) {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    namespacePrefix + ":ListMgmtResponseType", xmlWriter);
            } else {
                writeAttribute("xsi",
                    "http://www.w3.org/2001/XMLSchema-instance", "type",
                    "ListMgmtResponseType", xmlWriter);
            }
        }

        if (localSUCCESSTracker) {
            if (localSUCCESS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SUCCESS cannot be null!!");
            }

            localSUCCESS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "SUCCESS"), xmlWriter);
        }

        if (localFaultTracker) {
            if (localFault == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "Fault cannot be null!!");
            }

            localFault.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "Fault"), xmlWriter);
        }

        if (localEMAILTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.RecipientActions";
            writeStartElement(null, namespace, "EMAIL", xmlWriter);

            if (localEMAIL == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "EMAIL cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEMAIL);
            }

            xmlWriter.writeEndElement();
        }

        if (localEmailETracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.RecipientActions";
            writeStartElement(null, namespace, "Email", xmlWriter);

            if (localEmailE == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "Email cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localEmailE);
            }

            xmlWriter.writeEndElement();
        }

        if (localRecipientIdTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.RecipientActions";
            writeStartElement(null, namespace, "RecipientId", xmlWriter);

            if (localRecipientId == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "RecipientId cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localRecipientId);
            }

            xmlWriter.writeEndElement();
        }

        if (localEmailTypeTracker) {
            if (localEmailType == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "EmailType cannot be null!!");
            }

            localEmailType.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "EmailType"), xmlWriter);
        }

        if (localLastModifiedTracker) {
            if (localLastModified == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "LastModified cannot be null!!");
            }

            localLastModified.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "LastModified"), xmlWriter);
        }

        if (localCreatedFromTracker) {
            if (localCreatedFrom == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "CreatedFrom cannot be null!!");
            }

            localCreatedFrom.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "CreatedFrom"), xmlWriter);
        }

        if (localOptedInTracker) {
            if (localOptedIn == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "OptedIn cannot be null!!");
            }

            localOptedIn.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "OptedIn"), xmlWriter);
        }

        if (localOptedOutTracker) {
            if (localOptedOut == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "OptedOut cannot be null!!");
            }

            localOptedOut.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "OptedOut"), xmlWriter);
        }

        if (localResumeSendDateTracker) {
            if (localResumeSendDate == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "ResumeSendDate cannot be null!!");
            }

            localResumeSendDate.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "ResumeSendDate"), xmlWriter);
        }

        if (localORGANIZATION_IDTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.RecipientActions";
            writeStartElement(null, namespace, "ORGANIZATION_ID", xmlWriter);

            if (localORGANIZATION_ID == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "ORGANIZATION_ID cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localORGANIZATION_ID);
            }

            xmlWriter.writeEndElement();
        }

        if (localVISITOR_ASSOCIATIONTracker) {
            namespace = "SilverpopApi:EngageService.ListMgmt.RecipientActions";
            writeStartElement(null, namespace, "VISITOR_ASSOCIATION", xmlWriter);

            if (localVISITOR_ASSOCIATION == null) {
                // write the nil attribute
                throw new org.apache.axis2.databinding.ADBException(
                    "VISITOR_ASSOCIATION cannot be null!!");
            } else {
                xmlWriter.writeCharacters(localVISITOR_ASSOCIATION);
            }

            xmlWriter.writeEndElement();
        }

        if (localCOLUMNSTracker) {
            if (localCOLUMNS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "COLUMNS cannot be null!!");
            }

            localCOLUMNS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "COLUMNS"), xmlWriter);
        }

        if (localCONTACT_LISTSTracker) {
            if (localCONTACT_LISTS == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "CONTACT_LISTS cannot be null!!");
            }

            localCONTACT_LISTS.serialize(new javax.xml.namespace.QName(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                    "CONTACT_LISTS"), xmlWriter);
        }

        xmlWriter.writeEndElement();
    }

    private static java.lang.String generatePrefix(java.lang.String namespace) {
        if (namespace.equals(
                    "SilverpopApi:EngageService.ListMgmt.RecipientActions")) {
            return "ns4";
        }

        return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
    }

    /**
     * Utility method to write an element start tag.
     */
    private void writeStartElement(java.lang.String prefix,
        java.lang.String namespace, java.lang.String localPart,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
        } else {
            if (namespace.length() == 0) {
                prefix = "";
            } else if (prefix == null) {
                prefix = generatePrefix(namespace);
            }

            xmlWriter.writeStartElement(prefix, localPart, namespace);
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }
    }

    /**
     * Util method to write an attribute with the ns prefix
     */
    private void writeAttribute(java.lang.String prefix,
        java.lang.String namespace, java.lang.String attName,
        java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

        if (writerPrefix != null) {
            xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
        } else {
            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
            xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeAttribute(java.lang.String namespace,
        java.lang.String attName, java.lang.String attValue,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attValue);
        } else {
            xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace),
                namespace, attName, attValue);
        }
    }

    /**
     * Util method to write an attribute without the ns prefix
     */
    private void writeQNameAttribute(java.lang.String namespace,
        java.lang.String attName, javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String attributeNamespace = qname.getNamespaceURI();
        java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

        if (attributePrefix == null) {
            attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
        }

        java.lang.String attributeValue;

        if (attributePrefix.trim().length() > 0) {
            attributeValue = attributePrefix + ":" + qname.getLocalPart();
        } else {
            attributeValue = qname.getLocalPart();
        }

        if (namespace.equals("")) {
            xmlWriter.writeAttribute(attName, attributeValue);
        } else {
            registerPrefix(xmlWriter, namespace);
            xmlWriter.writeAttribute(attributePrefix, namespace, attName,
                attributeValue);
        }
    }

    /**
     *  method to handle Qnames
     */
    private void writeQName(javax.xml.namespace.QName qname,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String namespaceURI = qname.getNamespaceURI();

        if (namespaceURI != null) {
            java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

            if (prefix == null) {
                prefix = generatePrefix(namespaceURI);
                xmlWriter.writeNamespace(prefix, namespaceURI);
                xmlWriter.setPrefix(prefix, namespaceURI);
            }

            if (prefix.trim().length() > 0) {
                xmlWriter.writeCharacters(prefix + ":" +
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            } else {
                // i.e this is the default namespace
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        } else {
            xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    qname));
        }
    }

    private void writeQNames(javax.xml.namespace.QName[] qnames,
        javax.xml.stream.XMLStreamWriter xmlWriter)
        throws javax.xml.stream.XMLStreamException {
        if (qnames != null) {
            // we have to store this data until last moment since it is not possible to write any
            // namespace data after writing the charactor data
            java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
            java.lang.String namespaceURI = null;
            java.lang.String prefix = null;

            for (int i = 0; i < qnames.length; i++) {
                if (i > 0) {
                    stringToWrite.append(" ");
                }

                namespaceURI = qnames[i].getNamespaceURI();

                if (namespaceURI != null) {
                    prefix = xmlWriter.getPrefix(namespaceURI);

                    if ((prefix == null) || (prefix.length() == 0)) {
                        prefix = generatePrefix(namespaceURI);
                        xmlWriter.writeNamespace(prefix, namespaceURI);
                        xmlWriter.setPrefix(prefix, namespaceURI);
                    }

                    if (prefix.trim().length() > 0) {
                        stringToWrite.append(prefix).append(":")
                                     .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                } else {
                    stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qnames[i]));
                }
            }

            xmlWriter.writeCharacters(stringToWrite.toString());
        }
    }

    /**
     * Register a namespace prefix
     */
    private java.lang.String registerPrefix(
        javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
        throws javax.xml.stream.XMLStreamException {
        java.lang.String prefix = xmlWriter.getPrefix(namespace);

        if (prefix == null) {
            prefix = generatePrefix(namespace);

            javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

            while (true) {
                java.lang.String uri = nsContext.getNamespaceURI(prefix);

                if ((uri == null) || (uri.length() == 0)) {
                    break;
                }

                prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
            }

            xmlWriter.writeNamespace(prefix, namespace);
            xmlWriter.setPrefix(prefix, namespace);
        }

        return prefix;
    }

    /**
     *  Factory class that keeps the parse method
     */
    public static class Factory {
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        /**
         * static method to create the object
         * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
         *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
         * Postcondition: If this object is an element, the reader is positioned at its end element
         *                If this object is a complex type, the reader is positioned at the end element of its outer element
         */
        public static ListMgmtResponseType parse(
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            ListMgmtResponseType object = new ListMgmtResponseType();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix = "";
            java.lang.String namespaceuri = "";

            try {
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();

                if (reader.getAttributeValue(
                            "http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
                    java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "type");

                    if (fullTypeName != null) {
                        java.lang.String nsPrefix = null;

                        if (fullTypeName.indexOf(":") > -1) {
                            nsPrefix = fullTypeName.substring(0,
                                    fullTypeName.indexOf(":"));
                        }

                        nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                        java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                    ":") + 1);

                        if (!"ListMgmtResponseType".equals(type)) {
                            //find namespace for the prefix
                            java.lang.String nsUri = reader.getNamespaceContext()
                                                           .getNamespaceURI(nsPrefix);

                            return (ListMgmtResponseType) mailmanagement.engageservice.ExtensionMapper.getTypeObject(nsUri,
                                type, reader);
                        }
                    }
                }

                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();

                reader.next();

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "SUCCESS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "SUCCESS").equals(
                            reader.getName())) {
                    object.setSUCCESS(recipientactions.listmgmt.engageservice.ListMgmtSuccess.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "Fault").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "Fault").equals(
                            reader.getName())) {
                    object.setFault(recipientactions.listmgmt.engageservice.FaultType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "EMAIL").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EMAIL").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "EMAIL" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEMAIL(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "Email").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "Email").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "Email" + "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setEmailE(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "RecipientId").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "RecipientId").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "RecipientId" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setRecipientId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "EmailType").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "EmailType").equals(
                            reader.getName())) {
                    object.setEmailType(recipientactions.listmgmt.engageservice.EmailType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "LastModified").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "LastModified").equals(
                            reader.getName())) {
                    object.setLastModified(recipientactions.listmgmt.engageservice.DateTime1.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "CreatedFrom").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CreatedFrom").equals(
                            reader.getName())) {
                    object.setCreatedFrom(recipientactions.listmgmt.engageservice.CreatedFrom.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "OptedIn").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "OptedIn").equals(
                            reader.getName())) {
                    object.setOptedIn(recipientactions.listmgmt.engageservice.DateTime1.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "OptedOut").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "OptedOut").equals(
                            reader.getName())) {
                    object.setOptedOut(recipientactions.listmgmt.engageservice.DateTime1.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "ResumeSendDate").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ResumeSendDate").equals(
                            reader.getName())) {
                    object.setResumeSendDate(recipientactions.listmgmt.engageservice.Date1.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "ORGANIZATION_ID").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "ORGANIZATION_ID").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "ORGANIZATION_ID" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setORGANIZATION_ID(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "VISITOR_ASSOCIATION").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "VISITOR_ASSOCIATION").equals(
                            reader.getName())) {
                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                            "nil");

                    if ("true".equals(nillableValue) ||
                            "1".equals(nillableValue)) {
                        throw new org.apache.axis2.databinding.ADBException(
                            "The element: " + "VISITOR_ASSOCIATION" +
                            "  cannot be null");
                    }

                    java.lang.String content = reader.getElementText();

                    object.setVISITOR_ASSOCIATION(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            content));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "COLUMNS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "COLUMNS").equals(
                            reader.getName())) {
                    object.setCOLUMNS(recipientactions.listmgmt.engageservice.ColumnsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if ((reader.isStartElement() &&
                        new javax.xml.namespace.QName(
                            "SilverpopApi:EngageService.ListMgmt.RecipientActions",
                            "CONTACT_LISTS").equals(reader.getName())) ||
                        new javax.xml.namespace.QName("", "CONTACT_LISTS").equals(
                            reader.getName())) {
                    object.setCONTACT_LISTS(recipientactions.listmgmt.engageservice.ContactListsElementType.Factory.parse(
                            reader));

                    reader.next();
                } // End of if for expected property start element

                else {
                }

                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                if (reader.isStartElement()) {
                    // 2 - A start element we are not expecting indicates a trailing invalid property
                    throw new org.apache.axis2.databinding.ADBException(
                        "Unexpected subelement " + reader.getName());
                }
            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }
    } //end of factory class
}
