/**
 * EngageSoapApiClientService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.4  Built on : Oct 21, 2016 (10:47:34 BST)
 */
package engageservice;


/*
 *  EngageSoapApiClientService java interface
 */
public interface EngageSoapApiClientService {
    /**
     * Auto generated method signature
     *
     * @param deleteRelationalTableData
     * @param sessionHeader
     */
    public useractions.listmgmt.engageservice.RESULT deleteRelationalTableData(
        useractions.listmgmt.engageservice.DeleteRelationalTableData deleteRelationalTableData,
        sessionheader.engageservice.SessionHeader sessionHeader)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param deleteDCRuleset
     * @param sessionHeader0
     */
    public useractions.mailmanagement.engageservice.RESULT deleteDCRuleset(
        useractions.mailmanagement.engageservice.DeleteDCRuleset deleteDCRuleset,
        sessionheader.engageservice.SessionHeader sessionHeader0)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param addRecipient
     * @param sessionHeader2
     */
    public recipientactions.listmgmt.engageservice.RESULT addRecipient(
        recipientactions.listmgmt.engageservice.AddRecipient addRecipient,
        sessionheader.engageservice.SessionHeader sessionHeader2)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getAggregateTrackingForOrg
     * @param sessionHeader4
     */
    public reporting.engageservice.RESULT getAggregateTrackingForOrg(
        reporting.engageservice.GetAggregateTrackingForOrg getAggregateTrackingForOrg,
        sessionheader.engageservice.SessionHeader sessionHeader4)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param doubleOptInRecipient
     * @param sessionHeader6
     */
    public recipientactions.listmgmt.engageservice.RESULT doubleOptIn(
        recipientactions.listmgmt.engageservice.DoubleOptInRecipient doubleOptInRecipient,
        sessionheader.engageservice.SessionHeader sessionHeader6)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getReportIdByDate
     * @param sessionHeader8
     */
    public reporting.engageservice.RESULT getReportIdByDate(
        reporting.engageservice.GetReportIdByDate getReportIdByDate,
        sessionheader.engageservice.SessionHeader sessionHeader8)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getMailingTemplates
     * @param sessionHeader10
     */
    public useractions.mailmanagement.engageservice.RESULT getMailingTemplates(
        useractions.mailmanagement.engageservice.GetMailingTemplates getMailingTemplates,
        sessionheader.engageservice.SessionHeader sessionHeader10)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param deleteTable
     * @param sessionHeader12
     */
    public useractions.listmgmt.engageservice.RESULT deleteTable(
        useractions.listmgmt.engageservice.DeleteTable deleteTable,
        sessionheader.engageservice.SessionHeader sessionHeader12)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param exportMailingTemplate
     * @param sessionHeader14
     */
    public useractions.mailmanagement.engageservice.RESULT exportMailingTemplate(
        useractions.mailmanagement.engageservice.ExportMailingTemplate exportMailingTemplate,
        sessionheader.engageservice.SessionHeader sessionHeader14)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param optOutRecipient
     * @param sessionHeader16
     */
    public recipientactions.listmgmt.engageservice.RESULT optOutRecipient(
        recipientactions.listmgmt.engageservice.OptOutRecipient optOutRecipient,
        sessionheader.engageservice.SessionHeader sessionHeader16)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getSentMailingsForOrg
     * @param sessionHeader18
     */
    public reporting.engageservice.RESULT getSentMailingsForOrg(
        reporting.engageservice.GetSentMailingsForOrg getSentMailingsForOrg,
        sessionheader.engageservice.SessionHeader sessionHeader18)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param sendMailing
     */
    public mailmanagement.engageservice.RESULT sendMailing(
        mailmanagement.engageservice.SendMailing sendMailing)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param addContactToProgram
     * @param sessionHeader21
     */
    public useractions.listmgmt.engageservice.RESULT addContactToProgram(
        useractions.listmgmt.engageservice.AddContactToProgram addContactToProgram,
        sessionheader.engageservice.SessionHeader sessionHeader21)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param rawRecipientDataExport
     * @param sessionHeader23
     */
    public reporting.engageservice.RESULT rawRecipientDataExport(
        reporting.engageservice.RawRecipientDataExport rawRecipientDataExport,
        sessionheader.engageservice.SessionHeader sessionHeader23)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param replaceDCRuleset
     * @param sessionHeader25
     */
    public useractions.mailmanagement.engageservice.RESULT replaceDCRuleset(
        useractions.mailmanagement.engageservice.ReplaceDCRuleset replaceDCRuleset,
        sessionheader.engageservice.SessionHeader sessionHeader25)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getDCRuleset
     * @param sessionHeader27
     */
    public useractions.mailmanagement.engageservice.RESULT getDCRuleset(
        useractions.mailmanagement.engageservice.GetDCRuleset getDCRuleset,
        sessionheader.engageservice.SessionHeader sessionHeader27)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param listRecipientMailings
     * @param sessionHeader29
     */
    public useractions.listmgmt.engageservice.RESULT listRecipientMailings(
        useractions.listmgmt.engageservice.ListRecipientMailings listRecipientMailings,
        sessionheader.engageservice.SessionHeader sessionHeader29)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param previewMailing
     * @param sessionHeader31
     */
    public useractions.mailmanagement.engageservice.RESULT previewMailing(
        useractions.mailmanagement.engageservice.PreviewMailing previewMailing,
        sessionheader.engageservice.SessionHeader sessionHeader31)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getLists
     * @param sessionHeader33
     */
    public useractions.listmgmt.engageservice.RESULT getLists(
        useractions.listmgmt.engageservice.GetLists getLists,
        sessionheader.engageservice.SessionHeader sessionHeader33)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param removeRecipient
     * @param sessionHeader35
     */
    public useractions.listmgmt.engageservice.RESULT removeRecipient(
        useractions.listmgmt.engageservice.RemoveRecipient removeRecipient,
        sessionheader.engageservice.SessionHeader sessionHeader35)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param calculateQuery
     * @param sessionHeader37
     */
    public useractions.listmgmt.engageservice.RESULT calculateQuery(
        useractions.listmgmt.engageservice.CalculateQuery calculateQuery,
        sessionheader.engageservice.SessionHeader sessionHeader37)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param logout
     * @param sessionHeader39
     */
    public login.sessionmgmt.engageservice.RESULT logout(
        login.sessionmgmt.engageservice.Logout logout,
        sessionheader.engageservice.SessionHeader sessionHeader39)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param validateDCRuleset
     * @param sessionHeader41
     */
    public useractions.mailmanagement.engageservice.RESULT validateDCMailingRuleset(
        useractions.mailmanagement.engageservice.ValidateDCRuleset validateDCRuleset,
        sessionheader.engageservice.SessionHeader sessionHeader41)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getAggregateTrackingForUser
     * @param sessionHeader43
     */
    public reporting.engageservice.RESULT getAggregateTrackingForUser(
        reporting.engageservice.GetAggregateTrackingForUser getAggregateTrackingForUser,
        sessionheader.engageservice.SessionHeader sessionHeader43)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param selectRecipientData
     * @param sessionHeader45
     */
    public recipientactions.listmgmt.engageservice.RESULT selectRecipientData(
        recipientactions.listmgmt.engageservice.SelectRecipientData selectRecipientData,
        sessionheader.engageservice.SessionHeader sessionHeader45)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getListMetaData
     * @param sessionHeader47
     */
    public useractions.listmgmt.engageservice.RESULT getListMetaData(
        useractions.listmgmt.engageservice.GetListMetaData getListMetaData,
        sessionheader.engageservice.SessionHeader sessionHeader47)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getSentMailingsForUser
     * @param sessionHeader49
     */
    public reporting.engageservice.RESULT getSentMailingsForUser(
        reporting.engageservice.GetSentMailingsForUser getSentMailingsForUser,
        sessionheader.engageservice.SessionHeader sessionHeader49)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getAggregateTrackingForMailing
     * @param sessionHeader51
     */
    public reporting.engageservice.RESULT getAggregateTrackingForMailing(
        reporting.engageservice.GetAggregateTrackingForMailing getAggregateTrackingForMailing,
        sessionheader.engageservice.SessionHeader sessionHeader51)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param createContactList
     * @param sessionHeader53
     */
    public useractions.listmgmt.engageservice.RESULT createContactList(
        useractions.listmgmt.engageservice.CreateContactList createContactList,
        sessionheader.engageservice.SessionHeader sessionHeader53)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param joinTable
     * @param sessionHeader55
     */
    public useractions.listmgmt.engageservice.RESULT joinTable(
        useractions.listmgmt.engageservice.JoinTable joinTable,
        sessionheader.engageservice.SessionHeader sessionHeader55)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param importList
     * @param sessionHeader57
     */
    public useractions.listmgmt.engageservice.RESULT importList(
        useractions.listmgmt.engageservice.ImportList importList,
        sessionheader.engageservice.SessionHeader sessionHeader57)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param exportDCRuleset
     * @param sessionHeader59
     */
    public useractions.mailmanagement.engageservice.RESULT exportDCRuleset(
        useractions.mailmanagement.engageservice.ExportDCRuleset exportDCRuleset,
        sessionheader.engageservice.SessionHeader sessionHeader59)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param importDCRuleset
     * @param sessionHeader61
     */
    public useractions.mailmanagement.engageservice.RESULT importDCRuleset(
        useractions.mailmanagement.engageservice.ImportDCRuleset importDCRuleset,
        sessionheader.engageservice.SessionHeader sessionHeader61)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getSentMailingsForList
     * @param sessionHeader63
     */
    public reporting.engageservice.RESULT getSentMailingsForList(
        reporting.engageservice.GetSentMailingsForList getSentMailingsForList,
        sessionheader.engageservice.SessionHeader sessionHeader63)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param addListColumn
     * @param sessionHeader65
     */
    public useractions.listmgmt.engageservice.RESULT addListColumn(
        useractions.listmgmt.engageservice.AddListColumn addListColumn,
        sessionheader.engageservice.SessionHeader sessionHeader65)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param purgeTable
     * @param sessionHeader67
     */
    public useractions.listmgmt.engageservice.RESULT purgeTable(
        useractions.listmgmt.engageservice.PurgeTable purgeTable,
        sessionheader.engageservice.SessionHeader sessionHeader67)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param getJobStatus
     * @param sessionHeader69
     */
    public reporting.engageservice.RESULT getJobStatus(
        reporting.engageservice.GetJobStatus getJobStatus,
        sessionheader.engageservice.SessionHeader sessionHeader69)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param trackingMetricExport
     * @param sessionHeader71
     */
    public reporting.engageservice.RESULT trackingMetricExport(
        reporting.engageservice.TrackingMetricExport trackingMetricExport,
        sessionheader.engageservice.SessionHeader sessionHeader71)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param forwardToFriend
     */
    public mailmanagement.engageservice.FTF_RESULT forwardToFriend(
        mailmanagement.engageservice.ForwardToFriend forwardToFriend)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param deleteJob
     * @param sessionHeader73
     */
    public reporting.engageservice.RESULT deleteJob(
        reporting.engageservice.DeleteJob deleteJob,
        sessionheader.engageservice.SessionHeader sessionHeader73)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param createQuery
     * @param sessionHeader75
     */
    public useractions.listmgmt.engageservice.RESULT createQuery(
        useractions.listmgmt.engageservice.CreateQuery createQuery,
        sessionheader.engageservice.SessionHeader sessionHeader75)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param importTable
     * @param sessionHeader77
     */
    public useractions.listmgmt.engageservice.RESULT importTable(
        useractions.listmgmt.engageservice.ImportTable importTable,
        sessionheader.engageservice.SessionHeader sessionHeader77)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param login
     */
    public login.sessionmgmt.engageservice.RESULT login(
        login.sessionmgmt.engageservice.Login login)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param webTrackingDataExport
     * @param sessionHeader80
     */
    public reporting.engageservice.RESULT webTrackingDataExport(
        reporting.engageservice.WebTrackingDataExport webTrackingDataExport,
        sessionheader.engageservice.SessionHeader sessionHeader80)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param createTable
     * @param sessionHeader82
     */
    public useractions.listmgmt.engageservice.RESULT createTable(
        useractions.listmgmt.engageservice.CreateTable createTable,
        sessionheader.engageservice.SessionHeader sessionHeader82)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param exportTable
     * @param sessionHeader84
     */
    public useractions.listmgmt.engageservice.RESULT exportTable(
        useractions.listmgmt.engageservice.ExportTable exportTable,
        sessionheader.engageservice.SessionHeader sessionHeader84)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param scheduleMailing
     * @param sessionHeader86
     */
    public useractions.mailmanagement.engageservice.RESULT scheduleMailing(
        useractions.mailmanagement.engageservice.ScheduleMailing scheduleMailing,
        sessionheader.engageservice.SessionHeader sessionHeader86)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param insertUpdateRelationalTable
     * @param sessionHeader88
     */
    public useractions.listmgmt.engageservice.RESULT insertUpdateRelationalTable(
        useractions.listmgmt.engageservice.InsertUpdateRelationalTable insertUpdateRelationalTable,
        sessionheader.engageservice.SessionHeader sessionHeader88)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param addDCRuleset
     * @param sessionHeader90
     */
    public useractions.mailmanagement.engageservice.RESULT addDCRuleset(
        useractions.mailmanagement.engageservice.AddDCRuleset addDCRuleset,
        sessionheader.engageservice.SessionHeader sessionHeader90)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param importRecipientFieldData
     * @param sessionHeader92
     */
    public useractions.listmgmt.engageservice.RESULT importRecipientFieldData(
        useractions.listmgmt.engageservice.ImportRecipientFieldData importRecipientFieldData,
        sessionheader.engageservice.SessionHeader sessionHeader92)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param listDCRulesetsForMailing
     * @param sessionHeader94
     */
    public useractions.mailmanagement.engageservice.RESULT listDCRulesetsForMailing(
        useractions.mailmanagement.engageservice.ListDCRulesetsForMailing listDCRulesetsForMailing,
        sessionheader.engageservice.SessionHeader sessionHeader94)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param updateRecipient
     * @param sessionHeader96
     */
    public recipientactions.listmgmt.engageservice.RESULT updateRecipient(
        recipientactions.listmgmt.engageservice.UpdateRecipient updateRecipient,
        sessionheader.engageservice.SessionHeader sessionHeader96)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param exportList
     * @param sessionHeader98
     */
    public useractions.listmgmt.engageservice.RESULT exportList(
        useractions.listmgmt.engageservice.ExportList exportList,
        sessionheader.engageservice.SessionHeader sessionHeader98)
        throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param addContactToContactList
     * @param sessionHeader100
     */
    public useractions.listmgmt.engageservice.RESULT addContactToContactList(
        useractions.listmgmt.engageservice.AddContactToContactList addContactToContactList,
        sessionheader.engageservice.SessionHeader sessionHeader100)
        throws java.rmi.RemoteException;

    //
}
