
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.LeadResponseTypeJetAirways;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubmitGNAWithoutPrequalApplicationResult" type="{http://schemas.datacontract.org/2004/07/}LeadResponseTypeJetAirways" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "submitGNAWithoutPrequalApplicationResult"
})
@XmlRootElement(name = "SubmitGNAWithoutPrequalApplicationResponse")
public class SubmitGNAWithoutPrequalApplicationResponse {

    @XmlElementRef(name = "SubmitGNAWithoutPrequalApplicationResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<LeadResponseTypeJetAirways> submitGNAWithoutPrequalApplicationResult;

    /**
     * Gets the value of the submitGNAWithoutPrequalApplicationResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LeadResponseTypeJetAirways }{@code >}
     *     
     */
    public JAXBElement<LeadResponseTypeJetAirways> getSubmitGNAWithoutPrequalApplicationResult() {
        return submitGNAWithoutPrequalApplicationResult;
    }

    /**
     * Sets the value of the submitGNAWithoutPrequalApplicationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LeadResponseTypeJetAirways }{@code >}
     *     
     */
    public void setSubmitGNAWithoutPrequalApplicationResult(JAXBElement<LeadResponseTypeJetAirways> value) {
        this.submitGNAWithoutPrequalApplicationResult = value;
    }

}
