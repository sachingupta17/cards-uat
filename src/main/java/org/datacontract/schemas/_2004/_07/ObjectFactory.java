
package org.datacontract.schemas._2004._07;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UserLeadDetailsJetAirways_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "UserLeadDetailsJetAirways");
    private final static QName _LeadResponseTypeJetAirways_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "LeadResponseTypeJetAirways");
    private final static QName _LeadResponseTypeJetAirwaysDeclineReason_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_declineReason");
    private final static QName _LeadResponseTypeJetAirwaysGnastatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_gnastatus");
    private final static QName _LeadResponseTypeJetAirwaysMessageToBeDisplayed_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_messageToBeDisplayed");
    private final static QName _LeadResponseTypeJetAirwaysPCNNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_PCNNumber");
    private final static QName _LeadResponseTypeJetAirwaysReferenceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_referenceId");
    private final static QName _LeadResponseTypeJetAirwaysValidationErrorDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_validationErrorDescription");
    private final static QName _UserLeadDetailsJetAirwaysFNAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_FNAME");
    private final static QName _UserLeadDetailsJetAirwaysConsentForAdditionalPreviliges_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_consentForAdditionalPreviliges");
    private final static QName _UserLeadDetailsJetAirwaysPermaddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_permaddress2");
    private final static QName _UserLeadDetailsJetAirwaysPermaddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_permaddress3");
    private final static QName _UserLeadDetailsJetAirwaysJetPriviligeMembershipTier_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_JetPriviligeMembershipTier");
    private final static QName _UserLeadDetailsJetAirwaysPermcity_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_permcity");
    private final static QName _UserLeadDetailsJetAirwaysOState_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_O_State");
    private final static QName _UserLeadDetailsJetAirwaysPartner_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_partner");
    private final static QName _UserLeadDetailsJetAirwaysIP_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_IP");
    private final static QName _UserLeadDetailsJetAirwaysJetPriviligeMembershipNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_jetPriviligeMembershipNumber");
    private final static QName _UserLeadDetailsJetAirwaysConsentForOnlineStatements_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_consentForOnlineStatements");
    private final static QName _UserLeadDetailsJetAirwaysJetStatementTobeSentTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_jetStatementTobeSentTO");
    private final static QName _UserLeadDetailsJetAirwaysAddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_address2");
    private final static QName _UserLeadDetailsJetAirwaysCreditCardNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_creditCardNumber");
    private final static QName _UserLeadDetailsJetAirwaysAddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_address3");
    private final static QName _UserLeadDetailsJetAirwaysOPHONE_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_O_PHONE");
    private final static QName _UserLeadDetailsJetAirwaysJetPriviligeMember_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_jetPriviligeMember");
    private final static QName _UserLeadDetailsJetAirwaysPeraddresssameascurr_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_peraddresssameascurr");
    private final static QName _UserLeadDetailsJetAirwaysCampaignCategorization_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_CampaignCategorization");
    private final static QName _UserLeadDetailsJetAirwaysConsentForEmailCommunication_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_consentForEmailCommunication");
    private final static QName _UserLeadDetailsJetAirwaysConsentForPaybackPointsEnrolment_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_consentForPaybackPointsEnrolment");
    private final static QName _UserLeadDetailsJetAirwaysCreditCardType_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_creditCardType");
    private final static QName _UserLeadDetailsJetAirwaysChosenCard_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_chosenCard");
    private final static QName _UserLeadDetailsJetAirwaysGENDER_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_GENDER");
    private final static QName _UserLeadDetailsJetAirwaysSTD_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_STD");
    private final static QName _UserLeadDetailsJetAirwaysPHONE_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_PHONE");
    private final static QName _UserLeadDetailsJetAirwaysPlatinumCardBillingPreference_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_platinumCardBillingPreference");
    private final static QName _UserLeadDetailsJetAirwaysAccesstype_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_accesstype");
    private final static QName _UserLeadDetailsJetAirwaysAadharnumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_aadharnumber");
    private final static QName _UserLeadDetailsJetAirwaysEMAIL_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_EMAIL");
    private final static QName _UserLeadDetailsJetAirwaysPincode_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_pincode");
    private final static QName _UserLeadDetailsJetAirwaysCompanyName_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_companyName");
    private final static QName _UserLeadDetailsJetAirwaysEmploymentType_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_employmentType");
    private final static QName _UserLeadDetailsJetAirwaysDOB_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_DOB");
    private final static QName _UserLeadDetailsJetAirwaysMOBILE_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_MOBILE");
    private final static QName _UserLeadDetailsJetAirwaysOADDRESS3_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_O_ADDRESS3");
    private final static QName _UserLeadDetailsJetAirwaysOCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_O_City");
    private final static QName _UserLeadDetailsJetAirwaysOADDRESS2_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_O_ADDRESS2");
    private final static QName _UserLeadDetailsJetAirwaysPANCARD_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_PANCARD");
    private final static QName _UserLeadDetailsJetAirwaysDisclaimer_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_disclaimer");
    private final static QName _UserLeadDetailsJetAirwaysAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_address");
    private final static QName _UserLeadDetailsJetAirwaysPassword_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_password");
    private final static QName _UserLeadDetailsJetAirwaysEducationalQualification_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_educationalQualification");
    private final static QName _UserLeadDetailsJetAirwaysSourceOfLead_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_SourceOfLead");
    private final static QName _UserLeadDetailsJetAirwaysConsentForMarketingCommunicationOverPhone_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_consentForMarketingCommunicationOverPhone");
    private final static QName _UserLeadDetailsJetAirwaysPermaddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_permaddress");
    private final static QName _UserLeadDetailsJetAirwaysCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_city");
    private final static QName _UserLeadDetailsJetAirwaysLeadCampaign_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_LeadCampaign");
    private final static QName _UserLeadDetailsJetAirwaysPermpincode_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_permpincode");
    private final static QName _UserLeadDetailsJetAirwaysMNAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_MNAME");
    private final static QName _UserLeadDetailsJetAirwaysOPincode_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_O_Pincode");
    private final static QName _UserLeadDetailsJetAirwaysTeleCallerFlag_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_TeleCallerFlag");
    private final static QName _UserLeadDetailsJetAirwaysPermstate_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_permstate");
    private final static QName _UserLeadDetailsJetAirwaysUsername_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_username");
    private final static QName _UserLeadDetailsJetAirwaysMonthlyInCome_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_monthlyInCome");
    private final static QName _UserLeadDetailsJetAirwaysState_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_state");
    private final static QName _UserLeadDetailsJetAirwaysConsentForInsurancePlanCommunication_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_consentForInsurancePlanCommunication");
    private final static QName _UserLeadDetailsJetAirwaysLNAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_LNAME");
    private final static QName _UserLeadDetailsJetAirwaysOADDRESS_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_O_ADDRESS");
    private final static QName _UserLeadDetailsJetAirwaysOSTD_QNAME = new QName("http://schemas.datacontract.org/2004/07/", "_O_STD");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LeadResponseTypeJetAirways }
     * 
     */
    public LeadResponseTypeJetAirways createLeadResponseTypeJetAirways() {
        return new LeadResponseTypeJetAirways();
    }

    /**
     * Create an instance of {@link UserLeadDetailsJetAirways }
     * 
     */
    public UserLeadDetailsJetAirways createUserLeadDetailsJetAirways() {
        return new UserLeadDetailsJetAirways();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserLeadDetailsJetAirways }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "UserLeadDetailsJetAirways")
    public JAXBElement<UserLeadDetailsJetAirways> createUserLeadDetailsJetAirways(UserLeadDetailsJetAirways value) {
        return new JAXBElement<UserLeadDetailsJetAirways>(_UserLeadDetailsJetAirways_QNAME, UserLeadDetailsJetAirways.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LeadResponseTypeJetAirways }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "LeadResponseTypeJetAirways")
    public JAXBElement<LeadResponseTypeJetAirways> createLeadResponseTypeJetAirways(LeadResponseTypeJetAirways value) {
        return new JAXBElement<LeadResponseTypeJetAirways>(_LeadResponseTypeJetAirways_QNAME, LeadResponseTypeJetAirways.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_declineReason", scope = LeadResponseTypeJetAirways.class)
    public JAXBElement<String> createLeadResponseTypeJetAirwaysDeclineReason(String value) {
        return new JAXBElement<String>(_LeadResponseTypeJetAirwaysDeclineReason_QNAME, String.class, LeadResponseTypeJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_gnastatus", scope = LeadResponseTypeJetAirways.class)
    public JAXBElement<String> createLeadResponseTypeJetAirwaysGnastatus(String value) {
        return new JAXBElement<String>(_LeadResponseTypeJetAirwaysGnastatus_QNAME, String.class, LeadResponseTypeJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_messageToBeDisplayed", scope = LeadResponseTypeJetAirways.class)
    public JAXBElement<String> createLeadResponseTypeJetAirwaysMessageToBeDisplayed(String value) {
        return new JAXBElement<String>(_LeadResponseTypeJetAirwaysMessageToBeDisplayed_QNAME, String.class, LeadResponseTypeJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_PCNNumber", scope = LeadResponseTypeJetAirways.class)
    public JAXBElement<String> createLeadResponseTypeJetAirwaysPCNNumber(String value) {
        return new JAXBElement<String>(_LeadResponseTypeJetAirwaysPCNNumber_QNAME, String.class, LeadResponseTypeJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_referenceId", scope = LeadResponseTypeJetAirways.class)
    public JAXBElement<String> createLeadResponseTypeJetAirwaysReferenceId(String value) {
        return new JAXBElement<String>(_LeadResponseTypeJetAirwaysReferenceId_QNAME, String.class, LeadResponseTypeJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_validationErrorDescription", scope = LeadResponseTypeJetAirways.class)
    public JAXBElement<String> createLeadResponseTypeJetAirwaysValidationErrorDescription(String value) {
        return new JAXBElement<String>(_LeadResponseTypeJetAirwaysValidationErrorDescription_QNAME, String.class, LeadResponseTypeJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_FNAME", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysFNAME(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysFNAME_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_consentForAdditionalPreviliges", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysConsentForAdditionalPreviliges(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysConsentForAdditionalPreviliges_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_permaddress2", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPermaddress2(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPermaddress2_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_permaddress3", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPermaddress3(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPermaddress3_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_JetPriviligeMembershipTier", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysJetPriviligeMembershipTier(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysJetPriviligeMembershipTier_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_permcity", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPermcity(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPermcity_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_O_State", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysOState(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysOState_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_partner", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPartner(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPartner_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_IP", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysIP(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysIP_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_jetPriviligeMembershipNumber", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysJetPriviligeMembershipNumber(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysJetPriviligeMembershipNumber_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_consentForOnlineStatements", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysConsentForOnlineStatements(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysConsentForOnlineStatements_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_jetStatementTobeSentTO", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysJetStatementTobeSentTO(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysJetStatementTobeSentTO_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_address2", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysAddress2(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysAddress2_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_creditCardNumber", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysCreditCardNumber(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysCreditCardNumber_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_address3", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysAddress3(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysAddress3_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_O_PHONE", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysOPHONE(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysOPHONE_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_jetPriviligeMember", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysJetPriviligeMember(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysJetPriviligeMember_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_peraddresssameascurr", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPeraddresssameascurr(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPeraddresssameascurr_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_CampaignCategorization", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysCampaignCategorization(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysCampaignCategorization_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_consentForEmailCommunication", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysConsentForEmailCommunication(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysConsentForEmailCommunication_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_consentForPaybackPointsEnrolment", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysConsentForPaybackPointsEnrolment(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysConsentForPaybackPointsEnrolment_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_creditCardType", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysCreditCardType(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysCreditCardType_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_chosenCard", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysChosenCard(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysChosenCard_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_GENDER", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysGENDER(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysGENDER_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_STD", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysSTD(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysSTD_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_PHONE", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPHONE(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPHONE_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_platinumCardBillingPreference", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPlatinumCardBillingPreference(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPlatinumCardBillingPreference_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_accesstype", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysAccesstype(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysAccesstype_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_aadharnumber", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysAadharnumber(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysAadharnumber_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_EMAIL", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysEMAIL(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysEMAIL_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_pincode", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPincode(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPincode_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_companyName", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysCompanyName(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysCompanyName_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_employmentType", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysEmploymentType(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysEmploymentType_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_DOB", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysDOB(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysDOB_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_MOBILE", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysMOBILE(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysMOBILE_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_O_ADDRESS3", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysOADDRESS3(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysOADDRESS3_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_O_City", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysOCity(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysOCity_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_O_ADDRESS2", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysOADDRESS2(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysOADDRESS2_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_PANCARD", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPANCARD(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPANCARD_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_disclaimer", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysDisclaimer(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysDisclaimer_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_address", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysAddress(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysAddress_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_password", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPassword(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPassword_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_educationalQualification", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysEducationalQualification(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysEducationalQualification_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_SourceOfLead", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysSourceOfLead(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysSourceOfLead_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_consentForMarketingCommunicationOverPhone", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysConsentForMarketingCommunicationOverPhone(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysConsentForMarketingCommunicationOverPhone_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_permaddress", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPermaddress(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPermaddress_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_city", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysCity(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysCity_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_LeadCampaign", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysLeadCampaign(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysLeadCampaign_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_permpincode", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPermpincode(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPermpincode_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_MNAME", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysMNAME(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysMNAME_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_O_Pincode", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysOPincode(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysOPincode_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_TeleCallerFlag", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysTeleCallerFlag(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysTeleCallerFlag_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_permstate", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysPermstate(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysPermstate_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_username", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysUsername(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysUsername_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_monthlyInCome", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysMonthlyInCome(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysMonthlyInCome_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_state", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysState(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysState_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_consentForInsurancePlanCommunication", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysConsentForInsurancePlanCommunication(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysConsentForInsurancePlanCommunication_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_LNAME", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysLNAME(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysLNAME_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_O_ADDRESS", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysOADDRESS(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysOADDRESS_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/", name = "_O_STD", scope = UserLeadDetailsJetAirways.class)
    public JAXBElement<String> createUserLeadDetailsJetAirwaysOSTD(String value) {
        return new JAXBElement<String>(_UserLeadDetailsJetAirwaysOSTD_QNAME, String.class, UserLeadDetailsJetAirways.class, value);
    }

}
